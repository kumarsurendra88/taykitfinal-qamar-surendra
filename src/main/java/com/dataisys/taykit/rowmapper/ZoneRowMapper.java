package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.Zone;

public class ZoneRowMapper implements RowMapper<Zone>{

	
	public Zone mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
	
		Zone zone=new Zone();
	/*zone.setZone_id(rs.getInt("zone_id"));
	zone.setZone_name(rs.getString("zone_name"));
	System.out.println("zone name in rowmapper : "+rs.getString("zone_name"));
	return zone;*/
		
		zone.setZone_id(rs.getInt("zone_id"));
		zone.setZone_name(rs.getString("zone_name"));
//		System.out.println("zone name in rowmapper : "+rs.getString("zone_name"));
		return zone;
}
	
}
