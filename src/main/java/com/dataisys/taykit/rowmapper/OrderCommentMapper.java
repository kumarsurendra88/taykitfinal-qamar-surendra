package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.OrderComment;

public class OrderCommentMapper implements RowMapper<OrderComment> {

	public OrderComment mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderComment obj=new OrderComment();
		obj.setCommentId(rs.getDouble("comment_id"));
		obj.setTrackingId(rs.getString("tracking_id"));
		obj.setCommentText(rs.getString("comment_text"));
		//obj.setCommentDate(rs.getDate("comment_date"));
		obj.setCommentBy(rs.getString("comment_by"));
		obj.setUpdatedOn(rs.getTimestamp("updated_on"));
		obj.setReceived_by(rs.getString("received_by"));
		return obj;
	}

}
