package com.dataisys.taykit.model;

public class RegistrationKey {
	
	private String regKey;
	private String accountNo;	
	
	public RegistrationKey() {
		
	}
	
	public RegistrationKey(String regKey, String accountNo) {
		super();
		this.regKey = regKey;
		this.accountNo = accountNo;
	}
	public String getRegKey() {
		return regKey;
	}
	public void setRegKey(String regKey) {
		this.regKey = regKey;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
}
