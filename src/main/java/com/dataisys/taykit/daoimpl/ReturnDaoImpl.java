package com.dataisys.taykit.daoimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RetailerDao;
import com.dataisys.taykit.dao.ReturnDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.CancelledOrderByData;
import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.SearchReturn;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.OrderReturnRowMapper;
import com.dataisys.taykit.rowmapper.RandomReturnRowMapper;
import com.dataisys.taykit.rowmapper.ReturnAdvanceSearchRowMapper;
import com.dataisys.taykit.rowmapper.RunsheetMapperForStore;
import com.dataisys.taykit.rowmapper.StoreRowMapper;
import com.dataisys.taykit.services.OtpService;

public class ReturnDaoImpl extends JdbcDaoSupport implements ReturnDao {
	
	@Autowired
	PudoStoreDao storeDao;
	@Autowired
	DeliveryBoyDao deliveryBoyDao;	
	@Autowired
	UserDao userDao;
	@Autowired
	RetailerDao retailerDao;
	

	public void makeReturnEntry(Return returnvalues) {
		// TODO Auto-generated method stub
		
		String sql="insert into spot_return(tracking_id,reverse_id,retailer_id,date,category,details,customer_name,customer_address,pincode,contact_no,emailid,approved,updated_by,product_code,quantity,value)"+
					"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int i=getJdbcTemplate().update(sql, returnvalues.getTrackingId(),returnvalues.getReverseId(),returnvalues.getRetailerId(),returnvalues.getReturnDate(),returnvalues.getCategory(),returnvalues.getDetails(),returnvalues.getCustomerName(),returnvalues.getCustomerAddress(),returnvalues.getPincode(),returnvalues.getContactNo(),returnvalues.getEmailId(),returnvalues.getApproved(),returnvalues.getUpdateBy(),returnvalues.getProductCode(),returnvalues.getQuantity(),returnvalues.getQuantity());
		
			if(i==1){
					sendingEmail(returnvalues);
			}
	
	}

	public List<Return> returnvalues(HttpSession session) {
		// TODO Auto-generated method stub
		/*User user=(User)session.getAttribute("user");
		double storeId=(Double)session.getAttribute("storeId");
		if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")){
			
			String sql="select *from order_return";
			System.out.println("insdie returnDaoImpl");
			List<Return> values=getJdbcTemplate().query(sql, new OrderReturnRowMapper());
			
			return values;
		}
		String sql="select *from order_return where store_id="+storeId+"or reporting_store_id="+storeId;
		System.out.println("insdie returnDaoImpl");
		List<Return> values=getJdbcTemplate().query(sql, new OrderReturnRowMapper());
		
		return values;*/
		// TODO Auto-generated method stub
				String sql="select *from order_return";
				System.out.println("insdie returnDaoImpl");
				List<Return> values=getJdbcTemplate().query(sql, new OrderReturnRowMapper());
				
				return values;
	}
	
	private void sendingEmail(Return values) {
		System.out.println("Im in sending Email");
	    final String username = "mabrar91@gmail.com";
	    final String password = "";

	    Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
	    props.put("mail.smtp.port", "587"); //TLS Port
	    props.put("mail.smtp.auth", "true"); //enable authentication
	    props.put("mail.smtp.starttls.enable", "true");
	    Session session = Session.getInstance(props,
	            new javax.mail.Authenticator() {
	                protected PasswordAuthentication getPasswordAuthentication() {
	                    return new PasswordAuthentication(username, password);
	                }
	            });

	    try {

	        Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress("mabrar91@gmail.com"));
	        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("mabrar91@gmail.com"));
	        message.setText("Details of Order Return\n\nInvoice No:"+values.getInvoiceNo()+"\n Tracking Id:"+values.getTrackingId());
	        
	        MimeBodyPart bodyPart = new MimeBodyPart(); // code for setting text with body
	        String body = "test";	        
	        bodyPart.setContent(body, "text/html");
	        System.out.println("Sending");
	        Transport.send(message);
	        Thread.sleep(6000);
	        System.out.println("Done");

	    } catch (MessagingException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	    	System.err.println("Error: "+e.getMessage());
	    }
	}	
	
	
	public void makeReturnEntry1(com.dataisys.taykit.model.Return returns) {
		// TODO Auto-generated method stub

		String sql="insert into order_return(tracking_id,order_id,return_id,courier_service,customer_name,city_name,state,country,address,pincode,customer_phone,customer_mobile,order_type,order_value,order_details,weight,retailer_id)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		getJdbcTemplate().update(sql,returns.getTrackingId(),returns.getOrderId(),returns.getReturnId(),returns.getCourierService(),returns.getConsigneeName(),returns.getCity(),returns.getState(),returns.getCountry(),returns.getCustomerAddress(),
				returns.getPincode(),returns.getPhone(),returns.getContactNo(),returns.getPaymentMode(),returns.getOrderValue(),returns.getProductToBeShipped(),returns.getWeight(),returns.getRetailerId());
		
		System.out.println("Making success entry");
		
		/*String sql="update order_return set address=? where return_id=?";
		getJdbcTemplate().update(sql,returns.getCustomerAddress(),returns.getReturnId());*/




		}

	public Return retrurnOrder(String trackingId) {
		// TODO Auto-generated method stub
		
		
			String sql = "select * from `order_return` where tracking_id=? ";
			try {
				return getJdbcTemplate().queryForObject(sql,
						new Object[] { trackingId }, new OrderReturnRowMapper());
			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
				return null;
			}

		}
		
	public void assignToDeliveryBoy(AssignDeliveryBoy obj) {

		double fromstoreId = storeDao.getStoreIdByName(obj.getFromLocation());
		DeliveryBoys boy = deliveryBoyDao.DeliveryBoyId(obj.getAssignTo());
		String sql1="select attempts from `order_return` where tracking_id=?";
		System.out.println("obj.getTrackingId() : "+obj.getTrackingId());
		
		//int attempts=getJdbcTemplate().queryForInt(sql1, obj.getTrackingId());
		@SuppressWarnings("deprecation")
		int attempts=getJdbcTemplate().queryForInt(sql1, obj.getTrackingId());
		System.out.println("attempts : "+attempts);
		int numAttempts=0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dbAssignedDate = new Date();
		System.out.println(dateFormat.format(dbAssignedDate));
		if(attempts>0){
		numAttempts=attempts+1;
		}else{
		numAttempts=1;
		}
	//	String sql = "update `order_return` set order_status=?,assigned_to=?,location=?,attempts=?,delivery_boy_assign_date=? where tracking_id=?";
		
		String sql3 = "update `order_return` set  runsheet_number=?, order_status=?,assigned_to=?,location=?,attempts=?,delivery_boy_assign_date=? where tracking_id=?";
		System.out.println("runsheet number in returnDaoImpl : "+obj.getRunSheetNumber());
		getJdbcTemplate().update(sql3, obj.getRunSheetNumber(),"Picked Up for Delivery",obj.getAssignTo(),"OFD",numAttempts,dateFormat.format(dbAssignedDate),obj.getTrackingId());
		numAttempts=0;
	//*******adding runsheet dump starts**********//
		
		String sqlR="insert into runsheetdump values(?,?,?,?)";
		getJdbcTemplate().update(sqlR,obj.getRunSheetNumber(),obj.getTrackingId(),obj.getAssignTo(),dateFormat.format(dbAssignedDate));
		System.out.println("inserted into runsheet dump");
		//*******adding runsheet dump ends**********//
		
		String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";

		getJdbcTemplate().update(sql2,"Assigned to "+obj.getAssignTo(),fromstoreId,"Out for delivery", obj.getTrackingId(), new Date());
		String queryForOrder="select *from `order_return` where tracking_id=?";
		Return ord=getJdbcTemplate().queryForObject(queryForOrder,new Object[] { obj.getTrackingId()},new OrderReturnRowMapper());
		OtpService otp=new OtpService();
		String cstName=ord.getCustomerName();
		String trackingId=ord.getTrackingId();
		Order order=new Order();
		order.setCustomerName(cstName);
		order.setTrackingId(trackingId);
		System.out.println("befor sending otp");
		otp.sendingOTPToCustomer(order);
		
		
		}
	
	public List<Return> getRunSheetDeliveries(Return order) {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dbassignedDate = new Date();
		String sql="SELECT * FROM `order_return` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered' AND runsheet_number ='NONE'";
		List<Return> orders=getJdbcTemplate().query(sql,new Object[] { order.getAssignedTo(),dateFormat.format(dbassignedDate) },new OrderReturnRowMapper());

		System.out.println("size of list is" +orders.size());
		return orders;
	}
	
	public void getRunsheetNumber(String userNames, String randomNumbers) {
		// TODO Auto-generated method stub
		
		
	
		/*System.out.println("while updating");
		java.util.Date utilDate = new java.util.Date();*/
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		System.out.println("delivery boy"+dateFormat.format(date));
		
		
		String sql="select runsheet_number,tracking_id from `order_return` where assigned_to=? and delivery_boy_assign_date=?";
		List <Return> ords=getJdbcTemplate().query(sql,new Object[]{userNames,dateFormat.format(date)},new RandomReturnRowMapper());
			for(Return order:ords){
				
				System.out.println("runsheet numbers are"+order.getRunSheetNo());
				if(order.getRunSheetNo().equalsIgnoreCase("NONE")){

					System.out.println("inside if");
					System.out.println("tracking id is"+order.getTrackingId());
					
					
					Return o=new Return();
					o.setRunSheetNo(randomNumbers);
				
					o.setAssignedTo(userNames);
					
					try{
						String status="OPEN";
						getJdbcTemplate().update("update `order_return` set runsheet_number="+"\""+randomNumbers+"\""+"," +"runsheet_status="+"\""+status+"\""  +"where assigned_to="+"\""+userNames+"\"" +"and delivery_boy_assign_date="+"\""+dateFormat.format(date)+"\""+"and tracking_id="+"\""+order.getTrackingId()+"\"");
						
					
				System.out.println("after updating");
					}catch(Exception e){
						System.out.println(e.getMessage());
					}
					
				
					
				}
				else{
					
					System.out.println("run sheet number already availabale");
				}
			}
		
		
		
		
		
	}

	
	public com.dataisys.taykit.model.Return getOrders(String trackingtId) {
		String sql = "select * from `order_return` where tracking_id=? ";
		try {
			return getJdbcTemplate().queryForObject(sql,
					new Object[] { trackingtId }, new OrderReturnRowMapper());
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			return null;
		}

	}
	
	
	public void updateOrderStatus(com.dataisys.taykit.model.Return object) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date orderStatusDate = new Date();
		if(object.getStatus().equalsIgnoreCase("scheduled delivery")){
			String sql = "update `order_return` set order_status=? ,status_reason=?, delivery_date=?,recent_status_date=?,location=? where tracking_id=?";
			String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
			getJdbcTemplate().update(sql, object.getStatus(),object.getStatusReason(),object.getDeliveryDate(),dateFormat.format(orderStatusDate),"",object.getTrackingId());
			getJdbcTemplate().update(sql2,"status updated as " + object.getStatus() +" on "+object.getDeliveryDate(),object.getStatusReason(),object.getStatus(), object.getTrackingId(), new Date());

			
		}
		if(object.getStatus().equalsIgnoreCase("returned")){

		
			
			String sql = "update `order_return` set order_status=? ,status_reason=?, delivered_date=?,recent_status_date=?,location=? where tracking_id=?";
		String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql, object.getStatus(),object.getStatusReason(),new Date(),dateFormat.format(orderStatusDate),"",object.getTrackingId());
		getJdbcTemplate().update(sql2,"status updated as " + object.getStatus(),object.getStatusReason(),object.getStatus(), object.getTrackingId(), new Date());

		}
		else
		{
		String sql = "update `order_return` set order_status=?,status_reason=?,recent_status_date=? where tracking_id=?";
		String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql, object.getStatus(),object.getStatusReason(),dateFormat.format(orderStatusDate),object.getTrackingId());
		getJdbcTemplate().update(sql2,
		"status updated as " + object.getStatus(),object.getStatusReason(),
		object.getStatusReason(), object.getTrackingId(), new Date());
		}

		}
	
	//Added By Kishore
	
		public List<Return> getAllReturnsForPickupByStoreId(String storeId) {
			
			Store store = storeDao.getStoreByAccountNo(storeId);
			System.out.println("Store Id fetched by giing storeId from client in getAllReturnsForPickupByStoreId is: "+store.getStoreId());
			
			String sql = "SELECT * FROM order_return WHERE store_id = ? AND order_type = ?";
			List<Return> listOfReturnsForPickup = getJdbcTemplate().query(sql,new Object[]{store.getStoreId(),"Pickup"},new OrderReturnRowMapper());
			return listOfReturnsForPickup;
		}
		
		public List<Return> returnvaluesByStoreId(String storeId) {
			Store store = storeDao.getStoreByAccountNo(storeId);
			String sql="SELECT * FROM order_return WHERE store_id = ? and order_status != ?";
			List<Return> listOfReturnsForPickup = getJdbcTemplate().query(sql,new Object[]{store.getStoreId(), "Returned"},new OrderReturnRowMapper());
			return listOfReturnsForPickup;
		}
		
		public void assignReturns(List<com.dataisys.taykit.model.Return> list,
				String assignId, String userType) {
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date dbAssignedDate = new Date();
			System.out.println(dateFormat.format(dbAssignedDate));
			
			if (userType.trim().equalsIgnoreCase("DELIVERY_BOY")) {
				System.out.println("Inside  Delivery_boy condition");
				//Added on 05092015
				
				User user = userDao.getUserByAccNumber(assignId);
				System.out.println("User: "+user);
				//End
				if(user != null) {
					//changing in code
					System.out.println("Inside if user != null and userName is: "+user.getUserName());
					String sql="update `order_return` set assigned_to=?, order_status = ?, location=?, attempts=?, delivery_boy_assign_date=? where tracking_id=?";

					Iterator<com.dataisys.taykit.model.Return> itr=list.iterator();
					while(itr.hasNext()) {
						com.dataisys.taykit.model.Return obj=itr.next();
						
						//for attempts
						String sql1="select attempts from `order_return` where tracking_id=?";
						int attempts=getJdbcTemplate().queryForInt(sql1, obj.getTrackingId());
						int numAttempts=0;
						if(attempts>0){
							numAttempts=attempts+1;
						} else{
							numAttempts=1;
						}				
						getJdbcTemplate().update(sql, user.getUserName(), "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), obj.getTrackingId());
						
						String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";
						getJdbcTemplate().update(sql2,"Assigned to "+assignId,obj.getStoreId(),"Out for delivery", obj.getTrackingId(), new Date());
					}
				} else {
					System.err.println("User is empty in OrderDaoImpl");
				}
			}
			
			if (userType.trim().equalsIgnoreCase("POD_USER")) {
				
				//Added on 05092015
				User user = userDao.getUserByAccNumber(assignId);
				//End
				if(user != null) {
					String sql="update `order_return` set assigned_to=? where tracking_id=?";
					Iterator<com.dataisys.taykit.model.Return> itr=list.iterator();
					while(itr.hasNext()) {
						com.dataisys.taykit.model.Return obj=itr.next();
						getJdbcTemplate().update(sql, user.getUserName(), obj.getTrackingId());
					}
				} else {
					System.err.println("User is null in OrderDaoImpl");
				}
				
			}				
		}
		
		public void updateOrderReturnDelivery(String trackingId, String orderStatus) {
			
			System.out.println("Inside updateOrderReturnDelivery");
			String sql="update `order_return` set order_status=?,delivered_date = ? where tracking_id=?";		
			
			//getJdbcTemplate().update(sql,orderStatus,new java.sql.Timestamp(cal.getTimeInMillis()),paymentStatus,trackingId);
			getJdbcTemplate().update(sql,orderStatus,new Date(),trackingId);		
			String sql2 = "insert into order_activity (activity_desc,status,tracking_id,activity_date) values(?,?,?,?)";
			getJdbcTemplate().update(sql2,"status updated as " + orderStatus, orderStatus, trackingId, new Date());
		}

		
		
		//Added By Surendra

		//Added By Surendra
		public List<Return> getPickupReturnsByDeliveryBoyId(String deliveryBoyAccNo) {	

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date dbAssignedDate = new Date();
			System.out.println(dateFormat.format(dbAssignedDate));

			//Added on 05092015
			User user = userDao.getUserByAccNumber(deliveryBoyAccNo);
			//End	
			String sql="SELECT * FROM order_return WHERE assigned_to = ? and order_type = ? and order_status != ? AND order_status NOT LIKE ? And delivery_boy_assign_date = ? AND (recent_status_date IS NULL OR recent_status_date != ?)";
			List<Return> listOfReturnsForPickup = getJdbcTemplate().query(sql,new Object[]{user.getUserName(),"pickup", "Returned","Order cancelled%", dateFormat.format(dbAssignedDate), dateFormat.format(dbAssignedDate)},new OrderReturnRowMapper());
			return listOfReturnsForPickup;
			}
		
		//return bulk assign
		
		public void updateAssignedToReturns(List<Return> returnOrders, String assignedTo) {
			System.out.println("Inside  returnDaoImpl updateAssignedToReturns");
			//Added on 05092015
			
			System.out.println("size of list inside update AssignedToReturns" +returnOrders.size());
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dbAssignedDate = new Date();
			System.out.println(dateFormat.format(dbAssignedDate));
			
			String sql="update `order_return` set assigned_to=?, order_status = ?, location=?, attempts=?, delivery_boy_assign_date=? where tracking_id=?";
			String sql1="select attempts from `order_return` where tracking_id=?";
			String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";
			/*returnOrders.remove(0);
			System.out.println("size of retturnOrdeers after removing 1st elememt is" +returnOrders.size());*/
			Return ord=returnOrders.get(0);
			if(ord.getTrackingId()==null || ord.getTrackingId().trim().equals("")){
				
				System.out.println("inside if statement");
				returnOrders.remove(0);
			
			}
			
			for(Return ret: returnOrders){
				
				System.out.println(" inside for loop tracking Id is" +ret.getTrackingId());
				
				
				int attempts=getJdbcTemplate().queryForInt(sql1, ret.getTrackingId());
				int numAttempts=0;
				if(attempts>0){
					numAttempts=attempts+1;
				} else{
					numAttempts=1;
				}				
				getJdbcTemplate().update(sql, assignedTo, "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), ret.getTrackingId());
			
				//for attempts
				//String sql1="select attempts from `order_return` where tracking_id=?";
			 			
				getJdbcTemplate().update(sql, assignedTo, "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), ret.getTrackingId());
				
			
				getJdbcTemplate().update(sql2,"Assigned to "+assignedTo,ret.getStoreId(),"Out for delivery", ret.getTrackingId(), new Date());
			
			}
			
		
			
			
			//End
		
				//changing in code
				
				
				
		}

		public void updateAssign(Return object, double assignId,
				HttpSession session) {
			// TODO Auto-generated method stub
			
	
				
				double storeId=(Double)session.getAttribute("storeId");
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date storeAssignDate = new Date();
				String sql = "update `order_return` set store_id=?,order_type=?,store_assign_date=?,reporting_store_id=? where tracking_id=?";

				getJdbcTemplate().update(sql, assignId, object.getOrderType(),dateFormat.format(storeAssignDate),storeId,object.getTrackingId());
				}
			
		public List<Return> ordersForStoreRunSheet(Return order) {
			// TODO Auto-generated method stub
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date storeAssignDate = new Date();
			String orderType=order.getOrderType();
			
			System.out.println("order type is"+orderType);
			String sql="select * from `order_return` where store_runsheet='NONE' and store_id=? and store_assign_date=? and order_type=?";
			
			List<Return> listOfStoreOrder= getJdbcTemplate().query(sql, new Object[]{order.getStoreId(),dateFormat.format(storeAssignDate),orderType},new OrderReturnRowMapper());
			System.out.println(listOfStoreOrder);
			return listOfStoreOrder;
		}

		public void getRunSheetNumberForStore(double storeId,
				String runsheetNumber, String orderType) {
			// TODO Auto-generated method stub
			

			// TODO Auto-generated method stub
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			System.out.println("delivery boy"+dateFormat.format(date));
			
			
			String sql="select store_runsheet,tracking_id from `order_return` where store_id=? and store_assign_date=? and order_type=? and store_runsheet='NONE'";
			List <Order> ords=getJdbcTemplate().query(sql,new Object[]{storeId,dateFormat.format(date),orderType},new RunsheetMapperForStore());
				for(Order order:ords){
					
					System.out.println("runsheet numbers are"+order.getRunSheetNumber());
					if(order.getStoreRunsheet().equalsIgnoreCase("NONE")){

						System.out.println("inside if");
						System.out.println("tracking id is"+order.getTrackingId());
						
						
						Order o=new Order();
						o.setStoreRunsheet(runsheetNumber);
						o.setStoreId(storeId);
						
						try{
							String status="OPEN";
							getJdbcTemplate().update("update `order_return` set store_runsheet="+"\""+runsheetNumber+"\""+"," +"runsheet_status="+"\""+status+"\""  +"where store_id="+"\""+storeId+"\"" +"and store_assign_date="+"\""+dateFormat.format(date)+"\""+"and tracking_id="+"\""+order.getTrackingId()+"\"");
							
						
					System.out.println("after updating");
						}catch(Exception e){
							System.out.println(e.getMessage());
						}
						
					
						
					}
					else{
						
						System.out.println("run sheet number already availabale");
					}
			
			
			
		}
		

			
		}
	
	//added 12/5/2015 3:36pm
		
public List<Return> getAllPendingReturns(HttpSession session, CancelledOrderByData cancelledOrderByData) {
			
			List<Return> allCancelledOrders = new ArrayList<Return>();
			System.out.println("RetailerName: "+cancelledOrderByData.getRetailerName());
			System.out.println("Status: "+cancelledOrderByData.getStatus());
			System.out.println("From Date: "+cancelledOrderByData.getFromDate());
			System.out.println("To Date: "+cancelledOrderByData.getToDate());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			Date fromDate = cancelledOrderByData.getFromDate();
			GregorianCalendar calFromDate = new GregorianCalendar();
			calFromDate.setTime(fromDate);
			
			Date toDate = cancelledOrderByData.getToDate();
			GregorianCalendar calToDate = new GregorianCalendar();
			calToDate.setTime(toDate);
			
			try {			
				
				User user = (User)session.getAttribute("user");
				int id = (int)retailerDao.getRetailerIdByName(cancelledOrderByData.getRetailerName());
				
				if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
					String sql="select * from `order_return` WHERE order_status IN(TRIM('Picked'), TRIM('Delivered'))  AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					System.out.println("Sql Statement: "+sql);
					allCancelledOrders = getJdbcTemplate().query(sql, new OrderReturnRowMapper());
					return allCancelledOrders;	
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("Store id is" +userId);
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("Store size is" +stores.size());
					
					for(Store str :stores) {					
							double storeId=str.getStoreId();
							String sql1="select * from `order_return` where store_id="+storeId+" AND order_status IN(TRIM('Picked'), TRIM('Delivered')) AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
							allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status IN(TRIM('Picked'), TRIM('Delivered')) AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {					
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status IN(TRIM('Picked'), TRIM('Delivered')) AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders= getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status IN(TRIM('Picked'), TRIM('Delivered')) AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
					double storeId=(Double)session.getAttribute("storeId");
					String sql1="select *from `order_return` where store_id="+storeId+" AND order_status IN(TRIM('Picked'), TRIM('Delivered')) AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					return allCancelledOrders;
				} else {
					return null;
				}			
			} catch(Exception e) {
				System.err.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			return allCancelledOrders;
		}
	
		public void updateReferenceNumberForReturn(CancelledOrderByData cancelledOrderByData) {
			System.out.println("Inside updateCancelledOrderStatus in OrderDaoImpl");
			List<String> trackingIds = cancelledOrderByData.getTrackingIds();
			String cancelledOrderRefNo = getReferenceValue();
			for (String trackingId : trackingIds) {
				String sql = "UPDATE `order_return` set return_order_ref_no = '"+cancelledOrderRefNo+"',order_status = 'Return Order Dispatched' WHERE tracking_id = '"+trackingId+"'";
				getJdbcTemplate().update(sql);
			}
			System.out.println("Updated Successfully !!!");
		}
		
		public static String getReferenceValue() {
			List<Integer> numbers = new ArrayList<Integer>();
		    for(int i = 0; i < 10; i++) {
		        numbers.add(i);
		    }
		    Collections.shuffle(numbers);
		    String result = "";
		    for(int i = 0; i < 10; i++) {
		        result += numbers.get(i).toString();
		    }
		    return "R"+result;
		}

		public List<Return> getRunSheetDeliveries2(String DeliveryBoyName,
				String runSheetNumber) {
			// TODO Auto-generated method stub
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
						Date dbassignedDate = new Date();
						//String sql="SELECT * FROM `order_return` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered' AND runsheet_number ='NONE'";
						String sql2="SELECT * FROM `order_return` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered' AND runsheet_number =?";
						List<Return> orders=getJdbcTemplate().query(sql2,new Object[] { DeliveryBoyName,dateFormat.format(dbassignedDate),runSheetNumber },new OrderReturnRowMapper());

						System.out.println("size of list in return is" +orders.size());
						return orders;
					
		}

		public void updateAssignedToReturns2(List<Return> returnOrders,
				String assignedTo, String runSheetNumber) {
			System.out.println("Inside  returnDaoImpl updateAssignedToReturns2 values are : "+assignedTo+": "+runSheetNumber);
			//Added on 05092015
			
			System.out.println("size of list inside update AssignedToReturns" +returnOrders.size());
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dbAssignedDate = new Date();
			System.out.println(dateFormat.format(dbAssignedDate));
			
			String sql="update `order_return` set runsheet_number=?, assigned_to=?, order_status = ?, location=?, attempts=?, delivery_boy_assign_date=? where tracking_id=?";
			String sql1="select attempts from `order_return` where tracking_id=?";
			String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";
			/*returnOrders.remove(0);
			System.out.println("size of retturnOrdeers after removing 1st elememt is" +returnOrders.size());*/
			Return ord=returnOrders.get(0);
			if(ord.getTrackingId()==null || ord.getTrackingId().trim().equals("")){
				
				System.out.println("inside if statement");
				returnOrders.remove(0);
			
			}
			
			for(Return ret: returnOrders){
				
				System.out.println(" inside for loop tracking Id is" +ret.getTrackingId());
				
				
				int attempts=getJdbcTemplate().queryForInt(sql1, ret.getTrackingId());
				int numAttempts=0;
				if(attempts>0){
					numAttempts=attempts+1;
				} else{
					numAttempts=1;
				}				
				getJdbcTemplate().update(sql,runSheetNumber, assignedTo, "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), ret.getTrackingId());
			System.out.println("Done");
				//for attempts
				//String sql1="select attempts from `order_return` where tracking_id=?";
			 			
				//getJdbcTemplate().update(sql,runSheetNumber, assignedTo, "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), ret.getTrackingId());
				
			
				getJdbcTemplate().update(sql2,"Assigned to "+assignedTo,ret.getStoreId(),"Out for delivery", ret.getTrackingId(), new Date());
			
			}
			
		
			
			
			//End
		
				//changing in code
				
				
				
		
		}

		public void updateReturnStatus(List<Return> orders, String status,
				String reason) {
			System.out.println("Inside ReturnDaoImpl-> updateReturnStatus");
			java.sql.Date null1 = null;
			Iterator<Return> iterator = orders.iterator();
			Date today= new Date();
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
			java.util.Date utilDate = new java.util.Date();
			null1 = new java.sql.Date(utilDate.getTime());
			while (iterator.hasNext()) {
				Return obj = iterator.next();
				if (obj.getTrackingId() == null) {

				} else {
					if(status.equalsIgnoreCase("Delivered") || status.equalsIgnoreCase("returned")) {
						
						System.out.println("Inside updateReturnStatus-> if condition of Delivered or Returned");
						
						//obj.setDeliveredDate(null1);
						
						String sql = "update `order_return` set order_status=?,status_reason=?,delivered_date=? where tracking_id=?";
						getJdbcTemplate().update(sql, status,reason,	dateFormat.format(today), obj.getTrackingId());
					} else {
						String sql = "update `order_return` set order_status=?,status_reason=? where tracking_id=?";
						getJdbcTemplate().update(sql, status,reason, obj.getTrackingId());
					}				
				}
				String sql="insert into order_activity (tracking_id,status,activity_date,reason,activity_desc) values(?,?,?,?,?)";
				getJdbcTemplate().update(sql,obj.getTrackingId(),status,	dateFormat.format(today),reason,"status updated as " +status);
			}
			
		}

		public void updateOrderReturn(Return object) {
			System.out.println("Inside updateOrderReturn");
			String sql = "update `order_return` set retailer_id=?,order_type=?,order_category=?,order_status=?,order_details=?,"
					+ "customer_name=?,customer_mobile=?,customer_email=?,address=?,city_name=?,pincode=?,assigned_to=? where tracking_id=?";
			
			System.out.println("OrderStatus is: "+object.getStatus());
			getJdbcTemplate().update(sql,
					object.getRetailerId(), object.getOrderType(),object.getCategory(),
					object.getStatus(),object.getDetails(),
					object.getCustomerName(), object.getContactNo(),
					object.getEmailId(), object.getCustomerAddress(),
					object.getCity(), object.getPincode(),
					object.getAssignedTo(), object.getTrackingId());
		}

		public List<Return> getAllReturnDispatchedOrdersWithRefNo(
				HttpSession session, CancelledOrderByData cancelledOrderByData) {
			List<Return> allCancelledOrders = new ArrayList<Return>();
			System.out.println("RetailerName: "+cancelledOrderByData.getRetailerName());
			System.out.println("Status: "+cancelledOrderByData.getStatus());
			System.out.println("From Date: "+cancelledOrderByData.getFromDate());
			System.out.println("To Date: "+cancelledOrderByData.getToDate());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			Date fromDate = cancelledOrderByData.getFromDate();
			GregorianCalendar calFromDate = new GregorianCalendar();
			calFromDate.setTime(fromDate);
			
			Date toDate = cancelledOrderByData.getToDate();
			GregorianCalendar calToDate = new GregorianCalendar();
			calToDate.setTime(toDate);		
			
			try {			
				
				User user = (User)session.getAttribute("user");
				int id = (int)retailerDao.getRetailerIdByName(cancelledOrderByData.getRetailerName());
				
				if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
					String sql="select * from `order_return` WHERE order_status LIKE TRIM('Return Order Dispatched')  AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					System.out.println("Sql Statement: "+sql);
					allCancelledOrders = getJdbcTemplate().query(sql, new OrderReturnRowMapper());
					return allCancelledOrders;	
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("Store id is" +userId);
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("Store size is" +stores.size());
					
					for(Store str :stores) {					
							double storeId=str.getStoreId();
							String sql1="select * from `order_return` where store_id="+storeId+" AND order_status LIKE TRIM('Return Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
							allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status LIKE TRIM('Return Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status LIKE TRIM('Return Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders= getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {
					double userId=user.getUserId();
					System.out.println("store id is" +userId);
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order_return` where store_id="+storeId+" AND order_status LIKE TRIM('Return Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					}
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
					double storeId=(Double)session.getAttribute("storeId");
					String sql1="select *from `order_return` where store_id="+storeId+" AND order_status LIKE TRIM('Return Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderReturnRowMapper());
					return allCancelledOrders;
				} else {
					return null;
				}			
			} catch(Exception e) {
				System.err.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			return allCancelledOrders;
		}

		public List<Return> getOrdersBasedOnSearch1(SearchReturn searchReturn) {

			 String sqlWithStoreName="SELECT *,b.store_name FROM `order_return` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE";
             
             String sql="SELECT *,b.store_name FROM `order_return` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE ";
             String sql2="SELECT *,b.store_name FROM `order_return` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE ";
             
             if(searchReturn.getAssignedStoreName() != null && !searchReturn.getAssignedStoreName().isEmpty()){
                 
                 System.out.println("assign store name is "+searchReturn.getAssignedStoreName());
                 String mySql = "SELECT store_id FROM store  WHERE store_name = ?";
                 String myStoreId = (String)getJdbcTemplate().queryForObject(
                         mySql, new Object[] { searchReturn.getAssignedStoreName() }, String.class);
                 System.out.println("store id returned is "+myStoreId);
                 
                 sql+="a.store_id="+'"' +myStoreId+'"';
                 
             }
             if(searchReturn.getCustomerContact()!=null && !searchReturn.getCustomerContact().isEmpty()){
         
                 if(sql==sql2){
                     sql+="customer_mobile="+'"' +searchReturn.getCustomerContact()+'"' ;
                     System.out.println("sql : "+sql);
                 }
                 else{
                     sql+="AND customer_mobile="+'"' +searchReturn.getCustomerContact()+'"' ;
                 }
                 
             }
             
             if(searchReturn.getDeliveredFromDate()!=null&&searchReturn.getDeliveredToDate()!=null ){
                 
                 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
                 String dateFrom=dateFormat.format(searchReturn.getDeliveredFromDate());
                 System.out.println("after converting from date"+dateFrom);
                 
                 
                 String dateTo=dateFormat.format(searchReturn.getDeliveredToDate());
                 System.out.println("after converting to date"+dateTo);
         
                 if(sql==sql2){
                     sql+="delivered_date>="+'"' +dateFrom+'"' +"and delivered_date<="+'"' +dateTo+'"' ;
                 }
                 else{
                     sql+="AND delivered_date>="+'"' +dateFrom+'"' +"and delivered_date<="+'"' +dateTo+'"' ;
                 }
                 
             }
             
         
             if(searchReturn.getLocation()!=null && !searchReturn.getLocation().isEmpty()){
                 
             
                 if(sql==sql2){
                     sql+="location="+'"' +searchReturn.getLocation()+'"' ;
                 }
                 else{
                     sql+="AND location="+'"' +searchReturn.getLocation()+'"' ;
                 }
                 
             }
             
             
             if(searchReturn.getReturnId()!=null && !searchReturn.getReturnId().isEmpty()){
                 
             
                 if(sql==sql2){
                     sql+="return_id="+'"' +searchReturn.getReturnId()+'"' ;
                 }
                 else{
                     sql+="AND return_id="+'"' +searchReturn.getReturnId()+'"' ;
                 }
                 
             }
             
             if(searchReturn.getOrderStatus()!=null && !searchReturn.getOrderStatus().isEmpty()){
                 System.out.println("inside orderStatus if ");
                 
                 
                 if(sql==sql2){
                     sql+="order_status="+'"' +searchReturn.getOrderStatus()+'"' ;
                 
                     System.out.println("uery is "+sql);
                 }
                 else{
                     sql+="AND order_status="+'"' +searchReturn.getOrderStatus()+'"' ;
                     System.out.println("uery is "+sql);
                 }
                 
             }
                 
             if(searchReturn.getPaymentType()!=null && !searchReturn.getPaymentType().isEmpty()){
                 
                 if(sql==sql2){
                     sql+="payment_modes="+'"' +searchReturn.getPaymentType()+'"';
                 }
                 else{
                     sql+="AND payment_modes="+'"' +searchReturn.getPaymentType()+'"' ;
                 }
                 
             }
             
             if(searchReturn.getRetailerName()!=null && !searchReturn.getRetailerName().isEmpty()){
                 
                 
                 String mySql = "SELECT retailer_id  FROM retailer  WHERE retailer_name = ?";
                 String myRetailerId = (String)getJdbcTemplate().queryForObject(
                         mySql, new Object[] { searchReturn.getRetailerName() }, String.class);
                 
                 System.out.println("retailer Id returned is "+myRetailerId);
                 
                 if(sql==sql2){
                     sql+="retailer_id="+'"' +myRetailerId+'"' ;
                 }
                 else{
                     sql+="AND retailer_id="+'"' +myRetailerId+'"' ;
                 }
                 
             }
             
         
                 
             if(searchReturn.getFromDate()!=null&&searchReturn.getToDate()!=null){
             
                 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
                 String dateFrom=dateFormat.format(searchReturn.getFromDate());
                 System.out.println("after converting from date"+dateFrom);
                 
                 
                 String dateTo=dateFormat.format(searchReturn.getToDate());
                 System.out.println("after converting to date"+dateTo); 
                 
                 if(sql==sql2){
                     sql+="receipt_date>="+'"' +dateFrom+'"'+"and receipt_date<="+'"'+dateTo+'"' ;
                 }
                 else{
                     sql+="AND receipt_date>="+'"' +dateFrom+'"'+"and receipt_date<="+'"'+dateTo+'"' ;
                 }
                 
             }
                 
             
             String sqlReady= sql+=";";
             System.out.println("my final query is "+sqlReady);
             
             
             return getJdbcTemplate().query(sqlReady, new ReturnAdvanceSearchRowMapper());
			
		}
}
