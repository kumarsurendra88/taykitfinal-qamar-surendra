routerApp.factory('xcelHeader',function() {
	var headers;
	 
	return {
			setId:function (id) {
				//console.log("Inside setId() of updateFactory and the Id is: ");
				headers = id;
			},
			getId:function() {
				//console.log("Inside getId() of updateFactory and the Id is: ");
				return headers;
			}
	};
});

routerApp.factory('retailerFactory',function() {
	var retailer;
	 
	return {
			setRetailer:function (id) {
				//console.log("Inside setId() of updateFactory and the Id is: ");
				retailer = id;
			},
			getRetailer:function() {
				//console.log("Inside getId() of updateFactory and the Id is: ");
				return retailer;
			}
	};
});
routerApp.service('fileUpload', ['$http' ,'$location','xcelHeader' , function ($http , $location,xcelHeader ) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	 
        	xcelHeader.setId(data);
        	//console.log("hi... file uploaded successfully",data);
        	//$location.path('modal2');
        	//alert("File Loaded")
        	$location.path('/importdata/step2')
        })
        .error(function(){
        });
        
        
        
    }
}]);

routerApp.controller('myFileCtrl',  function($scope, $location,$http, fileUpload,retailerFactory){
	
	
	$http.get("./getRetailer")
	     .success(function(response){
	    	 console.log(response)
	    	 $scope.retailers=response;
	     })
  
	     
	$scope.uploadFile = function(){
	  if($scope.retailerName==null){
		  alert("Please select a Retailer..!!")
		  document.getElementById("retailer").focus();
	  }else if($scope.myFile==null){
		 
		  alert("Please select a file to upload..!!")
		  document.getElementById("uploadFile").focus();
	  
	  } else{
		  	console.log("File name is"+$scope.myFile)
	        var file = $scope.myFile;
		  	var retailer=$scope.retailerName;
		  	retailerFactory.setRetailer(retailer)
	        console.log('file is ' + retailer);
	        var uploadUrl = "./importData";
	        fileUpload.uploadFileToUrl(file, uploadUrl);
	  }
	  
    };
    
});


routerApp.controller('step2',function($scope, $location,$http, xcelHeader,fileUpload,retailerFactory){
   // console.log("inside myFileCtrl1")
     $scope.mapList=[];
     console.log(retailerFactory.getRetailer())
     var retailerId;
     $http({
 		method : "POST",
 		url : "./checkMapping" , 
 		data :retailerFactory.getRetailer()
 		}).success(function(data){
 			console.log(data)
 			retailerId=data;
 			if(data>0){
 				 console.log("Got Retailer")
 				 $http.get("./getMappingById/"+retailerId)
 				 	.success(function(response) {$scope.mapList=response});
 			 }else{
 				 console.log("Retailer not found")
 				 angular.forEach($scope.Columns, function(hdr) {
 					 //console.log("header is"+hdr)
 					 var map={
 							 targetColumn:hdr,
 							 sourceColumn:'',
 							 nullValue: ''
 						}
 					$scope.mapList.push(map)
 				})
 			 }});
     
    $scope.next2=function(){
    	 console.log("Global variable"+retailerId)
    	 if(retailerId>0){
    		 $http({
    	    		method : "POST",
    	    		url : "./updateMapping/"+retailerId , 
    	    		data :$scope.mapList
    	    		}).success(function(data){
    	    			$location.path('/importdata/step2/step3')
    	    		});
    	 }else{
    		 $http({
    	    		method : "POST",
    	    		url : "./saveMapping/"+retailerFactory.getRetailer() , 
    	    		data :$scope.mapList
    	    		}).success(function(data){
    	    			$location.path('/importdata/step2/step3')
    	    		});
    	 }
    	
    	
    	
    	/*$http.get("./getDummy")
    	
    	.success(function(response) {$location.path('/product/step2/step3')});*/
    }
    
    
	//xcelHeader.setId("abcd");
	 $scope.headers=xcelHeader.getId();
    //console.log(a)
   $scope.Columns=["Tracking Id","Order Id","Order Title","Order Details",
                   "Order Type","Order Category","Order Priority","Order Product Id","Activity Id","Revision Id","Attachment Id",
                   "Store Id","Retailer Id","Order Reference No","Customer Name","Customer Mobile","Customer email","Customer Address",
                   "City Name","Customer Pincode","Payment Type","Bay Number","Order Status","Receipt Date","Delivery Date","Assigned To",
                   "Payment Status","Payment Reference","Order Value"]
	 
	 
	 
	  
    $scope.goBack=function(){
    	 
    	$location.path('/importdata')
    }
 
    
});

routerApp.controller('step3',function($scope, $location,$http, xcelHeader,fileUpload,uiGridConstants){
	$scope.goBack=function(){
		 
    	$location.path('/importdata/step2')
    }
	
	$scope.mySelections = [];
	
	/*console.log("data fetched is" $scope.users);*/
	/*
	 $scope.gridOptions1 = {
			    enableSorting: true,
			    columnDefs: [
			      { name: 'orderId',field: 'orderId' },
			      {name: 'Title',field: 'orderTitle'},
			      {name:'Details',field:'orderDetail'},
			      {name:'CustomerName',field:'customerName'},
			      {name:'Mobile',field:'customerContactno'},
			      {name:'Address',field:'customerAddress'}
			    ]
			  };*/
	 
	 /*$http.get("./getDummy")
		
		.success(function(response) {
			
			$scope.gridOptions1.data = response;
			});
	*/
	

$scope.gridOptions1 = {
	    paginationPageSizes: [25, 50, 75],
	    enableFiltering: true,
	    paginationPageSize: 25,
	   
	    columnDefs: [
	      { name: 'Tracking Id',field:'trackingId' },
	      { name: 'Order Type' ,field:'orderType'},
	      { name: 'Order Status',field:'orderStatus' },
	      { name: 'Customer Name',field:'customerName' },
	      { name: 'Mobile',field:'customerContact' },
	      {name:'Store Id',field:'storeId'}
	      ]
	  };
$http.get("./getAllImports")

.success(function(response) {
	$scope.gridOptions1.data = response;

	
});

	/*$scope.filterOptions = {
	        filterText: ''
	      };
	
     $scope.gridUser = { 
     
    		
    		data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: false,
        
     	multiSelect: true ,
        columnDefs: [
        			{field: 'trackingId',
        				displayName: 'TrackingId',
        				enableCellEdit: false,
        				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
      
                     {
        				field: 'orderType', 
        				displayName: 'Order Type', 
        				enableCellEdit: false,
        				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        				}, 
        				{	
                          	 field:'orderStatus', 
                          	 displayName:'Status', 
                          	 enableCellEdit: false,
                          	// cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                          },
        				 
                     {
        					field:'customerName', 
        					displayName:'CustomerName', 
        					enableCellEdit: false,
        					//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {	
                    	 field:'customerContact', 
                    	 displayName:'Mobile', 
                    	 enableCellEdit: false,
                    	 //cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
                    {	
                   	 field:'customerAddress', 
                   	 displayName:'Address', 
                   	 enableCellEdit: false,
                   	 //cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                   },
                   {field: 'storeId',
       				displayName: 'Store Id',
       				enableCellEdit: false,
       				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
       			},
                    
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    }; 
    */
});


routerApp.controller('step4',function($scope, $location,$http, xcelHeader,fileUpload){
    
    var validcnt,invalidcnt;
	$scope.goBack=function(){
		 
    	$location.path('/importdata/step2/step3')
    }
    
	$scope.mySelections = [];
	
	$http.get("./getAllImports")
	
	.success(function(response) {
	 $scope.all=response.length;
	});
	
	$http.get("./getUnderProcess")
	
	.success(function(response) {$scope.gridOptions1.data = response;
	 $scope.validCnt=response.length;
	});
	
	$http.get("./getInvalid")
	
	.success(function(response) {
		$scope.gridOptions2.data = response;
		 invalidcnt=parseInt(response.length);
	});
	
	$scope.gridOptions1 = {
		    paginationPageSizes: [25, 50, 75],
		    enableFiltering: true,
		    paginationPageSize: 25,
		   
		    columnDefs: [
		      { name: 'Tracking Id',field:'trackingId' },
		      { name: 'Order Type' ,field:'orderType'},
		      { name: 'Order Status',field:'orderStatus' },
		      { name: 'Customer Name',field:'customerName' },
		      { name: 'Mobile',field:'customerContact' },
		      {name:'Store Id',field:'storeId'}
		      ]
		  };
	$scope.gridOptions2 = {
		    paginationPageSizes: [25, 50, 75],
		    enableFiltering: true,
		    paginationPageSize: 25,
		   
		    columnDefs: [
		      { name: 'Tracking Id',field:'trackingId' },
		      { name: 'Order Type' ,field:'orderType'},
		      { name: 'Order Status',field:'orderStatus' },
		      { name: 'Customer Name',field:'customerName' },
		      { name: 'Mobile',field:'customerContact' },
		      {name:'Store Id',field:'storeId'}
		      ]
		  };
	
	/*console.log("Total"+$scope.totcnt+"Valid: "+validcnt)
	
	$scope.filterOptions = {
	        filterText: ''
	      };
	//console.log("Valid: "+process.length+" Invalid: "+invalid.length );
    $scope.gridUser = { 
     
    		
    	data: 'process',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: false,
        
     	multiSelect: true ,
        columnDefs: [{field: 'trackingId',
        				displayName: 'Tracking Id',
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
        			{field: 'trackingId',
        				displayName: 'trackingId',
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
      
                     {
        				field: 'orderType', 
        				displayName: 'Order Type', 
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        				}, 
        				{	
                          	 field:'orderStatus', 
                          	 displayName:'Order Status', 
                          	 enableCellEdit: false,
                          	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                          },
        				 
                     {
        					field:'customerName', 
        					displayName:'CustomerName', 
        					enableCellEdit: false,
        					cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {	
                    	 field:'customerContact', 
                    	 displayName:'Mobile', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
                    {	
                   	 field:'customerAddress', 
                   	 displayName:'Address', 
                   	 enableCellEdit: false,
                   	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                   },
                   {field: 'storeId',
          				displayName: 'Store Id',
          				enableCellEdit: false,
          				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
          			}
                    
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };
    $scope.gridUser1 = { 
    	     
    		
        	data: 'invalid',
            enableCellSelection: false,
            enableRowSelection: true,
            showSelectionCheckbox: false,
            
         	multiSelect: true ,
            columnDefs: [{field: 'trackingId',
				displayName: 'Tracking Id',
				enableCellEdit: false,
				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			},
			{field: 'trackingId',
				displayName: 'trackingId',
				enableCellEdit: false,
				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			},

             {
				field: 'orderType', 
				displayName: 'Order Type', 
				enableCellEdit: false,
				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
				}, 
				{	
                  	 field:'orderStatus', 
                  	 displayName:'Order Status', 
                  	 enableCellEdit: false,
                  	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                  },
				 
             {
					field:'customerName', 
					displayName:'CustomerName', 
					enableCellEdit: false,
					cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
             },
             {	
            	 field:'customerContact', 
            	 displayName:'Mobile', 
            	 enableCellEdit: false,
            	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
            {	
           	 field:'customerAddress', 
           	 displayName:'Address', 
           	 enableCellEdit: false,
           	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
           },
           {field: 'storeId',
  				displayName: 'Store Id',
  				enableCellEdit: false,
  				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
  			}
            
                        
                         ],
                        
                         selectedItems: $scope.mySelections,
                         filterOptions: $scope.filterOptions
        };	
    */	$scope.redirect=function(){
    	$location.path('/imporders')
    }
    
 
    
});

routerApp.controller('step5',function($scope, $location,$http,xcelHeader,fileUpload){
    
	$scope.goBack=function(){
		 
    	$location.path('/importdata/step2/step3/step4')
    }
	$scope.mySelections = [];
	$http.get("./getUnderProcess")

	.success(function(response) {
		$scope.gridOptions1.data = response;
		$scope.validCnt = response.length;
	});
	
	$scope.gridOptions1 = {
		    paginationPageSizes: [25, 50, 75],
		    enableFiltering: true,
		    paginationPageSize: 25,
		   
		    columnDefs: [
		      { name: 'Tracking Id',field:'trackingId' },
		      { name: 'Order Type' ,field:'orderType'},
		      { name: 'Order Status',field:'orderStatus' },
		      { name: 'Customer Name',field:'customerName' },
		      { name: 'Mobile',field:'customerContact' },
		      {name:'Store Id',field:'storeId'}
		      ]
		  };
	/*console.log("data fetched is" $scope.users);*/
   /* $scope.gridUser = { 
     
    		
    		data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: true,
        
     	multiSelect: true ,
        columnDefs: [{field: 'trackingId',
			displayName: 'Tracking Id',
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
		},
		{field: 'trackingId',
			displayName: 'trackingId',
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
		},

         {
			field: 'orderType', 
			displayName: 'Order Type', 
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			}, 
			{	
              	 field:'orderStatus', 
              	 displayName:'Order Status', 
              	 enableCellEdit: false,
              	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
              },
			 
         {
				field:'customerName', 
				displayName:'CustomerName', 
				enableCellEdit: false,
				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
         },
         {	
        	 field:'customerContact', 
        	 displayName:'Mobile', 
        	 enableCellEdit: false,
        	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
        {	
       	 field:'customerAddress', 
       	 displayName:'Address', 
       	 enableCellEdit: false,
       	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
       },
       {field: 'storeId',
				displayName: 'Store Id',
				enableCellEdit: false,
				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			},
        
                   {	
                     	 field:'category', 
                     	 displayName:'Category',
                     	 width:'20%',
                     	 enableCellEdit: false,
                     	 cellTemplate: '<div class="ngCellText"><span ng-show="{{row.getProperty(col.orderDetails)}}=="null"">Not Assigned{{row.getProperty(col.orderDetails)}}</span><span ng-show="{{row.getProperty(col.orderDetails)}}!="null">Assigned{{row.getProperty(col.orderDetails)}}</span></div>'
                     }
                    				 
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };*/
    	
    	$scope.redirect=function(){
    	$location.path('/imporders')
    }
    
 
    
});


routerApp.controller('step6',function($scope, $location,$http,$state,$timeout,uiGridConstants,xcelHeader,fileUpload){	
    
	$scope.goBack=function(){
		 
    	$location.path('/importdata/step2/step3/step4/step5')
    }
	
	$http.get("./getStores")
	.success(function(response){
		$scope.stores=response;
	})
	$scope.mySelections = [];
	$scope.mySelections.length=0;
	$scope.filterOptions = {
	        filterText: ''
	      };
	
	$http.get("./getUnassignedStore")
	
	.success(function(response) {
		if(response.length>0){
			$scope.gridOptions1.data = response;
		}else{
			alert("No unassigned orders found... You can proceed to next step..")
		}
	});
	
	$scope.gridOptions1 = {
			enableRowSelection: true,
			enableSelectAll: true,
		    paginationPageSizes: [25, 50, 75],
		    enableFiltering: true,
		    paginationPageSize: 25,
		    selectedItems: [] ,
		    multiSelect:true,
		    columnDefs: [
		      { name: 'Tracking Id',field:'trackingId' },
		      { name: 'Order Type' ,field:'orderType'},
		      { name: 'Order Status',field:'orderStatus' },
		      { name: 'Customer Name',field:'customerName' },
		      { name: 'Mobile',field:'customerContact' },
		      {name:'Zipcode',field:'customerZipcode'}
		      ]
		  };
	 
    /*$scope.gridUser = { 
     
    		
    	data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: true,
        
     	multiSelect: true ,
        columnDefs: [{field: 'trackingId',
			displayName: 'Tracking Id',
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
		},
		{field: 'trackingId',
			displayName: 'trackingId',
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
		},

         {
			field: 'orderType', 
			displayName: 'Order Type', 
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			}, 
			{	
              	 field:'orderStatus', 
              	 displayName:'Order Status', 
              	 enableCellEdit: false,
              	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
              },
			 
         {
				field:'customerName', 
				displayName:'CustomerName', 
				enableCellEdit: false,
				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
         },
         {	
        	 field:'customerContact', 
        	 displayName:'Mobile', 
        	 enableCellEdit: false,
        	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
        {	
       	 field:'customerAddress', 
       	 displayName:'Address', 
       	 enableCellEdit: false,
       	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
       },
       {field: 'storeId',
				displayName: 'Store Id',
				enableCellEdit: false,
				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			},
        
                   {	
                     	 field:'customerZipcode', 
                     	 displayName:'Pincode',
                     	 width:'20%',
                     	 enableCellEdit: false,
                      cellTemplate: '<div class="ngCellText"><input type="checkbox"   ng-input=\"\" ng-model=\"\"><b>Electronics</b>&nbsp;&nbsp;<input type="checkbox"   ng-input=\"\" ng-model=\"\"><b>Apperals</b></div>'
                     }
                    
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };*/
    	
    $scope.redirect=function(){
    	$location.path('/imporders')
    }
   
    $scope.gridOptions1.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
          var msg = 'row selected ' + row.isSelected;
          var index=$scope.mySelections.indexOf(row.entity.trackingId)
          console.log(index)
          if(index>=0){
        	  $scope.mySelections.splice(index,1);
        	  index=-1;
          }else{
        	  $scope.mySelections.push(row.entity.trackingId);
        	  index=-1
          }
          
          console.log(row.entity.trackingId);
          console.log($scope.mySelections)
        });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
          var msg = 'rows changed ' + rows.length;
          console.log(msg);
        });
      };
    
 $scope.assignStore=function(){
	 console.log($scope.gridOptions1.selectedItems)
	if($scope.name==null){
		alert("Please Select a Store to be assigned")
		document.getElementById("name").focus();
	}/*else if($scope.mySelections.length==0){
		alert("Please select atleast one order to assign")
	}*/else{
		 $http({
		 		method : "POST",
		 		url : "./assignStoreByTrackingId/"+$scope.name , 
		 		data :$scope.mySelections
		 		}).success(function(response){
		 			$scope.mySelections=[]
		 			$scope.mySelections.length=0;
		 			   	$http.get("./getUnassignedStore")
		 		            .success(function(data) {
		 		            	if(data.length>0){
		 		            		 //$scope.users=[];
		 	 		                $scope.gridOptions1.data = data;
		 	 		              
		 	 		               alert("Orders Assigned..!!!")
		 		            	}else{
		 		            		alert("No orders with unassigned store found..!!! You can proceed to next step..")
		 		            		 $scope.gridOptions1.data=[];
		 		            	}
		 		            	
		 		            });                           
		 		   
		 			 
		 		})
	}
	 		
 }
 $scope.refresh = function () {
	 console.log("refresh")
	    
	
	}; 
 var getUnassigned=function(){
	 alert("Function Called")
	 
	 //$scope.$apply();
 }
    
});

routerApp.controller('step7',function($scope, $location,$http, xcelHeader,fileUpload){
    
	$scope.goBack=function(){
		 
    	$location.path('/importdata/step2/step3/step4/step5/step6')
    }
	$scope.mySelections = [];
	$http.get("./getFinal")
	
	.success(function(response) {$scope.users = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
	/*console.log("data fetched is" $scope.users);*/
    $scope.gridUser = { 
     
    		
    		data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: false,
        
     	multiSelect: true ,
        columnDefs: [{field: 'trackingId',
			displayName: 'Tracking Id',
			enableCellEdit: false,
			/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
		},
		/*{field: 'trackingId',
			displayName: 'trackingId',
			enableCellEdit: false,
			cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
		},*/

         {
			field: 'orderType', 
			displayName: 'Order Type', 
			enableCellEdit: false,
			/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
			}, 
			{	
              	 field:'orderStatus', 
              	 displayName:'Order Status', 
              	 enableCellEdit: false,
              /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
              },
			 
         {
				field:'customerName', 
				displayName:'CustomerName', 
				enableCellEdit: false,
				/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
         },
         {	
        	 field:'customerContact', 
        	 displayName:'Mobile', 
        	 enableCellEdit: false,
        	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
*/                    },
        {	
       	 field:'customerAddress', 
       	 displayName:'Address', 
       	 enableCellEdit: false,
       /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
       },
       {field: 'storeId',
				displayName: 'Store Id',
				enableCellEdit: false,
				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
			}
        
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };
    	
    	$scope.redirect=function(){
    		$http.get("./moveOrders")
    		
    		.success(function(response) {$scope.users = response;
    		$location.path('/bulkassign1')
    		});
    	
    }
    
 
    
});
routerApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);