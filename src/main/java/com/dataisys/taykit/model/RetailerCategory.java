package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class RetailerCategory {

private double retailerId;
private double retailerCategoryId;
private double productCategoryId;
private String categoryName;
private String updatedBy;
private Timestamp updatedOn;
public RetailerCategory() {
	super();
}
public RetailerCategory(double retailerId, double retailerCategoryId,
		double productCategoryId, String categoryName, String updatedBy,
		Timestamp updatedOn) {
	super();
	this.retailerId = retailerId;
	this.retailerCategoryId = retailerCategoryId;
	this.productCategoryId = productCategoryId;
	this.categoryName = categoryName;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
}
@Override
public String toString() {
	return "RetailerCategory [retailerId=" + retailerId
			+ ", retailerCategoryId=" + retailerCategoryId
			+ ", productCategoryId=" + productCategoryId + ", categoryName="
			+ categoryName + ", updatedBy=" + updatedBy + ", updatedOn="
			+ updatedOn + "]";
}
public double getRetailerId() {
	return retailerId;
}
public void setRetailerId(double retailerId) {
	this.retailerId = retailerId;
}
public double getRetailerCategoryId() {
	return retailerCategoryId;
}
public void setRetailerCategoryId(double retailerCategoryId) {
	this.retailerCategoryId = retailerCategoryId;
}
public double getProductCategoryId() {
	return productCategoryId;
}
public void setProductCategoryId(double productCategoryId) {
	this.productCategoryId = productCategoryId;
}
public String getCategoryName() {
	return categoryName;
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public String getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}


}

