package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreServices;

public interface RetailerServicesDao {
public List<RetailerServices> getServices();
public void setRetailerService(List<RetailerService> providedServices);
public List<RetailerService> getRetailerServicesById(double id);
public void updateRetailerService(List<RetailerService> providedServices,
		double id);

}
