package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class RetailerServices {
private double serviceTypeId;
private String serviceName;
private String serviceDesc;
private Double updatedBy;
private Timestamp updatedOn;
private boolean serviceStatus;
public double getServiceTypeId() {
	return serviceTypeId;
}
public void setServiceTypeId(double serviceTypeId) {
	this.serviceTypeId = serviceTypeId;
}
public String getServiceName() {
	return serviceName;
}
public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}
public RetailerServices() {
	super();
}
public RetailerServices(double serviceTypeId, String serviceName,
		String serviceDesc, Double updatedBy, Timestamp updatedOn,
		boolean serviceStatus) {
	super();
	this.serviceTypeId = serviceTypeId;
	this.serviceName = serviceName;
	this.serviceDesc = serviceDesc;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
	this.serviceStatus = serviceStatus;
}
@Override
public String toString() {
	return "RetailerServices [ServiceTypeId=" + serviceTypeId
			+ ", serviceName=" + serviceName + ", serviceDesc=" + serviceDesc
			+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
			+ ", serviceStatus=" + serviceStatus + "]";
}
public String getServiceDesc() {
	return serviceDesc;
}
public void setServiceDesc(String serviceDesc) {
	this.serviceDesc = serviceDesc;
}
public Double getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(Double updatedBy) {
	this.updatedBy = updatedBy;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}
public boolean getServiceStatus() {
	return serviceStatus;
}
public void setServiceStatus(boolean serviceStatus) {
	this.serviceStatus = serviceStatus;
}
}

