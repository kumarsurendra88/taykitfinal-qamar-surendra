package test;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class ReportTask {

	public void printMe() {
		System.out.println("Spring 3 + Quartz 1.8.6 ~");
		sendingEmail();
	}
	
	public static void main(String args[])
	{
		ReportTask task = new ReportTask();
		task.sendingEmail();
	}
	
	public void sendingEmail() {
		System.out.println("Im in sending Email");
	    final String username = "mabrar91@gmail.com";
	    final String password = "mohamed1312";

	    Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        props.put("mail.smtp.port", "587"); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true");
	    Session session = Session.getInstance(props,
	            new javax.mail.Authenticator() {
	                protected PasswordAuthentication getPasswordAuthentication() {
	                    return new PasswordAuthentication(username, password);
	                }
	            });

	    try {

	        Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress("mabrar91@gmail.com"));
	        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("kishore.h001@gmail.com"));
	       /* message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse("lokeshr@himusoftech.com"));
	        message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("gjprasad@gmail.com"));
	        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse("kishore.h001@gmail.com"));
	        message.setSubject("Vehicle Deviation Report - 23-Dec-2014");*/
	        message.setText("Please find the attached deviation report as on 20-Dec-2014. This is an automated email, please dont reply to this email.");
	        
	        MimeBodyPart bodyPart = new MimeBodyPart(); // code for setting text with body
	        String body = "test";	        
	        bodyPart.setContent(body, "text/html");
	        
	       /* MimeBodyPart messageBodyPart = new MimeBodyPart();//code for setting file with body
	        messageBodyPart = new MimeBodyPart();
	        String file = "C:\\Users\\Dataisys\\Documents\\DeviationReport.xls";
	        String fileName = "DeviationReport23122014.xls";
	        DataSource source = new FileDataSource(file);
	        messageBodyPart.setDataHandler(new DataHandler(source));
	        messageBodyPart.setFileName(fileName);
	        
	        
	        Multipart multipart = new MimeMultipart();//code for sending both text and file with body
	        multipart.addBodyPart(messageBodyPart);
	        multipart.addBodyPart(bodyPart);

	        message.setContent(multipart);*///setting complete content and ready to send entire mail

	        System.out.println("Sending");
	        Transport.send(message);
	        Thread.sleep(6000);
	        System.out.println("Done");

	    } catch (MessagingException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	    	System.err.println("Error: "+e.getMessage());
	    }
	}	
}
