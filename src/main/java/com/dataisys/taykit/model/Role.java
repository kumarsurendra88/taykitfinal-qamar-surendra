package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Role {


private double roleId;
private String roleName;
private String roleDesc;
private String roleStatus;
private String updatedBy;
private Timestamp 	updatedOn;

public Role(){}

public Role(double roleId, String roleName, String roleDesc, String roleStatus,
		String updatedBy, Timestamp updatedOn) {
	super();
	this.roleId = roleId;
	this.roleName = roleName;
	this.roleDesc = roleDesc;
	this.roleStatus = roleStatus;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
}

public double getRoleId() {
	return roleId;
}

public void setRoleId(double roleId) {
	this.roleId = roleId;
}

public String getRoleName() {
	return roleName;
}

public void setRoleName(String roleName) {
	this.roleName = roleName;
}

public String getRoleDesc() {
	return roleDesc;
}

public void setRoleDesc(String roleDesc) {
	this.roleDesc = roleDesc;
}

public String getRoleStatus() {
	return roleStatus;
}

public void setRoleStatus(String roleStatus) {
	this.roleStatus = roleStatus;
}

public String getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}

public Timestamp getUpdatedOn() {
	return updatedOn;
}

public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}



}
