package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.ReturnDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.ExcelUtil;
import com.dataisys.taykit.model.Return;
@RestController
public class Repair {
	
	@Autowired
	ReturnDao returnDao;
	@Autowired
	OrderDao orderDao;
	@Autowired
	UserDao userDao;
	@Autowired
	RegistrationKeyDao regKeyDao;
	
	@Autowired
	PudoStoreDao storeDao;
	
	@RequestMapping(value="/uploadReturn1", method =RequestMethod.POST )
	public @ResponseBody ModelAndView uploadFile( UploadForm uploadItem, BindingResult result) throws IOException
	
	
	{
		
		 System.out.println("inside repair");
		
		System.out.println("checking for upload item" );
		if(uploadItem == null) {
			System.out.println("file is null");
		} else {
			System.out.println("file is not null");
		}
		
		
		
		
		
		System.out.println("Inside Order controller");
	System.out.println(uploadItem.getFile().getOriginalFilename());
		
	System.out.println(	"upload file"+uploadItem.getFile());
		InputStream is = uploadItem.getFile().getInputStream();
		
		HSSFWorkbook workbook = new HSSFWorkbook(is);
		HSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		
		
		boolean isHeader = true;
		try {
				
			java.sql.Date sqlDate=null;
			java.sql.Date sqlDate1=null;
			java.sql.Date null1=null;
			java.sql.Date null2=null;
			while(rowIterator.hasNext()){			
				Row row = rowIterator.next();
				if(!isHeader) 
				
				{
					System.out.println("last cell value"+row.getLastCellNum());
					if(row.getLastCellNum()>13)
					
					{
						
						System.out.println("inside myntra upload");
						System.out.println("inside if is header :-)");
						Cell trackingId=row.getCell(0);
						System.out.println("trackingId" +trackingId);
						
						Cell courierService=row.getCell(1);
						System.out.println("courierService" +courierService);
						
						Cell returnId = row.getCell(2);
						
						System.out.println("returnId" +returnId);
						
						Cell orderId = row.getCell(3);
						System.out.println("orderId" +orderId);
						Cell consigneeName = row.getCell(4);
						System.out.println("consigneeName" +consigneeName);
						Cell city = row.getCell(5);
						System.out.println("city" +city);
						Cell state = row.getCell(6);
						System.out.println("state" +state);
						
						Cell country = row.getCell(7);
						System.out.println("country" +country);
						Cell address = row.getCell(8);
						System.out.println("address" +address);
				
									
						Cell pinCode = row.getCell(9);
						System.out.println("pinCode" +pinCode);
					
						Cell phone=row.getCell(10);
						System.out.println("phone" +phone);
						Cell mobile=row.getCell(11);
					
						Cell paymentMode = row.getCell(12);
						Cell packageAmount = row.getCell(13);
						Cell productToBeShipped=row.getCell(14);
					
					
					try {
						
						
						
						Return returnOrder = new Return();
						
						/*if(trackingId != null){
							returnOrder.setTrackingId(ExcelUtil.getStringCellData(trackingId));
						}
							long d=ExcelUtil.getNumbericCellData(customerContactno);
							System.out.println("value in number format is"+d);
						
						if(courierService !=null){
							
							returnOrder.setCourierService(ExcelUtil.getStringCellData(courierService));
						}*/
						if(returnId!=null){
							
							returnOrder.setReturnId(ExcelUtil.getStringCellData(returnId));;
							BigDecimal bd = new BigDecimal(returnOrder.getReturnId());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							if(newContactNo.contains("+"))
							{
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								returnOrder.setReturnId(newContactForFail);
							}
							
							else{
								returnOrder.setReturnId(newContactNo);
							}
						}
						
					/*	if(orderId!=null){
							
							returnOrder.setOrderId(ExcelUtil.getStringCellData(orderId));
							
							BigDecimal bd = new BigDecimal(returnOrder.getOrderId());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							if(newContactNo.contains("+"))
							{
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								returnOrder.setOrderId(newContactForFail);
							
							
						}
							else{
								returnOrder.setOrderId(newContactNo);
							}
						}
						if(consigneeName!=null){
							
							returnOrder.setConsigneeName(ExcelUtil.getStringCellData(consigneeName));
							System.out.println("consigne name"+returnOrder.getConsigneeName());
						}
						
						if(city!=null){
							
							returnOrder.setCity(ExcelUtil.getStringCellData(city));
						}
						
						if(state !=null){
							
							returnOrder.setState(ExcelUtil.getStringCellData(state));
						}
						if(country!=null){
							returnOrder.setCountry(ExcelUtil.getStringCellData(country));
						}*/
						if(address!=null){
						
							returnOrder.setCustomerAddress(ExcelUtil.getStringCellData(address));
							
							System.out.println("customer address is" +returnOrder.getCustomerAddress());
						}
						
					
							
						
						
						//orderDao.createDummyOrder(order);
						returnDao.makeReturnEntry1(returnOrder);
						System.out.println("aftermaking call");
						}
						catch(Exception e){
							
							System.out.println("Error while creating myntra data");
							e.printStackTrace();
						}
					}
					else
					{
						System.out.println("inside myntra zivameUpload");
					Cell returnId=row.getCell(0);
					System.out.println("returnId" +returnId);
					
					Cell customerName=row.getCell(1);
					
					Cell customerCity = row.getCell(2);
					
					System.out.println("city is" +customerCity);
					
					Cell state = row.getCell(3);
					
					Cell address = row.getCell(4);
					Cell pinCode = row.getCell(5);
					Cell customerMobiel = row.getCell(6);
					
					Cell orderType = row.getCell(7);
					
					Cell orderValue = row.getCell(8);
				
					
					
					
					
					Cell orderDetail = row.getCell(9);
					
					Cell quantity=row.getCell(10);
					Cell weight=row.getCell(11);
					Cell retailerId= row.getCell(12);
					
				
				try {
					
					Return returnOrder = new Return();
					
					//generate TrackingId and OrderId For zivame Data
					
					
				
					
						
					
					if(returnId!=null){
						
						returnOrder.setReturnId(ExcelUtil.getStringCellData(returnId));;
						BigDecimal bd = new BigDecimal(returnOrder.getReturnId());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						if(newContactNo.contains("+"))
						{
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							returnOrder.setReturnId(newContactForFail);
						}
						
						else{
							returnOrder.setReturnId(newContactNo);
						}
					}
					
					
					if(customerName!=null){
						
						returnOrder.setConsigneeName(ExcelUtil.getStringCellData(customerName));
						System.out.println("consigne name"+returnOrder.getConsigneeName());
					}
					
					if(customerCity!=null){
						
						returnOrder.setCity(ExcelUtil.getStringCellData(customerCity));
					}
					
					if(state !=null){
						
						returnOrder.setState(ExcelUtil.getStringCellData(state));
					}
				
					if(address!=null){
						//returnOrder.setExcelAddress(ExcelUtil.getStringCellData(address));
						returnOrder.setCustomerAddress(ExcelUtil.getStringCellData(address));
						
					}
					
					if(pinCode!=null){
						returnOrder.setPincode(ExcelUtil.getNumbericCellData(pinCode));
					}
					if(customerMobiel!=null){
						
						returnOrder.setPhone(ExcelUtil.getStringCellData(customerMobiel));
						
						BigDecimal bd = new BigDecimal(returnOrder.getPhone());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						
						if(newContactNo.contains("+"))
						{
							//failCountNumber++;
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							
							returnOrder.setPhone(newContactForFail);
							
							
						}
						else{
						//System.out.println("Contact number after removing 0 is"+newContactNo);
						returnOrder.setPhone(newContactNo);;
						}
						
					}
					
					if(customerMobiel!=null){
						
						returnOrder.setContactNo(ExcelUtil.getStringCellData(customerMobiel));
						
						BigDecimal bd = new BigDecimal(returnOrder.getContactNo());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						
						if(newContactNo.contains("+"))
						{
							//failCountNumber++;
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							
							returnOrder.setContactNo(newContactForFail);
							
							
						}
						else{
						//System.out.println("Contact number after removing 0 is"+newContactNo);
						returnOrder.setContactNo(newContactNo);;
						}
					}
						
					if(orderType!=null){
								
						returnOrder.setPaymentMode(ExcelUtil.getStringCellData(orderType));
					}
					
					if(orderValue!=null){
						
						returnOrder.setOrderValue(ExcelUtil.getNumbericCellData(orderValue));
					}
					if(orderDetail!=null){
						
						returnOrder.setProductToBeShipped(ExcelUtil.getStringCellData(orderDetail));
					}
					
					if(weight!=null){
						
						returnOrder.setWeight(ExcelUtil.getNumbericCellData(weight));
					}
						
					if(retailerId!=null){
						
						returnOrder.setRetailerId(ExcelUtil.getNumbericCellData(retailerId));
					}
						
						
					
					
					//orderDao.createDummyOrder(order);
					returnDao.makeReturnEntry1(returnOrder);
					System.out.println("aftermaking call");
					
					
					
				}
		
				catch(Exception ex) {
						ex.printStackTrace();
					}
		
					}
			}
				isHeader = false;
				
				}
		}
		catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
	}
		is.close();
		return new ModelAndView("customer.html");
		
	}
	

}
