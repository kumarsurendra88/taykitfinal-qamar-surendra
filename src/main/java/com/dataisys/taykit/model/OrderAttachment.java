package com.dataisys.taykit.model;

import java.sql.Blob;
import java.sql.Time;
import java.sql.Timestamp;

public class OrderAttachment {

	private double attachmentId;
	private double orderId;
	private Blob attachmentDesc;
	private String updatedBy;
	private Timestamp updatedOn;
	public OrderAttachment() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderAttachment(double attachmentId, double orderId,
			Blob attachmentDesc, String updatedBy, Timestamp updatedOn) {
		super();
		this.attachmentId = attachmentId;
		this.orderId = orderId;
		this.attachmentDesc = attachmentDesc;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	public double getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(double attachmentId) {
		this.attachmentId = attachmentId;
	}
	public double getOrderId() {
		return orderId;
	}
	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}
	public Blob getAttachmentDesc() {
		return attachmentDesc;
	}
	public void setAttachmentDesc(Blob attachmentDesc) {
		this.attachmentDesc = attachmentDesc;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Override
	public String toString() {
		return "OrderAttachment [attachmentId=" + attachmentId + ", orderId="
				+ orderId + ", attachmentDesc=" + attachmentDesc
				+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + "]";
	}
	
	
}
