package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Store;

public class ReportingStoreRowMapper implements RowMapper<Store> {

	public Store mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Store store=new Store();
		store.setReportingStore(rs.getString("reporting_store"));
		store.setName(rs.getString("store_name"));
		store.setStoreId(rs.getDouble("store_id"));
		return store;
	}

}
