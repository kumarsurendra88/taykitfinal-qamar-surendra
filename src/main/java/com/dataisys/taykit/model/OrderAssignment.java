package com.dataisys.taykit.model;

import java.util.ArrayList;


public class OrderAssignment {
	
	private ArrayList<String> orderTrackingIds;
	private String deliveryBoyId;
	private String storeId;
	private String toDeliveryBoyNote;
	private String toStoreIdNote;
	
	public ArrayList<String> getOrderTrackingIds() {
		return orderTrackingIds;
	}
	public void setOrderTrackingIds(ArrayList<String> orderTrackingIds) {
		this.orderTrackingIds = orderTrackingIds;
	}
	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}
	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getToDeliveryBoyNote() {
		return toDeliveryBoyNote;
	}
	public void setToDeliveryBoyNote(String toDeliveryBoyNote) {
		this.toDeliveryBoyNote = toDeliveryBoyNote;
	}
	public String getToStoreIdNote() {
		return toStoreIdNote;
	}
	public void setToStoreIdNote(String toStoreIdNote) {
		this.toStoreIdNote = toStoreIdNote;
	}	
}
