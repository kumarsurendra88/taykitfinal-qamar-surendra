package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class DataMapping {

	private double mappingId;
	private double retailerId;
	private String sourceColumn;
	private String targetColumn;
	private String type;
	private String nullValue;
	private String description;
	private String updatedBy;
	private Timestamp updatedOn;
	public DataMapping() {
		super();
	}
	 
	public DataMapping(double mappingId, double retailerId,
			String sourceColumn, String targetColumn, String type,
			String nullValue, String description, String updatedBy,
			Timestamp updatedOn) {
		super();
		this.mappingId = mappingId;
		this.retailerId = retailerId;
		this.sourceColumn = sourceColumn;
		this.targetColumn = targetColumn;
		this.type = type;
		this.nullValue = nullValue;
		this.description = description;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}

	public String getNullValue() {
		return nullValue;
	}

	public void setNullValue(String nullValue) {
		this.nullValue = nullValue;
	}

	public double getMappingId() {
		return mappingId;
	}
	public void setMappingId(double mappingId) {
		this.mappingId = mappingId;
	}
	public double getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(double retailerId) {
		this.retailerId = retailerId;
	}
	public String getSourceColumn() {
		return sourceColumn;
	}
	public void setSourceColumn(String sourceColumn) {
		this.sourceColumn = sourceColumn;
	}
	public String getTargetColumn() {
		return targetColumn;
	}
	public void setTargetColumn(String targetColumn) {
		this.targetColumn = targetColumn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "DataMapping [mappingId=" + mappingId + ", retailerId="
				+ retailerId + ", sourceColumn=" + sourceColumn
				+ ", targetColumn=" + targetColumn + ", type=" + type
				+ ", nullValue=" + nullValue + ", description=" + description
				+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + "]";
	}
	 
	
	
}
