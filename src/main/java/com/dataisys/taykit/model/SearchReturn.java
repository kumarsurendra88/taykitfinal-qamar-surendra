package com.dataisys.taykit.model;

import java.util.Date;

public class SearchReturn {

	
	private String orderStatus;	
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getDeliveredFromDate() {
		return deliveredFromDate;
	}
	public void setDeliveredFromDate(Date deliveredFromDate) {
		this.deliveredFromDate = deliveredFromDate;
	}
	public Date getDeliveredToDate() {
		return deliveredToDate;
	}
	public void setDeliveredToDate(Date deliveredToDate) {
		this.deliveredToDate = deliveredToDate;
	}
	public String getReturnId() {
		return returnId;
	}
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	public String getAssignedStoreName() {
		return assignedStoreName;
	}
	public void setAssignedStoreName(String assignedStoreName) {
		this.assignedStoreName = assignedStoreName;
	}
	
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	private String paymentType;
	private Date fromDate;
	private Date toDate;
	private String retailerName;
	private String location;
	private Date deliveredFromDate;
	private Date deliveredToDate;	
	private String returnId;
	private String customerContact;
	private String assignedStoreName;
	private String assignedTo;
}
