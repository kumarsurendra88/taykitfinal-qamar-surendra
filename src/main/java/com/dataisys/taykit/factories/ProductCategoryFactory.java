package com.dataisys.taykit.factories;

import java.util.ArrayList;
import java.util.List;

import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.ProductCategories;

public class ProductCategoryFactory {
private static ProductCategoryFactory factory;
private List<ProductCategories> categoryServices;

	
	public static ProductCategoryFactory getFactory() {
		if (factory==null){
			factory=new ProductCategoryFactory();
		   return factory;
		}
             return factory;
   } 


public static void setFactory(ProductCategoryFactory factory) {
	ProductCategoryFactory.factory = factory;
}


public List<ProductCategories> getServices() {
	return categoryServices;
}


public void setServices(List <ProductCategories> services) {
	this.categoryServices=services;
}


public ProductCategoryFactory() {
	super();
}

}
