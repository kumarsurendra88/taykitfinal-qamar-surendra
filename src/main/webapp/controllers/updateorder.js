routerApp.factory('updateFactory',function() {
	var oderId=null;
	//var orderTitle;
	return {
			setId:function (id) {
				console.log("Inside setId() of updateFactory and the Id is: "+id);
				oderId = id;
			},
			getId:function() {
				console.log("Inside getId() of updateFactory and the Id is: "+oderId);
				return oderId;
			}
	};
});


routerApp.service('fileUpload1', ['$http' ,'$location','xcelHeader' , function ($http , $location,xcelHeader ) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        
        fd.append('file', file);
        console.log(fd)
        
        
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	 
        	 alert("Attachement uploaded Successfully..")
        	
        })
        .error(function(){
        });
        
        
        
    }
}]);


routerApp.controller("updateOrderController",['$scope','$http','$location','updateFactory',function($scope,$http,$location,updateFactory) {
	console.log("Inside updateOrderController");
	
	$scope.mySelections = [];
	$http.get("./getOrder")
	
	.success(function(response) 
			{$scope.names = response;});
	 $scope.filterOptions = {
		        filterText: ''
		      };  
	
	
	
	$scope.gridOptions = { 
	        data: 'names',
	        
	       
	        enableCellSelection: true,
	        enableRowSelection: true,
	        showSelectionCheckbox: true,
	        enableCellEdit: true,
	        
	        columnDefs: [
	                     {
	                    	 field: 'orderId', 
	                    	 displayName: 'OrderId', 
	                    	 enableCellEdit: false,
	                    	 cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>' 
	                     
	                    },
	                    {field: 'trackingId', displayName: 'trackingId', enableCellEdit: false, cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>'},
	                     
	                     {field:'customerName', displayName:'CustomerName', enableCellEdit: false},
	                     {field:'customerContactno', displayName:'ContactNo', enableCellEdit: false},
	                     {field:'customerEmailId', displayName:'Email', enableCellEdit: false},
	                     {field:'orderStatus', displayName:'orderStatus', enableCellEdit: false},
	                     {field:'paymentType', displayName:'paymentType', enableCellEdit: false},
	                     {field:'reciptDate', displayName:'reciptDate', enableCellEdit: false},
	                     {field:'deliveredDate', displayName:'DeliveredDate', enableCellEdit: false},
	                     {field:'retailerId', displayName:'retailerId', enableCellEdit: false},
	                     {field:'outletId', displayName:'outletId', enableCellEdit: false},
	                     {field:'orderType', displayName:'Order Type', enableCellEdit: false},
	                     {field:'value', displayName:'value', enableCellEdit: false},
	                     {field:'assignTo', displayName:'assignTo', enableCellEdit: false}
	                     ],
	                     selectedItems: $scope.mySelections,
	                     filterOptions: $scope.filterOptions
	                     
	                    
	    };
			   /* onRegisterApi: function( gridApi ) {
			      $scope.grid1Api = gridApi;*/
	
	 $scope.filterName = function() {
		    var filterText1 = 'orderStatus:' + $scope.orderFilter;
		    console.log("hiii"+filterText1);
		    var filterText2='assignTo:' +$scope.userIdFilter;
		    console.log("hii" +filterText2);
		    var filterText=filterText1 + ';'+ filterText2;
		    console.log("hii" +filterText)
		    if (filterText !== 'name:') {
		      console.log("hi inside if")
		      $scope.filterOptions.filterText = filterText;
		    } else {
		      
		      console.log("inside else")
		      $scope.filterOptions.filterText = '';
		      
		    }
		    
		  };
		  $scope.reset = function() { 
			  var inputs = document.getElementsByTagName('input');
			  var select=document.getElementsByTagName('select');
			    for (var i = 0; i<inputs.length; i++) {
			        switch (inputs[i].type) {
			            case 'hidden':
			            case 'text':
			                inputs[i].value = '';
			               
			        }
			    }
			   
                for (var i = 0; i<select.length; i++)
                	select[i].selectedIndex = 0;
		  }

	 $scope.loadById = function(row) { 
					 console.log("Inside edit()");
					/* var ordId=row.entity.orderId;*/
					 var ordId=row.entity.trackingId;
					 console.log(ordId);
					 updateFactory.setId(ordId);
					 
				 $location.path('updateorder');
				 };
		
				 $scope.exportData = function () {
				    	alasql('SELECT * INTO XLSX("ExportedData.xlsx",{headers:true}) FROM ?',[$scope.mySelections]);
				    };

	              
}]);

routerApp.controller("productsDetailsController",['$scope','$http','updateFactory',function($scope,$http,updateFactory) {
$scope.gridUser = { 
     
    		
    		//data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: false,
        
     	multiSelect: true ,
        columnDefs: [{field: '',
        				displayName: 'Sl No.',
        				enableCellEdit: false,
        				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
        			{field: '',
        				displayName: 'Product Code',
        				enableCellEdit: false,
        				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
      
                     {
        				field: '', 
        				displayName: 'Description', 
        				enableCellEdit: false,
        				//cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        				}, 
        				{	
                          	 field:'', 
                          	 displayName:'Qty', 
                          	 enableCellEdit: false,
                          	// cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                          },
        				 
                     
                    
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    }; 
    
}]);

routerApp.controller("Distributor3Controller",['$scope','$http','$location','updateFactory',function($scope,$http,$location,updateFactory) {
	$scope.user = {};
	 
	$scope.payment='cash';
	 
	 
	console.log("Inside Distributor3Controller");
	var d=updateFactory.getId();
	console.log("OrderId: "+d);	 
	
	$scope.settlePayment=function(){
		if($scope.payment=='cash'){
			$http({
		 		method : "POST",
		 		url : "./makeDelivery/+d" , 
		 		data :$scope.cash
		 		}).success(function(data){
		 			alert("Order Updated Successfully..!!!")
		 			 $location.path('bulkassign');
		 		});
			
		}else if($scope.payment==	'card'){
			console.log("Card Payent Details"+$scope.card)
			$http({
		 		method : "POST",
		 		url : "./makeDelivery/+d" , 
		 		data :$scope.cash
		 		}).success(function(data){
		 			alert("Order Updated Successfully..!!!")
		 			 $location.path('bulkassign');
		 		});
		}
		
	} 
		
	/*var dataObj = {"OrderId":d};
		console.log("dataObj format" +dataObj);
		  var data1= JSON.stringify(dataObj);
		  $scope.dis=dataObj;*/
	/*	$http({
	        url: './getOrderById',
	        method: "get",
	       params:{orderId:d},
	        headers: {'Content-Type': 'application/json'}
	      })*/
		
	

		var res = $http.get('./getOrderById/'+d)
		
		.success(function(data, status, headers, config) {
			$scope.dis = data;
			$scope.card={amount:data.orderValue}
			$scope.cash={amount:data.orderValue}
			console.log("I'm inside success method of $http.post");
			 
		});
		
		$scope.updateOrder=function(dis){
			console.log(dis);
			$http({
		 		method : "POST",
		 		url : "./updateOrder" , 
		 		data :dis
		 		}).success(function(data){
		 			alert("Order Updated Successfully..!!!")
		 			 $location.path('bulkassign');
		 		});
		}
	
}]);orderCommentController

function orderCommentController($scope,$http,$state,updateFactory){
	 
	var id=updateFactory.getId();
	
	//Getting Comments
	$http({
 		method : "POST",
 		url : "./getComments/"+id , 
 		data :$scope.comment
 		}).success(function(data){
 			console.log(data)
 			$scope.comments=data
 			var obj=data[0];
 			 
 			 console.log(obj.updatedOn)
 			var newDate = (new Date(obj.updatedOn)).toLocaleString();
 			//newDate.setTime();
 			$scope.lastUpdated = newDate;
 			
 			 
 		});
	
	//Post a new comment
	$scope.addComment=function(comment){
		console.log(comment)
		$http({
	 		method : "POST",
	 		url : "./addComment/"+id , 
	 		data :$scope.comment
	 		}).success(function(data){
	 			alert("Comment was posted successfully...!!!")
	 			 //$location.path('bulkassign');
	 			$scope.comment="";
	 			$state.reload();
	 		});
	}
}

function attachmentController($scope,$http,updateFactory,fileUpload1) {
	var id=updateFactory.getId();
	 
	$scope.addAttachment=function(){
		if($scope.attachment==null){
			alert("Please select a file to upload..!!!")
		}else{
			var file=$scope.attachment;
			console.log(file)
			 var uploadUrl = "./addAttachment/"+id;
			 fileUpload1.uploadFileToUrl(file, uploadUrl);
			 $scope.attachment="";
		}
		
		/*$http({
	 		method : "POST",
	 		url : "./addAttachment/"+id , 
	 		data :fd
	 		}).success(function(data){
	 			alert("Comment was posted successfully...!!!")
	 			 //$location.path('bulkassign');
	 			$scope.comment="";
	 			$state.reload();
	 		});*/
	};
}
	
routerApp.factory('inwardEntry', function($resource) {
	"use strict";
	return $resource('./InwardEntry', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});

function inwardEntryController($scope,$http,inwardEntry) {
	$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
	
	 $http.get("./getLoggedStore")
	 		.success(function(response){
	 			//$scope.mydata=response;
	 			//console.log($scope.mydata);
	 			//alert("response.FinalName"+mydata.finalName);
	 			
	 			$scope.inward={storeName:response.name};
	 		})
    $scope.save=function(){
		 console.log("inside save : "+$scope.inward)
    	inwardEntry.save($scope.inward,function(){
    		console.log($scope.inward)
    		alert("Updated..!!");
    		$scope.inward.trackingId="";
    		/*$http.get("./getLoggedStore")
	 		.success(function(response){
	 			$scope.inward={storeName:response.name};
	 		})*/
    	})
    }

}




function parceltransfer($scope,$http,inwardEntry) {
//$scope.outward={};
	var loggedStoreId=null;
	 $http.get("./getLoggedStore")
		.success(function(response){
			console.log(response)
			loggedStoreId=response.storeId;
			$scope.outward={fromLocation:response.name};
		})
		$http.get("./getStores")
		.success(function(response){
			
			$scope.stores=response;
		})
   $scope.save=function(){
	   var trackingId=$scope.outward.trackingId;
	   $http.get("./getOrderById/"+trackingId)
		
		.success(function(response) {
			 if(response.trackingId==null){
					alert("Invalid tracking Id..!!!")
				}else if(loggedStoreId==response.storeId){
					alert("Parcel belongs to this store..!! You Cannot Transfer..!!!")
				}else{
				 $http({
				 		method : "POST",
				 		url : "./transferStock", 
				 		data :$scope.outward
				 		}).success(function(data){
				 			alert("Updated...!!!")
				 			$scope.outward.trackingId=""
				 				/*$http.get("./getLoggedStore")
				 				.success(function(response){
				 					
				 					$scope.outward={fromLocation:response.name};
				 				})
				 				$http.get("./getStores")
				 				.success(function(response){
				 					
				 					$scope.stores=response;
				 				})*/
				 		});
			}
			
		});
	   
	  
   }

}


function outwardFordeliveryBoysController($scope,$http,$location) {
	$scope.outward={}; 
	$scope.errFlag=false;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = yyyy+'-'+mm+'-'+dd;
	console.log(today);
	/*$http.get("./getUnassignedOrder")
	
	.success(function(response) {
		
		$scope.trackingIds=response;
	});*/
	
	//*********************show inactive run sheets starts*******************//
	$scope.getInactiveRunsheetList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
		var Q=obj;
			alert("obj inside function"+Q);
			
			$http.get("./inactiveRunsheets/"+Q).success(function(runsheets){
				
				console.log("inside inactive runshets");

				
				$scope.listOfInactiveRunsheets=runsheets;
			console.log($scope.listOfInactiveRunsheets)
			
			})

		};
		//*********************show inactive run sheet ends*******************//
	
	/*$http.get("./inactiveRunsheets").success(function(runsheets){
		
		console.log("inside inactive runshets")

		
		$scope.listOfInactiveRunsheets=runsheets;
	console.log($scope.listOfInactiveRunsheets)
	
	});
	*/
	
	
	
	
	
	
	
	
/*$http.get("./userdetail")
	
	.success(function(response) {
		
		$scope.areaNames=response;
	});*/
	$http.get("./getDeliveryBoys")
	
	.success(function(response) {
		
		$scope.deliveryBoys=response;
	});
	
	$http.get("./getLoggedStore")
	.success(function(response){
		
		$scope.outward={fromLocation:response.name};
		console.log($scope.outward);
	})
	
	   $scope.save=function(){ var trackingId=$scope.outward.trackingId;
	   console.log("id is "+trackingId);
	   $http.get("./getOrderById/"+trackingId)
		
		.success(function(response) {
			
			
			console.log("response tracking Id" +response.trackingId);
			if(response !=null){
				console.log("orderstatus received is "+response.orderStatus+" order type is "+response.orderType);
			 var orderStatus=response.orderStatus.toUpperCase();
			 var orderType=response.orderType.toUpperCase();
			// var runsheetStatus=response.runSheetStatus.toUpperCase();
			// var assignedTo=response.assignedTo.toUpperCase();
			 var deliveryDate=new Date(response.deliveryDate);
			 var dateToday=new Date(today)
			 console.log(deliveryDate)
			 console.log(dateToday)
			 
			}
			if(response.trackingId==null){
				 
				$scope.errFlag=true;
				$scope.errorMessage = "Invalid tracking Id..!!!";
			}else if(orderStatus=="DELIVERED"){
			//	alert("Order is delivered..!! You cannot assign..!!")
				$scope.errFlag=true;
				$scope.errorMessage = "Order is delivered..!! You cannot assign..!!";
			}else if(response.orderStatus=="Order cancelled-RTO" ||response.orderStatus=="Order cancelled"){
				//alert("Order is cancelled..!! You cannot assign..!!")
				$scope.errFlag=true;
				$scope.errorMessage = "Order is delivered..!! You cannot assign..!!";
			}else if(orderType=="COLLECT"){
				//alert("Order type is collect..!! You cannot assign..!!")
				$scope.errFlag=true;
				$scope.errorMessage = "Order type is collect..!! You cannot assign..!!";
			}/*else if(runsheetStatus=="OPEN"){
				//alert("Cannot assign.!!! Runsheet status is open ");
				$scope.errFlag=true;
				$scope.errorMessage = "Cannot assign.!!! Runsheet status is open ";
			}*/else if(+dateToday<+deliveryDate){
				//alert("This order has scheduled delivery on "+response.deliveryDate)
				$scope.errFlag=true;
				$scope.errorMessage = "This order has scheduled delivery on "+response.deliveryDate;
				
			}
			
			else if(response.attempts>3){
				//	alert("Order is delivered..!! You cannot assign..!!")
					$scope.errFlag=true;
					$scope.errorMessage = " Cannot Assign Attempt count greter than 3!!";
				}
			else{
				
				$http({
	 		 		method : "POST",
			 		url : "./assignDeliverBoy", 
			 		data :$scope.outward
			 		}).success(function(data){
			 			$scope.errFlag=false
			 			alert("Updated...!!!")
			 			$scope.outward.trackingId=""
			 			
			 		});
			}
			
		});
	   }
	
	
	//**************activating runsheet starts******************//
	$scope.ActivateRunSheet=function(obj,obj1){
		 alert("inside function value : "+obj+" : "+obj1);
		 console.log("inside function value : "+obj+" : "+obj1);
		 
		$http({
		method : "POST",
		//url : "./generateRunSheetNumberWithUserName", 
		url : "./generateRunSheetNumber/"+obj+","+obj1,
		data :$scope.order
		}).success(function(data){
		alert("RunSheeet Generated...!!!")
		
		
		 $location.path('downloadfile');
		//$scope.order.trackingId="";
		});
	}
	//*******************activating runsheet ends******************//
		
	
	
	$scope.generateRunsheet=function(){
		
		$http.get("./generateRunSheet").success(function(data){
			
			alert("Generated Runsheet is" +data)
			$http.get("./inactiveRunsheets").success(function(runsheets){
				
				console.log("inside inactive runshets")

				
				$scope.listOfInactiveRunsheets=runsheets;
			console.log($scope.listOfInactiveRunsheets)
			
			})
			
			
			
		})
		
		
		
		
		
	}

	}


function updateOrderStatus($scope,$http ){
	$scope.showDate=false;
	/*$scope.statuses=[{status:'Out For Delivery'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled material at warehouse'},{status:'Order cancelled-RTO'},{status:'Door Locked'},{status:'No Response'},{status:'Customer Not reachable'},{status:'Wrong Number'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]*/
	$scope.statuses=["Delivered","At Taykit warehouse","In transit","Out For Delivery","Future Delivery","Could not attempt","Undeliverable","Order cancelled","Scheduled delivery"]
	var futureDeliveryReasons=["Door Locked","No Response","Customer Not Reachable"];
	var orderCancelledReasons=["Late","Not Required","Parcel Tampered"];
	var cudntAttemptReasons=["Vehicle breakdown","Bad Weather"];
	var undlrvbleReasons=["Wrong number","Customer not at address"];
	$scope.save=function(){
	 
	 var trakingId=$scope.order.trackingId;
	 $http.get('./getOrderById/'+trakingId)
	 .success(function(response){
		 var orderStatus=response.orderStatus.toUpperCase();
		 if(response.trackingId==null){
			 alert("Invalid Tracking Id...!!")
		 }else if(orderStatus=="DELIVERED"){
				alert("Order is delivered.. You cannot change the status..!!")
		}else{
			
			 $http({
					method : "POST",
					url : "./updateOrderStatus", 
					data :$scope.order
					}).success(function(data){
					alert("Updated...!!!")
					$scope.order.trackingId="";
					$scope.showDate=false;
					});
		 }
	 })
	
}
	
	$scope.loadReason=function(index,status){
		console.log(status)
		
		/*if(status!="Scheduled delivery"){
			console.log("etxt")
			document.getElementById("inf").innerHTML="<input type=\"text\" ng-model=\"order.reason\" placeholder=\"Reason\" class=\"form-control\" typeahead=\"ob for ob in reasons|filter:$viewValue |limitTo:4\" >"
		}else{
			console.log("date")
			document.getElementById("inf").innerHTML="<input type=\"date\" ng-model=\"order.reason\" placeholder=\"Reason\" class=\"form-control\" >"
		}*/
		if(status=="Future Delivery"){
			$scope.reasons=futureDeliveryReasons;
			$scope.showDate=false;
			}else if(status=="Undeliverable"){
			$scope.reasons=undlrvbleReasons;
			$scope.showDate=false;
			}else if(status=="Order cancelled"){
			$scope.reasons=orderCancelledReasons;
			$scope.showDate=false;
			}else if(status=="Could not attempt"){
			$scope.reasons=cudntAttemptReasons;
			$scope.showDate=false;
			}else if(status=="Scheduled delivery"){
				$scope.showDate=true;
			}else{
			$scope.reasons="";
			$scope.showDate=false;
			}
		 
	}
}


function runSheet($scope,$http,$location ){
	
	$http.get("./getDeliveryBoys")
		
		.success(function(response) {
			
			$scope.deliveryBoys=response;
		});

		//$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
		$scope.save=function(){
		 
		 
		$http({
		method : "POST",
		url : "./generateRunSheetNumberWithUserName", 
		//url : "./generateRunSheetNumber",
		data :$scope.order
		}).success(function(data){
		alert("RunSheeet Generated...!!!")
		
		
		// $location.path('downloadfile');
		//$scope.order.trackingId="";
		});
	}
	}


function returnRunSheet($scope,$http,$location ){
	
	$http.get("./getDeliveryBoys")
		
		.success(function(response) {
			
			$scope.deliveryBoys=response;
		});

		//$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
		$scope.save=function(){
		 
		 
		$http({
		method : "POST",
		//url : "./generateReturnRunSheetNumber", 
		url : "./generateReturnRunSheetNumberWithUserName",
		data :$scope.order
		}).success(function(data){
		alert("RunSheeet Generated...!!!")
		
		
		// $location.path('downloadfile');
		//$scope.order.trackingId="";
		});
	}
	}



function runSheetForStore($scope,$http,$location ){
	
	
	 $scope.OrderTypes=["Collect","Delivery"]
	
	$http.get("./getStores")
	.success(function(response){
		
		$scope.stores=response;
	})
		//$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
		$scope.save=function(){
		 
		 
		$http({
		method : "POST",
		url : "./storeRunSheetNumber", 
		data :$scope.store
		}).success(function(data){
		alert("RunSheeet Generated...!!!")
		
		
		 $location.path('listofstorerunsheets');
		//$scope.order.trackingId="";
		});
	}
	}

function runsheetNumbers($scope,$http,$location ){
	
	$http.get("./getAllFiles")
		
		.success(function(response) {
			
			$scope.deliveryBoys=response;
			
			
			console.log("file name in different variable" +$scope.deliveryBoys);
		
			
		});

		//$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
		$scope.save=function(){}
	}
function listOfStoreRunSheets($scope,$http,$location ){
	
	$http.get("./getAllFilesForStore")
		
		.success(function(response) {
			
			$scope.listOfStores=response;
			console.log($scope.listOfStores);
		});

		//$scope.statuses=[{status:'Out For Delivery'},{status:'POD Outlet'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled materail at warehouse'},{status:'Order cancelled-RTO'},{status:'Not at delivery point'},{status:'Costumer Not reachable'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]
		$scope.save=function(){}
	}


function outwardRetursTodeliveryBoys($scope,$http,$location) {
	$scope.outward={}; 
	$scope.listOfUserType1= [{id:1,name: 'Deliver boy'},{id:2,name: 'Store Executive'}];
	$scope.listOfUserType= [{id:1,name: 'Super Admin'},{id:2,name: 'Zonal Manager'},{id:3,name: 'Regional Manager'},{id:4,name: 'Hub Admin'},{id:5,name: 'Hub Executive'},{id:6,name: 'Store Admin'},{id:7,name: 'Store Executive'} ];
	$scope.listOfUserTypeforZM= [{id:1,name:'Regional Manager'},{id:2,name: 'Hub Admin'},{id:3,name: 'Hub Executive'},{id:4,name: 'Store Admin'},{id:5,name: 'Store Executive'} ];
	$scope.listOfUserTypeforRM= [{id:1,name: 'Hub Admin'},{id:2,name: 'Hub Executive'},{id:3,name: 'Store Admin'},{id:4,name: 'Store Executive'} ];
	$scope.listOfUserTypeforHA= [{id:1,name: 'Hub Executive'},{id:2,name: 'Store Admin'},{id:3,name: 'Store Executive'} ];
	$scope.listOfUserTypeforHE= [{id:1,name: 'Store Admin'},{id:2,name: 'Store Executive'}];
	$scope.listOfUserTypeforSA= [{id:1,name: 'Store Executive'},{id:2,name: 'Deliver boy'}];

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = yyyy+'-'+mm+'-'+dd;
	console.log(today);
	/*$http.get("./getUnassignedOrder")
	
	.success(function(response) {
		
		$scope.trackingIds=response;
	});*/
	
	
	//*********************show inactive return run sheet starts*******************//
	$scope.getInactiveRunsheetList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
		var Q=obj;
			alert("obj inside function"+Q);
			
			$http.get("./inactiveRunsheets/"+Q).success(function(runsheets){
				
				console.log("inside inactive runshets");

				
				$scope.listOfInactiveRunsheets=runsheets;
			console.log($scope.listOfInactiveRunsheets)
			
			})

		};
		//*********************show inactive return run sheet ends*******************//
		

		//**************activating return runsheet starts******************//
		$scope.ActivateReturnRunSheet=function(obj,obj1){
			 alert("inside function value : "+obj+" : "+obj1);
			 console.log("inside function value : "+obj+" : "+obj1);
			 
			$http({
			method : "POST",
			//url : "./generateRunSheetNumberWithUserName", 
			url : "./generateReturnRunSheetNumber/"+obj+","+obj1,
			data :$scope.order
			}).success(function(data){
			alert("RunSheeet Generated...!!!")
			
			
			 $location.path('downloadfile');
			//$scope.order.trackingId="";
			});
		}
		//*******************activating return runsheet ends******************//
	
	
	
	$http.get("./getDeliveryBoys")
	
	.success(function(response) {
		
		$scope.deliveryBoys=response;
	});
	
	$http.get("./getLoggedStore")
	.success(function(response){
		
		$scope.outward={fromLocation:response.name};
	})
	
//************ designation list*****************
	
	//$scope.listOfUserType1= ['Deliver boy','Store Executive'];
	//$scope.listOfUserType= ['Super Admin','Store Admin','Store Executive','Hub Admin','Hub Executive','Deliver boy' ];
	//$scope.outward={listOfOptions:response.area_name};
	
	$scope.getDesignationList=function(obj){
		var obj1=obj;
		// var x = Math.floor((Math.random() * 1000000) + 1);

		// x+="_"+obj1;
		//alert("Generated Account Number : "+x);
		//alert(obj1);
		$scope.outward={Desig:obj1};

	};
	
		
	
	
	//$scope.outward={listOfOptions:response.area_name};

	
	//**********************************getting zone list***********************//
	$http.get("./getZoneNames")

.success(function(response) {
	//console.log(response)
	$scope.listOfZone=response;//.area_name;
	//$scope.outward={listOfOptions:response.area_name};
	
})

//*********************show region list on zone selection starts*******************//
$scope.getRegionList=function(obj){
		//console.log("obj inside function"+obj.zone_id);
		alert("obj inside function"+obj);
		
		$http.get("./getRegionNamesByZone_Name/"+obj)

		.success(function(response) {
			//console.log(response)
			if(response==''){
				//alert("done");
			
					alert("No Region found!!Please select the other Zone");
					}
			$scope.listOfRegions=response;//.area_name;
			//$scope.outward={listOfOptions:response.area_name};
			
		})

	};
	//*********************show region list on zone selection ends*******************//
	
	//*********************show hub list on region selection starts*******************//
	$scope.getHubList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
			alert("obj inside function*************"+obj)
			
			$http.get("./getHubNamesByRegion_Name/"+obj)

			.success(function(response) {
				//console.log(response)
				if(response==''){
				//alert("done");
			
					alert("No Hubs found!!Please select the other region");
					}
				$scope.listOfHubs=response;//.area_name;
				//$scope.outward={listOfOptions:response.area_name};
				
			})

		};
		//*********************show hub list on region selection ends*******************//	
		
		//*********************show store list on hub selection starts*******************//
		$scope.getStoreList=function(obj){
				//console.log("obj inside function"+obj.zone_id);
				alert("obj inside function****"+obj);
				
				$http.get("./getStoreNamesByHub_Name/"+obj)

				.success(function(response) {
					//console.log(response)
					if(response==''){
					//alert("done");
				
						alert("No Store found!!Please select the other Hub");
						}
					$scope.listOfStores=response;//.area_name;
					alert(response)
					console.log(response)
					//$scope.outward={listOfOptions:response.area_name};
					
				})

			};
			//*********************show store list on hub selection ends*******************//	
		
		
	//$scope.listOfOptions = ['One', 'Two', 'Three'];

	$scope.AccCreation=function(obj){
		var obj1=obj.area_name;
		 var x = Math.floor((Math.random() * 1000000) + 1);

		 x+="_"+obj1;
		//alert("Generated Account Number : "+x);
		$scope.outward={AccNum:x};

	};
	
	   $scope.save=function(){
		   var trackingId=$scope.outward.trackingId;
		   
		   $http.get("./getReturnById/"+trackingId)
			
			.success(function(response) {
				 
					
					$http({
		 		 		method : "POST",
				 		url : "./assignReturnToDeliverBoy", 
				 		data :$scope.outward
				 		}).success(function(data){
				 			alert("Updated...!!!")
				 			$scope.outward.trackingId=""
				 			
				 		});
				
				
			});
		   
	};
	
	

	/*=====================Add user starts==================*/

	$scope.CreatUser=function(){
	//	alert("inside function");
		//alert($scope.region_name);
		//alert($scope.region_desc);
		//alert($scope.store_name);
			$http({
				method:"POST",
				url:"./createUser",
				data:$scope.obj
			})
			.success(function(response){
				flag=response;
				console.log(flag);
				if(flag){
					 
					//$scope.details=response;
					alert("User Added Successfully!!!");
					 $location.path('product');
					/*$scope.$apply(function() {
						  $location.path('product');
						});*/
				}
				else{
					alert("Error Occurred..!!");
				}
				//$location.path('product');
			})
			//$location.path('product');
		}

	/*=====================add user ends==================*/
	
	//===============create order API starts===================//
	$scope.CreatOrderAPI=function(){
		//	alert("inside function");
			//alert($scope.region_name);
			//alert($scope.region_desc);
			//alert($scope.store_name);
				$http({
					method:"POST",
					url:"./CreateOrderAPI",
					data:$scope.obj
				})
				.success(function(response){
					flag=response;
					console.log(flag);
					if(flag){
						 
						alert("Order Added Successfully!!!");
						 $location.path('product');
					
					}
					else{
						alert("Error Occurred..!!");
					}
			
				})
			}

	//================create order API ends==========================//
	
	

	}

routerApp.factory('TrackindIdFactory', function() {
	var data = [ {
		trackingId : 0
	} ];
	return {
		getTrackingId : function() {
			return data.trackingId;
		},
		setTrackingId : function(track) {
			data.trackingId = track;

		}
	};
});

function searchOrderByparam($scope,$http,$location,TrackindIdFactory){ 
	$scope.getOrder=function(){
		console.log($scope.search)
		var parameter=$scope.search.param;
		
		if(parameter=="TrackingId"){
			 $http.get("./searchOrderByTrackingId/"+$scope.search.srchVal)
			 .success(function(response){
				if(response.length>0){
					$scope.orders=response;
				}else{
					alert("No order found with that Tracking Id..!!!")
				}
				 
			 })
		}else if(parameter=="OrderId"){
			$http.get("./searchOrderByOrderId/"+$scope.search.srchVal)
			 .success(function(response){
				 if(response.length>0){
						$scope.orders=response;
					}else{
						alert("No order found with that Order Id..!!!")
					}
			 })
		}else if(parameter=="Customer Name"){
			$http.get("./searchOrderByCustomerName/"+$scope.search.srchVal)
			 .success(function(response){
				 if(response.length>0){
						$scope.orders=response;
					}else{
						alert("No order found with that Customer Name..!!!")
					}
			 })
		}else{
			$http.get("./searchOrderByCustomerContact/"+$scope.search.srchVal)
			 .success(function(response){
				 console.log(response)
				 if(response.length>0){
						$scope.orders=response;
					}else{
						alert("No order found with that Customer Contact..!!!")
					 
					}
			 })
		}
	}
	
	$scope.loadOrder=function(trackingId){
		TrackindIdFactory.setTrackingId(trackingId);
  		$location.path('updateorder');
	}
}


function updateReturnStatus($scope,$http ){
	$scope.showDate=false;
	/*$scope.statuses=[{status:'Out For Delivery'},{status:'Delivered'},{status:'Future Delivery'},{status:'Order cancelled material at warehouse'},{status:'Order cancelled-RTO'},{status:'Door Locked'},{status:'No Response'},{status:'Customer Not reachable'},{status:'Wrong Number'},{status:'Product at TAYKIT warehouse'},{status:'Product at POD waiting for customer pickup'}]*/
	$scope.statuses=["returned","At Taykit warehouse","In transit","Out For Delivery","Future Delivery","Could not attempt","Undeliverable","Scheduled delivery","Product Collected from Customer"]
	var futureDeliveryReasons=["Door Locked","No Response","Customer Not Reachable"];
	var orderCancelledReasons=["Late","Not Required","Parcel Tampered"];
	var cudntAttemptReasons=["Vehicle breakdown","Bad Weather"];
	var undlrvbleReasons=["Wrong number","Customer not at address"];
	$scope.save=function(){
	 
	 var trakingId=$scope.order.trackingId;
	 $http.get('./getReturnDataById/'+trakingId)
	 .success(function(response){
		// var orderStatus=response.orderStatus.toUpperCase();
		
			
			 $http({
					method : "POST",
					url : "./updateReturnStatus", 
					data :$scope.order
					}).success(function(data){
					alert("Updated...!!!")
					$scope.order.trackingId="";
					$scope.showDate=false;
					});
		 
	 })
	
}
	
	$scope.loadReason=function(index,status){
		console.log(status)
		
		/*if(status!="Scheduled delivery"){
			console.log("etxt")
			document.getElementById("inf").innerHTML="<input type=\"text\" ng-model=\"order.reason\" placeholder=\"Reason\" class=\"form-control\" typeahead=\"ob for ob in reasons|filter:$viewValue |limitTo:4\" >"
		}else{
			console.log("date")
			document.getElementById("inf").innerHTML="<input type=\"date\" ng-model=\"order.reason\" placeholder=\"Reason\" class=\"form-control\" >"
		}*/
		if(status=="Future Delivery"){
			$scope.reasons=futureDeliveryReasons;
			$scope.showDate=false;
			}else if(status=="Undeliverable"){
			$scope.reasons=undlrvbleReasons;
			$scope.showDate=false;
			}else if(status=="Order cancelled"){
			$scope.reasons=orderCancelledReasons;
			$scope.showDate=false;
			}else if(status=="Could not attempt"){
			$scope.reasons=cudntAttemptReasons;
			$scope.showDate=false;
			}else if(status=="Scheduled delivery"){
				$scope.showDate=true;
			}else{
			$scope.reasons="";
			$scope.showDate=false;
			}
		 
	}
}