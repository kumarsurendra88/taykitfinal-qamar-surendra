package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StoreServices;

public class StoreProductServiceRowMapper implements RowMapper<ProductCategories>{
	
	
	public ProductCategories mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		ProductCategories service= new ProductCategories();
		service.setCategoryDesc(rs.getString("category_desc"));
		service.setCategoryId(rs.getDouble("category_id"));
		service.setCategoryName(rs.getString("category_name"));
		service.setCategoryStatus(rs.getBoolean("category_status"));
		service.setUpdatedBy(rs.getDouble("updated_by"));
		return service;
		
	}



}
