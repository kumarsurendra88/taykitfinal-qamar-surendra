package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreServices;

public class RetailerAvailableServicesRowMapper implements RowMapper<RetailerService>{
	
	
	public RetailerService mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		RetailerService service= new RetailerService();
		service.setRetailerId(rs.getDouble("retailer_id"));
		service.setRetailerServiceId(rs.getDouble("retailer_service_id"));
		service.setServiceId(rs.getDouble("service_id"));
		service.setUdatedBy(rs.getString("updated_by"));
		return service;
		
	}



}
