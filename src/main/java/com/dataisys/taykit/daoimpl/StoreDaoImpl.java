package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.StoreDao;
import com.dataisys.taykit.model.MasterBag;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreReport;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.StoreRowMapper;

public class StoreDaoImpl extends JdbcDaoSupport implements StoreDao {
	
	
	public List<Store> getStoreNameInOrderReturn() {
		String sql="SELECT store.`store_name` FROM store INNER JOIN order_return ON store.store_id=order_return.`store_id`";
		
		final List<Store>getStoreNameInOrderReturn=new ArrayList<Store>();
		
		getJdbcTemplate().query(sql, new ResultSetExtractor<Store>(){
			
			public Store extractData(ResultSet rs)throws SQLException,DataAccessException 
			{
				System.out.println("inside ");
				
				Store rt=new Store();  
				while(rs.next()){
				
					rt.setName(rs.getString("store_name"));
					getStoreNameInOrderReturn.add(rt);
					
				
				}
				return null;
				
			}
			
		});
		return getStoreNameInOrderReturn;
	}

	public List<Store> getStoreNameOfHub(User user) {
		// TODO Auto-generated method stub
		String sqlStoreName="select store_name from store where hub_id=(select hub_id from user_hub where account_no="+"'"+user.getAccNumber()+"')";
		//String sqlStoreName="select store_name from store where hub_id=(select hub_id from user_hub where account_no='hubadmin1@taykit.in')";
		return getJdbcTemplate().query(sqlStoreName,new ResultSetExtractor<List<Store>>(){

			public List<Store> extractData(ResultSet rs) throws SQLException,
					DataAccessException {System.out.println("inside");
				List<Store> list=new ArrayList<Store>(); 
				System.out.println("inside");
			    while(rs.next()){  
			    	Store e=new Store();  
			   //  e.setActivityDate(rs.getDate("activityDate"));
			    e.setName(rs.getString("store_name"));
			    list.add(e);  
			    System.out.println("inside while");
			    }  
			    return list;
			}

			});
	}

	public String getStoreId(String storeName) {
		// TODO Auto-generated method stub
		String storeId=null;
		
		String sql="select store_id from store where store_name='"+storeName+"'";
		System.out.println("query is "+sql);
		storeId= getJdbcTemplate().queryForObject(sql, String.class); 
		return storeId;
	}

	public StoreReport getTotalInward(String storeId,String fromDate,String toDate) {
		// TODO Auto-generated method stub
		System.out.println("data collect from frontend is ");
		System.out.println("storeId is "+storeId+" fromDate is "+fromDate+" todate is "+toDate);
		
	/*	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		Date fromDate1 = cancelledOrderByData.getFromDate();*/
		/*GregorianCalendar calFromDate = new GregorianCalendar();
		calFromDate.setTime(fromDate);*/
		
		StoreReport sr=new StoreReport();
		HashMap storeReportHash=new HashMap();
		
		String count=null;
		String sqlInward="SELECT COUNT(masterbagdata.masterbag_no) FROM masterbagdata INNER JOIN masterbag ON masterbag.masterbag_no=masterbagdata.masterbag_no WHERE masterbagdata.flag='2' AND masterbag.destination="+"'"+storeId+"'"+" AND masterbag.flag='S'";
		System.out.println("query is "+sqlInward);
		count=getJdbcTemplate().queryForObject(sqlInward, String.class); 
		storeReportHash.put("totInward", count);
		//sr.setTotInward((String) storeReportHash.get("totInward"));
		String sqlCollect="SELECT COUNT(*) FROM `order` WHERE order_status='delivered' AND order_type='collect' AND store_id="+"'"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("second query is "+sqlCollect);
		String count1=getJdbcTemplate().queryForObject(sqlCollect, String.class);
		storeReportHash.put("totCollected",count1);
		//
		String sqlDelivery="SELECT COUNT(*) FROM `order` WHERE order_status='delivered' AND order_type='delivery' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("third query is "+sqlDelivery);
		String count2=getJdbcTemplate().queryForObject(sqlDelivery, String.class);
		storeReportHash.put("totDelivery",count2);
		//
		String sqlCancelled="SELECT COUNT(*) FROM `order` WHERE order_status LIKE '%cancelled%' and order_type='delivery' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("fourth query is "+sqlCancelled);
		String count3=getJdbcTemplate().queryForObject(sqlCancelled, String.class);
		storeReportHash.put("totCancelled",count3);
		//
		/*String sqlReassigned="";
		System.out.println("fifth query is "+sqlReassigned);
		String count4=getJdbcTemplate().queryForObject(sqlReassigned, String.class);
		storeReportHash.put("totReassigned",count4);*/
		//
		String sqlPicked="SELECT COUNT(*) FROM `order_return` WHERE order_status LIKE '%picked%' and order_type='pickup' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("sixth query is "+sqlPicked);
		String count5=getJdbcTemplate().queryForObject(sqlPicked, String.class);
		storeReportHash.put("totPicked", count5);
		//
		String sqlDropoff="SELECT COUNT(*) FROM `order_return` WHERE order_type='dropoff' AND order_status LIKE 'returned%' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("seventh query is "+sqlDropoff);
		String count6=getJdbcTemplate().queryForObject(sqlDropoff, String.class);
		storeReportHash.put("totDropOff", count6);
		//
		/*String sqlUndel="";
		System.out.println("8th query is "+sqlUndel);
		String count7=getJdbcTemplate().queryForObject(sqlUndel, String.class);
		storeReportHash.put("totUndelivered", count7);*/
		//
		String sqlAssignedReturn="select count(*) from `order_return` where store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("9th query is "+sqlAssignedReturn);
		String count8=getJdbcTemplate().queryForObject(sqlAssignedReturn, String.class);
		storeReportHash.put("totAssignedReturn",count8);
		//
		String sqlSD="SELECT COUNT(*) FROM `order` WHERE order_status='Scheduled delivery' and order_type='delivery' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("10th query "+sqlSD);
		String count9=getJdbcTemplate().queryForObject(sqlSD, String.class);
		storeReportHash.put("totScheduleDel", count9);
		//
		String sqlSR="SELECT COUNT(*) FROM `order_return` WHERE order_status='Scheduled pickup' and order_type='pickup' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("11th query is "+sqlSR);
		String count10=getJdbcTemplate().queryForObject(sqlSD, String.class);
		storeReportHash.put("totScheduleRet",count10);
		//
		String sqlCod="SELECT SUM(order_value) FROM `order` WHERE payment_type='cod' and order_status='delivered' AND store_id='"+storeId+"'"+" AND delivered_date BETWEEN '"+fromDate+"'"+" AND'"+toDate+"'";
		System.out.println("12th query "+sqlCod);
		String count11=getJdbcTemplate().queryForObject(sqlCod, String.class);
		storeReportHash.put("totCodCollect", count11);
		//
		/*String sqlCash="SELECT SUM(order_value) FROM `order` WHERE payment_type='cod' and payment_mode='cash' and order_status='delivered'";
		System.out.println("13th query is"+sqlCash );
		String count12=getJdbcTemplate().queryForObject(sqlCash, String.class);
		storeReportHash.put("totCashCollect", count12);*/
		//
		/*String sqlCashRemit="SELECT SUM(order_value) FROM `order` WHERE payment_type='cod' and payment_mode='card' and order_status='delivered'";
		System.out.println("14th query is "+sqlCashRemit);
		String count13=getJdbcTemplate().queryForObject(sqlCashRemit, String.class);
		storeReportHash.put("totCashRemit", count13);*/
		//
		sr.setTotInward((String) storeReportHash.get("totInward"));
		sr.setTotScheduleDel((String) storeReportHash.get("totScheduleDel"));
		sr.setTotCancelled((String)storeReportHash.get("totCancelled"));
		//sr.setTotCashCollect((String)storeReportHash.get("totCashCollect"));
		//sr.setTotCashRemit((String) storeReportHash.get("totCashRemit"));
		sr.setTotDelivery((String) storeReportHash.get("totDelivery"));
		sr.setTotCollected((String) storeReportHash.get("totCollected"));
		sr.setTotPicked((String) storeReportHash.get("totPicked"));
		sr.setTotDropOff((String) storeReportHash.get("totDropOff"));
		sr.setTotAssignedReturn((String) storeReportHash.get("totAssignedReturn"));
		sr.setTotScheduleRet((String) storeReportHash.get("totScheduleRet"));
		sr.setTotCodCollect((String) storeReportHash.get("totCodCollect"));
		
		return sr;
	}

}
