package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dataisys.taykit.GCM.App;
import com.dataisys.taykit.GCM.Content;
import com.dataisys.taykit.GCM.POST2GCM;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.DeliveryBoyDao;
/*import com.dataisys.pudo.daoimpl.StoreOrdersDAOImpl;
import com.dataisys.pudo.storetabletforms.Card;
import com.dataisys.pudo.storetabletforms.Company;
import com.dataisys.pudo.storetabletforms.InwardEntry;
import com.dataisys.pudo.storetabletforms.InwardSearch;
import com.dataisys.pudo.storetabletforms.MakeOrderDelivery;
import com.dataisys.pudo.storetabletforms.OrderAssignment;
import com.dataisys.pudo.storetabletforms.OrderReturn;
import com.dataisys.pudo.storetabletforms.OrderReturnForMultipleProducts;
import com.dataisys.pudo.storetabletforms.OutwardEntry;
import com.dataisys.pudo.storetabletforms.ReturnDispatch;
import com.dataisys.pudo.storetabletmodel.Order;*/
/*import com.dataisys.taykit.storetabletforms.ReturnDispatch;*/
import com.dataisys.taykit.dao.OrderAssignmentDAO;
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.OrderDeliveryDAO;
import com.dataisys.taykit.dao.OrderReturnDAO;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.ReturnDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.dao.UserStoreDAO;
import com.dataisys.taykit.mobile.model.InwardSearch;
import com.dataisys.taykit.mobile.model.OrderDelivery;
import com.dataisys.taykit.mobile.model.ReturnsForAndroid;
import com.dataisys.taykit.mobile.model.UserStore;
import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.InwardEntry;
import com.dataisys.taykit.model.MakeOrderDelivery;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderAssignment;
import com.dataisys.taykit.model.OrderReturn;
import com.dataisys.taykit.model.OrderReturnForMultipleProducts;
import com.dataisys.taykit.model.OutwardEntry;
import com.dataisys.taykit.model.RegistrationKey;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.ReturnDispatch;
import com.dataisys.taykit.model.StockInward;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.services.OtpService;

@Controller
@RequestMapping("/getData/version1")
public class StoreTabletApplicationController {
	
	@Autowired
	public OrderAssignmentDAO orderAssignmentDAO;
	
	@Autowired
	public OrderDeliveryDAO orderDeliveryDAO;
	
	@Autowired
	public UserStoreDAO userStoreDAO;
	
	@Autowired
	public OrderDao orderDao;
	
	@Autowired
	public UserDao userDao;
	
	@Autowired
	public OrderReturnDAO orderReturnDAO;
	
	@Autowired
	public CustomerDao customerDao;
	
	@Autowired
	public RegistrationKeyDao regKeyDao;
	
	@Autowired
	public PudoStoreDao storeDao;
	
	@Autowired
	public DeliveryBoyDao deliveryBoyDao;
	
	@Autowired
	public ReturnDao returnDao;
	
	/*@RequestMapping(value="/getName/{name}", method = RequestMethod.GET)
	public @ResponseBody Company getShopInJSON(@PathVariable String name) {
		
		System.out.println("Inside getShopInJSON() of Version1Controller");
		Company company = new Company();
		company.setName(name);
		company.setStaffName(new String[]{"Suri", "Kishore"});
		return company;
	}*/
	
	/* Methods for getting orders */
	//Original Code 
	/*@RequestMapping("getOrder")
	public @ResponseBody ArrayList<Order> getOrderList() {
		ArrayList<Order> orderDetails = new StoreContactsDaoImpl().getAllOrders();
		return orderDetails;
	}*/
	
	@RequestMapping("getTaykitOrders/{storeId}")
	public @ResponseBody List<com.dataisys.taykit.model.Order> getTaykitOrderListByStoreId(@PathVariable String storeId) {
		List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getOrdersByStoreId(storeId);
		return orderDetails;
	}
	
	
	@RequestMapping("getTaykitOrders")
	public @ResponseBody List<com.dataisys.taykit.model.Order> getTaykitOrderList() {
		List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getOrders();
		return orderDetails;
	}
	
	@RequestMapping("getOrder/{storeId}")
	public @ResponseBody List<Order> getOrderListByStoreId(@PathVariable String storeId) {
		String orderType = "delivery";
		List<Order> orderDetails = orderDao. getOrdersByStoreId(storeId,orderType);
		return orderDetails;
	}
	
	//10-09-2015
	@RequestMapping("getOrdersForInward/{storeId}")
	public @ResponseBody List<Order> getOrdersForInward(@PathVariable String storeId) {
		List<Order> orderDetails = orderDao.getOrdersForInwardByStoreId(storeId);
		return orderDetails;
	}
	
	@RequestMapping("getOrderByTrackingId/{trackingId}")
	public @ResponseBody Order getOrderByTrackingId(@PathVariable String trackingId) {		
		System.out.println("Tracking Id: "+trackingId);
		Order order = orderDao.getOrdersByTrackingId(trackingId);
		System.out.println("Store Id in getOrderByTracking id is: "+order.getStoreId());
		if(order.getStoreId() == 0) {
			order.setStoreId(Long.valueOf(0));
		} else {
			Store store = storeDao.getStoreById(order.getStoreId());
			order.setStoreId(Long.valueOf(store.getAccountNo()));	
		}
		return order;
		
	}
	/* End of the methods for getting orders */
	
	/* Method for making delivery */
	@RequestMapping(value = "makeDelivery", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void makeDelivery(@RequestBody MakeOrderDelivery makeOrderDelivery) {
		String signature = makeOrderDelivery.getSignature();
		String trackingId = makeOrderDelivery.getTrackingId();
		
		System.out.println("Tracking Iddd: "+trackingId);
		//String transactionRefNumber = makeOrderDelivery.getTransactionRefNumber();
		System.out.println("Signature: "+signature);
		//System.out.println("TransactionRefNo: "+transactionRefNumber);
	}
	
	@RequestMapping(value = "orderDelivery", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void orderDelivery(@RequestBody OrderDelivery orderDelivery) {
		orderDeliveryDAO.createOrderDelivery(orderDelivery);
		System.out.println("Tracking Id: "+orderDelivery.getTrackingId());
		System.out.println("Customer Signature: "+orderDelivery.getSignature());
		
		String paymentStatus = "paid";
		String orderStatus = "Delivered";
        Order fetchedOrder = null;
		
		try {
			fetchedOrder = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fetchedOrder != null) {
			System.out.println("Inside orderDelivery if(fetchedOrder != null): and order status is: "+fetchedOrder.getOrderStatus());
			if(!((fetchedOrder.getOrderStatus().trim().equalsIgnoreCase("Delivered")) || (fetchedOrder.getOrderStatus().trim().contains("Order cancelled")))) {
				System.out.println("Inside if->if");
				orderDao.updateOrderDelivery(orderDelivery.getTrackingId(), orderStatus, paymentStatus);
			} else {
				System.out.println("Order Status is already Delivered so no updation again");
			}
		} else {
			System.out.println("Fetched order is null");
		}				
	}
	/* End of this method of Making delivery */
	
	
	/* Order Returns based on Abraar's Pojo */
	
	@RequestMapping("getOrdersForReturn/{storeId}")
	public @ResponseBody List<com.dataisys.taykit.mobile.model.ReturnsForAndroid> getOrdersForreturn(@PathVariable String storeId) {
		List<com.dataisys.taykit.model.Return> orderDetails = returnDao.returnvaluesByStoreId(storeId);
		List<ReturnsForAndroid> returnDetails = new ArrayList<ReturnsForAndroid>();
		
		for(Return ret : orderDetails) {
			ReturnsForAndroid retForAndroid = new ReturnsForAndroid();
			retForAndroid.setTrackingId(ret.getTrackingId());
			retForAndroid.setOrderId(ret.getOrderId());
			retForAndroid.setReturnId(String.valueOf((long)Double.parseDouble(ret.getReturnId())));
			retForAndroid.setAssignedTo(ret.getAssignedTo());
			retForAndroid.setOrderType(ret.getPaymentMode().trim());
			retForAndroid.setCustomerName(ret.getConsigneeName().trim());
			retForAndroid.setCustomerAddress(ret.getCustomerAddress());
			retForAndroid.setCustomerMobile(ret.getContactNo());
			retForAndroid.setOrderDetails(ret.getProductToBeShipped());
			returnDetails.add(retForAndroid);
		}
		return returnDetails;
	}
	
	
	@RequestMapping("getPickUpOrder/{storeId}")
	public @ResponseBody List<com.dataisys.taykit.mobile.model.ReturnsForAndroid> getOrderListForPickupByStoreId(@PathVariable String storeId) {		
		
		
		System.out.println("Inside getPickUpOrder and storeId from client is: "+storeId);
		
		List<Return> orderDetails = returnDao.getAllReturnsForPickupByStoreId(storeId);
		List<ReturnsForAndroid> returnDetails = new ArrayList<ReturnsForAndroid>();
		for(Return ret : orderDetails) {
			ReturnsForAndroid retForAndroid = new ReturnsForAndroid();
			retForAndroid.setTrackingId(ret.getTrackingId());
			retForAndroid.setOrderId(ret.getOrderId());
			retForAndroid.setReturnId(String.valueOf((long)Double.parseDouble(ret.getReturnId())));
			retForAndroid.setAssignedTo(ret.getAssignedTo());
			retForAndroid.setOrderType(ret.getPaymentMode().trim());
			retForAndroid.setCustomerName(ret.getConsigneeName().trim());
			retForAndroid.setCustomerAddress(ret.getCustomerAddress());
			retForAndroid.setCustomerMobile(ret.getContactNo());
			retForAndroid.setOrderDetails(ret.getProductToBeShipped());
			returnDetails.add(retForAndroid);
		}		
		return returnDetails;
	}
	
	@RequestMapping(value = "assignReturnOrder", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void assignReturnOrder(@RequestBody OrderAssignment orderAssignment) {				
		
		ArrayList<String> trackingIdList = orderAssignment.getOrderTrackingIds();
		String deliveryBoyId = orderAssignment.getDeliveryBoyId();
		String toDeliveryBoyNote = orderAssignment.getToDeliveryBoyNote();
		
		for (String string : trackingIdList) {
			System.out.println("Tracking Id: "+string);
		}
		
		if(deliveryBoyId != null && toDeliveryBoyNote != null) {
			System.out.println("DeliveryBoyId: "+deliveryBoyId);
			System.out.println("Note For DeliveryBoy: "+toDeliveryBoyNote);
		}
		
		String storeId = orderAssignment.getStoreId();
		String toStoreNote = orderAssignment.getToStoreIdNote();
		if(storeId != null && toStoreNote != null) {
			System.out.println("StoreId: "+storeId);
			System.out.println("Note For Store: "+toStoreNote);
		}		
		
		//In the following code I'm Assigning orders to orders table
		List<com.dataisys.taykit.model.Return> orders = new ArrayList<com.dataisys.taykit.model.Return>();
		for (String trackingId : trackingIdList) {
			com.dataisys.taykit.model.Return order = new com.dataisys.taykit.model.Return();
			order.setTrackingId(trackingId);
			if(deliveryBoyId != null) {
				order.setStatus("out for delivery");
			}
			
			orders.add(order);
		}
		if(deliveryBoyId != null) {			
			returnDao.assignReturns(orders, deliveryBoyId, "DELIVERY_BOY");
			
			/*code for posting the notification to the particular delivery boy id to whome assigned*/
			System.out.println("DeliveryBoy id in assignOrder is: "+orderAssignment.getDeliveryBoyId());			
			RegistrationKey regKeyObj = regKeyDao.getRegkeyByAccountid(orderAssignment.getDeliveryBoyId());
			System.out.println("Registration key is: "+regKeyObj.getRegKey());
			String apiKey = "AIzaSyAHuIzTDBpD7Cr8oST3TtL2hQn8xJoggjo";			
			
			for (Return order : orders) {
				Content content = new Content();
				content.addRegId(regKeyObj.getRegKey());
				content.createData("You've got an Order To Deliver", order.getTrackingId());
				POST2GCM.post(apiKey, content);
			}
			/*end of suri code - end of posting notification to the del boy while assignig*/			
		}
		if(storeId != null) {
			returnDao.assignReturns(orders, storeId, "POD_USER");
		}
	}
	
	@RequestMapping(value = "orderReturnDelivery", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void orderReturnDelivery(@RequestBody OrderDelivery orderDelivery) {
		//orderDeliveryDAO.createOrderDelivery(orderDelivery);
		System.out.println("Inside OrderReturnDelivery");
		System.out.println("Tracking Id: "+orderDelivery.getTrackingId());
		System.out.println("Customer Signature: "+orderDelivery.getSignature());
		String orderStatus = "Returned";
		orderDeliveryDAO.createOrderDelivery(orderDelivery);
		returnDao.updateOrderReturnDelivery(orderDelivery.getTrackingId(), orderStatus);		
	}
	
	
	/* End of the Order Returns */
	
	
	
	/* Return Product */	
	
	@RequestMapping("getReturnOrders")
	public @ResponseBody List<com.dataisys.taykit.model.Order> getReturnOrders() {
		List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getReturnOrder();
		return orderDetails;
	}
	
	
	@RequestMapping(value = "returnOrder", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void returnOrder(@RequestBody OrderReturn orderReturn) {		
		//ArrayList<Order> orderDetails = new OrderDAOImpl().getAllOrders();
		System.out.println("Invoice: "+orderReturn.getInvoiceNumber());
		System.out.println("OrderTitle: "+orderReturn.getOrderTitle());
		System.out.println("ReturnNote: "+orderReturn.getReturnNote());
		System.out.println("CustomerName: "+orderReturn.getCustomerName());
		System.out.println("ContactNumber: "+orderReturn.getContactNumber());
		System.out.println("EmailId: "+orderReturn.getEmailId());
		System.out.println("ProductQuantity: "+orderReturn.getQuantity());
	}	
	
	@RequestMapping(value = "returnMultipleOrder", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void returnMultipleOrder(@RequestBody OrderReturnForMultipleProducts orderReturn) {		
		//ArrayList<Order> orderDetails = new OrderDAOImpl().getAllOrders();
		System.out.println("Invoice: "+orderReturn.getInvoiceNumber());
		System.out.println("OrderTitle: "+orderReturn.getOrderTitle());
		System.out.println("ReturnNote: "+orderReturn.getReturnNote());
		System.out.println("CustomerName: "+orderReturn.getCustomerName());
		System.out.println("ContactNumber: "+orderReturn.getContactNumber());
		System.out.println("EmailId: "+orderReturn.getEmailId());
		//System.out.println("Products Length: "+orderReturn.getProducts().size());
	}
	
	@RequestMapping(value="/getReturnOrdersByStoreId/{storeId}", method = RequestMethod.GET)
	public @ResponseBody List<com.dataisys.taykit.mobile.model.ReturnsForAndroid> getReturnOrdersByStoreId(@PathVariable String storeId) {
		
		System.out.println("Inside getReturnOrdersByStoreId");
		double storeIdd = Double.parseDouble(storeId);
		System.out.println("StoreIdd: "+storeId);
		List<com.dataisys.taykit.mobile.model.ReturnsForAndroid> listOfOrderReturns = orderReturnDAO.getOrderReturnByStoreId(storeIdd);
		System.out.println("size of fetched listOfOrderReturns: "+listOfOrderReturns.size());
		return listOfOrderReturns;
	}
	
	/* End of this Return Product */
	
	/* Assign Order */
	@RequestMapping(value = "assignOrder", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void assignOrder(@RequestBody OrderAssignment orderAssignment) {				
		
		ArrayList<String> trackingIdList = orderAssignment.getOrderTrackingIds();
		String deliveryBoyId = orderAssignment.getDeliveryBoyId();
		String toDeliveryBoyNote = orderAssignment.getToDeliveryBoyNote();
		
		for (String string : trackingIdList) {
			System.out.println("Tracking Id: "+string);
		}
		
		if(deliveryBoyId != null && toDeliveryBoyNote != null) {
			System.out.println("DeliveryBoyId: "+deliveryBoyId);
			System.out.println("Note For DeliveryBoy: "+toDeliveryBoyNote);
		}		
		String storeId = orderAssignment.getStoreId();
		String toStoreNote = orderAssignment.getToStoreIdNote();
		if(storeId != null && toStoreNote != null) {
			System.out.println("StoreId: "+storeId);
			System.out.println("Note For Store: "+toStoreNote);
		}		
		
		//In the following code I'm Assigning orders to orders table
		List<com.dataisys.taykit.model.Order> orders = new ArrayList<com.dataisys.taykit.model.Order>();
		for (String trackingId : trackingIdList) {
			com.dataisys.taykit.model.Order order = new com.dataisys.taykit.model.Order();
			order.setTrackingId(trackingId);
			if(deliveryBoyId != null) {
				order.setOrderStatus("out for delivery");
			}
			
			orders.add(order);
		}
		if(deliveryBoyId != null) {			
			orderDao.updateAssign(orders, deliveryBoyId, "DELIVERY_BOY");
			
			/*code for posting the notification to the particular delivery boy id to whome assigned*/
			System.out.println("DeliveryBoy id in assignOrder is: "+orderAssignment.getDeliveryBoyId());			
			RegistrationKey regKeyObj = regKeyDao.getRegkeyByAccountid(orderAssignment.getDeliveryBoyId());
			System.out.println("Registration key is: "+regKeyObj.getRegKey());
			String apiKey = "AIzaSyAHuIzTDBpD7Cr8oST3TtL2hQn8xJoggjo";			
			
			for (Order order : orders) {
				Content content = new Content();
				content.addRegId(regKeyObj.getRegKey());
				content.createData("You've got an Order To Deliver", order.getTrackingId());
				POST2GCM.post(apiKey, content);
			}
			/*end of suri code - end of posting notification to the del boy while assignig*/			
		}
		if(storeId != null) {
			orderDao.updateAssign(orders, storeId, "POD_USER");
		}
	}	
	/* End of the assign order */
	
	/* Stock Management */
	/*@RequestMapping(value = "inwardEntry", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void inwardEntry(@RequestBody InwardEntry inwardEntry) {
		ArrayList<String> trackingIds = inwardEntry.getTrackingIds();
		String note = inwardEntry.getNote();
		System.out.println("InwardEntry Note: "+note);
		System.out.println("Size of ListOfTrackingIds: "+trackingIds.size());
	}*/
	
	@RequestMapping(value="/podLevelInward")
	public @ResponseBody void podLevelInward(@RequestBody StockInward obj){
		System.out.println("Inside podLevelInward in StoreTabletApplicationController");
		System.out.println("is Object is null : "+(obj==null));
		orderDao.podLevelInward(obj);
		
		//Code for sending OTP
		try {
			Order order = orderDao.getOrdersByTrackingId(obj.getTrackingId());
			
			Store store = storeDao.getStoreById((long)order.getStoreId());
			if(order != null) {
				if(order.getOrderType().trim().equalsIgnoreCase("collect")) {
					OtpService service = new OtpService();
					String otp = service.otpService(order, store);
					System.out.println("TrackingId: "+order.getTrackingId());
					System.out.println("Otp is: "+otp);
					orderDao.updateOtp(order.getTrackingId(), otp);
				}
			}
		} catch (Exception e) {
			System.err.println("Inside StockInwardController's catch block and the error is: "+e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "outwardEntry", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void outwardEntry(@RequestBody OutwardEntry outwardEntry) {
		ArrayList<String> trackingIds = outwardEntry.getTrackingIds();
		String note = outwardEntry.getNote();
		System.out.println("OutwardEntry Note: "+note);
		System.out.println("Size of ListOfTrackingIds: "+trackingIds.size());
	}
	
	@RequestMapping(value = "returnDispatch", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void returnDispatch(@RequestBody ReturnDispatch returnDispatch) {
		ArrayList<String> trackingIds = returnDispatch.getTrackingIds();		
	}
	
	@RequestMapping(value = "inwardSearch", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void inwardSearch(@RequestBody InwardSearch inwardSearch) {
		
	}
	/* End of Stock Management */
	
	/* Getting DeliveryBoys for particular Store Id */
	@RequestMapping(value="/getDeliveryBoysById/{storeid}", method = RequestMethod.GET)
	public @ResponseBody List<User> getDeliveryBoysById(@PathVariable String storeid) {
		System.out.println("StoreId is: "+storeid);
		double storeId = Double.parseDouble(storeid);
		System.out.println("After parsing the storeId: "+storeId);
		
		//My UserStore original code
		/*List<UserStore> userStores = userStoreDAO.getUsersByStoreId(storeId);
		List<User> deliveryBoys = new ArrayList<User>();
		for (UserStore userStore : userStores) {
			double userId = userStore.getUserId();
			User user = userDao.getUserById(userId);
			deliveryBoys.add(user);
		}
		return deliveryBoys;*/
		
		//Based on Irfan's deliveryboy table
		Store store = storeDao.getStoreByAccountNo(storeid);
		long actualStoreId = (long) store.getStoreId();
		System.out.println("Store Table Id: "+actualStoreId);
		
		List<DeliveryBoys> deliveryBoys = deliveryBoyDao.getDeliveryBoysByStoreId(actualStoreId);
		List<User> boys = new ArrayList<User>();
		
		for (DeliveryBoys deliveryBoys2 : deliveryBoys) {
			String deliveryBoyName = deliveryBoys2.getDeliveryBoyName().trim().toString();
			User user = userDao.getDeliveryBoyByName(deliveryBoyName);
			if(user != null) {
				boys.add(user);
				System.out.println("Users Name: "+user.getUserName());				
			}
		}
		System.out.println("Size of DeliveryBoys By StoreId: "+boys.size());
		return boys;
	}
	
	@RequestMapping(value="/getStoreDetailsById/{storeid}", method = RequestMethod.GET)
	public @ResponseBody Store getStoreById(@PathVariable String storeid) {
		System.out.println("StoreId is: "+storeid);
		double storeId = Double.parseDouble(storeid);
		System.out.println("After parsing the storeId: "+storeId);
		Store store = storeDao.getStoreByAccountNo(storeid);
		System.out.println("is Store null: "+(store == null));
		return store;
	}
	/* End of Getting DeliveryBoys */
	
	/* Getting pod stores to assign orders from a pod store*/
	@RequestMapping("getPartnerStores")
	public @ResponseBody List<User> getDeliveryPodStores() {
		String userType = "PUDO_USER";
		List<User> stores = userDao.getAllUsersByType(userType);
		return stores;
	}
	
	@RequestMapping("getAllStores")
	public @ResponseBody List<Store> getAllStores() {
		List<Store> stores = storeDao.getStores();
		return stores;
	}
	/* End of getting pod stores */
	
	/* Cash Management */	
	/*@RequestMapping(value = "payAmountViaCard", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void payAmount(@RequestBody Card card) {
		
		String cardType = card.getCardType();
		String cardName = card.getCardName();
		String bankName = card.getBankName();
		String amount = card.getAmount();
		String signature = card.getSignature();
		
		System.out.println("CardType: "+cardType);
		System.out.println("CardName: "+cardName);
		System.out.println("BankName: "+bankName);
		System.out.println("Amount: "+amount);
		System.out.println("Signature: "+signature);
	}	*/
	
	/*@RequestMapping("getOwnData")
	public @ResponseBody List<String> getSomeJunkData() {
		System.out.println("Inside getDeliveryB");
		List<String> list = new ArrayList<String>();
		list.add("One");
		list.add("Two");
		list.add("Three");
		return list;
	}*/
	
	@RequestMapping(value="/getAssignedOrderForDb/{deliveryBoyId}", method = RequestMethod.GET)
	public @ResponseBody List<com.dataisys.taykit.model.Order> getAssignedOrders(@PathVariable String deliveryBoyId) {
		System.out.println("Inside getDeliveryB");
		List<com.dataisys.taykit.model.Order> ordersAssignedToDb = orderDao.getOrdersByDeliveryBoyId(deliveryBoyId);
		return ordersAssignedToDb;
	}
	
	/*@RequestMapping(value="/InwardEntry")
	public void inwardStock(@RequestBody StockInward obj){
		System.out.println("Inside InwardEntry");
		System.out.println("got object"+obj);
		orderDao.inwardOrder(obj);
	}*/
	
	/* For Creating Customer */
	@RequestMapping(value = "createCustomer", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void createOrder(@RequestBody com.dataisys.taykit.model.Customer customer){
		System.out.println("I'm Inside createCustomer method");		
		System.out.println("Customer Name: "+customer.getCustomerName());
		System.out.println("Customer Address: "+customer.getCustomerAddress());
		System.out.println("Customer Mobile Number: "+customer.getCustomerMobile());
		System.out.println("Customer City: "+customer.getCity());
		System.out.println("Customer PinCode: "+customer.getPinCode());
		System.out.println("Customer Email: "+customer.getCustomerEmail());
		
		customerDao.createCustomer(customer);
	}
	
	@RequestMapping(value = "getPerticularCustomer", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody com.dataisys.taykit.model.Customer getCustomerList(@RequestBody com.dataisys.taykit.model.Customer customer) throws IOException {		
		com.dataisys.taykit.model.Customer customer1 = customerDao.getPerticularCustomer(customer);
		return customer1;
	}
	/* End of the Customer Code */
	
	@RequestMapping(value = "storeLocationBasedOnBarcodeScanning", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void storeLocationBasedOnBarcodeScan(@RequestBody Order order) {
		
		System.out.println("(Inside storeLocationBasedOnBarcode");
		System.out.println("TrackingId: "+order.getTrackingId());
		System.out.println("DeliveredLocation: "+order.getDeliveredLocationLatLongs());
		orderDao.updateDeliveredLocationLatLongsInOrder(order);
		
	}
}
