package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.OrderAttachment;

public class OrderAttachmentMapper implements RowMapper<OrderAttachment> {

	public OrderAttachment mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderAttachment ob= new OrderAttachment();
			ob.setAttachmentId(rs.getDouble("attachment_id"));
			ob.setOrderId(rs.getDouble("order_id"));
			ob.setAttachmentDesc(rs.getBlob("attachment_desc"));
			ob.setUpdatedBy(rs.getString("updated_by"));
			ob.setUpdatedOn(rs.getTimestamp("updated_on"));
		return ob;
	}

}
