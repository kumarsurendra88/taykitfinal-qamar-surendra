package com.dataisys.taykit.daoimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;

import com.dataisys.taykit.dao.ZoneDAO;
import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.Zone;
import com.dataisys.taykit.rowmapper.RegionRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.rowmapper.ZoneRowMapper;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ZoneDAOImpl extends JdbcDaoSupport implements ZoneDAO{

	public boolean CreateZone(String zone_name) {
		// TODO Auto-generated method stub
		boolean flag=false;
		//System.out.println("zone_name : "+zone_name);
		Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		System.out.println("running scheduler" );
		
		Calendar cal = Calendar.getInstance();
		/*cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -days;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();*/
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				
		System.out.println("after converting"+dateFormat.format(today));
		
		String sql="insert into zones(zone_name) values(?)";
	//	getJdbcTemplate().update(sql,dateFormat.format(today));
		int i=getJdbcTemplate().update(sql,zone_name);
		System.out.println("value of i : "+i);
		if(i>0)
		{
			flag=true;
			System.out.println("Data Inserted");
		}
		
		return flag;
	}

	public List<Zone> getZoneNames() {
		// TODO Auto-generated method stub
		String sql="select * from zones ";
		List<Zone> ListOfZoneNames=getJdbcTemplate().query(sql,new ZoneRowMapper());
		//getJdbcTemplate().query(sql,new ZoneRowMapper());
				
			//	getJdbcTemplate().queryForObject(sql, new Object[]{zone_id},new RegionRowMapper());
//		System.out.println("******************: "+ListOfZoneNames);

		return ListOfZoneNames;
	}

	public String getZoneNamesByID(String accNumber) {
		System.out.println("accno in zoneimpl : "+accNumber);
		accNumber="zonalManager1@taykit.in";
		String sql="select * from zones where zone_id=1";
		List<Zone> ListOfZoneNames=getJdbcTemplate().query(sql, new ZoneRowMapper());
				
		String Q="";
		for (Zone temp : ListOfZoneNames) {
			Q=temp.getZone_name();
		//	System.out.println("Zone Name : "+Q);
		}
			//	getJdbcTemplate().queryForObject(sql, new Object[]{zone_id},new RegionRowMapper());
		//System.out.println("******list************: "+ListOfZoneNames);

		return Q;
	}

		
		
	}
