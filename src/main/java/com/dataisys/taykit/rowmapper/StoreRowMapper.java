package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

//import com.dataisys.taykit.model.OrderAttachments;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.Store;

public class StoreRowMapper implements RowMapper<Store>{

	
	
	public Store mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 Store store= new Store();
	 	store.setAddress(rs.getString("store_address"));
	 	store.setCctvId(rs.getString("store_cctvid"));
	 	store.setCity(rs.getString("store_city"));
	 	store.setEdmAvailbility(rs.getBoolean("edm_availability"));
	 	store.setGeoLocation(rs.getString("store_gpslocation"));
	 	store.setLandMark(rs.getString("store_landmark"));
	 	store.setName(rs.getString("store_name"));
	 	store.setPinCode(rs.getString("pincode"));
	 	store.setStoreId(rs.getDouble("store_id"));
	 	store.setType(rs.getString("store_type"));
	 	store.setStoreStatus(rs.getBoolean("store_status"));
	 	store.setUpdatedOn(rs.getTimestamp("updated_on"));
	 	store.setUpdatedBy(rs.getString("updated_by"));
	 	store.setSurveyToken(rs.getString("survey_token"));
	 	store.setReportingStore(rs.getString("reporting_store"));
	 	store.setCashPayment(rs.getString("cash_payment"));
	 	store.setCreditCardPayment(rs.getString("creditcard_payment"));
	 	store.setMobilePayment(rs.getString("mobile_payment"));
	 	store.setHub_id(rs.getDouble("hub_id"));
		store.setRegion_id(rs.getInt("region_id"));
		store.setZone_id(rs.getInt("zone_id"));
	 	return store;
 }
	 	
 
 
 }
}

