package com.dataisys.taykit.daoimpl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.CustomerHistoryDao;
import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.dao.HubDAO;
/*import com.dataisy.taykit.rowmapper.AttachmentRowMapper;
 import com.dataisy.taykit.rowmapper.DummyRowMapper;
 import com.dataisy.taykit.rowmapper.OrderCommentsRowMapper;
 import com.dataisy.pudo.rowmapper.OrderRowMapper;*/
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RetailerDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.CancelledOrderByData;
import com.dataisys.taykit.model.CustomerHistory;
import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderActivity;
import com.dataisys.taykit.model.OrderAttachment;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RunsheetData;
import com.dataisys.taykit.model.SearchOrder;
import com.dataisys.taykit.model.StockInward;
import com.dataisys.taykit.model.StockOutward;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreLocation;
import com.dataisys.taykit.model.StoreRunSheet;
import com.dataisys.taykit.model.StoreSummery;
import com.dataisys.taykit.model.UpdateDeliveryStatus;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.OrderActivityMapper;
import com.dataisys.taykit.rowmapper.OrderAdvanceSearchRowMapper;
import com.dataisys.taykit.rowmapper.OrderCommentMapper;
import com.dataisys.taykit.rowmapper.OrderMapper;
import com.dataisys.taykit.rowmapper.RandomTestRowMapper;
import com.dataisys.taykit.rowmapper.ReportingStoreRowMapper;
/*import com.dataisys.taykit.model.OrderAttachments;*/
import com.dataisys.taykit.rowmapper.RunsheetMapperForStore;
import com.dataisys.taykit.rowmapper.StoreRowMapper;
import com.dataisys.taykit.services.OtpService;

public class OrderDaoImpl extends JdbcDaoSupport implements OrderDao {
	private JdbcTemplate template; 
	@Autowired
	PudoStoreDao storeDao;
	@Autowired
	DeliveryBoyDao deliveryBoyDao;
	@Autowired 
	UserDao userDao;
	@Autowired
	RetailerDao retailerDao;
	@Autowired
	HubDAO hubdao;
	@Autowired
	CustomerHistoryDao customerHistoryDao;

	final int batchSize = 1000;
	int count = 0;
	int successCount = 0;
	int failCount = 0;
	int notAavailable = 0;

	

	public void createOrder(com.dataisys.taykit.model.Order order) {
		System.out.println("Inserting in order" + order);
		System.out.println("checking for order type" + order.getOrderType());

		
/* Code For Storing Customer History */
		
		try {
			
			String customerContact = order.getCustomerContact();
			List<Order> orderList = getAllOrdersByContactNumber(customerContact);
			if(orderList != null && orderList.size() >= 0) {
				for (Order order2 : orderList) {
					CustomerHistory customerHistory = new CustomerHistory();
					customerHistory.setCustomerName(order2.getCustomerName());
					customerHistory.setCustomerMobile(order2.getCustomerContact());
					customerHistory.setTrackingId(order2.getTrackingId());
					customerHistoryDao.createCustomerHistory(customerHistory);
				}				
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}		
		
		/* End of the Code For Storing Customer History */
		
		/*
		 * if(order.getOrderType().equalsIgnoreCase("return")){
		 * 
		 * System.out.println("making an entry in return table");
		 * //makeReturnEntry(order); } else {
		 */

		if (order.getStoreId() > 0) {
			System.out.println("with store id");
			order.setStockFlag("Not Available");
			String sql = "insert into `order` (tracking_id,order_id,order_title,order_details,order_type,order_category,order_priority,"
					+ "store_id,retailer_id,order_refno,customer_name,customer_mobile,customer_email,customer_address,"
					+ "city_name,customer_zipcode,payment_type,bay_number,order_status,receipt_date,delivery_date,assigned_to,delivered_date,delivered_by,payment_status,"
					+ "payment_reference,order_value,created_by,created_on,updated_by,updated_on,stock_flag) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			try {
				getJdbcTemplate().update(sql, order.getTrackingId(),
						order.getOrderId(), order.getOrderTitle(),
						order.getOrderDetails(), order.getOrderType(),
						order.getOrderCategory(), order.getOrderPriority(),
						order.getStoreId(), order.getRetailerId(),
						order.getOrderRefno(), order.getCustomerName(),
						order.getCustomerContact(), order.getCustomerEmail(),
						order.getCustomerAddress(), order.getCityName(),
						order.getCustomerZipcode(), order.getPaymentType(),
						order.getBayNumber(), order.getOrderStatus(),
						order.getReceiptDate(), order.getDeliveryDate(),
						order.getAssignedTo(), order.getDeliveredDate(),
						order.getDeliveredBy(), order.getPaymentStatus(),
						order.getPaymentReference(), order.getOrderValue(),
						order.getCreatedBy(), order.getCreatedOn(),
						order.getUpdatedBy(), order.getUpdatedOn(),
						order.getStockFlag());

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
		} else {
			System.out.println("without store id");

			order.setStockFlag("Not Available");
			String sql = "insert into `order` (tracking_id,order_id,order_title,order_details,order_type,order_category,order_priority,"
					+ "retailer_id,order_refno,customer_name,customer_mobile,customer_email,customer_address,"
					+ "city_name,customer_zipcode,payment_type,bay_number,order_status,receipt_date,delivery_date,assigned_to,delivered_date,delivered_by,payment_status,"
					+ "payment_reference,order_value,created_by,created_on,updated_by,updated_on,stock_flag) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			getJdbcTemplate().update(sql, order.getTrackingId(),
					order.getOrderId(), order.getOrderTitle(),
					order.getOrderDetails(), order.getOrderType(),
					order.getOrderCategory(), order.getOrderPriority(),
					order.getRetailerId(), order.getOrderRefno(),
					order.getCustomerName(), order.getCustomerContact(),
					order.getCustomerEmail(), order.getCustomerAddress(),
					order.getCityName(), order.getCustomerZipcode(),
					order.getPaymentType(), order.getBayNumber(),
					order.getOrderStatus(), order.getReceiptDate(),
					order.getDeliveryDate(), order.getAssignedTo(),
					order.getDeliveredDate(), order.getDeliveredBy(),
					order.getPaymentStatus(), order.getPaymentReference(),
					order.getOrderValue(), order.getCreatedBy(),
					order.getCreatedOn(), order.getUpdatedBy(),
					order.getUpdatedOn(), order.getStockFlag());

			/* } */

		}
	}

	public List<com.dataisys.taykit.model.Order> getUnassignedOrder() {
		String sql = "select * from `order` where assigned_to<=0  and stock_flag='Available'";

		return getJdbcTemplate().query(sql, new OrderMapper());
	}

	public com.dataisys.taykit.model.Order getOrders(String trackingtId) {
		String sql = "select * from `order` where tracking_id=? ";
		try {
			return getJdbcTemplate().queryForObject(sql,
					new Object[] { trackingtId }, new OrderMapper());
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			return null;
		}

	}

	public void updateOrder(com.dataisys.taykit.model.Order object) {
		String sql = "update `order` set order_title=?,retailer_id=?,order_type=?,order_category=?,payment_type=?,order_status=?,order_details=?,"
				+ "customer_name=?,customer_mobile=?,customer_email=?,customer_address=?,city_name=?,customer_zipcode=?,assigned_to=? where tracking_id=?";
		getJdbcTemplate().update(sql, object.getOrderTitle(),
				object.getRetailerId(), object.getOrderType(),
				object.getOrderCategory(), object.getPaymentType(),
				object.getOrderStatus(), object.getOrderDetails(),
				object.getCustomerName(), object.getCustomerContact(),
				object.getCustomerEmail(), object.getCustomerAddress(),
				object.getCityName(), object.getCustomerZipcode(),
				object.getAssignedTo(), object.getTrackingId());

	}

	public void updateAssign(com.dataisys.taykit.model.Order object,
			double assignId) {
		
		
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date storeAssignDate = new Date();
			String sql = "update `order` set store_id=?,order_type=?,store_assign_date=? where tracking_id=?";

			getJdbcTemplate().update(sql, assignId, object.getOrderType(),dateFormat.format(storeAssignDate),object.getTrackingId());
			}

	

	public void addComment(String comment, String trackingId,
			HttpSession session) {
		// TODO Auto-generated method stub
		// String
		// sql="select order_id from `order` where tracking_id=? limit 1";
		String sql2 = "insert into order_comment (tracking_id,comment_text,comment_by,comment_date) values(?,?,?,?)";

		// int orderId=getJdbcTemplate().queryForInt(sql, new
		// Object[]{trackingId});

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));

		getJdbcTemplate().update(sql2, trackingId, comment,
				session.getAttribute("loggedStore"), date);

	}

	public List<com.dataisys.taykit.model.OrderComment> getComments(
			String trackingId) {
		String sql = "select order_id from `order` where tracking_id=? limit 1";
		String sql2 = "select * from `order_comment` where tracking_id=? order by updated_on desc";
		int orderId = getJdbcTemplate().queryForInt(sql,
				new Object[] { trackingId });

		return getJdbcTemplate().query(sql2, new Object[] { trackingId },
				new OrderCommentMapper());
	}

	

	public void addAttachments(String trackingId, UploadForm uploadItem)
			throws IOException {
		String sql = "select order_id from `order` where tracking_id=? limit 1";
		String sql2 = "insert into `order_attachment`(order_id,attachment_desc,updated_by) values(?,?,?)";
		int orderId = getJdbcTemplate().queryForInt(sql,
				new Object[] { trackingId });
		InputStream is = uploadItem.getFile().getInputStream();
		OrderAttachment obj = new OrderAttachment();
		// obj.setAttachmentDesc(is);
		try {
			getJdbcTemplate().update(sql2, orderId, is, 2);
		} catch (Exception e) {
			e.getMessage();
			System.err.println(e);
		}
	}

	public List<Order> getOrdersByDeliveryBoyId(String deliveryBoyAccNo) {	

		//Added on 05092015

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dbAssignedDate = new Date();
		System.out.println(dateFormat.format(dbAssignedDate));
		String formattedDate = dateFormat.format(dbAssignedDate);

		User user = userDao.getUserByAccNumber(deliveryBoyAccNo);
		//End
		List<Order> orders = null;
		if(user != null) {

		String sql = "SELECT * FROM `order` WHERE assigned_to = ? AND order_type = ? AND order_status != ? AND order_status NOT LIKE ? And delivery_boy_assign_date = ? AND (recent_status_date IS NULL OR recent_status_date != ?)";
		orders=getJdbcTemplate().query(sql,new Object[]{user.getUserName(), "Delivery", "Delivered","Order cancelled%", dateFormat.format(dbAssignedDate), formattedDate},new OrderMapper());	}
		return orders;
		}

	public List<Order> getReturnOrder() {
		String sql = "SELECT * FROM `order` WHERE order_type = 'return'";
		List<Order> listofReturnedOrders = getJdbcTemplate().query(sql,
				new OrderMapper());
		return listofReturnedOrders;
	}

	public Order getOrdersByTrackingId(String trackingId) {
		// TODO Auto-generated method stub
		String sql= "SELECT * FROM `order` WHERE tracking_id = ?";
		Order order = getJdbcTemplate().queryForObject(sql,new Object[]{trackingId},new OrderMapper());
		return order;
	}

	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		System.out.println("Inside getOrders of OrderDAOImpl");
		String sql = "SELECT * FROM `order`";
		List<Order> listofOrders = getJdbcTemplate().query(sql,
				new OrderMapper());
		System.out.println("list of Orders: " + listofOrders.isEmpty());
		return listofOrders;
	}

	public List<StoreLocation> getStoreDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateStatus(List<Order> orders, String status,String reason) {
		java.sql.Date null1 = null;
		Iterator<Order> iterator = orders.iterator();
		Date today= new Date();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		java.util.Date utilDate = new java.util.Date();
		null1 = new java.sql.Date(utilDate.getTime());
		while (iterator.hasNext()) {
			Order obj = iterator.next();
			if (obj.getTrackingId() == null) {

			}
			else{
	
			
			if(status.equalsIgnoreCase("Delivered")){
				
				System.out.println("inside delivered");
				
				System.out.println();
				//obj.setDeliveredDate(null1);
				
				String sql = "update `order` set order_status=?,status_reason=?,delivered_date=? where tracking_id=?";
				getJdbcTemplate().update(sql, status,reason,	dateFormat.format(today), obj.getTrackingId());
			}

			else {
				String sql = "update `order` set order_status=?,status_reason=? where tracking_id=?";
				getJdbcTemplate().update(sql, status,reason, obj.getTrackingId());
			}
			
			
		}
			String sql="insert into order_activity (tracking_id,status,activity_date,reason,activity_desc) values(?,?,?,?,?)";
				getJdbcTemplate().update(sql,obj.getTrackingId(),status,	dateFormat.format(today),reason,"status updated as " +status);
			}

	}

	@SuppressWarnings("null")
	public List<com.dataisys.taykit.model.Order> getAllOrders(HttpSession session) {
		
		
		
		
		List<Order> allOrders=new ArrayList<Order>();
		System.out.println("logged in name is"+session.getAttribute("loggedStore"));
		
		
		
		String loggedInStore=(String)session.getAttribute("loggedStore");
		User user=(User)session.getAttribute("user");
		if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")){
			System.out.println("Inside Supre_admin's condition in getAllOrders");
			//String sql="select *from `order`";
			String sql="SELECT *,b.store_name FROM `order` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id";
			return getJdbcTemplate().query(sql, new OrderMapper());
		}
		
		else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")){
			System.out.println("Inside Zonal_manager's condition in getAllOrders");
			double userId=user.getUserId();
			
			System.out.println("store id is" +userId);
			
			String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
			
			List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
			
							System.out.println("store size is" +stores.size());
			
			List<Order> orders=new ArrayList<Order>();
			
			for(Store str :stores){
				
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId;
						List <Order> orde= getJdbcTemplate().query(sql1, new OrderMapper());
					orders.addAll(orde);
			}
			
				return orders;
		}
		
		
				else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")){
					System.out.println("Inside Regional_manager's condition in getAllOrders");
			double userId=user.getUserId();
			
			System.out.println("store id is" +userId);
			
			String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";
			
			List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
			
							System.out.println("store size is" +stores.size());
			
			List<Order> orders=new ArrayList<Order>();
			
			for(Store str :stores){
				
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId;
						List <Order> orde= getJdbcTemplate().query(sql1, new OrderMapper());
					orders.addAll(orde);
			}
			
				return orders;
		}
		
				else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")){
					System.out.println("Inside Hub_manager's condition in getAllOrders");

					double userId=user.getUserId();
					
					System.out.println("store id is" +userId);
					
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";
					
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					
									System.out.println("store size is" +stores.size());
					
					List<Order> orders=new ArrayList<Order>();
					
					for(Store str :stores){
						
								double storeId=str.getStoreId();
								String sql1="select *from `order` where store_id="+storeId;
								List <Order> orde= getJdbcTemplate().query(sql1, new OrderMapper());
							orders.addAll(orde);
					}
					
						return orders;
				}
		
		
		
				else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")){
					System.out.println("Inside Store_admin's condition in getAllOrders");

					//double userId=user.getUserId();
					double storeId=(Double)session.getAttribute("storeId");
					
					
						
								//double storeId=str.getStoreId();
								String sql1="select *from `order` where store_id="+storeId;
								List <Order> orde= getJdbcTemplate().query(sql1, new OrderMapper());
								
								return orde;
					
				}
				
		
		
		
		
		
		
		else
		{
		Store store=storeDao.getStoreByAccountNo(loggedInStore);
		String storeName=store.getName();
		System.out.println("reporting store name"+storeName);
		double storeId=store.getStoreId();
		/*String sql1="select * from `store` where account_no=?";
		List<Store> stores=getJdbcTemplate().query(sql1,new Object[] {loggedInStore },new StoreRowMapper());
		*/
		
		
		System.out.println("inside getAll ordersd");
		String sql = "select store_id,store_name,reporting_store from `store` where reporting_store=?";
		System.out.println("reporting store query"+sql);
		
		List<Store> stores=getJdbcTemplate().query(sql, new Object[] {storeName },new ReportingStoreRowMapper());
		
		System.out.println("size of stores"+stores.size());
		
		String sqlOrders="select *from `order` where  store_id="+storeId;
		
		System.out.println("query" +sqlOrders);
		List<Order> ord=getJdbcTemplate().query(sqlOrders,new OrderMapper());
		
		/*System.out.println("orders"+ord);*/
		allOrders.addAll(ord);
		for(Store s:stores){
			
			String reportingStore=s.getReportingStore();
		double storeID=s.getStoreId();
			String storeNames=s.getName();
			System.out.println("reporting stores"+reportingStore);
			String storeAccno=storeDao.getStoreAccNo(storeNames);
			//double reportingStroreId=storeDao.getStoreIdByName(reportingStore);
			System.out.println("reporting store accno is"+storeAccno);
			String sqlstoreOrders="select *from `order` where store_id=?";
			List<Order> order=getJdbcTemplate().query(sqlstoreOrders,new Object[] {storeID },new OrderMapper());
			System.out.println("The size of orders in for loop is: "+order.size());
				allOrders.addAll(order);
			
			
		}
		
		}
		System.out.println("size");

		return allOrders;
	}
	public List<Order> getAllDeliveredOrders() {
		String sql = "select * from `order` where order_status=\"Delivered\" OR order_status=\"Delivered1\"";

		return getJdbcTemplate().query(sql, new OrderMapper());
		}

	public void inwardOrder(StockInward obj,HttpSession session) {
		// String storeName=storeDao.getStoreNameById(obj.getStoreId());
		
		String flag="S";
		double storeId=0;
		String storeName = obj.getStoreName();
		
		User ut=(User)session.getAttribute("user");
		if(ut.getUserType().equalsIgnoreCase("Store Admin") || ut.getUserType().equalsIgnoreCase("Store Executive"))
		{
    		
    		flag="S";
    		 storeId = storeDao.getStoreIdByName(storeName);
    		
		}
    	else if(ut.getUserType().equalsIgnoreCase("Hub Admin") || ut.getUserType().equalsIgnoreCase("Hub Executive"))
		{
    		storeId =hubdao.getHubIdByHubName(storeName) ;//storeDao.getStoreIdByName(storeName);
    		flag="H";
    		
		}
		
		// String orderType="Delivery";
		com.dataisys.taykit.model.Order order = getOrders(obj.getTrackingId());
		if (order.getStoreId() == obj.getStoreId()
				&& order.getOrderType() != null) {

			if (order.getOrderType().equalsIgnoreCase("Delivery")) {
			
				String sql1 = "update `order` set stock_flag=?,order_status=?,location=?,location_flag=? where tracking_id=?";
				// getJdbcTemplate().update(sql1,"Available","Picked Up for Delivery",obj.getTrackingId());
			//the last query---->//getJdbcTemplate().update(sql1, "Available",obj.getOrderStatus(),obj.getStoreName(), obj.getTrackingId());
				getJdbcTemplate().update(sql1, "Available","product at central ware house",storeId,flag, obj.getTrackingId());
			
			} else {
				String sql2 = "update `order` set stock_flag=?,order_status=?,location=?,location_flag=? where tracking_id=?";
				// getJdbcTemplate().update(sql2,"Available","Availaible for Pickup @"+storeName,obj.getTrackingId());
				/*getJdbcTemplate().update(sql2, "Available",obj.getOrderStatus(),obj.getStoreName(), obj.getTrackingId());*/
				getJdbcTemplate().update(sql2, "Available","product at central ware house",storeId,flag, obj.getTrackingId());
			}
		} else {

			String sql2 = "update `order` set stock_flag=?,order_status=?,location=?,location_flag=? where tracking_id=?";
			// getJdbcTemplate().update(sql2,"Available","Availaible for Pickup @"+storeName,obj.getTrackingId());
			getJdbcTemplate().update(sql2, "Available",obj.getOrderStatus(),storeId,flag, obj.getTrackingId());
		}
		String sq="insert into order_activity (to_location,tracking_id,status,activity_desc,activity_date) value (?,?,?,?,?)";
		//getJdbcTemplate().update(sq,storeId,obj.getTrackingId(),"Recived from retailer","recieved At "+storeId,new Date());
		getJdbcTemplate().update(sq,storeId,obj.getTrackingId(),obj.getOrderStatus(),"recieved At "+obj.getStoreName(),new Date());
		
		System.out.println("Done");
	}

	public void transferStock(StockOutward obj) {
		double fromstoreId = storeDao.getStoreIdByName(obj.getFromLocation());
		double tostoreId = storeDao.getStoreIdByName(obj.getToLocation());
		Order order=(Order) getOrders(obj.getTrackingId());
		if(order.getStoreId()==tostoreId){
			String sql1="update `order` set order_status=?,location=? where tracking_id=?";
			getJdbcTemplate().update(sql1, "Parcel available for collection at store "+obj.getToLocation(),obj.getToLocation(),obj.getTrackingId());
			String sq = "insert into order_activity (to_location,from_location,tracking_id,status,activity_desc,activity_date) value (?,?,?,?,?,?)";
			getJdbcTemplate().update(sq,tostoreId,fromstoreId,obj.getTrackingId(),"Parcel available for collection at store "+obj.getToLocation(),"Transfering from " + obj.getFromLocation()+ " Transfering to " + obj.getToLocation(), new Date());
		
		}else{
			String sql2="update `order` set order_status=?,location=? where tracking_id=?";
			getJdbcTemplate().update(sql2, "In Transit",obj.getToLocation(),obj.getTrackingId());
			String sq = "insert into order_activity (to_location,from_location,tracking_id,status,activity_desc,activity_date) value (?,?,?,?,?,?)";
			getJdbcTemplate().update(sq,tostoreId,fromstoreId,obj.getTrackingId(),"In Transit","Transfering from " + obj.getFromLocation()+ " Transfering to " + obj.getToLocation(), new Date());
		
		}
		}

	public void assignDeliveryBoy(AssignDeliveryBoy obj) {

		double fromstoreId = storeDao.getStoreIdByName(obj.getFromLocation());
		DeliveryBoys boy = deliveryBoyDao.DeliveryBoyId(obj.getAssignTo());
		String sql1="select attempts from `order` where tracking_id=?";
		
		int attempts=getJdbcTemplate().queryForInt(sql1, obj.getTrackingId());
		int numAttempts=0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dbAssignedDate = new Date();
		System.out.println(dateFormat.format(dbAssignedDate));
		if(attempts>0){
		numAttempts=attempts+1;
		}else{
		numAttempts=1;
		}
		String sql = "update `order` set order_status=?,assigned_to=?,location=?,attempts=?,delivery_boy_assign_date=?,runsheet_number=? where tracking_id=?";
		getJdbcTemplate().update(sql, "Picked Up for Delivery",obj.getAssignTo(),"OFD",numAttempts,dateFormat.format(dbAssignedDate),obj.getRunSheetNumber(),obj.getTrackingId());
		numAttempts=0;
		String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";
		//*******adding runsheet dump starts**********//
		
		String sql3="insert into runsheetdump values(?,?,?,?)";
		getJdbcTemplate().update(sql3,obj.getRunSheetNumber(),obj.getTrackingId(),obj.getAssignTo(),dateFormat.format(dbAssignedDate));
		System.out.println("inserted into runsheet dump");
		//*******adding runsheet dump ends**********//
		
		getJdbcTemplate().update(sql2,"Assigned to "+obj.getAssignTo(),fromstoreId,"Out for delivery", obj.getTrackingId(), new Date());
		String queryForOrder="select *from `order` where tracking_id=?";
		Order ord=getJdbcTemplate().queryForObject(queryForOrder,new Object[] { obj.getTrackingId()},new OrderMapper());
		OtpService otp=new OtpService();
		System.out.println("befor sending otp");
		otp.sendingOTPToCustomer(ord);
		
		}
	
	public List<com.dataisys.taykit.model.Order> getStoreUnaasigned() {
		String sql = "select * from `order` where store_id is NULL";

		return getJdbcTemplate().query(sql, new OrderMapper());

	}

	public void updateOrderStatus(com.dataisys.taykit.model.Order object) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date orderStatusDate = new Date();
		if(object.getOrderStatus().equalsIgnoreCase("scheduled delivery")){
			String sql = "update `order` set order_status=? ,status_reason=?, delivery_date=?,recent_status_date=?,location=? where tracking_id=?";
			String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
			getJdbcTemplate().update(sql, object.getOrderStatus(),object.getReason(),object.getDeliveryDate(),dateFormat.format(orderStatusDate),"",object.getTrackingId());
			getJdbcTemplate().update(sql2,"status updated as " + object.getOrderStatus() +" on "+object.getDeliveryDate(),object.getReason(),object.getOrderStatus(), object.getTrackingId(), new Date());

			
		}
		if(object.getOrderStatus().equalsIgnoreCase("delivered")){

		
			
			String sql = "update `order` set order_status=? ,status_reason=?, delivered_date=?,recent_status_date=?,location=? where tracking_id=?";
		String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql, object.getOrderStatus(),object.getReason(),new Date(),dateFormat.format(orderStatusDate),"",object.getTrackingId());
		getJdbcTemplate().update(sql2,"status updated as " + object.getOrderStatus(),object.getReason(),object.getOrderStatus(), object.getTrackingId(), new Date());

		}
		else
		{
		String sql = "update `order` set order_status=?,status_reason=?,recent_status_date=? where tracking_id=?";
		String sql2 = "insert into order_activity (activity_desc,status,reason,tracking_id,activity_date) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql, object.getOrderStatus(),object.getReason(),dateFormat.format(orderStatusDate),object.getTrackingId());
		getJdbcTemplate().update(sql2,
		"status updated as " + object.getOrderStatus(),object.getReason(),
		object.getOrderStatus(), object.getTrackingId(), new Date());
		}

		}

	public List<Order> getRunSheetDeliveries(Order order) {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dbassignedDate = new Date();
		//String sql="SELECT * FROM `order` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered' AND runsheet_number = 'NONE'";
		
		System.out.println("runsheet number is " +order.getRunSheetNumber());
		
		String sql="select *from `order` where runsheet_number=?";
		//List<Order> orders=getJdbcTemplate().query(sql,new Object[] { order.getAssignedTo(),dateFormat.format(dbassignedDate) },new OrderMapper());
		List<Order> orders=getJdbcTemplate().query(sql,new Object[] { order.getRunSheetNumber()},new OrderMapper());

		System.out.println("size of list is" +orders.size());
		return orders;
	}
	
	public List<com.dataisys.taykit.model.OrderActivity> getAllOrderActivities(String trackId) {
        String sql="select * from `order_activity` where tracking_id=? order by activity_id desc";
		
		return getJdbcTemplate().query(sql,new Object[]{trackId} ,new OrderActivityMapper());
	}
	
	/********************************************************** Code by Kishore ***************************************************************/
	public List<Order> getOrdersByStoreId(String storeId) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dbAssignedDate = new Date();
		System.out.println("Formatted Date is: "+dateFormat.format(dbAssignedDate));
		
		Store store = storeDao.getStoreByAccountNo(storeId);
		List<Order> orders = null;
		if(store != null) {
			//String sql = "SELECT * FROM `order` WHERE store_id = ? AND stock_flag = ? AND order_status != ? AND recent_status_date IS NOT NULL AND recent_status_date != ?"; //Original Code
			String sql = "SELECT * FROM `order` WHERE store_id = ? AND pod_flag = ? AND (order_status != ? AND order_status NOT LIKE ?) AND (recent_status_date IS NULL OR recent_status_date != ?)";
			/*orders = getJdbcTemplate().query(sql,new Object[]{storeId,"Available","Delivered"},new OrderMapper());*/
			orders = getJdbcTemplate().query(sql,new Object[]{store.getStoreId(),"Available","Delivered","Order Cancelled%",dateFormat.format(dbAssignedDate)},new OrderMapper());
		} else {
			System.err.println("Store is null in getOrdersByStoreId in OrderDaoImpl");
		}
		System.out.println("The length of getOrdersByStoreId in OrderDaoImpl is: "+orders.size());
		//System.out.println("The RecentStatusDate is: "+orders.get(1).getRecentStatusDate()+" And the new date is: "+new Date());				
		
		return orders;
	}
			
			//new
		public List<Order> getOrdersForInwardByStoreId(String storeId) {
			Store store = storeDao.getStoreByAccountNo(storeId);
			List<Order> orders = null;
			if(store != null) {	
				String sql = "SELECT * FROM `order` WHERE store_id = ? AND (order_status != ? AND order_status NOT LIKE ?) AND pod_flag != ?";
				/*orders = getJdbcTemplate().query(sql,new Object[]{storeId,orderType,"Available","Delivered"},new OrderMapper());*/
				orders = getJdbcTemplate().query(sql,new Object[]{store.getStoreId(),"Delivered","Order Cancelled%","Available"},new OrderMapper());
			} else {
				System.err.println("Store is null in getOrdersForInwardByStoreId in OrderDaoImpl");
			}
			return orders;
		}

			
			
			public List<Order> getOrdersByStoreId(String storeId, String orderType) {	
				
				Store store = storeDao.getStoreByAccountNo(storeId);			
				List<Order> orders = null;
				if(store != null) {			
					String sql = "SELECT * FROM `order` WHERE store_id = ? AND order_type = ? AND stock_flag = ? AND order_status != ?";
					/*orders = getJdbcTemplate().query(sql,new Object[]{storeId,orderType,"Available","Delivered"},new OrderMapper());*/
					orders = getJdbcTemplate().query(sql,new Object[]{store.getStoreId(),orderType,"Available","Delivered"},new OrderMapper());
				} else {
					System.err.println("Store is null in getOrdersByStoreId in OrderDaoImpl");
				}
				return orders;
			}
			
			public void updateOrderDelivery(String trackingId,String orderStatus,String paymentStatus, String deliveredGeoLocation, String rRNo) {
				System.out.println("*****Inside UpdateOrderDelivery*****");
				String sql="update `order` set order_status=?,delivered_date = ?,payment_status = ?,recent_status_date = ?,payment_reference = ? where tracking_id=?";
				
				java.util.Date utilDate = new java.util.Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(utilDate);
				cal.set(Calendar.MILLISECOND, 0);
				String orderStatusSql = "select order_status from `order` where tracking_id=?";
				String orderStatusSqlData=getJdbcTemplate().queryForObject(orderStatusSql, new Object[]{trackingId},String.class);
				System.out.println("Order Sataus in updateOrder Delivery is: "+orderStatusSqlData);
				System.out.println(!((orderStatusSqlData).trim().equalsIgnoreCase("Delivered") || (orderStatusSqlData).trim().contains("Order cancelled")));
				
				if(!((orderStatusSqlData).trim().equalsIgnoreCase("Delivered") || (orderStatusSqlData).trim().contains("Order cancelled"))) {
					System.out.println("Inside if");
					getJdbcTemplate().update(sql,orderStatus,new Date(),paymentStatus,new Date(),rRNo,trackingId);
					String sql2 = "insert into order_activity (activity_desc,status,tracking_id,activity_date,delivered_geolocation) values(?,?,?,?,?)";
					getJdbcTemplate().update(sql2,"status updated as " + orderStatus,
							orderStatus, trackingId, new Date(), deliveredGeoLocation);
				} else {
					System.out.println("Data Can't be updated");
				}
				
				//getJdbcTemplate().update(sql,orderStatus,new java.sql.Timestamp(cal.getTimeInMillis()),paymentStatus,trackingId);
				//getJdbcTemplate().update(sql,orderStatus,new Date(),paymentStatus,trackingId);	
			}
			
			
			public void updateOrderDelivery(String trackingId,String orderStatus,String paymentStatus) {
				String sql="update `order` set order_status=?,delivered_date = ?,payment_status = ?,recent_status_date = ? where tracking_id=?";
				
				java.util.Date utilDate = new java.util.Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(utilDate);
				cal.set(Calendar.MILLISECOND, 0);
				
				String orderStatusSql="select order_status from `order` where tracking_id=?";				
				String orderStatusSqlData=getJdbcTemplate().queryForObject(orderStatusSql, new Object[]{trackingId},String.class);
				System.out.println("order status from db is "+orderStatusSqlData);
				
				if(!((orderStatusSqlData).trim().equalsIgnoreCase("Delivered") || (orderStatusSqlData).trim().contains("Order cancelled"))) {					
					getJdbcTemplate().update(sql,orderStatus,new Date(),paymentStatus,new Date(),trackingId);
					
					String sql2 = "insert into order_activity (activity_desc,status,tracking_id,activity_date) values(?,?,?,?)";
					getJdbcTemplate().update(sql2,"status updated as " + orderStatus,
							orderStatus, trackingId, new Date());			
					
				} else {
					System.out.println("No updatation has to be done in updateOrderDelivery");
				}
				
				//getJdbcTemplate().update(sql,orderStatus,new java.sql.Timestamp(cal.getTimeInMillis()),paymentStatus,trackingId);
				
			}
			
			//Added on 08-12-2015
			public void podLevelInward(StockInward obj) {
				System.out.println("Inside PodLevelInward");
				// String storeName=storeDao.getStoreNameById(obj.getStoreId());
				String storeName = obj.getStoreName();
				double storeId = storeDao.getStoreIdByName(storeName);
				// String orderType="Delivery";
				com.dataisys.taykit.model.Order order = getOrders(obj.getTrackingId());
				if (order.getStoreId() == obj.getStoreId()
						&& order.getOrderType() != null) {

					if (order.getOrderType().equalsIgnoreCase("Delivery")) {
						//
						String sql1 = "update `order` set stock_flag=?,pod_flag=?,order_status=?,location=? where tracking_id=?";
						// getJdbcTemplate().update(sql1,"Available","Picked Up for Delivery",obj.getTrackingId());
						getJdbcTemplate().update(sql1, "Available","Available",obj.getOrderStatus(),obj.getStoreName(), obj.getTrackingId());

					} else {
						String sql2 = "update `order` set stock_flag=?,pod_flag=?,order_status=?,location=? where tracking_id=?";
						// getJdbcTemplate().update(sql2,"Available","Availaible for Pickup @"+storeName,obj.getTrackingId());
						getJdbcTemplate().update(sql2, "Available","Available",obj.getOrderStatus(),obj.getStoreName(), obj.getTrackingId());
					}
				} else {

					String sql2 = "update `order` set stock_flag=?,pod_flag=?,order_status=?,location=? where tracking_id=?";
					// getJdbcTemplate().update(sql2,"Available","Availaible for Pickup @"+storeName,obj.getTrackingId());
					getJdbcTemplate().update(sql2, "Available","Available",obj.getOrderStatus(),obj.getStoreName(), obj.getTrackingId());
				}
				String sq="insert into order_activity (to_location,tracking_id,status,activity_desc,activity_date) value (?,?,?,?,?)";
				//getJdbcTemplate().update(sq,storeId,obj.getTrackingId(),"Recived from retailer","recieved At "+storeId,new Date());
				getJdbcTemplate().update(sq,storeId,obj.getTrackingId(),obj.getOrderStatus(),"recieved At "+obj.getStoreName(),new Date());
			}
			

			public void updateOtp(String trackingId, String otp) {
				System.out.println("Inside updateOtp");
				System.out.println("Tracking Id: "+trackingId+" OTP: "+otp);
				String sql = "UPDATE `order` set otp = ? WHERE tracking_id = ?";
				getJdbcTemplate().update(sql, otp, trackingId);
				System.out.println("Successfully updated the otp");			
			}
			
			public void updateAssign(List<com.dataisys.taykit.model.Order> list,
					String assignId, String userType) {
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date dbAssignedDate = new Date();
				System.out.println(dateFormat.format(dbAssignedDate));
				
				if (userType.trim().equalsIgnoreCase("DELIVERY_BOY")) {
					System.out.println("Inside  Delivery_boy condition");
					//Added on 05092015
					
					User user = userDao.getUserByAccNumber(assignId);
					System.out.println("User: "+user);
					//End
					if(user != null) {
						//changing in code
						System.out.println("Inside if user != null and userName is: "+user.getUserName());
						String sql="update `order` set assigned_to=?, order_status = ?, location=?, attempts=?, delivery_boy_assign_date=? where tracking_id=?";

						Iterator<com.dataisys.taykit.model.Order> itr=list.iterator();
						while(itr.hasNext()) {
							com.dataisys.taykit.model.Order obj=itr.next();
							
							//for attempts
							String sql1="select attempts from `order` where tracking_id=?";
							int attempts=getJdbcTemplate().queryForInt(sql1, obj.getTrackingId());
							int numAttempts=0;
							if(attempts>0){
								numAttempts=attempts+1;
							} else{
								numAttempts=1;
							}				
							getJdbcTemplate().update(sql, user.getUserName(), "Out For Delivery", "OFD",numAttempts, dateFormat.format(dbAssignedDate), obj.getTrackingId());
							
							String sql2 = "insert into order_activity (activity_desc,from_location,status,tracking_id,activity_date) values(?,?,?,?,?)";
							getJdbcTemplate().update(sql2,"Assigned to "+assignId,obj.getStoreId(),"Out for delivery", obj.getTrackingId(), new Date());
						}
					} else {
						System.err.println("User is empty in OrderDaoImpl");
					}
				}
				
				if (userType.trim().equalsIgnoreCase("POD_USER")) {
					
					//Added on 05092015
					User user = userDao.getUserByAccNumber(assignId);
					//End
					if(user != null) {
						String sql="update `order` set assigned_to=? where tracking_id=?";
						Iterator<com.dataisys.taykit.model.Order> itr=list.iterator();
						while(itr.hasNext()) {
							com.dataisys.taykit.model.Order obj=itr.next();
							getJdbcTemplate().update(sql, user.getUserName(), obj.getTrackingId());
						}
					} else {
						System.err.println("User is null in OrderDaoImpl");
					}
					
				}				
			}
			
			public List<com.dataisys.taykit.model.Order> getAllOrdersByContactNumber(String contactNumber) {		
				String sql = "SELECT * FROM `order` WHERE customer_mobile = ?";
				try {
					return getJdbcTemplate().query(sql, new Object[] { contactNumber }, new OrderMapper());
				} catch (Exception e) {
					return null;
				}		
			}
			
			public void updateDeliveredLocationLatLongsInOrder(Order order) {
				
				System.out.println("Inside updateDeliveredLocationLatLongsInOrder");
				
				//It's an Original Code
				/*String sql = "UPDATE `order` SET delivered_location_latlongs = ? WHERE tracking_id = ?";
				getJdbcTemplate().update(sql, order.getDeliveredLocationLatLongs(), order.getTrackingId());	*/
				String sql = "INSERT INTO location_storing (tracking_id,geo_location) values(?,?)";
				getJdbcTemplate().update(sql, order.getDeliveredLocationLatLongs(), order.getTrackingId());
				
			} 
			

			public void getRunsheetNumber(String userNames, String randomNumbers) {
				// TODO Auto-generated method stub
				
				
			
				/*System.out.println("while updating");
				java.util.Date utilDate = new java.util.Date();*/
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				System.out.println("delivery boy"+dateFormat.format(date));
				
				
				String sql="select runsheet_number,tracking_id from `order` where assigned_to=? and delivery_boy_assign_date=? and runsheet_number='NONE'";
				List <Order> ords=getJdbcTemplate().query(sql,new Object[]{userNames,dateFormat.format(date)},new RandomTestRowMapper());
					for(Order order:ords){
						
						System.out.println("runsheet numbers are"+order.getRunSheetNumber());
						if(order.getRunSheetNumber().equalsIgnoreCase("NONE")){

							System.out.println("inside if");
							System.out.println("tracking id is"+order.getTrackingId());
							
							
							Order o=new Order();
							o.setRunSheetNumber(randomNumbers);
							o.setAssignedTo(userNames);
							
							try{
								String status="OPEN";
								getJdbcTemplate().update("update `order` set runsheet_number="+"\""+randomNumbers+"\""+"," +"runsheet_status="+"\""+status+"\""  +"where assigned_to="+"\""+userNames+"\"" +"and delivery_boy_assign_date="+"\""+dateFormat.format(date)+"\""+"and tracking_id="+"\""+order.getTrackingId()+"\"");
								
							
						System.out.println("after updating");
							}catch(Exception e){
								System.out.println(e.getMessage());
							}
							
						
							
						}
						else{
							
							System.out.println("run sheet number already availabale");
						}
					}
				
				
				
				
				
			}

			public List<OrderActivity> getCommentsId(String trackId1,String idType) {
				/*	String sql="select *from order_activity where tracking_id=? order by activity_id desc";
				List<OrderActivity> od=getJdbcTemplate().query(sql,new Object[]{trackId1}, new OrderActivityMapper());
				return od;*/
				//return null;
				
				
//String sql="SELECT Order.order_type,order.customer_address,order.order_status,order_activity.updated_on,order_activity.status,order_activity.reason FROM `Order` INNER JOIN order_activity ON order.tracking_id=order_activity.tracking_id where order_activity.tracking_id='"+trackId1+"'";
//String sql="SELECT Order.order_type,order.customer_address,order.order_status,order_activity.updated_on,order_activity.status,order_activity.reason FROM `Order_activity` INNER JOIN `order` ON order.tracking_id=order_activity.tracking_id where order_activity.tracking_id='"+trackId1+"'"; 
				//String sql="SELECT st.store_name,oa.updated_on,oa.status,oa.reason FROM `order_activity` oa INNER JOIN store st ON st.store_id=oa.from_location where oa.tracking_id='"+trackId1+"'";
//String sql="select st.store_name,oa.status from `order_activity` oa,store st where tracking_id='"+trackId1+"' and from_location=st.store_id";				

				String sql="select st.store_name,oa.activity_desc,oa.updated_on,oa.reason from `order_activity` oa,store st where tracking_id='"+trackId1+"'  and from_location=st.store_id";
				String sql2="SELECT st.store_name,oa.activity_desc,oa.updated_on, oa.reason FROM `order_activity` oa INNER JOIN store st ON st.store_id=oa.from_location where oa.tracking_id=(select tracking_id from `order` where order_id='"+trackId1+"')";
				System.out.println("sql1 : "+sql);
				System.out.println("sql2 : "+sql2);
				String finalQuery="";
				if(idType.equalsIgnoreCase("Tracking Id"))
				{
					finalQuery=sql;
				}
				else
				{
					finalQuery=sql2;
				}
return getJdbcTemplate().query(finalQuery,new ResultSetExtractor<List<OrderActivity>>(){
	
public List<OrderActivity> extractData(ResultSet rs) throws SQLException,
			DataAccessException {System.out.println("inside");
		List<OrderActivity> list=new ArrayList<OrderActivity>(); 
		System.out.println("inside");
        while(rs.next()){  
        	OrderActivity e=new OrderActivity();  
       //  e.setActivityDate(rs.getDate("activityDate"));
e.setStore_name(rs.getString("store_name"));
         e.setUpdatedOn(rs.getTimestamp("updated_on"));
         e.setStatus(rs.getString("activity_desc"));
         e.setReason(rs.getString("reason"));
        list.add(e);  
        System.out.println("inside while");
        }  
        return list;
	}

    });  
  }

			public void runSheetDetails(RunsheetData runSheet) {
				// TODO Auto-generated method stub
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				System.out.println("date "+dateFormat.format(date));
				
				System.out.println(""+runSheet.getRunsheetNo());
				
				
				try{
					/*String sql="insert into runsheet(runsheet_no,total_shipment,total_prepaid,total_cod,shortfall_id,cod_status,delivery_user,date,total_cod_cash,store_runsheetno,prepaid_delivered_total,cod_delivered_total)"+
						"values(?,?,?,?,?,?,?,?,?,?,?,?)";*/
					
					String sql="update `runsheet` set runsheet_no=?,total_shipment=?,total_prepaid=?,total_cod=?,shortfall_id=?,cod_status=?,delivery_user=?,date=?,total_cod_cash=?,store_runsheetno=?,prepaid_delivered_total=?,cod_delivered_total=? ,runsheet_status=? where runsheet_no=?";
					getJdbcTemplate().update(sql,runSheet.getRunsheetNo(),runSheet.getTotalShipment(),runSheet.getTotalPrepaid(),runSheet.getTotalCodOrders(),0,runSheet.getCodStatus(),runSheet.getDeliveryBoy(),dateFormat.format(date),runSheet.getTotalCod(),runSheet.getStoreRunSheetNumber(),runSheet.getPrepaidDelivered(),runSheet.getCodDelivered(),"Active",runSheet.getRunsheetNo());
					
			
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
			}

	
			
			public List<UpdateDeliveryStatus> getRunSheets(Order order) {
				
				List<UpdateDeliveryStatus> returnList=new ArrayList<UpdateDeliveryStatus>();
				DateFormat format=new SimpleDateFormat("yyyy/MM/dd");
				String todaysDate=format.format(new Date());
				
				 String sql="select * from `order` where delivery_boy_assign_date=? and assigned_to=?  GROUP BY runsheet_number";
				List<Order> ob=getJdbcTemplate().query(sql,new Object[] {order.getDeliveryBoyAssignDate(),order.getAssignedTo()},new OrderMapper());
				
				Iterator<Order> itr=ob.iterator();
				boolean runFlag=false;
				while(itr.hasNext()){
					Order ord=itr.next();
					System.out.println("runsheets"+ord.getRunSheetNumber());
					System.out.println(ord.getRunSheetStatus()); 
					
					UpdateDeliveryStatus obj=new UpdateDeliveryStatus();
					obj.setRunsheetNumber(ord.getRunSheetNumber());
					obj.setRunsheetStatus(ord.getRunSheetStatus());
					String totalOrders="SELECT COUNT(*) FROM `order`  WHERE assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					 
					obj.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String totalPrepaid="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='PREPAID' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaid(getJdbcTemplate().queryForInt(totalPrepaid, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String prepaidPending="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status<>'Delivered' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaidPending(getJdbcTemplate().queryForInt(prepaidPending, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String prepaidDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status='Delivered' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaidDelivered(getJdbcTemplate().queryForInt(prepaidDelivered, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCOD(getJdbcTemplate().queryForInt(totalCOD, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codPending="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status<>'Delivered' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCODPending(getJdbcTemplate().queryForInt(codPending, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCODDelivered(getJdbcTemplate().queryForInt(codDelivered, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND assigned_to=? AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setCodCollected((double) getJdbcTemplate().queryForInt(codValue, order.getAssignedTo(),order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					System.out.println("FInal object"+obj);
					returnList.add(obj);

				
				
				/*List<UpdateDeliveryStatus> returnList=new ArrayList<UpdateDeliveryStatus>();
				DateFormat format=new SimpleDateFormat("yyyy/MM/dd");
				String todaysDate=format.format(new Date());
				
				 String sql="select * from `order` where delivery_boy_assign_date=?   GROUP BY runsheet_number";
				List<Order> ob=getJdbcTemplate().query(sql,new Object[] {order.getDeliveryBoyAssignDate()},new OrderMapper());
				
				Iterator<Order> itr=ob.iterator();
				boolean runFlag=false;
				while(itr.hasNext()){
					Order ord=itr.next();
					System.out.println("runsheets"+ord.getRunSheetNumber());
					System.out.println(ord.getRunSheetStatus()); 
					
					UpdateDeliveryStatus obj=new UpdateDeliveryStatus();
					obj.setRunsheetNumber(ord.getRunSheetNumber());
					obj.setRunsheetStatus(ord.getRunSheetStatus());
					String totalOrders="SELECT COUNT(*) FROM `order`  WHERE delivery_boy_assign_date=? AND runsheet_number=?";
					 
					obj.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String totalPrepaid="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='PREPAID'  AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaid(getJdbcTemplate().queryForInt(totalPrepaid, order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String prepaidPending="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status<>'Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaidPending(getJdbcTemplate().queryForInt(prepaidPending,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String prepaidDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status='Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalPrepaidDelivered(getJdbcTemplate().queryForInt(prepaidDelivered,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD'  AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCOD(getJdbcTemplate().queryForInt(totalCOD,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codPending="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status<>'Delivered'  AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCODPending(getJdbcTemplate().queryForInt(codPending,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status='Delivered'  AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setTotalCODDelivered(getJdbcTemplate().queryForInt(codDelivered,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
					obj.setCodCollected((double) getJdbcTemplate().queryForInt(codValue,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
					
					System.out.println("FInal object"+obj);
					returnList.add(obj);*/
				}
				System.out.println(returnList);
			
				return returnList;
				
				
			}

			public List<Order> getSelectedRunsheetDetails(String runsheetNumber) {
				// TODO Auto-generated method stub
				String sql="select * from `order` where runsheet_number=? and runsheet_status<>'closed' ";
				System.out.println("runsheet Number is" +runsheetNumber);
				return getJdbcTemplate().query(sql, new Object[]{runsheetNumber}, new OrderMapper());
			}

			public void updateOrdersByRunsheet(Order object) {
				
				Date today= new Date();
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
				java.util.Date utilDate = new java.util.Date();
				java.sql.Date null1 = new java.sql.Date(utilDate.getTime());
			
				String sql="update `order` set order_status=?,status_reason=? where tracking_id=?";
				String sql1 = "update `order` set order_status=?,status_reason=?, delivered_date = ? where tracking_id=?";
				//Order order = getOrdersByTrackingId(object.getTrackingId());
				
				//System.out.println("order status is" +order.getOrderStatus());
				if(object.getOrderStatus().trim().equalsIgnoreCase("Delivered")) {
					System.out.println("inside if");
					
					getJdbcTemplate().update(sql1, object.getOrderStatus(),object.getReason(),new Date(),object.getTrackingId());
				} else {	
					
					System.out.println(" inside else");
					getJdbcTemplate().update(sql, object.getOrderStatus(),object.getReason(),object.getTrackingId());
				}	
				String sql11="insert into order_activity (tracking_id,status,activity_date,reason,activity_desc) values(?,?,?,?,?)";
				getJdbcTemplate().update(sql11,object.getTrackingId(),object.getOrderStatus(),	dateFormat.format(today),object.getReason(),"status updated as " +object.getOrderStatus());
			}


			public void closeRunSheet(String runSheetNumber) {
				String sql="update `order` set runsheet_status=? where runsheet_number=?";
				getJdbcTemplate().update(sql, "CLOSE",runSheetNumber);
				
			}
			
			public List<Order> getOrdersByRunsheetId(String id) {
				String sql="select * from `order` where runsheet_number='"+id+"'";

				return getJdbcTemplate().query(sql, new OrderMapper());
				}

			public List<Order> ordersForStoreRunSheet(Order order) {
				// TODO Auto-generated method stub
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date storeAssignDate = new Date();
				String orderType=order.getOrderType();
				String sql="select * from `order` where store_runsheet is null and store_id=? and store_assign_date=? and order_type=?";
				
				List<Order> listOfStoreOrder= getJdbcTemplate().query(sql, new Object[]{order.getStoreId(),dateFormat.format(storeAssignDate),orderType},new OrderMapper());
				System.out.println(listOfStoreOrder);
				return listOfStoreOrder;
			}

			public void getRunSheetNumberForStore(double storeId,
					String runsheetNumber,String orderType) {
				// TODO Auto-generated method stub
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				System.out.println("delivery boy"+dateFormat.format(date));
				
				
				String sql="select store_runsheet,tracking_id from `order` where store_id=? and store_assign_date=? and order_type=? and store_runsheet='NONE'";
				List <Order> ords=getJdbcTemplate().query(sql,new Object[]{storeId,dateFormat.format(date),orderType},new RunsheetMapperForStore());
					for(Order order:ords){
						
						System.out.println("runsheet numbers are"+order.getRunSheetNumber());
						if(order.getStoreRunsheet().equalsIgnoreCase("NONE")){

							System.out.println("inside if");
							System.out.println("tracking id is"+order.getTrackingId());
							
							
							Order o=new Order();
							o.setStoreRunsheet(runsheetNumber);
							o.setStoreId(storeId);
							
							try{
								String status="OPEN";
								getJdbcTemplate().update("update `order` set store_runsheet="+"\""+runsheetNumber+"\""+"," +"runsheet_status="+"\""+status+"\""  +"where store_id="+"\""+storeId+"\"" +"and store_assign_date="+"\""+dateFormat.format(date)+"\""+"and tracking_id="+"\""+order.getTrackingId()+"\"");
								
							
						System.out.println("after updating");
							}catch(Exception e){
								System.out.println(e.getMessage());
							}
							
						
							
						}
						else{
							
							System.out.println("run sheet number already availabale");
						}
				
				
				
			}
			
	}

			public List<UpdateDeliveryStatus> getStoreRunsheets(StoreRunSheet obj) {

				List<UpdateDeliveryStatus> returnList=new ArrayList<UpdateDeliveryStatus>();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				double storeId=storeDao.getStoreIdByName(obj.getStoreName());
				String sql="select * from `order` where store_assign_date between ? and ? and store_id=? and order_type=? and store_runsheet IS NOT NULL group by store_runsheet ";
				List<Order> orders=getJdbcTemplate().query(sql, new Object[]{obj.getFromDate(),obj.getToDate(),storeId,obj.getOrderType()}, new OrderMapper());
				 System.out.println(orders);
					Iterator<Order> itr=orders.iterator();
					while(itr.hasNext()){
					 
						//StoreRunSheet object=new StoreRunSheet();
						UpdateDeliveryStatus obje=new UpdateDeliveryStatus();
						
						Order order=itr.next();
						obje.setRunsheetNumber(order.getStoreRunsheet());
						obje.setRunsheetStatus(order.getStoreRunsheetStatus());
						/*
						System.out.println(order.getTrackingId()+"   "+order.getStoreRunsheet());
						object.setStoreRunsheet(order.getStoreRunsheet());
						object.setStoreRunsheetStatus(order.getStoreRunsheetStatus());
						String totalOrders="select count(*) from `order` where store_runsheet=?";
						object.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders, order.getStoreRunsheet()));
						
						String prepaidOrders="select count(*) from `order` where payment_type='PREPAID' and store_runsheet=?";
						object.setTotalPrepaid(getJdbcTemplate().queryForInt(prepaidOrders,  order.getStoreRunsheet()));
						
						String prepaidValue="select sum(order_value) from `order` where payment_type='PREPAID' and store_runsheet=?";
						object.setSumPrepaid((double) getJdbcTemplate().queryForInt(prepaidValue,  order.getStoreRunsheet()));
						
						String codOrders="select count(*) from `order` where payment_type='COD' and store_runsheet=?";
						object.setTotalCod(getJdbcTemplate().queryForInt(codOrders, order.getStoreRunsheet()));
						
						String codOrdersValue="select sum(order_value) from `order` where payment_type='COD' and store_runsheet=?";
						object.setSumCod((double) getJdbcTemplate().queryForInt(codOrdersValue, order.getStoreRunsheet()));
						returnList.add(object);*/
						
						String totalOrders="select count(*) from `order` where store_runsheet=?";
						obje.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders, order.getStoreRunsheet()));
						
						String totalPrepaid="select count(*) from `order` where payment_type='PREPAID' and store_runsheet=?";
						obje.setTotalPrepaid(getJdbcTemplate().queryForInt(totalPrepaid,  order.getStoreRunsheet()));
						
						String prepaidPending="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status<>'Delivered' AND store_runsheet=?";
						obje.setTotalPrepaidPending(getJdbcTemplate().queryForInt(prepaidPending,order.getStoreRunsheet()));
						
						String prepaidDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status='Delivered' AND store_runsheet=?";
						obje.setTotalPrepaidDelivered(getJdbcTemplate().queryForInt(prepaidDelivered, order.getStoreRunsheet()));
						
						String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD' AND store_runsheet=?";
						obje.setTotalCOD(getJdbcTemplate().queryForInt(totalCOD, order.getStoreRunsheet()));
						
						String codPending="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status<>'Delivered' AND store_runsheet=?";
						obje.setTotalCODPending(getJdbcTemplate().queryForInt(codPending, order.getStoreRunsheet()));
						
						String codDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND store_runsheet=?";
						obje.setTotalCODDelivered(getJdbcTemplate().queryForInt(codDelivered, order.getStoreRunsheet()));
						
						String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND store_runsheet=?";
						obje.setCodCollected((double) getJdbcTemplate().queryForInt(codValue, order.getStoreRunsheet()));
						System.out.println();
						
						System.out.println("FInal object"+obje);
						returnList.add(obje);
					}
				 return returnList;
			
			}
			
			public List<UpdateDeliveryStatus> getStoreRunSheetsCash(StoreRunSheet obj) {
				List<UpdateDeliveryStatus> returnList=new ArrayList<UpdateDeliveryStatus>();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				double storeId=storeDao.getStoreIdByName(obj.getStoreName());
				String sql="select * from `order` where store_assign_date between ? and ? and store_id=? and order_type=? and store_runsheet IS NOT NULL group by store_runsheet ";
				List<Order> orders=getJdbcTemplate().query(sql, new Object[]{obj.getFromDate(),obj.getToDate(),storeId,obj.getOrderType()}, new OrderMapper());
				 System.out.println(orders);
					Iterator<Order> itr=orders.iterator();
					while(itr.hasNext()){
					 
						//StoreRunSheet object=new StoreRunSheet();
						UpdateDeliveryStatus obje=new UpdateDeliveryStatus();
						
						Order order=itr.next();
						obje.setRunsheetNumber(order.getStoreRunsheet());
						obje.setRunsheetStatus(order.getStoreRunsheetStatus());
						/*
						System.out.println(order.getTrackingId()+"   "+order.getStoreRunsheet());
						object.setStoreRunsheet(order.getStoreRunsheet());
						object.setStoreRunsheetStatus(order.getStoreRunsheetStatus());
						String totalOrders="select count(*) from `order` where store_runsheet=?";
						object.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders, order.getStoreRunsheet()));
						
						String prepaidOrders="select count(*) from `order` where payment_type='PREPAID' and store_runsheet=?";
						object.setTotalPrepaid(getJdbcTemplate().queryForInt(prepaidOrders,  order.getStoreRunsheet()));
						
						String prepaidValue="select sum(order_value) from `order` where payment_type='PREPAID' and store_runsheet=?";
						object.setSumPrepaid((double) getJdbcTemplate().queryForInt(prepaidValue,  order.getStoreRunsheet()));
						
						String codOrders="select count(*) from `order` where payment_type='COD' and store_runsheet=?";
						object.setTotalCod(getJdbcTemplate().queryForInt(codOrders, order.getStoreRunsheet()));
						
						String codOrdersValue="select sum(order_value) from `order` where payment_type='COD' and store_runsheet=?";
						object.setSumCod((double) getJdbcTemplate().queryForInt(codOrdersValue, order.getStoreRunsheet()));
						returnList.add(object);*/
						try{
							String codRemitted="select Cod_paid from paymentdetails where runsheet_no=?";
							String shortFall="select shortfall_amount from paymentdetails where runsheet_no=?";
							double remitted=getJdbcTemplate().queryForInt(codRemitted, order.getStoreRunsheet());
							double shortamt=getJdbcTemplate().queryForInt(shortFall, order.getStoreRunsheet());
							
							if(remitted>0){
								obje.setCodRemitted(remitted);
							}else{
								obje.setCodRemitted(0);
							}
							if(shortamt>0){
								obje.setCodShortFall(shortamt);
							}else{
								obje.setCodShortFall(0);
							}
							
							
							
							
						}catch(EmptyResultDataAccessException e){
							obje.setCodRemitted(0);
							obje.setCodShortFall(0);
						}
						
						
						String totalOrders="select count(*) from `order` where store_runsheet=?";
						obje.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders, order.getStoreRunsheet()));
						
						String totalPrepaid="select count(*) from `order` where payment_type='PREPAID' and store_runsheet=?";
						obje.setTotalPrepaid(getJdbcTemplate().queryForInt(totalPrepaid,  order.getStoreRunsheet()));
						
						String prepaidPending="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status<>'Delivered' AND store_runsheet=?";
						obje.setTotalPrepaidPending(getJdbcTemplate().queryForInt(prepaidPending,order.getStoreRunsheet()));
						
						String prepaidDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status='Delivered' AND store_runsheet=?";
						obje.setTotalPrepaidDelivered(getJdbcTemplate().queryForInt(prepaidDelivered, order.getStoreRunsheet()));
						
						String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD' AND store_runsheet=?";
						obje.setTotalCOD(getJdbcTemplate().queryForInt(totalCOD, order.getStoreRunsheet()));
						
						String codPending="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status<>'Delivered' AND store_runsheet=?";
						obje.setTotalCODPending(getJdbcTemplate().queryForInt(codPending, order.getStoreRunsheet()));
						
						String codDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND store_runsheet=?";
						obje.setTotalCODDelivered(getJdbcTemplate().queryForInt(codDelivered, order.getStoreRunsheet()));
						
						String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND store_runsheet=?";
						obje.setCodCollected((double) getJdbcTemplate().queryForInt(codValue, order.getStoreRunsheet()));
						System.out.println();
						
						System.out.println("FInal object"+obje);
						returnList.add(obje);
					}
				 return returnList;
			}

			public List<Order> getOrdersByStoreRunSheet(String storeRunsheet) {
					String sql="select * from `order` where store_runsheet=?";
				
				return getJdbcTemplate().query(sql, new Object[]{storeRunsheet}, new OrderMapper());
				 
			}

			public void closeStoreRunsheet(String runSheetNumber) {
				String sql="update `order` set store_runsheet_status=? where store_runsheet=?";
				getJdbcTemplate().update(sql, "CLOSE",runSheetNumber);
				
			}

			public List<Order> getOrdersByRunsheet(String runSheetNo) {
				// TODO Auto-generated method stub
				
				System.out.println("inside orderDao impl checking for ");
				
				String sql="";
				List<Order> orders= getJdbcTemplate().query("select *from `order` where runsheet_number=?",new Object[]{runSheetNo}, new OrderMapper());
				System.out.println("after fetching some data ");
				return orders;
			}

			public double valueForOrder(String runSheetNo) {
				// TODO Auto-generated method stub
				
				String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND runsheet_number='"+runSheetNo+"'";
				double value=((double) getJdbcTemplate().queryForInt(codValue));
				
				return value;
			}

			public void storeTempNumber(double number) {
				// TODO Auto-generated method stub
				
				String sql="update `temp` set temp_tracknum="+number;
				System.out.println("SQL in impl : "+sql);
				getJdbcTemplate().update(sql);
				
			}
			
			public void storeTempRunNumber(double number) {
				// TODO Auto-generated method stub
				
				String sql="update `temp` set temp_runsheetnum="+number;
				getJdbcTemplate().update(sql);
				
			}
			
			public double getTempNumber(){
				String sql="select temp_tracknum from temp";
				
				double tempNumber=((double) getJdbcTemplate().queryForInt(sql));
				return tempNumber;
						
			}
			
			public double getTempRunNumber(){
				String sql="select temp_runsheetnum from temp";
				
				double tempRunNumber=((double) getJdbcTemplate().queryForInt(sql));
				return tempRunNumber;
						
			}
			
			public List<Order> searchOrderByTrackingId(String trackingId) {
				// TODO Auto-generated method stub
				String forSql = "%" + trackingId + "%";
				String sql="select * from `order` where tracking_id=?";
				
				return getJdbcTemplate().query(sql, new Object[]{forSql}, new OrderMapper());
			}

			public List<Order> searchOrderByOrderId(String orderId) {
				String sql="select * from `order` where order_id=?";
				
				return getJdbcTemplate().query(sql, new Object[]{orderId}, new OrderMapper());
			}

			public List<Order> searchOrderByCustomerName(String customerName) {
				String sql="select * from `order` where customer_name=?";
				
				return getJdbcTemplate().query(sql, new Object[]{customerName}, new OrderMapper());
			}

			public List<Order> searchOrderByCustomerContact(String customerContact) {
				String sql="select * from `order` where customer_mobile=?";
				
				return getJdbcTemplate().query(sql, new Object[]{customerContact}, new OrderMapper());
			}
public List<Order> getRecentOrders() {
			  
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				 Date date=new Date();
				 GregorianCalendar cal = new GregorianCalendar();
				 cal.setTime(date);
				 cal.add(Calendar.DATE, -15);
		        
			// System.out.println("Calculated date "+dateFormat.format(cal.getTime()));
				  String sql="select * from `order` where receipt_date>=?";
				  
				  return getJdbcTemplate().query(sql, new Object[]{dateFormat.format(cal.getTime())}, new OrderMapper());
			}

public List<UpdateDeliveryStatus> getRunsheetsForDeliveryBoys(Order order) {
	// TODO Auto-generated method stub
	
	List<UpdateDeliveryStatus> returnList=new ArrayList<UpdateDeliveryStatus>();
	DateFormat format=new SimpleDateFormat("yyyy/MM/dd");
	String todaysDate=format.format(new Date());
	
	 String sql="select * from `order` where delivery_boy_assign_date=? and runsheet_number <> 'NONE' GROUP BY runsheet_number";
	List<Order> ob=getJdbcTemplate().query(sql,new Object[] {order.getDeliveryBoyAssignDate()},new OrderMapper());
	
	Iterator<Order> itr=ob.iterator();
	boolean runFlag=false;
	while(itr.hasNext()){
		Order ord=itr.next();
		System.out.println("runsheets"+ord.getRunSheetNumber());
		System.out.println(ord.getRunSheetStatus()); 
		
		UpdateDeliveryStatus obj=new UpdateDeliveryStatus();
		obj.setRunsheetNumber(ord.getRunSheetNumber());
		obj.setRunsheetStatus(ord.getRunSheetStatus());
		String totalOrders="SELECT COUNT(*) FROM `order`  WHERE delivery_boy_assign_date=? AND runsheet_number=?";
		 
		obj.setTotalOrders(getJdbcTemplate().queryForInt(totalOrders,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String totalPrepaid="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='PREPAID'  AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalPrepaid(getJdbcTemplate().queryForInt(totalPrepaid, order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String prepaidPending="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status<>'Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalPrepaidPending(getJdbcTemplate().queryForInt(prepaidPending,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String prepaidDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='PREPAID' AND order_status='Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalPrepaidDelivered(getJdbcTemplate().queryForInt(prepaidDelivered,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD'  AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalCOD(getJdbcTemplate().queryForInt(totalCOD,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String codPending="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status<>'Delivered'  AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalCODPending(getJdbcTemplate().queryForInt(codPending,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String codDelivered="SELECT COUNT(*)FROM `order` WHERE payment_type='COD' AND order_status='Delivered'  AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setTotalCODDelivered(getJdbcTemplate().queryForInt(codDelivered,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND delivery_boy_assign_date=? AND runsheet_number=?";
		obj.setCodCollected((double) getJdbcTemplate().queryForInt(codValue,order.getDeliveryBoyAssignDate(),ord.getRunSheetNumber()));
		
		System.out.println("FInal object"+obj);
		returnList.add(obj);
	}
	System.out.println(returnList);

	return returnList;
	
	
}


public List<StoreSummery> getStoreSummery(HttpSession session) {

	List<StoreSummery> returnList=new ArrayList<StoreSummery>();
	
	
	
	
/*	String sql="select * from `order` where store_id is NOT NULL and order_status<>'Delivered'";
	List<Order> orders=getJdbcTemplate().query(sql, new OrderMapper());
	 System.out.println(orders);*/
	 
	 
	  String lessThanOneDay=returnDays(1);
	  
	  System.out.println("less than one day"+lessThanOneDay);
	  String twoDaysOldDate=returnDays(2);
	  System.out.println("less than two day"+twoDaysOldDate);
	  String threeDaysOldDate=returnDays(3);
	  System.out.println("less than three day"+threeDaysOldDate);
	  String sixDaysOldDate=returnDays(6);
	  System.out.println("less than six day"+sixDaysOldDate);
	  String seveenDaysOldDate=returnDays(7);
	  System.out.println("less than seven day"+sixDaysOldDate);
	//	Iterator<Order> itr=orders.iterator();
		
		 
			
			Date today= new Date();
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
			//StoreRunSheet object=new StoreRunSheet();
			
			
			//Order order=itr.next();
			//double storeId=order.getStoreId();
			List<Store> details=storeDao.getStores(session);
			//System.out.println("details length is" +details.size());
			for(Store det:details){
				
				StoreSummery obje=new StoreSummery();
				obje.setStoreNames(det.getName());
			//	System.out.println("store names"+det.getName());
				String queryForOneDay="SELECT COUNT(*) from `order` where store_assign_date between ? and ? and store_id=? and order_status<> 'Delivered'" ; 
				obje.setOrdersLessThanOneDay(getJdbcTemplate().queryForInt(queryForOneDay,new Object[]{lessThanOneDay,dateFormat.format(today),det.getStoreId()}));
				
				String queryForTwoToThree="SELECT COUNT(*) from `order` where store_assign_date between ? and ? and store_id=? and order_status<> 'Delivered'"; 
				obje.setOrdersLessThanThreeDay(getJdbcTemplate().queryForInt(queryForTwoToThree,new Object[]{threeDaysOldDate,twoDaysOldDate,det.getStoreId()}));
				
				String queryForThreeToSix="SELECT COUNT(*) from `order` where store_assign_date between ? and ? and store_id=? and order_status<> 'Delivered'"; 
				obje.setOrderLessThansixDays(getJdbcTemplate().queryForInt(queryForThreeToSix,new Object[]{sixDaysOldDate,threeDaysOldDate,det.getStoreId()}));
				
				String queryForgreaterThan="SELECT COUNT(*) from `order` where store_assign_date < ? and store_id=? and order_status<> 'Delivered'"; 
				obje.setOrdergreaterThanSevenDays(getJdbcTemplate().queryForInt(queryForgreaterThan,new Object[]{seveenDaysOldDate,det.getStoreId()}));
				
				 returnList.add(obje);
			}
			
			
			
			
			return returnList;
			
		
	

}

public String returnDays(int days){
	
	 
	 Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		
		Calendar cal = Calendar.getInstance();
		cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -days;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				
		System.out.println("after converting"+dateFormat.format(today));
	
	
	return dateFormat.format(	today);
}

public void updateNumbers(){
	 Date today = new Date(); 
	 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	 System.out.println("asdfasdfasdfa");
	 
	 System.out.println("date format is "+dateFormat.format(today));
	 
		String sql="select *from `order` where receipt_date="+dateFormat.format(today)+"";
		List<Order> ord=getJdbcTemplate().query(sql,new OrderMapper());
		
		System.out.println("outside od"+ord.size());
		for(Order od : ord){
			
			System.out.println("inside od");
			System.out.println("befor converting" +od.getCustomerContact());
			
			
			BigDecimal bd = new BigDecimal(od.getCustomerContact());
			
				
			System.out.println("customer contact number after converting"+bd);
			
			String s="0"+bd;
			String newContactNo=s.substring(1);
			if(newContactNo.contains("+"))
			{
				//failCountNumber++;
			Double d = (Double.parseDouble(newContactNo));
				BigDecimal bd1 = new BigDecimal(d);
				System.out.println("trying to convert failed contact number"+d);
				System.out.println("Big decimal type of failed contact number"+bd1);
				//System.out.println("failed number of contacts conversion"+failCountNumber);
				String contactForFail="0"+bd1;
				String newContactForFail=contactForFail.substring(1);
				od.setCustomerContact(newContactForFail);
				
			}
			else{
				System.out.println("Contact number after removing 0 is"+newContactNo);
				od.setCustomerContact(newContactNo);
			}
			
			String sql11="update `order` set customer_mobile=? where tracking_id=?";
			
			getJdbcTemplate().update(sql11,od.getCustomerContact(),od.getTrackingId());
		}
	
		}

	
	public List<Order> DetailSummery(String storeName,String status){
	
		
		
		  String lessThanOneDay=returnDays(1);
		  
		  //System.out.println("less than one day"+lessThanOneDay);
		  String twoDaysOldDate=returnDays(2);
		 // System.out.println("less than two day"+twoDaysOldDate);
		  String threeDaysOldDate=returnDays(3);
		//  System.out.println("less than three day"+threeDaysOldDate);
		  String sixDaysOldDate=returnDays(6);
		  //System.out.println("less than six day"+sixDaysOldDate);
		 String seveenDaysOldDate=returnDays(7);
		//  System.out.println("less than seven day"+sixDaysOldDate);
		
		  List<Order> orders=null;
		  Date today= new Date();
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
			System.out.println("store name is" +storeName);
			
			
		
		double storeId=storeDao.getStoreIdByName(storeName);
		System.out.println("store Id is" +storeId);
		if(status.equalsIgnoreCase("lessThnOneDay")){
		
				String sql= "SELECT * from `order` where store_assign_date between ? and ? and store_id=? and order_status<> 'Delivered'" ;;
				return orders=getJdbcTemplate().query(sql,new Object[]{lessThanOneDay,dateFormat.format(today),storeId}, new OrderMapper());
	}else if(status.equalsIgnoreCase("lessThnTwoDay")){
		String sql= "SELECT * from `order` where store_assign_date between ? and ? and store_id=? and order_status<>'Delivered'" ;;
		return orders=getJdbcTemplate().query(sql,new Object[]{threeDaysOldDate,twoDaysOldDate,storeId}, new OrderMapper());
	}
	else if(status.equalsIgnoreCase("lessThnSixDay")){
		String sql= "SELECT * from `order` where store_assign_date between ? and ? and store_id=? and order_status<>'Delivered'" ;;
		return orders=getJdbcTemplate().query(sql,new Object[]{sixDaysOldDate,threeDaysOldDate,storeId}, new OrderMapper());
	}
	else if(status.equalsIgnoreCase("greaterThnSevenDay")){
		String sql= "SELECT * from `order` where store_assign_date < ? and store_id=? and order_status<>'Delivered'" ;;
		return orders=getJdbcTemplate().query(sql,new Object[]{seveenDaysOldDate,storeId}, new OrderMapper());
	}
	return orders;
}

	
	public void runScheduler(){
		
		Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		System.out.println("running scheduler" );
		
		Calendar cal = Calendar.getInstance();
		/*cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -days;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();*/
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				
		System.out.println("after converting"+dateFormat.format(today));
		
		String sql="update `order` set runsheet_number='NONE' where delivery_boy_assign_date< ?";
		getJdbcTemplate().update(sql,dateFormat.format(today));
	}

	public List<Order> getRecentOrdersNew(Order order) {
		// TODO Auto-generated method stub
		return null;
	}

	public void transferToRetailer(StockOutward obj) {
		// TODO Auto-generated method stub
		
	}

	public void storeRunsheetNumber(String runsheetNumber) {
		// TODO Auto-generated method stub
		Date today = new Date(); 
		System.out.println("today date is" +today);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("after converting"+dateFormat.format(today));
		
		
		
		
		String sql="insert into runsheet(runsheet_no,runsheet_generated_date)"+"values(?,?)";
		getJdbcTemplate().update(sql,runsheetNumber,dateFormat.format(today));
		
		
	}

	public List<String> listOfInactiveRunsheets() {
		// TODO Auto-generated method stub
		
		final List<String> s= new ArrayList<String>();
		
		String sql="select runsheet_no from `runsheet` where runsheet_status="+"'inactive'";
							List<String> runsheets=getJdbcTemplate().query(sql,new RowMapper<String>(){
								
								public String mapRow(ResultSet rs, int rowNum) 
                                        throws SQLException {
									s.add(rs.getString(1));
                   return rs.getString(1);
           }
							
							});
							
							
							System.out.println("size of string is" +s.size());
							return s;
							
	
	
	
	
	}

	public void setRunsheetToActive(String runSheetNumber,String status) {
		
		
		
		// TODO Auto-generated method stub
		System.out.println("before updating" +runSheetNumber);
		String sql="update `runsheet` set runsheet_status=? where runsheet_no=?";
		getJdbcTemplate().update(sql,status,runSheetNumber);
		System.out.println("after updating" );
		
	}
	
	
	//Added By Kishore for CancelledOrders on 20151202
		public List<Order> getAllCancelledOrders(HttpSession session) {
			
			List<Order> allCancelledOrders = new ArrayList<Order>();
			
			try {			
				User user = (User)session.getAttribute("user");
				
				if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
					String sql="select * from `order` WHERE order_status LIKE 'Order cancelled%'";
					allCancelledOrders = getJdbcTemplate().query(sql, new OrderMapper());
					return allCancelledOrders;	
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("Store id is" +userId);
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("Store size is" +stores.size());
					
					for(Store str :stores) {					
							double storeId=str.getStoreId();
							String sql1="select * from `order` where store_id="+storeId+" AND order_status LIKE 'Order cancelled%'";
							allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE 'Order cancelled%'";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {					
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE 'Order cancelled%'";
						allCancelledOrders= getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE 'Order cancelled%'";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
					double storeId=(Double)session.getAttribute("storeId");
					String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE 'Order cancelled%'";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					return allCancelledOrders;				
				} else {
					return null;
				}			
			} catch(Exception e) {
				System.err.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			return allCancelledOrders;
		}
		
		
		
		
		//added 12/2/2015 4:34pm
		
	public List<Order> getAllCancelledOrders(HttpSession session, CancelledOrderByData cancelledOrderByData) {
			
			List<Order> allCancelledOrders = new ArrayList<Order>();
			System.out.println("RetailerName: "+cancelledOrderByData.getRetailerName());
			System.out.println("Status: "+cancelledOrderByData.getStatus());
			System.out.println("From Date: "+cancelledOrderByData.getFromDate());
			System.out.println("To Date: "+cancelledOrderByData.getToDate());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			Date fromDate = cancelledOrderByData.getFromDate();
			GregorianCalendar calFromDate = new GregorianCalendar();
			calFromDate.setTime(fromDate);
			
			Date toDate = cancelledOrderByData.getToDate();
			GregorianCalendar calToDate = new GregorianCalendar();
			calToDate.setTime(toDate);
			
			
			try {			
				
				User user = (User)session.getAttribute("user");
				int id = (int)retailerDao.getRetailerIdByName(cancelledOrderByData.getRetailerName());
				
				if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
					String sql="select * from `order` WHERE (order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					System.out.println("Sql Statement: "+sql);
					allCancelledOrders = getJdbcTemplate().query(sql, new OrderMapper());
					return allCancelledOrders;	
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("Store id is" +userId);
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("Store size is" +stores.size());
					
					for(Store str :stores) {					
							double storeId=str.getStoreId();
							String sql1="select * from `order` where (store_id="+storeId+" AND order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
							allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where (store_id="+storeId+" AND order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {					
						double storeId=str.getStoreId();
						String sql1="select *from `order` where (store_id="+storeId+" AND order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders= getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where (store_id="+storeId+" AND order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
					double storeId=(Double)session.getAttribute("storeId");
					String sql1="select *from `order` where (store_id="+storeId+" AND order_status LIKE 'Order cancelled-%' OR order_status LIKE 'Order cancelled - %') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					return allCancelledOrders;				
				} else {
					return null;
				}			
			} catch(Exception e) {
				System.err.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			return allCancelledOrders;
		}
		
	//added 12/3/2015 2:07

	public void updateCancelledOrderStatus(CancelledOrderByData cancelledOrderByData) {
	System.out.println("Inside updateCancelledOrderStatus in OrderDaoImpl");
	List<String> trackingIds = cancelledOrderByData.getTrackingIds();
	for (String trackingId : trackingIds) {
	String sql = "UPDATE `order` set order_status = '"+cancelledOrderByData.getStatus()+"' WHERE tracking_id = '"+trackingId+"'";
	getJdbcTemplate().update(sql);
	}
	System.out.println("Updated Successfully !!!");
	}

	//added 12/3/2015 4:33pm

	public List<Order> getAllRTOConfirmedOrders(HttpSession session, CancelledOrderByData cancelledOrderByData) {
		List<Order> allCancelledOrders = new ArrayList<Order>();
		System.out.println("RetailerName: "+cancelledOrderByData.getRetailerName());
		System.out.println("Status: "+cancelledOrderByData.getStatus());
		System.out.println("From Date: "+cancelledOrderByData.getFromDate());
		System.out.println("To Date: "+cancelledOrderByData.getToDate());
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		Date fromDate = cancelledOrderByData.getFromDate();
		GregorianCalendar calFromDate = new GregorianCalendar();
		calFromDate.setTime(fromDate);
		
		Date toDate = cancelledOrderByData.getToDate();
		GregorianCalendar calToDate = new GregorianCalendar();
		calToDate.setTime(toDate);		
		
		try {			
			
			User user = (User)session.getAttribute("user");
			int id = (int)retailerDao.getRetailerIdByName(cancelledOrderByData.getRetailerName());
			
			if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
				String sql="select * from `order` WHERE order_status LIKE TRIM('RTO confirmed')  AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
				System.out.println("Sql Statement: "+sql);
				allCancelledOrders = getJdbcTemplate().query(sql, new OrderMapper());
				return allCancelledOrders;	
				
			} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
				double userId=user.getUserId();				
				System.out.println("Store id is" +userId);
				String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
				List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
				System.out.println("Store size is" +stores.size());
				
				for(Store str :stores) {					
						double storeId=str.getStoreId();
						String sql1="select * from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO confirmed') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
				}				
				return allCancelledOrders;
				
			} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
				double userId=user.getUserId();				
				System.out.println("store id is" +userId);				
				String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
				List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
				System.out.println("store size is" +stores.size());				
				for(Store str :stores) {
					double storeId=str.getStoreId();
					String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO confirmed') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
				}				
				return allCancelledOrders;
				
			} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
				double userId=user.getUserId();				
				System.out.println("store id is" +userId);				
				String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
				List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
				System.out.println("store size is" +stores.size());				
				for(Store str :stores) {					
					double storeId=str.getStoreId();
					String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO confirmed') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders= getJdbcTemplate().query(sql1, new OrderMapper());
				}				
				return allCancelledOrders;
				
			} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {				
				double userId=user.getUserId();				
				System.out.println("store id is" +userId);				
				String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";				
				List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
				System.out.println("store size is" +stores.size());
				for(Store str :stores) {
					double storeId=str.getStoreId();
					String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO confirmed') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
				}				
				return allCancelledOrders;
				
			} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
				double storeId=(Double)session.getAttribute("storeId");
				String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO confirmed') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
				allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
				return allCancelledOrders;
			} else {
				return null;
			}			
		} catch(Exception e) {
			System.err.println("Error: "+e.getMessage());
			e.printStackTrace();
		}
		return allCancelledOrders;
	}
	//added 12/3/2015 4:34pm

	public void updateReferenceNoForRTO(CancelledOrderByData cancelledOrderByData) {
	System.out.println("Inside updateCancelledOrderStatus in OrderDaoImpl");
	List<String> trackingIds = cancelledOrderByData.getTrackingIds();
	String cancelledOrderRefNo = getReferenceValue();
	for (String trackingId : trackingIds) {
	String sql = "UPDATE `order` set cancel_order_ref_no = '"+cancelledOrderRefNo+"' , order_status = 'RTO Order Dispatched' WHERE tracking_id = '"+trackingId+"'";
	getJdbcTemplate().update(sql);
	}
	System.out.println("Updated Successfully !!!");
	}
	public static String getReferenceValue() {
	List<Integer> numbers = new ArrayList<Integer>();
	   for(int i = 0; i < 10; i++) {
	       numbers.add(i);
	   }
	   Collections.shuffle(numbers);
	   String result = "";
	   for(int i = 0; i < 10; i++) {
	       result += numbers.get(i).toString();
	   }
	   return "c".concat(result);
	}


	//**********************adding partial details in runsheet table starts*********************//
	public void runSheetPartialDetails(String RunSheetNumber,String username) {
		// TODO Auto-generated method stub
		
	
			// TODO Auto-generated method stub
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			System.out.println("date "+dateFormat.format(date));
			
		//	System.out.println(""+runSheet.getRunsheetNo());
			
			
			try{
				String sql="insert into runsheet(runsheet_no,delivery_user,date)"+
					"values(?,?,?)";
				getJdbcTemplate().update(sql,RunSheetNumber,username,dateFormat.format(date));
				System.out.println("Data inserted");
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		}
		//*********qamar code*************adding partial details in runsheet table ends*********************//	

	//qamar code
		public List<Order> getRunSheetDeliveries2(String DeliveryBoyName,String runsheetNumber) {
			System.out.println("runsheetnumber in getRunSheetDeliveries2 : "+runsheetNumber);
			// TODO Auto-generated method stub
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date dbassignedDate = new Date();
		//	String sql="SELECT * FROM `order` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered' AND runsheet_number = 'NONE'";
			String sql2="SELECT * FROM `order` WHERE assigned_to=? AND delivery_boy_assign_date=? AND order_status<>'Delivered'and runsheet_number = ?";
			List<Order> orders=getJdbcTemplate().query(sql2,new Object[] { DeliveryBoyName,dateFormat.format(dbassignedDate),runsheetNumber },new OrderMapper());

			System.out.println("size of list is" +orders.size());
			System.out.println("done");
			return orders;
		}

		public List<String> listOfInactiveRunsheets(String assignTo) {
			final List<String> s= new ArrayList<String>();
			
			String sql="select runsheet_no from `runsheet` where runsheet_status="+"'inactive' and delivery_user='"+assignTo+"'";
			System.out.println("Query inside list of inavtive runsheet : "+sql);					
			List<String> runsheets=getJdbcTemplate().query(sql,new RowMapper<String>(){
									
									public String mapRow(ResultSet rs, int rowNum) 
	                                        throws SQLException {
										s.add(rs.getString(1));
	                   return rs.getString(1);
	           }

									
								
								});
								
								
								System.out.println("size of string is" +s.size());
								return s;
		}

		public List<Order> getOrdersBasedOnSearch(SearchOrder searchOrder) {

			
			return null;}

		public List<Order> getAllRTODispatchedOrdersWithRefNo(
				HttpSession session, CancelledOrderByData cancelledOrderByData) {
			List<Order> allCancelledOrders = new ArrayList<Order>();
			System.out.println("RetailerName: "+cancelledOrderByData.getRetailerName());
			System.out.println("Status: "+cancelledOrderByData.getStatus());
			System.out.println("From Date: "+cancelledOrderByData.getFromDate());
			System.out.println("To Date: "+cancelledOrderByData.getToDate());
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			Date fromDate = cancelledOrderByData.getFromDate();
			GregorianCalendar calFromDate = new GregorianCalendar();
			calFromDate.setTime(fromDate);
			
			Date toDate = cancelledOrderByData.getToDate();
			GregorianCalendar calToDate = new GregorianCalendar();
			calToDate.setTime(toDate);		
			
			try {			
				
				User user = (User)session.getAttribute("user");
				int id = (int)retailerDao.getRetailerIdByName(cancelledOrderByData.getRetailerName());
				
				if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")) {				
					String sql="select * from `order` WHERE order_status LIKE TRIM('RTO Order Dispatched')  AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					System.out.println("Sql Statement: "+sql);
					allCancelledOrders = getJdbcTemplate().query(sql, new OrderMapper());
					return allCancelledOrders;	
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("Store id is" +userId);
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("Store size is" +stores.size());
					
					for(Store str :stores) {					
							double storeId=str.getStoreId();
							String sql1="select * from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
							allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("ZONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where zone_id=(select zone_id from user_zone where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("REGIONAL_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where region_id=(select region_id from user_region where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());				
					System.out.println("store size is" +stores.size());				
					for(Store str :stores) {					
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders= getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("HUB_MANAGER")) {				
					double userId=user.getUserId();				
					System.out.println("store id is" +userId);				
					String sql="select * from `store` where hub_id=(select hub_id from user_hub where account_no="+userId+")";				
					List <Store> stores=getJdbcTemplate().query(sql, new StoreRowMapper());
					System.out.println("store size is" +stores.size());
					for(Store str :stores) {
						double storeId=str.getStoreId();
						String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
						allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					}				
					return allCancelledOrders;
					
				} else if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")) {				
					double storeId=(Double)session.getAttribute("storeId");
					String sql1="select *from `order` where store_id="+storeId+" AND order_status LIKE TRIM('RTO Order Dispatched') AND retailer_id = "+id+" AND (receipt_date > '"+dateFormat.format(calFromDate.getTime())+"' AND receipt_date < '"+dateFormat.format(calToDate.getTime())+"')";
					allCancelledOrders = getJdbcTemplate().query(sql1, new OrderMapper());
					return allCancelledOrders;
				} else {
					return null;
				}			
			} catch(Exception e) {
				System.err.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			return allCancelledOrders;	
		}

		public List<Order> getOrdersBasedOnSearch1(SearchOrder searchOrder) {
String sqlWithStoreName="SELECT *,b.store_name FROM `order` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE";
			
			String sql="SELECT *,b.store_name FROM `order` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE ";
			String sql2="SELECT *,b.store_name FROM `order` AS a LEFT OUTER JOIN store AS b ON a.store_id = b.store_id WHERE ";
			
			if(searchOrder.getAssignedStoreName() != null && !searchOrder.getAssignedStoreName().isEmpty()){
				
				
				String mySql = "SELECT store_id FROM store  WHERE store_name = ?";
				String myStoreId = (String)getJdbcTemplate().queryForObject(
						mySql, new Object[] { searchOrder.getAssignedStoreName() }, String.class);
				System.out.println("store id returned is "+myStoreId);
				
				sql+="store_id="+'"' +myStoreId+'"';
				
			}
			if(searchOrder.getCustomerContact()!=null && !searchOrder.getCustomerContact().isEmpty()){
		
				if(sql==sql2){
					sql+="customer_mobile="+'"' +searchOrder.getCustomerContact()+'"' ;
					System.out.println("sql : "+sql);
				}
				else{
					sql+="AND customer_mobile="+'"' +searchOrder.getCustomerContact()+'"' ;
				}
				
			}
			
			if(searchOrder.getDeliveredFromDate()!=null&&searchOrder.getDeliveredToDate()!=null ){
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
				String dateFrom=dateFormat.format(searchOrder.getDeliveredFromDate());
				System.out.println("after converting from date"+dateFrom);
				
				
				String dateTo=dateFormat.format(searchOrder.getDeliveredToDate());
				System.out.println("after converting to date"+dateTo);
		
				if(sql==sql2){
					sql+="delivered_date>="+'"' +dateFrom+'"' +"and delivered_date<="+'"' +dateTo+'"' ;
				}
				else{
					sql+="AND delivered_date>="+'"' +dateFrom+'"' +"and delivered_date<="+'"' +dateTo+'"' ;
				}
				
			}
			
		
			if(searchOrder.getLocation()!=null && !searchOrder.getLocation().isEmpty()){
				
			
				if(sql==sql2){
					sql+="location="+'"' +searchOrder.getLocation()+'"' ;
				}
				else{
					sql+="AND location="+'"' +searchOrder.getLocation()+'"' ;
				}
				
			}
			
			
			if(searchOrder.getOrderId()!=null && !searchOrder.getOrderId().isEmpty()){
				
			
				if(sql==sql2){
					sql+="order_id="+'"' +searchOrder.getOrderId()+'"' ;
				}
				else{
					sql+="AND order_id="+'"' +searchOrder.getOrderId()+'"' ;
				}
				
			}
			
			if(searchOrder.getOrderStatus()!=null && !searchOrder.getOrderStatus().isEmpty()){
				System.out.println("inside orderStatus if ");
				
				
				if(sql==sql2){
					sql+="order_status="+'"' +searchOrder.getOrderStatus()+'"' ;
				
					System.out.println("uery is "+sql);
				}
				else{
					sql+="AND order_status="+'"' +searchOrder.getOrderStatus()+'"' ;
					System.out.println("uery is "+sql);
				}
				
			}
				
			if(searchOrder.getPaymentType()!=null && !searchOrder.getPaymentType().isEmpty()){
				
				if(sql==sql2){
					sql+="payment_type="+'"' +searchOrder.getPaymentType()+'"';
				}
				else{
					sql+="AND payment_type="+'"' +searchOrder.getPaymentType()+'"' ;
				}
				
			}
			
			if(searchOrder.getRetailerName()!=null && !searchOrder.getRetailerName().isEmpty()){
				
				
				String mySql = "SELECT retailer_id  FROM retailer  WHERE retailer_name = ?";
				String myRetailerId = (String)getJdbcTemplate().queryForObject(
						mySql, new Object[] { searchOrder.getRetailerName() }, String.class);
				
				System.out.println("retailer Id returned is "+myRetailerId);
				
				if(sql==sql2){
					sql+="retailer_id="+'"' +myRetailerId+'"' ;
				}
				else{
					sql+="AND retailer_id="+'"' +myRetailerId+'"' ;
				}
				
			}
			
		
				
			if(searchOrder.getFromDate()!=null&&searchOrder.getToDate()!=null){
			
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
				String dateFrom=dateFormat.format(searchOrder.getFromDate());
				System.out.println("after converting from date"+dateFrom);
				
				
				String dateTo=dateFormat.format(searchOrder.getToDate());
				System.out.println("after converting to date"+dateTo); 
				
				if(sql==sql2){
					sql+="receipt_date>="+'"' +dateFrom+'"'+"and receipt_date<="+'"'+dateTo+'"' ;
				}
				else{
					sql+="AND receipt_date>="+'"' +dateFrom+'"'+"and receipt_date<="+'"'+dateTo+'"' ;
				}
				
			}
				
			
			String sqlReady= sql+=";";
			System.out.println("my final query is "+sqlReady);
			
			
			return getJdbcTemplate().query(sqlReady, new OrderAdvanceSearchRowMapper());
			
			
		}

		public List<Order> getOrderDetailsById(String trackId,String idType) {
			//String sql="SELECT Order.order_type,order.customer_address,order.order_status,order_activity.updated_on,order_activity.status,order_activity.reason FROM `Order_activity` INNER JOIN `order` ON order.tracking_id=order_activity.tracking_id where order_activity.tracking_id='"+trackId1+"'"; 
System.out.println(idType);
			String sql="SELECT order_type,customer_address,order_status from `order` where tracking_id='"+trackId+"'";
String sql2="SELECT order_type,customer_address,order_status from `order` where order_id='"+trackId+"'";
String finalQuery="";
if(idType.equalsIgnoreCase("Tracking Id"))
{
	finalQuery=sql;
}
else
{
	finalQuery=sql2;
}
				System.out.println(sql);
return getJdbcTemplate().query(finalQuery,new ResultSetExtractor<List<Order>>(){
	
public List<Order> extractData(ResultSet rs) throws SQLException,
			DataAccessException {
	System.out.println("inside");
		List<Order> list=new ArrayList<Order>(); 
		System.out.println("inside");
        while(rs.next()){  
        	Order e=new Order();  
       //  e.setActivityDate(rs.getDate("activityDate"));
         e.setOrderStatus(rs.getString("order_status"));
         e.setOrderType(rs.getString("order_type"));
         e.setCustomerAddress(rs.getString("customer_address"));
         System.out.println(rs.getString("order_status"));
         System.out.println(rs.getString("order_type"));
         System.out.println(rs.getString("customer_address"));
        list.add(e);  
        System.out.println("inside while");
        }  
        return list;
	}

    });
		}

		public boolean CreatedOrderAPI(Order object) 
		{
			// Qamar's Code
			System.out.println("retailor id :"+object.getRetailerId());
			//===========================================================
			
			 String tracking=getNextTrackingId();//"D005000001";
			 System.out.println("tracking id : "+tracking);
             String firstHalf=tracking.substring(0, 3);
             String seconfHalf=tracking.substring(3);
     System.out.println(firstHalf+" : " +seconfHalf);
    
     int number1=Integer.parseInt(seconfHalf);
     System.out.println("number1 "+number1);
     int number=number1+1;
     System.out.println(number);
     StringBuilder sb = new StringBuilder();
     sb.append(number);
     
     String strI = sb.toString();
     System.out.println("converted back to string "+strI);
     String result=firstHalf.concat(strI);
     System.out.println("result is "+result);
			
			
			//===========================================================
			
			Date today= new Date();
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			boolean flag=false;
			String sql = "insert into `order`(tracking_id,order_id,order_category,order_details,"
						 + "payment_type,order_value,customer_name,customer_address,customer_email,city_name,customer_mobile,"
						 + "customer_zipcode,created_on,order_status,retailer_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			int i=getJdbcTemplate().update(sql, result,object.getOrderId(),
											object.getQuantity(),object.getCategory(),
											object.getPaymentType(),object.getOrderValue(),
											object.getCustomerName(),object.getCustomerAddress(),
											object.getCustomerEmail(),
											object.getCityName(),object.getCustomerMobile(),
											object.getCustomerZipcode(),dateFormat.format(today),"Data Received At Central Warehouse","5");
					
			if(i>0)
			{
				flag=true;
			}
			System.out.println("flag inside impl : "+flag);
			return flag;
			
		}

public String getNextTrackingId()
{
	System.out.println("inside getNextTrackingId");
	//=================================================================================
		//	System.out.println("values in impl : "+masterbag_no);
			String sql2="SELECT tracking_id FROM `order` where tracking_id like '%D0050%' "
					+ "ORDER BY tracking_id DESC LIMIT 1 ";

				System.out.println(sql2);
	return getJdbcTemplate().query(sql2,new ResultSetExtractor<String>(){

	public String extractData(ResultSet rs) throws SQLException,DataAccessException
	{
		String s="";
		System.out.println("inside");
		//List<MasterBag> list=new ArrayList<MasterBag>(); 
		System.out.println("inside");
	    while(rs.next()){  
	    s=rs.getString("tracking_id");
	    System.out.println("inside while");
	    }  
	    
	    System.out.println("value of s in getNextTrackingId : "+s);
	    return s;
	}

	});
//=================================================================================
	
}

public void updateAssignReturn(Order object, double id) {


    System.out.println("inside update Assign Return implementation assignId is"+id+" object is "+object);
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Date storeAssignDate = new Date();
    String sql = "update `order_return` set store_id=?,order_type=?,store_assign_date=? where order_id=?";
    System.out.println("query to be executed is "+sql);

    getJdbcTemplate().update(sql, id, object.getOrderType(),dateFormat.format(storeAssignDate),object.getOrderId());
    
	
}

public List<Order> getMultipleTrackingIdSearch(String id) {
	System.out.println("inside daoimpl data is "+id);
	String sql = "select * from `order` where tracking_id IN ("+id+") ";
	/*NamedParameterJdbcTemplate np=new NamedParameterJdbcTemplate(getDataSource());
	MapSqlParameterSource parameters = new MapSqlParameterSource();
	parameters.addValue("names", Arrays.asList(id));*/
	

	try {
		return getJdbcTemplate().query(sql, new OrderMapper());
	} catch (EmptyResultDataAccessException e) {
		// TODO: handle exception
		System.out.println("error is "+e);
		return null;
	}
	
}

public Order getMultipleOrderIdSearch(String id) {
	// TODO Auto-generated method stub
	return null;
}


}
