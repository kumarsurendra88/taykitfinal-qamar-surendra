package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Module;

public class ModuleRowMapper implements RowMapper<Module> {

	public Module mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Module module= new Module();
		module.setModuelId(rs.getDouble("module_id"));
		module.setModuleName(rs.getString("module_name"));
		module.setModuleCode(rs.getString("module_code"));
		
		return module;
	}

}
