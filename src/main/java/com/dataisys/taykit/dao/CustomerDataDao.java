package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.controllers.CustomerDataController;
import com.dataisys.taykit.model.CustomerData;
import com.dataisys.taykit.model.Order;

public interface CustomerDataDao {

	void saveData(CustomerData obj);

	List<CustomerData> getPrevoius(String contact);
	
	public List <Order> getOrderDetails(String ContactNo);

	public List <Order> getcustomerPastOrders(String ContactNo);
	 
}
