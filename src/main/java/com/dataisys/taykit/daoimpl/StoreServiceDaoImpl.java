package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;



import com.dataisys.taykit.rowmapper.StoreAvailableServicesRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.StoreService;
import com.dataisys.taykit.model.StoreServices;

public class StoreServiceDaoImpl extends JdbcDaoSupport implements StoreServicesDao {

	

	
	




	public List<StoreServices> getServices() {

				String sql= "SELECT * FROM `store_service_type`";
				
				List<StoreServices> services=getJdbcTemplate().query(sql,new StoreTotalServiceRowMapper());
				return services;
		
	}

	public void addServices(final List<StoreService> services) {
		  String SQL = "insert into store_services(store_id, service_type_id,service_name) values (?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   StoreService detail = services.get(i);
		   ps.setDouble(1, detail.getStoreId());
		   ps.setDouble(2, detail.getStoreServiceTypeId());
		   ps.setString(3, detail.getServiceName());
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return services.size();
		      
		                  }


		                });
        return;
		
	}

	public List<StoreService> getStoreServicesById(double id) {
		
			String sql="select * from `store_services` where store_id=?";
			return  getJdbcTemplate().query(sql, new Object[]{id}, new StoreAvailableServicesRowMapper());
			
		}

	public void updateServices(List<StoreService> providedServices, int id) {
		// TODO Auto-generated method stub
		String sql="delete from `store_services` where store_id="+id+"";
		getJdbcTemplate().execute(sql);
		addServices(providedServices);
	}
	}
	


