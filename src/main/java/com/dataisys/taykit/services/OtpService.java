package com.dataisys.taykit.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Store;

public class OtpService {
	//public static long otp=321;

	public static void main(String[] args) {
		Order order = new Order();
		Store store = new Store();
		String otppp = new OtpService().otpService(order, store);
	}
	public String otpService(Order order, Store store) {
		System.out.println("in otp service");
		String otp=sendingOTP(order, store);
		return otp;
	}

	public String sendingOTP(Order order, Store store) {
		
		try {
			
			//Code For generating random 4digit unique number as a OTP
			List<Integer> numbers = new ArrayList<Integer>();
		    for(int i = 0; i < 10; i++){
		        numbers.add(i);
		    }

		    Collections.shuffle(numbers);

		    String result = "";
		    for(int i = 0; i < 4; i++){
		        result += numbers.get(i).toString();
		    }
		    System.out.println(result);
			//End of the code of generating random numbers
			
			String password = result;
			/*String phone=order.getCustomerContact();
			String name=order.getCustomerName();
			String parcel=order.getOrderTitle();*/
			String pickupLocation="TayKitStore";
			//String url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?";
			//URL obj = new URL(url);
			URL url = new URL(null, "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?", new sun.net.www.protocol.https.Handler());
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			//con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			String geoLocation = store.getGeoLocation();
			if(geoLocation == null) {
				geoLocation = "0,0";
			}
			String address = "http://maps.google.com/?q="+geoLocation;
			
			
			String urlParameters = "feedid=352731&username=9448285891&password=ppwjp&To="+"919448285891"+"&text="+
			"Dear "+order.getCustomerName()+", thank you for choosing to COLLECT your Taykit parcel from a Taykit store, India's first try and pick centre network. You may collect your parcel from "+store.getName()+", Landmark "+store.getAddress()+", any time after 6AM. We are open 6AM to 9PM. Use this OTP "+password+" to authenticate yourself at the Taykit store. We at "+address+" are happy to help you.";

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
			}
			in.close();
			return password;
		} catch(Exception e) {
			System.err.println("Error: "+e.getMessage());
			return null;
		}		
	}
	
	
	
public String sendingOTPToCustomer(Order order) {
		
		try {
			
			System.out.println("while sending");
			
			//Code For generating random 4digit unique number as a OTP
			List<Integer> numbers = new ArrayList<Integer>();
		    for(int i = 0; i < 10; i++){
		        numbers.add(i);
		    }

		    Collections.shuffle(numbers);

		    String result = "";
		    for(int i = 0; i < 4; i++){
		        result += numbers.get(i).toString();
		    }
		    System.out.println(result);
			//End of the code of generating random numbers
			
			String password = result;
			/*String phone=order.getCustomerContact();
			String name=order.getCustomerName();
			String parcel=order.getOrderTitle();*/
			String pickupLocation="TayKitStore";
			//String url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?";
			//URL obj = new URL(url);
			URL url = new URL(null, "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?", new sun.net.www.protocol.https.Handler());
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			//con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			//String geoLocation = store.getGeoLocation();
			/*if(geoLocation == null) {
				geoLocation = "0,0";
			}
			String address = "http://maps.google.com/?q="+geoLocation;
			*/
			
			String urlParameters = "feedid=352731&username=9448285891&password=ppwjp&To="+"919448285891"+"&text="+
					"Dear "+order.getCustomerName()+", Taykit, India's first 'Try and Pick Store network, has picked up your <<>> parcel to hand over to you. Your tracking Id is "+order.getTrackingId()+". You may check the status of your parcel at www.taykit.in"; 

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
			}
			in.close();
			return password;
		} catch(Exception e) {
			System.err.println("Error: "+e.getMessage());
			return null;
		}		
	}
	
	
	
	
}