package com.dataisys.taykit.daoimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.controllers.CustomerDataController;
import com.dataisys.taykit.dao.CustomerDataDao;
import com.dataisys.taykit.model.CustomerData;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.rowmapper.CustomerDataMapper;
import com.dataisys.taykit.rowmapper.OrderMapper;

public class CustomerDataDaoImpl extends JdbcDaoSupport implements CustomerDataDao   {

	public void saveData(CustomerData obj) {
	String sql="insert into customer_data (contact,order_type,store_name,comments) values (?,?,?,?)";
	getJdbcTemplate().update(sql, obj.getContact(),obj.getOrderType(),obj.getStoreName(),obj.getComments());
		
	}

	public List<CustomerData> getPrevoius(String contact) {
		String sql="select * from customer_data where contact=?";
		
		return getJdbcTemplate().query(sql, new Object[]{contact}, new CustomerDataMapper());
	}
	
	public List <Order> getOrderDetails(String ContactNo){
		
Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		//System.out.println("Today is " + toddMMyy(today)); 
		Calendar cal = Calendar.getInstance();
		cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -7;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat.format(	today);
				
		System.out.println("after converting"+dateFormat.format(today));
		
		String sql="select *from `order` where customer_mobile='"+ContactNo+"' and receipt_date>'"+dateFormat.format(today)+"'";
			return getJdbcTemplate().query(sql,new OrderMapper());
		
	}
	
	public List <Order> getcustomerPastOrders(String ContactNo){
		
		
		Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		//System.out.println("Today is " + toddMMyy(today)); 
		Calendar cal = Calendar.getInstance();
		cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -7;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat.format(	today);
				
		System.out.println("after converting"+dateFormat.format(today));
	
		

		
		
		String sql="select *from `order` where customer_mobile='"+ContactNo+"' and receipt_date<'"+dateFormat.format(today)+"'";
		
		System.out.println("sql query is" +sql);
		return getJdbcTemplate().query(sql,new OrderMapper());
	
}
	

}
