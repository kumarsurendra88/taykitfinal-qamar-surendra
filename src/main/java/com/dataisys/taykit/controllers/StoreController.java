package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.StoreDao;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreReport;
import com.dataisys.taykit.model.User;

@RestController
public class StoreController {
	
	@Autowired StoreDao storeDaoNew ;
	@RequestMapping(value="/getStoreNameInOrderReturn",method=RequestMethod.GET)
	public List<Store> getStoreNameInOrderReturn() throws IOException{
		System.out.println("inside getStoreNameInOrderReturn inside controller");
		List<Store> getStoreNameInOrderReturn=storeDaoNew.getStoreNameInOrderReturn();
		System.out.println("response is "+getStoreNameInOrderReturn.size());
		return getStoreNameInOrderReturn;
	}

	
	@RequestMapping(value="/getStoreNameOfHub",method=RequestMethod.GET)
	public List<Store> getStoreNameOfHub(HttpSession session){
		System.out.println("inside getStoreNameOfHub controller ");
		User user=(User)session.getAttribute("user");
		System.out.println("user id is :"+user.getUserId()+"usertype : "+user.getUserType()+" user account number:"+user.getAccNumber());
		List<Store> storeName=storeDaoNew.getStoreNameOfHub(user);
		System.out.println("response is "+storeName.size());
		return storeName;		
	}
	
	@RequestMapping(value="/getTotalInward/{fromDate},{toDate},{storeName}", method=RequestMethod.GET)
	public StoreReport getTotalInward(@PathVariable("fromDate")String fromDate,@PathVariable("toDate")String toDate,@PathVariable("storeName")String storeName){
		
		System.out.println("data received are:");
		System.out.println("fromDate: "+fromDate);
		System.out.println("toDate: "+toDate);
		System.out.println("storeName: "+storeName);
		String storeId=storeDaoNew.getStoreId(storeName);
		StoreReport getTotalStoreReport=storeDaoNew.getTotalInward(storeId,fromDate,toDate);
		return getTotalStoreReport;
		
		
	}
}
