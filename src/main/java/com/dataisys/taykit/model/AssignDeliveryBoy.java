package com.dataisys.taykit.model;

public class AssignDeliveryBoy {

	private String fromLocation;
	private String assignTo;
	private String trackingId;
	private String runSheetNumber;
	
	public AssignDeliveryBoy() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AssignDeliveryBoy(String fromLocation, String assignTo,
			String trackingId) {
		super();
		this.fromLocation = fromLocation;
		this.assignTo = assignTo;
		this.trackingId = trackingId;
	}
	
	

	public String getRunSheetNumber() {
		return runSheetNumber;
	}

	public void setRunSheetNumber(String runSheetNumber) {
		this.runSheetNumber = runSheetNumber;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	@Override
	public String toString() {
		return "AssignDeliveryBoy [fromLocation=" + fromLocation
				+ ", assignTo=" + assignTo + ", trackingId=" + trackingId + "]";
	}
	
	
	
}
