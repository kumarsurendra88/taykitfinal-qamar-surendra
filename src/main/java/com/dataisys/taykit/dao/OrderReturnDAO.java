package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.mobile.model.*;

public interface OrderReturnDAO {

	public void createOrderReturn(ReturnsForAndroid orderReturn);
	public void updateOrderReturn(ReturnsForAndroid orderReturn);
	public List<ReturnsForAndroid> getAllOrderReturns();
	public List<ReturnsForAndroid> getOrderReturnByStoreId(double storeId);
}
