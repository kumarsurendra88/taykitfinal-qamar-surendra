package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.StoreServices;

public class StoreServiceRowMapper implements RowMapper<StoreServices>{
	
	
	public StoreServices mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		StoreServices service= new StoreServices();
		service.setName(rs.getString("name"));
		service.setDescription(rs.getString("service_desc"));
		service.setServiceId(rs.getDouble("service_id"));
		service.setUpdatedBy(rs.getDouble("updated_by"));
		return service;
		
	}



}
