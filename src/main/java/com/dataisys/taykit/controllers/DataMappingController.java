package com.dataisys.taykit.controllers;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.DataMappingDao;
import com.dataisys.taykit.dao.RetailerDao;
import com.dataisys.taykit.model.DataMapping;

@RestController
public class DataMappingController {

	@Autowired
	DataMappingDao mapDao;
	@Autowired
	RetailerDao retailerDao;
	@RequestMapping(value="/saveMapping/{retailerName}")
	public void saveMapping(@RequestBody List<DataMapping> list,@PathVariable("retailerName") String retailerName){
		System.out.println("SAving"+list);
		int retailerId=(int) retailerDao.getRetailerIdByName(retailerName);
		Iterator<DataMapping> itr=list.iterator();
		while(itr.hasNext()){
			DataMapping obj=itr.next();
			obj.setRetailerId(retailerId);
			
		}
		mapDao.saveMapping(list);
	}
	
	@RequestMapping(value="/updateMapping/{retailerId}")
	public void updateMapping(@RequestBody List<DataMapping> list,@PathVariable("retailerId") double retailerId){
		System.out.println("SAving"+list);
		System.out.println("Id"+retailerId);
		mapDao.updateMapping(list,retailerId);
	}
	
	@RequestMapping(value="/getMappingById/{retailerId}")
	public List<DataMapping> getMapping(@PathVariable("retailerId") int retailerId){
		 
		return mapDao.getMappingByRetailer(retailerId);
	}
	
	@RequestMapping(value="/checkMapping")
	public int checkMapping(@RequestBody String retailerName){
		int retailerId=(int) retailerDao.getRetailerIdByName(retailerName);
		System.out.println(retailerId);
		return mapDao.checkMapping(retailerId);
	}
}
