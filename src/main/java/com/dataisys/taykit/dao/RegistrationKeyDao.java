package com.dataisys.taykit.dao;

import com.dataisys.taykit.model.RegistrationKey;

public interface RegistrationKeyDao {	
	public RegistrationKey getRegkeyByAccountid(String accountNo);
	public RegistrationKey getAccountNoByRegKey(String regKey);
	public void createNewRegKey(RegistrationKey regkey);
	public void updateRegKeyByAccountNo(RegistrationKey regkey);
}