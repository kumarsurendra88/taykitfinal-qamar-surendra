package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.DataMapping;

public class DataMappingMapper implements RowMapper<DataMapping> {

	public DataMapping mapRow(ResultSet rs, int rowNum) throws SQLException {
		DataMapping obj = new DataMapping();
		obj.setMappingId(rs.getDouble("mapping_id"));
		obj.setRetailerId(rs.getDouble("retailer_id"));
		obj.setSourceColumn(rs.getString("source_column"));
		obj.setTargetColumn(rs.getString("target_column"));
		obj.setType(rs.getString("type"));
		obj.setNullValue(rs.getString("allow_null"));
		obj.setDescription(rs.getString("Description"));
		obj.setUpdatedBy(rs.getString("updated_by"));
		obj.setUpdatedOn(rs.getTimestamp("updated_on"));
		return obj;
	}

}
