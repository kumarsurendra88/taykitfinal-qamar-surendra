package com.dataisys.taykit.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Order {
	private String location;
	private String trackingId;
	private String orderId;
	private String orderTitle;
	private String orderDetails;
	private String orderType;
	private String orderCategory;
	private String orderPriority;
	private double orderProductId;
	private double activityId;
	private double revisionId;
	private double attachmentId;
	private double customerMobile;
	private String quantity;
	private String category;
	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(double customerMobile) {
		this.customerMobile = customerMobile;
	}

	private double storeId;
	private double retailerId;
	private int counter;
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	private String orderRefno;
	private String customerName;
	private String customerContact;
	private String customerEmail;
	private String customerAddress;
	private String cityName;
	private String areaName;
	private int customerZipcode;
	private String paymentType;
	private int bayNumber;
	private String orderStatus;
	private Date receiptDate;
	private Date deliveryDate;
	private String assignedTo;
	private Date deliveredDate;
	private String deliveredBy;
	private String paymentStatus;
	private String paymentReference;
	private double orderValue;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Timestamp updatedOn;
	private String sessionId;
	private String importFlag;
	private String stockFlag;
	private String country;
	private double weight;
	private String content;
	private double vat;
	private String alternaitveContactNo;
	private double acctualAmount;
	private int attempts;
	
	private String runSheetNumber;
	private String runSheetStatus;
	
	private Date deliveryBoyAssignDate;
	private Date storeAssignDate;
	private Date recentStatusDate;
	private String otp;
	private String reason;
	
	private String storeRunsheet;
	private String deliveredFlag;
	private String storeRunsheetStatus;
	private double reportingStoreId;
	//added by kishor and surandra
	private String storeName=null;
	private String cancelledOrderRef;
	public String getCancelledOrderRef() {
		return cancelledOrderRef;
	}













	public void setCancelledOrderRef(String cancelledOrderRef) {
		this.cancelledOrderRef = cancelledOrderRef;
	}













	public String getStoreName() {
		return storeName;
	}













	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}













	public double getReportingStoreId() {
		return reportingStoreId;
	}













	public void setReportingStoreId(double reportingStoreId) {
		this.reportingStoreId = reportingStoreId;
	}













	//Added By Kishore
	private String deliveredLocationLatLongs;	
	
	
	public String getDeliveredLocationLatLongs() {
		return deliveredLocationLatLongs;
	}













	public void setDeliveredLocationLatLongs(String deliveredLocationLatLongs) {
		this.deliveredLocationLatLongs = deliveredLocationLatLongs;
	}













	public String getStoreRunsheetStatus() {
		return storeRunsheetStatus;
	}













	public void setStoreRunsheetStatus(String storeRunsheetStatus) {
		this.storeRunsheetStatus = storeRunsheetStatus;
	}













	public Order() {
		super();
	}
	
	
	
	
	
	
	






	public Order(String location, String trackingId, String orderId,
			String orderTitle, String orderDetails, String orderType,
			String orderCategory, String orderPriority, double orderProductId,
			double activityId, double revisionId, double attachmentId,
			double storeId, double retailerId, String orderRefno,
			String customerName, String customerContact, String customerEmail,
			String customerAddress, String cityName, String areaName,
			int customerZipcode, String paymentType, int bayNumber,
			String orderStatus, Date receiptDate, Date deliveryDate,
			String assignedTo, Date deliveredDate, String deliveredBy,
			String paymentStatus, String paymentReference, double orderValue,
			String createdBy, Date createdOn, String updatedBy,
			Timestamp updatedOn, String sessionId, String importFlag,
			String stockFlag, String country, double weight, String content,
			double vat, String alternaitveContactNo, double acctualAmount,
			int attempts, String runSheetNumber) {
		super();
		this.location = location;
		this.trackingId = trackingId;
		this.orderId = orderId;
		this.orderTitle = orderTitle;
		this.orderDetails = orderDetails;
		this.orderType = orderType;
		this.orderCategory = orderCategory;
		this.orderPriority = orderPriority;
		this.orderProductId = orderProductId;
		this.activityId = activityId;
		this.revisionId = revisionId;
		this.attachmentId = attachmentId;
		this.storeId = storeId;
		this.retailerId = retailerId;
		this.orderRefno = orderRefno;
		this.customerName = customerName;
		this.customerContact = customerContact;
		this.customerEmail = customerEmail;
		this.customerAddress = customerAddress;
		this.cityName = cityName;
		this.areaName = areaName;
		this.customerZipcode = customerZipcode;
		this.paymentType = paymentType;
		this.bayNumber = bayNumber;
		this.orderStatus = orderStatus;
		this.receiptDate = receiptDate;
		this.deliveryDate = deliveryDate;
		this.assignedTo = assignedTo;
		this.deliveredDate = deliveredDate;
		this.deliveredBy = deliveredBy;
		this.paymentStatus = paymentStatus;
		this.paymentReference = paymentReference;
		this.orderValue = orderValue;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.sessionId = sessionId;
		this.importFlag = importFlag;
		this.stockFlag = stockFlag;
		this.country = country;
		this.weight = weight;
		this.content = content;
		this.vat = vat;
		this.alternaitveContactNo = alternaitveContactNo;
		this.acctualAmount = acctualAmount;
		this.attempts = attempts;
		this.runSheetNumber = runSheetNumber;
	}













	public String getDeliveredFlag() {
		return deliveredFlag;
	}













	public void setDeliveredFlag(String deliveredFlag) {
		this.deliveredFlag = deliveredFlag;
	}













	public String getStoreRunsheet() {
		return storeRunsheet;
	}













	public void setStoreRunsheet(String storeRunsheet) {
		this.storeRunsheet = storeRunsheet;
	}













	public String getRunSheetStatus() {
		return runSheetStatus;
	}













	public void setRunSheetStatus(String runSheetStatus) {
		this.runSheetStatus = runSheetStatus;
	}













	public String getReason() {
		return reason;
	}













	public void setReason(String reason) {
		this.reason = reason;
	}













	public String getOtp() {
		return otp;
	}













	public void setOtp(String otp) {
		this.otp = otp;
	}













	public Date getDeliveryBoyAssignDate() {
		return deliveryBoyAssignDate;
	}













	public void setDeliveryBoyAssignDate(Date deliveryBoyAssignDate) {
		this.deliveryBoyAssignDate = deliveryBoyAssignDate;
	}













	public Date getStoreAssignDate() {
		return storeAssignDate;
	}













	public void setStoreAssignDate(Date storeAssignDate) {
		this.storeAssignDate = storeAssignDate;
	}













	public Date getRecentStatusDate() {
		return recentStatusDate;
	}













	public void setRecentStatusDate(Date recentStatusDate) {
		this.recentStatusDate = recentStatusDate;
	}













	public String getRunSheetNumber() {
		return runSheetNumber;
	}






	public void setRunSheetNumber(String runSheetNumber) {
		this.runSheetNumber = runSheetNumber;
	}






	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = vat;
	}

	public String getAlternaitveContactNo() {
		return alternaitveContactNo;
	}

	public void setAlternaitveContactNo(String alternaitveContactNo) {
		this.alternaitveContactNo = alternaitveContactNo;
	}

	public double getAcctualAmount() {
		return acctualAmount;
	}

	public void setAcctualAmount(double acctualAmount) {
		this.acctualAmount = acctualAmount;
	}

	public String getStockFlag() {
		return stockFlag;
	}

	public void setStockFlag(String stockFlag) {
		this.stockFlag = stockFlag;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public String getImportFlag() {
		return importFlag;
	}

	public void setImportFlag(String importFlag) {
		this.importFlag = importFlag;
	}

	
	 
	 

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	 

	

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderTitle() {
		return orderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}
	public String getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(String orderDetails) {
		this.orderDetails = orderDetails;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderCategory() {
		return orderCategory;
	}
	public void setOrderCategory(String orderCategory) {
		this.orderCategory = orderCategory;
	}
	public String getOrderPriority() {
		return orderPriority;
	}
	public void setOrderPriority(String orderPriority) {
		this.orderPriority = orderPriority;
	}
	public double getOrderProductId() {
		return orderProductId;
	}
	public void setOrderProductId(double orderProductId) {
		this.orderProductId = orderProductId;
	}
	public double getActivityId() {
		return activityId;
	}
	public void setActivityId(double activityId) {
		this.activityId = activityId;
	}
	public double getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(double revisionId) {
		this.revisionId = revisionId;
	}
	public double getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(double attachmentId) {
		this.attachmentId = attachmentId;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public double getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(double retailerId) {
		this.retailerId = retailerId;
	}
	public String getOrderRefno() {
		return orderRefno;
	}
	public void setOrderRefno(String orderRefno) {
		this.orderRefno = orderRefno;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String string) {
		this.customerContact = string;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public int getCustomerZipcode() {
		return customerZipcode;
	}
	public void setCustomerZipcode(int customerZipcode) {
		this.customerZipcode = customerZipcode;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public int getBayNumber() {
		return bayNumber;
	}
	public void setBayNumber(int bayNumber) {
		this.bayNumber = bayNumber;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveredBy() {
		return deliveredBy;
	}
	public void setDeliveredBy(String deliveredBy) {
		this.deliveredBy = deliveredBy;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentReference() {
		return paymentReference;
	}
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	public double getOrderValue() {
		return orderValue;
	}
	public void setOrderValue(double orderValue) {
		this.orderValue = orderValue;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	

	






	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}






	public int getAttempts() {
		return attempts;
	}






	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}













	@Override
	public String toString() {
		return "Order [location=" + location + ", trackingId=" + trackingId
				+ ", orderId=" + orderId + ", orderTitle=" + orderTitle
				+ ", orderDetails=" + orderDetails + ", orderType=" + orderType
				+ ", orderCategory=" + orderCategory + ", orderPriority="
				+ orderPriority + ", orderProductId=" + orderProductId
				+ ", activityId=" + activityId + ", revisionId=" + revisionId
				+ ", attachmentId=" + attachmentId + ", storeId=" + storeId
				+ ", retailerId=" + retailerId + ", orderRefno=" + orderRefno
				+ ", customerName=" + customerName + ", customerContact="
				+ customerContact + ", customerEmail=" + customerEmail
				+ ", customerAddress=" + customerAddress + ", cityName="
				+ cityName + ", areaName=" + areaName + ", customerZipcode="
				+ customerZipcode + ", paymentType=" + paymentType
				+ ", bayNumber=" + bayNumber + ", orderStatus=" + orderStatus
				+ ", receiptDate=" + receiptDate + ", deliveryDate="
				+ deliveryDate + ", assignedTo=" + assignedTo
				+ ", deliveredDate=" + deliveredDate + ", deliveredBy="
				+ deliveredBy + ", paymentStatus=" + paymentStatus
				+ ", paymentReference=" + paymentReference + ", orderValue="
				+ orderValue + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + ", sessionId=" + sessionId + ", importFlag="
				+ importFlag + ", stockFlag=" + stockFlag + ", country="
				+ country + ", weight=" + weight + ", content=" + content
				+ ", vat=" + vat + ", alternaitveContactNo="
				+ alternaitveContactNo + ", acctualAmount=" + acctualAmount
				+ ", attempts=" + attempts + ", runSheetNumber="
				+ runSheetNumber + "]";
	}

	  
	 
	
	
	
}
