<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Geocoding service</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
        top: 5px;
        left: 50%;
        margin-left: -180px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    
    <%-- var customerEmail = '<%= request.getParameter("customerEmail") %>'; --%>
    var customerMobile = '<%= request.getParameter("customerMobile") %>';
    console.log("cusotmerMobile is" +customerMobile);
	var geocoder;
	var map;
	var latitude;
	  var longitude;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(13.0279661, 77.54091560000006);
  var mapOptions = {
    zoom: 8,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function validate() {
    /* var ra = document.getElementById("uname").value;
    var rag = document.getElementById("pwd").value; */

    console.log("latitude is" +latitude);
    console.log("longitude is" +longitude)
    console.log("Mobile" +customerMobile)
    $.ajax({
   type: "POST",
    url: "./updatemap",
    contentType: "application/json",
     dataType: 'json',
   		data:JSON.stringify({
        latitude:latitude,
        longitude:longitude,
        customerMobile:customerMobile
   }),
   complete: function(data) {
    
	   console.log(data);
	   alert("data saved successfully");
   }
});
  /*   console.log(ra, rag) */

}

function codeAddress() {
  var address = document.getElementById('address').value;
  
  

  var geocoder = new google.maps.Geocoder();

  
  
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    	latitude = results[0].geometry.location.lat();
        longitude = results[0].geometry.location.lng();
        console.log("latitude of address is" +latitude)
         console.log("longitude of address is" +longitude)
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
          
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
   
    
  });
}

	google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
    <div id="panel">
      <input id="address" type="textbox" value="Bengaluru, Yeshwantpur">
      <input type="button" value="Geocode" onclick="codeAddress()">
      <input type="button" value="Save Location" name="Submit" onclick= "validate()">
    </div>
    <div id="map-canvas"></div>
  </body>
</html>