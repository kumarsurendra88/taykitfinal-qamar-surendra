package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class ProductCategory {


	private String categoryName;
	
	private String categoryDesc;
	
	private String categoryStatus;
	
	private String updatedBy;
	private Timestamp updatedOn;
	
	
	public ProductCategory(){}


	public ProductCategory(String categoryName, String categoryDesc,
			String categoryStatus, String updatedBy, Timestamp updatedOn) {
		super();
		this.categoryName = categoryName;
		this.categoryDesc = categoryDesc;
		this.categoryStatus = categoryStatus;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public String getCategoryDesc() {
		return categoryDesc;
	}


	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}


	public String getCategoryStatus() {
		return categoryStatus;
	}


	public void setCategoryStatus(String categoryStatus) {
		this.categoryStatus = categoryStatus;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Timestamp getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
	

}
