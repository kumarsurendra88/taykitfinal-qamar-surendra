routerApp.controller('logController',['$scope','$http','$window','$location',function($scope,$http,$window) {
        
    	$http.get("./getCalLogs")
    	
    	.success(function(response) {$scope.logs = response;});
    	$scope.filterOptions = {
    	        filterText: ''
    	      };
        $scope.gridUser = { 
            data: 'logs',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [
                         {field:'userId', displayName:'user Id', enableCellEdit: false},
                         {field: 'customerName', displayName: 'customer name', enableCellEdit: false},
                         {field: 'customerContactNo', displayName: 'customer mobile', enableCellEdit: false}, 
                         {field:'trackingId', displayName:'tracking id', enableCellEdit: false},
                         {field:'callTime', displayName:'Call Time', enableCellEdit: false},
                         {field: '', displayName:'Call Location',cellTemplate: '<button class="btn btn-default btn-flat" ng-click="loadById(row)">Map Address</button>'},
                         
                         
                         ],
                         filterOptions: $scope.filterOptions
        };
        $scope.loadById = function(row) {  
        	  // window.console && console.log(row.entity);
        	   console.log("GeoLocation is" +row.entity.geoLoction)
        	   var uri="mapForEachAttempt.jsp?geoLocation="+row.entity.geoLoction;
        	   console.log("uri" +uri)
        	   $window.open(uri);
        	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
        };  
    }]);