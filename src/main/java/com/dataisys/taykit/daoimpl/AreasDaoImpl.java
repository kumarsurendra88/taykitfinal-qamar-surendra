package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.AreasDao;
import com.dataisys.taykit.model.Areas;

import com.dataisys.taykit.rowmapper.AreasRowMapper;


public class AreasDaoImpl extends JdbcDaoSupport implements AreasDao{

	public List<Areas> getAreas() {

		String sql="select * from area";
		List<Areas> listofAreas=getJdbcTemplate().query(sql, new AreasRowMapper());
		System.out.println("***************** : "+listofAreas);
		
		return listofAreas;
		
	}

	
	
}
