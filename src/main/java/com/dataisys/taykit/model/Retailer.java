package com.dataisys.taykit.model;

import java.sql.Timestamp;

/**
 * @author DataIsys
 *
 */
public class Retailer {

private double retailerId;
private String retailerName;
private String retailerType;
private String retailerCity;
private String retailerAddress;
private String retailerPhone;
private String retailerEmail;
private String retailerGeoLocation;
private String retailerZip;
private Timestamp updatedOn;
private String updatedBY;
private Timestamp createdOn;
private String createdBY;
public double getRetailerId() {
	return retailerId;
}
public void setRetailerId(double retailerId) {
	this.retailerId = retailerId;
}
public String getRetailerName() {
	return retailerName;
}
public void setRetailerName(String retailerName) {
	this.retailerName = retailerName;
}
public String getRetailerType() {
	return retailerType;
}
public void setRetailerType(String retailerType) {
	this.retailerType = retailerType;
}
public String getRetailerCity() {
	return retailerCity;
}
public void setRetailerCity(String retailerCity) {
	this.retailerCity = retailerCity;
}
public String getRetailerAddress() {
	return retailerAddress;
}
public void setRetailerAddress(String retailerAddress) {
	this.retailerAddress = retailerAddress;
}
public String getRetailerPhone() {
	return retailerPhone;
}
public void setRetailerPhone(String retailerPhone) {
	this.retailerPhone = retailerPhone;
}
public String getRetailerEmail() {
	return retailerEmail;
}
public void setRetailerEmail(String retailerEmail) {
	this.retailerEmail = retailerEmail;
}
public String getRetailerGeoLocation() {
	return retailerGeoLocation;
}
public void setRetailerGeoLocation(String retailerGeoLocation) {
	this.retailerGeoLocation = retailerGeoLocation;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}
public String getUpdatedBY() {
	return updatedBY;
}
public void setUpdatedBY(String updatedBY) {
	this.updatedBY = updatedBY;
}

@Override
public String toString() {
	return "Retailer [retailerId=" + retailerId + ", retailerName="
			+ retailerName + ", retailerType=" + retailerType
			+ ", retailerCity=" + retailerCity + ", retailerAddress="
			+ retailerAddress + ", retailerPhone=" + retailerPhone
			+ ", retailerEmail=" + retailerEmail + ", retailerGeoLocation="
			+ retailerGeoLocation 
			+ ", updatedOn=" + updatedOn + ", updatedBY=" + updatedBY
			+ ", createdOn=" + createdOn + ", createdBY=" + createdBY + "]";
}
public Timestamp getCreatedOn() {
	return createdOn;
}
public void setCreatedOn(Timestamp createdOn) {
	this.createdOn = createdOn;
}
public String getCreatedBY() {
	return createdBY;
}
public void setCreatedBY(String createdBY) {
	this.createdBY = createdBY;
}
public String getRetailerZip() {
	return retailerZip;
}
public void setRetailerZip(String retailerZip) {
	this.retailerZip = retailerZip;
} 

}
