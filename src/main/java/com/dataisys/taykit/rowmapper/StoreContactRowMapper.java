package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Device;
//import com.dataisys.taykit.model.OrderAttachments;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.StoreContact;

public class StoreContactRowMapper implements RowMapper<StoreContact>{

	
	
	public StoreContact mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 StoreContact contact= new StoreContact();
	 contact.setDesignation(rs.getString("contact_designation"));
	 contact.setEmail(rs.getString("contact_email"));
	 contact.setContactNo(rs.getString("contact_mobile"));
	 contact.setName(rs.getString("contact_name"));
	 contact.setNotes(rs.getString("contact_notes"));
	 contact.setStoreId(rs.getDouble("store_id"));
	 	return contact;
	 	
 }
	 	
 
 
 }
}

