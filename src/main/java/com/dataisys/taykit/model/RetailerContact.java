package com.dataisys.taykit.model;


public class RetailerContact {
private String contactName;
private String notes;
private String contactDesignation;
private String contactMobile;
private String contactEmail;
private double retailerId;


public String getContactName() {
	return contactName;
}
public void setContactName(String contactName) {
	this.contactName = contactName;
}
@Override
public String toString() {
	return "RetailerContact [contactName=" + contactName + ", contactType="
			+ notes + ", contactDesignation=" + contactDesignation
			+ ", contactMobile=" + contactMobile + ", contactEmail="
			+ contactEmail + "]";
}
public String getNotes() {
	return notes;
}
public void setnotes(String notes) {
	this.notes = notes;
}
public String getContactDesignation() {
	return contactDesignation;
}
public void setContactDesignation(String contactDesignation) {
	this.contactDesignation = contactDesignation;
}
public String getContactMobile() {
	return contactMobile;
}
public void setContactMobile(String contactMobile) {
	this.contactMobile = contactMobile;
}
public String getContactEmail() {
	return contactEmail;
}
public void setContactEmail(String contactEmail) {
	this.contactEmail = contactEmail;
}
public double getRetailerId() {
	return retailerId;
}
public void setRetailerId(double retailerId2) {
	this.retailerId = retailerId2;
}
}
