routerApp.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
    .controller('MyCtrl',function(Excel,$timeout){
      $scope.exportToExcel=function(tableId){ // ex: '#my-table'
            $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
            $timeout(function(){location.href=$scope.fileData.exportHref;},100); // trigger download
        }
    });

routerApp.controller('storeReportSummaryController',['$scope','$http','Excel','$timeout',function($scope,$http,Excel,$timeout){
	
	console.log("inside storeReportSummaryController in storeController.js");
	$scope.name="Store name"
	$('#mydiv').show();
	var storeName;
	$http.get('./getStoreNameOfHub')
	.success(function (data) {
	console.log(data);
	console.log("success")
	$scope.storeName=data;
	$('#mydiv').hide();
	});
	
	$scope.submit=function(dis){
		
		
		console.log("inside submit function");
		console.log("data colected are :")
		console.log("from date "+dis.fromDate+" todate :"+dis.toDate+" store name is "+$scope.name.name);
		if( angular.isUndefined($scope.name.name)){
			alert("Warning:Please select store Name!")
		}
		else{
		$http.get('./getTotalInward/'+dis.fromDate+','+dis.toDate+','+$scope.name.name)
		.success(function (data) {
		console.log(data);
		storeName=$scope.name.name;
		$scope.myData=data;

		
		});
	}
	}
	var sheetName="StoreReport"
	$scope.exportToExcel=function(tableId){ // ex: '#my-table'
		console.log("sfeetName is "+sheetName);
		 var exportHref=Excel.tableToExcel(tableId,sheetName);
         $timeout(function(){location.href=exportHref;},100); // trigger download
    }
	
	$scope.returnDate=function(){
		$scope.CurrentDate = new Date();
		console.log($scope.CurrentDate);
		console.log("date"+$scope.CurrentDate.getDate());
		//console.log($scope.CurrentDate.getDay());
		//console.log($scope.CurrentDate.getTime());
		console.log("month"+$scope.CurrentDate.getMonth());
		console.log("minutes"+$scope.CurrentDate.getMinutes());
		console.log("hours"+$scope.CurrentDate.getHours());
		console.log("year"+$scope.CurrentDate.getFullYear());
		console.log("year"+$scope.CurrentDate.getSeconds());
		
		$scope.myDate=" date"+$scope.CurrentDate.getDate()+"-"+$scope.CurrentDate.getMonth()+"-"+$scope.CurrentDate.getFullYear()+"@ time: "+$scope.CurrentDate.getHours()+":"+$scope.CurrentDate.getMinutes()+":"+$scope.CurrentDate.getSeconds();
		console.log("mydate is "+$scope.myDate);
		
		return $scope.myDate;
	}
	
	  $scope.exportData = function () {
	        var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "Report.xls");
	    };

}]);