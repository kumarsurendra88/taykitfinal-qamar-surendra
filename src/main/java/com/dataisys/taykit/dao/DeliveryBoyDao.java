package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.DeliveryBoys;

//import com.dataisys.pudo.model.DeliveryBoys;
//import com.dataisys.pudo.model.Order;
//import com.dataisys.pudo.model.User;

public interface DeliveryBoyDao {

public List getAll();

public com.dataisys.taykit.model.DeliveryBoys DeliveryBoyId(String name);

//public List<com.dataisys.taykit.model.DeliveryBoys> getAllByStore();

public List<DeliveryBoys> getDeliveryBoysByStoreId(double storeId);
public List<DeliveryBoys> getAllByStore(HttpSession session);

/* Added on 2016-02-05 */
public List<Integer> getStoreIdsForEachDeliveryBoy(String deliveryBoyName);
public void updateOrderStatusToPickup(String trackingId, int storeId);
public void updateOrderStatusToDropOff(String trackingId, int storeId);

}
