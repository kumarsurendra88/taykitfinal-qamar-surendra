package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Category;

public interface CategoryDao {


public void createCategory(Category category);

public void updateCategory(Category category);

public List<Category> getCategory();


}
