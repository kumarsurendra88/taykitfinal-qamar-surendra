package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.MasterBagDao;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.MasterBag;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.MasterBagRowMapper;
import com.dataisys.taykit.rowmapper.StoreRowMapper;


public class MasterBagDaoImpl extends JdbcDaoSupport implements MasterBagDao
{

	ArrayList<String> arr=new ArrayList<String>();
	public boolean createMasterBag(String destination, String masterbag_type,String type,HttpSession session) {
		System.out.println("masterbag destination : "+destination);
		System.out.println("master bag type : "+masterbag_type);
		System.out.println(" type : "+type);
		User user1=(User)session.getAttribute("user");
		System.out.println(user1.getUserId()+"usertype : "+user1.getUserType());
		
		String Q="";
		String Q1="";
		
		String flag1="";
		String createrFlag="";
		if(type.equals("Store"))
		{
			flag1="S";
			Q1="(select store_id from store where store_name=?)";
		}
		else
		{
			flag1="H";
			Q1="(select hub_id from hubs where hub_name=?)";
		}
		
		if(user1.getUserType().equalsIgnoreCase("Hub Admin") || user1.getUserType().equalsIgnoreCase("Hub Executive"))
		{
			createrFlag="H";
			Q="(select hub_id from user_hub where account_no=?)";
			
		}
		else if(user1.getUserType().equalsIgnoreCase("Store Admin") || user1.getUserType().equalsIgnoreCase("Store Executive"))
		{
			createrFlag="S";
			Q="(select store_id from user_store where account_no=?)";
			
		}
		int j=0;
		System.out.println("flag1 : "+flag1);
		boolean flag=false;
		//String sql="insert into masterbag(destination,masterbag_type,created_by) values(?,?,?)";
		String sql="insert into masterbag(destination,masterbag_type,created_by,source,flag,creater_flag) values("+Q1+",?,?,"+Q+",?,?)";
        System.out.println("Q : "+sql);
        System.out.println("values : "+destination+" : "+masterbag_type+" : "+user1.getUserId()+" : "+user1.getAccNumber()+" : "+flag1);
        int i=getJdbcTemplate().update(sql,destination,masterbag_type,user1.getUserId(),user1.getAccNumber(),flag1,createrFlag);
		if(i>0)
		{
					System.out.println("Master Bag Created");
					
						flag=true;
		}
		else
		{
			System.out.println("Error occurred");
		}
	
		
		return flag;
	}

	public List<MasterBag> getMasterBagListById(String user_id,
			String masterbag_type, HttpSession session) {
		System.out.println("*****QQ******userid and masterbag type : "+user_id+" : "+masterbag_type);
		String sql="select * from masterbag where masterbag_type=? and created_by=? and status='Inactive'";
		List<com.dataisys.taykit.model.MasterBag> ListOfMasterBag=getJdbcTemplate().query(sql,new Object[] {masterbag_type,user_id}, new MasterBagRowMapper());
				
			
		System.out.println("********QQ**********: "+ListOfMasterBag);

		return ListOfMasterBag;
	}

	public boolean AddToMasterBag(double masterbag_no, String order_id,
			String masterbag_type, HttpSession session) {
		
		int i1=CheckOrderLocationFromOrderBeforeAdding(masterbag_no,order_id);
		System.out.println("masterbag no : "+masterbag_no);
		System.out.println("master bag type : "+masterbag_type);
		System.out.println("orderid : "+order_id);
		User user1=(User)session.getAttribute("user");
		//System.out.println(user1.getUserId());
		int j=0;
		String Q="";
		boolean flag=false;
		if(masterbag_type.equals("Delivery"))
		{
			Q="order_id";
		}
		else
		{
			Q="return_id";
		}
		String sql="insert into masterbagdata(masterbag_no,"+Q+",masterbag_type) values(?,?,?)";
		//String sql2="select * from masterbagdata where masterbag_no=?";
	
		//List<com.dataisys.taykit.model.MasterBag> ListOfMasterBag=null;
		if(i1>0)
		{
		int i=getJdbcTemplate().update(sql,masterbag_no,order_id,masterbag_type);
		if(i>0)
		{
					System.out.println("Added to Master Bag");
					flag=true;
					//ListOfMasterBag=getJdbcTemplate().query(sql2,new Object[] {masterbag_no}, new MasterBagRowMapper());
					
		}
		else
		{
			System.out.println("Error occurred");
		}
		}
		//System.out.println("list of masterbag : "+ListOfMasterBag);
		return flag;
		}

	public boolean ActivateMasterBag(double masterbag_no, HttpSession session) {
		// TODO Auto-generated method stub
		System.out.println("masterbag no : "+masterbag_no);
		
Date today= new Date();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		User user1=(User)session.getAttribute("user");
		System.out.println(user1.getUserId());
		
	
		boolean flag=false;
		int i=0;
		String sql="update masterbag set status='Active',activated_on=? where masterbag_no=?";
		String sql2="update masterbagdata set flag=1,updated_on=? where masterbag_no=?";
		String sql3="update `order` set location='some location' where tracking_id=?";
		int j,k;
	
		
		
		
		/*if(CheckOrderLocationFromOrder(masterbag_no)==getTotalNoOrdersFromMasterBagData(masterbag_no))
		{*/

		
		i=getJdbcTemplate().update(sql,dateFormat.format(today),masterbag_no);
		
		
		if(i>0)
		{
					System.out.println("Master Bag activated");
					 j=getJdbcTemplate().update(sql2,dateFormat.format(today),masterbag_no);
					if(j>0){
						
						for(String tracking_id:arr){
						k=getJdbcTemplate().update(sql3,tracking_id);
						System.out.println("tracking_id " +tracking_id);
						}
						flag=true;
					}
		}
		else
		{
			System.out.println("Error occurred");
		}
	
		//}
		System.out.println("********"+flag);
		return flag;
		
	}

	public List<MasterBag> GetMasterBagData(double masterbag_no,String masterbag_type, HttpSession session) {

		System.out.println("values in impl : "+masterbag_no+" : "+masterbag_type);
		String sql2="select * from masterbagdata where masterbag_no='"+masterbag_no+"' and masterbag_type='"+masterbag_type+"'";

			System.out.println(sql2);
return getJdbcTemplate().query(sql2,new ResultSetExtractor<List<MasterBag>>(){

public List<MasterBag> extractData(ResultSet rs) throws SQLException,
		DataAccessException {System.out.println("inside");
	List<MasterBag> list=new ArrayList<MasterBag>(); 
	System.out.println("inside");
    while(rs.next()){  
    	MasterBag e=new MasterBag();  
   //  e.setActivityDate(rs.getDate("activityDate"));
    e.setOrder_id(rs.getString("order_id"));
    e.setReturn_id(rs.getString("return_id"));
    e.setMasterbag_no(rs.getDouble("masterbag_no"));
    e.setMasterbag_type(rs.getString("masterbag_type"));
    	
     System.out.println(rs.getString("order_id"));
     System.out.println(rs.getString("return_id"));
     System.out.println(rs.getDouble("masterbag_no"));
     System.out.println(rs.getString("masterbag_type"));
    list.add(e);  
    System.out.println("inside while");
    }  
    return list;
}

});
	
	}

	public Hub getUserMasterBag(HttpSession session) {

		User user1=(User)session.getAttribute("user");
		System.out.println("values in impl : "+user1.getAccNumber());
		String sqlStore="SELECT store_name FROM store WHERE store_id=(select store_id from user_store where account_no='"+user1.getAccNumber()+"')";
		
String sqlHub="SELECT hub_name FROM hubs WHERE hub_id=(select hub_id from user_hub where account_no='"+user1.getAccNumber()+"')";
String finalsql="";
String column="";
if(user1.getUserType().equalsIgnoreCase("Store Admin"))
{
	finalsql=sqlStore;
	column="store_name";
}
else if(user1.getUserType().equalsIgnoreCase("Hub Admin"))
{
	finalsql=sqlHub;
	column="hub_name";
}

final String finalcolumn=column;

			System.out.println("final column : "+finalcolumn+" : final query: "+finalsql);
return getJdbcTemplate().query(finalsql,new ResultSetExtractor<Hub>(){

public Hub extractData(ResultSet rs) throws SQLException,
		DataAccessException {System.out.println("inside");
	//List<Hub> list=new ArrayList<Hub>(); 
	System.out.println("inside");
	Hub e=new Hub();  
    while(rs.next()){  
    	
   //  e.setActivityDate(rs.getDate("activityDate"));
  e.setHub_name(rs.getString(finalcolumn));
   // list.add(e);  
    System.out.println("inside while");
    }  
    return e;
}

});
	}

	public List<MasterBag> getMasterBagListForCurrentUser(HttpSession session) {
		
		User user1=(User)session.getAttribute("user");
		System.out.println("values in impl : "+user1.toString());
		String sql2="";
				
		//============Condition for admin r store user=======================/
		//String sqlStore="select * from masterbag where destination=(select store_id from user_store where account_no='"+user1.getAccNumber()+"') and status='Active'"; 
		/*String sqlStore="select * from masterbag where destination="+
						"(select hub_id from store where store_id="+
						"(select store_id from user_store where account_no='"+user1.getAccNumber()+"'))"+
						"and status='Active'";
		//===============================================================================//
		String sqlStore1="select * from masterbag where destination="+
				"(select hub_id from store where store_id="+
				"(select store_id from user_store where account_no='"+user1.getAccNumber()+"'))"+
				"and status='Active'";
		*/
		//==============================================================================//
		//String sqlHub="select * from masterbag where destination=(select hub_id from user_hub where account_no='"+user1.getAccNumber()+"') and status='Active'";
		//String sqlStore="select * from masterbag where destination=(select store_id from user_store where account_no='"+user1.getAccNumber()+"') and status='Active'";
		
		//String sqlHub="select m.masterbag_no,m.masterbag_type from masterbag m , masterbagdata md where m.destination=(select hub_id from user_hub where account_no='"+user1.getAccNumber()+"') and status='Active' and md.flag=1";
		//String sqlStore="select m.masterbag_no,m.masterbag_type from masterbag m , masterbagdata md where m.destination=(select store_id from user_store where account_no='"+user1.getAccNumber()+"') and status='Active' and md.flag=1";
		String sqlHub="SELECT distinct m.masterbag_no,m.masterbag_type FROM "+
                "`masterbag` m INNER JOIN `masterbagdata` md ON md.masterbag_no=m.masterbag_no "+
                "where m.destination=(select hub_id from user_hub where account_no='"+user1.getAccNumber()+"')"+ 
                "and status='Active' and md.flag=1";

        
        
        String sqlStore="SELECT distinct m.masterbag_no,m.masterbag_type FROM "+
                        "`masterbag` m INNER JOIN `masterbagdata` md ON md.masterbag_no=m.masterbag_no "+
                        "where m.destination=(select store_id from user_store where account_no='"+user1.getAccNumber()+"')"+ 
                        "and status='Active' and md.flag=1";



		
		
		if(user1.getUserType().equalsIgnoreCase("Store Admin") || user1.getUserType().equalsIgnoreCase("Store Executive"))
		{
			sql2=sqlStore;
		}
		else if(user1.getUserType().equalsIgnoreCase("Hub Admin") || user1.getUserType().equalsIgnoreCase("Hub Executive"))
		{
			sql2=sqlHub;
		}
		 
		
			System.out.println("Query : "+sql2);
return getJdbcTemplate().query(sql2,new ResultSetExtractor<List<MasterBag>>(){

public List<MasterBag> extractData(ResultSet rs) throws SQLException,
		DataAccessException {System.out.println("inside");
	List<MasterBag> list=new ArrayList<MasterBag>(); 
	System.out.println("inside");
    while(rs.next()){  
    	MasterBag e=new MasterBag();  
   //  e.setActivityDate(rs.getDate("activityDate"));
 //   e.setOrder_id(rs.getString("order_id"));
   // e.setReturn_id(rs.getString("return_id"));
    e.setMasterbag_no(rs.getDouble("masterbag_no"));
    e.setMasterbag_type(rs.getString("masterbag_type"));
    	
    // System.out.println(rs.getString("order_id"));
   //  System.out.println(rs.getString("return_id"));
     System.out.println(rs.getDouble("masterbag_no"));
     System.out.println(rs.getString("masterbag_type"));
    list.add(e);  
    System.out.println("inside while");
    }  
    return list;
}

});
	
}

	public MasterBag GetMasterBagDetailsByNo(double masterbag_no,
			HttpSession session) {
		
	
		System.out.println("values in impl : "+masterbag_no+" : ");
		//String sql2="SELECT a.masterbag_no, count(*)  as 'counter',b.source,b.activated_on,a.masterbag_type FROM masterbagdata a, masterbag b WHERE a.masterbag_no = b.masterbag_no and a.masterbag_no="+masterbag_no+" GROUP BY a.masterbag_no";

		final String sqlHub="SELECT A.masterbag_no, count(*)  as 'counter' ,A.masterbag_type,B.activated_on, B.source, C.store_name"+
					 " FROM masterbagdata A,masterbag B,store C  WHERE B.masterbag_no = A.masterbag_no AND"+
					 " C.store_id = B.source and  B.creater_flag='S'  and A.masterbag_no="+masterbag_no+" GROUP BY A.masterbag_no ";

		
		final String sqlStore="SELECT A.masterbag_no, count(*)  as 'counter' ,A.masterbag_type,B.activated_on, "
				+ "B.source, C.hub_name FROM masterbagdata A,masterbag B,hubs C  WHERE B.masterbag_no = A.masterbag_no "
				+ "AND C.hub_id = B.source and  B.creater_flag='H'  and A.masterbag_no='"+masterbag_no+"' GROUP BY A.masterbag_no ";

		
		final MasterBag e=new MasterBag();
		
			
return getJdbcTemplate().query(sqlHub,new ResultSetExtractor<MasterBag>(){

public MasterBag extractData(ResultSet rs) throws SQLException,
		DataAccessException {System.out.println("inside");
	//List<MasterBag> list=new ArrayList<MasterBag>(); 
	System.out.println("inside");
	//MasterBag e=new MasterBag();  
	System.out.println("HUB : "+sqlHub);
    if(rs.next()){  
    	System.out.println("*********inside rs.next if********");
   //  e.setActivityDate(rs.getDate("activityDate"));
e.setCounter(rs.getInt("counter"));
e.setSource(rs.getDouble("source"));
e.setActivated_on(rs.getDate("activated_on"));
 e.setHub_name(rs.getString("store_name"));
    e.setMasterbag_type(rs.getString("masterbag_type"));
    
    System.out.println("inside while of hub");
    }
    
    else
    {
    	System.out.println("****inside else*******");
    	return getJdbcTemplate().query(sqlStore,new ResultSetExtractor<MasterBag>(){

    		public MasterBag extractData(ResultSet rs) throws SQLException,
    				DataAccessException {System.out.println("inside");
    			//List<MasterBag> list=new ArrayList<MasterBag>(); 
    			System.out.println("inside else return");
    			//MasterBag e=new MasterBag();  
    			System.out.println("Store : "+sqlStore);
    		    if(rs.next()){  
    		    	
    		   //  e.setActivityDate(rs.getDate("activityDate"));
    		e.setCounter(rs.getInt("counter"));
    		e.setSource(rs.getDouble("source"));
    		e.setActivated_on(rs.getDate("activated_on"));
    		 e.setHub_name(rs.getString("hub_name"));
    		    e.setMasterbag_type(rs.getString("masterbag_type"));
    		    
    		    System.out.println("inside while of store");
    		    }
    		    
    		    return e;
    			}
    		});
    }
    
    
    
    
    return e;
	}
});
}

	public Order UpdateOrderByInwardMasterbag(double masterbag_no,String tracking_id,
			String location, String fromlocation,HttpSession session) {
		
		User user1=(User)session.getAttribute("user");
		
		String sqlhub="select * from masterbagdata md,masterbag m where md.order_id='"+tracking_id+"' "
					+ "and md.masterbag_no='"+masterbag_no+"' and m.destination=(select hub_id from store where hub_name='"+location+"') and m.flag='H'";

		 
		String sqlstore="select * from masterbagdata md,masterbag m where md.order_id='"+tracking_id+"' "
					+ "and md.masterbag_no='"+masterbag_no+"' and m.destination=(select store_id from store where store_name='"+location+"') and m.flag='S'";

		 
		String sqlfinal="";
		if(user1.getUserType().equalsIgnoreCase("Hub Admin") || user1.getUserType().equalsIgnoreCase("Hub Executive"))
		{
			sqlfinal=sqlhub;
		}
		
		if(user1.getUserType().equalsIgnoreCase("Store Admin") || user1.getUserType().equalsIgnoreCase("Store Executive"))
		{
			sqlfinal=sqlstore;
		}

		int i=CheckOrderFromMasterBagData(masterbag_no, tracking_id, location,sqlfinal);
		
		String sql2="";
	if(i>0)	
	{
		
		String sql = "update `order` set location=? where tracking_id=?";
		getJdbcTemplate().update(sql,location,tracking_id);
System.out.println("first query executed");
//=====================================================
String sql1 = "update `masterbagdata` set flag=2 where order_id=?";
getJdbcTemplate().update(sql1,tracking_id);
System.out.println("second query executed");
//=====================================================
//String sql3 = "update `order_activity` set to_location=?,from_location=? where tracking_id=?";
String sql3="update `order_activity` set to_location=(select hub_id from hubs where hub_name=?),from_location=(select hub_id from hubs where hub_name=?) where tracking_id=?";
getJdbcTemplate().update(sql3,location,fromlocation,tracking_id);
System.out.println("third query executed");
//=====================================================
		sql2="SELECT count(*)  as 'counter' from `order` where location='"+location+"' GROUP BY location";

		System.out.println(sql2);
		
	}
return getJdbcTemplate().query(sql2,new ResultSetExtractor<Order>(){

public Order extractData(ResultSet rs) throws SQLException,
	DataAccessException {System.out.println("inside");
//List<MasterBag> list=new ArrayList<MasterBag>(); 
System.out.println("inside");
Order e=new Order();  
while(rs.next()){  
	
//  e.setActivityDate(rs.getDate("activityDate"));
e.setCounter(rs.getInt("counter"));

System.out.println("inside while before return");
}  
return e;
}
});
	
}

	public MasterBag GetMasterBagDestinationByNo(double masterbag_no,
			HttpSession session) {

		
		System.out.println("values in GetMasterBagDestinationByNo impl : "+masterbag_no+" : ");
		//String sql2="SELECT a.masterbag_no, count(*)  as 'counter',b.source,b.activated_on,a.masterbag_type FROM masterbagdata a, masterbag b WHERE a.masterbag_no = b.masterbag_no and a.masterbag_no="+masterbag_no+" GROUP BY a.masterbag_no";

		String sql2="select hub_name from hubs where hub_id=(select destination from masterbag where masterbag_no="+masterbag_no+" and flag='H')";
		final String sql3="select store_name from store where store_id=(select destination from masterbag where masterbag_no="+masterbag_no+" and flag='S')";

			System.out.println(sql2);
			System.out.println(sql3);
			final MasterBag e=new MasterBag();
return getJdbcTemplate().query(sql2,new ResultSetExtractor<MasterBag>(){

public MasterBag extractData(ResultSet rs) throws SQLException,
		DataAccessException {
	System.out.println("inside");
	//List<MasterBag> list=new ArrayList<MasterBag>(); 
	
	//MasterBag e=new MasterBag();  
    if(!rs.next()){  
 //  if(rs.getString("hub_name")==null){
	   System.out.println("inside if");
	   
	   return getJdbcTemplate().query(sql3,new ResultSetExtractor<MasterBag>(){

		   public MasterBag extractData(ResultSet rs) throws SQLException,
		   		DataAccessException {
		   	System.out.println("inside ");
		   	//List<MasterBag> list=new ArrayList<MasterBag>(); 
		   	
		   	//MasterBag e=new MasterBag();  
		       while(rs.next()){  
		      
		      e.setHub_name(rs.getString("store_name")); 
		    
		       System.out.println("inside second while");
		       System.out.println("hub name set as store: "+e.getHub_name());
		       }  
		       return e;
		   	}
		   });
   }
   else{
	   e.setHub_name(rs.getString("hub_name")); 
   
    //System.out.println("inside while");
    //}  
    System.out.println("hub name set as hub : "+e.getHub_name());
    return e;
	}}
});
	}
	
	public int CheckOrderLocationFromOrder(double masterbag_no)
	{
		System.out.println("inside CheckOrderLocationFromOrder");
		//=================================================================================
				System.out.println("values in impl : "+masterbag_no);
				String sql2="select md.order_id from  masterbagdata md"
						+ " inner join  masterbag m   on m.masterbag_no = md.masterbag_no"
						+ "  inner join    `order` o   on o.tracking_id = md.order_id"
						+ " where md.masterbag_no='"+masterbag_no+"' and m.source=o.location";

					System.out.println(sql2);
		return getJdbcTemplate().query(sql2,new ResultSetExtractor<Integer>(){

		public Integer extractData(ResultSet rs) throws SQLException,DataAccessException
		{
			int i=0;
			System.out.println("inside");
			//List<MasterBag> list=new ArrayList<MasterBag>(); 
			System.out.println("inside");
		    while(rs.next()){  
		    	i++;
		    System.out.println("inside while");
		    arr.add(rs.getString("order_id"));
		    }  
		    System.out.println("value of i in CheckOrderLocationFromOrder: "+i);
		    System.out.println("arr : "+arr);
		    return i;
		}

		});
	//=================================================================================
		
	}	
	
	public int getTotalNoOrdersFromMasterBagData(double masterbag_no)
	{
		System.out.println("inside getTotalNoOrdersFromMasterBagData");
		//=================================================================================
				System.out.println("values in impl : "+masterbag_no);
				String sql2="select count(*) as TOTAL from masterbagdata where masterbag_no='"+masterbag_no+"'";

					System.out.println(sql2);
		return getJdbcTemplate().query(sql2,new ResultSetExtractor<Integer>(){

		public Integer extractData(ResultSet rs) throws SQLException,DataAccessException
		{
			int i=0;
			System.out.println("inside");
			//List<MasterBag> list=new ArrayList<MasterBag>(); 
			System.out.println("inside");
		    while(rs.next()){  
		    i=rs.getInt("TOTAL");
		    System.out.println("inside while");
		    }  
		    System.out.println("total no of rows in masterbagdata: "+i);
		    System.out.println("value of i in getTotalNoOrdersFromMasterBagData : "+i);
		    return i;
		}

		});
	//=================================================================================
	}	
	
	public int CheckOrderFromMasterBagData(double masterbag_no,String tracking_id,String destination,String sql2)
	{
		System.out.println("inside CheckOrderFromMasterBagData");
		//=================================================================================
				System.out.println("values in impl : "+masterbag_no+" :  : "+tracking_id);
				
					System.out.println(sql2);
		return getJdbcTemplate().query(sql2,new ResultSetExtractor<Integer>(){

		public Integer extractData(ResultSet rs) throws SQLException,DataAccessException
		{
			int i=0;
			System.out.println("inside");
			//List<MasterBag> list=new ArrayList<MasterBag>(); 
			System.out.println("inside");
		    while(rs.next()){  
		    i++;
		    }
		    System.out.println("total no of rows in masterbagdata: "+i);
		    //System.out.println("value of i in getTotalNoOrdersFromMasterBagData : "+i);
		    return i;
		}

		});
	//=================================================================================
	}
	public int CheckOrderLocationFromOrderBeforeAdding(double masterbag_no,String tracking_id)
	{
		System.out.println("inside CheckOrderLocationFromOrder");
		//=================================================================================
				System.out.println("values in impl : "+masterbag_no);
			/*	String sql2="select md.order_id from  masterbagdata md"
						+ " inner join  masterbag m   on m.masterbag_no = md.masterbag_no"
						+ "  inner join    `order` o   on o.tracking_id = md.order_id"
						+ " where md.masterbag_no='"+masterbag_no+"' and m.source=o.location";
*/
				
				String sql2="select o.tracking_id from `order` o inner join masterbag m on"
						+ " o.location=m.source where o.tracking_id='"+tracking_id+"' and m.masterbag_no='"+masterbag_no+"'";
					System.out.println(sql2);
		return getJdbcTemplate().query(sql2,new ResultSetExtractor<Integer>(){

		public Integer extractData(ResultSet rs) throws SQLException,DataAccessException
		{
			int i=0;
			System.out.println("inside");
			//List<MasterBag> list=new ArrayList<MasterBag>(); 
			System.out.println("inside");
		    while(rs.next()){  
		    	i++;
		    System.out.println("inside while");
		    arr.add(rs.getString("tracking_id"));
		    }  
		    System.out.println("value of i in CheckOrderLocationFromOrder: "+i);
		    System.out.println("arr : "+arr);
		    return i;
		}

		});
	//=================================================================================
		
	}	
}