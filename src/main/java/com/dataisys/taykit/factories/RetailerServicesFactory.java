package com.dataisys.taykit.factories;

import java.util.ArrayList;
import java.util.List;

import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerServices;

public class RetailerServicesFactory {
private static RetailerServicesFactory factory;
private List<RetailerServices> services;;

	
	public static RetailerServicesFactory getFactory() {
		if(factory==null){
			factory=new RetailerServicesFactory();
		   return factory;
		}
             return factory;
   } 
public static void setFactory(RetailerServicesFactory factory) {
	RetailerServicesFactory.factory = factory;
}

public List<RetailerServices> getServices() {
	return services;
}


public  void setServices(List<RetailerServices> service) {
	services=service;
	
}


public RetailerServicesFactory() {
	super();
}

}
