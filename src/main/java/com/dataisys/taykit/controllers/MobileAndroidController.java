package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.mobile.model.LoginForm;
import com.dataisys.taykit.mobile.model.RegisterKeyResponse;
import com.dataisys.taykit.mobile.model.ResponseForm;
import com.dataisys.taykit.model.RegistrationKey;
import com.dataisys.taykit.model.User;
import com.sun.mail.handlers.message_rfc822;

@RestController
public class MobileAndroidController {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	public RegistrationKeyDao registrationKeyDao;
	
	@RequestMapping(value="/mobileAuthenticate" , method=RequestMethod.POST)
	public ResponseForm validateMobileUser(@ModelAttribute("loginform") LoginForm loginform, HttpSession session)throws IOException
	{	
		ResponseForm respone = new ResponseForm();
		System.out.println("Username:"+loginform.getUsername());
		System.out.println("Password:"+loginform.getPassword());
		System.out.println("PudoType:"+loginform.getPudoType());
		
		String username = loginform.getUsername();
		String password = loginform.getPassword();
		String pudoType=loginform.getPudoType();
		
		boolean isAuthenticated = false;
		User user = null;
		
		try {			
				user = userDao.getUserByCredentials(username,password,pudoType);
				if(user != null)
				{
				isAuthenticated = true;				
				session.setAttribute("user", user);
				String pudoType1= user.getAccNumber();
				System.out.println("session inside login controoler"+pudoType1);
				
				session.setAttribute("pudoType", pudoType1);
				long active = session.getLastAccessedTime();
				Timestamp activeTime= new Timestamp(active);
				session.setAttribute("activeLastd", activeTime);				
			}			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	
		if(isAuthenticated) {
			respone.setStatus(true);
			respone.setMessage("Login Successful");			
			respone.setData("data");
		} else {
			respone.setStatus(false);
			respone.setMessage("Invalid credentials");	
		}
		return respone;
	}
	
	@RequestMapping(value="/tabletAuthenticate" , method=RequestMethod.POST)	
	public ResponseForm validateTabletUser(@ModelAttribute("loginform") User loginform, HttpSession session)throws IOException
	{	
		
		System.out.println("Inside tabletAuthenticate");
		ResponseForm respone = new ResponseForm();
		System.out.println("Username:"+loginform.getUserLogin());
		System.out.println("Password:"+loginform.getUserPassword());
		System.out.println("PudoType:"+loginform.getAccNumber());
		System.out.println("RegKey: "+loginform.getRegKey());
		
		String username = loginform.getUserLogin();
		String password = loginform.getUserPassword();
		String pudoType= loginform.getAccNumber();
		String registrationKey = loginform.getRegKey();
		
		
		RegistrationKey registrationKeyModel = new RegistrationKey();
		registrationKeyModel.setAccountNo(pudoType);
		registrationKeyModel.setRegKey(registrationKey.trim());
		String regKeyStatusMessage = null;
		
		boolean isAuthenticated = false;
		User user = null;
		
		try {			
				user = userDao.getUserByCredentials(username,password,pudoType);
				//user = new User();
				if(user != null) {
				System.out.println("User Type is: "+user.getUserType());
				isAuthenticated = true;
				session.setAttribute("user", user);
				//String pudoType1= user.getAccountNo();
				//System.out.println("session inside login controoler"+pudoType1);
				
				//session.setAttribute("pudoType", pudoType1);
				/*long active = session.getLastAccessedTime();
				Timestamp activeTime= new Timestamp(active);
				session.setAttribute("activeLastd", activeTime);*/				
			}			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	
		/* Code for Notifications */
		if(user.getUserType().trim().equalsIgnoreCase("DELIVERY_BOY")) {
			RegistrationKey checkAccountNo = registrationKeyDao.getRegkeyByAccountid(registrationKeyModel.getAccountNo());
			RegistrationKey checkRegKey = registrationKeyDao.getAccountNoByRegKey(registrationKeyModel.getRegKey());
			System.out.println("checkAccountNo: "+checkAccountNo);
			System.out.println("checkRegKey: "+checkRegKey);
			
			if((checkAccountNo == null && checkRegKey == null) || (checkAccountNo == null && checkRegKey != null)) {
				System.out.println("Inside (checkAccountNo == null && checkRegKey == null) || (checkAccountNo == null && checkRegKey != null)");
				if((checkAccountNo == null && checkRegKey != null)) {
					System.out.println("Inside if -> (checkAccountNo == null && checkRegKey != null)");
					regKeyStatusMessage = "Failure";
				} else {					
					System.out.println("Inside if -> else");
					//if(registrationKey.trim().equalsIgnoreCase(checkRegKey.getRegKey().trim()))
					registrationKeyDao.createNewRegKey(registrationKeyModel);
					regKeyStatusMessage = "Success";
				}
				
			} else {
				System.out.println("Inside else");
				RegistrationKey forAccountNoMatch = registrationKeyDao.getAccountNoByRegKey(registrationKeyModel.getRegKey());
				RegistrationKey forRegKeyMatch = registrationKeyDao.getRegkeyByAccountid(registrationKeyModel.getAccountNo());
				
				if(forAccountNoMatch != null && forRegKeyMatch != null) {
					System.out.println("forAccountNoMatch: "+forAccountNoMatch);
					System.out.println("forRegKeyMatch: "+forRegKeyMatch);
					
					if(registrationKeyModel.getAccountNo().trim().equalsIgnoreCase(forAccountNoMatch.getAccountNo()) && registrationKeyModel.getRegKey().trim().equalsIgnoreCase(forRegKeyMatch.getRegKey())) {
						System.out.println("Inside else-> if");
						regKeyStatusMessage = "Success";
					} else {
						System.out.println("Inside else-> else");
						regKeyStatusMessage = "Failure";
					}					
				}
				//registrationKeyDao.updateRegKeyByAccountNo(regkey);
			}	
		}
		/* End of the Code for Notification */
		
		
		if(isAuthenticated) {
			respone.setStatus(true);
			respone.setMessage("Login Successful");			
			respone.setData("data");
			respone.setUserType(user.getUserType());
			respone.setRegKeyMessage(regKeyStatusMessage);
		} else {
			respone.setStatus(false);
			respone.setMessage("Invalid credentials");	
			respone.setData("data");
			respone.setUserType("null");
			respone.setRegKeyMessage("null");
		}
		
		System.out.println("Response status: "+respone.isStatus());
		System.out.println("Response Message: "+respone.getMessage());
		System.out.println("Response UserType: "+respone.getUserType());
		System.out.println("Response RegKeyMessage: "+respone.getRegKeyMessage());
		return respone;
	}
	
	@RequestMapping(value="/createRegKey" , method=RequestMethod.POST)
	public RegisterKeyResponse createRegKeyMethod(@ModelAttribute("registrationKey") RegistrationKey registrationKey, BindingResult result, HttpSession session)throws IOException {
		
		System.out.println("Inside Create RegKey Method");
		System.out.println("Registration Key is: "+registrationKey.getRegKey());
		System.out.println("AccountNumber is: "+registrationKey.getAccountNo());
		
		RegisterKeyResponse response = new RegisterKeyResponse();
		String message = null;
		
		RegistrationKey key = registrationKeyDao.getRegkeyByAccountid(registrationKey.getAccountNo());
		System.out.println("key: "+key);
		if(key == null) {
			System.out.println("Inside if");
			registrationKeyDao.createNewRegKey(registrationKey);
		} else {			
			System.out.println("Inside else");
			RegistrationKey forAccountNoMatch = registrationKeyDao.getAccountNoByRegKey(registrationKey.getRegKey());
			RegistrationKey forRegKeyMatch = registrationKeyDao.getRegkeyByAccountid(registrationKey.getAccountNo());
			
			if(registrationKey.getAccountNo().trim().equalsIgnoreCase(forAccountNoMatch.getAccountNo()) && registrationKey.getRegKey().trim().equalsIgnoreCase(forRegKeyMatch.getRegKey())) {
				System.out.println("Inside else-> if");
				message = "Success";
			} else {
				System.out.println("Inside else-> else");
				message = "Failure";
			}
			//registrationKeyDao.updateRegKeyByAccountNo(regkey);
		}	
		
		response.setMessage(message);
		return response;
	}
	
	/*@RequestMapping(value="/createRegKey1" , method=RequestMethod.POST)
	public RegisterKeyResponse createRegKeyMethod1(@ModelAttribute("registrationKey") RegistrationKey registrationKey, HttpSession session)throws IOException {
		
		System.out.println("Inside Create RegKey Method");
		System.out.println("Registration Key is: "+registrationKey.getRegKey());
		System.out.println("AccountNumber is: "+registrationKey.getAccountNo());
		
		RegisterKeyResponse response = new RegisterKeyResponse();
		String message = null;
		
		RegistrationKey key = registrationKeyDao.getRegkeyByAccountid(registrationKey.getAccountNo());
		System.out.println("key: "+key);
		if(key == null) {
			System.out.println("Inside if");
			registrationKeyDao.createNewRegKey(registrationKey);
		} else {			
			System.out.println("Inside else");
			RegistrationKey forAccountNoMatch = registrationKeyDao.getAccountNoByRegKey(registrationKey.getRegKey());
			RegistrationKey forRegKeyMatch = registrationKeyDao.getRegkeyByAccountid(registrationKey.getAccountNo());
			
			if(registrationKey.getAccountNo().trim().equalsIgnoreCase(forAccountNoMatch.getAccountNo()) && registrationKey.getRegKey().trim().equalsIgnoreCase(forRegKeyMatch.getRegKey())) {
				System.out.println("Inside else-> if");
				message = "Success";
			} else {
				System.out.println("Inside else-> else");
				message = "Failure";
			}
			//registrationKeyDao.updateRegKeyByAccountNo(regkey);
		}	
		
		response.setMessage(message);
		return response;
	}*/
	
}
