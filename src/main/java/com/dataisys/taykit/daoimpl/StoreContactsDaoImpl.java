package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.HubRowMapper;
import com.dataisys.taykit.rowmapper.RetailerContactRowMapper;
import com.dataisys.taykit.rowmapper.RetailerTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.StoreContactRowMapper;
import com.dataisys.taykit.rowmapper.StoreRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.StoreContactDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class StoreContactsDaoImpl extends JdbcDaoSupport implements StoreContactDao {

	public void addContact(final List<StoreContact> contacts) {
		String SQL = "insert into store_contact(store_id,contact_name,contact_mobile,contact_email,contact_designation,contact_notes) values (?,?,?,?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   StoreContact detail = contacts.get(i);
		   ps.setDouble(1, detail.getStoreId());
		   ps.setString(2, detail.getName());
		   ps.setString(3, detail.getContactNo());
		   ps.setString(4, detail.getEmail());
		   ps.setString(5, detail.getDesignation());
		   ps.setString(6, detail.getNotes());
		  
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return contacts.size();
		      
		                  }


		                });
     return;
		
	}

	public StoreContact getContact(int storeID) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<StoreContact> getStoreContactsBId(double id) {
		String sql="select * from `store_contact` where store_id=?";
		List<StoreContact>contacts=getJdbcTemplate().query(sql, new Object[]{id}, new StoreContactRowMapper());
		return contacts;
	}

	public List<Store> getStoreByHubName(String hub_name) {
		System.out.println("*****QQ******hub_name : "+hub_name);
		String sql="select * from store where hub_id=(select hub_id from hubs where hub_name=?) ";
		List<com.dataisys.taykit.model.Store> ListOfStoreNames=getJdbcTemplate().query(sql,new Object[] {hub_name}, new StoreRowMapper());
				
			
		System.out.println("********QQ**********: "+ListOfStoreNames);

		return ListOfStoreNames;
	}

	public List<Store> getStoreByAccNo(double user_id) {
		System.out.println("*****QQ******hub_name : "+user_id);
		String sql="select * from store where hub_id=(select hub_id from user_hub where account_no=(select account_no from user where user_id=?))";
		List<com.dataisys.taykit.model.Store> ListOfStoreNames=getJdbcTemplate().query(sql,new Object[] {user_id}, new StoreRowMapper());
				
			
		System.out.println("********QQ**********: "+ListOfStoreNames);

		return ListOfStoreNames;
	}

	public List<Hub> getHubNamesById(double user_id) {
		System.out.println("*****QQ******userid : "+user_id);
		String sql="select * from hubs";// where region_id=(select region_id from hubs where hub_id=(select hub_id from user_hub where account_no=(select account_no from `user` where user_id=?)))";
		List<com.dataisys.taykit.model.Hub> ListOfHubNames=getJdbcTemplate().query(sql, new HubRowMapper());
				
			
		System.out.println("********QQ**********: "+ListOfHubNames);

		return ListOfHubNames;
	}

	public List<Hub> getHubNamesByIdForSA(double user_id) {
		System.out.println("*****QQ******userid : "+user_id);
		//String sql="select * from hubs where region_id=(select region_id from hubs where hub_id=(select hub_id from user_hub where account_no=(select account_no from `user` where user_id=?)))";
		String sql="select * from hubs where hub_id=(select hub_id from store where store_id=(select store_id from user_store where account_no=(select account_no from `user` where user_id=?)))";
		List<com.dataisys.taykit.model.Hub> ListOfHubNames=getJdbcTemplate().query(sql,new Object[] {user_id}, new HubRowMapper());
				
			
		System.out.println("********QQ**********: "+ListOfHubNames);

		return ListOfHubNames;
	}

	
}
