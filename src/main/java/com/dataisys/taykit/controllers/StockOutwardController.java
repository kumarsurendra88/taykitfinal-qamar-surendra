package com.dataisys.taykit.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.model.StockOutward;

@RestController
public class StockOutwardController{
	@Autowired
	OrderDao orderDao;
	@RequestMapping(value="/transferStock")
	public void transferStock(@RequestBody StockOutward obj) {
		System.out.println("Got object"+obj);
		System.out.println("From: "+obj.getFromLocation());
		System.out.println("To: "+obj.getToLocation());
		System.out.println("TrackingNumber: "+obj.getTrackingId());
		System.out.println("PersonName: "+obj.getPersonName());
		System.out.println("Person Contact: "+obj.getPersonNo());
		orderDao.transferStock(obj);
	}
	
	@RequestMapping(value="/returnToRetailer")
	public void transferReturn(@RequestBody StockOutward obj){
		System.out.println("Got object"+obj);
		System.out.println("From: "+obj.getFromLocation());
		System.out.println("To: "+obj.getToLocation());
		System.out.println("TrackingNumber: "+obj.getTrackingId());
		System.out.println("PersonName: "+obj.getPersonName());
		System.out.println("Person Contact: "+obj.getPersonNo());
		obj.setOrderStatus("Order Cancelled");
		//orderDao.transferToRetailer(obj);
	}
	
}
