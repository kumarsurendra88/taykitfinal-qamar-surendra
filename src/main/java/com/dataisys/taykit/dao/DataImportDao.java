package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.DataImport;

public interface DataImportDao {

	public void dataImport(DataImport data);
	public List<DataImport> getAll(HttpSession session);
	public List<DataImport> getUnderProcess(HttpSession session);
	public List<DataImport> getInvalid(HttpSession session);
	public void assignStore(List<DataImport> list,HttpSession session,int storeId);
	public List<DataImport> getUnassignedStore(HttpSession session);
	public void moveOrders(HttpSession session);
	public void markMoved(double dataId);
	public void assignStoreByTrackingId(List<String> list, HttpSession session,
			int id);
}
