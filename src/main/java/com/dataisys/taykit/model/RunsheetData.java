package com.dataisys.taykit.model;

import java.sql.Date;

public class RunsheetData {

private String runsheetNo;
private double totalShipment;
private double totalPrepaid;
private double totalCodOrders;
private double totalCod;
private String shortFallId;
private String codStatus;
private String deliveryStatus;
private Date runSheetDate;
private String deliveryBoy;
private String storeRunSheetNumber;
private double codDelivered;
private double prepaidDelivered;



public double getCodDelivered() {
	return codDelivered;
}

public void setCodDelivered(double codDelivered) {
	this.codDelivered = codDelivered;
}

public double getPrepaidDelivered() {
	return prepaidDelivered;
}

public void setPrepaidDelivered(double prepaidDelivered) {
	this.prepaidDelivered = prepaidDelivered;
}

public String getStoreRunSheetNumber() {
	return storeRunSheetNumber;
}

public void setStoreRunSheetNumber(String storeRunSheetNumber) {
	this.storeRunSheetNumber = storeRunSheetNumber;
}

public RunsheetData(){
	
}

public RunsheetData(String runsheetNo, double totalShipment,
		double totalPrepaid, double totalCod, String shortFallId,
		String codStatus, String deliveryStatus, Date runSheetDate) {
	super();
	this.runsheetNo = runsheetNo;
	this.totalShipment = totalShipment;
	this.totalPrepaid = totalPrepaid;
	this.totalCod = totalCod;
	this.shortFallId = shortFallId;
	this.codStatus = codStatus;
	this.deliveryStatus = deliveryStatus;
	this.runSheetDate = runSheetDate;
}





public double getTotalCodOrders() {
	return totalCodOrders;
}

public void setTotalCodOrders(double totalCodOrders) {
	this.totalCodOrders = totalCodOrders;
}

public String getDeliveryBoy() {
	return deliveryBoy;
}

public void setDeliveryBoy(String deliveryBoy) {
	this.deliveryBoy = deliveryBoy;
}

public String getRunsheetNo() {
	return runsheetNo;
}

public void setRunsheetNo(String runsheetNo) {
	this.runsheetNo = runsheetNo;
}

public double getTotalShipment() {
	return totalShipment;
}

public void setTotalShipment(double totalShipment) {
	this.totalShipment = totalShipment;
}

public double getTotalPrepaid() {
	return totalPrepaid;
}

public void setTotalPrepaid(double totalPrepaid) {
	this.totalPrepaid = totalPrepaid;
}

public double getTotalCod() {
	return totalCod;
}

public void setTotalCod(double totalCod) {
	this.totalCod = totalCod;
}

public String getShortFallId() {
	return shortFallId;
}

public void setShortFallId(String shortFallId) {
	this.shortFallId = shortFallId;
}

public String getCodStatus() {
	return codStatus;
}

public void setCodStatus(String codStatus) {
	this.codStatus = codStatus;
}

public String getDeliveryStatus() {
	return deliveryStatus;
}

public void setDeliveryStatus(String deliveryStatus) {
	this.deliveryStatus = deliveryStatus;
}

public Date getRunSheetDate() {
	return runSheetDate;
}

public void setRunSheetDate(Date runSheetDate) {
	this.runSheetDate = runSheetDate;
}



}
