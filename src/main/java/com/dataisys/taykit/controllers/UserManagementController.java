package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;














import com.dataisys.taykit.dao.AreasDao;
import com.dataisys.taykit.dao.HubDAO;
import com.dataisys.taykit.dao.RegionDAO;
import com.dataisys.taykit.dao.StoreContactDao;
import com.dataisys.taykit.dao.ZoneDAO;
import com.dataisys.taykit.factories.ProductCategoryFactory;
import com.dataisys.taykit.factories.StoreContactFactory;
import com.dataisys.taykit.factories.StoreServicesFactory;
import com.dataisys.taykit.model.Areas;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.Zone;

@RestController
public class UserManagementController {

	@Autowired
	HubDAO hubDao;
	@Autowired
	ZoneDAO zoneDao;
	@Autowired
	RegionDAO regionDao;
	@Autowired
	StoreContactDao storecontactdao;
	
	@RequestMapping(value="/CreatZone/{zone_name}")
	public boolean CreateZone(@PathVariable("zone_name") String zone_name){
		System.out.println("Got zone_name : "+zone_name);
		return zoneDao.CreateZone(zone_name);
	}
	
	@RequestMapping(value="/CreatRegion/{zone_id},{region_name},{region_desc}")
	public boolean CreatRegion(@PathVariable("zone_id")int zone_id,@PathVariable("region_name")String region_name,@PathVariable("region_desc")String region_desc){
		System.out.println("Got zone_id : "+zone_id);
		System.out.println("Got region_name : "+region_name);
		System.out.println("Got region_desc : "+region_desc);
		return regionDao.CreatRegion(zone_id,region_name,region_desc);
	}
	
	@RequestMapping(value="/CreatHub/{region_id},{hub_name},{hub_desc}")
	public boolean CreatHub(@PathVariable("region_id")int region_id,@PathVariable("hub_name")String hub_name,@PathVariable("hub_desc")String hub_desc){
		System.out.println("inside create hub");
		System.out.println("Got region_id : "+region_id);
		System.out.println("Got hub_name : "+hub_name);
		System.out.println("Got hub_desc : "+hub_desc);
		return hubDao.CreatHub(region_id, hub_name, hub_desc);
	}
	
	@RequestMapping(value="/getZoneNames")
	public List<Zone> getZoneNames(){
		System.out.println("inside controller");
		return zoneDao.getZoneNames();
	}
	
	
	
	@RequestMapping(value="/getZoneNamesByID/{accNumber}", method=RequestMethod.GET)

	public String  getZoneNamesByID(@PathVariable("accNumber")String accNumber) throws IOException{
		System.out.println("***********accNumber : inside controller  : "+accNumber);
		String ZoneName=zoneDao.getZoneNamesByID(accNumber);
	System.out.println("ZoneNames : "+ZoneName);
	   return ZoneName;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	
	
	
	@RequestMapping(value="/getRegionNames/{zone_id}", method=RequestMethod.GET)

	public List<Region>  getRegionNames(@PathVariable("zone_id")int zone_id) throws IOException{
		System.out.println("***********zone_id : inside controller  : "+zone_id);
		List <Region> regionNames=regionDao.getRegionByZoneId(zone_id);
	System.out.println("regionNames : "+regionNames);
	   return regionNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	@RequestMapping(value="/getRegionNamesByZone_Name/{zone_name}", method=RequestMethod.GET)

	public List<Region>  getRegionNamesByZone_Name(@PathVariable("zone_name")String zone_name) throws IOException{
		System.out.println("***********zone_id : inside controller  : "+zone_name);
		List <Region> regionNames=regionDao.getRegionByZoneName(zone_name);
	System.out.println("regionNames : "+regionNames);
	   return regionNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	
	@RequestMapping(value="/getHubNamesByRegion_Name/{region_name}", method=RequestMethod.GET)

	public List<Hub>  getHubNamesByRegion_Name(@PathVariable("region_name")String region_name) throws IOException{
		System.out.println("***********zone_id : inside controller  : "+region_name);
		List<Hub> hubNames=hubDao.getHubByRegionName(region_name);
	System.out.println("regionNames : "+hubNames);
	   return hubNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	
	@RequestMapping(value="/getStoreNamesByHub_Name/{hub_name}", method=RequestMethod.GET)

	public List<Store>  getStoreNamesByHub_Name(@PathVariable("hub_name")String hub_name) throws IOException{
		System.out.println("********qq***hub_name : inside controller  : "+hub_name);
		List<Store> StoreNames=storecontactdao.getStoreByHubName(hub_name);
	System.out.println("storeNames : "+StoreNames);
	   return StoreNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	@RequestMapping(value="/getStoreNamesById/{user_id}", method=RequestMethod.GET)

	public List<Store>  getStoreNamesByAccNo(@PathVariable("user_id")double user_id) throws IOException{
		System.out.println("********qq***hub_name : inside controller  : "+user_id);
		List<Store> StoreNames=storecontactdao.getStoreByAccNo(user_id);
	System.out.println("storeNames : "+StoreNames);
	   return StoreNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	
	@RequestMapping(value="/getHubNamesById/{user_id}", method=RequestMethod.GET)

	public List<Hub>  getHubNamesById(@PathVariable("user_id")double user_id) throws IOException{
		System.out.println("********qq***hub_name : inside controller  : "+user_id);
		List<Hub> HubNames=storecontactdao.getHubNamesById(user_id);
	System.out.println("storeNames : "+HubNames);
	   return HubNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
	@RequestMapping(value="/getHubNamesByIdForSA/{user_id}", method=RequestMethod.GET)

	public List<Hub>  getHubNamesByIdForSA(@PathVariable("user_id")double user_id) throws IOException{
		System.out.println("********qq***hub_name : inside controller  : "+user_id);
		List<Hub> HubNames=storecontactdao.getHubNamesByIdForSA(user_id);
	System.out.println("hubNames : "+HubNames);
	   return HubNames;//new ModelAndView("getAreasNames", "areasName", areaNames);

	}
@RequestMapping(value="/getHubs" , method=RequestMethod.GET)
	
	public List<com.dataisys.taykit.model.Hub> getHubs() throws IOException{
		
		List<com.dataisys.taykit.model.Hub> hubs= hubDao.getHubs();
		/*System.out.println("list : "+users.get(0));
		System.out.println("list : "+users.get(1));
		System.out.println("list : "+users.get(2));
		System.out.println("list : "+users.get(3));*/
		return hubs;
	}

@RequestMapping(value="/getRegions" , method=RequestMethod.GET)

public List<com.dataisys.taykit.model.Region> getRegions() throws IOException{
	
	List<com.dataisys.taykit.model.Region> regions= regionDao.getRegions();
System.out.println("list : "+regions);
	
	return regions;
}
@RequestMapping(value="/UpdateHubById/{id}" , method=RequestMethod.POST,headers="Content-Type=application/json")	
public void UpdateHub(@RequestBody Hub hub,@PathVariable ("id") double id){
	 System.out.println("hub : "+hub);	
	 
	hubDao.updateHub(hub,id);
	
}
@RequestMapping(value="/getHubById/{id}" , method=RequestMethod.GET)	
public Hub getHubById(@PathVariable("id") double id){
	System.out.println("inside controller");
	return hubDao.getHubById(id);
}


@RequestMapping(value="/getRegionList" , method=RequestMethod.GET)

public List<com.dataisys.taykit.model.Region> getRegionList() throws IOException{
	
	List<com.dataisys.taykit.model.Region> regions= regionDao.getRegionList();
System.out.println("list : "+regions);
	
	return regions;
}

@RequestMapping(value="/getRegionsById/{id}" , method=RequestMethod.GET)	
public Region getRegionsById(@PathVariable("id") int id){
	System.out.println("inside controller");
	return regionDao.getRegionsById(id);
}
@RequestMapping(value="/UpdateRegionById/{id}" , method=RequestMethod.POST,headers="Content-Type=application/json")	
public void UpdateHub(@RequestBody Region region,@PathVariable ("id") int id){
	 System.out.println("region : "+region);	
	 
	regionDao.updateRegion(region,id);
	
}
}
