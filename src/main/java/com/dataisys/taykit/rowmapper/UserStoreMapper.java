package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.mobile.model.UserStore;

public class UserStoreMapper implements RowMapper<UserStore> {

	public UserStore mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserStore userStore = new UserStore();
		userStore.setUserStoreId(rs.getDouble("userstore_id"));
		userStore.setUserId(rs.getDouble("user_id"));
		userStore.setStoreId(rs.getDouble("store_id"));
		userStore.setUpdatedBy(rs.getString("updated_by"));
		userStore.setUpdatedOn(rs.getTimestamp("updated_on"));
		return userStore;
	}

}
