var routerApp = angular.module('routerApp', [ 'ui.router', 'ngGrid',
		'ngResource', 'ui.bootstrap', 'ngTouch', 'ui.grid',
		'ui.grid.pagination','ui.grid.selection','ui.grid.exporter','ui.grid.resizeColumns','ui.grid.pagination','barcode','dynamicMenu' ]);


routerApp.factory('DeliveryBoys', function($resource) {
	"use strict";
	return $resource('./getDeliveryBoy', {}, {
		update : {
			method : 'PUT'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});


//hiiiiiii live app angular

routerApp.factory('deliveryBoyId', function($resource) {
	"use strict";
	return $resource('./deliveryboyid/get', {}, {
		update : {
			method : 'PUT'
		},
		query : {
			method : 'GET',
			isArray: true
		}
	});
});
routerApp.factory('AmountTopay', function($resource) {
	"use strict";
	return $resource('./Amount/get', {}, {
		update : {
			method : 'PUT'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
routerApp.directive('uiBlur', function() {
	return function(scope, elem, attrs) {
		elem.bind('blur', function() {
			scope.$apply(attrs.uiBlur);
		});
	};
});
/*routerApp.directive('onKeyupFn', function() {
 return function(scope, elem, attrs) {
 elem.bind('onKeyUp', function() {
 scope.$apply(attrs.onKeyupFn);
 });
 }
 });*/
routerApp.directive('onKeyupFn', function() {
	return function(scope, elm, attrs) {
		var keyupFn = scope.$eval(attrs.onKeyupFn);
		elm.bind('keyup', function(evt) {
			scope.$apply(function() {
				keyupFn.call(scope, evt.which);
			});
		});
	};
});
routerApp.factory('TotalCash', function() {
	var total = [ {
		tot : 0
	} ];
	return {
		gettot : function() {
			return total.tot;
		},
		settot : function(totl) {
			total.tot = totl;

		}
	};
});
routerApp.factory('denominationcheck', function() {
	var flag = [ {
		check : 0
	} ];
	return {
		getflag : function() {
			return flag.check;
		},
		setflag : function(value) {
			flag.check = value;

		}
	};
});
routerApp.factory('PayPayment', function($resource) {
	"use strict";
	return $resource('./PaymentCollection/pay', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
//factory for setting Denominations
routerApp.factory('denomination', function($resource) {
	"use strict";
	return $resource('./denom.do', {}, {
		update : {
			method : 'PUT'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});

routerApp
		.factory(
				'updateFactoryOther',
				function() {
					var oderId;

					return {
						setId : function(id) {
							console
									.log("Inside setId() of updateFactory and the Id is: "
											+ id);
							oderId = id;
						},
						getId : function() {
							console
									.log("Inside getId() of updateFactory and the Id is: "
											+ oderId);
							return oderId;
						}
					};
				});





routerApp.directive('barcodeGenerator', [function() {
    var Barcode	= (function () {
        var barcode	= {
            settings: {
                barWidth:		1,
                barHeight:		50,
                moduleSize:		5,
                showHRI:		false,
                addQuietZone:	true,
                marginHRI:		5,
                bgColor:		"#FFFFFF",
                color:			"#000000",
                fontSize:		10,
                posX:			0,
                posY:			0
            },
            intval: function(val) {
                var type	= typeof(val);
                if ( type == 'string' ) {
                    val = val.replace(/[^0-9-.]/g, "");
                    val = parseInt(val * 1, 10);
                    return isNaN(val) || !isFinite(val)? 0: val;
                }
                return type == 'number' && isFinite(val)? Math.floor(val): 0;
            },
            code128: {
                encoding:[
                    "11011001100", "11001101100", "11001100110", "10010011000",
                    "10010001100", "10001001100", "10011001000", "10011000100",
                    "10001100100", "11001001000", "11001000100", "11000100100",
                    "10110011100", "10011011100", "10011001110", "10111001100",
                    "10011101100", "10011100110", "11001110010", "11001011100",
                    "11001001110", "11011100100", "11001110100", "11101101110",
                    "11101001100", "11100101100", "11100100110", "11101100100",
                    "11100110100", "11100110010", "11011011000", "11011000110",
                    "11000110110", "10100011000", "10001011000", "10001000110",
                    "10110001000", "10001101000", "10001100010", "11010001000",
                    "11000101000", "11000100010", "10110111000", "10110001110",
                    "10001101110", "10111011000", "10111000110", "10001110110",
                    "11101110110", "11010001110", "11000101110", "11011101000",
                    "11011100010", "11011101110", "11101011000", "11101000110",
                    "11100010110", "11101101000", "11101100010", "11100011010",
                    "11101111010", "11001000010", "11110001010", "10100110000",
                    "10100001100", "10010110000", "10010000110", "10000101100",
                    "10000100110", "10110010000", "10110000100", "10011010000",
                    "10011000010", "10000110100", "10000110010", "11000010010",
                    "11001010000", "11110111010", "11000010100", "10001111010",
                    "10100111100", "10010111100", "10010011110", "10111100100",
                    "10011110100", "10011110010", "11110100100", "11110010100",
                    "11110010010", "11011011110", "11011110110", "11110110110",
                    "10101111000", "10100011110", "10001011110", "10111101000",
                    "10111100010", "11110101000", "11110100010", "10111011110",
                    "10111101110", "11101011110", "11110101110", "11010000100",
                    "11010010000", "11010011100", "11000111010"
                ],
                getDigit: function(code) {
                    var tableB	= " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
                    var result	= "";
                    var sum		= 0;
                    var isum	= 0;
                    var i		= 0;
                    var j		= 0;
                    var value	= 0;

                    // check each characters
                    for(i=0; i<code.length; i++){
                        if (tableB.indexOf(code.charAt(i)) == -1){
                            return("");
                        }
                    }

                    // check firsts characters : start with C table only if enought numeric
                    var tableCActivated = code.length > 1;
                    var c = '';

                    for (i=0; i<3 && i<code.length; i++) {
                        c = code.charAt(i);
                        tableCActivated &= c >= '0' && c <= '9';
                    }

                    sum	= tableCActivated ? 105 : 104;

                    // start : [105] : C table or [104] : B table
                    result = this.encoding[ sum ];

                    i = 0;
                    while ( i < code.length ) {
                        if ( !tableCActivated) {
                            j = 0;
                            // check next character to activate C table if interresting
                            while ( (i + j < code.length) && (code.charAt(i+j) >= '0') && (code.charAt(i+j) <= '9') ) {
                                j++;
                            }

                            // 6 min everywhere or 4 mini at the end
                            tableCActivated = (j > 5) || ((i + j - 1 == code.length) && (j > 3));

                            if ( tableCActivated ){
                                result += this.encoding[ 99 ]; // C table
                                sum += ++isum * 99;
                            } //		 2 min for table C so need table B
                        } else if ( (i == code.length) || (code.charAt(i) < '0') || (code.charAt(i) > '9') || (code.charAt(i+1) < '0') || (code.charAt(i+1) > '9') ) {
                            tableCActivated = false;
                            result += this.encoding[ 100 ]; // B table
                            sum += ++isum * 100;
                        }

                        if ( tableCActivated ) {
                            value = barcode.intval(code.charAt(i) + code.charAt(i+1)); // Add two characters (numeric)
                            i += 2;
                        } else {
                            value = tableB.indexOf( code.charAt(i) ); // Add one character
                            i += 1;
                        }
                        result	+= this.encoding[ value ];
                        sum += ++isum * value;
                    }

                    result += this.encoding[sum % 103];// Add CRC
                    result += this.encoding[106];// Stop
                    result += "11";// Termination bar

                    return(result);
                }
            },
            bitStringTo2DArray: function( digit) {//convert a bit string to an array of array of bit char
                var d = [];
                d[0] = [];

                for ( var i=0; i<digit.length; i++) {
                    d[0][i] = digit.charAt(i);
                }

                return(d);
            },
            digitToCssRenderer: function( $container, settings, digit, hri, mw, mh, type) {// css barcode renderer
                var lines = digit.length;
                var columns = digit[0].length;
                var content = "";
                var len, current;
                var bar0 = "<td class='w w%s' ></td>";
                var bar1 = "<td class='b b%s' ></td>";

                for ( var y=0, x; y<lines; y++) {
                    len = 0;
                    current = digit[y][0];

                    for ( x=0; x<columns; x++){
                        if ( current == digit[y][x] ) {
                            len++;
                        } else {
                            content += (current == '0'? bar0: bar1).replace("%s", len * mw);
                            current = digit[y][x];
                            len=1;
                        }
                    }
                    if ( len > 0) {
                        content += (current == '0'? bar0: bar1).replace("%s", len * mw);
                    }
                }

                if ( settings.showHRI) {
                    content += "<td style=\"clear:both; width: 100%; background-color: " + settings.bgColor + "; color: " + settings.color + "; text-align: center; font-size: " + settings.fontSize + "px; margin-top: " + settings.marginHRI + "px;\">"+hri+"</td>";
                }

                var td = document.createElement('td');
                td.innerHTML = content;
                td.className = 'barcode '+ type +' clearfix-child';
                return td;
            },
            digitToCss: function($container, settings, digit, hri, type) {// css 1D barcode renderer
                var w = barcode.intval(settings.barWidth);
                var h = barcode.intval(settings.barHeight);

                return this.digitToCssRenderer($container, settings, this.bitStringTo2DArray(digit), hri, w, h, type);
            }
        };

        var generate	= function(datas, type, settings) {
            var
                digit	= "",
                hri		= "",
                code	= "",
                crc		= true,
                rect	= false,
                b2d		= false
                ;

            if ( typeof(datas) == "string") {
                code = datas;
            } else if (typeof(datas) == "object") {
                code	= typeof(datas.code) == "string" ? datas.code : "";
                crc		= typeof(datas.crc) != "undefined" ? datas.crc : true;
                rect	= typeof(datas.rect) != "undefined" ? datas.rect : false;
            }

            if (code == "") {
                return(false);
            }

            if (typeof(settings) == "undefined") {
                settings = [];
            }

            for( var name in barcode.settings) {
                if ( settings[name] == undefined) {
                    settings[name] = barcode.settings[name];
                }
            }

            switch (type) {
                case "std25":
                case "int25": {
                    digit = barcode.i25.getDigit(code, crc, type);
                    hri = barcode.i25.compute(code, crc, type);
                    break;
                }
                case "ean8":
                case "ean13": {
                    digit = barcode.ean.getDigit(code, type);
                    hri = barcode.ean.compute(code, type);
                    break;
                }
                case "upc": {
                    digit = barcode.upc.getDigit(code);
                    hri = barcode.upc.compute(code);
                    break;
                }
                case "code11": {
                    digit	= barcode.code11.getDigit(code);
                    hri	= code;
                    break;
                }
                case "code39": {
                    digit = barcode.code39.getDigit(code);
                    hri = code;
                    break;
                }
                case "code93": {
                    digit = barcode.code93.getDigit(code, crc);
                    hri = code;
                    break;
                }
                case "code128": {
                    digit = barcode.code128.getDigit(code);
                    hri = code;
                    break;
                }
                case "codabar": {
                    digit = barcode.codabar.getDigit(code);
                    hri = code;
                    break;
                }
                case "msi": {
                    digit = barcode.msi.getDigit(code, crc);
                    hri = barcode.msi.compute(code, crc);
                    break;
                }
                case "datamatrix": {
                    digit = barcode.datamatrix.getDigit(code, rect);
                    hri = code;
                    b2d = true;
                    break;
                }
            }

            if ( digit.length == 0) {
                return this;
            }

            if ( !b2d && settings.addQuietZone) {
                digit = "0000000000" + digit + "0000000000";
            }

            var fname = 'digitToCss' + (b2d ? '2D' : '');

            return barcode[fname](this, settings, digit, hri, type);
        };

        return generate;
    }());

    return {
        link: function(scope, element, attrs) {
            attrs.$observe('barcodeGenerator', function(value){
                var code = Barcode(value, "code128",{barWidth:2}),
                    code_wrapper = angular.element("<div class='barcode code128'></div>")

                code_wrapper.append(code);
                angular.element(element).html('').append(code_wrapper);

            });
        }
    }
}]);









routerApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/product');

	$stateProvider

	.state('home', {
		url : '/home',
		templateUrl : 'partialviews/dashboard1.html',
	//controller:'myDomDirective'
	}).state('returnsrunsheet', {
		url : '/returnsrunsheet',
		templateUrl : 'partialviews/returnrunsheet.html',
	//controller:'myDomDirective'
	}).state('cancelOrder', {
		url : '/cancelOrder',
		templateUrl : 'partialviews/cancel/cancel.html',
		controller:'cancelOrderController'
	})
	
	.state('pendingReturn', {
		url : '/pendingReturn',
		templateUrl : 'partialviews/cancel/pendingReturn.html',
		controller:'pendingReturnController'
	})
	
	.state('pendingRto', {
		url : '/pendingRto',
		templateUrl : 'partialviews/cancel/pendingRTO.html',
		controller:'pendingRTOController'
	})
	.state('rtoDispatch', {
		url : '/rtoDispatch',
		templateUrl : 'partialviews/cancel/rtoDispatch.html',
		controller:'rtoDispatchController'
	})
	.state('pendingReturn2', {
		url : '/pendingReturn2',
		templateUrl : 'partialviews/cancel/pendingReturn2.html',
		controller:'pendingReturn2Controller'
	})
	
	.state('assigntostore', {
		url : '/assigntostore',
		templateUrl : 'partialviews/storereturnassign.jsp',
	//controller:'myDomDirective'
	})

	.state('deleteuser', {

		url : '/deleteuser',
		templateUrl : 'partialviews/deleteuser.html',
	/*controller: 'customersController'*/
	/*controller: 'customersController1'*/

	}).state('resetpassword', {

		url : '/resetpassword',
		templateUrl : 'partialviews/resetpassword.html'
	/*controller: 'customersController'*/
	/*controller: 'customersController1'*/

	}).state('createuser', {

		url : '/createuser',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/createuser.html',
		controller : 'productController'
	/*templateUrl: 'Estimationscreen.html',
	controller: 'MainCtrl' ,*/

	}).state('users', {

		url : '/users',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/users.html',
		controller : 'userController'

	}).state('outscanMasterBag', {

		url : '/outscanMasterBag',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/OutScanToMasterBag.jsp',
	//	controller : 'userController'
	
	
	}).state('attachments', {

		url : '/attachments',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/attachments.html',
		controller : 'attachmentController'

	}).state('addcustomer', {

		url : '/addcustomer',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/addcustomer.html'

	})

	.state('customer', {

		url : '/customer',
		templateUrl : 'grid.html',
		templateUrl : 'partialviews/customer.html',
		controller : 'customerController'

	}).state('updateCustomer', {

		url : '/updateCustomer',
		templateUrl : 'partialviews/updateCustomer.html'

	})
	.state('updateCustomerByPhone', {
        url : '/updateCustomerByPhone',
        templateUrl : 'partialviews/updateCustomer.html',
        controller:'updateCustomerController'
    })
	.state('CreateOrderAPI', {

		url : '/CreateOrderAPI',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'API_UI.html'

	})

	.state('createorder', {

		url : '/createorder',

		/* templateUrl: 'partialviews/createorder.html'*/
		templateUrl : 'partialviews/createorder.html',
		controller : 'createOrderController'
	/* controller: dummyController*/
	/*controller:'customerController'*/

	}).state('updateorder', {

		url : '/updateorder',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/updateorder.html',
		controller : 'Distributor3Controller'
	/*controller:'createOrderController'*/

	}).state('order', {

		url : '/order',
		/* templateUrl: 'grid.html',*/
		templateUrl : 'partialviews/orderactivity.html',
		controller : 'commentsController'

	}).state('comments', {
			
		url : '/comments',
		templateUrl : 'partialviews/comments.html',

	}).state('customercalling', {
			
		url : '/customercalling',
		templateUrl : 'partialviews/customercalling.html',

	})

	/*.state('imporders', {

		url : '/imporders',
		templateUrl : 'partialviews/dummy.html',

	}).*/.state('product123', {

		url : '/product123',
		templateUrl : 'partialviews/product1.html'
	/*controller:'productController',
		controller:'myFileCtrl'*/

	})

	.state('bulkassign', {

		url : '/bulkassign',
		templateUrl : 'partialviews/bulkassign1.html',
		/*controller : 'bulkAssign'*/

			
	})
	.state('deliveredSummary', {

			url : '/deliveredSummary',
			templateUrl : 'partialviews/deliveredSummary.html',
			//controller : 'bulkAssign'

			})
			
			
.state('orderSearch', {

url : '/orderSearch',
templateUrl : 'partialviews/searchOrder.html',
//controller : 'bulkAssign'

})
.state('assignStoreReturn', {
                                url : '/assignStoreReturn',
                                templateUrl : 'partialviews/assignStoreReturn.html',
                                controller: 'assignStoreReturn'
                            })
.state('orderRecent', {

url : '/orderSummaryRecent',
templateUrl : 'partialviews/orderSummaryRecent.html',
//controller : 'bulkAssign'

}).state('stores', {

		url : '/stores',
		templateUrl : 'partialviews/stores.html',

	}).state('createstore', {

		url : '/createstore',
		templateUrl : 'partialviews/createstore.html',

	})



	.state('deletestore', {

		url : '/deletestore',
		templateUrl : 'partialviews/deletestore.html',

	})

	.state('product', {

		url : '/product',
		templateUrl : 'partialviews/emptypage.html',
		controller : 'updateOrderController',
		controller : 'myFileCtrl'

	}).state('importdata', {

		url : '/importdata',
		templateUrl : 'partialviews/importdata.html',

	})

	.state('deposit', {

		url : '/deposits',
		templateUrl : 'partialviews/deposit.html',
		controller : 'depositController'

	}).state('updatereturnstatus', {

		url : '/updatereturnstatus',
		templateUrl : 'partialviews/updatereturnstatus.html',
		controller : 'depositController'

	})


	.state('addrole', {

		url : '/addrole',
		templateUrl : 'partialviews/addrole.html',

	}).state('roledetail', {

		url : '/roledetail',
		templateUrl : 'partialviews/roledetail.html',

	}).state('adduser', {

		url : '/adduser',
		templateUrl : 'partialviews/adduser.html',
		
		

	})
	.state('CreatHub', {

		url : '/CreatHub',
		templateUrl : 'partialviews/AddHub.jsp',
	
	})
	
	
	.state('CreatRegion', {

		url : '/CreatRegion',
		templateUrl : 'partialviews/AddRegion.jsp',

	})
	.state('CreatZone', {

		url : '/CreatZone',
		templateUrl : 'partialviews/AddZone.jsp',
		
	})
	.state('CreateMasterBag', {

		url : '/CreateMasterBag',
		templateUrl : 'partialviews/CreateMasterBag.jsp',
		
	})
	.state('InwardMasterBag', {

		url : '/InwardMasterBag',
		templateUrl : 'partialviews/InwardMasterBag.jsp',
		
	})
	
	.state('userdetail', {

		url : '/userdetail',
		templateUrl : 'partialviews/userdetail.jsp',

	}).state('attempts', {

		url : '/attempts',
		templateUrl : 'partialviews/attempts.html',

	})
	.state('orderactivity', {

		url : '/orderactivity',
		templateUrl : 'partialviews/orderact.html',

	})


	.state('category', {

		url : '/category',
		templateUrl : 'partialviews/category.html',

	}).

	state('city', {

		url : '/city',
		templateUrl : 'partialviews/city.html',

	}).

	state('area', {

		url : '/area',
		templateUrl : 'partialviews/area.html',

	}).

	state('orderupdate', {

		url : '/orderupdate',
		templateUrl : 'partialviews/orderupdate.html',
		controller : 'updateOrderControllerNew',

	}).state('return', {

		url : '/return',
		templateUrl : 'partialviews/return.html',
		controller : 'updateOrderControllerNew',

	}).state('returns', {

		url : '/returns',
		templateUrl : 'partialviews/returns.html',
		//controller : 'returns',

	})

	.state('returnDetail', {
             
           	url: '/returnDetail',
                templateUrl: 'partialviews/returnDetail.html',
                controller : 'returnUpdate'
                
               })
	.state('stocks', {

		url : '/stocks',
		templateUrl : 'partialviews/stocks.html',
		controller : 'stocks',

	
}).state('cashtransfer', {
	url : '/cashtransfer',
	templateUrl : 'partialviews/cashtransferdetails.html',
	controller : 'cashTranferDetailsController'

})

.state('addStore', {
	url : '/addStore',
	templateUrl : 'partialviews/addstore.jsp',
	controller : 'addStoreController'

}).state('retailer', {

	url : '/retailer',
	templateUrl : 'partialviews/retailer.html',
	controller : 'retailerController'

})

.state('addretailer', {

	url : '/addretailer',
	templateUrl : 'partialviews/addretailer.html',
})
.state('retailerupdate', {

		url : '/retailerupdate',
		templateUrl : 'partialviews/updateretailer.html',

	})
	.state('storeupdate', {

		url : '/storeupdate',
		templateUrl : 'partialviews/updatestore.html',

	})
	.state('hubupdate', {

		url : '/hubupdate',
		templateUrl : 'partialviews/updatehub.html',

	})
	.state('regionupdate', {

		url : '/regionupdate',
		templateUrl : 'partialviews/updateregion.html',

	})
.state('transfercash', {

	url : '/transfercash',
	templateUrl : 'partialviews/transfercash.html',
	controller : 'transfercash'

}).state('addretailercontact', {

	url : '/addretailercontact',
	templateUrl : 'partialviews/addretailerContact.html',

}).state('addretailergodown', {
	url : '/addretailergodown',
	templateUrl : 'partialviews/addretailergodown.html',

}).state('paymentcollection', {

	url : '/PaymentCollection',
	templateUrl : 'partialviews/PaymentCollection.html',
	controller : 'PaymentCollection',

}).state('cashmanagment', {

	url : '/cashmanagment',
	templateUrl : 'partialviews/cashmanagment.html',
	controller : 'cashManagmentController',

}).state('devices', {

	url : '/devices',
	templateUrl : 'partialviews/devicemanagment.html',
	controller : 'deviceController',

}).state('hubmanagement', {

	url : '/hubmanagement',
	templateUrl : 'partialviews/HubManagement.html',
	controller : 'HubManagementController',
}).state('storemanagment', {

	url : '/storemanagment',
	templateUrl : 'partialviews/storemanagment.html',
	controller : 'StoreController',
})
.state('regionmanagement', {

	url : '/regionmanagement',
	templateUrl : 'partialviews/RegionManagement.html',
	controller : 'RegionManagementController',
})
.state('importdata.modal2', {
	url : '/step2',
	templateUrl : 'partialviews/modal2.html',
// controller:'stocks',

}).state('importdata.modal2.modal3', {
	url : '/step3',
	templateUrl : 'partialviews/modal3.html',
//controller:'stocks',

}).state('importdata.modal2.modal3.modal4', {
	url : '/step4',
	templateUrl : 'partialviews/modal4.html',
//controller:'stocks',

}).state('importdata.modal2.modal3.modal4.modal5', {
	url : '/step5',
	templateUrl : 'partialviews/modal5.html',
//controller:'stocks',

}).state('importdata.modal2.modal3.modal4.modal5.modal6', {
	url : '/step6',
	templateUrl : 'partialviews/modal6.html',
//controller:'stocks',

}).state('importdata.modal2.modal3.modal4.modal5.modal6.modal7', {
	url : '/step7',
	templateUrl : 'partialviews/modal7.html',
//controller:'stocks',

})
.state('inward', {

url : '/inward',
templateUrl : 'partialviews/InwardEntry.html',

})
.state('updateOrderStatus', {

url : '/updateOrderStatus',
templateUrl : 'partialviews/updateOrderStatus.html',

}).state('RunsheetDetails', {

	url : '/RunsheetDetails',
	templateUrl : 'partialviews/RunsheetDetails.html',

	})




.state('outward', {

url : '/outward',
templateUrl : 'partialviews/Transferparcel.html',

})
.state('transfertoretailer', {

url : '/transfertoretailer',
templateUrl : 'partialviews/Transfertoretailer.jsp',

})
.state('runsheet', {

url : '/runsheet',
templateUrl : 'partialviews/testnew.html',



}).state('returnstodeliveryBoy', {

	url : '/returnstodeliveryBoy',
	templateUrl : 'partialviews/assignReturnsToDeliveryBoy.html',

	})
.state('downloadfile', {

url : '/downloadfile',
templateUrl : 'partialviews/runsheetsnumber.html',

}).state('downloadStorefiles', {

	url : '/downloadStorefiles',
	templateUrl : 'partialviews/runsheetdownloads.html',

	})
.state('storetoboy', {
            
          url: '/storetoboy',
               templateUrl: 'partialviews/storeToDeliveryBoy.jsp'
         /* templateUrl: 'partialviews/NewFile.jsp'*/
               
               
              }).state('storeassign', {

            	  url : '/storeassign',
            	  templateUrl : 'partialviews/storeassign.html',
            	  controller : 'storeAssign'

            	  })
            	   
                      	  .state('deliveryStatusUpdate', {

            	  url : '/deliveryStatusUpdate',
            	  templateUrl : 'partialviews/deliveryStatusUpdate.html',
            	 // controller : 'storeAssign'

            	  })
.state('deliveryStatusUpdate2', {

            	  url : '/deliveryStatusUpdate2',
            	  templateUrl : 'partialviews/deliveryStatusUpdate2.html',
            	 // controller : 'storeAssign'

            	  })
.state('deliveryStatusUpdateView', {

            	  url : '/deliveryStatusUpdateView',
            	  templateUrl : 'partialviews/deliveryStatusUpdateView.html',
            	 // controller : 'storeAssign'

            	  })
            	  
 .state('storeRunSheet', {

            	  url : '/storeRunSheet',
            	  templateUrl : 'partialviews/viewStoreRunSheet.html',
            	 // controller : 'storeAssign'

            	  })
  .state('storeRunSheet2', {

            	  url : '/storeRunSheet2',
            	  templateUrl : 'partialviews/viewStoreRunSheet1.html',
            	 // controller : 'storeAssign'

            	  })
.state('storeRunSheetView', {

            	  url : '/storeRunSheetView',
            	  templateUrl : 'partialviews/viewStoreRunSheetView.html',
            	 // controller : 'storeAssign'

            	  })
.state('storeRunSheetCash', {

            	  url : '/storeRunSheetCash',
            	  templateUrl : 'partialviews/storeRunSheetCash.html',
            	 // controller : 'storeAssign'

            	  })
.state('storeRunSheetCash2', {

            	  url : '/storeRunSheetCash2',
            	  templateUrl : 'partialviews/storeRunSheetCash2.html',
            	 // controller : 'storeAssign'

            	  })  	  
            	  .state('runsheetforstore', {

                	  url : '/runsheetforstore',
                	  templateUrl : 'partialviews/StoreRunsheet.html',
                	 // controller : 'storeAssign'

                	  })
                	  
                	  .state('returnsstorerunsheet', {

                	  url : '/returnsstorerunsheet',
                	  templateUrl : 'partialviews/storerunsheetforreturns.jsp',
                	 // controller : 'storeAssign'

                	  })
                	  .state('listofrunsheets', {

                	  url : '/listofrunsheets',
                	  templateUrl : 'partialviews/runsheetsnumber.html',
                	 // controller : 'storeAssign'

                	  }) .state('listofstorerunsheets', {

                    	  url : '/listofstorerunsheets',
                    	  templateUrl : 'partialviews/storerunsheetlist.html',
                    	 // controller : 'storeAssign'

                    	  }).state('customerHistory', {
                    			url : '/customerHistory',
                    			templateUrl : 'partialviews/customerHistory.html',
                    		})
                    		
                    		.state('storeSummery', {
                    			url : '/storeSummery',
                    			templateUrl : 'partialviews/storesummery.html',
                    		})
                    		
                    		.state('storeDetailSummery', {
                    			url : '/storeDetailSummery',
                    			templateUrl : 'partialviews/detailstoresummery.html',
                    		})
                    			
                    		.state('bulkassignreturns', {
                    			url : '/bulkassignreturns',
                    			templateUrl : 'partialviews/returnsbulkassign.html',
                    		})


.state('callogs', {

	url : '/callogs',
	templateUrl : 'partialviews/callogs.html',

})
.state('storeReportSummary', {
    url: '/storeReportSummary',
    templateUrl : 'partialviews/store/storeReportSummary.html',
    controller: 'storeReportSummaryController'
})
.state('retailerSearch', {
    url: '/retailerSearch',
    templateUrl : 'partialviews/retailer/retailerSearch.html',
    controller: 'retailerSearchController'
})
.state('returnSummary1', {
    url: '/returnSummary1',
    templateUrl : 'partialviews/returnMainSummary1.html',
    controller: 'returnSummaryWitOrderStatus'
});
})

/*var routerApp = angular.module('routerApp', ['ngRoute','customersController']);

 routerApp.config(['$routeProvider',function($routeProvider) {
 $routeProvider.
 when('/home', {
 templateUrl: 'dashview.html',


 }).
 $routeProvider. 
 when('/about', {
 templateUrl: 'taskpage.html',
 resolve: resolveController('/js/controllers1/customersController.js')
 }).
 otherwise({
 redirectTo: '/home'

 });
 }]);*/
