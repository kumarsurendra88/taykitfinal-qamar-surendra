package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Return;

public class RandomReturnRowMapper implements RowMapper<Return> {

	public Return mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Return order= new Return();
		order.setRunSheetNo(rs.getString("runsheet_number"));
		order.setTrackingId(rs.getString("tracking_id"));
		//order.setRunSheetNumber(rs.getString("store_runsheet"));
		
		return order;
	}

}
