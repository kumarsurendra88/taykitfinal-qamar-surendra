package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Store;

public class AccountNoRowMapper implements RowMapper<Store> {

	public Store mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Store store= new Store();
		store.setStoreId(rs.getDouble("store_id"));
		return store;
	}

}
