package com.dataisys.taykit.forms;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UploadForm {

private CommonsMultipartFile file;

public CommonsMultipartFile getFile() {
return file;
}

public void setFile(CommonsMultipartFile file) {
this.file = file;
}	

}