package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.MasterBagDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.MasterBag;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Store;

@RestController
public class MasterBagController {
	@Autowired
	MasterBagDao masterbagDao;
	@RequestMapping(value="/CreateMasterBag/{destination},{masterbag_type},{type}",method=RequestMethod.POST)
	public boolean createMasterBag(@PathVariable("destination")String destination,@PathVariable("masterbag_type")String masterbag_type,@PathVariable("type")String type,HttpSession session){
		System.out.println("hii*(*((((*(*iii");
		
		System.out.println("**********obj :"+masterbag_type+"  "+destination+" : "+type);
		boolean flag=masterbagDao.createMasterBag(destination,masterbag_type,type,session);//userDao.createUser(user,session);
		return flag;//new ModelAndView("landingpage.html");
		
	}
	
	
	@RequestMapping(value="/getMasterBagListByIdAndType/{user_id},{masterbag_type}",method=RequestMethod.GET)
	public List<MasterBag> getMasterBagListByIdAndType(@PathVariable("user_id")String user_id,@PathVariable("masterbag_type")String masterbag_type,HttpSession session){
		System.out.println("inside masterbag controller");
		List<MasterBag> MasterbagList=masterbagDao.getMasterBagListById(user_id,masterbag_type,session);
		System.out.println("MasterBag : "+MasterbagList);
		   return MasterbagList;//new ModelAndView("getAreasNames", "areasName", areaNames);
	
		
	}
	@RequestMapping(value="/AddToMasterBag/{masterbag_no},{order_id},{masterbag_type}",method=RequestMethod.POST)
	public boolean AddToMasterBag(@PathVariable("masterbag_no")double masterbag_no,@PathVariable("order_id")String order_id,@PathVariable("masterbag_type")String masterbag_type,HttpSession session){
		System.out.println("hii*(*((((*(*inside controller");
		
		System.out.println("**********obj :"+masterbag_type+"  "+order_id+" "+masterbag_no);
		boolean flag=masterbagDao.AddToMasterBag(masterbag_no,order_id, masterbag_type, session);

		return flag;//new ModelAndView("landingpage.html");
		
	}
	
	@RequestMapping(value="/ActivateMasterBag/{masterbag_no}",method=RequestMethod.POST)
	public boolean ActivateMasterBag(@PathVariable("masterbag_no")double masterbag_no,HttpSession session){
		System.out.println("hii*(*((((*(*inside controller");
		
		System.out.println("**********obj :"+masterbag_no);
		boolean flag=masterbagDao.ActivateMasterBag(masterbag_no,session);
		return flag;
	}
	
@RequestMapping(value="/GetMasterBagData/{masterbag_no},{masterbag_type}" , method=RequestMethod.GET)
	
	public List<com.dataisys.taykit.model.MasterBag> GetMasterBagData(@PathVariable("masterbag_no")double masterbag_no,@PathVariable("masterbag_type")String masterbag_type,HttpSession session) throws IOException{
		
		List<com.dataisys.taykit.model.MasterBag> masterBag=masterbagDao.GetMasterBagData(masterbag_no, masterbag_type, session);
		return masterBag;
	}
	
	
@RequestMapping(value="/getLoggedStoreOrHub" , method=RequestMethod.GET)
public Hub getLoggedStoreOrHub(HttpSession session) throws IOException
{
	System.out.println("inside controller");
	Hub masterBag=masterbagDao.getUserMasterBag(session);//GetMasterBagData(masterbag_no, masterbag_type, session);
	
	System.out.println(masterBag);
	return masterBag;

}


@RequestMapping(value="/getMasterBagListForCurrentUser" , method=RequestMethod.GET)

public List<com.dataisys.taykit.model.MasterBag> getMasterBagListForCurrentUser(HttpSession session) throws IOException{
	
	List<com.dataisys.taykit.model.MasterBag> masterBag=masterbagDao.getMasterBagListForCurrentUser(session);
	return masterBag;
}

@RequestMapping(value="/GetMasterBagDetailsByNo/{masterbag_no}" , method=RequestMethod.GET)

public MasterBag GetMasterBagDetailsByNo(@PathVariable("masterbag_no")double masterbag_no,HttpSession session) throws IOException{
	
	MasterBag masterBag=masterbagDao.GetMasterBagDetailsByNo(masterbag_no,session);
	return masterBag;
}
@RequestMapping(value="/GetMasterBagDestinationByNo/{masterbag_no}" , method=RequestMethod.GET)
public MasterBag GetMasterBagDestinationByNo(@PathVariable("masterbag_no")double masterbag_no,HttpSession session) throws IOException{
	
	System.out.println("inside controller");
	MasterBag masterBag=masterbagDao.GetMasterBagDestinationByNo(masterbag_no,session);
	return masterBag;
}

@RequestMapping(value="/UpdateOrderByInwardMasterbag/{masterbag_no},{tracking_id},{location},{fromlocation}" , method=RequestMethod.GET)

public Order UpdateOrderByInwardMasterbag(@PathVariable("masterbag_no")double masterbag_no,@PathVariable("tracking_id")String tracking_id,@PathVariable("location")String location,@PathVariable("fromlocation")String fromlocation,HttpSession session) throws IOException{
	
	System.out.println("masterbag_no : "+masterbag_no+" tracking id : "+tracking_id+" Location : "+location+" fromLocation : "+fromlocation);
	
	Order order=masterbagDao.UpdateOrderByInwardMasterbag(masterbag_no,tracking_id,location,fromlocation,session);
	return order;
}

}
