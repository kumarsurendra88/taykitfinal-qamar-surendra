package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class UserRole {

private double userId;

private double roleId;

private Timestamp updatedOn;

public UserRole(){}

public UserRole(double userId, double roleId, Timestamp updatedOn) {
	super();
	this.userId = userId;
	this.roleId = roleId;
	this.updatedOn = updatedOn;
}

public double getUserId() {
	return userId;
}

public void setUserId(double userId) {
	this.userId = userId;
}

public double getRoleId() {
	return roleId;
}

public void setRoleId(double roleId) {
	this.roleId = roleId;
}

public Timestamp getUpdatedOn() {
	return updatedOn;
}

public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}





}
