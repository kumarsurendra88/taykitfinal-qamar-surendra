package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class User {/*

	
	private double userId;
	private String userType;
	private String userName;
	private String userRole;
	private String userLogin;
	private String userPassword;
	private String accNumber;
	private String userMobil;
	private String userEmail;
	private String userStatus;
	private String userChangePassword;
	private Timestamp userLastLogin;
	private String createdBy;
	private String updatedBy;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private String regKey;	
	private String userAccount;
	private String moduleName;
	
	public String getUserAccount() {
		return userAccount;
	}



	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}



	public String getUserAccount() {
		return userAccount;
	}



	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}



	public String getModuleName() {
		return moduleName;
	}



	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}



	public String getRegKey() {
		return regKey;
	}



	public void setRegKey(String regKey) {
		this.regKey = regKey;
	}



	public User(){}
	
	
	
	public User(String accNumber,double userId, String userType, String userRole,
			String userLogin, String userPassword, String userMobil,
			String userEmail, String userStatus, String userChangePassword,
			Timestamp userLastLogin, String createdBy, String updatedBy,
			Timestamp createdOn, Timestamp updatedOn) {
		super();
		this.accNumber=accNumber;
		this.userId = userId;
		this.userType = userType;
		this.userRole = userRole;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.userMobil = userMobil;
		this.userEmail = userEmail;
		this.userStatus = userStatus;
		this.userChangePassword = userChangePassword;
		this.userLastLogin = userLastLogin;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}
	
	
	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public double getUserId() {
		return userId;
	}
	public void setUserId(double userId) {
		this.userId = userId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		System.out.println("user type in bean : "+userType);
		this.userType = userType;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	
	


	public String getAccNumber() {
		return accNumber;
	}



	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}



	public String getUserMobil() {
		return userMobil;
	}
	public void setUserMobil(String userMobil) {
		this.userMobil = userMobil;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getUserChangePassword() {
		return userChangePassword;
	}
	public void setUserChangePassword(String userChangePassword) {
		this.userChangePassword = userChangePassword;
	}
	public Timestamp getUserLastLogin() {
		return userLastLogin;
	}
	public void setUserLastLogin(Timestamp userLastLogin) {
		this.userLastLogin = userLastLogin;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}	
	
*/


	
	private double userId;
	private String userType;
	private String userName;
	private String userRole;
	private String userLogin;
	private String userPassword;
	private String accNumber;
	private String userMobil;
	private String userEmail;
	private String userStatus;
	private String userChangePassword;
	private Timestamp userLastLogin;
	private String createdBy;
	private String updatedBy;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private String regKey;	
	private String userAccount;
	//zone for zonal manager
	private String zone_name;
	private String region_name;
	private String hub_name;
	private int zone_id;
	private int region_id;
	private double hub_id;
	private String store_name;
	private String moduleName;
	/*public String getUserAccount() {
		return userAccount;
	}



	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}*/



	public String getModuleName() {
		return moduleName;
	}



	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}



	public String getStore_name() {
		return store_name;
	}



	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}



	public int getZone_id() {
		return zone_id;
	}



	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}



	public int getRegion_id() {
		return region_id;
	}



	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}



	public double getHub_id() {
		return hub_id;
	}



	public void setHub_id(double hub_id) {
		this.hub_id = hub_id;
	}



	public String getHub_name() {
		return hub_name;
	}



	public void setHub_name(String hub_name) {
		this.hub_name = hub_name;
	}



	public String getRegion_name() {
		return region_name;
	}



	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}



	public String getZone_name() {
		return zone_name;
	}



	public void setZone_name(String zone_name) {
		System.out.println("zone name in bean : "+zone_name);
		this.zone_name = zone_name;
	}



	public String getRegKey() {
		return regKey;
	}



	public void setRegKey(String regKey) {
		this.regKey = regKey;
	}



	public User(){}
	
	
	
	public User(String accNumber,double userId,String userName, String userType, String userRole,
			String userLogin, String userPassword, String userMobil,
			String userEmail, String userStatus, String userChangePassword,
			Timestamp userLastLogin, String createdBy, String updatedBy,
			Timestamp createdOn, Timestamp updatedOn,String zone_name,String region_name,int zone_id,int region_id,double hub_id,String store_name) {
		super();
		this.accNumber=accNumber;
		this.userId = userId;
		this.userName=userName;
		this.userType = userType;
		this.userRole = userRole;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.userMobil = userMobil;
		this.userEmail = userEmail;
		this.userStatus = userStatus;
		this.userChangePassword = userChangePassword;
		this.userLastLogin = userLastLogin;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.zone_name=zone_name;
		this.region_name=region_name;
		this.zone_id=zone_id;
		this.region_id=region_id;
		this.hub_id=hub_id;
		this.store_name=store_name;
	}
	
	
	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public double getUserId() {
		return userId;
	}
	public void setUserId(double userId) {
		this.userId = userId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		System.out.println("user type in bean : "+userType);
		this.userType = userType;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	
	


	public String getAccNumber() {
		return accNumber;
	}



	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}



	public String getUserMobil() {
		return userMobil;
	}
	public void setUserMobil(String userMobil) {
		this.userMobil = userMobil;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getUserChangePassword() {
		return userChangePassword;
	}
	public void setUserChangePassword(String userChangePassword) {
		this.userChangePassword = userChangePassword;
	}
	public Timestamp getUserLastLogin() {
		return userLastLogin;
	}
	public void setUserLastLogin(Timestamp userLastLogin) {
		this.userLastLogin = userLastLogin;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}	
	




}
