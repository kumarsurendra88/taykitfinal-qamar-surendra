package com.dataisys.taykit.model;

public class Module {


		private double moduelId;
		private String moduleName;
		private String moduleStatus;
		private String updatedBy;
		private String updatedOn;
		private String moduleCode;
		
		
		public Module(){}


		public Module(double moduelId, String moduleName, String moduleStatus,
				String updatedBy, String updatedOn) {
			super();
			this.moduelId = moduelId;
			this.moduleName = moduleName;
			this.moduleStatus = moduleStatus;
			this.updatedBy = updatedBy;
			this.updatedOn = updatedOn;
		}

		
		

		

		@Override
		public String toString() {
			return "Module [moduelId=" + moduelId + ", moduleName="
					+ moduleName + ", moduleStatus=" + moduleStatus
					+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
					+ ", moduleCode=" + moduleCode + "]";
		}


		public String getModuleCode() {
			return moduleCode;
		}


		public void setModuleCode(String moduleCode) {
			this.moduleCode = moduleCode;
		}


		public double getModuelId() {
			return moduelId;
		}


		public void setModuelId(double moduelId) {
			this.moduelId = moduelId;
		}


		public String getModuleName() {
			return moduleName;
		}


		public void setModuleName(String moduleName) {
			this.moduleName = moduleName;
		}


		public String getModuleStatus() {
			return moduleStatus;
		}


		public void setModuleStatus(String moduleStatus) {
			this.moduleStatus = moduleStatus;
		}


		public String getUpdatedBy() {
			return updatedBy;
		}


		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}


		public String getUpdatedOn() {
			return updatedOn;
		}


		public void setUpdatedOn(String updatedOn) {
			this.updatedOn = updatedOn;
		}
		
		
		
}
