package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Order;

public class RandomTestRowMapper implements RowMapper<Order> {

	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Order order= new Order();
		order.setRunSheetNumber(rs.getString("runsheet_number"));
		order.setTrackingId(rs.getString("tracking_id"));
		//order.setRunSheetNumber(rs.getString("store_runsheet"));
		
		return order;
	}

}
