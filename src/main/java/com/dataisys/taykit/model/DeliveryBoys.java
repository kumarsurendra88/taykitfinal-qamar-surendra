package com.dataisys.taykit.model;
//@author irfan okkk
public class DeliveryBoys {
private int deliveryBoyId;
private String deliveryBoyName;
private String deliveryBoyAddress;
private int storeId;
private String swipeDevice;

public int getStoreId() {
	return storeId;
}
public void setStoreId(int storeId) {
	this.storeId = storeId;
}
public String getSwipeDevice() {
	return swipeDevice;
}
public void setSwipeDevice(String swipeDevice) {
	this.swipeDevice = swipeDevice;
}
public int getDeliveryBoyId() {
	return deliveryBoyId;
}
public void setDeliveryBoyId(int deliveryBoyId) {
	this.deliveryBoyId = deliveryBoyId;
}
public String getDeliveryBoyName() {
	return deliveryBoyName;
}
public void setDeliveryBoyName(String deliveryBoyName) {
	this.deliveryBoyName = deliveryBoyName;
}
public String getDeliveryBoyAddress() {
	return deliveryBoyAddress;
}
public void setDeliveryBoyAddress(String deliveryBoyAddress) {
	this.deliveryBoyAddress = deliveryBoyAddress;
}
@Override
public String toString() {
	return "DeliveryBoys [deliveryBoyId=" + deliveryBoyId
			+ ", deliveryBoyName=" + deliveryBoyName + ", deliveryBoyAddress="
			+ deliveryBoyAddress + "]";
}
public DeliveryBoys(int deliveryBoyId, String deliveryBoyName,
		String deliveryBoyAddress) {
	super();
	this.deliveryBoyId = deliveryBoyId;
	this.deliveryBoyName = deliveryBoyName;
	this.deliveryBoyAddress = deliveryBoyAddress;
}
public DeliveryBoys() {
	super();
}
}
