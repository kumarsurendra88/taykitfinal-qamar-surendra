package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.RegionDAO;
import com.dataisys.taykit.dao.ZoneDAO;
import com.dataisys.taykit.model.Areas;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.AreasRowMapper;
import com.dataisys.taykit.rowmapper.RegionRowMapper;
import com.dataisys.taykit.rowmapper.StoreRowMapper;

public class RegionDAOImpl extends JdbcDaoSupport implements RegionDAO

{

	
	public boolean CreatRegion(int zone_id,String region_name,String region_desc) {
		boolean flag=false;
		System.out.println("zone_id : "+zone_id);
		System.out.println("region_name : "+region_name);
		System.out.println("region_desc : "+region_desc);
		Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		System.out.println("running scheduler" );
		
		Calendar cal = Calendar.getInstance();
		/*cal.setTime ( today ); // convert your date to Calendar object
		int daysToDecrement = -days;
		cal.add(Calendar.DATE, daysToDecrement);
		today = cal.getTime();*/
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				
		System.out.println("after converting"+dateFormat.format(today));
		
		String sql="insert into regions(zone_id,region_name,region_desc) values(?,?,?)";
	//	getJdbcTemplate().update(sql,dateFormat.format(today));
		int i=getJdbcTemplate().update(sql,zone_id,region_name,region_desc);
		System.out.println("value of i : "+i);
		if(i>0)
		{
			flag=true;
			System.out.println("region Inserted");
		}
		
		return flag;
	}

	public List<Region> getNames(int zone_id) {
		String sql="select * from regions where zone_id=? ";
		List<Region> ListOfRegionNames=getJdbcTemplate().query(sql,new Object[] {zone_id}, new RegionRowMapper());
				
			//	getJdbcTemplate().queryForObject(sql, new Object[]{zone_id},new RegionRowMapper());
		System.out.println("******************: "+ListOfRegionNames);
		
		return ListOfRegionNames;
		
	}

	public List<Region> getRegionByZoneId(int zone_id) {
		System.out.println("***********zone_id : "+zone_id);
		String sql="select * from regions where zone_id=? ";
		List<Region> ListOfRegionNames=getJdbcTemplate().query(sql,new Object[] {zone_id}, new RegionRowMapper());
				
			//	getJdbcTemplate().queryForObject(sql, new Object[]{zone_id},new RegionRowMapper());
		System.out.println("******************: "+ListOfRegionNames);
		
		return ListOfRegionNames;
	}

	public List<Region> getRegionByZoneName(String zone_name) {
		
		
		
		
		System.out.println("***********zone_id : "+zone_name);
		String sql="select * from regions where zone_id=(select zone_id from zones where zone_name=?) ";
		List<Region> ListOfRegionNames=getJdbcTemplate().query(sql,new Object[] {zone_name}, new RegionRowMapper());
				
			
		System.out.println("******************: "+ListOfRegionNames);
		
		return ListOfRegionNames;
	}

	public List<Region> getRegions() {



		String sql="SELECT rg.region_id,rg.region_name,rg.region_desc,zn.zone_name FROM `regions` rg INNER JOIN zones zn ON zn.zone_id=rg.zone_id";

			System.out.println(sql);
return getJdbcTemplate().query(sql,new ResultSetExtractor<List<Region>>(){

public List<Region> extractData(ResultSet rs) throws SQLException,
		DataAccessException {
System.out.println("inside");
	List<Region> list=new ArrayList<Region>(); 
	System.out.println("inside");
    while(rs.next()){  
    	Region e=new Region();  
   //  e.setActivityDate(rs.getDate("activityDate"));
    e.setRegion_id(rs.getInt("region_id"));
    e.setRegion_name(rs.getString("region_name"));
    e.setRegion_desc(rs.getString("region_desc"));
    e.setZone_name(rs.getString("zone_name"));
    list.add(e);
    System.out.println("inside while");
    }  
    return list;
}

});
	
	}

	public List<Region> getRegionList() {

		String sql="select * from regions";
		List<Region> ListOfRegionNames=getJdbcTemplate().query(sql, new RegionRowMapper());
				
			
		System.out.println("******************: "+ListOfRegionNames);
		
		return ListOfRegionNames;
	}

	public Region getRegionsById(double id) {
		String sql="SELECT a.zone_name,b.region_id,b.region_name,b.region_desc FROM zones a, regions b WHERE a.zone_id = b.zone_id and b.region_id="+id;
		
		 System.out.println(sql);
		 return getJdbcTemplate().query(sql,new ResultSetExtractor<Region>(){

		 public Region extractData(ResultSet rs) throws SQLException,
		 		DataAccessException {
		 System.out.println("inside");
		 	
		 	Region e=new Region();  
		     while(rs.next()){  
		 
		     e.setRegion_id(rs.getInt("region_id"));
		    e.setRegion_name(rs.getString("region_name"));
		    e.setRegion_desc(rs.getString("region_desc"));
		    e.setZone_name(rs.getString("zone_name"));
		     System.out.println("inside while");
		     }  
		     return e;
		 }

		 });
		
	}

	public void updateRegion(Region region, int id) {
		String SQL = "update regions set region_name=?,zone_id=(select zone_id from zones where zone_name=?),region_desc =? where region_id=?";
		System.out.println("data in impl : "+region.getRegion_name()+" : "+region.getZone_name()+" : "+region.getRegion_desc()+" : "+id); 
		getJdbcTemplate().update( SQL,region.getRegion_name(),region.getZone_name(),region.getRegion_desc(),id);
		System.out.println("Region Updated");
	}

}
