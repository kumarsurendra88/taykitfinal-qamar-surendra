package com.dataisys.taykit.controllers;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.GCM.Content;
import com.dataisys.taykit.GCM.POST2GCM;
import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.ReturnDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.CancelledOrderByData;
import com.dataisys.taykit.model.ExcelUtil;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.PudoStore;
import com.dataisys.taykit.model.RegistrationKey;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.SearchReturn;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpSession;




@RestController
public class ReturnController {
	
	
	@Autowired
	ReturnDao returnDao;
	@Autowired
	OrderDao orderDao;
	@Autowired
	UserDao userDao;
	@Autowired
	RegistrationKeyDao regKeyDao;
	
	@Autowired
	PudoStoreDao storeDao;
	


	@RequestMapping(value="/makeReturnEntry",method=RequestMethod.POST)	
public void makeEntry(Model model,@ModelAttribute Return returnvalues ){
	
		System.out.println("inside"+returnvalues.getTrackingId());
			
		returnDao.makeReturnEntry(returnvalues);
			
			
	
	}
	
	@RequestMapping(value="/getReturn",method=RequestMethod.GET)
	public List<Return> getReturns(HttpSession session){
		
	List<Return> returnproduct=	returnDao.returnvalues(session);
	
	return returnproduct;
		
	}
	
	
	@RequestMapping(value="/uploadReturn", method =RequestMethod.POST )
	public @ResponseBody ModelAndView uploadFile( UploadForm uploadItem, BindingResult result) throws IOException
	
	
	{
		
		 
		
		System.out.println("checking for upload item" );
		if(uploadItem == null) {
			System.out.println("file is null");
		} else {
			System.out.println("file is not null");
		}
		
		
		
		
		
		System.out.println("Inside Order controller");
		System.out.println(uploadItem.getFile().getOriginalFilename());
		
		System.out.println(	"upload file"+uploadItem.getFile());
		InputStream is = uploadItem.getFile().getInputStream();
		
		HSSFWorkbook workbook = new HSSFWorkbook(is);
		HSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		
		
		boolean isHeader = true;
		try {
				
			java.sql.Date sqlDate=null;
			java.sql.Date sqlDate1=null;
			java.sql.Date null1=null;
			java.sql.Date null2=null;
			while(rowIterator.hasNext()){			
				Row row = rowIterator.next();
				if(!isHeader) 
				{
					System.out.println("last cell value"+row.getLastCellNum());
					if(row.getLastCellNum()>13)
					
					{
						
						System.out.println("inside myntra upload");
						System.out.println("inside if is header :-)");
						Cell trackingId=row.getCell(0);
						System.out.println("trackingId" +trackingId);
						
						Cell courierService=row.getCell(1);
						System.out.println("courierService" +courierService);
						
						Cell returnId = row.getCell(2);
						
						System.out.println("returnId" +returnId);
						
						Cell orderId = row.getCell(3);
						System.out.println("orderId" +orderId);
						Cell consigneeName = row.getCell(4);
						System.out.println("consigneeName" +consigneeName);
						Cell city = row.getCell(5);
						System.out.println("city" +city);
						Cell state = row.getCell(6);
						System.out.println("state" +state);
						
						Cell country = row.getCell(7);
						System.out.println("country" +country);
						Cell address = row.getCell(8);
						System.out.println("address" +address);
				
									
						Cell pinCode = row.getCell(9);
						System.out.println("pinCode" +pinCode);
					
						Cell phone=row.getCell(10);
						System.out.println("phone" +phone);
						Cell mobile=row.getCell(11);
					
						Cell paymentMode = row.getCell(12);
						Cell packageAmount = row.getCell(13);
						Cell productToBeShipped=row.getCell(14);
					
					
					try {
						
						
						
						Return returnOrder = new Return();
						
						if(trackingId != null){
							returnOrder.setTrackingId(ExcelUtil.getStringCellData(trackingId));
						}
							/*long d=ExcelUtil.getNumbericCellData(customerContactno);
							System.out.println("value in number format is"+d);*/
						
						if(courierService !=null){
							
							returnOrder.setCourierService(ExcelUtil.getStringCellData(courierService));
						}
						if(returnId!=null){
							
							returnOrder.setReturnId(ExcelUtil.getStringCellData(returnId));;
							BigDecimal bd = new BigDecimal(returnOrder.getReturnId());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							if(newContactNo.contains("+"))
							{
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								returnOrder.setReturnId(newContactForFail);
							}
							
							else{
								returnOrder.setReturnId(newContactNo);
							}
						}
						
						if(orderId!=null){
							
							returnOrder.setOrderId(ExcelUtil.getStringCellData(orderId));
							
							BigDecimal bd = new BigDecimal(returnOrder.getOrderId());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							if(newContactNo.contains("+"))
							{
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								returnOrder.setOrderId(newContactForFail);
							
							
						}
							else{
								returnOrder.setOrderId(newContactNo);
							}
						}
						if(consigneeName!=null){
							
							returnOrder.setConsigneeName(ExcelUtil.getStringCellData(consigneeName));
							System.out.println("consigne name"+returnOrder.getConsigneeName());
						}
						
						if(city!=null){
							
							returnOrder.setCity(ExcelUtil.getStringCellData(city));
						}
						
						if(state !=null){
							
							returnOrder.setState(ExcelUtil.getStringCellData(state));
						}
						if(country!=null){
							returnOrder.setCountry(ExcelUtil.getStringCellData(country));
						}
						if(address!=null){
							returnOrder.setExcelAddress(ExcelUtil.getStringCellData(address));
						}
						
						if(pinCode!=null){
							returnOrder.setPincode(ExcelUtil.getNumbericCellData(pinCode));
						}
						if(phone!=null){
							
							returnOrder.setPhone(ExcelUtil.getStringCellData(phone));
							
							BigDecimal bd = new BigDecimal(returnOrder.getPhone());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							
							if(newContactNo.contains("+"))
							{
								//failCountNumber++;
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								
								returnOrder.setPhone(newContactForFail);
								
								
							}
							else{
							//System.out.println("Contact number after removing 0 is"+newContactNo);
							returnOrder.setPhone(newContactNo);;
							}
							
						}
						
						if(mobile!=null){
							
							returnOrder.setContactNo(ExcelUtil.getStringCellData(mobile));
							
							BigDecimal bd = new BigDecimal(returnOrder.getContactNo());
							String s="0"+bd;
							String newContactNo=s.substring(1);
							
							if(newContactNo.contains("+"))
							{
								//failCountNumber++;
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed contact number"+d);
								System.out.println("Big decimal type of failed contact number"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								
								returnOrder.setContactNo(newContactForFail);
								
								
							}
							else{
							//System.out.println("Contact number after removing 0 is"+newContactNo);
							returnOrder.setContactNo(newContactNo);;
							}
						}
							
						if(paymentMode!=null){
									
							returnOrder.setPaymentMode(ExcelUtil.getStringCellData(paymentMode));
						}
						
						if(packageAmount!=null){
							
							returnOrder.setOrderValue(ExcelUtil.getNumbericCellData(packageAmount));
						}
						if(productToBeShipped!=null) {
							
							returnOrder.setProductToBeShipped(ExcelUtil.getStringCellData(productToBeShipped));
						}					
						
						//orderDao.createDummyOrder(order);
						returnDao.makeReturnEntry1(returnOrder);
						System.out.println("aftermaking call");
						}
						catch(Exception e){
							
							System.out.println("Error while creating myntra data");
							e.printStackTrace();
						}
					}
					else
					{
						System.out.println("inside myntra zivameUpload");
					Cell returnId=row.getCell(0);
					System.out.println("returnId" +returnId);
					
					Cell customerName=row.getCell(1);
					
					Cell customerCity = row.getCell(2);
					
					System.out.println("city is" +customerCity);
					
					Cell state = row.getCell(3);
					
					Cell address = row.getCell(4);
					Cell pinCode = row.getCell(5);
					Cell customerMobiel = row.getCell(6);
					
					Cell orderType = row.getCell(7);
					
					Cell orderValue = row.getCell(8);
				
					
					
					
					
					Cell orderDetail = row.getCell(9);
					
					Cell quantity=row.getCell(10);
					Cell weight=row.getCell(11);
					Cell retailerId= row.getCell(12);
					
				
				try {
					
					Return returnOrder = new Return();
					
					//generate TrackingId and OrderId For zivame Data
					
					String trackinId=generateTrackingNumber();
					returnOrder.setTrackingId(trackinId);
					
						
					
					if(returnId!=null){
						
						returnOrder.setReturnId(ExcelUtil.getStringCellData(returnId));;
						BigDecimal bd = new BigDecimal(returnOrder.getReturnId());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						if(newContactNo.contains("+"))
						{
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							returnOrder.setReturnId(newContactForFail);
						}
						
						else{
							returnOrder.setReturnId(newContactNo);
						}
					}
					
					
					if(customerName!=null){
						
						returnOrder.setConsigneeName(ExcelUtil.getStringCellData(customerName));
						System.out.println("consigne name"+returnOrder.getConsigneeName());
					}
					
					if(customerCity!=null){
						
						returnOrder.setCity(ExcelUtil.getStringCellData(customerCity));
					}
					
					if(state !=null){
						
						returnOrder.setState(ExcelUtil.getStringCellData(state));
					}
				
					if(address!=null){
						//returnOrder.setExcelAddress(ExcelUtil.getStringCellData(address));
						returnOrder.setCustomerAddress(ExcelUtil.getStringCellData(address));
						
					}
					
					if(pinCode!=null){
						returnOrder.setPincode(ExcelUtil.getNumbericCellData(pinCode));
					}
					if(customerMobiel!=null){
						
						returnOrder.setPhone(ExcelUtil.getStringCellData(customerMobiel));
						
						BigDecimal bd = new BigDecimal(returnOrder.getPhone());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						
						if(newContactNo.contains("+"))
						{
							//failCountNumber++;
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							
							returnOrder.setPhone(newContactForFail);
							
							
						}
						else{
						//System.out.println("Contact number after removing 0 is"+newContactNo);
						returnOrder.setPhone(newContactNo);;
						}
						
					}
					
					if(customerMobiel!=null){
						
						returnOrder.setContactNo(ExcelUtil.getStringCellData(customerMobiel));
						
						BigDecimal bd = new BigDecimal(returnOrder.getContactNo());
						String s="0"+bd;
						String newContactNo=s.substring(1);
						
						if(newContactNo.contains("+"))
						{
							//failCountNumber++;
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							
							returnOrder.setContactNo(newContactForFail);
							
							
						}
						else{
						//System.out.println("Contact number after removing 0 is"+newContactNo);
						returnOrder.setContactNo(newContactNo);;
						}
					}
						
					if(orderType!=null){
								
						returnOrder.setPaymentMode(ExcelUtil.getStringCellData(orderType));
					}
					
					if(orderValue!=null){
						
						returnOrder.setOrderValue(ExcelUtil.getNumbericCellData(orderValue));
					}
					if(orderDetail!=null){
						
						returnOrder.setProductToBeShipped(ExcelUtil.getStringCellData(orderDetail));
					}
					
					if(weight!=null){
						
						returnOrder.setWeight(ExcelUtil.getNumbericCellData(weight));
					}
						
					if(retailerId!=null){
						
						returnOrder.setRetailerId(ExcelUtil.getNumbericCellData(retailerId));
					}
						
						
					
					
					//orderDao.createDummyOrder(order);
					returnDao.makeReturnEntry1(returnOrder);
					System.out.println("aftermaking call");
					
					
					
				}
		
				catch(Exception ex) {
						ex.printStackTrace();
					}
		
					}
			}
				isHeader = false;
				
				}
		}
		catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
	}
		is.close();
		return new ModelAndView("customer.html");
		
	}
	

		
	public String generateTrackingNumber(){
		
		int trackingId=(int) orderDao.getTempNumber();
		String sequence="RET";
		System.out.println("traclin id from table" +trackingId);
	
		   String name = String.format("%08d", trackingId);
		   System.out.println("asdfasdf"+name);
		   String NewString=sequence+name;
		   
		    System.out.println("tracking number generated for returns is" +NewString);
		    	trackingId++;
		    	orderDao.storeTempNumber(trackingId);
		    	return NewString;
	    }
	
	
	@RequestMapping(value="/getReturnById/{tracking}", method=RequestMethod.GET)
	public @ResponseBody Return getReturnListById(@PathVariable("tracking") String trackingId) throws IOException{
		
		
		
		Return order= returnDao.retrurnOrder(trackingId);
		
		return order;
	}
	
	@RequestMapping(value="/assignReturnToDeliverBoy", method=RequestMethod.POST)
	public void assignToDeliveryBoy(@RequestBody AssignDeliveryBoy obj) {
		System.out.println("Got Object"+obj);
		returnDao.assignToDeliveryBoy(obj);
		
		//Code to make a delivery boy get an alert by Kishore
		
		try {
			System.out.println("Delivery Boy Name: "+obj.getAssignTo());
			User user = userDao.getDeliveryBoyByName(obj.getAssignTo());
			RegistrationKey regKey = regKeyDao.getRegkeyByAccountid(user.getAccNumber());
			System.out.println("Obtained RegKey: "+regKey.getRegKey()+" For DeliveryBoy is: "+user.getUserName());
			
			String apiKey = "AIzaSyAHuIzTDBpD7Cr8oST3TtL2hQn8xJoggjo";
			Content content = new Content();
			content.addRegId(regKey.getRegKey());
			content.createData("You've got an Order To Deliver", obj.getTrackingId());
			POST2GCM.post(apiKey, content);
			
		} catch (Exception e) {
			System.out.println("Inside catch of assignDeliveryBoy from webapp in OrderController");
			e.printStackTrace();
		}
		
		
	}
	
	
	//*****************Generating runsheet with username starts************************//
		@RequestMapping(value="/generateReturnRunSheetNumberWithUserName" , method=RequestMethod.POST)
			
			public void generateReturnRunSheetWithUserName(@RequestBody Order order,HttpSession session){
				
	OrderController oc=new OrderController();
			
			String RandomRunsheetNumber=oc.generateRunSheetNumber(9);
				
			//	String RandomRunsheetNumber=generateRunSheetNumber(9);
			//	String RandomRunsheetNumber=generateUniqueRunsheetNumber();
				String sheet=order.getAssignedTo();
				//adding only runsheet and delivery boy name and date  to the runsheet table for returns
				 orderDao.runSheetPartialDetails(RandomRunsheetNumber,order.getAssignedTo());
				 System.out.println("Done");
		}
			
		//*****************Generating runsheet with username ends************************//
	
		//*****************generating run sheet in reverse order starts*********************************//
		
		@RequestMapping(value="/generateReturnRunSheetNumber/{runSheetNumber},{DeliveryBoyName}" , method=RequestMethod.POST)
			
			public void generateRunSheet(@PathVariable ("runSheetNumber") String runSheetNumber,@PathVariable ("DeliveryBoyName") String DeliveryBoyName, HttpSession session){
				
				OrderController oc=new OrderController();
				System.out.println("inside generate runsheet api controller");
				//String RandomRunsheetNumber=oc.generateRunSheetNumber(9);
				System.out.println("runSheetNumber : DeliveryBoyName: "+runSheetNumber+" : "+DeliveryBoyName);
				String RandomRunsheetNumber=runSheetNumber;
				String sheet=DeliveryBoyName;//order.getAssignedTo();
				java.util.Date date= new java.util.Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String newDate=dateFormat.format(date);
				String fff=newDate.replace(":", "-");
							String ggg=fff.replace("/", "-");
				sheet=sheet+""+ggg;
				
				System.out.println("file name is"+sheet);
				
				//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
				String path1="runsheets";
				ClassLoader classLoader = getClass().getClassLoader();
				File file = new File(classLoader.getResource(path1).getFile());
				System.out.println("file loader is "+file);
				path1=file+"/"+sheet+".csv";
				//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheets"+"/"+sheet+".csv";
				
				
				List<Return> ord=returnDao.getRunSheetDeliveries2(DeliveryBoyName,runSheetNumber);
				System.out.println("size of return orders is" +ord.size());
				//Return runsheeetVaulu=ord.get(0);
				
				System.out.println("runsheer");
				
			/*	String runSheetNumber=runsheeetVaulu.getRunSheetNumber();
				System.out.println("runsheeet value in order controller is" +runSheetNumber);*/
				String accountNo=(String) session.getAttribute("loggedStore");
				Store obj=storeDao.getStoreByAccountNo(accountNo);
				
				
				
				
				 if(RandomRunsheetNumber!=null){
					 
						// System.out.println("Ofder dao"+dao);
						 
						/* System.out.println("Ofder dao"+orderDao);
						 orderDao.getAllOrders();*/
						 System.out.println("befor calling updateRunsheet");
						 
						 returnDao.getRunsheetNumber(runSheetNumber,DeliveryBoyName);
						
						 System.out.println("After calling updateRunsheet");
						 
						 
					 }
				
			
				
				try{
				FileWriter writer = new FileWriter(path1);
				//CSVWriter writer = new CSVWriter(new FileWriter(path1));
				
				System.out.println("inside file writer");
				//writer.writeNext("STORE ID");
				 
			    writer.append("STORE ID");
			    writer.append(',');
			    writer.write(obj.getName());
			    writer.append('\n');

			    writer.append("ASSIGNEE NAME");
			    writer.append(',');
			    writer.write(DeliveryBoyName);
		            writer.append('\n');
					
			    writer.append("ASSIGNEE CONTACT NO");
			    writer.append(',');
			    writer.append("");
			    writer.append('\n');
			    
			    writer.append("RUNSHEET NUMBER");
			    writer.append(',');
			    writer.write(RandomRunsheetNumber);
			    writer.append('\n');
			    
			    writer.append('\n');
			    
			    writer.append("COD");
			    writer.append(',');
			    writer.append("CUSTOMER NAME");
			    writer.append(',');
			    writer.append("CUSTOMER ADDRESS");
			    writer.append(',');
			    writer.append("CONTACT NO");
			    writer.append(',');
			    writer.append("TRACKING ID");
			    writer.append(',');
			    writer.append("SHIPMENT TYPE");
			    writer.append(',');
			    writer.append("RETAILER ID");
			    writer.append(',');
			  //Added By Kishore
			    writer.append("RETURN ID");
			    writer.append(',');
			    writer.append("ORDER DETAILS");
			    writer.append(',');
			    writer.append("QUANTITY");
			    writer.append(',');
			    //End of it
			   writer.append('\n');
			   String retailerName=null;
			   String newAdd=null;
			   for(Return o:ord){
				   
				   if(o.getRetailerId()==3){
					   retailerName="Myntra";
				   }
				   else{
					   retailerName="retailer1";
				   }
				   
				   writer.append(""+o.getOrderValue());
				   writer.append(',');
				   System.out.println("custmer name is" +o.getConsigneeName());
				   writer.write(o.getConsigneeName());
				   writer.append(',');
				   String customerAddress=o.getCustomerAddress().replace(",", ":");
				   String newcustomerAddress= customerAddress.replace("#", "No");
				   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
				  
				   /*if(newcustomerAddress!=null){
					  // System.out.println("Address is"+newAdd);
					   newAdd=   newcustomerAddress.trim();
					   System.out.println("Address After trimming"+newAdd);
				   }*/
				  System.out.println("Before"+newcustomerAddress);
				  newAdd=   newCustomerAddress1.trim();
				   System.out.println("Address After trimming"+newAdd);
				  
				   writer.write(newAdd);
				   writer.append(',');
				   writer.write(o.getContactNo());
				   writer.append(',');
				   writer.write(o.getTrackingId());
				   writer.append(',');
				   //writer.write(o.get);
				 //  writer.append(',');
				   writer.write(retailerName);
				   writer.append(',');
				   /* Added By Kishore */
				   writer.append(o.getReturnId());
				   writer.append(',');
				   writer.append(o.getProductToBeShipped());
				   writer.append(',');
				   writer.append(o.getQuantity()+"");
				   writer.append(',');
				   /* End of this*/
				   writer.append('\n');
			   }
			    
			    
			    
			    
			    
					
			    //generate whatever data you want
					
			    writer.flush();
			    writer.close();
			}
				
			
			catch(IOException e)
			{
			     e.printStackTrace();
			} 
				System.out.println("going inside RunsheetGenerator");
				 RunsheetGenerator rsg= new RunsheetGenerator();
		         rsg.main1(orderDao,sheet);
				// return new ModelAndView("fileupload.html");
		    }


			
			//*****************generating run sheet in reverse order ends*********************************//
	
@RequestMapping(value="/storeRunSheetNumberFroReturn" , method=RequestMethod.POST)
public void generateRunsheetForStores(@RequestBody PudoStore store,HttpSession session){
	
	if(store.getOrderType().equalsIgnoreCase("Drop Off") ){
		
		generateRunsheetForDropOff(store,session);
	}
	
	
	else{
	
	
	String RandomRunsheetNumber=generateRunSheetNumber(10);
	

	String storeName=store.getStoreName();
	String fileName=storeName;
	java.util.Date date= new java.util.Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	String newDate=dateFormat.format(date);
	String fff=newDate.replace(":", "-");
				String ggg=fff.replace("/", "-");
				fileName=fileName+""+ggg;
	
	
	//String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+fileName+".csv";
				String path1="storerunsheets";
				
				ClassLoader classLoader = getClass().getClassLoader();
				File file = new File(classLoader.getResource(path1).getFile());
				path1=file+"/"+fileName+".csv";
	System.out.println("complete file path is" +path1);
	System.out.println("store name is"+storeName);
	double storeId=storeDao.getStoreIdByName(storeName);
	//Order order=new Order();
	Return order = new Return();
	order.setStoreId(storeId);
	order.setOrderType(store.getOrderType());
	
	List<Return> ord=returnDao.ordersForStoreRunSheet(order);
	/*Order runsheeetVaulu=ord.get(0);
	
	System.out.println("runsheer");
	
	String runSheetNumber=runsheeetVaulu.getRunSheetNumber();
	System.out.println("runsheeet value in order controller is" +runSheetNumber);*/
	String accountNo=(String) session.getAttribute("loggedStore");
	Store obj=storeDao.getStoreByAccountNo(accountNo);
	
	
	
	
	 if(RandomRunsheetNumber!=null){
		 
			
			 System.out.println("befor calling updateRunsheet");
			 
			 returnDao.getRunSheetNumberForStore(storeId, RandomRunsheetNumber,order.getOrderType());
			 System.out.println("After calling updateRunsheet");
			 
			 
		 }
	
	
	
	
	
	
	
	try{
	FileWriter writer = new FileWriter(path1);

	 
    writer.append("STORE ID");
    writer.append(',');
    writer.write(obj.getName());
    //writer.write(obj.getName());
    writer.append('\n');

    writer.append("ASSIGNEE NAME");
    writer.append(',');
    writer.write(storeName);
        writer.append('\n');
		
    writer.append("ASSIGNEE CONTACT NO");
    writer.append(',');
    writer.append("");
    writer.append('\n');
    
    writer.append("RUNSHEET NUMBER");
    writer.append(',');
    writer.write(RandomRunsheetNumber);
    writer.append('\n');
    
    writer.append('\n');
    
    writer.append("COD");
    writer.append(',');
    writer.append("CUSTOMER NAME");
    writer.append(',');
    writer.append("CUSTOMER ADDRESS");
    writer.append(',');
    writer.append("CONTACT NO");
    writer.append(',');
    writer.append("TRACKING ID");
    writer.append(',');
    writer.append("SHIPMENT TYPE");
    writer.append(',');
    writer.append("RETAILER ID");
    writer.append(',');
   writer.append('\n');
   String retailerName=null;
   String newAdd=null;
   for(Return o:ord){
	   
	   
	   
	   
	   if(o.getRetailerId()==3){
		   retailerName="Myntra";
	   } else if(o.getRetailerId()==1){
		   
		   retailerName="Zivame";
	   }
	   else{
		   retailerName="Clovia";
	   }
	   
	   writer.append(""+o.getOrderValue());
	   writer.append(',');
	   writer.write(o.getConsigneeName());
	   writer.append(',');
	   String customerAddress=o.getCustomerAddress().replace(",", ":");
	   String newcustomerAddress= customerAddress.replace("#", "No");
	   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
	 
	  System.out.println("Before"+newcustomerAddress);
	  newAdd=   newCustomerAddress1.trim();
	   System.out.println("Address After trimming"+newAdd);
	  
	   writer.write(newAdd);
	   writer.append(',');
	   writer.write(o.getPhone());
	   writer.append(',');
	   writer.write(o.getTrackingId());
	   writer.append(',');
	   writer.write(o.getOrderType());
	   writer.append(',');
	   writer.write(retailerName);
	   writer.append(',');
	   writer.append('\n');
   }
    
    
    
    
    
		
    //generate whatever data you want
		
    writer.flush();
    writer.close();
}
	

catch(IOException e)
{
     e.printStackTrace();
} 
	 RunsheetGenerator rsg= new RunsheetGenerator();
     rsg.main3(orderDao,fileName);
	// return new ModelAndView("fileupload.html");
}
	

	
}	
	


public  void generateRunsheetForDropOff(PudoStore details, HttpSession sessi){
	
	
	System.out.println("inside run sheet generator for collect");
	
	String RandomRunsheetNumber=generateRunSheetNumber(10);
	
	String storeName=details.getStoreName();
	String fileName=storeName;
	java.util.Date date= new java.util.Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	String newDate=dateFormat.format(date);
	String fff=newDate.replace(":", "-");
				String ggg=fff.replace("/", "-");
				fileName=fileName+""+ggg;
	
	
				//String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+fileName+".csv";
				String path1="storerunsheets";
				ClassLoader classLoader = getClass().getClassLoader();
				File file = new File(classLoader.getResource(path1).getFile());
				path1=file+"/"+fileName+".csv";
	double storeId=storeDao.getStoreIdByName(storeName);
	Return order=new Return();
	//Order order=new Order();
	order.setStoreId(storeId);
	order.setOrderType(details.getOrderType());
	
	List<Return> ord=returnDao.ordersForStoreRunSheet(order);
	Return runsheeetVaulu=ord.get(0);
	
	System.out.println("runsheer");
	
	String runSheetNumber=runsheeetVaulu.getStoreRunsheet();
	System.out.println("runsheeet value in order controller is" +runSheetNumber);
	String accountNo=(String) sessi.getAttribute("loggedStore");
	Store obj=storeDao.getStoreByAccountNo(accountNo);
	
	

	
	
	 if(RandomRunsheetNumber!=null){
		 
			
			 System.out.println("befor calling updateRunsheet");
			 
			 returnDao.getRunSheetNumberForStore(storeId, RandomRunsheetNumber,order.getOrderType());
			 System.out.println("After calling updateRunsheet");
			 
			 
		 }
	
	
	
	
	
	
	
	try{
	FileWriter writer = new FileWriter(path1);

	 
    writer.append("STORE ID");
    writer.append(',');
    writer.write(obj.getName());
    //writer.write(obj.getName());
    writer.append('\n');

    writer.append("ASSIGNEE NAME");
    writer.append(',');
    writer.write(storeName);
        writer.append('\n');
		
    writer.append("ASSIGNEE CONTACT NO");
    writer.append(',');
    writer.append("");
    writer.append('\n');
    
    writer.append("RUNSHEET NUMBER");
    writer.append(',');
    writer.write(RandomRunsheetNumber);
    writer.append('\n');
    
    writer.append('\n');
    
    writer.append("COD");
    writer.append(',');
    writer.append("CUSTOMER NAME");
    writer.append(',');
    writer.append("CUSTOMER ADDRESS");
    writer.append(',');
    writer.append("CONTACT NO");
    writer.append(',');
    writer.append("TRACKING ID");
    writer.append(',');
    writer.append("SHIPMENT TYPE");
    writer.append(',');
    writer.append("RETAILER ID");
    writer.append(',');
   writer.append('\n');
   String retailerName=null;
   String newAdd=null;
   for(Return o:ord){
	   
	   
	   
	   
	   if(o.getRetailerId()==3){
		   retailerName="Myntra";
	   }else if(o.getRetailerId()==1){
		   
		   retailerName="Zivame";
	   }
	   else{
		   retailerName="Clovia";
	   }
	   
	   writer.append(""+o.getOrderValue());
	   writer.append(',');
	   writer.write(o.getConsigneeName());
	   writer.append(',');
	   String customerAddress=o.getCustomerAddress().replace(",", ":");
	   String newcustomerAddress= customerAddress.replace("#", "No");
	   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
	 
	  System.out.println("Before"+newcustomerAddress);
	  newAdd=   newCustomerAddress1.trim();
	   System.out.println("Address After trimming"+newAdd);
	  
	   writer.write(newAdd);
	   writer.append(',');
	   writer.write(o.getPhone());
	   writer.append(',');
	   writer.write(o.getTrackingId());
	   writer.append(',');
	   writer.write(o.getOrderType());
	   writer.append(',');
	   writer.write(retailerName);
	   writer.append(',');
	   writer.append('\n');
   }
    
    
    
    
    
		
    //generate whatever data you want
		
    writer.flush();
    writer.close();
}
	

catch(IOException e)
{
     e.printStackTrace();
} 
	 RunsheetGenerator rsg= new RunsheetGenerator();
     rsg.main2(orderDao,fileName);
	// return new ModelAndView("fileupload.html");
}




public String generateRunSheetNumber(int length){
	Random number=new Random();
	
/*	Random number=new Random();
	return number.nextInt(500);*/
	char[] values = {'M','L','D' ,'A','B','R','E','F','G','K','I','N','X','0','O','1','2','5','4','5','6','7','8','9'};
      String out = "";
      for (int i=0;i<length;i++) {
          int idx=number.nextInt(values.length);
        out += values[idx];
      }
      System.out.println(out);
      return out;
    }









@RequestMapping(value="/getReturnDataById/{orderId}", method=RequestMethod.GET)
public @ResponseBody Return getReturnOrder(@PathVariable("orderId") String trackingtId) throws IOException{
	
	
	
	Return order= returnDao.getOrders(trackingtId);
	
	return order;
}

@RequestMapping(value="/updateReturnStatus" , method=RequestMethod.POST)
public void updateOrderStatus(@RequestBody Return object){
System.out.println("inside update return status");
returnDao.updateOrderStatus(object);;
}


@RequestMapping(value="/bulkReturnAssign/{assignedTo},{runSheetNo}", method=RequestMethod.POST)
public void updateOrderStatus(@PathVariable("assignedTo") String assignedTo,@PathVariable("runSheetNo") String runSheetNo,@RequestBody List<Return> orders) throws IOException{
	
	System.out.println("size of list is"+orders.size());
	System.out.println("assignTo : "+assignedTo);
	System.out.println("runSheetNo : "+runSheetNo);
	returnDao.updateAssignedToReturns2(orders, assignedTo,runSheetNo);
	//orderDao.updateOrder(object);
}


@RequestMapping(value="/assignReturnsToStore/{storeName}", method=RequestMethod.POST)
public void updateOrder(@RequestBody Return object,@PathVariable("storeName") String storeName,HttpSession session) throws IOException{
	System.out.println("Got Object"+object +storeName );
	double id=(int)storeDao.getStoreIdByName(storeName);
	returnDao.updateAssign(object,id,session);
}


//added 12/2/2015
@RequestMapping("getAllCancelledOrders")
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllCancelledOrders(HttpSession session) {	
User user=(User)session.getAttribute("user");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllCancelledOrders(session);
return orderDetails;
}


@RequestMapping(value="/getAllCancelledOrdersByData" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllCancelledOrdersByData(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session){
System.out.println("inside getAllCancelledOrdersByData");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllCancelledOrders(session,cancelledOrderByData);
return orderDetails;
}

	//addded 12/3/15 2:07pm
@RequestMapping(value="/updateCancelledOrderStatus" , method=RequestMethod.POST)
public void updateCancelledOrderStatus(@RequestBody CancelledOrderByData cancelledOrderByData) {
try {
System.out.println("inside updateCancelledOrderStatus");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
orderDao.updateCancelledOrderStatus(cancelledOrderByData);
} catch (Exception e) {
//TODO Auto-generated catch block
e.printStackTrace();
}
}

//added 12/3/2015 4:31pm

@RequestMapping(value="/getAllRTOConfirmedOrders" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllRTOConfirmedOrders(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session){
System.out.println("inside getAllRTOConfirmedOrders");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllRTOConfirmedOrders(session,cancelledOrderByData);
return orderDetails;
}

@RequestMapping(value="/updateReferenceNumberForRTO" , method=RequestMethod.POST)
public void updateReferenceNumberForRTO(@RequestBody CancelledOrderByData cancelledOrderByData) {
try {
System.out.println("inside updateCancelledOrderStatus");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
orderDao.updateReferenceNoForRTO(cancelledOrderByData);
} catch (Exception e) {
//TODO Auto-generated catch block
e.printStackTrace();
}
}

//added 12/5/2015 3:33pm screen4
@RequestMapping(value="/getAllPendingReturns" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Return> getAllPendingReturns(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session) {
System.out.println("getAllPendingReturns");
List<Return> listOfPendingReturns = returnDao.getAllPendingReturns(session, cancelledOrderByData);
return listOfPendingReturns;
}

@RequestMapping(value="/updateReferenceNumberForReturn" , method=RequestMethod.POST)
public void updateReferenceNumberForReturn(@RequestBody CancelledOrderByData cancelledOrderByData) {
try{
System.out.println("inside updateReferenceNumberForReturn");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
returnDao.updateReferenceNumberForReturn(cancelledOrderByData);	
} catch(Exception e) {
e.printStackTrace();
}	
}


//screen 5
@RequestMapping(value="/getAllReturnDispatchedOrdersWithRefNo" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Return> getAllReturnDispatchedOrdersWithRefNo(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session) {
System.out.println("inside getAllRTODispatchedOrdersWithRefNo");
List<com.dataisys.taykit.model.Return> orderDetails = returnDao.getAllReturnDispatchedOrdersWithRefNo(session,cancelledOrderByData);
return orderDetails;
}

//For Screen3
@RequestMapping(value="/getAllRTODispatchedOrdersWithRefNo" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllRTODispatchedOrdersWithRefNo(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session) {
System.out.println("inside getAllRTODispatchedOrdersWithRefNo");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllRTODispatchedOrdersWithRefNo(session,cancelledOrderByData);
return orderDetails;
}

@RequestMapping(value="/uploadPor", method =RequestMethod.POST )
public @ResponseBody ModelAndView uploadFile1( UploadForm uploadItem, BindingResult result) throws IOException

{
	System.out.println("inside upload por "+uploadItem+" binding result"+result);
	if(uploadItem == null) {
		System.out.println("file is null");
	} else {
		System.out.println("file is not null");
	}
	System.out.println(uploadItem.getFile().getOriginalFilename());
	System.out.println(	"upload file"+uploadItem.getFile());
	InputStream is = uploadItem.getFile().getInputStream();
	System.out.println("inputstream is  "+is);
	HSSFWorkbook workbook = new HSSFWorkbook(is); 
	System.out.println("HSSFWorkbook is "+workbook);
	HSSFSheet sheet = workbook.getSheetAt(0);
	System.out.println("HSSFSheet is "+sheet);
	Iterator<Row> rowIterator = sheet.iterator();
	System.out.println("Iterator is "+rowIterator);
	   
  while (rowIterator.hasNext()) {
      Row nextRow = rowIterator.next();
      Iterator<Cell> cellIterator = nextRow.cellIterator();
       
      while (cellIterator.hasNext()) {
          Cell cell = cellIterator.next();
           
          switch (cell.getCellType()) {
              case Cell.CELL_TYPE_STRING:
                  System.out.print(cell.getStringCellValue());
                  System.out.println("its string type");
                  break;
              case Cell.CELL_TYPE_BOOLEAN:
                  System.out.print(cell.getBooleanCellValue());
                  System.out.println("its boolean type");
                  break;
              case Cell.CELL_TYPE_NUMERIC:
                  System.out.print(cell.getNumericCellValue());
                  System.out.println("its numeric type");
                  break;
          }
          System.out.print(" - ");
      }
}
	return null;
}

//Added By Kishore on 12-23-2015
@RequestMapping(value="/updateReturnOrderStatus/{status},{reason}", method=RequestMethod.POST)
public void updateReturnOrderStatus(@PathVariable("status") String status,@PathVariable("reason") String reason,@RequestBody List<Return> returnOrders) throws IOException{
	returnDao.updateReturnStatus(returnOrders, status, reason);
	//orderDao.updateOrder(object);
}
@RequestMapping(value="/updateReturnOrder", method=RequestMethod.POST)
public void updateReturnOrder(@RequestBody Return object) throws IOException{
	//System.out.println("Got Object"+object);
	System.out.println("Inside ReturnController-> updateReturnOrder");
	returnDao.updateOrderReturn(object);
}


@RequestMapping(value="/searchReturn" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Return> searchOrder(@RequestBody SearchReturn searchReturn) {
    System.out.println("Inside searchOrder in OrderController");
    System.out.println("OrderStatus: "+searchReturn.getOrderStatus());
    System.out.println("OrderId: "+searchReturn.getReturnId());
    System.out.println("FromDate: "+searchReturn.getFromDate());
    System.out.println("ToDate: "+searchReturn.getToDate());
    System.out.println("RetailerName: "+searchReturn.getRetailerName());
    System.out.println("payment type"+searchReturn.getPaymentType());
    System.out.println("customer contact "+searchReturn.getCustomerContact());
    System.out.println("assigned store name "+searchReturn.getAssignedStoreName());
    System.out.println("location is "+searchReturn.getLocation());
    System.out.println("delivered from date "+searchReturn.getFromDate());
    System.out.println("deliverd to date "+searchReturn.getDeliveredToDate());
    List<Return> listOfPendingReturns = returnDao.getOrdersBasedOnSearch1(searchReturn); 
    System.out.println("response is "+listOfPendingReturns.size());
    
    return listOfPendingReturns;
}}



/*//added 12/2/2015
@RequestMapping("getAllCancelledOrders")
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllCancelledOrders(HttpSession session) {	
User user=(User)session.getAttribute("user");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllCancelledOrders(session);
return orderDetails;
}


@RequestMapping(value="/getAllCancelledOrdersByData" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllCancelledOrdersByData(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session){
System.out.println("inside getAllCancelledOrdersByData");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllCancelledOrders(session,cancelledOrderByData);
return orderDetails;
}

	//addded 12/3/15 2:07pm
@RequestMapping(value="/updateCancelledOrderStatus" , method=RequestMethod.POST)
public void updateCancelledOrderStatus(@RequestBody CancelledOrderByData cancelledOrderByData) {
try {
System.out.println("inside updateCancelledOrderStatus");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
orderDao.updateCancelledOrderStatus(cancelledOrderByData);
} catch (Exception e) {
// TODO Auto-generated catch block
e.printStackTrace();
}
}

//added 12/3/2015 4:31pm

@RequestMapping(value="/getAllRTOConfirmedOrders" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> getAllRTOConfirmedOrders(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session){
System.out.println("inside getAllRTOConfirmedOrders");
List<com.dataisys.taykit.model.Order> orderDetails = orderDao.getAllRTOConfirmedOrders(session,cancelledOrderByData);
return orderDetails;
}

@RequestMapping(value="/updateReferenceNumberForRTO" , method=RequestMethod.POST)
public void updateReferenceNumberForRTO(@RequestBody CancelledOrderByData cancelledOrderByData) {
try {
System.out.println("inside updateCancelledOrderStatus");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
orderDao.updateReferenceNoForRTO(cancelledOrderByData);
} catch (Exception e) {
// TODO Auto-generated catch block
e.printStackTrace();
}
}

//added 12/5/2015 3:33pm
@RequestMapping(value="/getAllPendingReturns" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Return> getAllPendingReturns(@RequestBody CancelledOrderByData cancelledOrderByData, HttpSession session) {
System.out.println("getAllPendingReturns");
List<Return> listOfPendingReturns = returnDao.getAllPendingReturns(session, cancelledOrderByData);
return listOfPendingReturns;
}

@RequestMapping(value="/updateReferenceNumberForReturn" , method=RequestMethod.POST)
public void updateReferenceNumberForReturn(@RequestBody CancelledOrderByData cancelledOrderByData) {
try{
System.out.println("inside updateReferenceNumberForReturn");
System.out.println("Size of trackingId list is: "+cancelledOrderByData.getTrackingIds().size());
System.out.println("UpdatedCancelledOrder Status is: "+cancelledOrderByData.getStatus());
returnDao.updateReferenceNumberForReturn(cancelledOrderByData);	
} catch(Exception e) {
e.printStackTrace();
}	
}
}
*/