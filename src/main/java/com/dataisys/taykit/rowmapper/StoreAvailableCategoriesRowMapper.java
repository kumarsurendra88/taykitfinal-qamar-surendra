package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreCategory;
import com.dataisys.taykit.model.StoreServices;

public class StoreAvailableCategoriesRowMapper implements RowMapper<StoreCategory>{
	
	
	public StoreCategory mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		StoreCategory service= new StoreCategory();
		service.setCategoryName(rs.getString("category_name"));
		service.setProductCategoryId(rs.getDouble("product_category_id"));
		service.setStoreCategoryId(rs.getDouble("store_category_id"));
		service.setStoreId(rs.getDouble("store_id"));
		service.setUpdatedBy(rs.getString("updated_by"));
		return service;
		
	}



}
