package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Region;

public interface RegionDAO {

	public boolean CreatRegion(int zone_id,String region_name,String region_desc);
	public List<Region> getNames(int zone_id);
	public List<Region> getRegionByZoneId(int zone_id);
	public List<Region> getRegionByZoneName(String zone_name);
	public List<Region> getRegions();
	public List<Region> getRegionList();
	public Region getRegionsById(double id);
	public void updateRegion(Region region, int id);
	
}
