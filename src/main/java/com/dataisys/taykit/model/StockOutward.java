package com.dataisys.taykit.model;

public class StockOutward {
	private String fromLocation;
	private String toLocation;
	private String trackingId;
	private String personName;
	private String personNo;
	
	private String orderStatus;
	public StockOutward() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StockOutward(String fromLocation, String toLocation,
			String trackingId, String personName, String personNo) {
		super();
		this.fromLocation = fromLocation;
		this.toLocation = toLocation;
		this.trackingId = trackingId;
		this.personName = personName;
		this.personNo = personNo;
	}
	
	

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getFromLocation() {
		return fromLocation;
	}
	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}
	public String getToLocation() {
		return toLocation;
	}
	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getPersonNo() {
		return personNo;
	}
	public void setPersonNo(String personNo) {
		this.personNo = personNo;
	}
	
	@Override
	public String toString() {
		return "StockOutward [fromLocation=" + fromLocation + ", toLocation="
				+ toLocation + ", trackingId=" + trackingId + ", personName="
				+ personName + ", personNo=" + personNo + "]";
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
	
	
}
