package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.MasterBag;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Store;

public interface MasterBagDao {

	public boolean createMasterBag(String destination,String masterbag_type,String type,HttpSession session);

	public boolean AddToMasterBag(double masterbag_no,String order_id, String masterbag_type,
			HttpSession session);

	public List<MasterBag> getMasterBagListById(String user_id,
			String masterbag_type, HttpSession session);

	public boolean ActivateMasterBag(double masterbag_no, HttpSession session);

	public List<MasterBag> GetMasterBagData(double masterbag_no,
			String masterbag_type, HttpSession session);

	public Hub getUserMasterBag(HttpSession session);

	public List<MasterBag> getMasterBagListForCurrentUser(HttpSession session);


	public MasterBag GetMasterBagDetailsByNo(double masterbag_no,
			HttpSession session);

	public Order UpdateOrderByInwardMasterbag(double masterbag_no,String tracking_id,
			String location, String fromlocation,HttpSession session);

	public MasterBag GetMasterBagDestinationByNo(double masterbag_no,
			HttpSession session);

	

}
