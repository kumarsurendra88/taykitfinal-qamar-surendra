package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.RegistrationKey;

public class RegistrationKeyRowMapper implements RowMapper<RegistrationKey>{

	public RegistrationKey mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub		
		RegistrationKey regKey= new RegistrationKey();
		regKey.setAccountNo(rs.getString("account_no"));
		regKey.setRegKey(rs.getString("reg_key"));
		return regKey;
	}

}
