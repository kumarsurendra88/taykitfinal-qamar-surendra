package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.User;

public class UserMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
	//	user.setAccountNo(rs.getString("account_no"));
		user.setAccNumber(rs.getString("account_no"));
		user.setUserId(rs.getDouble("user_id"));
		user.setUserType(rs.getString("user_type"));
		//user.setUserFullName(rs.getString("user_fullname"));
		user.setUserName(rs.getString("user_fullname"));
		user.setUserEmail(rs.getString("user_email"));
	
		user.setUserMobil(rs.getString("user_mobile"));
		//user.setUserLoginName(rs.getString("user_loginname"));
		user.setUserLogin(rs.getString("user_loginname"));
		//user.setUserLoginPassword(rs.getString("user_loginpassword"));
		user.setUserPassword(rs.getString("user_loginpassword"));
		user.setUserStatus(rs.getString("user_status"));
		user.setUserChangePassword(rs.getString("user_changepassword"));
		user.setUserLastLogin(rs.getTimestamp("user_lastlogin"));
		user.setCreatedBy(rs.getString("created_by"));
		//user.setCreatedOn(rs.getDate("created_on"));
		
		user.setUpdatedBy(rs.getString("updated_by"));
		user.setUpdatedOn(rs.getTimestamp("updated_on"));
		return user;
	}
}
