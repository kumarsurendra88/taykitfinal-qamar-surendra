package com.dataisys.taykit.daoimpl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.Module;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.ModuleRowMapper;
import com.dataisys.taykit.rowmapper.UserMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;

public class UserDaoImpl extends JdbcDaoSupport implements UserDao{

	public boolean createUser(User user,HttpSession session) {
		
//		System.out.println("******values : "+user.getAccNumber()+user.getUserType()+user.getUserName()+user.getUserEmail()+user.getUserMobil()+user.getUserLogin()+user.getUserPassword());
			//System.out.println("%$%$%$%$%$% usertype in impl: "+user.getUserType());
			
			System.out.println("username : "+user.getUserName());
			System.out.println("account or id : "+user.getAccNumber());
			System.out.println("user type : "+user.getUserType());
			System.out.println("user pass : "+user.getUserPassword());
			System.out.println("zone : "+user.getZone_name());
			System.out.println("region : "+user.getRegion_name());
			System.out.println("hub : "+user.getHub_name());
			
			
			
			User user1=(User)session.getAttribute("user");
			//double storeId=(Double)session.getAttribute("storeId");
		/*	String sql="insert into `user`(account_no,user_type,user_fullname,user_email,user_mobile,user_loginname,user_loginpassword)" 
					+"values(?,?,?,?,?,?,?)";*/
			int j=0;
			boolean flag=false;
			
			//=========Queries for different user types====================//
			String sql="insert into user(account_no,user_type,user_fullname, user_loginpassword) values(?,?,?,?)";
			String sqlZM="insert into user_zone (account_no,zone_id) values(?,(select zone_id from zones where zone_name=?))";
			String sqlRM="insert into user_region (account_no,region_id) values(?,(select region_id from regions where region_name=?))";
			String sqlHA="insert into user_hub (account_no,hub_id) values(?,(select hub_id from hubs where hub_name=?))";
			String sqlSA="insert into user_store (account_no,store_id,updated_by) values(?,(select store_id from store where store_name=?),?)";
			
			System.out.println("******values : "+user.getAccNumber()+user.getUserType()+user.getUserName()+user.getUserEmail()+user.getUserMobil()+user.getUserLogin()+user.getUserPassword());
			int i=getJdbcTemplate().update(sql,user.getAccNumber(),user.getUserType(),user.getUserName(),user.getUserPassword());
			if(i>0)
			{
						System.out.println("User Created");
						if(user.getUserType().equals("Zonal Manager"))
						{
							j=getJdbcTemplate().update(sqlZM,user.getAccNumber(),user.getZone_name());
							if(j>0)
								{
									System.out.println("user_zone updated");
								}
							flag=true;
						}
						else if(user.getUserType().equals("Regional Manager"))
						{
							j=getJdbcTemplate().update(sqlRM,user.getAccNumber(),user.getRegion_name());
							if(j>0)
								{
									System.out.println("user_region updated");
								}
							flag=true;
						}
						else if(user.getUserType().equals("Hub Admin"))
						{
							j=getJdbcTemplate().update(sqlHA,user.getAccNumber(),user.getHub_name());
							if(j>0)
								{
									System.out.println("user_hub updated");
								}
							flag=true;
						}
						else if(user.getUserType().equals("Hub Executive"))
						{
							j=getJdbcTemplate().update(sqlHA,user.getAccNumber(),user.getHub_name());
							if(j>0)
								{
									System.out.println("user_hub updated");
								}
							flag=true;
						}
						else if(user.getUserType().equals("Store Admin"))
						{
							System.out.println("storename : "+user.getStore_name());
							j=getJdbcTemplate().update(sqlSA,user.getAccNumber(),user.getStore_name(),user.getAccNumber());
							if(j>0)
								{
									System.out.println("user_store updated");
								}
							flag=true;
						}
				
			}
			else
			{
				System.out.println("Error occurred");
			}
			/*if(user.getUserType().equalsIgnoreCase("delivery_boy"))
			{
				String sql1="insert into deliveryboy(deliveryboy_name,mobile,store_id)"+"values(?,?,?)";
				
				
				getJdbcTemplate().update(sql1,user.getUserName(),user.getUserMobil(),storeId);
				System.out.println("inside if query executed");
				
			}
			else
			{
	String sql2="insert into user_store(account_no,store_id)"+"values(?,?)";
				
				
				getJdbcTemplate().update(sql2,user.getAccNumber(),storeId);
				System.out.println("inside else query executed");
			}*/
			
			return flag;
		
		
		
	}

	public List<User> getUSers() {
		// TODO Auto-generated method stub
		
		
		String sql="select *from `user`";
	
		List<User> listofUser=getJdbcTemplate().query(sql,new UserRowMapper());
		System.out.println("*********list of users : "+listofUser);
		System.out.println("*********************************");
		for (int i = 0; i < listofUser.size(); i++) {
			System.out.println("|"+i+"|  "+listofUser.get(i));
		}
		return listofUser;
	}

	public User getUserByCredentials(String username, String password, String accNo) {
		// TODO Auto-generated method stub
		
		User user = null;	
		try {
			/*user = getJdbcTemplate().queryForObject("SELECT * FROM user WHERE user_loginname=? and user_loginpassword=? and account_no=?",  new Object[] { username, password,accNo }, new UserRowMapper());*/
			
			user = getJdbcTemplate().queryForObject("SELECT * FROM `user` WHERE  user_loginpassword=? and account_no=?",  new Object[] { password,accNo }, new UserRowMapper());
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return user;
		
	}

	public void updateUser(User user) {
		// TODO Auto-generated method stub
		
	}

	public User getUserById(double userId) {
		String sql= "SELECT * FROM `user` WHERE user_id = ?";
		User user=getJdbcTemplate().queryForObject(sql,new Object[]{userId},new UserMapper());
		return user;
	}

	public List<User> getAllUsers() {
		String sql= "SELECT * FROM `user`";		
		List<User> listofUsers=getJdbcTemplate().query(sql,new UserMapper());
		return listofUsers;
	}
	
	public List<User> getAllUsersByType(String userType) {
		String sql = "SELECT * FROM `user` WHERE user_type = ?";
		List<User> listOfUsers = getJdbcTemplate().query(sql,new Object[]{userType},new UserMapper());
		return listOfUsers;
	}
	
	//recently added by kishore and it won't work according to me
	public User getDeliveryBoyByName(String name) {
				System.out.println("DeliveryBoy name inside getDeliveryBoyByName: "+name);
				String sql = "SELECT * FROM `user` WHERE user_fullname = ? AND user_type = ?";
				User user = null;
				try {
					user = getJdbcTemplate().queryForObject(sql,new Object[]{name, "DELIVERY_BOY"},new UserMapper());
				} catch (DataAccessException e) {
					System.out.println("Inside catch block of getDelivreyBoyByName() of UserDaoImpl");
					System.err.println("Error: "+e.getMessage());
				} catch (Exception e) {
					System.err.println("Error: "+e.getMessage());
				}
				return user;
	}
	
	public User getUserByAccNumber(String accountNumber) {
		System.out.println("Inside getUserByAccountNumber and AccountNumber is: "+accountNumber);
		String sql = "SELECT * FROM `user` WHERE account_no=?";
		
		User user = null;
		try {
			user = getJdbcTemplate().queryForObject(sql,new Object[]{accountNumber},new UserMapper());
		} catch (DataAccessException e) {
			System.out.println("Inside catch block of getUserByAccNumber of UserDaoImpl");
			System.out.println("User: "+user);
			System.err.println("Error: "+e.getMessage());
		} catch (Exception e) {
			System.err.println("Error: "+e.getMessage());
		}
		
		return user;
	}

	public Module getModuleDetails(String moduleName) {
		// TODO Auto-generated method stub
		
		
		String sql="select *from `module` where module_name=?";
		Module mod=getJdbcTemplate().queryForObject(sql, new Object[]{moduleName},new ModuleRowMapper());
		return mod;
	}

}
