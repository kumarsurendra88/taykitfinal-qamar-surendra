package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreServices;

public class RetailerTotalServiceRowMapper implements RowMapper<RetailerServices>{
	
	
	public RetailerServices mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		RetailerServices service= new RetailerServices();
		service.setServiceDesc(rs.getNString("service_desc"));
		service.setServiceName(rs.getString("service_name"));
		service.setServiceTypeId(rs.getDouble("retailer_servicetype_id"));
		service.setUpdatedBy(rs.getDouble("updated_by"));
		return service;
		
	}



}
