package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.RetailerContactRowMapper;
import com.dataisys.taykit.rowmapper.RetailerRowMapper;
import com.dataisys.taykit.rowmapper.RetailerTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class RetailerContactsDaoImpl extends JdbcDaoSupport implements RetailerContactDao {

	public void addContact(final List<RetailerContact> contacts) {
		 String SQL = "insert into retailer_contact(retailer_id,contact_name,contact_phone,contact_email,contact_designation,contact_notes) values (?,?,?,?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   RetailerContact detail = contacts.get(i);
		   ps.setDouble(1, detail.getRetailerId());
		   ps.setString(2, detail.getContactName());
		   ps.setString(3, detail.getContactMobile());
		   ps.setString(4, detail.getContactEmail());
		   ps.setString(5, detail.getContactDesignation());
		   ps.setString(6, detail.getNotes());
		  
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return contacts.size();
		      
		                  }


		                });
       return;
	}
		
	

	public RetailerContact getContact(int retailerID) {
		// TODO Auto-generated method stub
		return null;
	}



	public List<RetailerContact> getretailerContactsBId(double retailerID) {
		String sql="select * from`retailer_contact` where retailer_id=?";
		List<RetailerContact>contacts=getJdbcTemplate().query(sql, new Object[]{retailerID}, new RetailerContactRowMapper());
		return contacts;
	}



	
	
}
