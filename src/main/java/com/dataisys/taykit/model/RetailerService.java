package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class RetailerService {
private double retailerServiceId;
private double retailerId;
private Double serviceId;
private String udatedBy;
private Timestamp updatedOn;
public double getRetailerServiceId() {
	return retailerServiceId;
}
public RetailerService() {
	super();
}
public RetailerService(double retailerServiceId, double retailerId,
		Double serviceId, String udatedBy, Timestamp updatedOn) {
	super();
	this.retailerServiceId = retailerServiceId;
	this.retailerId = retailerId;
	this.serviceId = serviceId;
	this.udatedBy = udatedBy;
	this.updatedOn = updatedOn;
}
@Override
public String toString() {
	return "RetailerService [retailerServiceId=" + retailerServiceId
			+ ", retailerId=" + retailerId + ", serviceId=" + serviceId
			+ ", udatedBy=" + udatedBy + ", updatedOn=" + updatedOn + "]";
}
public void setRetailerServiceId(double retailerServiceId) {
	this.retailerServiceId = retailerServiceId;
}
public double getRetailerId() {
	return retailerId;
}
public void setRetailerId(double retailerId) {
	this.retailerId = retailerId;
}
public Double getServiceId() {
	return serviceId;
}
public void setServiceId(Double serviceId) {
	this.serviceId = serviceId;
}
public String getUdatedBy() {
	return udatedBy;
}
public void setUdatedBy(String udatedBy) {
	this.udatedBy = udatedBy;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}
}

