package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.CategoryDao;
import com.dataisys.taykit.model.Category;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.CategoryRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;

public class CategoryDaoImpl extends JdbcDaoSupport implements CategoryDao  {

	public void createCategory(Category category) {
		// TODO Auto-generated method stub
		
		String sql="insert into product_category(category_name,category_desc,category_status,updated_by)"+"values(?,?,?,?)";
		getJdbcTemplate().update(sql,category.getCategoryName(),category.getCategoryDesc(),category.getCategroyStatus(),category.getUpdatedBy());
		
	}

	public void updateCategory(Category category) {
		// TODO Auto-generated method stub
		
	}

	public List<Category> getCategory() {
		// TODO Auto-generated method stub
		
		String sql="select *from product_category";
				List<Category> listofCategory=getJdbcTemplate().query(sql,new CategoryRowMapper());
		
		return listofCategory;
		
		
		
	}

}
