package com.dataisys.taykit.model;


import java.sql.Date;
import java.sql.Timestamp;


public class Customer {


	private double customerId;
	private String customerName;
	private String customerAddress;
	private String customerMobile;
	private String customerEmail;
	private String customerStatus;
	private String customerType;
	private String gender;
	private Date dob;
	private String customerCategory;
	private String customerGpsLocation;
	private String city;
	private String pinCode;
	private String id;
	private String value;
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	
	
	
	
	
	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public String getPinCode() {
		return pinCode;
	}


	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}
	private Timestamp updatedOn;
	private String updatedBy;
	
	
	
	public Customer(){}
	
	
	public Customer(double customerId, String customerName,
			String customerAddress, String customerMobile,
			String customerEmail, String customerStatus, String customerType,
			String customerCategory, String customerGpsLocation,
			Timestamp updatedOn, String updatedBy) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerMobile = customerMobile;
		this.customerEmail = customerEmail;
		this.customerStatus = customerStatus;
		this.customerType = customerType;
		this.customerCategory = customerCategory;
		this.customerGpsLocation = customerGpsLocation;
		this.updatedOn = updatedOn;
		this.updatedBy = updatedBy;
	}
	public double getCustomerId() {
		return customerId;
	}
	public void setCustomerId(double customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCustomerCategory() {
		return customerCategory;
	}
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}
	public String getCustomerGpsLocation() {
		return customerGpsLocation;
	}
	public void setCustomerGpsLocation(String customerGpsLocation) {
		this.customerGpsLocation = customerGpsLocation;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


}
