package com.dataisys.taykit.daoimpl;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.OrderDeliveryDAO;
import com.dataisys.taykit.mobile.model.OrderDelivery;

public class OrderDeliveryDAOImpl extends JdbcDaoSupport implements OrderDeliveryDAO {
	public void createOrderDelivery(OrderDelivery orderDelivery) {
		String sql = "insert into order_delivery(tracking_id,user_id,date,status,location,notes,updated_by,customer_signature)"
				+ "VALUES (?,?,?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,orderDelivery.getTrackingId(),orderDelivery.getUserId(),
				orderDelivery.getDate(),orderDelivery.getStatus(),orderDelivery.getDeliveredLocation(),
				orderDelivery.getNotes(),orderDelivery.getUpdatedBy(),orderDelivery.getSignature());
	}
}