package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class ProductCategories {
private double categoryId;
private String categoryName;
private String categoryDesc;
private boolean categoryStatus;
private double updatedBy;
private Timestamp UpdatedOn;
public double getCategoryId() {
	return categoryId;
}
public void setCategoryId(double d) {
	this.categoryId = d;
}
public String getCategoryName() {
	return categoryName;
}
public ProductCategories(double categoryId, String categoryName,
		String categoryDesc, boolean categoryStatus, double updatedBy,
		Timestamp updatedOn) {
	super();
	this.categoryId = categoryId;
	this.categoryName = categoryName;
	this.categoryDesc = categoryDesc;
	this.categoryStatus = categoryStatus;
	this.updatedBy = updatedBy;
	UpdatedOn = updatedOn;
}
public ProductCategories() {
	super();
}
@Override
public String toString() {
	return "StoreProductCategory [categoryId=" + categoryId + ", categoryName="
			+ categoryName + ", categoryDesc=" + categoryDesc
			+ ", categoryStatus=" + categoryStatus + ", updatedBy=" + updatedBy
			+ ", UpdatedOn=" + UpdatedOn + "]";
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public String getCategoryDesc() {
	return categoryDesc;
}
public void setCategoryDesc(String categoryDesc) {
	this.categoryDesc = categoryDesc;
}
public boolean getCategoryStatus() {
	return categoryStatus;
}
public void setCategoryStatus(boolean categoryStatus) {
	this.categoryStatus = categoryStatus;
}
public double getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(double updatedBy) {
	this.updatedBy = updatedBy;
}
public Timestamp getUpdatedOn() {
	return UpdatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	UpdatedOn = updatedOn;
}


} 
