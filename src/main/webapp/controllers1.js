

//irfan,s factory
/*routerApp.service('newStoreContacts', [function () {

    // private
    var contacts= [];

    this.setContacts = function(newObj) {
    	contacts.push(newObj);
    	console.log(contacts);
       return [].concat(contacts);
    };
    this.getContacts = function(){
        // will return a copy of the contacts at the time of request
    	console.log("in service get"+contacts);
        return [].concat(contacts);
    };    
    this.clear = function(){
    	contacts = [];
        return [].concat(contacts);
    };

}]);*/

routerApp.controller('returnUpdate', ['$scope','$http','TrackindIdFactory','$location', function($scope,$http,TrackindIdFactory,$location){

        var id=TrackindIdFactory.getTrackingId();
        console.log("going to do query on tracking id "+id);
        console.log("inside return update controller");
        $http.get("./getDeliveryBoys")

        .success(function(response) {

        $scope.deliveryBoys=response;
        console.log("got response from deliveryboys")
        });
        
        if(id==0){
            alert("no data with the selected tracking id");
        }else{
        $http.get("./getReturnDataById/"+id)
        
        .success(function(response) {
            
            $scope.dis = response;
            console.log("got data from getreturnbyid")
            
        });
        }
        
    }])

routerApp.factory('TrackindIdFactory', function() {
	var data = [ {
		trackingId : 0
	} ];
	return {
		getTrackingId : function() {
			return data.trackingId;
		},
		setTrackingId : function(track) {
			data.trackingId = track;

		}
	};
});










routerApp.factory('updatestore', function($resource) {
"use strict";
return $resource('./UpdateStoreById/:id', {}, {
update : {
method : 'PUT'
},
save : {
method : 'POST'
},
query : {
method : 'GET',
isArray : true
}
});
});
routerApp.service('newStoreContacts', [function () {

    // private
    var contacts= [];

    this.setContacts = function(newObj) {
    	contacts.push(newObj);
    	console.log(contacts);
       return [].concat(contacts);
    };
    this.getContacts = function(){
        // will return a copy of the personArray at the time of request
    	console.log("in service get"+contacts);
        return [].concat(contacts);
    };    
    this.clear = function(){
    	contacts = [];
        return [].concat(contacts);
    };

}]);

routerApp.factory('StoreIdFactory', function() {
	var data = [ {
		storeId : 0
	} ];
	return {
		getStoreId : function() {
			return data.storeId;
		},
		setStoreId : function(storeId) {
			
			data.storeId = storeId;

		}
	};
});

routerApp.factory('HubIdFactory', function() {
	var data = [ {
		hub_id : 0
	} ];
	return {
		getHub_id : function() {
			return data.hub_id;
		},
		setHub_id : function(hub_id) {
			
			data.hub_id = hub_id;

		}
	};
});
routerApp.factory('RegionIdFactory', function() {
	var data = [ {
		region_id : 0
	} ];
	return {
		getRegion_id : function() {
			return data.region_id;
		},
		setRegion_id : function(region_id) {
			
			data.region_id = region_id;

		}
	};
});


routerApp.factory('updateretailer', function($resource) {
	"use strict";
	return $resource('./updateRetailerbyid/:id', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('RetailerIdFactory', function() {
	var data = [ {
		retailerId : 0
	} ];
	return {
		getRetailerId : function() {
			return data.retailerId;
		},
		setRetailerId : function(track) {
			
			data.retailerId = track;

		}
	};
});
routerApp.factory('ShortfallFactory', function() {
	var data = [ {
		name : 0
	} ];
	return {
		getShortfall : function() {
			return data.name;
		},
		setShortfall : function(track) {
			
			data.name = track;

		}
	};
});
routerApp.factory('CashToCollectFactory', function() {
	var data = [ {
		name : 0
	} ];
	return {
		getCash : function() {
			return data.name;
		},
		setCash : function(track) {
			
			data.name = track;

		}
	};
});
routerApp.factory('CodStatusFactory', function() {
	var data = [ {
		name : ''
	} ];
	return {
		getStatus : function() {
			return data.name;
		},
		setStatus : function(track) {
			
			data.name = track;

		}
	};
});
routerApp.factory('DeliveryUserFactory', function() {
	var data = [ {
		name : ''
	} ];
	return {
		getName : function() {
			return data.name;
		},
		setName : function(track) {
			
			data.name = track;

		}
	};
});


routerApp.factory('RunsheetNoFactory', function() {
	var data = [ {
		retailerId : 0
	} ];
	return {
		getRunsheetNo : function() {
			return data.retailerId;
		},
		setRunsheetNo : function(track) {
			
			data.retailerId = track;

		}
	};
});


routerApp.factory('addretailer', function($resource) {
	"use strict";
	return $resource('./addRetailer', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('retailerServices', function($resource) {
	"use strict";
	return $resource('./getRetailerServices', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('retailerServicesAdd', function($resource) {
	"use strict";
	return $resource('./RetailerServicesAdd', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('CategoryServicesAdd', function($resource) {
	"use strict";
	return $resource('./storeCategoryServicesAdd', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storeCategories', function($resource) {
	"use strict";
	return $resource('./getStorecategories', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storeServicesAdd', function($resource) {
	"use strict";
	return $resource('./StoreServicesAdd', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storeServices', function($resource) {
	"use strict";
	return $resource('./getStoreServices', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('StoreAdd', function($resource) {
	"use strict";
	return $resource('./addStore', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storeContacts', function($resource) {
	"use strict";
	return $resource('./getStoreContact', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('contact', function($resource) {
	"use strict";
	return $resource('./addRetailerContact', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});

routerApp.factory('contactsadded', function($resource) {
	"use strict";
	return $resource('./getRetailerContact', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('warehousesAdded', function($resource) {
	"use strict";
	return $resource('./getRetailerWarehouse', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});

routerApp.factory('hubs', function($resource) {
	"use strict";
	return $resource('./getHubs', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('regions', function($resource) {
	"use strict";
	return $resource('./getRegions', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});


routerApp.factory('stores', function($resource) {
	"use strict";
	return $resource('./getStores', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('userslist', function($resource) {
	"use strict";
	return $resource('./getUser', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});

routerApp.factory('devices', function($resource) {
	"use strict";
	return $resource('./getDevices', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('addDevice', function($resource) {
	"use strict";
	return $resource('./addDevices', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storecontact', function($resource) {
	"use strict";
	return $resource('./addstoreContact', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
routerApp.factory('warehouse', function($resource) {
	"use strict";
	return $resource('./addRetailerWarehouse', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
routerApp.factory('getRetailer', function($resource) {
"use strict";
return $resource('./getRetailer', {}, {
update : {
method : 'PUT'
},
query : {
method : 'GET',
isArray : true
}
});
});
routerApp.service('con', [function () {

    // private
    var contacts= [];

    this.setContacts = function(newObj) {
    	contacts.push(newObj);
    	console.log(contacts);
       return [].concat(contacts);
    };
    this.getContacts = function(){
        // will return a copy of the personArray at the time of request
    	console.log("in service get"+contacts);
        return [].concat(contacts);
    };    
    this.clear = function(){
    	contacts = [];
        return [].concat(contacts);
    };

}]);
routerApp.service('retWarehouse', [function () {

    // private
    var warehouses= [];

    this.setWarehouse = function(newObj) {
    	warehouses.push(newObj);
    	console.log(warehouses);
       return [].concat(warehouses);
    };
    this.getWarehouse = function(){
        // will return a copy of the personArray at the time of request
    	console.log("in service get"+warehouses);
        return [].concat(warehouses);
    };    
    this.clear = function(){
    	warehouses = [];
        return [].concat(warehouses);
    };

}]);
routerApp.service('newStoreContacts', [function () {

    // private
    var contacts= [];

    this.setContacts = function(newObj) {
    	contacts.push(newObj);
    	console.log(contacts);
       return [].concat(contacts);
    };
    this.getContacts = function(){
        // will return a copy of the personArray at the time of request
    	console.log("in service get"+contacts);
        return [].concat(contacts);
    };    
    this.clear = function(){
    	contacts = [];
        return [].concat(contacts);
    };

}]);
/*routerApp.factory('inwardEntry', function($resource) {
	"use strict";
	return $resource('192.168.0.110:8080//pudo/InwardEntry', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});*/
routerApp.factory('addDevice', function($resource) {
	"use strict";
	return $resource('./addDevices', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : true
		}
	});
});
routerApp.factory('storecontact', function($resource) {
	"use strict";
	return $resource('./addstoreContact', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
routerApp.factory('warehouse', function($resource) {
	"use strict";
	return $resource('./addRetailerWarehouse', {}, {
		update : {
			method : 'PUT'
		},
		save : {
			method : 'POST'
		},
		query : {
			method : 'GET',
			isArray : false
		}
	});
});
//.............
routerApp.factory('deposit', function($resource) {
"use strict";
return $resource('./depositNew', {}, {
update : {
method : 'PUT'
},
query : {
method : 'GET',
isArray : true
}
});
});

routerApp.factory('getRetailer', function($resource) {
"use strict";
return $resource('./getRetailer', {}, {
update : {
method : 'PUT'
},
query : {
method : 'GET',
isArray : true
}
});
});

function commentsController($scope, $http)
{

$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
$http.get("./getComments").success(function(data)
{
$scope.comments = data;

 });

$scope.addComment = function(){
// Validate the comment is not an empty and undefined
if("undefined" != $scope.comment){
	
// Angular AJAX call
console.log("data i m sending is"+$scope.comment)
$http({
method : "POST",
url : "./createComment" , 
data :"commentText="+$scope.comment
}).success(function(data){
// Add the data into DOM for future use
$scope.comments.unshift(data); 
});
$scope.comment = "";
}
}

// index : index of global DOM
$scope.deleteComment = function(index){
// Angular AJAX call
$http({
method : "GET",
url : "index.php?action=delete&id="+$scope.comments[index].id,
}).success(function(data){
// Removing Data from Global DOM
$scope.comments.splice(index,1);
});
}
}

function depositController($scope, $http, deposit, $state, $location,$filter,getRetailer) {

//Setting Today's date as default
$scope.deposit={date:$filter("date")(Date.now(), 'yyyy-MM-dd')}

// Getting the amount to be collected for selected retailer
$scope.getRet=function(name){

getRetailer.save(name,function(data){
console.log(data.value)
if(data.value>0){
$scope.deposit.amount=data.value;
}else if(data.value==0){
alert("No deposits found for "+name+" today.. Select another Retailer..")
document.getElementById("retailer").focus();
}else{
alert("Deposit for the "+name+" is already made.. Select another Retailer");
document.getElementById("retailer").focus();
}

});
}

  // Getting all retailers
$http.get("./getRetailer").success(function(response) {
$scope.retailers = response
});

// Saving the details to the database
$scope.insert = function(deposits) {

console.log(deposits);

/*deposits.img = document.getElementById('image').value;
console.log(deposits);
*/
deposit.save(deposits, function(data) {
alert("Deposit Details Saved...!!")
$scope.deposit = {};
//document.getElementById('image').value="";
$scope.deposit={date:$filter("date")(Date.now(), 'yyyy-MM-dd')}

})

};
}

function productController($scope,$http,$location) {
    
	
	
	$http.get("./getOrder")
	
	.success(function(response) {$scope.names = response;});
	 $scope.filterOptions = {
		        filterText: ''
		      };
	
	$scope.gridOptions = { 
        data: 'names',
        
        enableCellSelection: true,
        enableRowSelection: false,
        showSelectionCheckbox: true,
        enableCellEdit: true,
        columnDefs: [
                     {
                    	 field: 'orderId', 
                    	 displayName: 'OrderId', 
                    	 enableCellEdit: false,
                    	 cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>' 
                     
                    },
                    {field: 'trackingId', displayName: 'trackingId', enableCellEdit: false, cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>' },
                     {field: 'orderTitle', displayName: 'OrderTitle', enableCellEdit: false}, 
                     {field:'customerName', displayName:'CustomerName', enableCellEdit: false},
                     {field:'customerContactno', displayName:'ContactNo', enableCellEdit: false},
                     {field:'customerEmailId', displayName:'Email', enableCellEdit: false},
                     {field:'orderStatus', displayName:'orderStatus', enableCellEdit: false},
                     {field:'paymentType', displayName:'paymentType', enableCellEdit: false},
                     {field:'reciptDate', displayName:'reciptDate', enableCellEdit: false},
                     {field:'deliveredDate', displayName:'DeliveredDate', enableCellEdit: false},
                     {field:'retailerId', displayName:'retailerId', enableCellEdit: false},
                     {field:'OutletId', displayName:'OutletId', enableCellEdit: false},
                     {field:'value', displayName:'value', enableCellEdit: false}
                     ],
                     filterOptions: $scope.filterOptions
                     
                    
    };
	 $scope.loadById = function(row) {  
		 window.console &&console.log(row.entity);
		 var ordId=row.entity.orderId;
		 console.log(ordId);
		 var data=row.entity;
		 console.log("values" +data);
		 $scope.dis=data;
		 //$scope.dis= row.entity;
		 console.log($scope.dis);
			$location.path('updateorder');
  	   	// $window.location.href= 'newPage/='+ row.entity;
		
		// $state.go('updateorder')
  	};
   
}





function customerController($scope,$http) {
    
	
	 $scope.gridOptions1 = {
			    paginationPageSizes: [25, 50, 75],
			    enableFiltering: true,
			    paginationPageSize: 25,
			   
			    columnDefs: [
			      { name: 'sl no',field:'customerId' },
			      { name: 'Customer Email' ,field:'customerEmail'},
			      { name: 'Customer Name ',field:'customerName' },
			      { name: 'Customer Address ',field:'customerAddress' },
			      { name: 'Customer Mobile ',field:'customerMobile' },
			      { name: ' City ' , field:'city' },
			      
			      ]
			  };
	
	$http.get('./getCustomer')
	
	
	.success(function(response) { 
		
		$scope.gridOptions1.data = response;
		
	});

	
    
    /*$scope.openMap = function($window) {
   	 console.log("Inside openMap()");
   	 //$window.open('angularJS/partials/angularMaps/menuMap.html'); original code
   	 window.open('map.html');
    }*/
    $scope.loadById = function(row) {  
    	  // window.console && console.log(row.entity);
    	   console.log("customerEmail is" +row.entity.customerEmail)
    	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
    	   var uri="mapForEachVehicle.jsp?customerMobile="+row.entity.customerMobile;
    	   console.log("uri" +uri)
    	   $window.open(uri);
    	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
    }; 
    
    $scope.loadId = function(row) { 
		 console.log("Inside edit()");
		/* var ordId=row.entity.orderId;*/
		 var CustId=row.entity.customerId;
		 updateFactory.setId(CustId);
		 console.log(CustId);
		 $location.path('updateCustomer');
		 
		};
}

//update CustomerController
function updateCustomerController($scope,$http,updateFactory) {
	
	var CustId=updateFactory.getId();
	console.log("customer Id: "+CustId);
	$http.get('./getCustomer/'+CustId)
	 .success(function(data,status, headers, config)
			 {
		 			$scope.dis = data;
		 			$scope.CustName=$scope.dis.customerEmail;
		 			console.log("custmer Email is" +$scope.CustName)
		 			console.log("customer Mobile"+$scope.dis.customerId)
		 			
		 });
	
	
	
}

function FrmController($scope) {
    $scope.comment = [];
    $scope.btn_add = function() {
        if($scope.txtcomment !=''){
        $scope.comment.push($scope.txtcomment);
        $scope.txtcomment = "";
        }
    }

    $scope.remItem = function($index) {
        $scope.comment.splice($index, 1);
    }


    

    // index : index of global DOM
    /*$scope.deleteComment = function(index){
    // Angular AJAX call
    $http({
    method : "GET",
    url : "index.php?action=delete&id="+$scope.comments[index].id,
    }).success(function(data){
    // Removing Data from Global DOM
    $scope.comments.splice(index,1);
    }*/
    };
    

            
 /*function commentsController($scope, $http)
    {
    	$scope.user = {};
   
    $http.get("./getComments").success(function(data)
    {
    $scope.user = data;
     });
    	
    $scope.addComments = function(user){
    // Validate the comment is not an empty and undefined
    	console.log($scope.user)
    	
    	
    	   	
   
    // Angular AJAX call
    $http({
    method : "POST",
    url : "./createComment", 
    headers: {'Content-Type': 'application/json'},
    data :  $scope.user
   
    
    }).success(function(data){
    // Add the data into DOM for future use
    $scope.user.unshift(data); 
    });
    $scope.user = "";
    }
    };*/
            



    function userController($scope,$http,userslist) {
        
    
    	 $http.get("./getUser")
 	
 	.success(function(response) {$scope.gridOptions.data = response;});
		$scope.filterOptions = {
		        filterText: ''
		      };
	$scope.gridOptions = { 
			 enableFiltering: true,
			 enableRowSelection: true,
			 enableSelectAll: true,
	         showSelectionCheckbox: true,
	         enableCellEdit: true,
	         onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			   /*   gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });*/
			    },
// 	.success(function(response) {  
// 		//console.log("res : "+response.user_id);
// 		$scope.gridOptionsQ.data= response;
// 		});
//    		$scope.filterOptions = {
//    		        filterText: ''
//    		      };
//    	
//    	 $scope.gridOptionsQ = {
//    			 
//    			 enableFiltering: true,
//    			 enableRowSelection: true,
//    			 enableSelectAll: true,
//    	         showSelectionCheckbox: true,
//    	         enableCellEdit: true,
//    	         onRegisterApi: function(gridApi){
//    			      $scope.gridApi = gridApi;
//    			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
//    			          var msg = 'row selected ' + row.entity;
//    			          console.log(msg)
//    			          $scope.selected.push(row.entity);
//    			          console.log($scope.selected);
//    			        });
//    			    },
//    			 
//  		/*	    paginationPageSizes: [25, 50, 75],
//  			    enableFiltering: true,
//  			    paginationPageSize: 25,*/
 			    columnDefs: [
  			      { name: 'sl no',field:'userId' },
  			      { name: 'Full Name' ,field:'userName'},
  			      { name: 'Designation ',field:'userType' },
  			      { name: 'Contact No ',field:'userMobil' },
  			   
  			      { name: ' Status' , field:'userStatus' },
  			    
  			      ]
  			  }	;
    	 //alert($scope.gridOptionsQ.user_id);
   }

   
    
function attachmentController($scope,$http) {	
        
    	$http.get("./getattachments")
    	
    	.success(function(response) {$scope.attach = response;});
    	$scope.filterOptions = {
    	        filterText: ''
    	      };
        $scope.gridUse = { 
            data: 'attach',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [{field: 'attachmentId', 
            	displayName: 'AttachmentId', 
            	enableCellEdit: false
            	,},
                         {field: 'orderId', displayName: 'orderId', enableCellEdit: false}, 
                         {field:'attachmentName', displayName:'attachmentName', enableCellEdit: false},
                         {field:'attachmentDesc', displayName:'attachmentDesc', enableCellEdit: false},
                         {field:'attachmentDate', displayName:'AttachmentDate', enableCellEdit: false},
                         
                         ],
                         filterOptions: $scope.filterOptions
        };
    }


    


function damyController($scope,$http,$location) {
    
	$scope.mySelections = [];
	$http.get("./getDummy")
	
	.success(function(response) {$scope.users = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
	/*console.log("data fetched is" $scope.users);*/
    $scope.gridUser = { 
     
    		
    		data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: true,
        
     	multiSelect: true ,
        columnDefs: [{field: 'orderId',
        				displayName: 'orderId',
        				enableCellEdit: false,
        				/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
        			},
        			{field: 'trackingId',
        				displayName: 'trackingId',
        				enableCellEdit: false,
        				/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
        			},
      
                     {
        				field: 'orderTitle', 
        				displayName: 'orderTitle', 
        				enableCellEdit: false,
        				/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
        				}, 
        				 
                     {
        					field:'customerName', 
        					displayName:'customerName', 
        					enableCellEdit: false,
        					/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                     {	
                    	 field:'customerEmailId', 
                    	 displayName:'customerEmailId', 
                    	 enableCellEdit: false,
                    	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
*/                    },
                    {	
                   	 field:'orderStatus', 
                   	 displayName:'orderStatus', 
                   	 enableCellEdit: false,
                   /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                   },
                   {	
                     	 field:'paymentType', 
                     	 displayName:'paymentType', 
                     	 enableCellEdit: false,
                     /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                   
                     
                     
                     
                     {
                    	 field:'reciptDate', 
                    	 displayName:'reciptDate', 
                    	 enableCellEdit: false,
                    	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                     {field:'deliveredDate', 
                    	 displayName:'deliveredDate', 
                    	 enableCellEdit: false,
                    	/* cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                     {
                    	 field:'retailerId', 
                    	 displayName:'retailerId', 
                    	 enableCellEdit: false,
                    /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                     {
                    	 field:'OutletId', 
                    	 displayName:'OutletId', 
                    	 enableCellEdit: false,
                    	/* cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     },
                     {
                    	 field:'value', 
                    	 displayName:'value', 
                    	 enableCellEdit: false,
                    	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                     }
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };
    
    $scope.save = function() {
    		
    	console.log("hii inside save function");
    	console.log($scope.mySelections);
    	console.log("after myselection")
    	var d=$scope.mySelections; 
    	console.log("hhhhh" +$scope.d);
    	var B={};
    	B=d[0];
    	console.log(" the data i am having" +B);
    	
    	 $http({
    		 
    		 method: 'POST',
    	   
    	      url: './createorder',
    	      headers: {'Content-Type': 'application/json'},
    	      data: $scope.mySelections
    	     
    	    })
    	    .success(function (data) 
    	      {
    	    	
    	    	console.log("data inside controller is"+data);
    	    	/*$scope.status=data;
    	    	console.log("After success" +$scope.data);*/
    	    	console.log("inside success")
    	    	$location.path('product');
    	    	alert("Number of success records are :\n" +data[1] +
    	    			"\nNumber of failure records are:" +data[0]);
    	    	
    	      });
    	  };
    	
    	}

function uploadController($scope, $http)
{
	console.log("Inside uploadController");
  $scope.order = {};
 
  $scope.order = function() 
  {
	  console.log($scope.order);
    $http({
      method: 'POST',
      /*url: 'http://localhost:8080/sentinel/createUser111.do',*/
      url: './createorder',
      headers: {'Content-Type': 'application/json'},
      data:  $scope.order
    }).success(function (data) 
      {
    	$scope.status=data;
      });
  };
  
  function createOrderController($scope, $http)
  {
    $scope.dis = {};
   
    $scope.create = function() 
    {
  	  console.log($scope.user);
      $http({
        method: 'POST',
        /*url: 'http://localhost:8080/sentinel/createUser111.do',*/
        url: './createorderform',
        headers: {'Content-Type': 'application/json'},
        data:  $scope.dis
      }).success(function (data) 
        {
      	$scope.status=data;
        });
    };
  }
}



function ModalInstanceCtrl ($scope, $modalInstance,TotalCash,denominationcheck,denomination) {
	denominationcheck.setflag(1);
	console.log("new controller");
	var amountToRecieve=TotalCash.gettot();
	$scope.expense={denomination1000:'0',
			denomination500:'0',
			denomination100:'0',
			denomination50:'0',
			denomination20:'0',
			denomination10:'0',
			denomination5:'0',
			denomination2:'0',
			denomination1:'0'}

var total2=0;
	  
	  $scope.ok = function (expense) {
	           angular.forEach($scope.expense, function(value, key){
	          if(key=="denomination1000")
	        	  total2=total2+(value*1000)
	          if(key=="denomination500")
	        	  total2=total2+(value*500)
	          if(key=="denomination100")
	        	  total2=total2+(value*100)
	          if(key=="denomination50")
	        	  total2=total2+(value*50)
	          if(key=="denomination20")
	        		  total2=total2+(value*20)
	          if(key=="denomination10")
	        		  total2=total2+(value*10)
	          if(key=="denomination5")
	        		   total2=total2+(value*5)
	          if(key=="denomination2")
	        		   total2=total2+(value*2)
	          if(key=="denomination1")
	        		 total2=total2+(value*1)
	       });
		   if(total2===amountToRecieve){
			  
			   console.log("correct model total",total2);
			   console.log("amount to recieve",amountToRecieve);
			   TotalCash.settot(0);
			    denomination.save(expense);
			    $modalInstance.close($scope.selected);
		   }else{
			   alert(total2);
			   total2=0;
			   alert("Total amount and denomination total not matching...!!!");
		   }
		 
	    
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
	};	
	
	
	function createOrderController($scope,$http){
		
		  // Getting all retailers
		$http.get("./getRetailer").success(function(response) {
		$scope.retailers = response
		});
	}
	
	 /*function retailerController($scope,$http) {
	        
	    	$http.get("./getRetailer")
	    	
	    	.success(function(response) {$scope.users = response;});
	    	$scope.filterOptions = {
	    	        filterText: ''
	    	      };
	        $scope.gridUser = { 
	            data: 'users',
	            enableCellSelection: true,
	            enableRowSelection: false,
	            showSelectionCheckbox: true,
	            enableCellEdit: true,
	            columnDefs: [{field: 'retailerId', displayName: 'UserId', enableCellEdit: false},
	                         {field: 'retailerName', displayName: 'userType', enableCellEdit: false}, 
	                         {field:'retailerCode', displayName:'UserRole', enableCellEdit: false},
	                         {field:'retailerAddress', displayName:'UserLogin', enableCellEdit: false},
	                         {field:'mobile', displayName:'UserPassword', enableCellEdit: false},
	                         {field:'email', displayName:'UserMobile', enableCellEdit: false},
	                         {field:'status', displayName:'userEmail', enableCellEdit: false},
	                        
	                         ],
	                         filterOptions: $scope.filterOptions
	        };
	    }*/
	//------------- returns and stock-------------
	function returns($scope,$http,TrackindIdFactory) {
	    
		
		 $scope.gridOptions1 = {
				    paginationPageSizes: [25, 50, 75],
				    enableFiltering: true,
				    paginationPageSize: 25,
				    columnDefs: [
							      { name: 'Return Id',field:'returnId' },
							      {field: 'trackingId',displayName: 'TrackingId',width: '10%',cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.trackingId}}</a>',
				        				
				        				
				        				/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
				        			},
							      { name: 'Customer Name ',field:'consigneeName' },
							      { name: 'Customer Address ',field:'customerAddress' },
							      { name: 'PinCode ',field:'pincode' },
							      { name: 'Contact No ',field:'phone' },
							      { name: ' Order Value ' , field:'orderValue' },
							      { name: ' Order Type ' , field:'paymentMode' },
							      { name: ' Delivered Date ' , field:'deliveredDate' },
							      { name: ' Assigned To ' , field:'assignedTo' },
							      { name: ' Status ' , field:'status' },
							      { name: ' pincode ' , field:'pincode' }
							      
							      ]
				  };
		 $scope.edit=function(row)
		  	{
		  		
		  		TrackindIdFactory.setTrackingId(row.trackingId);
		  		$location.path('returnDetail');
		  	}
		
		$http.get('./getReturn')
		
		
		.success(function(response) { 
			
			$scope.gridOptions1.data = response;
			
		});

		
	  
	 
	   
	}
	
	
	//controller for return bulk assign
	 function returnsBulkAssign($scope,$http,$log,$location,$filter,$rootScope,$timeout,$modal,TrackindIdFactory) {
        
    
        //suri code
        console.log("inside return bulk assign");
         $('#mydiv').hide();
         $scope.IsHidden = true;
            $scope.ShowHide = function () {
                //If DIV is hidden it will be visible and vice versa.
                $scope.IsHidden = $scope.IsHidden ? false : true;
            }
            $('#mydiv').show(); 
            $http.get('./getRetailer')
            .success(function (data) {
            $scope.myRetailer = data;    
            console.log("got all retailer")
            });
            
            $http.get('./getReturn')
            .success(function (data) {
            $scope.myOrderReturn = data;    
            console.log("got all return")
            });
            $http.get('./getRetailerNameFromOrderReturnDb')
            .success(function (data) {
            $scope.myRetailerName = data;    
            console.log("got all retailer Name and length is "+data.length);
            });
            $http.get('./getStoreNameInOrderReturn')
            .success(function (data) {
            $scope.myStoreName = data;    
            console.log("got all stores in order return length is "+data.length);
            });
            
            $http.get('./getStores')
            .success(function (data) {
            $scope.myStore = data;
            console.log("got all stores")
            });
            
            $http.get("./getAllOrders")
            
            .success(function(response) {
                myDummyData=response;
                $scope.myAll = response;        
                
                
                console.log("got all orders")
                 $('#mydiv').hide(); 
            });

            
            $scope.resetTable=function(){
                
                console.log("inside reset dis is ");
              
              $scope.gridOptions.data =[];
          
              $scope.dis={};
               
      }
        $scope.returnDate=function(){
            $scope.CurrentDate = new Date();
            console.log($scope.CurrentDate);
            console.log("date"+$scope.CurrentDate.getDate());
            //console.log($scope.CurrentDate.getDay());
            //console.log($scope.CurrentDate.getTime());
            console.log("month"+$scope.CurrentDate.getMonth());
            console.log("minutes"+$scope.CurrentDate.getMinutes());
            console.log("hours"+$scope.CurrentDate.getHours());
            console.log("year"+$scope.CurrentDate.getFullYear());
            console.log("year"+$scope.CurrentDate.getSeconds());
            
            $scope.myDate=" date"+$scope.CurrentDate.getDate()+"-"+$scope.CurrentDate.getMonth()+"-"+$scope.CurrentDate.getFullYear()+"@ time: "+$scope.CurrentDate.getHours()+":"+$scope.CurrentDate.getMinutes()+":"+$scope.CurrentDate.getSeconds();
            console.log("mydate is "+$scope.myDate);
            
            return $scope.myDate;
        }
          //suri code end here  
        $scope.selected=[{}];
        console.log("selected array object value is "+$scope.selected);
         $scope.gridOptions = {
                /* paginationPageSizes: [25, 50, 75],
                   // paginationPageSize: 25,
                    showGridFooter: true,
                    enableFiltering: true,
                    enableColumnResizing: true,
                    enableRowSelection: true,
                    enableSelectAll: true,
                    showColumnFooter: true,

                    showSelectionCheckbox: true,
                       
                    multiSelect: true ,
                    onRegisterApi: function(gridApi){
                      $scope.gridApi = gridApi;
                      gridApi.selection.on.rowSelectionChanged($scope,function(row){
                          console.log("row");
                          if(row.isSelected){
                              console.log("row.entity is "+row.entity)
                              console.log("row.entity.trackingId: "+row.entity.trackingId)
                              $scope.selected.push(row.entity);
                              console.log("if")
                              console.log("scope.selected is "+$scope.selected);
                          }
                          else{
                              var index=$scope.selected.indexOf(row.entity)
                              $scope.selected.splice(index,1);
                              console.log("else")
                              console.log($scope.selected)
                          }
                          
                      });
                    },*/
                 pagingPageSizes: [25, 50, 75],
                    pagingPageSize: 25,
                    enableColumnResizing: true,
                    columnDefs: [
                                  { name: 'Return Id',field:'returnId' },
                                  {field: 'trackingId',displayName: 'TrackingId',width: '10%',cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.trackingId}}</a>',
                                        
                                        
                                        /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                                    },
                                  { name: 'Customer Name ',field:'consigneeName' },
                                  { name: 'Customer Address ',field:'customerAddress' },
                                  { name: 'PinCode ',field:'pincode' },
                                  { name: 'Contact No ',field:'phone' },
                                  { name: ' Order Value ' , field:'orderValue' },
                                  { name: ' Order Type ' , field:'paymentMode' },
                                  { name: ' Assigned To ' , field:'assignedTo' },
                                  { name: ' Status ' , field:'status' }
                                  
                                  ],
                    //suri code added from here
                    /*columnDefs: [
                                    {field:'trackingId',displayName:'Tracking Id',enableFiltering: true,cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.trackingId}}</a>'},
                                    { field:'returnId',displayName:'Return Id',enableFiltering: true},cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.trackingId}}</a>'},
                                    {field:'customerName',displayName:'customer Name',enableFiltering: true},
                                    {field:'pincode',displayName:'Pin Code',enableFiltering: true},
                                    {field:'status',displayName:'Order Status',enableFiltering: true},
                                    {field:'statusReason',displayName:'reason',enableFiltering: true},
                                    {field:'storeRunsheet',displayName:'runSheet Number',enableFiltering: true},        
                                    {field:'storeId',displayName:'Store Id',enableFiltering: false,visible:false},
                                    {field:'storeName',displayName:'Store Name',enableFiltering: true},
                                    {field:'location',displayName:'Current Location',enableFiltering: true},
                                    {field:'assignedTo',displayName:'Assigned To',enableFiltering: true},                        
                                    
                                    {field:'customerContact',displayName:'Customer Contact',width:'20%',enableFiltering: true,visible:false},
                                    {field:'orderTitle',displayName:'order Title',width:'20%',enableFiltering: false,visible:false},
                                    {field:'orderDetails',displayName:'order Details',width:'20%',enableFiltering: false,visible:false},
                                    {field:'orderType',displayName:'order Type',width:'20%',enableFiltering: false,visible:false},                    
                                    {field:'orderCategory',displayName:'order Category',width:'20%',enableFiltering: false,visible:false},
                                    {field:'orderPriority',displayName:'order Priority',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'orderProductId',displayName:'order ProductId',width:'20%',enableFiltering: false,visible:false},
                                    {field:'activityId',displayName:'activity Id',width:'20%',enableFiltering: false,visible:false},
                                    {field:'revisionId',displayName:'revision Id',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'attachmentId',displayName:'attachment Id',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'storeId',displayName:'store Id',width:'20%',enableFiltering: false,visible:false},
                                    {field:'retailerId',displayName:'retailer Id',width:'20%',enableFiltering: false,visible:false},
                                    {field:'orderRefno',displayName:'order Refno',width:'20%',enableFiltering: false,visible:false},            
                                    
                                    
                                    {field:'customerEmail',displayName:'customer Email',width:'20%',enableFiltering: false,visible:false},
                                    {field:'customerAddress',displayName:'customer Address',width:'70%',enableFiltering: false,visible:false},
                                    {field:'cityName',displayName:'city Name',width:'20%',enableFiltering: false,visible:false},
                                    {field:'areaName',displayName:'customer Zipcode',width:'20%',enableFiltering: false,visible:false},
                                    {field:'paymentType',displayName:'payment Type',width:'20%',enableFiltering: false,visible:false},                    
                                    {field:'bayNumber',displayName:'bay Number',width:'20%',enableFiltering: false,visible:false},
                                                            
                                    {field:'receiptDate',displayName:'receipt Date',width:'20%',enableFiltering: false,visible:false},
                                    {field:'deliveryDate',displayName:'delivery Date',width:'20%',enableFiltering: false,visible:false},
                                    {field:'deliveredDate',displayName:'delivered Date',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'deliveredBy',displayName:'delivered By',width:'20%',enableFiltering: false,visible:false},
                                    {field:'paymentStatus',displayName:'payment Status',width:'20%',enableFiltering: false,visible:false},
                                    {field:'paymentReference',displayName:'payment Reference',width:'20%',enableFiltering: false,visible:false},            
                                    {field:'orderValue',displayName:'order Value',width:'20%',enableFiltering: false,visible:false},
                                    {field:'createdBy',displayName:'created By',width:'20%',enableFiltering: false,visible:false},
                                    
                                    {field:'createdOn',displayName:'created On',width:'20%',enableFiltering: false,visible:false},
                                    {field:'updatedBy',displayName:'updated By',width:'20%',enableFiltering: false,visible:false},
                                    {field:'updatedOn',displayName:'updated On',width:'20%',enableFiltering: false,visible:false},
                                    {field:'sessionId',displayName:'Session Id',width:'20%',enableFiltering: false,visible:false},
                                    {field:'importFlag',displayName:'Import Flag',width:'20%',enableFiltering: false,visible:false},
                                    {field:'stockFlag',displayName:'Stock Flag',width:'20%',enableFiltering: false,visible:false},
                                    
                                    
                                    {field:'country',displayName:'country',width:'20%',enableFiltering: false,visible:false},
                                    {field:'vat',displayName:'vat',width:'20%',enableFiltering: false,visible:false},
                                    {field:'weight',displayName:'weight',width:'20%',enableFiltering: false,visible:false},                    
                                    {field:'content',displayName:'content',width:'20%',enableFiltering: false,visible:false},
                                    {field:'alternaitveContactNo',displayName:'alternaitve Contact No',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'acctualAmount',displayName:'acctual Amount',width:'20%',enableFiltering: false,visible:false},
                                    {field:'attempts',displayName:'attempts',width:'20%',enableFiltering: false,visible:false},
                                    {field:'runSheetStatus',displayName:'runSheet Status',width:'20%',enableFiltering: false,visible:false},                        
                                    {field:'deliveryBoyAssignDate',displayName:'deliveryBoy AssignDate',width:'20%',enableFiltering: false,visible:false},
                                    {field:'storeAssignDate',displayName:'store AssignDate',width:'20%',enableFiltering: false,visible:false},
                                    {field:'recentStatusDate',displayName:'recent StatusDate',width:'20%',enableFiltering: false,visible:false},            
                                    {field:'otp',displayName:'otp',width:'20%',enableFiltering: false,visible:false},
                                    
                                    {field:'storeRunsheet',displayName:'store Runsheet',width:'20%',enableFiltering: false,visible:false},
                                    {field:'deliveredFlag',displayName:'Delivered Flag',width:'20%',enableFiltering: false,visible:false},
                                    {field:'storeRunsheetStatus',displayName:'storeRunsheet Status',width:'20%',enableFiltering: false,visible:false},
                                    {field:'reportingStoreId',displayName:'reporting StoreId',width:'20%',enableFiltering: false,visible:false},
                                    {field:'cancelledOrderRef',displayName:'cancelled OrderRef',width:'20%',enableFiltering: false,visible:false},
                                    {field:'deliveredLocationLatLongs',displayName:'delivered LocationLatLongs',width:'20%',enableFiltering: false,visible:false},
                                 ],*/
                                  enableGridMenu: true,
                                    enableSelectAll: true,
                                    exporterCsvFilename: 'myFile.csv',
                                    exporterPdfDefaultStyle: {fontSize: 9},
                                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                                    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
                                    exporterPdfFooter: function ( currentPage, pageCount ) {
                                      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                                    },
                                    exporterPdfCustomFormatter: function ( docDefinition ) {
                                      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
                                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
                                      return docDefinition;
                                    },
                                    exporterPdfOrientation: 'portrait',
                                    exporterPdfPageSize: 'LETTER',
                                    exporterPdfMaxGridWidth: 500,
                                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                                    onRegisterApi: function(gridApi){ 
                                      $scope.gridApi = gridApi;
                                    }
                                  
                                     //suri code ends here
                  };
         $scope.edit=function(row)
              {
                  console.log("inside edit function and gonna forward to returnDetails for the tracking id "+row);
                  TrackindIdFactory.setTrackingId(row.trackingId);
                  $location.path('returnDetail');
              }
         //suri code starts here
         $scope.export = function(){
                console.log("inside export")
                $scope.export_format == 'csv';
                      
                  var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
                  $scope.gridApi.exporter.csvExport( "all", "all", myElement );
               
              };
              //suri code ends here
        
        $http.get('./getReturn')
        
        
        .success(function(response) { 
            //mainData=response;
            //$scope.gridOptions.data = response;
            
        });
          
        $scope.seacrhNormalOrder=function(dis){
              console.log("data to post is date"+dis.fromDate+"orderStatus"+dis.orderStatus+"paymentType"+dis.paymentType+"toDate"+dis.toDate);
              console.log("data to post is retailer"+dis.retailerName+"location"+dis.location+"deltodate"+dis.deliveredFromDate+"deltoDate"+dis.deliveredToDate);
              console.log("data to post is orderID"+dis.orderId+"contact"+dis.customerContact+"assigned storee"+dis.assignedStoreName);
              $('#mydiv').show(); 
             
              $http({
                  method: 'POST',
                  url: 'http://localhost:8090/taykitnew/searchReturn',
                  headers: {'Content-Type': 'application/json'},
                  //data: $scope.dis
                  data: $scope.dis
              }).success(function (data,status,headers,config) {
              console.log(data);
              $scope.status=data;
              console.log(data);
              $scope.myData = data;
              console.log("success function completed");
              $scope.gridOptions.data = data;          
              $scope.selected=[{}];
              //refresh();
              console.log("length after search is "+$scope.myData.length)
               $('#mydiv').hide();
              });
              
          }
        $http.get("./getDeliveryBoys")
        
        .success(function(response) {
            
            $scope.deliveryBoys=response;
        });

        //*********************show inactive return run sheet starts*******************//
        $scope.getInactiveRunsheetList=function(obj){
                //console.log("obj inside function"+obj.zone_id);
            var Q=obj;
                alert("obj inside function"+Q);
                
                $http.get("./inactiveRunsheets/"+Q).success(function(runsheets){
                    
                    console.log("inside inactive runshets");

                    
                    $scope.listOfInactiveRunsheets=runsheets;
                console.log($scope.listOfInactiveRunsheets)
                
                })

            };
            //*********************show inactive return run sheet ends*******************//
            
            
            $scope.ChangeStatus=function(){
                 console.log("selected items"+$scope.selected);
                 //
                   var selectedItems=$scope.gridApi.selection.getSelectedRows();
                   if((selectedItems.length)==0){
                        
                        alert("select tracking ids")
                    }
                   console.log("selected item is of length"+selectedItems.length);
                    console.log("trackingId is "+selectedItems[0].trackingId)
                    var selectedTrackingId=[];
                    for(i=0;i<selectedItems.length;i++){
                        selectedTrackingId.push(selectedItems[i].trackingId);        
                    }
                  var dataToPass={trackingIds:selectedTrackingId};
                  console.log("dataToPass is "+dataToPass);
                  //
                  console.log("after for loop the tracking is array is "+selectedTrackingId+"and length is "+selectedTrackingId.length)
                 
                 alert("runsheet number : "+$scope.outward.runSheetNo+" assigned to : "+$scope.outward.assignTo);
                
                         var reason='';
                             alert($scope.selected);
                         $http({
                             method : "POST",
                             url : "./bulkReturnAssign/"+$scope.outward.assignTo+","+$scope.outward.runSheetNo, 
                             //data :$scope.selected
                             data :dataToPass
                         
                             }).success(function(data){
                                 alert("Status updated successfully");
                         //        $scope.selected=[{}];
                                 
                             /*    $http.get("./getReturn")
                                 
                                 .success(function(response) {
                                     
                                     $scope.gridOptions.data = response;
                                     
                                 });*/
                             });
                         
                 
                 $scope.$on('statusReason', function (event, args) {
                     var reason=Reason.getReason();
                     $http({
                             method : "POST",
                             url : "./updateStatus/"+$scope.Status+","+reason,
                             data :$scope.selected
                             }).success(function(data){
                                 alert("Status updated successfully");
                                 $scope.selected=[{}];
                                 StatusReasons.clear();
                                 $http.get("./getAllOrders")
                                 
                                 .success(function(response) {
                                     
                                     $scope.gridOptions.data = response;
                                     
                                 });
                             });
                     });
             }
                
     
       
    }
	


		 //controller for stocks
		 function stocks($scope){
		 $scope.test="Stocks";
		 $scope.gridOptions = { 
		        data: 'names',
		        
		       
		        enableCellSelection: true,
		        enableRowSelection: true,
		        showSelectionCheckbox: true,
		        enableCellEdit: true,
		        
		        columnDefs: [
		                     {
		                    	field: '', 
		                    	displayName: 'OrderId',
		                    	enableCellEdit: false,
		                    	cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>' 
		                     
		                    },
		                    {field: '', displayName: 'trackingId', enableCellEdit: false, cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>'},
		                     
		                     {field:'', displayName:'CustomerName', enableCellEdit: false},
		                     {field:'', displayName:'ContactNo', enableCellEdit: false},
		                     {field:'', displayName:'Email', enableCellEdit: false},
		                     {field:'', displayName:'orderStatus', enableCellEdit: false},
		                     {field:'', displayName:'paymentType', enableCellEdit: false},
		                     {field:'', displayName:'reciptDate', enableCellEdit: false},
		                     {field:'', displayName:'DeliveredDate', enableCellEdit: false},
		                     {field:'', displayName:'retailerId', enableCellEdit: false},
		                     {field:'', displayName:'outletId', enableCellEdit: false},
		                     {field:'', displayName:'Order Type', enableCellEdit: false},
		                     {field:'', displayName:'value', enableCellEdit: false},
		                     {field:'', displayName:'assignTo', enableCellEdit: false}
		                     ],
		                     selectedItems: $scope.mySelections,
		                     filterOptions: $scope.filterOptions
		                     
		                    
		    };

		 $scope.save=function(product){
		 console.log(product)
		 $scope.product={};
		 }
		 }
		 
		 
	function roleController($scope,$http,$window,$location) {
		        
		 $scope.gridOptions1 = {
				    paginationPageSizes: [25, 50, 75],
				    enableFiltering: true,
				    paginationPageSize: 25,
				    columnDefs: [
							      { name: 'Sl no',field:'roleId' },
							      { name: 'Role Name' ,field:'roleName'},
							      { name: 'Description ',field:'roleDesc' },
							      { name: 'Status ',field:'roleStatus' },
							     
							      ]
				  }	
		
		 $http.get("./getRole")
		
		
		.success(function(response) {  $scope.gridOptions1.data = response;});
		
		        $scope.loadById = function(row) {  
		      	  // window.console && console.log(row.entity);
		      	   console.log("customerEmail is" +row.entity.customerEmail)
		      	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
		      	   var uri="mapForEachVehicle.jsp";
		      	   console.log("uri" +uri)
		      	   $window.open(uri);
		      	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
		      }; 
		    }
	function roleDetailsController($scope,$http,$window,$location) {
        
		 $scope.gridOptions1 = {
				   /* paginationPageSizes: [25, 50, 75],*/
				    enableFiltering: true,
				    paginationPageSize: 25,
				    columnDefs: [
				      { name: 'Sl no',field:'roleName' },
				      { name: 'Role Name' ,field:'roleName'},
				      { name: 'Description ',field:'roleName' },
				      { name: 'Status ',field:'roleName' },
				     
				      ]
				  }	
		
		$http.get("./getRole")
		
		
		.success(function(response) {  $scope.gridOptions1.data = response;});
		
        
        $scope.loadById = function(row) {  
      	  // window.console && console.log(row.entity);
      	   console.log("customerEmail is" +row.entity.customerEmail)
      	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
      	   var uri="mapForEachVehicle.jsp";
      	   console.log("uri" +uri)
      	   $window.open(uri);
      	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
      }; 
    }
	
function userDetailsController($scope,$http,$window,$location) {
        
    	$http.get("./getUser")
    	
    	.success(function(response) {$scope.users = response;});
    	$scope.filterOptions = {
    	        filterText: ''
    	      };
        $scope.gridUser = { 
            data: 'users',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [{field: 'user', displayName: 'Sl No', enableCellEdit: false},
                         {field: 'pudoTe', displayName: 'Module Name', enableCellEdit: false}, 
                         {field:'userle', displayName:'Permission ', enableCellEdit: false},
                         /*{field:'userLogin', displayName:'Status', enableCellEdit: false},*/
                        /* {field: 'Map Address', cellTemplate: '<button class="btn btn-default btn-flat" ng-click="loadById(row)">Map Address</button>'}*/
                         ],
                         filterOptions: $scope.filterOptions
        };
        
        $scope.loadById = function(row) {  
      	  // window.console && console.log(row.entity);
      	   console.log("customerEmail is" +row.entity.customerEmail)
      	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
      	   var uri="mapForEachVehicle.jsp";
      	   console.log("uri" +uri)
      	   $window.open(uri);
      	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
      }; 
    }
 
function cityController($scope,$http,$window,$location) {
    
	
	
	 $scope.gridOptions1 = {
			    paginationPageSizes: [25, 50, 75],
			    enableFiltering: true,
			    paginationPageSize: 25,
			    columnDefs: [
			      { name: 'sl no',field:'cityId' },
			      { name: 'City Name' ,field:'cityName'},
			      { name: 'Notes ',field:'notes' },
			     
			      ]
			  }	
	
	$http.get("./getCity")
	
	
	.success(function(response) {  $scope.gridOptions1.data = response;});
	
	
    
    $scope.loadById = function(row) {  
  	  // window.console && console.log(row.entity);
  	   console.log("customerEmail is" +row.entity.customerEmail)
  	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
  	   var uri="mapForEachVehicle.jsp";
  	   console.log("uri" +uri)
  	   $window.open(uri);
  	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
  }; 
}

function areaController($scope,$http,$window,$location) {
    
	 $scope.gridOptions1 = {
			    paginationPageSizes: [25, 50, 75],
			    enableFiltering: true,
			    paginationPageSize: 25,
			    columnDefs: [
			      { name: 'sl no',field:'slNo' },
			      { name: 'Area Name' ,field:'City Name'},
			      { name: 'City  ',field:'Notes' },
			      { name: 'Notes  ',field:'Notes' },
			      ]
			  }	
	
	$http.get("./getCustomer")
	
	
	.success(function(response) {  $scope.gridOptions1.data = response;});
    
    $scope.loadById = function(row) {  
  	  // window.console && console.log(row.entity);
  	   console.log("customerEmail is" +row.entity.customerEmail)
  	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
  	   var uri="mapForEachVehicle.jsp";
  	   console.log("uri" +uri)
  	   $window.open(uri);
  	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
  }; 
}

function categoryController($scope,$http,$window,$location) {
    
	
	
	 $scope.gridOptions1 = {
			    paginationPageSizes: [25, 50, 75],
			    enableFiltering: true,
			    paginationPageSize: 25,
			    columnDefs: [
			      { name: 'sl no',field:'customerId' },
			      { name: 'Category ' ,field:'categoryName'},
			      { name: 'Status ',field:'categroyStatus' },
			      { name: 'Description',field:'categoryDesc' },
			     
			      ]
			  }	
	
	/*--------*/
	
	$http.get("./getCategory")
	
	.success(function(response) 
		{  
		$scope.gridOptions1.data = response;
		
	});
   
    
    $scope.loadById = function(row) {  
  	  // window.console && console.log(row.entity);
  	   console.log("customerEmail is" +row.entity.customerEmail)
  	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
  	   var uri="mapForEachVehicle.jsp";
  	   console.log("uri" +uri)
  	   $window.open(uri);
  	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
  }; 
}


function importController($scope,$http,$window,$location) {
    
	$http.get("./getUser")
	
	.success(function(response) {$scope.users = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
    $scope.gridUser = { 
        data: 'users',
        enableCellSelection: true,
        enableRowSelection: false,
        showSelectionCheckbox: true,
        enableCellEdit: true,
        columnDefs: [{field: 'usId', displayName: 'Sl No', enableCellEdit: false},
                     {field: 'doType', displayName: 'Retailer', enableCellEdit: false}, 
                     {field:'urRole', displayName:'Imported On', enableCellEdit: false},
                     {field:'urLogin', displayName:'Summary', enableCellEdit: false},
                    /* {field: 'Map Address', cellTemplate: '<button class="btn btn-default btn-flat" ng-click="loadById(row)">Map Address</button>'}*/
                     ],
                     filterOptions: $scope.filterOptions
    };
    
    $scope.loadById = function(row) {  
  	  // window.console && console.log(row.entity);
  	   console.log("customerEmail is" +row.entity.customerEmail)
  	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
  	   var uri="mapForEachVehicle.jsp";
  	   console.log("uri" +uri)
  	   $window.open(uri);
  	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
  }; 
}
	
//controller for returns
/*function returns($scope,$http){
$scope.uploadFile = function(){
       var file = $scope.myFile;
       console.log('file is ' + JSON.stringify(file));
       var uploadUrl = "./uploadorder";
       fileUpload.uploadFileToUrl(file, uploadUrl);
   };
   
$http.get("./getOrder")

.success(function(response) {$scope.names = response;});
$scope.filterOptions = {
       filterText: ''
     };

$scope.gridOptions = { 
       data: 'names',
       
       enableCellSelection: true,
       enableRowSelection: false,
       showSelectionCheckbox: false,
       enableCellEdit: true,
       columnDefs: [
                    {
                   	field: 'OrderId', 
                   	displayName: 'Sl No.', 
                   	enableCellEdit: false,
                   	cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>' 
                    
                   },
                   {field: 'trackingId', displayName: 'Reference No.', enableCellEdit: false},  
                   {field:'retailerId', displayName:'Invoice No.', enableCellEdit: false},
                   {field:'retailerId', displayName:'Retailer', enableCellEdit: false},
                   {field:'customerName', displayName:'CustomerName', enableCellEdit: false},
                     
                    
                    {field:'customerContactno', displayName:'ContactNo', enableCellEdit: false},
                    
                    {field:'orderStatus', displayName:'Status', enableCellEdit: false},
                     
                    {field:'retailerId', displayName:'Assigned To', enableCellEdit: false},
                    ],
                    filterOptions: $scope.filterOptions
                    
                   
   };
}
*/





function retailerController($scope,$http,RetailerIdFactory,$location) {
    
	$http.get("./getRetailer")
	
	.success(function(response) {$scope.gridOptions.data = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
    $scope.gridOptions = { 
    		 enableFiltering: true,
			    enableRowSelection: true,
			    enableSelectAll: true,
               showSelectionCheckbox: true,
             enableCellEdit: true,
             onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });
			    },
        columnDefs: [{field: 'retailerId', displayName: 'Retailer Id', cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.retailerId}}</a>'},
                     {field: 'retailerName', displayName: 'retailer Name', enableCellEdit: false}, 
                     {field:'retailerType', displayName:'Retailer Type', enableCellEdit: false},
                     {field:'retailerCity', displayName:'Retailer City', enableCellEdit: false},
                     {field:'retailerAddress', displayName:'Retailer Address', enableCellEdit: false},
                     {field:'retailerPhone', displayName:'Phone', enableCellEdit: false},
                     {field:'retailerEmail', displayName:'Retailer Email', enableCellEdit: false},
                    
                     ],
                    
    };
    $scope.edit=function(row)
  	{
  		
    	RetailerIdFactory.setRetailerId(row.retailerId);
  	    $location.path('retailerupdate');
  	}
}

function updateRetailerController($scope,$http,RetailerIdFactory,updateretailer,retailerServices,storeCategories,con,$log,$modal,retWarehouse,$timeout,retailerServicesAdd,CategoryServicesAdd) {
	
	var id=RetailerIdFactory.getRetailerId();
	if(id==0){
		alert("no data with the selected Retailer id");
	}else{
    $http.get("./getRetailerById/"+id)
	
	.success(function(response) {
		
		$scope.Retailer = response;
		
	});
$http.get("./getRetailerContactsById/"+id)
	
	.success(function(response) {
		
		$scope.contact1=response;
		angular.forEach($scope.contact1, function(value, key){
			con.setContacts(value);
		});
		$scope.contacts=con.getContacts();
		/*$timeout(function() {
			$scope.getContacts();
		},2000);*/
	});
$http.get("./getRetailerWarehouseById/"+id)

.success(function(response) {
	
	$scope.warehouses1 = response;
	angular.forEach($scope.warehouses1, function(value, key){
		retWarehouse.setWarehouse(value);
	});
	 $scope.warehouses=retWarehouse.getWarehouse();
});
$http.get("./getRetailerAvailableServices/"+id)

.success(function(response) {
	
	$scope.services=response;
	
});
$http.get("./getRetailerAvailableCategories/"+id)

.success(function(response) {
	
	$scope.categoryServices=response;
	
});
}
	
	$scope.$on('getNewContacts', function (event, args) {
		 $scope.contacts=con.getContacts();
		 });

	$scope.$on('getNewWarehouses', function (event, args) {
		  $scope.warehouses=retWarehouse.getWarehouse();
		 });
	$scope.getContacts=function(){
		$scope.contacts=con.getContacts();
	}
	$scope.addContact=function(){
		var modalInstance = $modal.open({
					templateUrl :'partialviews/retailercontact.html',
					controller : 'AddRetailerContactCtrl',
					backdrop:'static'
				});

				modalInstance.result.then(function() {
				}, function() {
					$log.info('Modal dismissed at: ' + new Date());
				});
		   }
		 $scope.addWarehouse=function(){
			 var modalInstance = $modal.open({
					templateUrl :'partialviews/retailerwarehouse.html',
					controller  : 'AddRetailerWarehouseCtrl',
					backdrop:'static'
				});

				modalInstance.result.then(function() {
				}, function() {
					$log.info('Modal dismissed at: ' + new Date());
				});
		   }
	
    $scope.contactGrid = { 
            data: 'contacts',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [{field: 'contactName', displayName: 'Contact Name', enableCellEdit: false},
                         {field: 'ContactType', displayName: 'Contact Type', enableCellEdit: false}, 
                         {field:'contactDesignation', displayName:'Contact Designation', enableCellEdit: false},
                         {field:'contactMobile', displayName:'Contact Phone', enableCellEdit: false},
                         {field:'contactEmail', displayName:'Contact Email', enableCellEdit: false},	                        
                         ],
                         filterOptions: $scope.filterOptions
        };
        $scope.warehouseGrid = { 
                data: 'warehouses',
                enableCellSelection: true,
                enableRowSelection: false,
                showSelectionCheckbox: true,
                enableCellEdit: true,
                columnDefs: [{field: 'name', displayName: 'Warehouse Name', enableCellEdit: false},
                             {field: 'address', displayName: 'Warehouse Address', enableCellEdit: false}, 
                             {field:'city',  displayName:'Warehouse City', enableCellEdit: false},
                             {field:'area', displayName:'Warehouse Area', enableCellEdit: false},
                             {field:'contactNo', displayName:'Warehouse Phone', enableCellEdit: false},
                             {field:'notes', displayName:'Warehouse Notes', enableCellEdit: false},
                             {field:'status', displayName:'Warehouse Status', enableCellEdit: false},
                             {field:'gpsLocation', displayName:'Warehouse location', enableCellEdit: false},
                            
                             ],
                             filterOptions: $scope.filterOptions
            };
        
        $scope.save=function(){
        	alert("saving");
        	$timeout(function() {
        		retailerServicesAdd.save($scope.services,function(){
        			})
        			},500)
        	$timeout(function() {
        		CategoryServicesAdd.save($scope.categoryServices,function(){
        			})
        			},1000)
        			$timeout(function() {
        				updateretailer.save({id:id},$scope.Retailer,function(){
        					retWarehouse.clear();
        					con.clear();
        						alert("Retailer updated");
        					})
        					},1500);
        }
	
}
function transfercash($scope,$http) {
 
}

function cashTranferDetailsController($scope,$http) {
    
	$http.get("./getRetailer")
	
	.success(function(response) {$scope.users = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
    $scope.gridUser = { 
        data: 'users',
        enableCellSelection: true,
        enableRowSelection: false,
        showSelectionCheckbox: true,
        enableCellEdit: true,
        columnDefs: [{field: 'sNo', displayName: 'Sno.', enableCellEdit: false},
                     {field: 'referenceNo', displayName: 'Reference Number', enableCellEdit: false}, 
                     {field:'orderReferenceNO',displayName:'Order Reference Number', enableCellEdit: false},
                     {field:'date', displayName:'Date', enableCellEdit: false},
                     {field:'amount', displayName:'Amount', enableCellEdit: false},
                     {field:'status', displayName:'Status', enableCellEdit: false},                    
                     ],
                     filterOptions: $scope.filterOptions
    };
}


function deviceController($scope,$http,$modal,devices) {

	
	$scope.gridOptions = { 
			 enableFiltering: true,
			 enableRowSelection: true,
			 enableSelectAll: true,
	         showSelectionCheckbox: true,
	         enableCellEdit: true,
	         onRegisterApi: function(gridApi){
			  $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });
	         },
	        columnDefs:[{field: 'deviceId', displayName: 'Device Id', enableCellEdit: false},
	                     {field: 'serialNO', displayName: 'serial NO', enableCellEdit: false}, 
	                     {field:'name', displayName:'Device Name', enableCellEdit: false},
	                     {field:'type', displayName:'Device Type', enableCellEdit: false},
	                     {field:'assignedTo', displayName:'Assigned To', enableCellEdit: false},
	                     {field:'emeiNO', displayName:'Emei NO', enableCellEdit: false},
	                     {field:'simNO', displayName:'Sim NO', enableCellEdit: false},
	                     {field:'description', displayName:'Device description', enableCellEdit: false},

	                    
	                     ],
	                  
	    };
	    $scope.gridOptions.data=devices.query();
	    $scope.addDevice=function(){
	   	 var modalInstance = $modal.open({
	   			templateUrl :'partialviews/adddevice.html',
	   			controller :'addDeviceController',
	   		});
	    
	   		modalInstance.result.then(function() {
	   		}, function() {
	   			//$log.info('Modal dismissed at: ' + new Date());
	   		});
	    };

	}

function addDeviceController($scope,$http,addDevice) {
	$scope.Device={};
    $scope.save=function(){
    	addDevice.save($scope.Device,function(){
    		alert("device added");
		    $modalInstance.dismiss('cancel');

    	});
    }

function addStoreController($scope,$http,$modal) {
$scope.addContact=function(){
 var modalInstance = $modal.open({
		templateUrl :'partialviews/storecontact.html',
		controller  : 'AddStoreContactctrl',
	});

	modalInstance.result.then(function() {
	}, function() {
		$log.info('Modal dismissed at: ' + new Date());
	});
}
$scope.contactGrid = { 
        data: 'users',
        enableCellSelection: true,
        enableRowSelection: false,
        showSelectionCheckbox: true,
        enableCellEdit: true,
        columnDefs: [{field: 'contactName', displayName: 'Contact Name', enableCellEdit: false},
                     {field: 'ContactType', displayName: 'Contact Type', enableCellEdit: false}, 
                     {field:'contactDesignation', displayName:'Contact Designation', enableCellEdit: false},
                     {field:'contactMobile', displayName:'Contact Phone', enableCellEdit: false},
                     {field:'contactEmail', displayName:'Contact Email', enableCellEdit: false},	                        
                     ],
                     filterOptions: $scope.filterOptions
    };
}
function AddretailerController($scope,$http,$location,$modal) {
 $scope.addContact=function(){
var modalInstance = $modal.open({
			templateUrl :'partialviews/retailercontact.html',
			controller : 'AddRetailerContactCtrl',
		});

		modalInstance.result.then(function() {
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
   }
 $scope.addWarehouse=function(){
	 var modalInstance = $modal.open({
			templateUrl :'partialviews/retailerwarehouse.html',
			controller  : 'AddRetailerWarehouseCtrl',
		});

		modalInstance.result.then(function() {
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
   }
 
 $http.get("./getRetailer")
	
	.success(function(response) {$scope.users = response;});
	$scope.filterOptions = {
	        filterText: ''
	      };
    $scope.contactGrid = { 
        data: 'users',
        enableCellSelection: true,
        enableRowSelection: false,
        showSelectionCheckbox: true,
        enableCellEdit: true,
        columnDefs: [{field: 'contactName', displayName: 'Contact Name', enableCellEdit: false},
                     {field: 'ContactType', displayName: 'Contact Type', enableCellEdit: false}, 
                     {field:'contactDesignation', displayName:'Contact Designation', enableCellEdit: false},
                     {field:'contactMobile', displayName:'Contact Phone', enableCellEdit: false},
                     {field:'contactEmail', displayName:'Contact Email', enableCellEdit: false},	                        
                     ],
                     filterOptions: $scope.filterOptions
    };
    $scope.warehouseGrid = { 
            data: 'users',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [{field: 'warehouseName', displayName: 'Warehouse Name', enableCellEdit: false},
                         {field: 'warehouseType', displayName: 'Warehouse Type', enableCellEdit: false}, 
                         {field:'warehouseCity',  displayName:'Warehouse City', enableCellEdit: false},
                         {field:'warehouseArea', displayName:'Warehouse Area', enableCellEdit: false},
                         {field:'warehousePhone', displayName:'Warehouse Phone', enableCellEdit: false},
                         {field:'warehouseStatus', displayName:'Warehouse Status', enableCellEdit: false},
                        /* {field:'status', displayName:'userEmail', enableCellEdit: false},*/
                        
                         ],
                         filterOptions: $scope.filterOptions
        };
}

function AddRetailerContactCtrl($modalInstance,$http,$scope,contact,$log){
	$scope.addContact=function(Contact)
	{
		console.log($scope.Contact)
		contact.save($scope.Contact,function(data){
			alert("contact added");
		},function(error){
			
		});
	}

	$scope.close=function()
	{

	//$modalInstance.close();
	   $modalInstance.dismiss('cancel');
	 
	}
}
function AddRetailerWarehouseCtrl($modalInstance,$http,$scope,warehouse){
	$scope.addWarehouse=function()
	{
		/*$http.post('/addRetailerContact', {data:$scope.Contact}).
		  success(function(data, status, headers, config) {
		   
		  }).
		  error(function(data, status, headers, config) {
		  
		  });*/
		warehouse.save($scope.Warehouse,function(data){
			alert("Warehouse added");
		},function(error){
			
		});
	}

	$scope.close=function()
	{

	//$modalInstance.close();
	   $modalInstance.dismiss('cancel');
	 
	}
}
function addStoreController($scope,$http,$modal,storeContacts,StoreAdd,storeServices,$log,$timeout,storeServicesAdd,CategoryServicesAdd,storeCategories,newStoreContacts) {

	$scope.services=storeServices.query();
	$scope.categoryServices=storeCategories.query();

	$scope.$on('getStoreContacts', function (event, args) {
		$scope.storeContacts=newStoreContacts.getContacts();
		 });
	$scope.addContact=function(){
	 var modalInstance = $modal.open({
			templateUrl :'partialviews/storecontact.html',
			controller  : 'AddStoreContactctrl',
			backdrop:'static'
		});

		modalInstance.result.then(function() {
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	}
	$scope.contactGrid = { 
	        data: 'storeContacts',
	        enableCellSelection: true,
	        enableRowSelection: false,
	        showSelectionCheckbox: true,
	        enableCellEdit: true,
	        columnDefs: [{field: 'Name', displayName: 'Contact Name', enableCellEdit: false},
	                     {field:'designation', displayName:'Contact Designation', enableCellEdit: false},
	                     {field:'contactNo', displayName:'Contact Phone', enableCellEdit: false},
	                     {field:'email', displayName:'Contact Email', enableCellEdit: false},
	                     {field: 'notes', displayName: 'Contact notes', enableCellEdit: false}

	                     ],
	                     filterOptions: $scope.filterOptions
	    };
	$scope.save=function(){
		$timeout(function() {
			storeServicesAdd.save($scope.services,function(){
				})
				},500)
		$timeout(function() {
			CategoryServicesAdd.save($scope.categoryServices,function(){
				})
				},1000)
				$timeout(function() {
					StoreAdd.save($scope.Store,function(){
						newStoreContacts.clear();
							alert("Store added");
						})
						},1500);
						
	}
	}
	}

function AddretailerController($scope,$http,$location,$modal,contactsadded,warehousesAdded,addretailer,$state,retailerServices,storeCategories,CategoryServicesAdd,retailerServicesAdd,$timeout,con,retWarehouse,$log){
	$scope.$on('getNewContacts', function (event, args) {
		 $scope.contacts=con.getContacts();
		 });

	$scope.$on('getNewWarehouses', function (event, args) {
		  $scope.warehouses=retWarehouse.getWarehouse();
		 });

	$scope.services=retailerServices.query();
	$scope.categoryServices=storeCategories.query();

	 $scope.addContact=function(){
	var modalInstance = $modal.open({
				templateUrl :'partialviews/retailercontact.html',
				controller : 'AddRetailerContactCtrl',
				backdrop:'static'
			});

			modalInstance.result.then(function() {
			}, function() {
				$log.info('Modal dismissed at: ' + new Date());
			});
	   }
	 $scope.addWarehouse=function(){
		 var modalInstance = $modal.open({
				templateUrl :'partialviews/retailerwarehouse.html',
				controller  : 'AddRetailerWarehouseCtrl',
				backdrop:'static'
			});

			modalInstance.result.then(function() {
			}, function() {
				$log.info('Modal dismissed at: ' + new Date());
			});
	   }
	    $scope.contactGrid = { 
	        data: 'contacts',
	        enableCellSelection: true,
	        enableRowSelection: false,
	        showSelectionCheckbox: true,
	        enableCellEdit: true,
	        columnDefs: [{field: 'contactName', displayName: 'Contact Name', enableCellEdit: false},
	                     {field: 'notes', displayName: 'Contact Notes', enableCellEdit: false}, 
	                     {field:'contactDesignation', displayName:'Contact Designation', enableCellEdit: false},
	                     {field:'contactMobile', displayName:'Contact Phone', enableCellEdit: false},
	                     {field:'contactEmail', displayName:'Contact Email', enableCellEdit: false},	                        
	                     ],
	                     filterOptions: $scope.filterOptions
	    };
	    $scope.warehouseGrid = { 
	            data: 'warehouses',
	            enableCellSelection: true,
	            enableRowSelection: false,
	            showSelectionCheckbox: true,
	            enableCellEdit: true,
	            columnDefs: [{field: 'name', displayName: 'Warehouse Name', enableCellEdit: false},
	                         {field: 'address', displayName: 'Warehouse Address', enableCellEdit: false}, 
	                         {field:'city',  displayName:'Warehouse City', enableCellEdit: false},
	                         {field:'area', displayName:'Warehouse Area', enableCellEdit: false},
	                         {field:'contactNo', displayName:'Warehouse Phone', enableCellEdit: false},
	                         {field:'notes', displayName:'Warehouse Notes', enableCellEdit: false},
	                         {field:'status', displayName:'Warehouse Status', enableCellEdit: false},
	                         {field:'gpsLocation', displayName:'Warehouse location', enableCellEdit: false},
	                        
	                         ],
	                         filterOptions: $scope.filterOptions
	        };
	    $scope.save=function(){
	    	alert("saving");
	    	$timeout(function() {
	    		retailerServicesAdd.save($scope.services,function(){
	    			})
	    			},500)
	    	$timeout(function() {
	    		CategoryServicesAdd.save($scope.categoryServices,function(){
	    			})
	    			},1000)
	    			$timeout(function() {
	    				addretailer.save($scope.Retailer,function(){
	    					retWarehouse.clear();
	    					con.clear();
	    						alert("Retailer added");
	    					})
	    					},1500);
	    }
	}


function AddRetailerContactCtrl($modalInstance,$http,$scope,contact,$log,$location,$state,con,$rootScope){
	$scope.Contact={}
	$scope.addContact=function(Contact)
	{
		console.log($scope.Contact)
		contact.save($scope.Contact,function(data){
			con.setContacts($scope.Contact);
			$rootScope.$broadcast('getNewContacts');
		    alert("contact added");
		    $scope.Contact=[];
		    $modalInstance.dismiss('cancel');
		},function(error){
			
		});
	}
	$scope.close=function()
	{
		
		    $modalInstance.dismiss('cancel');
		  
	}
}
function AddRetailerWarehouseCtrl($modalInstance,$http,$scope,warehouse,$location,$state,retWarehouse,$rootScope){
	$scope.Warehouse={};
	$scope.addWarehouse=function()
	{
		
		warehouse.save($scope.Warehouse,function(data){
			retWarehouse.setWarehouse($scope.Warehouse);
			$rootScope.$broadcast('getNewWarehouses');
		alert("Warehouse added");
		$scope.Warehouse=[];
	    $modalInstance.dismiss('cancel');

		},function(error){
			
		});
	}
	$scope.close=function()
	{
		
		    $modalInstance.dismiss('cancel');
		  
	}
}
function addStoreController($scope,$http,$modal,storeContacts,StoreAdd,storeServices,$log,$timeout,storeServicesAdd,CategoryServicesAdd,storeCategories,newStoreContacts,$location) 
{
	
	//alert("3");
//***************8grid for masterbag starts********************//
	/* $scope.gridOptions1 = {
			    paginationPageSizes: [25, 50, 75],
			    enableFiltering: true,
			    paginationPageSize: 25,
			   
			    columnDefs: [
			      { name: 'Master Bag no',field:'masterbag_no' },
			      { name: 'Order Id' ,field:'order_id'},
			      { name: 'Return Id',field:'return_id' },
			    
			      
			      ]
			  };
	
	$http.get('./getMasterBagData')
	
	
	.success(function(response) { 
		
		$scope.gridOptions1.data = response;
		
	});
*/
	

	
	//******************show hid div on radio button selection ends
	$scope.getZoneNamesByID=function(obj){
		//console.log("obj inside function"+obj.zone_id);
		alert("obj inside function"+obj);
		//alert("user object : "+$scope.users);
	//	var Q=obj;
		$http.get("./getZoneNamesByID/"+obj)

		.success(function(response) {
			//alert("inside get zone");
			alert("response inside get zone "+response);
			//$scope.ZoneNameZM=response;
			$scope.data1=response
			//console.log("response inside data get zone "+$scope.data1.zone_name);
			//alert($scope.data1);
			  alert("finally : "+$scope.data1);
			  
			 //  alert($scope.data1.response.zone_id);
			  
			//$scope.temp = angular.fromJson($scope.data1.response);
			  
			 // alert("finally : "+$scope.temp.zone_id);
	})
	
	};
	
	$http.get("./getZoneNames")

	.success(function(response) {
	//	alert("inside get zone");
	//	console.log("inside get zone");
		$scope.listOfZone=response;//.area_name;
		//$scope.outward={listOfOptions:response.area_name};
		//console.log("list of zonenames : "+$scope.listOfZone)
	})
	
	//*********************show region list on zone selection starts*******************//
	$scope.getRegionList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
			alert("obj inside function"+obj);
			
			$http.get("./getRegionNamesByZone_Name/"+obj)

			.success(function(response) {
				//console.log(response)
				if(response==''){
					//alert("done");
				
						alert("No Region found!!Please select the other Zone");
						}
				$scope.listOfRegions=response;//.area_name;
				//$scope.outward={listOfOptions:response.area_name};
				
			})

		};
		//*********************show region list on zone selection ends*******************//
		//**************hub list***********************//

		$scope.getHubList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
			//alert("obj inside function"+obj);
			
			$http.get("./getHubNamesByRegion_Name/"+obj)

			.success(function(response) {
				//console.log(response)
				if(response==''){
				//alert("done");
			
					alert("No Hubs found!!Please select the other region");
					}
				$scope.listOfHubs=response;//.area_name;
				//$scope.outward={listOfOptions:response.area_name};
				
			})

		};
		//hub list ends
	//*************************************************************************************************
		
		//**************hub store list***********************//

		$scope.getHubStoreList=function(obj){
			//console.log("obj inside function"+obj.zone_id);
			alert("obj inside function"+obj);
			
			$http.get("./getStoreNamesByHub_Name/"+obj)

			.success(function(response) {
				//console.log(response)
				if(response==''){
				//alert("done");
			
					alert("No Stores found!!Please select the other Hub");
					}
				$scope.listOfHubStores=response;//.area_name;
				//$scope.outward={listOfOptions:response.area_name};
				
			})

		};
			$http.get("./getStores")

	.success(function(response) {
		$scope.stores = response;
		})
		
	$scope.services=storeServices.query();
	$scope.categoryServices=storeCategories.query();

	$scope.$on('getStoreContacts', function (event, args) {
		$scope.storeContacts=newStoreContacts.getContacts();
		 });
	$scope.addContact=function(){
	 var modalInstance = $modal.open({
			templateUrl :'partialviews/storecontact.html',
			controller  : 'AddStoreContactctrl',
			backdrop:'static'
		});

		modalInstance.result.then(function() {
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	}
	$scope.contactGrid = { 
	        data: 'storeContacts',
	        enableCellSelection: true,
	        enableRowSelection: false,
	        showSelectionCheckbox: true,
	        enableCellEdit: true,
	        columnDefs: [{field: 'name', displayName: 'Contact Name', enableCellEdit: false},
	                     {field:'designation', displayName:'Contact Designation', enableCellEdit: false},
	                     {field:'contactNo', displayName:'Contact Phone', enableCellEdit: false},
	                     {field:'email', displayName:'Contact Email', enableCellEdit: false},
	                     {field: 'notes', displayName: 'Contact notes', enableCellEdit: false}

	                     ],
	                     filterOptions: $scope.filterOptions
	    };
	$scope.save=function(){
		$timeout(function() {
			storeServicesAdd.save($scope.services,function(){
				})
				},500)
		$timeout(function() {
			CategoryServicesAdd.save($scope.categoryServices,function(){
				})
				},1000)
				$timeout(function() {
					StoreAdd.save($scope.Store,function(){
						newStoreContacts.clear();
							alert("Store added");
						})
						},1500);
						
	}
	
	}




//cash management


function cashManagmentController($scope,$http,$modal) {
	$http.get("./GetPaymentsPaidForToday").success(function(response)
			{$scope.gridOptions.data = response;})
	$scope.gridOptions = { 
	   		 enableFiltering: true,  
	            enableCellEdit: true,
	            onRegisterApi: function(gridApi){
				      $scope.gridApi = gridApi;
				     
				    },
				    columnDefs:[{field: 'deliveryUser', displayName: 'Delivery User'},
				                {field:'runsheetNo', displayName:'Runsheet No'},
			                    {field: 'codToCollect', displayName: 'Cash to Collect' }, 
			                    {field:'codPaid', displayName:'Cash Paid'},
			                    {field:'cardPayment', displayName:'Card Payment'},
                                {field:'shortFallAmount', displayName:'ShortFall Amount'},
			                    {field:'date', displayName:'Date'},
			                 
	                   
	                    ],
	                   
	   };

}
function PaymentCollection($scope,$http,$filter,RunsheetNoFactory,$location,DeliveryUserFactory,CodStatusFactory,CashToCollectFactory)
{
	
	var codPayment;
	$scope.Payment = {payDate: $filter("date")(Date.now(), 'yyyy-MM-dd')};
	//$scope.deliveryboys = DeliveryBoys.query();
	$http.get("./getDeliveryBoys")

	.success(function(response) {$scope.deliveryboys = response;
	
	
	
	
	})
	$scope.gridOptions = { 
   		 enableFiltering: true,
			    enableRowSelection: true,
			    enableSelectAll: true,
              showSelectionCheckbox: true,
            enableCellEdit: true,
            onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });
			    },
			    columnDefs:[{field: 'runSheetNO', displayName: 'RunSheet NO',cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.runSheetNO}}</a>'},
		                     {field: 'totalShipment', displayName: 'Total Shipment' }, 
		                     {field:'totalPrepaidOrders', displayName:'Total Prepaid Orders'},
		                     {field:'totalPrepaidDelivered', displayName:'Prepaid Delivered'},
		                     {field:'totalCod', displayName:'Total Cod Orders'},
		                     {field:'totalCodDelivered', displayName:'Cod Delivered'},
		                     {field:'codCollected', displayName:'Cash to Collected'},
		                     {field:'shortFallValue', displayName:'Short Fall Value'},
		                     {field:'shortFallReciept', displayName:'Short Fall Reciept'},
		                     {field:'codStatus', displayName:'Cod Status'},
		                     
		                    
                    ],
                   
   };
	 $scope.edit=function(row)
	  	{
	  		
	    	RunsheetNoFactory.setRunsheetNo(row.runSheetNO);
	    	$http({
	    		
	    			method:"GET",
	    			url:"./getOrderValueForRunSheet/"+row.runSheetNO
	    	
	    	}).success(function(data){
	    		
	    		console.log(data);
	    		CashToCollectFactory.setCash(data)
	    	})
	    	
	    	
	  	    $location.path('RunsheetDetails');
	  	    
	  	}
	
	$scope.deliveries=function()
	{
		
		DeliveryUserFactory.setName($scope.Payment.deliveryBoyName);
		  $http({
		      method: "GET",
		      url: "./getAllDeliveriesForDate/"+$scope.Payment.deliveryBoyName+","+$scope.Payment.payDate
		    }).success(function (data) 
		      {
		    	if(data.length==0)
		    		{
		    		alert("NOT AVILABLE");
		    		}
		    	else{
		    	if(data[0].codStatus=='Close')
		    		$scope.status=true;
		    	else
		    		$scope.status=false;
		    	console.log("cod to be collected is" +data[0].codCollected);
		    	console.log("complete data	" +data);
		    	/*CashToCollectFactory.setCash(data[0].codCollected)
*/		    	//CashToCollectFactory.setCash(codCollected)
		    	//CodStatusFactory.setStatus(data[0].codStatus);
		    	$scope.gridOptions.data=data;
		    	console.log(data)
		    	}
		      }).error(function()
		      {
			    alert("not available")
		      }
		    );
	}
};
function RunsheetDetailsForCash($scope,$http,RunsheetNoFactory,$location,DeliveryUserFactory,$modal,CodStatusFactory,ShortfallFactory) {
	var status=CodStatusFactory.getStatus();
	if(status=='Close'){
		$scope.status=false;
	}
	else{
		$scope.status=true;
	}
	//$scope.status=false;
	$scope.deliveryName=DeliveryUserFactory.getName();
	$scope.runSheetNo=RunsheetNoFactory.getRunsheetNo();
	if($scope.runSheetNo==0){
		alert("no data with the selected Retailer id");
	}else{
    $http.get("./getOrdersByRunsheet/"+$scope.runSheetNo)
	
	.success(function(response) {
		
		$scope.gridOptions.data = response;
		
	});
	}
    $scope.gridOptions = { 
    		 enableFiltering: true,
			    enableRowSelection: true,
			    enableSelectAll: true,
               showSelectionCheckbox: true,
             enableCellEdit: true,
             onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });
			    },
			    columnDefs: [{field: 'orderId',
    				displayName: 'orderId',
    				enableCellEdit: false,
    			},
    			  {field: 'trackingId',
    			   displayName: 'Tracking Id', 
    			},
  
                 {
    				field: 'orderTitle', 
    				displayName: 'orderTitle', 
    				
    				}, 
    				 
                 {
    					field:'customerName', 
    					displayName:'customerName', 
    					enableCellEdit: false,
    					/*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 },
               
                {	
               	 field:'orderStatus', 
               	 displayName:'orderStatus', 
               	 enableCellEdit: false,
               /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
               },
               {	
                 	 field:'paymentType', 
                 	 displayName:'paymentType', 
                 	 enableCellEdit: false,
                 /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 },
               
                 
                 
                 
                 {
                	 field:'reciptDate', 
                	 displayName:'reciptDate', 
                	 enableCellEdit: false,
                	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 },
                 {field:'deliveredDate', 
                	 displayName:'deliveredDate', 
                	 enableCellEdit: false,
                	/* cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 },
                 {
                	 field:'retailerId', 
                	 displayName:'retailerId', 
                	 enableCellEdit: false,
                /*	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 },
                 {
                	 field:'orderValue', 
                	 displayName:'Order Value', 
                	 enableCellEdit: false,
                	 /*cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'*/
                 }
                
                 ],
                
                

                    
    };
    
	$scope.close=function()
	{
		StatusReasons.clear();
	   $modalInstance.dismiss('cancel');
	 
	}
    $scope.closeRunsheet=function(){
    	var shortfall=ShortfallFactory.getShortfall();
    	 if(shortfall>0){
    		 
    		alert("enter shortfall details before closing runsheet")
    	}
    	 else{
  	  $location.path('cashmanagment');
    	 }
     }
    $scope.shortfallDetails=function(){
 	   var modalInstance = $modal.open({
 			templateUrl :'partialviews/CashShortfall.html',
 			controller  : 'CashShortfallController',
 		});
    }
   $scope.finalizepayment=function(){
	   var modalInstance = $modal.open({
			templateUrl :'partialviews/CashReceipt.html',
			controller  : 'CashReceiptController',
		});
   }
}
function CashReceiptController ($scope,$modal,RunsheetNoFactory,DeliveryUserFactory,$http,$modalInstance,CashToCollectFactory,ShortfallFactory) {
	$scope.Receipt={};
	$scope.Receipt.deliveryUser=DeliveryUserFactory.getName();
	$scope.Receipt.runsheetNo=RunsheetNoFactory.getRunsheetNo();
	$scope.Receipt.codToCollect=CashToCollectFactory.getCash();
	$scope.Receipt.codPaid=0;
	$scope.Receipt.cardPayment=0;
	  /* $http.get("./GetShortFallAmount/"+$scope.Receipt.runsheetNo)
		.success(function(response) {
		$scope.Receipt.shortFallAmount =response.shortFallAmount;
			
		});*/
	//$scope.Receipt.shortFallAmount=$scope.Receipt.codToCollect-$scope.Receipt.codPaid;
	$scope.change=function()
	{
		
		$scope.Receipt.shortFallAmount=$scope.Receipt.codToCollect-$scope.Receipt.codPaid-$scope.Receipt.cardPayment;

	}
	$scope.save=function(){
		ShortfallFactory.setShortfall($scope.Receipt.shortFallAmount);
		
		 $http({
		        method: 'POST',
		        url: './PaymentCollection/pay',
		        headers: {'Content-Type': 'application/json'},
		        data:  $scope.Receipt
		      }).success(function (data) 
		        {
		      	alert("receipt updated !!")
		        });
	}
	$scope.close=function()
	{
		
		    $modalInstance.dismiss('cancel');
		  
	}
}
function CashShortfallController ($scope,$modal,RunsheetNoFactory,$modalInstance,DeliveryUserFactory,$http,ShortfallFactory) {
	$scope.Cash={};
	$scope.Cash.deliveryUser=DeliveryUserFactory.getName();
	$scope.Cash.runsheetNo=RunsheetNoFactory.getRunsheetNo();
	$scope.Cash.shortFallAmount=ShortfallFactory.getShortfall();
	$scope.save=function(){
		ShortfallFactory.setShortfall(0);
		 $http({
		        method: 'POST',
		        url: './ShortFallAmountDetails',
		        headers: {'Content-Type': 'application/json'},
		        data:  $scope.Cash
		      }).success(function (data) 
		        {
		      	alert("ShortFall Updated !!")
		        });
	}
	$scope.close=function()
	{
		
		    $modalInstance.dismiss('cancel');
		  
	}
	
}




function StoreController($scope,$http,stores,$location,StoreIdFactory) {
	$http.get("./getStores")
		
		.success(function(response) {$scope.gridOptions.data = response;});
		$scope.filterOptions = {
		        filterText: ''
		      };
	$scope.gridOptions = { 
			 enableFiltering: true,
			 enableRowSelection: true,
			 enableSelectAll: true,
	         showSelectionCheckbox: true,
	         enableCellEdit: true,
	         onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });
			    },
			    columnDefs: [{field: 'storeId', displayName: 'Store Number',  cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.storeId}}</a>'},
			                 {field: 'name', displayName: 'Store Name', enableCellEdit: false}, 
			                 {field:'pincode', displayName:'Store Pincode', enableCellEdit: false},
			                 {field:'address', displayName:'Store Address', enableCellEdit: false},
			                 {field:'type', displayName:'Store Type', enableCellEdit: false},
			                 {field:'city', displayName:'Store City', enableCellEdit: false},
			                 {field:'landMark', displayName:'Store landmark', enableCellEdit: false},
			                 {field:'edmAvailbility', displayName:'EDM', enableCellEdit: false},
			                 {field:'reportingStore', displayName:'Repoting Store', enableCellEdit: false},
			                 {field:'surveyToken', displayName:'Survey Token', enableCellEdit: false},
			                
			                 ],
	                
	};

	$scope.edit=function(row)
		{
			
		StoreIdFactory.setStoreId(row.storeId);
		    $location.path('storeupdate');
		}
	}

//Region Management controller
function RegionManagementController($scope,$http,regions,$location,RegionIdFactory) {
	
	$http.get("./getRegions")
		
		.success(function(response) {$scope.gridOptions.data = response;});
		$scope.filterOptions = {
		        filterText: ''
		      };
	$scope.gridOptions = { 
			 enableFiltering: true,
			 enableRowSelection: true,
			 enableSelectAll: true,
	       //  showSelectionCheckbox: true,
	         enableCellEdit: true,
	         onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			   /*   gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });*/
			    },
			    columnDefs: [
			                 
			                 {field: 'region_id', displayName: 'Region ID',  cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.region_id}}</a>'},
			                 {field: 'region_name', displayName: 'Region Name', enableCellEdit: false}, 
			                 {field:'region_desc', displayName:'Region Description', enableCellEdit: false},
			                 {field:'zone_name', displayName:'Zone Name', enableCellEdit: false},
			                 ],
	                
	};

	$scope.edit=function(row)
		{
			
		RegionIdFactory.setRegion_id(row.region_id);
		    $location.path('regionupdate');
		}
	}




//Hub Management controller
function HubManagementController($scope,$http,hubs,$location,HubIdFactory) {
	
	$http.get("./getHubs")
		
		.success(function(response) {$scope.gridOptions.data = response;});
		$scope.filterOptions = {
		        filterText: ''
		      };
	$scope.gridOptions = { 
			 enableFiltering: true,
			 enableRowSelection: true,
			 enableSelectAll: true,
	       //  showSelectionCheckbox: true,
	         enableCellEdit: true,
	         onRegisterApi: function(gridApi){
			      $scope.gridApi = gridApi;
			     /* gridApi.selection.on.rowSelectionChanged($scope,function(row){
			          var msg = 'row selected ' + row.entity;
			          console.log(msg)
			          $scope.selected.push(row.entity);
			          console.log($scope.selected);
			        });*/
			    },
			    columnDefs: [
			                 
			                 {field: 'hub_id', displayName: 'Hub ID',  cellTemplate:'<a ng-href="" ng-click="grid.appScope.edit(row.entity)">{{row.entity.hub_id}}</a>'},
			                 {field: 'hub_name', displayName: 'Hub Name', enableCellEdit: false}, 
			                 {field:'hub_desc', displayName:'Hub Description', enableCellEdit: false},
			                 {field:'region_name', displayName:'Region Name', enableCellEdit: false},
			                 ],
	                
	};

	$scope.edit=function(row)
		{
			
		HubIdFactory.setHub_id(row.hub_id);
		    $location.path('hubupdate');
		}
	}


//****delete hub

//delete hub ends







	function updateStoreController($scope,$http,$modal,storeContacts,StoreAdd,storeServices,$log,$timeout,storeServicesAdd,CategoryServicesAdd,storeCategories,newStoreContacts,StoreIdFactory,updatestore) {
		
		var id=StoreIdFactory.getStoreId();
		if(id==0){
			alert("no data with the selected Retailer id");
		}else{
			
	    $http.get("./getStoreById/"+id)
		
		.success(function(response) {
			
			$scope.Store = response;
			
		});
	    $http.get("./getStoreAvailableServices/"+id)

	    .success(function(response) {
	    	
	    	$scope.services=response;
	    	
	    });
	    
	    $http.get("./getStoreAvailableCategories/"+id)

	    .success(function(response) {
	    	
	    	$scope.categoryServices=response;
	    	
	    });
	    $http.get("./getStoreContactsById//"+id)

	    .success(function(response) {
	    	
	    	$scope.contact1=response;
			angular.forEach($scope.contact1, function(value, key){
				newStoreContacts.setContacts(value);
			});
			
			$scope.storeContacts=newStoreContacts.getContacts();
	    	
	    });
		}
		

		$scope.$on('getStoreContacts', function (event, args) {
			$scope.storeContacts=newStoreContacts.getContacts();
			 });
		$scope.addContact=function(){
		 var modalInstance = $modal.open({
				templateUrl :'partialviews/storecontact.html',
				controller  : 'AddStoreContactctrl',
				backdrop:'static'
			});

			modalInstance.result.then(function() {
			}, function() {
				$log.info('Modal dismissed at: ' + new Date());
			});
		}
		$scope.contactGrid = { 
		        data: 'storeContacts',
		        enableCellSelection: true,
		        enableRowSelection: false,
		        showSelectionCheckbox: true,
		        enableCellEdit: true,
		        columnDefs: [{field: 'Name', displayName: 'Contact Name', enableCellEdit: false},
		                     {field:'designation', displayName:'Contact Designation', enableCellEdit: false},
		                     {field:'contactNo', displayName:'Contact Phone', enableCellEdit: false},
		                     {field:'email', displayName:'Contact Email', enableCellEdit: false},
		                     {field: 'notes', displayName: 'Contact notes', enableCellEdit: false}

		                     ],
		                     filterOptions: $scope.filterOptions
		    };
		$scope.save=function(){
			$timeout(function() {
				storeServicesAdd.save($scope.services,function(){
					})
					},500)
			$timeout(function() {
				CategoryServicesAdd.save($scope.categoryServices,function(){
					})
					},1000)
					$timeout(function() {
						updatestore.save({id:id},$scope.Store,function(){
							newStoreContacts.clear();
								alert("Store Updated");
							})
							},1500);
							
		}
		}
	
	function AddStoreContactctrl($modalInstance,$http,$scope,storecontact,newStoreContacts,$rootScope){
		$scope.Contact={};
		$scope.save=function()
		{ 
			storecontact.save($scope.Contact,function(data){
				newStoreContacts.setContacts($scope.Contact)
				$rootScope.$broadcast('getStoreContacts');
				alert("contact added");
				$scope.Contact=[];
			    $modalInstance.dismiss('cancel');

			},function(error){
				
			});
		}
		$scope.close=function()
		{
			
			    $modalInstance.dismiss('cancel');
			 
		}
	}

	
	function ReturnUpdateController($scope,$http,TrackindIdFactory,$location){
		

		$http.get("./getDeliveryBoys")

		.success(function(response) {

		$scope.deliveryBoys=response;
		});
		var id=TrackindIdFactory.getTrackingId();
		if(id==0){
			alert("no data with the selected tracking id");
		}else{
	    $http.get("./getReturnDataById/"+id)
		
		.success(function(response) {
			
			$scope.dis = response;
			
		});
		}
		
	}
	
	
	/*===============controller for zone,region,hub++++++++++++++++++*/
	function UserManagement($scope,$location,$http) {
	      // alert();
		
		/*=================creating zone===============*/
	    	var flag;
	        $scope.CreatZone=function(){
	        	alert($scope.zone_name);
	    			$http({
	    				method:"POST",
	    				url:"./CreatZone/"+$scope.zone_name
	    				//data:$scope.obj
	    			})
	    			.success(function(response){
	    				flag=response;
	    				console.log(flag)
	    				if(flag){
	    					 
	    					//$scope.details=response;
	    					alert("Zone Added Successfully!!!");
	    					// $location.path('product');
	    			
	    				}
	    				else{
	    					alert("Error Occurred..!!")
	    				}
	    				
	    			})
	    			
	    		}
	        /*=====================creating zone ends==================*/
	        
	        /*=====================creating hub starts==================*/
	        
	        $scope.CreatHub=function(){
	        	//alert($scope.selectedItem.zone_id);
	  //      	alert($scope.selectedItem2.region_id);
	        	//alert($scope.region_desc);
	    			$http({
	    				method:"POST",
	    				url:"./CreatHub/"+$scope.selectedItem2.region_id+","+$scope.hub_name+","+$scope.hub_desc
	    				//data:$scope.obj
	    			})
	    			.success(function(response){
	    				flag=response;
	    				console.log(flag)
	    				if(flag){
	    					 
	    					//$scope.details=response;
	    					alert("Hub Added Successfully!!!");
	    					 $location.path('product');
	    					/*$scope.$apply(function() {
	    						  $location.path('product');
	    						});*/
	    				}
	    				else{
	    					alert("Error Occurred..!!")
	    				}
	    				//$location.path('product');
	    			})
	    			//$location.path('product');
	    		}
	        
	        /*=====================creating hub ends==================*/
	        
  /*=====================creating region starts==================*/
	        
	        $scope.CreatRegion=function(){
	        	//alert($scope.selectedItem.zone_id);
	        	//alert($scope.region_name);
	        	//alert($scope.region_desc);
	    			$http({
	    				method:"POST",
	    				url:"./CreatRegion/"+$scope.selectedItem.zone_id+","+$scope.region_name+","+$scope.region_desc
	    				//data:$scope.obj
	    			})
	    			.success(function(response){
	    				flag=response;
	    				console.log(flag)
	    				if(flag){
	    					 
	    					//$scope.details=response;
	    					alert("Region Added Successfully!!!");
	    					 $location.path('product');
	    					/*$scope.$apply(function() {
	    						  $location.path('product');
	    						});*/
	    				}
	    				else{
	    					alert("Error Occurred..!!")
	    				}
	    				//$location.path('product');
	    			})
	    			//$location.path('product');
	    		}
	        
	        /*=====================creating region ends==================*/

	        
	      
	        
	        
	        console.log("before call")
        	$http.get("./getZoneNames")
        	//console.log("after call")
        .success(function(response) {
        	console.log(response)
        	$scope.listOfOptions=response;//.area_name;
        	//$scope.outward={listOfOptions:response.area_name};
        	
        })
	        
	        
        $scope.GetRegionId=function(obj){
	      //  	alert("inside getregion");
				var obj1=obj.zone_id;
				//alert(obj1)
				$http.get("./getRegionNames/"+obj1)
	        	//console.log("after call")
	        .success(function(response) {
	        	console.log(response)
	        	$scope.listOfRegionNames=response;//.area_name;
	        	//$scope.outward={listOfOptions:response.area_name};
	        	
	        	console.log($scope.listOfRegionNames)
	        	//alert($scope.listOfRegionNames)
				//$scope.outward={AccNum:x};

			})
	        }
	        
	        
	    //================controller for zone to show table data===============
	       // function zoneController($scope,$http,$window,$location) {
	            
	        	
	        	
	       	 $scope.gridOptions1 = {
	       			    paginationPageSizes: [25, 50, 75],
	       			    enableFiltering: true,
	       			    paginationPageSize: 25,
	       			    columnDefs: [
	       			      { name: 'Zone ID',field:'zone_id' },
	       			      { name: 'Zone Name' ,field:'zone_name'},
	       			      { name: 'Zone Description ',field:'zone_desc' },
	       			  /* { name: 'Updated On',field:'zone_desc' },*/
	       			     
	       			      ]
	       			  }	
	       	
	       	$http.get("./getZoneNames")
	       	
	       	
	       	.success(function(response) {  $scope.gridOptions1.data = response;});
	       	
	       	
	           
	           $scope.loadById = function(row) {  
	         	  // window.console && console.log(row.entity);
	         	   console.log("customerEmail is" +row.entity.customerEmail)
	         	  /* var uri="mapForEachVehicle.jsp?customerEmail="+row.entity.customerEmail;*/
	         	   var uri="mapForEachVehicle.jsp";
	         	   console.log("uri" +uri)
	         	   $window.open(uri);
	         	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
	         }; 
	         
	         
	         
	       
	}