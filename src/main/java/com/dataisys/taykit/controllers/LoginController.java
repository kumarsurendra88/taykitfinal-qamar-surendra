package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.HubDAO;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Module;
import com.dataisys.taykit.model.RequiredBeans;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;

@RestController
public class LoginController {

	@Autowired
	private UserDao userDao;
	@Autowired
	PudoStoreDao storeDao;
	@Autowired
	HubDAO hubdao;
	
	@RequestMapping(value="/authenticate" , method=RequestMethod.POST)
	public ModelAndView validateUser(@ModelAttribute("User") User loginform, HttpSession session)throws IOException
	{	
		System.out.println("Username:"+loginform.getUserLogin());
		System.out.println("Password:"+loginform.getUserPassword());
		System.out.println("Account No" +loginform.getAccNumber());
		//System.out.println("PudoType:"+loginform.getPudoType());
		
		String username = loginform.getUserLogin();
		String password = loginform.getUserPassword();
		String accountNumber=loginform.getAccNumber();
		//double pudoType=loginform.getPudoType();
		
		boolean isAuthenticated = false;
		User user = null;
		
		try {			
				user = userDao.getUserByCredentials(username, password,accountNumber);
				if(user != null)
				{
				isAuthenticated = true;				
				session.setAttribute("user", user);
				//double pudoType1= user.getPudoType();
				//System.out.println("session inside login controler"+pudoType1);
				
			//	session.setAttribute("pudoType", pudoType1);
				long active = session.getLastAccessedTime();
				Timestamp activeTime= new Timestamp(active);
				session.setAttribute("activeLastd", activeTime);
				
			}			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	
		if(isAuthenticated) {
			session.setAttribute("isLoggedIn","true");	
			//session.setAttribute("userNamed", username);
			//double pudoType2=user.getPudoType();
			//System.out.println("hgjghggjg"+pudoType2);
			
			System.out.println("Inside if");
			//session.setAttribute("pudoTyped", pudoType2);
			session.setAttribute("user", user);
			session.setAttribute("loggedStore", user.getAccNumber());
			
			if(user.getUserType().equalsIgnoreCase("STORE_ADMIN")){
				double strId=storeDao.getStoreIdByAccountNo(user.getAccNumber());
				session.setAttribute("storeId", strId);
			}
			//System.out.println("logged in store Id is" +(Double)strId);
			
			/*Store obj=storeDao.getStoreByAccountNo(user.getAccNumber());
			session.setAttribute("storeDetails", obj);
			*/
			
			return new ModelAndView ("testlandingpage.html");
			
			/*if(user.getUserType().equalsIgnoreCase("Store_Admin")){
				
				return new ModelAndView ("storeadminpage.html");
				
			}
			else if(user.getUserType().equalsIgnoreCase("Store executive")){
				return new ModelAndView ("storexecutivepage.html");
			}
			else if(user.getUserType().equalsIgnoreCase("Master store admin")){
				return new ModelAndView ("masterstoreadminpage.html");
			}
			else if(user.getUserType().equalsIgnoreCase("Hub executive")){
				return new ModelAndView ("Hubexecutivepage.html");
			}
			else if(user.getUserType().equalsIgnoreCase("Hub admin")){
				return new ModelAndView ("Hubadminpage.html");
			}
			else if(user.getUserType().equalsIgnoreCase("Partner store team")){
				return new ModelAndView ("partnerstore.html");
			}
			else if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")){
				return new ModelAndView ("testlandingpage.html");
			}
			else
			
			return new ModelAndView ("NewFile1.html");
			*/
			
			
			/*if(UserType.equals("admin"))
			{
				System.out.println("Inside if");
				session.setAttribute("userTyped", UserType);
				session.setAttribute("user", user);
				return new ModelAndView ("landingpage.html");
				//return null;
			}
			else
			{
				System.out.println("iNSIDE else");
				session.setAttribute("userTyped", UserType);
				session.setAttribute("user", user);
				return new ModelAndView ("userlandingpage.html");
				//return null;
			}*/
		} else {
			ModelAndView loginModel = new ModelAndView("NewFile1.html");
			loginModel.addObject("msg", "Invalid credentials");
			return loginModel;
		}		
	}
	
	
	@RequestMapping(value="/getLoggedStore",method = RequestMethod.GET)
	public Object getLoggedStore(HttpSession session){
	System.out.println("account number"+session.getAttribute("loggedStore"));
	User u=(User)session.getAttribute("user");
	System.out.println("usertype : "+u.getUserType());
	String accountNo=(String) session.getAttribute("loggedStore");
	String userType=u.getUserType();
	
	if(userType.equalsIgnoreCase("SUPER_ADMIN")){
		System.out.println("inside superadmin");
		Store obj1=storeDao.getStoreByAccountNo(accountNo);
		
		return obj1;
	}
	else{
		System.out.println("inside hub or store");
		RequiredBeans obj=hubdao.getHubByAccountNo(accountNo,u.getUserType());
		System.out.println("after dao impl in cintroller");
		System.out.println(obj.getName());
		return obj;
	}
	
	//session.setAttribute("storeDetails", obj);
	
	
	//return obj;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public ModelAndView logoutUser(HttpSession session) throws IOException 
	{			
		session.invalidate();		
		return new ModelAndView("NewFile1.html");
	}
	
	@RequestMapping(value="/getJson",method=RequestMethod.GET)
	public String getJsonData(HttpSession session){
		
		User user=(User)session.getAttribute("user");
		user.getModuleName();
	Module mod=	userDao.getModuleDetails(user.getModuleName());
	
		//System.out.println("module code is" +mod.getModuleCode());
		return mod.getModuleCode();
	}
	



}
