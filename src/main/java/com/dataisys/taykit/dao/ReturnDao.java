package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.CancelledOrderByData;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.SearchReturn;

public interface ReturnDao {

	public void makeReturnEntry(Return returnvalues);
	public void makeReturnEntry1(Return returnvalues);
	
	public List<Return> returnvalues(HttpSession session);
	
	public Return retrurnOrder(String trackingId);
	public void assignToDeliveryBoy(AssignDeliveryBoy obj);
	List<Return> getRunSheetDeliveries(Return order);
	public void getRunsheetNumber(String userNames, String randomNumbers);
	public com.dataisys.taykit.model.Return getOrders(String trackingtId);
	public void updateOrderStatus(com.dataisys.taykit.model.Return object);
	public void updateAssignedToReturns(List<Return> list, String assignedTo);
	public void updateAssignedToReturns2(List<Return> list, String assignedTo,String runSheetNumber);	
	public void updateAssign(com.dataisys.taykit.model.Return object,
			double assignId,HttpSession session);
	
	public List<Return> ordersForStoreRunSheet(Return order);
	public void getRunSheetNumberForStore(double storeId,
			String runsheetNumber,String orderType);
	List<Return> getRunSheetDeliveries2(String DeliveryBoyName,String runSheetNumber);
	
	//Added By Kishore
		public List<Return> getAllReturnsForPickupByStoreId(String storeId);
		public List<Return> returnvaluesByStoreId(String storeId);
		public void assignReturns(List<com.dataisys.taykit.model.Return> list, String assignId, String userType);
		public void updateOrderReturnDelivery(String trackingId, String orderStatus);
		
		//Added By Surendra
		public List<Return> getPickupReturnsByDeliveryBoyId(String deliveryBoyId);
		
		public List<Return> getAllPendingReturns(HttpSession session, CancelledOrderByData cancelledOrderByData); //Added on 05-12-2015
		public void updateReferenceNumberForReturn(CancelledOrderByData cancelledOrderByData);
		
		//added by kishor and surandar
		public void updateReturnStatus(List<Return> orders ,String status, String reason);
		public void updateOrderReturn(Return object);
		public List<Return> getAllReturnDispatchedOrdersWithRefNo(
				HttpSession session, CancelledOrderByData cancelledOrderByData);
		public List<Return> getOrdersBasedOnSearch1(SearchReturn searchReturn);

}
