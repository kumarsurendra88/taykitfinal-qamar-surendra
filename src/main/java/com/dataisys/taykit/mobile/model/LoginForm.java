package com.dataisys.taykit.mobile.model;

public class LoginForm {
	
	private String username;
	private String password;
	private String userType;
	private String pudoType;
	private String lastActive;
	
	
	public String getLastActive() {
		return lastActive;
	}
	public void setLastActive(String lastActive) {
		this.lastActive = lastActive;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPudoType() {
		return pudoType;
	}
	public void setPudoType(String pudoType) {
		this.pudoType = pudoType;
	}
	
	

}
