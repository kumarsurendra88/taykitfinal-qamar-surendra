package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.CityDao;
import com.dataisys.taykit.model.City;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.rowmapper.CityRowMapper;
import com.dataisys.taykit.rowmapper.CustomerRowMapper;

public class CityDaoImpl extends JdbcDaoSupport implements CityDao{

	public void createCity(City city) {
		// TODO Auto-generated method stub
		
		String sql="insert into City(city_name,city_status,notes)" + "values(?,?,?)";
		getJdbcTemplate().update(sql,city.getCityName(),city.getStatus(),city.getNotes());
	}

	public List<City> getCity() {
		// TODO Auto-generated method stub
		
			String sql="select *from city";
			List<City> listofCity=getJdbcTemplate().query(sql,new CityRowMapper());
			return listofCity;
	
	}

}
