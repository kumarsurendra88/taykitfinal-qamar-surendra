package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.UserStoreDAO;
import com.dataisys.taykit.mobile.model.UserStore;
import com.dataisys.taykit.rowmapper.UserStoreMapper;

public class UserStoreDAOImpl extends JdbcDaoSupport implements UserStoreDAO {

	public void createUserStore() {
		// TODO Auto-generated method stub

	}
	public List<UserStore> getUsersByStoreId(double storeId) {
		String sql= "SELECT * FROM user_store WHERE store_id = ?";
		List<UserStore> UserStores = getJdbcTemplate().query(sql,new Object[]{storeId},new UserStoreMapper());
		return UserStores;
	}

	public List<UserStore> getStoreByUserId(double userId) {
		String sql= "SELECT * FROM user_store WHERE user_id = ?";
		List<UserStore> UserStores = getJdbcTemplate().query(sql,new Object[]{userId},new UserStoreMapper());
		return UserStores;
	}
}
