package com.dataisys.taykit.model;

public class PudoStore {

	private double storeId;
	private String storeName;
	private String storeAddress;
	private String storeContactno;
	private String storeType;
	private String storeCity;
	private String storeZip;
	private String storedesc;
	private  String orderType;
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	@Override
	public String toString() {
		return "PudoStore [storeId=" + storeId + ", storeName=" + storeName
				+ ", storeAddress=" + storeAddress + ", storeContactno="
				+ storeContactno + ", storeType=" + storeType + ", storeCity="
				+ storeCity + ", storeZip=" + storeZip + ", storedesc="
				+ storedesc + "]";
	}
	
	
	
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}
	public String getStoreContactno() {
		return storeContactno;
	}
	public void setStoreContactno(String storeContactno) {
		this.storeContactno = storeContactno;
	}
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getStoreCity() {
		return storeCity;
	}
	public void setStoreCity(String storeCity) {
		this.storeCity = storeCity;
	}
	public String getStoreZip() {
		return storeZip;
	}
	public void setStoreZip(String storeZip) {
		this.storeZip = storeZip;
	}
	public String getStoredesc() {
		return storedesc;
	}
	public void setStoredesc(String storedesc) {
		this.storedesc = storedesc;
	}
	

}
