package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Device;
import com.dataisys.taykit.model.OrderAttachment;
import com.dataisys.taykit.model.Retailer;

public class DeviceRowMapper implements RowMapper<Device>{

	
	
	public Device mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 Device device= new Device();
	 device.setAssignedTo(rs.getDouble("assigned_to"));
	 device.setDescription(rs.getString("device_desc"));
	 device.setDeviceId(rs.getDouble("device_id"));
	 device.setEmeiNO(rs.getString("device_number"));
	 device.setName(rs.getString("device_name"));
	 device.setSerialNO(rs.getString("serial_no"));
	 device.setSimNo(rs.getString("sim_no"));
	 device.setType(rs.getString("device_type"));
	 	return device;
	 	
 }
	 	
 
 
 }
}

