package com.dataisys.taykit.model;

import java.util.ArrayList;


public class InwardEntry {

	private ArrayList<String> trackingIds;
	private String note;		
	
	
	public ArrayList<String> getTrackingIds() {
		return trackingIds;
	}
	public void setTrackingIds(ArrayList<String> trackingIds) {
		this.trackingIds = trackingIds;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}	
}
