package com.dataisys.taykit.model;

import java.util.Date;
import java.util.List;

public class CancelledOrderByData {
	
	private Date fromDate;
	private Date toDate;
	private String retailerName;
	private String status;	
	private List<String> trackingIds;

	public List<String> getTrackingIds() {
		return trackingIds;
	}
	public void setTrackingIds(List<String> trackingIds) {
		this.trackingIds = trackingIds;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
