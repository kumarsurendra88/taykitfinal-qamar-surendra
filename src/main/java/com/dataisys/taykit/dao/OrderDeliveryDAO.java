package com.dataisys.taykit.dao;

import com.dataisys.taykit.mobile.model.OrderDelivery;

public interface OrderDeliveryDAO {	
	public void createOrderDelivery(OrderDelivery orderDelivery);
}
