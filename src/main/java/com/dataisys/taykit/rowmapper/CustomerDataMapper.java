package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.CustomerData;

public class CustomerDataMapper implements RowMapper<CustomerData> {

	public CustomerData mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		// TODO Auto-generated method stub
		CustomerData obj=new CustomerData();
		obj.setOrderType(rs.getString("order_type"));
		obj.setComments(rs.getString("comments"));
		obj.setContact(rs.getString("contact"));
		obj.setStoreName(rs.getString("store_name"));
		obj.setUpdatedOn(rs.getTimestamp("updated_on"));
		return obj;
	}

}
