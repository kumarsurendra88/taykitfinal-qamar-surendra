package com.dataisys.taykit.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.ProductCategoriesDao;
import com.dataisys.taykit.dao.StoreContactDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.factories.ProductCategoryFactory;
import com.dataisys.taykit.factories.RetailerContactFactory;
import com.dataisys.taykit.factories.RetailerServicesFactory;
import com.dataisys.taykit.factories.StoreContactFactory;
import com.dataisys.taykit.factories.StoreServicesFactory;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.Device;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreCategory;
import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StoreService;
import com.dataisys.taykit.model.StoreServices;


@RestController
public class PudoStoreController {
	
	@Autowired
	PudoStoreDao storeDao;
	@Autowired
	StoreServicesDao storeServiceDao;
	@Autowired
	ProductCategoriesDao categoryServiceDao;
	 @Autowired
		StoreContactDao storeContactDao;
	@RequestMapping(value="/getStores" , method=RequestMethod.GET)	
	public List<Store> getStoreList(HttpSession session){
		List<Store> stores=storeDao.getStores(session);
		return stores;
	}  
	@RequestMapping(value="/addStore" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addRetailer(@RequestBody Store store,HttpSession session){
		 System.out.println(store);	
		 StoreServicesFactory serviceFactory=StoreServicesFactory.getFactory();
		 List<StoreServices> storeservices=serviceFactory.getServices();
		 ProductCategoryFactory categoryFactory=ProductCategoryFactory.getFactory();
		 List<ProductCategories> categoryServices=categoryFactory.getServices();
		 StoreContactFactory factory= StoreContactFactory.getFactory();
		 List<StoreContact> storeContacts=factory.getContacts();
		 storeDao.AddStore(store, storeservices, categoryServices,storeContacts,session);
		 
	}
	@RequestMapping(value="/UpdateStoreById/{id}" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addRetailer(@RequestBody Store store,@PathVariable ("id") int id){
		 System.out.println(store);	
		 StoreServicesFactory serviceFactory=StoreServicesFactory.getFactory();
		 List<StoreServices> storeservices=serviceFactory.getServices();
		 ProductCategoryFactory categoryFactory=ProductCategoryFactory.getFactory();
		 List<ProductCategories> categoryServices=categoryFactory.getServices();
		 StoreContactFactory factory= StoreContactFactory.getFactory();
		 List<StoreContact> storeContacts=factory.getContacts();
		storeDao.updateStore(store, storeservices, categoryServices,storeContacts,id);
		 
	}
	
	@RequestMapping(value="/getStoreById/{id}" , method=RequestMethod.GET)	
	public Store getStorebyId(@PathVariable("id") double id){
		return storeDao.getStoreById(id);
	}
	@RequestMapping(value="/StoreServicesAdd" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addStoreServices(@RequestBody List<StoreServices> storeServices){
		 System.out.println(storeServices);
		 StoreServicesFactory factory=StoreServicesFactory.getFactory();
		 factory.setServices(storeServices);
	}
	@RequestMapping(value="/storeCategoryServicesAdd" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addStoreCategoryServices(@RequestBody List<ProductCategories> categoryServices){
		 System.out.println(categoryServices);
		 ProductCategoryFactory factory=ProductCategoryFactory.getFactory();
		 factory.setServices(categoryServices);
	}
	@RequestMapping(value="/addstoreContact" , method=RequestMethod.POST)	
	public void addStoreContact(@RequestBody StoreContact contact){
         System.out.println("contact"+contact);
		 StoreContactFactory object=StoreContactFactory.getFactory();
		 object.setContacts(contact);
		
		 
	}
	@RequestMapping(value="/getStoreContact", method=RequestMethod.GET)	
	public List<StoreContact> getRetailerContact(){
    StoreContactFactory object=StoreContactFactory.getFactory();
    List<StoreContact> contacts=object.getContacts();
    return contacts;
	}
	
	@RequestMapping(value="/getStoreServices" , method=RequestMethod.GET)	
	public List<StoreServices> getStoreServices(){
		List<StoreServices> services=storeServiceDao.getServices();
		
		return services;
		
	}
	@RequestMapping(value="/getStorecategories" , method=RequestMethod.GET)	
	public List<ProductCategories> getStorecategories(){
		List<ProductCategories> services=categoryServiceDao.getproductCategories();
		
		return services;
		
	}
	
	@RequestMapping(value="/getStoreAvailableServices/{id}" , method=RequestMethod.GET)	
	public List<StoreServices> getStoreavailableServices(@PathVariable("id") double id){
		List<StoreServices> totalservices=storeServiceDao.getServices();
		List<StoreService> availableservices=storeServiceDao.getStoreServicesById(id);
		Iterator<StoreService> it=availableservices.iterator();
		while(it.hasNext())
		{  
			StoreService obj=it.next();
			Double serviceId=obj.getStoreServiceTypeId();
			Iterator<StoreServices> it2=totalservices.iterator();
			while(it2.hasNext())
			{
				
				
				StoreServices obj2=it2.next();
				if(obj2.getServiceId()==serviceId)
				{
					
					obj2.setServiceStatus(true);
				}
			}
			
		}
		return totalservices;
		
	}
	@RequestMapping(value="/getStoreAvailableCategories/{id}" , method=RequestMethod.GET)	
	public List<ProductCategories> getStoreavailableCategoriesServiced(@PathVariable("id") double id){
		List<ProductCategories> totalCatogoriesServiced=categoryServiceDao.getproductCategories();
		List<StoreCategory> availablecategoriesserviced=categoryServiceDao.getAvailablecategoriesForStore(id);
		Iterator<StoreCategory> it=availablecategoriesserviced.iterator();
		while(it.hasNext())
		{  
			StoreCategory obj=it.next();
			Double serviceId=obj.getProductCategoryId();
			Iterator<ProductCategories> it2=totalCatogoriesServiced.iterator();
			while(it2.hasNext())
			{
				
				
				ProductCategories obj2=it2.next();
				if(obj2.getCategoryId()==serviceId)
				{
					
					obj2.setCategoryStatus(true);
				}
			}
			
		}
		
		return totalCatogoriesServiced;
		
	}
	@RequestMapping(value="/getStoreContactsById/{id}", method=RequestMethod.GET)	
	public List<StoreContact> getStoreContact(@PathVariable("id") double id){
		return storeContactDao.getStoreContactsBId(id);
			
	}
}
