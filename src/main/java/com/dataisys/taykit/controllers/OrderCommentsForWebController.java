package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderActivity;



@RestController
public class OrderCommentsForWebController {

	
	@Autowired
	OrderDao orderDao;
	@RequestMapping(value="/getComment/{trackId},{idType}", method=RequestMethod.GET)
	public @ResponseBody List<OrderActivity> getCommentById(@PathVariable("trackId") String trackId,@PathVariable("idType") String idType,HttpServletResponse response) throws IOException{
		
		response.setHeader("Access-Control-Allow-Origin", "*");
	    response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	    response.setHeader("Access-Control-Max-Age", "3600");
	    response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
	    List<OrderActivity> comment= orderDao.getCommentsId(trackId,idType);
	    Iterator<OrderActivity> itr=comment.iterator();
	    List<OrderActivity> sts= new ArrayList<OrderActivity>();
	    
	    while(itr.hasNext()){
	    	OrderActivity obj=itr.next();
		 if(obj.getActivityDate()==null){
		System.out.println("checking for udated on" +obj.getUpdatedOn());
		String commentDate1=obj.getUpdatedOn().toString();
		 obj.setCommentDate1(commentDate1);
		 System.out.println("checking " +obj.getActivityDate());
		 										
		 										
		 
		 }
		 else{
			 String commentDate1=obj.getUpdatedOn().toString();
			 obj.setCommentDate1(commentDate1);
		 }
		 sts.add(obj); 
		 }
		
	    Iterator<OrderActivity> sts1=comment.iterator();
		 while(sts1.hasNext())
			 System.out.println(sts1.next());
		 return sts;
		
	}
	@RequestMapping(value="/getOrderDetails/{trackId},{idType}", method=RequestMethod.GET)
	public @ResponseBody List<Order> getOrderDetailsById(@PathVariable("trackId") String trackId,@PathVariable("idType") String idType,HttpServletResponse response) throws IOException{
		
		response.setHeader("Access-Control-Allow-Origin", "*");
	    response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	    response.setHeader("Access-Control-Max-Age", "3600");
	    response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
	    List<Order> comment= orderDao.getOrderDetailsById(trackId,idType);
	    Iterator<Order> itr=comment.iterator();
	    List<Order> sts= new ArrayList<Order>();
	    
	    while(itr.hasNext()){
	    	Order obj=itr.next();
		
		 sts.add(obj); 
		 }
		
	    Iterator<Order> sts1=comment.iterator();
		 while(sts1.hasNext())
			 System.out.println(sts1.next());
		 return sts;
		
	}
}
