package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreServices;

public class RetailerAvailableCategoriesRowMapper implements RowMapper<RetailerCategory>{
	
	
	public RetailerCategory mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		RetailerCategory service= new RetailerCategory();
		service.setCategoryName(rs.getString("category_name"));
		service.setProductCategoryId(rs.getDouble("product_category_id"));
		service.setRetailerCategoryId(rs.getDouble("retailer_category_id"));
		service.setRetailerId(rs.getDouble("retailer_id"));
		service.setUpdatedBy(rs.getString("updated_by"));
		return service;
		
	}



}
