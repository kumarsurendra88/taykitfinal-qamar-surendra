package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Order;

public class OrderMapper implements RowMapper<Order>{

	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		Order obj= new Order();
		obj.setTrackingId(rs.getString("tracking_id"));
		obj.setOrderId(rs.getString("order_id"));
		obj.setOrderTitle(rs.getString("order_title"));
		obj.setOrderDetails(rs.getString("order_details"));
		obj.setOrderType(rs.getString( "order_type"));
		obj.setOrderCategory(rs.getString( "order_category"));
		obj.setOrderPriority(rs.getString( "order_priority"));
		obj.setOrderProductId(rs.getDouble("order_product_id"));
		obj.setActivityId(rs.getDouble("activity_id"));
		obj.setRevisionId(rs.getDouble("revision_id"));
		obj.setAttachmentId(rs.getDouble("attachment_id"));
		obj.setStoreId(rs.getDouble("store_id"));
		obj.setRetailerId(rs.getDouble("retailer_id"));
		obj.setOrderRefno(rs.getString("order_refno"));
		obj.setCustomerName(rs.getString("customer_name"));
		obj.setCustomerContact(rs.getString("customer_mobile"));
		obj.setCustomerEmail(rs.getString("customer_email"));
		obj.setCustomerAddress(rs.getString("customer_address"));
		obj.setCityName(rs.getString("city_name"));
		obj.setCustomerZipcode(rs.getInt("customer_zipcode"));
		obj.setPaymentType(rs.getString("payment_type"));
		obj.setBayNumber(rs.getInt("bay_number"));
		obj.setOrderStatus(rs.getString("order_status"));
		obj.setReceiptDate(rs.getDate("receipt_date"));
		obj.setDeliveryDate(rs.getDate("delivery_date"));
		obj.setAssignedTo(rs.getString("assigned_to"));
		obj.setDeliveredDate(rs.getDate("delivered_date"));
		obj.setDeliveredBy(rs.getString("delivered_by"));
		obj.setPaymentStatus(rs.getString("payment_status"));
		obj.setPaymentReference(rs.getString("payment_reference"));
		obj.setOrderValue(rs.getDouble("order_value"));
		obj.setCreatedBy(rs.getString("created_by"));
		obj.setCreatedOn(rs.getDate("created_on"));
		obj.setUpdatedBy(rs.getString("updated_by"));
		obj.setUpdatedOn(rs.getTimestamp("updated_on"));
		obj.setAttempts(rs.getInt("attempts"));
		obj.setDeliveryBoyAssignDate(rs.getDate("delivery_boy_assign_date"));
		obj.setStoreAssignDate(rs.getDate("store_assign_date"));
		obj.setRecentStatusDate(rs.getDate("recent_status_date"));
		obj.setOtp(rs.getString("otp"));
		obj.setReason(rs.getString("status_reason"));
		obj.setLocation(rs.getString("location"));
		obj.setRunSheetNumber(rs.getString("runsheet_number"));
		obj.setRunSheetStatus(rs.getString("runsheet_status"));
		obj.setStoreRunsheet(rs.getString("store_runsheet"));
		obj.setStoreRunsheetStatus(rs.getString("store_runsheet_status"));
		obj.setReportingStoreId(rs.getDouble("reporting_store_id"));	
		obj.setCancelledOrderRef(rs.getString("cancel_order_ref_no"));
		
		return obj;
	}

}
