package com.dataisys.taykit.model;

import java.sql.Date;
import java.sql.Timestamp;

public class OrderDelivery {

	private double deliveryId;
	private double orderId;
	private double userId;
	private Date date;
	private String status;
	private String location;
	private String notes;
	private String updatedBy;
	private Timestamp updatedOn;
	public OrderDelivery() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderDelivery(double deliveryId, double orderId, double userId,
			Date date, String status, String location, String notes,
			String updatedBy, Timestamp updatedOn) {
		super();
		this.deliveryId = deliveryId;
		this.orderId = orderId;
		this.userId = userId;
		this.date = date;
		this.status = status;
		this.location = location;
		this.notes = notes;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	public double getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(double deliveryId) {
		this.deliveryId = deliveryId;
	}
	public double getOrderId() {
		return orderId;
	}
	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}
	public double getUserId() {
		return userId;
	}
	public void setUserId(double userId) {
		this.userId = userId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Override
	public String toString() {
		return "OrderDelivery [deliveryId=" + deliveryId + ", orderId="
				+ orderId + ", userId=" + userId + ", date=" + date
				+ ", status=" + status + ", location=" + location + ", notes="
				+ notes + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + "]";
	}

	
	
}
