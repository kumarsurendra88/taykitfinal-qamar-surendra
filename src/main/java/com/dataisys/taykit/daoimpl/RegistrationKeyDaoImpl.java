package com.dataisys.taykit.daoimpl;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.model.RegistrationKey;
import com.dataisys.taykit.rowmapper.RegistrationKeyRowMapper;

public class RegistrationKeyDaoImpl extends JdbcDaoSupport implements RegistrationKeyDao {	

	public RegistrationKey getRegkeyByAccountid(String accountNo) {		
		String sql="SELECT * from `user_key` where account_no=?";		
		RegistrationKey regKey = null;
		try {
			regKey = getJdbcTemplate().queryForObject(sql,new Object[]{accountNo},new RegistrationKeyRowMapper());	
			return regKey;
		} catch(Exception e) {
			return regKey;
		}
	}
	
	public void createNewRegKey(RegistrationKey regkey) {
		// TODO Auto-generated method stub
		System.out.println("Inside CreateNewRegKey in DAOImpl");
		String sql = "INSERT INTO user_key(reg_key, account_no) VALUES (?,?)";
		getJdbcTemplate().update(sql,regkey.getRegKey(),regkey.getAccountNo());
	}
	
	public void updateRegKeyByAccountNo(RegistrationKey regkey) {
		// TODO Auto-generated method stub
		System.out.println("Inside updateRegKeyByAccountNo in DAOImpl");
		String sql = "UPDATE user_key SET reg_key = ? WHERE account_no = ?";
		getJdbcTemplate().update(sql,regkey.getRegKey(),regkey.getAccountNo());
	}
	
	public RegistrationKey getAccountNoByRegKey(String registrationKey) {
		String sql="SELECT * from `user_key` where reg_key=?";		
		RegistrationKey regKey = null;
		try {
			regKey = getJdbcTemplate().queryForObject(sql,new Object[]{registrationKey},new RegistrationKeyRowMapper());	
			return regKey;
		} catch(Exception e) {
			return regKey;
		}
	}
}
