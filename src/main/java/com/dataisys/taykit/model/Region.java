package com.dataisys.taykit.model;

public class Region {

	int region_id,zone_id;
	String region_name,region_desc,zone_name;
	
	
	public String getZone_name() {
		return zone_name;
	}
	public void setZone_name(String zone_name) {
		this.zone_name = zone_name;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public int getZone_id() {
		return zone_id;
	}
	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getRegion_desc() {
		return region_desc;
	}
	public void setRegion_desc(String region_desc) {
		this.region_desc = region_desc;
	}
	public Region(){}
	public Region(int region_id,int zone_id,String region_name,String region_desc) {
		super();
		this.region_id=region_id;
		this.zone_id=zone_id;
		this.region_name=region_name;
		this.region_desc=region_desc;
		
	}
	
	
}
