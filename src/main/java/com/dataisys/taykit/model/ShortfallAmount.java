package com.dataisys.taykit.model;

public class ShortfallAmount {

	
	private String runsheetNo;
	private String deliveryUser;
	private String trackingId;
	private double shortFallAmount;
	private String description;
	private String receipt;
	public String getRunsheetNo() {
		return runsheetNo;
	}
	public void setRunsheetNo(String runsheetNo) {
		this.runsheetNo = runsheetNo;
	}
	public String getDeliveryUser() {
		return deliveryUser;
	}
	public ShortfallAmount(String runsheetNo, String deliveryUser,
			String trackingId, double shortFallAmount, String description) {
		super();
		this.runsheetNo = runsheetNo;
		this.deliveryUser = deliveryUser;
		this.trackingId = trackingId;
		this.shortFallAmount = shortFallAmount;
		this.description = description;
	}
	@Override
	public String toString() {
		return "ShortfallAmount [runsheetNo=" + runsheetNo + ", deliveryUser="
				+ deliveryUser + ", trackingId=" + trackingId
				+ ", shortFallAmount=" + shortFallAmount + ", description="
				+ description + "]";
	}
	public ShortfallAmount() {
		super();
	}
	public void setDeliveryUser(String deliveryUser) {
		this.deliveryUser = deliveryUser;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public double getShortFallAmount() {
		return shortFallAmount;
	}
	public void setShortFallAmount(double d) {
		this.shortFallAmount = d;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
}
