package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Hub {

	private int region_id;
	private double hub_id;
	private String hub_name,hub_desc;
	private Timestamp updated_on;
	private String region_name;
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public Timestamp getUpdated_on() {
		return updated_on;
	}
	public void setUpdated_on(Timestamp updated_on) {
		this.updated_on = updated_on;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public double getHub_id() {
		return hub_id;
	}
	public void setHub_id(double hub_id) {
		this.hub_id = hub_id;
	}
	public String getHub_name() {
		return hub_name;
	}
	public void setHub_name(String hub_name) {
		this.hub_name = hub_name;
	}
	public String getHub_desc() {
		return hub_desc;
	}
	public void setHub_desc(String hub_desc) {
		this.hub_desc = hub_desc;
	}

	public Hub(){}
	
	public Hub(double hub_id, int region_id, String hub_name, String hub_desc,Timestamp updated_on) {
		super();
		this.hub_id=hub_id;
		this.region_id=region_id;
		this.hub_name=hub_name;
		this.hub_desc=hub_desc;
		this.updated_on = updated_on;
	}
}
