package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Return;

public class ReturnAdvanceSearchRowMapper implements RowMapper<Return>{

	public Return mapRow(ResultSet rs, int rowNum) throws SQLException {
		Return obj= new Return();
		obj.setReturnId(rs.getString("return_id"));
		obj.setTrackingId(rs.getString("tracking_id"));
		obj.setCustomerName(rs.getString("customer_name"));
		obj.setPincode(rs.getInt("pincode"));
		obj.setContactNo(rs.getString("customer_phone"));
		obj.setStatus(rs.getString("order_status"));
		
		obj.setOrderType(rs.getString( "order_type"));
		
		obj.setRetailerId(rs.getDouble("retailer_id"));
		obj.setCustomerAddress(rs.getString("address"));
	
		obj.setAssignedTo(rs.getString("assigned_to"));
		obj.setDeliveredDate(rs.getDate("delivered_date"));
		
		obj.setOrderValue(rs.getDouble("order_value"));
		obj.setStoreName(rs.getString("store_name"));
		obj.setLocation(rs.getString("location"));
	
		
		
		
		
		return obj;
	}

}
