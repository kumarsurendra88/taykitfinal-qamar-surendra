package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.City;
import com.dataisys.taykit.model.Role;

public class RolesRowMapper implements RowMapper<Role> {

	public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		// TODO Auto-generated met{
		
		Role role=new Role();
		role.setRoleName(rs.getString("role_name"));
		role.setRoleDesc(rs.getString("role_desc"));
		role.setRoleStatus(rs.getString("role_status"));
	
	
	
		return role;
	
	
	}
}
