

routerApp.factory('sheetNumber',function(){
	var runsheetNum=null;
	return{
		setNumber:function(number){
			runsheetNum=number
		},
		getNumber:function(){
			return runsheetNum;
		}
	}
});

function deliveryStatusUpdate($scope,$http,$location,sheetNumber){
	
	$http.get("./getDeliveryBoys")
	
	.success(function(response) {
		
		$scope.deliveryBoys=response;
	});
	
	$http.get("./getLoggedStore")
	.success(function(response){
		$scope.name=response.name;
		$scope.obj={storeId:response.storeId};
	})
	
	/*$scope.getRunSheet=function(){
		console.log($scope.obj);
		
		$http({
		 		 		method : "POST",
				 		url : "./getRunsheets", 
				 		data :$scope.obj
				 		})
		.success(function(response){
			 
			if(response.length>0){
			$scope.details=response;
			}else{
				alert("No Open Runsheet found..!! Try with other date or delivery boy..!!")
			}
		})
	}*/
	$scope.getRunSheet=function(){
		
		console.log("data which i m sending to"+$scope.obj);
		 console.log($scope.boys);
		 if($scope.boys==true){
		 $http({
		 method : "POST",
		 url : "./getRunsheetsForDeliveryBoys", 
		 data :$scope.obj
		 })
		 .success(function(response){

		 if(response.length>0){
		 $scope.details=response;
		 }else{
		 alert("No Open Runsheet found..!! Try with other date or delivery boy..!!")
		 }
		 })
		 }else{
		 $http({
		 method : "POST",
		 url : "./getRunsheets", 
		 data :$scope.obj
		 })
		 .success(function(response){

		 if(response.length>0){
		 $scope.details=response;
		 }else{
		 alert("No Open Runsheet found..!! Try with other date or delivery boy..!!")
		 }
		 })
		 }
		
		
	}
	
	$scope.loadByRunSheet=function(runsheetNumber,status){
		//alert(status);
		sheetNumber.setNumber(runsheetNumber);
		if(status=="OPEN"){
			$location.path('deliveryStatusUpdate2');
		}else{
			$location.path('deliveryStatusUpdateView');
		}
		 
	}
}

function deliveryStatusUpdate2($scope,$http,$location,sheetNumber){
	//$scope.number=sheetNumber.getNumber();
	$scope.statuses=["Delivered","At Taykit warehouse","In transit","Out For Delivery","Future Delivery","Could not attempt","Undeliverable","Order cancelled","Scheduled delivery"]
	var futureDeliveryReasons=["Door Locked","No Response","Customer Not Reachable"];
	var orderCancelledReasons=["Late","Not Required","Parcel Tampered"];
	var cudntAttemptReasons=["Vehicle breakdown","Bad Weather"];
	var undlrvbleReasons=["Wrong number","Customer not at address"];
	
	
	
	$http.get("./getSelectedRunsheetDetails/"+sheetNumber.getNumber())
	.success(function(response){
		console.log(response)
		$scope.orders=response;
		angular.forEach($scope.orders,function(value,key){
			console.log("Value "+value.trackingId+"  "+value.orderStatus)
			console.log(value.orderStatus.toUpperCase())
			if(value.orderStatus.toUpperCase()=="DELIVERED"||value.orderStatus.toUpperCase()=="DELIVERED1"){
				$scope.orders[key].deliveredFlag=true;
				console.log("deliverd")
			}else{
				$scope.orders[key].deliveredFlag=false;
				console.log("not deleivered")
			}
			console.log("Key "+key)
		})
	})
	
	$scope.loadReason=function(index,status){
		
		if(status=="Future Delivery"){
			$scope.reasons=futureDeliveryReasons;
		}else if(status=="Undeliverable"){
			$scope.reasons=undlrvbleReasons;
		}else if(status=="Order cancelled"){
			$scope.reasons=orderCancelledReasons;
		}else if(status=="Could not attempt"){
			$scope.reasons=cudntAttemptReasons;
		}else{
			$scope.reasons="";
		}
	}
	
	$scope.save=function(){
		console.log($scope.orders);

		$http({
		 		 		method : "POST",
				 		url : "./updateOrdersByRunsheet", 
				 		data :$scope.orders
				 		})
		.success(function(response){
			alert("Saved successfully..!!")
		})
	}
	
	$scope.closeRunSheet=function(){
		console.log($scope.orders);
		$http({
			method : "POST",
			url : "./closeRunsheet",
			data : sheetNumber.getNumber()
		})
		.success(function(response){
			alert("Runsheet was successfully closed..!!")
			 $location.path('deliveryStatusUpdate');
		})
	}
	
	$scope.cancel=function(){
		 $location.path('deliveryStatusUpdate');
		
	}
	
}