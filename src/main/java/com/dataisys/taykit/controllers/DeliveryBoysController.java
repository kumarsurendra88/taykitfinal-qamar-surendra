package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;




import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.model.DeliveryBoys;


@RestController
public class DeliveryBoysController {

	
	@Autowired
	DeliveryBoyDao deliveryBoyDao;
	@RequestMapping(value="/getDeliveryBoys", method=RequestMethod.GET)

	public List <DeliveryBoys> getAllDeliveryBoys(HttpSession session) throws IOException{
	List <DeliveryBoys> deliveryBoys=deliveryBoyDao.getAllByStore(session);
	  return deliveryBoys;

	}
	
	
	
	@RequestMapping(value="/deliveryboyid/get", method=RequestMethod.POST)

	public DeliveryBoys  DeliveryBoyId(@RequestBody String name) throws IOException{
		DeliveryBoys id=deliveryBoyDao.DeliveryBoyId(name);
		
	  return id;

	}

}
