package com.dataisys.taykit.daoimpl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.DataImportDao;
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.model.DataImport;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.rowmapper.DataImportMapper;

public class DataImportImpl extends JdbcDaoSupport implements DataImportDao{

	 @Autowired
	 OrderDao orderDao;
	public void dataImport(DataImport data) {
		
		if(data.getOrderType()==null){
			
			data.setOrderType("delivery");
		}
		
	    data.setDataStatus("Under_Process");
	    System.out.println();
	    	
	 
		String sql="insert into data_import (session_id,import_type,retailer_id,data_status,order_category,store_id,order_type,data_note,"
				+ "updated_by,updated_on,tracking_id,order_id,order_title,order_details,order_priority,order_product_id,activity_id,revision_id,attachment_id,"
				+ "order_refno,customer_name,customer_email,customer_mobile,customer_address,city_name,customer_zipcode,payment_type,bay_number,order_status,"
				+ "receipt_date,delivery_date,assigned_to,delivered_by,payment_status,payment_reference,order_value,created_by,created_on) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			getJdbcTemplate().update(sql,data.getSessionId(),data.getImportType(),data.getRetailerId(),data.getDataStatus(),data.getOrderCategory(),data.getStoreId(),data.getOrderType(),data.getDataNote()
					,data.getUpdatedBy(),data.getUpdatedOn(),data.getTrackingId(),data.getOrderId(),data.getOrderTitle(),data.getOrderDetails(),data.getOrderPriority(),data.getOrderProductId(),data.getActivityId(),data.getRevisionId(),data.getAttachmentId()
					,data.getOrderRefno(),data.getCustomerName(),data.getCustomerEmail(),data.getCustomerContact(),data.getCustomerAddress(),data.getCityName(),data.getCustomerZipcode(),data.getPaymentType(),data.getBayNumber(),data.getOrderStatus()
					,data.getReceiptDate(),data.getDeliveryDate(),data.getAssignedTo(),data.getDeliveredBy(),data.getPaymentStatus(),data.getPaymentReference(),data.getOrderValue(),data.getCreatedBy(),data.getCreatedOn());
		}catch(Exception e){
			System.out.println(e);
			System.err.println(e.getMessage());
		}
	}

	public List<DataImport> getAll(HttpSession session) {
		String sql="select * from data_import where session_id=?";
		List<DataImport> list=getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
		 
		return getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
	}

	public List<DataImport> getUnderProcess(HttpSession session) {
		String sql="select * from data_import where session_id=? and data_status='Under_Process'";
		List<DataImport> list=getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
		//System.out.println("Under Process"+list.size()+list);
		return getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
	}

	public List<DataImport> getInvalid(HttpSession session) {
		String sql="select * from data_import where session_id=? and data_status='Invalid'";
		List<DataImport> list=getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
		//System.out.println("Invalid"+list.size()+list);
		return getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
	
	}

	public void assignStore(List<DataImport> list, HttpSession session,int id) {
		System.out.println("Got List"+list);
		String sql="update data_import set store_id=? where session_id=? and order_id=? and data_status='Under_Process'";
		Iterator<DataImport> itr=list.iterator();
		while(itr.hasNext()){
			DataImport obj=itr.next();
			getJdbcTemplate().update(sql,id, session.getId(), obj.getOrderId());
		}
		
	}

	public List<DataImport> getUnassignedStore(HttpSession session) {
		String sql="select * from data_import where session_id=? and store_id<=0 and data_status='Under_Process'";
		List<DataImport> list=getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
		System.out.println("Unasssigned"+list.size()+list);
		return getJdbcTemplate().query(sql, new Object[]{session.getId()}, new DataImportMapper());
	
	}

	public void moveOrders(HttpSession session) {
		List<DataImport> list=getUnderProcess(session);
		Iterator<DataImport> itr=list.iterator();
		while(itr.hasNext()){
			DataImport ob1=itr.next();
			Order ob2= new Order();
			ob2.setTrackingId(ob1.getTrackingId());
			ob2.setOrderId(ob1.getOrderId());
			ob2.setOrderTitle(ob1.getOrderTitle());
			ob2.setOrderDetails(ob1.getOrderDetails());
			ob2.setOrderPriority(ob1.getOrderPriority());
			ob2.setOrderProductId(ob1.getOrderProductId());
			ob2.setActivityId(ob1.getActivityId());
			ob2.setRevisionId(ob1.getRevisionId());
			ob2.setAttachmentId(ob1.getAttachmentId());
			ob2.setOrderRefno(ob1.getOrderRefno());
			ob2.setCustomerName(ob1.getCustomerName());
			ob2.setCustomerContact(ob1.getCustomerContact());
			ob2.setCustomerAddress(ob1.getCustomerAddress());
			ob2.setCustomerEmail(ob1.getCustomerEmail());
			ob2.setCityName(ob1.getCityName());
			//ob2.setCustomerZipcode(ob1.getCustomerZipcode());
			ob2.setCustomerZipcode(ob1.getCustomerZipcode());
			ob2.setPaymentType(ob1.getPaymentType());
			ob2.setBayNumber(ob1.getBayNumber());
			ob2.setOrderStatus("data recieved at central ware house");
			ob2.setReceiptDate(ob1.getReceiptDate());
			ob2.setDeliveryDate(ob1.getDeliveryDate());
			ob2.setAssignedTo(ob1.getAssignedTo());
			ob2.setDeliveredBy(ob1.getDeliveredBy());
			ob2.setPaymentStatus(ob1.getPaymentStatus());
			ob2.setPaymentReference(ob1.getPaymentReference());
			ob2.setOrderValue(ob1.getOrderValue());
			ob2.setCreatedBy(ob1.getCreatedBy());
			ob2.setCreatedOn(ob1.getCreatedOn());
			ob2.setRetailerId(ob1.getRetailerId());
			ob2.setStoreId(ob1.getStoreId());
			ob2.setOrderType(ob1.getOrderType());
			ob2.setOrderCategory(ob1.getOrderCategory());
			try{
				orderDao.createOrder(ob2);
				markMoved(ob1.getDataId());
				
				
			}catch(Exception e){
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
			
		}
		
	}

	public void markMoved(double d) {
		System.out.println("Updating moved");
		String sql="update data_import set data_status=\"Moved\" where data_id=?";
		//double dataId=
		getJdbcTemplate().update(sql,d);
	}
	
	public void assignStoreByTrackingId(List<String> list, HttpSession session,
			int id) {
			String sql="update data_import set store_id=? where session_id=? and tracking_id=? and data_status='Under_Process'";
			Iterator<String> itr=list.iterator();
			while(itr.hasNext()){
			String	trackingId=itr.next();
			getJdbcTemplate().update(sql,id, session.getId(),trackingId);
			}

			}

}
