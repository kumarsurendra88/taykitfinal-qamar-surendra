package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.dataisys.taykit.model.RunsheetDetails;
import com.dataisys.taykit.model.ShortfallAmount;

public class ShortfallRowmapper implements RowMapper<ShortfallAmount>{
	
	
	public ShortfallAmount mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 ShortfallAmount shortfall= new ShortfallAmount();
	 shortfall.setDeliveryUser(rs.getString("delivery_user"));
	 shortfall.setDescription(rs.getString("description"));
	 shortfall.setRunsheetNo(rs.getString("runsheet_no"));
	 shortfall.setShortFallAmount(rs.getDouble("shortfall_amount"));
	 return shortfall;
	 	
 }
	 	
 
 
 }
}
