package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class StoreCategory {

private double storeId;
private double storeCategoryId;
private double productCategoryId;
private String categoryName;
private String updatedBy;
private Timestamp updatedOn;
public double getStoreId() {
	return storeId;
}
public void setStoreId(double storeId) {
	this.storeId = storeId;
}
public double getStoreCategoryId() {
	return storeCategoryId;
}
public void setStoreCategoryId(double storeCategoryId) {
	this.storeCategoryId = storeCategoryId;
}
public double getProductCategoryId() {
	return productCategoryId;
}
public void setProductCategoryId(double productCategoryId) {
	this.productCategoryId = productCategoryId;
}
public String getCategoryName() {
	return categoryName;
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public String getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}
@Override
public String toString() {
	return "StoreCategory [storeId=" + storeId + ", storeCategoryId="
			+ storeCategoryId + ", productCategoryId=" + productCategoryId
			+ ", categoryName=" + categoryName + ", updatedBy=" + updatedBy
			+ ", updatedOn=" + updatedOn + "]";
}
public StoreCategory(double storeId, double storeCategoryId,
		double productCategoryId, String categoryName, String updatedBy,
		Timestamp updatedOn) {
	super();
	this.storeId = storeId;
	this.storeCategoryId = storeCategoryId;
	this.productCategoryId = productCategoryId;
	this.categoryName = categoryName;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
}
public StoreCategory() {
	super();
}


}

