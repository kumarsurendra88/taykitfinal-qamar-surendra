package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.StoreCategory;
import com.dataisys.taykit.model.StoreServices;

public interface ProductCategoriesDao {
public List<ProductCategories> getproductCategories();
public void addRetailerCategories(List<RetailerCategory> categories);
public void addStoreCategories(List<StoreCategory> categories);
public List<RetailerCategory> getAvailablecategoriesForRetailer(double id);
public void updateRetailerCategories(List<RetailerCategory> providedCategories,
		double id);
public List<StoreCategory> getAvailablecategoriesForStore(double id);
public void updateStoreCategories(List<StoreCategory> providedCategories, int id);
}
