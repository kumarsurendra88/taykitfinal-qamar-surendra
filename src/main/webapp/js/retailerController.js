routerApp.controller('retailerSearchController',['$scope','$http',function($scope,$http){
	
	console.log("inside store report summary controller")
	$scope.radioValue;
	var Strid;
	
	$scope.searchOrders=function(dis){
	console.log("inside searchOrders");
	console.log(dis.id);	
	console.log($scope.radioValue);
	if(angular.isUndefined($scope.radioValue)){
		alert("Kindly select type of Id")
	}
	if($scope.radioValue=="1"){
		console.log("going to post for trackingid")
		Strid=dis.id;
		var array = Strid.split(',');
		console.log("data rerady to post is "+array)
		$http.get('./getTrackingIdMultiple/'+array)
		.success(function (data) {
		console.log(data);
		
		$scope.myData=data;

		
		});
	}
	if($scope.radioValue=="2"){
		console.log("going to post for orderid")
		$http.get('./getOrderIdMultiple/'+dis.id)
		.success(function (data) {
		console.log(data);
		
		$scope.myData=data;

		
		});
	}
	
	
	}
	
	$scope.searchFilterPackage=function(dis){
		
		console.log("inside searchFilterPackage");
		
		
		
	}
	
	/*$scope.gridOptions.data = [];*/
	$scope.gridOptions = {
	    pagingPageSizes: [25, 50, 75],
	    pagingPageSize: 25,
	    enableColumnResizing: true,
	    columnDefs: [
					{ name: 'trackingId',displayName:'Tracking#' },
					{ name: 'orderDetails',displayName:'Detail' },
					{ name: 'customerName',displayName:'Customer Name' },
					{ name: 'customerContact',displayName:'Customer Contact' },
					{name:'customerAddress',displayName:'Customer Address'},
					{name:'orderStatus',displayName:'Status'}
	    ],
	    enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'myFile.csv',
	    exporterPdfDefaultStyle: {fontSize: 9},
	    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
	    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
	    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
	    exporterPdfFooter: function ( currentPage, pageCount ) {
	      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
	    },
	    exporterPdfCustomFormatter: function ( docDefinition ) {
	      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
	      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
	      return docDefinition;
	    },
	    exporterPdfOrientation: 'portrait',
	    exporterPdfPageSize: 'LETTER',
	    exporterPdfMaxGridWidth: 500,
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    onRegisterApi: function(gridApi){ 
	      $scope.gridApi = gridApi;
	    }
	    
	  };
	
}])