routerApp.controller('attemptController',['$scope','$http','$window','$location',function($scope,$http,$window) {
        
    	$http.get("./getAttempts")
    	
    	.success(function(response) {$scope.attempts = response;});
    	$scope.filterOptions = {
    	        filterText: ''
    	      };
        $scope.gridUser = { 
            data: 'attempts',
            enableCellSelection: true,
            enableRowSelection: false,
            showSelectionCheckbox: true,
            enableCellEdit: true,
            columnDefs: [
                         {field:'userId', displayName:'user Id', enableCellEdit: false},
                         {field: 'customerName', displayName: 'customer name', enableCellEdit: false},
                         {field: 'customerContactNo', displayName: 'customer mobile', enableCellEdit: false}, 
                         {field:'trackingId', displayName:'tracking id', enableCellEdit: false},
                         {field:'callTime', displayName:'Attempt Time', enableCellEdit: false},
                         {field:'attemptNo', displayName:'Attempt Number', enableCellEdit: false},
                         {field: '', displayName:'Attempt Location',cellTemplate: '<button class="btn btn-default btn-flat" ng-click="loadById(row)">Map Address</button>'},
                         
                         ],
                         filterOptions: $scope.filterOptions
        };
        $scope.loadById = function(row) {  
      	  // window.console && console.log(row.entity);
      	   console.log("GeoLocation is" +row.entity.geoLocation)
      	   var uri="mapForEachAttempt.jsp?geoLocation="+row.entity.geoLocation;
      	   console.log("uri" +uri)
      	   $window.open(uri);
      	   //window.location.href= 'mapForEachVehicle.jsp?customerEmail='+row.entity.customerEmail;
      };  
    }]);