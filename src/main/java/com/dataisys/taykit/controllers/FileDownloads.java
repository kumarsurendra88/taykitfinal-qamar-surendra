package com.dataisys.taykit.controllers;

import java.io.*;  
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;  
import javax.servlet.http.*;  

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
  
public class FileDownloads extends HttpServlet {  
	

	//String filePath= System.getProperty("user.home") + "/Desktop"+"/runsheets";
	String filePath= System.getProperty("user.home") + "/Desktop"+"/htmlrunsheets";
	String filePathForStore= System.getProperty("user.home") + "/Desktop"+"/htmlrunsheetsforstores";
	
	
	@RequestMapping(value="/downloadCsv",method=RequestMethod.GET)
  
public void doGet(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
String filename = "runsheet_raw.csv";   

response.setContentType("APPLICATION/xls");   
response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
  
FileInputStream fileInputStream = new FileInputStream(path1);  
            
int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
}  
	
	@RequestMapping(value="/downloadHtml",method=RequestMethod.GET)
	public void doGet1(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_final.html";
String filename = "runsheet_final.html";   
String filepath = "e:\\";   
response.setContentType("APPLICATION/text");   
response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
  
FileInputStream fileInputStream = new FileInputStream(path1);  
            
int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
}  
	
	@RequestMapping(value="/downloadStoreCollect",method=RequestMethod.GET)
	public void doGet11(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
String path1= System.getProperty("user.home") + "/Desktop"+"/runsheetForCollect.html";
String filename = "runsheetForCollect.html";   
String filepath = "e:\\";   
response.setContentType("APPLICATION/text");   
response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
  
FileInputStream fileInputStream = new FileInputStream(path1);  
            
int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
}  
	
	@RequestMapping(value="/downloadStoreDelivery",method=RequestMethod.GET)
	public void doGet111(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
String path1= System.getProperty("user.home") + "/Desktop"+"/runsheetForDelivery.html";
String filename = "runsheetForDelivery.html";   
String filepath = "e:\\";   
response.setContentType("APPLICATION/text");   
response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
  
FileInputStream fileInputStream = new FileInputStream(path1);  
            
int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
}  
	
	@RequestMapping(value="/getAllFiles",method=RequestMethod.GET)
	
	public List<String> FileFolder(String folder){
		List<String> fileNames=listFilesAndFolders(filePath);
		return fileNames;
	}
	
	public List<String> listFilesAndFolders(String directoryName){
       
		
		//File directory = new File(directoryName);
		String directory="htmlrunsheets";
		ClassLoader classLoader = getClass().getClassLoader();
		File file1 = new File(classLoader.getResource(directory).getFile());
        //get all the files from a directory
        List<String> strings = new ArrayList<String>();
        Map<String,Long> values=new Hashtable<String,Long>();
        File[] fList = file1.listFiles();
        Arrays.sort(fList, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
        for (File file : fList){
            System.out.println(file.getName());
            System.out.println("date is before formating"+file.lastModified());
            	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    		
        	System.out.println("After Format : " + sdf.format(file.lastModified()));
        	String fileName=file.getName();
        	String fileCreationDate=sdf.format(file.lastModified());
            strings.add(fileName);
            strings.add(fileCreationDate);
            values.put(file.getName(), file.lastModified());
            System.out.println("");
        }
		return strings;
    }
	
	
	@RequestMapping(value="/downloadCsv/{fileName}",method=RequestMethod.GET)
	public void downlaodSpecifFile(@PathVariable("fileName") String fileName, HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
		response.setContentType("text/html");  
		PrintWriter out = response.getWriter();  
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
		String filename = "runsheet.html";   
		String directory="htmlrunsheets";
		ClassLoader classLoader = getClass().getClassLoader();
		File file1 = new File(classLoader.getResource(directory).getFile());

		response.setContentType("APPLICATION/xls");   
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   

		String newFilePath=file1+"/"+fileName+".html";
		
		System.out.println("download path is" +newFilePath);
  
		FileInputStream fileInputStream = new FileInputStream(newFilePath);  
            
		int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
} 
	
	
	@RequestMapping(value="/downloadStroeRunsheets/{fileName}",method=RequestMethod.GET)
	public void downlaodStoreRunsheets(@PathVariable("fileName") String fileName, HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
		
		//String filePathForStore= System.getProperty("user.home") + "/Desktop"+"/htmlrunsheets";
		
  
		response.setContentType("text/html");  
		PrintWriter out = response.getWriter();  
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
		String filename = "storerunsheet.html";   

		response.setContentType("APPLICATION/xls");   
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");  
		
		String  path="htmlrunsheetsforstores";
		ClassLoader classLoader = getClass().getClassLoader();
		File file1 = new File(classLoader.getResource(path).getFile());

		String newFilePath=file1+"/"+fileName+".html";
		
		System.out.println("download path is" +newFilePath);
  
		FileInputStream fileInputStream = new FileInputStream(newFilePath);  
            
		int i;   
while ((i=fileInputStream.read()) != -1) {  
out.write(i);   
}   
fileInputStream.close();   
out.close();   
} 
	
	
@RequestMapping(value="/getAllFilesForStore",method=RequestMethod.GET)
	
	public List<String> FileFolderForStore(String folder){
		List<String> fileNames=listFilesAndFoldersForStores(filePathForStore);
		return fileNames;
	}
	
	public List<String> listFilesAndFoldersForStores(String directoryName){
	       
		
			//File directory = new File(directoryName);
			String directory="htmlrunsheetsforstores";
			ClassLoader classLoader = getClass().getClassLoader();
			File file1 = new File(classLoader.getResource(directory).getFile());
	        //get all the files from a directory
	        List<String> strings = new ArrayList<String>();
	        Map<String,Long> values=new Hashtable<String,Long>();
	        File[] fList = file1.listFiles();
	        Arrays.sort(fList, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
	        for (File file : fList){
	            System.out.println(file.getName());
	            System.out.println("date is before formating"+file.lastModified());
	            	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	    		
	        	System.out.println("After Format : " + sdf.format(file.lastModified()));
	        	String fileName=file.getName();
	        	String fileCreationDate=sdf.format(file.lastModified());
	            strings.add(fileName);
	            strings.add(fileCreationDate);
	            values.put(file.getName(), file.lastModified());
	            System.out.println("");
	        }
			return strings;
	    }
	
  
}  