package com.dataisys.taykit.model;

public class RunsheetDetails {

	private String runSheetNO;
	private int totalShipment;
	private double shortFallValue;
	private int totalPrepaidDelivered;
	private int TotalPrepaidOrders;
	private int totalCodDelivered;
	private int totalCod;
	private double codCollected;
	private String shortFallReciept;
	private String codStatus;
	
	public RunsheetDetails() {
		super();
	}
	public RunsheetDetails(String runSheetNO, int totalShipment,
			double shortFallValue, int totalPrepaidDelivered,
			int totalPrepaidPending, int totalCodDelivered, int totalCod,
			double codCollected, String shortFallReciept, String codStatus) {
		super();
		this.runSheetNO = runSheetNO;
		this.totalShipment = totalShipment;
		this.shortFallValue = shortFallValue;
		this.totalPrepaidDelivered = totalPrepaidDelivered;
		this.TotalPrepaidOrders = totalPrepaidPending;
		this.totalCodDelivered = totalCodDelivered;
		this.totalCod = totalCod;
		this.codCollected = codCollected;
		this.shortFallReciept = shortFallReciept;
		this.codStatus = codStatus;
	}
	@Override
	public String toString() {
		return "CashDelivery [runSheetNO=" + runSheetNO + ", totalShipment="
				+ totalShipment + ", shortFallValue=" + shortFallValue
				+ ", totalPrepaidDelivered=" + totalPrepaidDelivered
				+ ", totalPrepaidPending=" + TotalPrepaidOrders
				+ ", totalCodDelivered=" + totalCodDelivered + ", totalCod="
				+ totalCod + ", codCollected=" + codCollected
				+ ", shortFallReciept=" + shortFallReciept + ", codStatus="
				+ codStatus + "]";
	}
	public String getRunSheetNO() {
		return runSheetNO;
	}
	public void setRunSheetNO(String runSheetNO) {
		this.runSheetNO = runSheetNO;
	}
	public int getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(int totalShipment) {
		this.totalShipment = totalShipment;
	}
	public double getShortFallValue() {
		return shortFallValue;
	}
	public void setShortFallValue(double shortFallValue) {
		this.shortFallValue = shortFallValue;
	}
	public int getTotalPrepaidDelivered() {
		return totalPrepaidDelivered;
	}
	public void setTotalPrepaidDelivered(int totalPrepaidDelivered) {
		this.totalPrepaidDelivered = totalPrepaidDelivered;
	}
	public int getTotalPrepaidOrders() {
		return TotalPrepaidOrders;
	}
	public void setTotalPrepaidOrders(int totalPrepaidPending) {
		this.TotalPrepaidOrders = totalPrepaidPending;
	}
	public int getTotalCodDelivered() {
		return totalCodDelivered;
	}
	public void setTotalCodDelivered(int totalCodDelivered) {
		this.totalCodDelivered = totalCodDelivered;
	}
	public int getTotalCod() {
		return totalCod;
	}
	public void setTotalCod(int totalCod) {
		this.totalCod = totalCod;
	}
	public double getCodCollected() {
		return codCollected;
	}
	public void setCodCollected(double codCollected) {
		this.codCollected = codCollected;
	}
	public String getShortFallReciept() {
		return shortFallReciept;
	}
	public void setShortFallReciept(String shortFallReciept) {
		this.shortFallReciept = shortFallReciept;
	}
	public String getCodStatus() {
		return codStatus;
	}
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
	}
}
