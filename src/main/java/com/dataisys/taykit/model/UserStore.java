package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class UserStore {

	private double userId;
	
	private double storeId;
	
	private String updatedBy;
	
	private Timestamp updatedOn;

	
	
	public UserStore(){}

	

	public UserStore(double userId, double storeId, String updatedBy,
			Timestamp updatedOn) {
		super();
		this.userId = userId;
		this.storeId = storeId;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}



	public double getUserId() {
		return userId;
	}



	public void setUserId(double userId) {
		this.userId = userId;
	}



	public double getStoreId() {
		return storeId;
	}



	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Timestamp getUpdatedOn() {
		return updatedOn;
	}



	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	


}
