package com.dataisys.taykit.model;

public class StoreReport {
	
	private String totInward;
	private String totCollected;
	private String totDelivery;
	private  String totCancelled;
	private String totReassigned;
	private String totPicked;
	private String totDropOff;
	private String totUndelivered;
	private String totAssignedReturn;
	private String totScheduleDel;
	private String totScheduleRet;
	private String totCodCollect;
	private String totCashCollect;
	private String totCashRemit;
	public String getTotInward() {
		return totInward;
	}
	public void setTotInward(String totInward) {
		this.totInward = totInward;
	}
	public String getTotCollected() {
		return totCollected;
	}
	public void setTotCollected(String totCollected) {
		this.totCollected = totCollected;
	}
	public String getTotDelivery() {
		return totDelivery;
	}
	public void setTotDelivery(String totDelivery) {
		this.totDelivery = totDelivery;
	}
	public String getTotCancelled() {
		return totCancelled;
	}
	public void setTotCancelled(String totCancelled) {
		this.totCancelled = totCancelled;
	}
	public String getTotReassigned() {
		return totReassigned;
	}
	public void setTotReassigned(String totReassigned) {
		this.totReassigned = totReassigned;
	}
	public String getTotPicked() {
		return totPicked;
	}
	public void setTotPicked(String totPicked) {
		this.totPicked = totPicked;
	}
	public String getTotDropOff() {
		return totDropOff;
	}
	public void setTotDropOff(String totDropOff) {
		this.totDropOff = totDropOff;
	}
	public String getTotUndelivered() {
		return totUndelivered;
	}
	public void setTotUndelivered(String totUndelivered) {
		this.totUndelivered = totUndelivered;
	}
	public String getTotAssignedReturn() {
		return totAssignedReturn;
	}
	public void setTotAssignedReturn(String totAssignedReturn) {
		this.totAssignedReturn = totAssignedReturn;
	}
	public String getTotScheduleDel() {
		return totScheduleDel;
	}
	public void setTotScheduleDel(String totScheduleDel) {
		this.totScheduleDel = totScheduleDel;
	}
	public String getTotScheduleRet() {
		return totScheduleRet;
	}
	public void setTotScheduleRet(String totScheduleRet) {
		this.totScheduleRet = totScheduleRet;
	}
	public String getTotCodCollect() {
		return totCodCollect;
	}
	public void setTotCodCollect(String totCodCollect) {
		this.totCodCollect = totCodCollect;
	}
	public String getTotCashCollect() {
		return totCashCollect;
	}
	public void setTotCashCollect(String totCashCollect) {
		this.totCashCollect = totCashCollect;
	}
	public String getTotCashRemit() {
		return totCashRemit;
	}
	public void setTotCashRemit(String totCashRemit) {
		this.totCashRemit = totCashRemit;
	}
	
	
	

}
