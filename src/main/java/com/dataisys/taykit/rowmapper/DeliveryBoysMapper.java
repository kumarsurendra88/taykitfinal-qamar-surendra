package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;



import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.User;

public class DeliveryBoysMapper  implements RowMapper<DeliveryBoys>{ 
	
	public DeliveryBoys mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		DeliveryBoys user = new DeliveryBoys();
		user.setDeliveryBoyId(rs.getInt("deliveryboy_id"));
		user.setDeliveryBoyAddress(rs.getString("deliveryboy_address"));
		user.setDeliveryBoyName(rs.getString("deliveryboy_name"));
		user.setStoreId(rs.getInt("store_id"));
		user.setSwipeDevice(rs.getString("swipeDevice"));
		System.out.println();
		return user;
	
	}

}
