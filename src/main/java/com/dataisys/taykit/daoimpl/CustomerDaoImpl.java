package com.dataisys.taykit.daoimpl;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;







import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.model.Customer;

public class CustomerDaoImpl extends JdbcDaoSupport implements CustomerDao {

	public void createCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		
			
			String sql = "INSERT INTO customer (customer_fullname,customer_email,customer_mobile,customer_address,customer_city,customer_pincode,customer_status,customer_type,customer_category,customer_gpslocation,updated_by)"
					+ " VALUES (?, ?, ?,?,?,?,?,?,?,?,?)";
			getJdbcTemplate().update(sql,customer.getCustomerName(),customer.getCustomerAddress(),customer.getCustomerMobile(),customer.getCustomerEmail(),
					customer.getCity(),customer.getPinCode(),customer.getCustomerStatus(),customer.getCustomerType(),customer.getCustomerCategory(),customer.getCustomerGpsLocation(),customer.getUpdatedBy());
			
		}
	
	

	public void deleteCustomer(double id) {
		// TODO Auto-generated method stub
		
	}

	public List<Customer> getCustomer() {
		// TODO Auto-generated method stub
		
		
				String sql= "SELECT * FROM `customer`";
		
		List<Customer> listofCustomers=getJdbcTemplate().query(sql,new CustomerRowMapper());
		return listofCustomers;
	}
	
	//Added By Kishore
		public Customer getPerticularCustomer(Customer customer) {
			System.out.println("Inside getPerticularCustomer");
			System.out.println("CustomerName: "+customer.getCustomerName());
			String sql = "SELECT * FROM `customer` WHERE customer_fullname = ? OR customer_email = ? OR customer_mobile = ?";
			Customer customer1 = getJdbcTemplate().queryForObject(sql,new Object[]{customer.getCustomerName(),customer.getCustomerEmail(),customer.getCustomerMobile()},new CustomerRowMapper());		
			return customer1;
		}
		public void updateCustomerByGpsLocation(Customer customer) {
			System.out.println("Inside updateCustomerByGpsLocation in CustomerDaoImpl");
			String sql = "UPDATE customer SET customer_gpslocation = ? WHERE customer_fullname = ? AND customer_mobile = ?";
			getJdbcTemplate().update(sql,customer.getCustomerGpsLocation(),customer.getCustomerName(),customer.getCustomerMobile());
			
		}



		public String updateCustomerByPhoneAddress(Customer customer) {

            // TODO Auto-generated method stub
            String result = null;
            System.out.println(" inside daoimplementation customer address and phone number in dao impl of customer daoimpl is "+customer.getCustomerAddress()+" and "+
            customer.getCustomerMobile()+" id is of type "+customer.getValue());
            if((customer.getValue()).equals("orderId")){
                System.out.println("inside orderId query");
                String orderId;
                try {
                    orderId = getJdbcTemplate().queryForObject(
                            "select order_id from `order` where order_id = ?",
                            new Object[]{customer.getId()}, String.class);
                    System.out.println("order id from db is "+orderId);
                    
                    if(customer.getCustomerAddress()!=null && !customer.getCustomerAddress().isEmpty()){
                        System.out.println("inside order id custom address");
                        result="1";
                        String sql = "UPDATE `order` SET customer_address = ? WHERE order_id = ?";
                        getJdbcTemplate().update(sql,customer.getCustomerAddress(),customer.getId());
                    }
                    if(customer.getCustomerMobile()!=null && !customer.getCustomerMobile().isEmpty())
                    {
                        System.out.println("inside orderid cust mobile number");
                        result="1";
                    String sql = "UPDATE `order` SET customer_mobile=? WHERE order_id = ?";
                    getJdbcTemplate().update(sql,customer.getCustomerMobile(),customer.getId());
                    }
                } catch (DataAccessException e) {
                    // TODO Auto-generated catch block
                    System.out.println("error is "+e);
                    
                    e.printStackTrace();
                    result="0";
                }                                                    
            }
            if((customer.getValue()).equals("trackingId")){
                System.out.println("inside trackingId query");
                try {
                    String trackingId = getJdbcTemplate().queryForObject(
                            "select tracking_id from `order` where tracking_id = ?",
                            new Object[]{customer.getId()}, String.class);
                    System.out.println("tracking  id from db is "+trackingId);
                    
                    if(customer.getCustomerAddress()!=null && !customer.getCustomerAddress().isEmpty()){
                        System.out.println("inside trackingId cust address");
                        result="1";
                    String sql = "UPDATE `order` SET customer_address = ?WHERE tracking_id = ?";
                    getJdbcTemplate().update(sql,customer.getCustomerAddress(),customer.getId());
                    }
                    if(customer.getCustomerMobile()!=null && !customer.getCustomerMobile().isEmpty()){
                        System.out.println("inside trackingid cust mobile");
                        result="1";
                        String sql = "UPDATE `order` SET customer_mobile=? WHERE tracking_id = ?";
                        getJdbcTemplate().update(sql,customer.getCustomerMobile(),customer.getId());
                        }
                } catch (DataAccessException e) {
                    // TODO Auto-generated catch block
                    
                    e.printStackTrace();
                    result="0";
                }                                                
            }
            return result;
            
		}

}
