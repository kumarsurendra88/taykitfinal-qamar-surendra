package com.dataisys.taykit.daoimpl;

import java.util.List;

import com.dataisys.taykit.rowmapper.DeviceRowMapper;
import com.dataisys.taykit.rowmapper.RetailerRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.DeviceDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.Device;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.User;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class DeviceDaoImpl extends JdbcDaoSupport implements DeviceDao {

	

	public void addDevice(Device device) {
		 String SQL = "insert into device (serial_no,device_name,device_type,device_number,sim_no,assigned_to,device_desc) values (?,?,?,?,?,?,?)";
		    getJdbcTemplate().update( SQL,device.getSerialNO(),device.getName(),device.getType(),device.getEmeiNO(),device.getSimNo(),device.getAssignedTo(),device.getDescription());
		
	}

	public void updateDevice(int id) {
		// TODO Auto-generated method stub
		
	}

	public List<Device> getDevices() {
String sql= "SELECT * FROM `device`";
		
		List<Device>devices=getJdbcTemplate().query(sql,new DeviceRowMapper());
		return devices;
	}

	

}
