package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Category;


public class CategoryRowMapper implements RowMapper<Category> {

	public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub {
				
		Category category=new Category();
		
		category.setCategoryDesc(rs.getString("category_desc"));
		category.setCategoryName(rs.getString("category_name"));
		category.setCategroyStatus(rs.getString("category_status"));
		return category;


	}


}
