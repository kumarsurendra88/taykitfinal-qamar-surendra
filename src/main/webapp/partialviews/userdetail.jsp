
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%>
<%@page import="org.springframework.jdbc.datasource.DataSourceUtils"%>
  
<%@page import="org.springframework.jdbc.core.JdbcTemplate"%>

<%@ page import="com.dataisys.taykit.model.*" %>

<%@page import="com.dataisys.taykit.dao.AreasDao"%>
<%@page language="java" import="com.dataisys.taykit.daoimpl.AreasDaoImpl"%>
<%@page import="com.dataisys.taykit.model.Areas"%>
<%@page import="java.util.List"%>

<%@page import="java.util.ListIterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-animate.js"></script>

</head>

    <body  style="width:100%">
 <%
 User user=(User)session.getAttribute("user");
 String userType=user.getUserType();
 //String acc=user.getAccNumber();
 //Store store=(Store)session.getAttribute("storeObj");

 System.out.println("&&&&&&&& usertype: "+userType);
// System.out.println("store name : "+store.getName());
 %>
    <div  ng-controller="outwardRetursTodeliveryBoys">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        

    
		<table class="table">
							<tr>
								<td style="border-top:0px;margin-left:500px;"><h2><b>User Details</b></h2></td>
								
							</tr>
						</table>
		 
			
				<form ng-submit="CreatUser()" method="POST">
				<div class="col-md-6">
				
						
					<table class="table">
						<tr>
							<td style="border-top:0px"><b> Full Name </b></td>
							<td style="border-top:0px"><input type="text"  name="userName"  placeholder="Name" ng-model = "obj.userName" class="form-control" ></td>
						</tr>
			
						<%
				//}else 
					if(userType.equalsIgnoreCase("SUPER_ADMIN")){
						%>
				
				<!-- ****Adding a condition to create drop down for the designation based on user type**** -->		
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserType" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
							<%
				} else if(userType.equalsIgnoreCase("Zonal Manager")){
					
					%>
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserTypeforZM" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
				<%	
				}else if(userType.equalsIgnoreCase("Regional Manager")){
					
					%>
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserTypeforRM" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
							<%
				} else if(userType.equalsIgnoreCase("Hub Admin")){
					
					%>
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserTypeforHA" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
							<% 
				}else if(userType.equalsIgnoreCase("Hub Executive")){
					
					%>
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserTypeforHE" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
				<% 
				} else if(userType.equalsIgnoreCase("Store Admin")){
					
					%>
						<tr>
							<td style="border-top:0px"><b>Designation</b></td>
							<td style="border-top:0px">
							
							<select name="userType" ng-options="option.name as option.name for option in listOfUserTypeforSA" 
        ng-model="obj.userType"   class="form-control">
      
						<option value="">Select Designation</option>
							</select>
							
							</td></tr>
				
				<%} %>	
				
					
							
							
							
							
							
							<!-- <td><input type="text"  name="userType"   ng-model="selectedItem2" placeholder="Designation"   class="form-control" readonly/></td> -->
						<tr ng-show="obj.userType=='Zonal Manager'">
						<td style="border-top:0px"><b>Zone</b></td>
						<td style="border-top:0px">
						
						
							<select ng-options="option.zone_name as option.zone_name for option in listOfZone" 
        ng-model="obj.zone_name" class="form-control" name="zone_name"  ng-change="getRegionList(obj.zone_name)">
      
						<option value="">Select Zone</option>
							</select>
							
						
							</td></tr>
					<tr ng-show="obj.userType=='Regional Manager'">	
					
							<td style="border-top:0px"><b>Zone</b></td><td style="border-top:0px"><select ng-options="option.zone_name as option.zone_name for option in listOfZone" 
        ng-model="obj.zone_name" class="form-control" name="zone_name"  ng-change="getRegionList(obj.zone_name)">
      
						<option value="">Select Zone</option>
							</select>
							
							</td>
						
							</tr>
							<tr ng-show="obj.userType=='Regional Manager'">	
							 <td style="border-top:0px"><b>Region</b></td><td style="border-top:0px"> <select ng-options="option.region_name as option.region_name for option in listOfRegions" 
        ng-model="obj.region_name" class="form-control" name="region_name"  >
      
						<option value="">Select Region</option>
							</select>
						
							</td>
							</tr>
							
							
							<tr ng-show="obj.userType=='Hub Admin'">
						
							<td style="border-top:0px"><b>Zone</b></td><td style="border-top:0px"><select ng-options="option.zone_name as option.zone_name for option in listOfZone" 
        ng-model="obj.zone_name" class="form-control" name="zone_name"  ng-change="getRegionList(obj.zone_name)">
      
						<option value="">Select Zone</option>
							</select>
							</td>
		</tr>
		<tr ng-show="obj.userType=='Hub Admin'">
							<td style="border-top:0px"><b>Region</b></td><td style="border-top:0px"><select ng-options="option.region_name as option.region_name for option in listOfRegions"
        ng-model="obj.region_name" class="form-control" name="region_name"  ng-change="getHubList(obj.region_name)">
      
						<option value="">Select Region</option>
							</select>
					</td>
					</tr>
					<tr ng-show="obj.userType=='Hub Admin'">
							 <td style="border-top:0px"><b>Hub</b></td><td style="border-top:0px"> <select ng-options="option.hub_name as option.hub_name for option in listOfHubs" 
        ng-model="obj.hub_name" class="form-control" name="hub_name"  >
      
						<option value="">Select Hub</option>
							</select>
						
							</td>
						</tr>
						<!-- Hub Executive starts -->
						<tr ng-show="obj.userType=='Hub Executive'">
						
							<td style="border-top:0px"><b>Zone</b></td><td style="border-top:0px"><select ng-options="option.zone_name as option.zone_name for option in listOfZone" 
        ng-model="obj.zone_name" class="form-control" name="zone_name"  ng-change="getRegionList(obj.zone_name)">
      
						<option value="">Select Zone</option>
							</select>
							</td>
		</tr>
		<tr ng-show="obj.userType=='Hub Executive'">
							<td style="border-top:0px"><b>Region</b></td><td style="border-top:0px"><select ng-options="option.region_name as option.region_name for option in listOfRegions"
        ng-model="obj.region_name" class="form-control" name="region_name"  ng-change="getHubList(obj.region_name)">
      
						<option value="">Select Region</option>
							</select>
					</td>
					</tr>
					<tr ng-show="obj.userType=='Hub Executive'">
							 <td style="border-top:0px"><b>Hub</b></td><td style="border-top:0px"> <select ng-options="option.hub_name as option.hub_name for option in listOfHubs" 
        ng-model="obj.hub_name" class="form-control" name="hub_name"  >
      
						<option value="">Select Hub</option>
							</select>
						
							</td>
						</tr>
			
			<!-- Hub Executive ends -->
			
			<tr ng-show="obj.userType=='Store Admin'">
						
							<td style="border-top:0px"><b>Zone</b></td><td style="border-top:0px"><select ng-options="option.zone_name as option.zone_name for option in listOfZone" 
        ng-model="obj.zone_name" class="form-control" name="zone_name"  ng-change="getRegionList(obj.zone_name)">
      
						<option value="">Select Zone</option>
							</select>
							</td>
		</tr>
		<tr ng-show="obj.userType=='Store Admin'">
							<td style="border-top:0px"><b>Region</b></td><td style="border-top:0px"><select ng-options="option.region_name as option.region_name for option in listOfRegions"
        ng-model="obj.region_name" class="form-control" name="region_name"  ng-change="getHubList(obj.region_name)">
      
						<option value="">Select Region</option>
							</select>
					</td>
					</tr>
					<tr ng-show="obj.userType=='Store Admin'">
							 <td style="border-top:0px"><b>Hub</b></td><td style="border-top:0px"> <select ng-options="option.hub_name as option.hub_name for option in listOfHubs" 
        ng-model="obj.hub_name" class="form-control" name="hub_name"  ng-change="getStoreList(obj.hub_name)">
      
						<option value="">Select Hub</option>
							</select>
						
							</td>
						</tr>
			
			<tr ng-show="obj.userType=='Store Admin'">
							 <td style="border-top:0px"><b>Store</b></td><td style="border-top:0px"> <select ng-options="option.name as option.name for option in listOfStores" 
        ng-model="obj.store_name" class="form-control" name="store_name"  >
      
						<option value="">Select Store</option>
							</select>
						
							</td>
						</tr>
			
			
			
						<tr>
							<td style="border-top:0px"><b>Contact No</b></td>
							<td style="border-top:0px"><input type="text"  name="userMobil"  placeholder="Contact No" ng-model="obj.userMobil"    class="form-control" ></td>
						</tr>
					<!-- 	<tr>
							<td style="border-top:0px"><b>Email Id</b></td>
							<td style="border-top:0px"><input type="text"  name="userEmail"  placeholder="Email Id"   class="form-control" ></td>
						</tr> -->
						<tr>
							<td style="border-top:0px"><b>Login ID</b></td>
							<td style="border-top:0px"><input type="text" name="accNumber" ng-model="obj.accNumber"  placeholder="Email Id or Mobile No"   class="form-control" ></td>
						</tr>
						<tr>
							<td style="border-top:0px"><b>Password</b></td>
							<td style="border-top:0px"><input type="text"  name="userPassword" ng-model="obj.userPassword" placeholder="Password"   class="form-control" ></td>
						</tr>
						<!-- <tr>
							<td style="border-top:0px"><b>Confirm password</b></td>
							<td style="border-top:0px"><input type="text"  name="userPassword"  placeholder="Confirm Password"   class="form-control" ng-click="getAccNum()"></td>
						</tr> -->
					<!-- 	<tr>
							<td style="border-top:0px"><b>Area</b></td>
							<td style="border-top:0px"><select ng-options="opt.area_name for opt in listOfOptions" 
        ng-model="selectedItem"
        ng-change="AccCreation(selectedItem)" class="form-control" >
						<option value="">Select Area</option>
							</select>
														</td>
					
						</tr> -->
					<!-- 	<tr>
							<td style="border-top:0px"><b>Account No</b></td>
							<td style="border-top:0px"><input type="text"  name="accNumber"  placeholder="Account No"   class="form-control" ng-model="outward.AccNum"></td>
						</tr> -->
					</table>
				</div>
				<div class="row">
				<div class="col-md-12">
					<table class="table">
					<tr>
						 
						<td style="border-top:0px" align="right" >
					
										
						
	   									<button class="btn btn-default">
	  									Cancel
										</button>
						
						
						 
	   									<button type="submit" class="btn btn-success">
	  									Save/Update
										</button>
						
						
	   									
								   
						</td>
					</tr>
				</table>
				</div>
			</div>
			
       
       </form>
</div>
    </body>
    
</html>