package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.mobile.model.UserStore;

public interface UserStoreDAO {
	
	public void createUserStore();
	public List<UserStore> getUsersByStoreId(double storeId);
	public List<UserStore> getStoreByUserId(double userId);

}
