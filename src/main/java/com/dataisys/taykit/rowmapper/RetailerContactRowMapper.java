package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Device;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerContact;

public class RetailerContactRowMapper implements RowMapper<RetailerContact>{

	
	
	public RetailerContact mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 RetailerContact contact= new RetailerContact();
	 contact.setContactDesignation(rs.getString("contact_designation"));
	 contact.setContactEmail(rs.getString("contact_email"));
	 contact.setContactMobile(rs.getString("contact_phone"));
	 contact.setContactName(rs.getString("contact_name"));
	 contact.setnotes(rs.getString("contact_notes"));
	 contact.setRetailerId(rs.getDouble("retailer_id"));
	 	return contact;
	 	
 }
	 	
 
 
 }
}

