package com.dataisys.taykit.controllers;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;






import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

















import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.daoimpl.OrderDaoImpl;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.RunsheetData;
//import com.csvreader.*;
import com.opencsv.CSVReader;


@RestController

public class RunsheetGenerator {
	
	/*@Autowired
	OrderDao orderDao;
	*/
//	OrderDaoImpl orderDao=new OrderDaoImpl();

	
	public  void main1(OrderDao dao,String userName) {    
        
		try{
			
			 BufferedReader br = null;
			// String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
			//	String path1= System.getProperty("user.home") + "/Desktop"+"/runsheets"+"/"+userName+".csv";
				String path1="runsheets";
				
				ClassLoader classLoader = getClass().getClassLoader();
				File file = new File(classLoader.getResource(path1).getFile());
				path1=file+"/"+userName+".csv";
			 
			 System.out.println("befor");
			 //String workingDirectory = System.getProperty("user.dir");
			 CSVReader reader = new CSVReader(new FileReader(path1));
			 //Generating run sheet number
						 
			
			 
			 
			
			/* CSVReader reader = new CSVReader(new FileReader("C:/Users/DataIsys/Documents/runsheet_raw.csv"));*/
			 System.out.println("after");
		     String [] nextLine;
		     nextLine = reader.readNext();
		     String Store =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeName =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeContact =  nextLine[1];
		     nextLine = reader.readNext();
		     String RunSheetID =  nextLine[1];
		     nextLine = reader.readNext();
		     reader.readNext();
		     String Table_top="";
		     String Runsheet ="";
		     String RunsheetScript="";
		     String returnSheetScript=""; //Added By Kishore on 2015-12-18
		     Double COD_TOTAL=0.0;
		     
		  
		     
		     
		     int i=0;
		     	 System.out.println("after int i");
		    	 RunsheetScript="";
		    	 Runsheet=Runsheet+"<style>tr{  page-break-inside: avoid;    -webkit-region-break-inside: avoid;} table,tr,td,th{ border-collapse: collapse; border: 1px solid black; }</style>\n";
			     /*Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Customer Name</th><th>Address</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th></tr></THEAD>\n<TBODY>";*/
		    	 //Following is Added By Kishore
		    	 Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Return ID</th><th>Customer Name</th><th>Address</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th><th>Order Details</th><th>Quantity</th></tr></THEAD>\n<TBODY>";
		     while ((nextLine = reader.readNext()) != null) {
		    	
		    	 System.out.println("inside while");
		    	 i++;
		    	 String COD=nextLine[0];
		    	 String Customer_Name=nextLine[1];
		    	 String Customer_Address=nextLine[2];
		    	 String Customer_Contact=nextLine[3];
		    	 String Tracking_ID=nextLine[4];
		    	 String Shipment_Type=nextLine[5];
		    	 String Retailer=nextLine[6];
		    	 String returnId=nextLine[7];
		    	 String orderDetails = nextLine[8];
		    	 String quantity = nextLine[9];
		    	 COD_TOTAL=COD_TOTAL+Double.parseDouble(COD);
		    	 Runsheet=Runsheet+"<tr><td>"+i+"</td><td>"+/*Tracking_ID+"<br>"+*/"<div id=\"bar_"+i+"\"></div></td>  <td>"+/*Tracking_ID+"<br>"+*/"<div id=\"ret_"+i+"\"></div></td>  <td>"+Customer_Name+"<br>"+Customer_Contact+"</td><td>"+Customer_Address+"</td><td>"+COD+"</td><td>"+Retailer+"</td><td>"+Shipment_Type+"</td><td></td><td></td><td></td><td>"+orderDetails+"</td><td>"+quantity+"</td></tr>\n";
		    	 RunsheetScript=RunsheetScript+" $(\"#bar_"+i+"\").barcode(\n";
		    	 RunsheetScript=RunsheetScript+"		 	\""+Tracking_ID+"\",\n";
		    	 RunsheetScript=RunsheetScript+"		 	\"code128\" \n";
		    	 RunsheetScript=RunsheetScript+"		 	);\n\n\n";
		    	 
		    	 //Added By Kishore -> This is to display Barcode for orderId
		    	 returnSheetScript=returnSheetScript+" $(\"#ret_"+i+"\").barcode(\n";
		    	 returnSheetScript=returnSheetScript+"		 	\""+returnId+"\",\n";
		    	 returnSheetScript=returnSheetScript+"		 	\"code128\" \n";
		    	 returnSheetScript=returnSheetScript+"		 	);\n\n\n";
		    	 
		    	 System.out.println("end of while");
		    	 
		     }
		     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		     Date date = new Date();
		     System.out.println(); //2014/08/06 15:59:48
		     
		   // String path= System.getProperty("user.home") + "/Desktop"+"/runsheet_final.html";
				//String path= System.getProperty("user.home") + "/Desktop"+"/htmlrunsheets"+"/"+userName+".html";
				
				String path="htmlrunsheets";

			
			 File file1 = new File(classLoader.getResource(path).getFile());
				
			 path=file1+"/"+userName+".html";
		     Runsheet=Runsheet+"</TBODY></table>";
		     PrintWriter writer=null;
		     File testf = null;
		     File testf1=null;
		     try{
		    	 
		    	/* writer = new PrintWriter(workingDirectory/runtime.ht, filename);*/
		    writer = new PrintWriter(path, "UTF-8");
		     testf = new File( this.getClass().getResource( "/jquery-barcode.js" ).toURI() );
		     testf1 = new File( this.getClass().getResource( "/jquery-latest.min.js" ).toURI() );
		     System.out.println(testf.toString());
		     if(testf.exists()){
		    	 System.out.println("file exist");
		     }
		     else{
		    	 System.out.println("sorry");
		     }
		     }
		     catch(Exception e){
		    	 System.out.println("inner"+e);
		     }

writer.println("<script src = \"http://52.74.176.39/taykit/jquery-latest.min.js\" type=\"text/javascript\" ></script>");
   writer.println("<script src = \"http://52.74.176.39/taykit/jquery-barcode.js\" type=\"text/javascript\" ></script>");
		   
		     writer.println("<table  border=\"1\" style=\"border-collapse: collapse; border: 1px solid black;\">");
		     writer.println("<tr><td>Store ID:<b>"+Store+"</b></td><td>Exec Name:<b>"+AssigneeName+"</b></td><td>Date:<b>"+dateFormat.format(date)+"</b></td></tr>");
		     writer.println("<tr><td>Runsheet ID:<b>"+RunSheetID+"</b></td><td>Exec Contact:<b>"+AssigneeContact+"</b></td></tr>");
		     writer.println("<tr><td># Shipments:<b>"+i+"</b></td><td>Total COD Amount:<b>"+COD_TOTAL+"</b></td></tr>");
		     writer.println("<tr><td>Opening Km:</td><td>Closing Km:</td></tr>");
		     writer.println("</table><br><br>");
		     
	    
		     writer.println(Runsheet);
		     writer.println("<script>");
		     writer.println(RunsheetScript);
		     writer.println(returnSheetScript);
		     writer.println("</script>");
		     System.out.println("before cloasing");
		     writer.close();
		     // inserting values into runsheet table
		     
		     RunsheetData rsd= new RunsheetData();
		     rsd.setRunsheetNo(RunSheetID);
		     rsd.setTotalCod(COD_TOTAL);
		     rsd.setTotalShipment(i);
		     rsd.setDeliveryBoy(AssigneeName);
		     List<Order> orders=dao.getOrdersByRunsheet(RunSheetID);
		     System.out.println("afetadasdfasdfasdfasdfasdfadfasdfasdf");
		     System.out.println("size of order is"+orders.size());
		     int prepaidOrdersCount=0;
		     int codOrdersCount=0;
		     int codDelivered=0;
		     int prepaidDeliverd=0;
		     
		     for(Order ord:orders) {
		    	
		    	 System.out.println("while generating orders");
		    	 if(ord.getOrderType().equalsIgnoreCase("perpaid")){
		    		 prepaidOrdersCount++;
		    	 }
		    	 else
		    	 {
		    		 codOrdersCount++;
		    	 }
		    	 if(ord.getOrderType().equalsIgnoreCase("prepaid")&&ord.getOrderStatus().equalsIgnoreCase("delivered")){
		    		 prepaidDeliverd++;
		    	 }
		    	 if(ord.getOrderType().equalsIgnoreCase("cod")&&ord.getOrderStatus().equalsIgnoreCase("delivered")){
		    		 codDelivered++;
		    	 }
		     }
		     rsd.setTotalPrepaid(prepaidOrdersCount);
		     rsd.setTotalCodOrders(codOrdersCount);
		     rsd.setPrepaidDelivered(prepaidDeliverd);
		     rsd.setCodDelivered(codDelivered);
		     dao.runSheetDetails(rsd);
		    // dao.setRunsheetToActive(RunSheetID, "active");
		}catch(Exception e){
			System.out.println("outer"+e);
		}
}
	
	
	
public  void main2(OrderDao dao ,String userName) {
		
		
		try{
			
			 BufferedReader br = null;
			// String path1= System.getProperty("user.home") + "/Desktop"+"/runsheetForCollect.csv";
			//	String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+userName+".csv";
				//String path1="storerunsheets";runsheets
			 String path1="storerunsheets";
				ClassLoader classLoader = getClass().getClassLoader();
				File file = new File(classLoader.getResource(path1).getFile());
				path1=file+"/"+userName+".csv";
			 System.out.println("befor");
			 //String workingDirectory = System.getProperty("user.dir");
			 CSVReader reader = new CSVReader(new FileReader(path1));
			 //Generating run sheet number
						 
			
			 
			 
			
			/* CSVReader reader = new CSVReader(new FileReader("C:/Users/DataIsys/Documents/runsheet_raw.csv"));*/
			 System.out.println("after");
		     String [] nextLine;
		     nextLine = reader.readNext();
		     String Store =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeName =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeContact =  nextLine[1];
		     nextLine = reader.readNext();
		     String RunSheetID =  nextLine[1];
		     nextLine = reader.readNext();
		     reader.readNext();
		     String Table_top="";
		     String Runsheet ="";
		     String RunsheetScript="";
		     Double COD_TOTAL=0.0;
		     
		  
		     
		     
		     int i=0;
		     	 System.out.println("after int i");
		    	 RunsheetScript="";
		    	 Runsheet=Runsheet+"<style>tr{  page-break-inside: avoid;    -webkit-region-break-inside: avoid;} table,tr,td,th{ border-collapse: collapse; border: 1px solid black; }</style>\n";
			     Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Customer Name</th><th>Address</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th></tr></THEAD>\n<TBODY>";
		     while ((nextLine = reader.readNext()) != null) {
		    	
		    	 System.out.println("inside while");
		    	 i++;
		    	 String COD=nextLine[0];
		    	 String Customer_Name=nextLine[1];
		    	 String Customer_Address=nextLine[2];
		    	 String Customer_Contact=nextLine[3];
		    	 String Tracking_ID=nextLine[4];
		    	 String Shipment_Type=nextLine[5];
		    	 String Retailer=nextLine[6];
		    	 COD_TOTAL=COD_TOTAL+Double.parseDouble(COD);
		    	 Runsheet=Runsheet+"<tr><td>"+i+"</td><td>"+/*Tracking_ID+"<br>"+*/"<div id=\"bar_"+i+"\"></div></td><td>"+Customer_Name+"<br>"+Customer_Contact+"</td><td>"+Customer_Address+"</td><td>"+COD+"</td><td>"+Retailer+"</td><td>"+Shipment_Type+"</td><td></td><td></td><td></tr>\n";
		    	 RunsheetScript=RunsheetScript+" $(\"#bar_"+i+"\").barcode(\n";
		    	 RunsheetScript=RunsheetScript+"		 	\""+Tracking_ID+"\",\n";
		    	 RunsheetScript=RunsheetScript+"		 	\"code128\" \n";
		    	 RunsheetScript=RunsheetScript+"		 	);\n\n\n";
		    	 
		    	 System.out.println("end of while");
		    	 
		     }
		     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		     Date date = new Date();
		     System.out.println(); //2014/08/06 15:59:48
		     
		    //String path= System.getProperty("user.home") + "/Desktop"+"/runsheetForCollect.html";
		  	String path= "htmlrunsheetsforstores";
		  	File file1 = new File(classLoader.getResource(path).getFile());
		  	path=file1+"/"+userName+".html";
		     Runsheet=Runsheet+"</TBODY></table>";
		     PrintWriter writer=null;
		     File testf = null;
		     File testf1=null;
		     try{
		    	 
		    	/* writer = new PrintWriter(workingDirectory/runtime.ht, filename);*/
		    writer = new PrintWriter(path, "UTF-8");
		     testf = new File( this.getClass().getResource( "/jquery-barcode.js" ).toURI() );
		     testf1 = new File( this.getClass().getResource( "/jquery-latest.min.js" ).toURI() );
		     System.out.println(testf.toString());
		     if(testf.exists()){
		    	 System.out.println("file exist");
		     }
		     else{
		    	 System.out.println("sorry");
		     }
		     }
		     catch(Exception e){
		    	 System.out.println();
		     }

writer.println("<script src = \"http://52.74.176.39/taykit/jquery-latest.min.js\" type=\"text/javascript\" ></script>");
    writer.println("<script src = \"http://52.74.176.39/taykit/jquery-barcode.js\" type=\"text/javascript\" ></script>");
		   
		     writer.println("<table  border=\"1\" style=\"border-collapse: collapse; border: 1px solid black;\">");
		     writer.println("<tr><td>Store ID:<b>"+Store+"</b></td><td>Exec Name:<b>"+AssigneeName+"</b></td><td>Date:<b>"+dateFormat.format(date)+"</b></td></tr>");
		     writer.println("<tr><td>Runsheet ID:<b>"+RunSheetID+"</b></td><td>Exec Contact:<b>"+AssigneeContact+"</b></td></tr>");
		     writer.println("<tr><td># Shipments:<b>"+i+"</b></td><td>Total COD Amount:<b>"+COD_TOTAL+"</b></td></tr>");
		     writer.println("<tr><td>Opening Km:</td><td>Closing Km:</td></tr>");
		     writer.println("</table><br><br>");
		     
	    
		     writer.println(Runsheet);
		     writer.println("<script>");
		     writer.println(RunsheetScript);
		     writer.println("</script>");
		     System.out.println("before cloasing");
		     writer.close();
		     // inserting values into runsheet table
		     
		     RunsheetData rsd= new RunsheetData();
		     rsd.setRunsheetNo(RunSheetID);
		     rsd.setTotalCod(COD_TOTAL);
		     rsd.setTotalShipment(i);
		     rsd.setDeliveryBoy(AssigneeName);
		     dao.runSheetDetails(rsd);
		}catch(Exception e){}

	}


public  void main3(OrderDao dao,String userName) {
	
	
	try{
		
		 BufferedReader br = null;
		//	String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+userName+".csv";	
		
			System.out.println("befor");
			
			String path1="storerunsheets";
			System.out.println("complete file path inside runsheet generator" +path1);
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(path1).getFile());
			path1=file+"/"+userName+".csv";
			//String workingDirectory = System.getProperty("user.dir");
			CSVReader reader = new CSVReader(new FileReader(path1));
			//Generating run sheet number
					 
		
		 
		 
		
		/* CSVReader reader = new CSVReader(new FileReader("C:/Users/DataIsys/Documents/runsheet_raw.csv"));*/
		 System.out.println("after");
	     String [] nextLine;
	     nextLine = reader.readNext();
	     String Store =  nextLine[1];
	     nextLine = reader.readNext();
	     String AssigneeName =  nextLine[1];
	     nextLine = reader.readNext();
	     String AssigneeContact =  nextLine[1];
	     nextLine = reader.readNext();
	     String RunSheetID =  nextLine[1];
	     nextLine = reader.readNext();
	     reader.readNext();
	     String Table_top="";
	     String Runsheet ="";
	     String RunsheetScript="";
	     Double COD_TOTAL=0.0;
	     
	  
	     
	     
	     int i=0;
	     	 System.out.println("after int i");
	    	 RunsheetScript="";
	    	 Runsheet=Runsheet+"<style>tr{  page-break-inside: avoid;    -webkit-region-break-inside: avoid;} table,tr,td,th{ border-collapse: collapse; border: 1px solid black; }</style>\n";
		     Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Customer Name</th><th>Address</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th></tr></THEAD>\n<TBODY>";
	     while ((nextLine = reader.readNext()) != null) {
	    	
	    	 System.out.println("inside while");
	    	 i++;
	    	 String COD=nextLine[0];
	    	 String Customer_Name=nextLine[1];
	    	 String Customer_Address=nextLine[2];
	    	 String Customer_Contact=nextLine[3];
	    	 String Tracking_ID=nextLine[4];
	    	 String Shipment_Type=nextLine[5];
	    	 String Retailer=nextLine[6];
	    	 COD_TOTAL=COD_TOTAL+Double.parseDouble(COD);
	    	 Runsheet=Runsheet+"<tr><td>"+i+"</td><td>"+/*Tracking_ID+"<br>"+*/"<div id=\"bar_"+i+"\"></div></td><td>"+Customer_Name+"<br>"+Customer_Contact+"</td><td>"+Customer_Address+"</td><td>"+COD+"</td><td>"+Retailer+"</td><td>"+Shipment_Type+"</td><td></td><td></td><td></tr>\n";
	    	 RunsheetScript=RunsheetScript+" $(\"#bar_"+i+"\").barcode(\n";
	    	 RunsheetScript=RunsheetScript+"		 	\""+Tracking_ID+"\",\n";
	    	 RunsheetScript=RunsheetScript+"		 	\"code128\" \n";
	    	 RunsheetScript=RunsheetScript+"		 	);\n\n\n";
	    	 
	    	 System.out.println("end of while");
	    	 
	     }
	     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	     Date date = new Date();
	     System.out.println(); //2014/08/06 15:59:48
	     
	   // String path= System.getProperty("user.home") + "/Desktop"+"/runsheetForDelivery.html";
	 	String path= "htmlrunsheetsforstores";
	 	
		File file1 = new File(classLoader.getResource(path).getFile());
		path=file1+"/"+userName+".html";
	     
	     Runsheet=Runsheet+"</TBODY></table>";
	     PrintWriter writer=null;
	     File testf = null;
	     File testf1=null;
	     try{
	    	 
	    	/* writer = new PrintWriter(workingDirectory/runtime.ht, filename);*/
	    writer = new PrintWriter(path, "UTF-8");
	     testf = new File( this.getClass().getResource( "/jquery-barcode.js" ).toURI() );
	     testf1 = new File( this.getClass().getResource( "/jquery-latest.min.js" ).toURI() );
	     System.out.println(testf.toString());
	     if(testf.exists()){
	    	 System.out.println("file exist");
	     }
	     else{
	    	 System.out.println("sorry");
	     }
	     }
	     catch(Exception e){
	    	 System.out.println();
	     }

writer.println("<script src = \"http://52.74.176.39/taykit/jquery-latest.min.js\" type=\"text/javascript\" ></script>");
writer.println("<script src = \"http://52.74.176.39/taykit/jquery-barcode.js\" type=\"text/javascript\" ></script>");
	   
	     writer.println("<table  border=\"1\" style=\"border-collapse: collapse; border: 1px solid black;\">");
	     writer.println("<tr><td>Store ID:<b>"+Store+"</b></td><td>Exec Name:<b>"+AssigneeName+"</b></td><td>Date:<b>"+dateFormat.format(date)+"</b></td></tr>");
	     writer.println("<tr><td>Runsheet ID:<b>"+RunSheetID+"</b></td><td>Exec Contact:<b>"+AssigneeContact+"</b></td></tr>");
	     writer.println("<tr><td># Shipments:<b>"+i+"</b></td><td>Total COD Amount:<b>"+COD_TOTAL+"</b></td></tr>");
	     writer.println("<tr><td>Opening Km:</td><td>Closing Km:</td></tr>");
	     writer.println("</table><br><br>");
	     
    
	     writer.println(Runsheet);
	     writer.println("<script>");
	     writer.println(RunsheetScript);
	     writer.println("</script>");
	     System.out.println("before cloasing");
	     writer.close();
	     // inserting values into runsheet table
	     
	     RunsheetData rsd= new RunsheetData();
	     int runSheetLength=RunSheetID.length();
	     if(runSheetLength<=8){
	     rsd.setRunsheetNo(RunSheetID);
	     }else{
	    	 rsd.setStoreRunSheetNumber(RunSheetID);
	     }
	     rsd.setTotalCod(COD_TOTAL);
	     rsd.setTotalShipment(i);
	     rsd.setDeliveryBoy(AssigneeName);
	     dao.runSheetDetails(rsd);
	}catch(Exception e){}

}

public  void main4(OrderDao dao ,String userName) {
	
	
	try{
		
		 BufferedReader br = null;
		// String path1= System.getProperty("user.home") + "/Desktop"+"/runsheetForCollect.csv";
		//	String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+userName+".csv";
			//String path1="storerunsheets";runsheets
		 String path1="runsheets";
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(path1).getFile());
			path1=file+"/"+userName+".csv";
		 System.out.println("befor");
		 //String workingDirectory = System.getProperty("user.dir");
		 CSVReader reader = new CSVReader(new FileReader(path1));
		 //Generating run sheet number
					 
		
		 
		 
		
		/* CSVReader reader = new CSVReader(new FileReader("C:/Users/DataIsys/Documents/runsheet_raw.csv"));*/
		 System.out.println("after");
	     String [] nextLine;
	     nextLine = reader.readNext();
	     String Store =  nextLine[1];
	     nextLine = reader.readNext();
	     String AssigneeName =  nextLine[1];
	     nextLine = reader.readNext();
	     String AssigneeContact =  nextLine[1];
	     nextLine = reader.readNext();
	     String RunSheetID =  nextLine[1];
	     nextLine = reader.readNext();
	     reader.readNext();
	     String Table_top="";
	     String Runsheet ="";
	     String RunsheetScript="";
	     Double COD_TOTAL=0.0;
	     
	  
	     
	     
	     int i=0;
	     	 System.out.println("after int i");
	    	 RunsheetScript="";
	    	 Runsheet=Runsheet+"<style>tr{  page-break-inside: avoid;    -webkit-region-break-inside: avoid;} table,tr,td,th{ border-collapse: collapse; border: 1px solid black; }</style>\n";
		     Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Customer Name</th><th>Address</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th></tr></THEAD>\n<TBODY>";
	     while ((nextLine = reader.readNext()) != null) {
	    	
	    	 System.out.println("inside while");
	    	 i++;
	    	 String COD=nextLine[0];
	    	 String Customer_Name=nextLine[1];
	    	 String Customer_Address=nextLine[2];
	    	 String Customer_Contact=nextLine[3];
	    	 String Tracking_ID=nextLine[4];
	    	 String Shipment_Type=nextLine[5];
	    	 String Retailer=nextLine[6];
	    	 COD_TOTAL=COD_TOTAL+Double.parseDouble(COD);
	    	 Runsheet=Runsheet+"<tr><td>"+i+"</td><td>"+/*Tracking_ID+"<br>"+*/"<div id=\"bar_"+i+"\"></div></td><td>"+Customer_Name+"<br>"+Customer_Contact+"</td><td>"+Customer_Address+"</td><td>"+COD+"</td><td>"+Retailer+"</td><td>"+Shipment_Type+"</td><td></td><td></td><td></tr>\n";
	    	 RunsheetScript=RunsheetScript+" $(\"#bar_"+i+"\").barcode(\n";
	    	 RunsheetScript=RunsheetScript+"		 	\""+Tracking_ID+"\",\n";
	    	 RunsheetScript=RunsheetScript+"		 	\"code128\" \n";
	    	 RunsheetScript=RunsheetScript+"		 	);\n\n\n";
	    	 
	    	 System.out.println("end of while");
	    	 
	     }
	     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	     Date date = new Date();
	     System.out.println(); //2014/08/06 15:59:48
	     
	    //String path= System.getProperty("user.home") + "/Desktop"+"/runsheetForCollect.html";
	  	String path= "htmlrunsheets";
	  	File file1 = new File(classLoader.getResource(path).getFile());
	  	path=file1+"/"+userName+".html";
	     Runsheet=Runsheet+"</TBODY></table>";
	     PrintWriter writer=null;
	     File testf = null;
	     File testf1=null;
	     try{
	    	 
	    	/* writer = new PrintWriter(workingDirectory/runtime.ht, filename);*/
	    writer = new PrintWriter(path, "UTF-8");
	     testf = new File( this.getClass().getResource( "/jquery-barcode.js" ).toURI() );
	     testf1 = new File( this.getClass().getResource( "/jquery-latest.min.js" ).toURI() );
	     System.out.println(testf.toString());
	     if(testf.exists()){
	    	 System.out.println("file exist");
	     }
	     else{
	    	 System.out.println("sorry");
	     }
	     }
	     catch(Exception e){
	    	 System.out.println();
	     }

writer.println("<script src = \"http://52.74.176.39/taykit/jquery-latest.min.js\" type=\"text/javascript\" ></script>");
writer.println("<script src = \"http://52.74.176.39/taykit/jquery-barcode.js\" type=\"text/javascript\" ></script>");
	   
	     writer.println("<table  border=\"1\" style=\"border-collapse: collapse; border: 1px solid black;\">");
	     writer.println("<tr><td>Store ID:<b>"+Store+"</b></td><td>Exec Name:<b>"+AssigneeName+"</b></td><td>Date:<b>"+dateFormat.format(date)+"</b></td></tr>");
	     writer.println("<tr><td>Runsheet ID:<b>"+RunSheetID+"</b></td><td>Exec Contact:<b>"+AssigneeContact+"</b></td></tr>");
	     writer.println("<tr><td># Shipments:<b>"+i+"</b></td><td>Total COD Amount:<b>"+COD_TOTAL+"</b></td></tr>");
	     writer.println("<tr><td>Opening Km:</td><td>Closing Km:</td></tr>");
	     writer.println("</table><br><br>");
	     
    
	     writer.println(Runsheet);
	     writer.println("<script>");
	     writer.println(RunsheetScript);
	     writer.println("</script>");
	     System.out.println("before cloasing");
	     writer.close();
	     // inserting values into runsheet table
	     
	     RunsheetData rsd= new RunsheetData();
	     rsd.setRunsheetNo(RunSheetID);
	     rsd.setTotalCod(COD_TOTAL);
	     rsd.setTotalShipment(i);
	     rsd.setDeliveryBoy(AssigneeName);
	     dao.runSheetDetails(rsd);
	}catch(Exception e){}

}


}

	
	
	

