package com.dataisys.taykit.daoimpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.AccountNoRowMapper;
import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.ReportingStoreRowMapper;
import com.dataisys.taykit.rowmapper.RetailerRowMapper;
import com.dataisys.taykit.rowmapper.StoreLocationRowMapper;
import com.dataisys.taykit.rowmapper.StoreRowMapper;
import com.dataisys.taykit.dao.ProductCategoriesDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.StoreContactDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.ShortfallAmount;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreCategory;
import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StoreLocation;
import com.dataisys.taykit.model.StorePaymentCollection;
import com.dataisys.taykit.model.StoreService;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class PudoStoreDaoImpl extends JdbcDaoSupport implements PudoStoreDao {
   @Autowired
	StoreContactDao storeContactDao;
	@Autowired
	StoreServicesDao storeServiceDao;
	@Autowired
	ProductCategoriesDao categoryServiceDao;
	public void createStore(Store pudoStore) {
		// TODO Auto-generated method stub
		
	}

	public void updateStore(Store pudoStore) {
		// TODO Auto-generated method stub
		
	}

	public void deleteStore(double storeId) {
		// TODO Auto-generated method stub
		
	}

	public void AddStore(Store store, List<StoreServices> services,
			List<ProductCategories> category,List<StoreContact> contacts,HttpSession session) {
		
		User user=(User)session.getAttribute("user");
		if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN"))
		System.out.println("in Store implementstion");
		System.out.println("store details are"+store);
		System.out.println("store services"+services);
		System.out.println("store category"+category);
		System.out.println("store contacts are"+contacts);
		
		/*double StoreId=(Double)session.getAttribute("storeId");
		
		if(store.getReportingStore()=="NULL"){
			Store str=getStoreById(StoreId);
			store.setReportingStore(str.getName());
			
		}*/
		 //String SQL = "insert into store (store_address,store_type,store_name,store_city,pincode,store_gpslocation,store_cctvid,edm_availability,store_landmark,store_category,survey_token,reporting_store,cash_payment,mobile_payment,creditcard_payment,) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 
String SQL = "insert into store (store_address,store_type,store_name,store_city,pincode,store_gpslocation,store_cctvid,edm_availability,store_landmark,store_category,survey_token,reporting_store,cash_payment,mobile_payment,creditcard_payment,hub_id,region_id,zone_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,(select hub_id from hubs where hub_name=?),(select region_id from regions where region_name=?),(select zone_id from zones where zone_name=?))";
		getJdbcTemplate().update( SQL,store.getAddress(),store.getType(),store.getName(),store.getCity(),store.getPinCode(),store.getGeoLocation(),store.getCctvId(),store.getEdmAvailbility(),store.getLandMark(),store.getCategory(),store.getSurveyToken(),store.getReportingStore(),store.getCashPayment(),store.getMobilePayment(),store.getCreditCardPayment(),store.getHub_name(),store.getRegion_name(),store.getZone_name());
		 double storeId=getStoreId();
		 
		List<StoreService> providedServices=new ArrayList<StoreService>();
		Iterator<StoreServices> it=services.iterator();
	    Iterator<StoreContact> contactsIt=contacts.iterator();
	    List<StoreCategory> providedCategories=new ArrayList<StoreCategory>();
	    Iterator<ProductCategories> categoryIt=category.iterator();
	    
	    while(contactsIt.hasNext())
		{
			contactsIt.next().setStoreId(storeId);
		}
	    while(categoryIt.hasNext())
	    {
	    	ProductCategories obj=categoryIt.next();
	    	if(obj.getCategoryStatus()==true)
	    	{
	    	StoreCategory data=new StoreCategory();
	    	data.setCategoryName(obj.getCategoryName());
	    	data.setProductCategoryId(obj.getCategoryId());
	    	data.setStoreId(storeId);
	    	providedCategories.add(data);
	    	
	    	}
	    }
	    while(it.hasNext())
	    {
	    	StoreServices obj=it.next();
	    	if(obj.getServiceStatus()==true)
	    	{
	    	StoreService data=new StoreService();
	    	data.setServiceName(obj.getName());
	    	data.setStoreServiceTypeId(obj.getServiceId());
	    	data.setStoreId(storeId);
	    	providedServices.add(data);
	    	}
	    }
	    
		storeContactDao.addContact(contacts);
		storeServiceDao.addServices(providedServices);
		categoryServiceDao.addStoreCategories(providedCategories);
	}

	
	
	public Double getStoreId()
	   {
		 
		   String SQl="SELECT * FROM store WHERE store_id=(SELECT MAX(store_id) FROM store)";
		   Store record=getJdbcTemplate().queryForObject(SQl, new StoreRowMapper()) ;
		   Double id=record.getStoreId();
		   System.out.println("returned stor_id is"+id);
		return id; 	
	   }

	public Store getStoreById(double id)
	   {
		 
		   String SQl="SELECT * FROM store WHERE store_id="+id+"";
		   Store record=getJdbcTemplate().queryForObject(SQl, new StoreRowMapper()) ;
		   return record; 	
	   }

	public List<Store> getStores(HttpSession session) {
	
		//HttpSession session;
		
		String loggedInStore=(String)session.getAttribute("loggedStore");
		User user=(User)session.getAttribute("user");
		//Store st=getStoreById((Double)session.getAttribute("storeId"));
	//	if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")){
		
		String sql= "SELECT *FROM `store`";
		
		List<Store>storeDeatils=getJdbcTemplate().query(sql,new StoreRowMapper());
		return storeDeatils;
		//}
	//	else{
			
		//	String sql = "select *from `store` where reporting_store=?";
			//System.out.println("reporting store query"+sql);
		//	System.out.println("logged store is"+st);
			
			// List<Store> stores=getJdbcTemplate().query(sql, new Object[] {st.getName()},new StoreRowMapper());
			 //return stores;
			
		//}
	}

	public void updateStore(Store store, List<StoreServices> storeservices,
			List<ProductCategories> categoryServices,
			List<StoreContact> storeContacts, int id) {
		String SQL = "update store set store_address=?,store_type=?,store_name=?,store_city=?,pincode=?,store_gpslocation=?,store_cctvid=?,edm_availability=?,store_landmark=?,store_category=?,survey_token=?,reporting_store=?,cash_payment=?,mobile_payment=?,creditcard_payment=? where store_id=?";
		 getJdbcTemplate().update( SQL,store.getAddress(),store.getType(),store.getName(),store.getCity(),store.getPinCode(),store.getGeoLocation(),store.getCctvId(),store.getEdmAvailbility(),store.getLandMark(),store.getCategory(),store.getSurveyToken(),store.getReportingStore(),store.getCashPayment(),store.getMobilePayment(),store.getCreditCardPayment(),id);
		
		 
		List<StoreService> providedServices=new ArrayList<StoreService>();
		Iterator<StoreServices> it=storeservices.iterator();
	    Iterator<StoreContact> contactsIt=storeContacts.iterator();
	    List<StoreCategory> providedCategories=new ArrayList<StoreCategory>();
	    Iterator<ProductCategories> categoryIt=categoryServices.iterator();
	    
	    while(contactsIt.hasNext())
		{
			contactsIt.next().setStoreId(id);
		}
	    while(categoryIt.hasNext())
	    {
	    	ProductCategories obj=categoryIt.next();
	    	if(obj.getCategoryStatus()==true)
	    	{
	    	StoreCategory data=new StoreCategory();
	    	data.setCategoryName(obj.getCategoryName());
	    	data.setProductCategoryId(obj.getCategoryId());
	    	data.setStoreId(id);
	    	providedCategories.add(data);
	    	
	    	}
	    }
	    while(it.hasNext())
	    {
	    	StoreServices obj=it.next();
	    	if(obj.getServiceStatus()==true)
	    	{
	    	StoreService data=new StoreService();
	    	data.setServiceName(obj.getName());
	    	data.setStoreServiceTypeId(obj.getServiceId());
	    	data.setStoreId(id);
	    	providedServices.add(data);
	    	}
	    }
	    
		storeContactDao.addContact(storeContacts);
		storeServiceDao.updateServices(providedServices,id);
		categoryServiceDao.updateStoreCategories(providedCategories,id);
		
	}
	
	public double getStoreIdByName(String storeName) {
		System.out.println("Store name : "+storeName);
		String sql="select store_id from store where store_name=?";

		return getJdbcTemplate().queryForInt(sql, new Object[]{storeName});
		}
	
	public Store getStoreByAccountNo(String accountNo) {
		// TODO Auto-generated method stub
		System.out.println("Account Number"+accountNo);
		String sql="select * from store where account_no=?";
		Store obj=getJdbcTemplate().queryForObject(sql, new Object[]{accountNo},new StoreRowMapper());
		return obj;
		}


	public void collectStorePayment(StorePaymentCollection obj) {
		String sql="insert into store_paymentcollection (runsheet_number,reporting_store,cod_value,cod_collected,card,cod_shortfall,DATE) values(?,?,?,?,?,?,?)";
		getJdbcTemplate().update(sql, obj.getRunsheetNumber(),obj.getStoreName(),obj.getCodAmount(),obj.getCodCollected(),obj.getCard(),obj.getShortFall(),obj.getHandedDate());
		
	}
	
	public void saveStoreShortFall(ShortfallAmount details) {
		System.out.println(details);	
		String sql="insert into store_shortfall(runsheet_number,store_name,shortfall_amount,description)values(?,?,?,?)";
		getJdbcTemplate().update(sql, details.getRunsheetNo(),details.getDeliveryUser(),details.getShortFallAmount(),details.getDescription());
	}

	public String getStoreAccNo(String storeName){
		
		System.out.println("store name is"+storeName);
		String sql="select account_no from `store` where store_name=?";
		return getJdbcTemplate().queryForObject(sql, new Object[]{storeName},String.class);
	}

	public double getStoreIdByAccountNo(String accountNo) {
		// TODO Auto-generated method stub
		
		System.out.println("Account number inside getStoreIdByAccountNo "+accountNo);
		
		String sql="select store_id from `user_store` where account_no=?";
		return getJdbcTemplate().queryForInt(sql, new Object[]{accountNo});
		
	}
	
	public List<Store> getStores() {
		String sql= "SELECT * FROM `store`";
			
			List<Store>storeDeatils=getJdbcTemplate().query(sql,new StoreRowMapper());
			return storeDeatils;
		}

}
