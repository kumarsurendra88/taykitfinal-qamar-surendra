package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class OrderRevision {

	private double revisionId;
	private double revisionNo;
	private double orderId;
	private String updatedBy;
	private Timestamp updatedOn;
	public OrderRevision() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderRevision(double revisionId, double revisionNo, double orderId,
			String updatedBy, Timestamp updatedOn) {
		super();
		this.revisionId = revisionId;
		this.revisionNo = revisionNo;
		this.orderId = orderId;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	public double getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(double revisionId) {
		this.revisionId = revisionId;
	}
	public double getRevisionNo() {
		return revisionNo;
	}
	public void setRevisionNo(double revisionNo) {
		this.revisionNo = revisionNo;
	}
	public double getOrderId() {
		return orderId;
	}
	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Override
	public String toString() {
		return "OrderRevision [revisionId=" + revisionId + ", revisionNo="
				+ revisionNo + ", orderId=" + orderId + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn + "]";
	}
	
	
}
