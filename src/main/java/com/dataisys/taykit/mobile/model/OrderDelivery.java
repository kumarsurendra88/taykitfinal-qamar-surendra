package com.dataisys.taykit.mobile.model;

import java.sql.Timestamp;

public class OrderDelivery {

	private double deliveryId;
	private String trackingId;
	private double userId;
	private Timestamp date;
	private String status;
	private String deliveredLocation;
	private String notes;
	private String updatedBy;
	private Timestamp updatedOn;
	private String paymentType;
	private String signature;
	private String receivedBy;
	//added on 11/5/2015
	private String updatedOnMobile;
	//Added on 26/11/2015
	private String rRNo;	
		
	public String getUpdatedOnMobile() {
			return updatedOnMobile;
		}
		public void setUpdatedOnMobile(String updatedOnMobile) {
			this.updatedOnMobile = updatedOnMobile;
		}
		public String getrRNo() {
			return rRNo;
		}
		public void setrRNo(String rRNo) {
			this.rRNo = rRNo;
		}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public double getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(double deliveryId) {
		this.deliveryId = deliveryId;
	}	
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public double getUserId() {
		return userId;
	}
	public void setUserId(double userId) {
		this.userId = userId;
	}	
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeliveredLocation() {
		return deliveredLocation;
	}
	public void setDeliveredLocation(String location) {
		this.deliveredLocation = location;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}	
}
