routerApp.service('returnUpload', ['$http' ,'$location' , function ($http , $location ) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	
        	console.log("hi... file uploaded successfully");
        	$location.path('imporders');
        })
        .error(function(){
        });
        
        
        
    }
}]);

routerApp.controller('returnCtrl', ['$scope','$location', 'returnUpload', function($scope, $location, returnUpload){
    
	
	$scope.uploadFile = function(){
        var file = $scope.myFile;
        console.log('file is ' + JSON.stringify(file));
        var uploadUrl = "./uploadReturn";
        returnUpload.uploadFileToUrl(file, uploadUrl);
    };
    
}]);

routerApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);