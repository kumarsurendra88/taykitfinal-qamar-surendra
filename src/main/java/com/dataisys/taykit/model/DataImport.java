package com.dataisys.taykit.model;

import java.sql.Date;
import java.sql.Timestamp;

public class DataImport {

		private String sessionId;
		private double dataId;
		private String importType;
		private double retailerId;
		private String dataStatus;
		private String orderCategory;
		private double storeId;
		private String orderType;
		private String dataNote;
		private String updatedBy;
		private Timestamp updatedOn;
		
		private String trackingId;
		private String orderId;
		private String orderTitle;
		private String orderDetails;
		//private String orderType;
		//private String orderCategory;
		private String orderPriority;
		private double orderProductId;
		private double activityId;
		private double revisionId;
		private double attachmentId;
		private String orderRefno;
		private String customerName;
		private String customerContact;
		private String customerEmail;
		private String customerAddress;
		private String cityName;
		private String areaName;
		private int customerZipcode;
		private String paymentType;
		private int bayNumber;
		private String orderStatus;
		private Date receiptDate;
		private Date deliveryDate;
		private String assignedTo;
		private String deliveredBy;
		private String paymentStatus;
		private String paymentReference;
		private double orderValue;
		private String createdBy;
		private Date createdOn;
		
		private String storeName;
		
		
		private String country;
		private double weight;
		private String content;
		private double vat;
		private String alternaitveContactNo;
		private double acctualAmount;
		public DataImport() {
			super();
		}
 
		
		 
		 

		public String getStoreName() {
			return storeName;
		}





		public void setStoreName(String storeName) {
			this.storeName = storeName;
		}





		public String getTrackingId() {
			return trackingId;
		}





		public void setTrackingId(String trackingId) {
			this.trackingId = trackingId;
		}





		public DataImport(String sessionId, double dataId, String importType,
				double retailerId, String dataStatus, String orderCategory,
				double storeId, String orderType, String dataNote,
				String updatedBy, Timestamp updatedOn, String trackingId,
				String orderId, String orderTitle, String orderDetails,
				String orderPriority, double orderProductId, double activityId,
				double revisionId, double attachmentId, String orderRefno,
				String customerName, String customerContact,
				String customerEmail, String customerAddress, String cityName,
				String areaName, int customerZipcode, String paymentType,
				int bayNumber, String orderStatus, Date receiptDate,
				Date deliveryDate, String assignedTo, String deliveredBy,
				String paymentStatus, String paymentReference,
				double orderValue, String createdBy, Date createdOn) {
			super();
			this.sessionId = sessionId;
			this.dataId = dataId;
			this.importType = importType;
			this.retailerId = retailerId;
			this.dataStatus = dataStatus;
			this.orderCategory = orderCategory;
			this.storeId = storeId;
			this.orderType = orderType;
			this.dataNote = dataNote;
			this.updatedBy = updatedBy;
			this.updatedOn = updatedOn;
			this.trackingId = trackingId;
			this.orderId = orderId;
			this.orderTitle = orderTitle;
			this.orderDetails = orderDetails;
			this.orderPriority = orderPriority;
			this.orderProductId = orderProductId;
			this.activityId = activityId;
			this.revisionId = revisionId;
			this.attachmentId = attachmentId;
			this.orderRefno = orderRefno;
			this.customerName = customerName;
			this.customerContact = customerContact;
			this.customerEmail = customerEmail;
			this.customerAddress = customerAddress;
			this.cityName = cityName;
			this.areaName = areaName;
			this.customerZipcode = customerZipcode;
			this.paymentType = paymentType;
			this.bayNumber = bayNumber;
			this.orderStatus = orderStatus;
			this.receiptDate = receiptDate;
			this.deliveryDate = deliveryDate;
			this.assignedTo = assignedTo;
			this.deliveredBy = deliveredBy;
			this.paymentStatus = paymentStatus;
			this.paymentReference = paymentReference;
			this.orderValue = orderValue;
			this.createdBy = createdBy;
			this.createdOn = createdOn;
		}



			

		public String getCountry() {
			return country;
		}





		public void setCountry(String country) {
			this.country = country;
		}





		public double getWeight() {
			return weight;
		}





		public void setWeight(double weight) {
			this.weight = weight;
		}





		public String getContent() {
			return content;
		}





		public void setContent(String content) {
			this.content = content;
		}





		public double getVat() {
			return vat;
		}





		public void setVat(double vat) {
			this.vat = vat;
		}





		public String getAlternaitveContactNo() {
			return alternaitveContactNo;
		}





		public void setAlternaitveContactNo(String alternaitveContactNo) {
			this.alternaitveContactNo = alternaitveContactNo;
		}





		public double getAcctualAmount() {
			return acctualAmount;
		}





		public void setAcctualAmount(double acctualAmount) {
			this.acctualAmount = acctualAmount;
		}





		public String getOrderId() {
			return orderId;
		}



		public void setOrderId(String string) {
			this.orderId = string;
		}



		public String getOrderTitle() {
			return orderTitle;
		}



		public void setOrderTitle(String orderTitle) {
			this.orderTitle = orderTitle;
		}



		public String getOrderDetails() {
			return orderDetails;
		}



		public void setOrderDetails(String orderDetails) {
			this.orderDetails = orderDetails;
		}



		public String getOrderPriority() {
			return orderPriority;
		}



		public void setOrderPriority(String orderPriority) {
			this.orderPriority = orderPriority;
		}



		public double getOrderProductId() {
			return orderProductId;
		}



		public void setOrderProductId(double orderProductId) {
			this.orderProductId = orderProductId;
		}



		public double getActivityId() {
			return activityId;
		}



		public void setActivityId(double activityId) {
			this.activityId = activityId;
		}



		public double getRevisionId() {
			return revisionId;
		}



		public void setRevisionId(double revisionId) {
			this.revisionId = revisionId;
		}



		public double getAttachmentId() {
			return attachmentId;
		}



		public void setAttachmentId(double attachmentId) {
			this.attachmentId = attachmentId;
		}



		public String getOrderRefno() {
			return orderRefno;
		}



		public void setOrderRefno(String orderRefno) {
			this.orderRefno = orderRefno;
		}



		public String getCustomerName() {
			return customerName;
		}



		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}



		public String getCustomerContact() {
			return customerContact;
		}



		public void setCustomerContact(String customerContact) {
			this.customerContact = customerContact;
		}



		public String getCustomerEmail() {
			return customerEmail;
		}



		public void setCustomerEmail(String customerEmail) {
			this.customerEmail = customerEmail;
		}



		public String getCustomerAddress() {
			return customerAddress;
		}



		public void setCustomerAddress(String customerAddress) {
			this.customerAddress = customerAddress;
		}



		public String getCityName() {
			return cityName;
		}



		public void setCityName(String cityName) {
			this.cityName = cityName;
		}



		public String getAreaName() {
			return areaName;
		}



		public void setAreaName(String areaName) {
			this.areaName = areaName;
		}



		public int getCustomerZipcode() {
			return customerZipcode;
		}



		public void setCustomerZipcode(int customerZipcode) {
			this.customerZipcode = customerZipcode;
		}



		public String getPaymentType() {
			return paymentType;
		}



		public void setPaymentType(String paymentType) {
			this.paymentType = paymentType;
		}



		public int getBayNumber() {
			return bayNumber;
		}



		public void setBayNumber(int bayNumber) {
			this.bayNumber = bayNumber;
		}



		public String getOrderStatus() {
			return orderStatus;
		}



		public void setOrderStatus(String orderStatus) {
			this.orderStatus = orderStatus;
		}



		public Date getReceiptDate() {
			return receiptDate;
		}



		public void setReceiptDate(Date receiptDate) {
			this.receiptDate = receiptDate;
		}



		public Date getDeliveryDate() {
			return deliveryDate;
		}



		public void setDeliveryDate(Date deliveryDate) {
			this.deliveryDate = deliveryDate;
		}



		public String getAssignedTo() {
			return assignedTo;
		}



		public void setAssignedTo(String assignedTo) {
			this.assignedTo = assignedTo;
		}



		public String getDeliveredBy() {
			return deliveredBy;
		}



		public void setDeliveredBy(String deliveredBy) {
			this.deliveredBy = deliveredBy;
		}



		public String getPaymentStatus() {
			return paymentStatus;
		}



		public void setPaymentStatus(String paymentStatus) {
			this.paymentStatus = paymentStatus;
		}



		public String getPaymentReference() {
			return paymentReference;
		}



		public void setPaymentReference(String paymentReference) {
			this.paymentReference = paymentReference;
		}



		public double getOrderValue() {
			return orderValue;
		}



		public void setOrderValue(double orderValue) {
			this.orderValue = orderValue;
		}



		public String getCreatedBy() {
			return createdBy;
		}



		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}



		public Date getCreatedOn() {
			return createdOn;
		}



		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}



		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public double getDataId() {
			return dataId;
		}

		public void setDataId(double dataId) {
			this.dataId = dataId;
		}

		public String getImportType() {
			return importType;
		}

		public void setImportType(String importType) {
			this.importType = importType;
		}

		public double getRetailerId() {
			return retailerId;
		}

		public void setRetailerId(double retailerId) {
			this.retailerId = retailerId;
		}

		public String getDataStatus() {
			return dataStatus;
		}

		public void setDataStatus(String dataStatus) {
			this.dataStatus = dataStatus;
		}

		public String getOrderCategory() {
			return orderCategory;
		}

		public void setOrderCategory(String orderCategory) {
			this.orderCategory = orderCategory;
		}

		public double getStoreId() {
			return storeId;
		}

		public void setStoreId(double storeId) {
			this.storeId = storeId;
		}

		public String getOrderType() {
			return orderType;
		}

		public void setOrderType(String orderType) {
			this.orderType = orderType;
		}

		public String getDataNote() {
			return dataNote;
		}

		public void setDataNote(String dataNote) {
			this.dataNote = dataNote;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Timestamp getUpdatedOn() {
			return updatedOn;
		}

		public void setUpdatedOn(Timestamp updatedOn) {
			this.updatedOn = updatedOn;
		}





		@Override
		public String toString() {
			return "DataImport [sessionId=" + sessionId + ", dataId=" + dataId
					+ ", importType=" + importType + ", retailerId="
					+ retailerId + ", dataStatus=" + dataStatus
					+ ", orderCategory=" + orderCategory + ", storeId="
					+ storeId + ", orderType=" + orderType + ", dataNote="
					+ dataNote + ", updatedBy=" + updatedBy + ", updatedOn="
					+ updatedOn + ", trackingId=" + trackingId + ", orderId="
					+ orderId + ", orderTitle=" + orderTitle
					+ ", orderDetails=" + orderDetails + ", orderPriority="
					+ orderPriority + ", orderProductId=" + orderProductId
					+ ", activityId=" + activityId + ", revisionId="
					+ revisionId + ", attachmentId=" + attachmentId
					+ ", orderRefno=" + orderRefno + ", customerName="
					+ customerName + ", customerContact=" + customerContact
					+ ", customerEmail=" + customerEmail + ", customerAddress="
					+ customerAddress + ", cityName=" + cityName
					+ ", areaName=" + areaName + ", customerZipcode="
					+ customerZipcode + ", paymentType=" + paymentType
					+ ", bayNumber=" + bayNumber + ", orderStatus="
					+ orderStatus + ", receiptDate=" + receiptDate
					+ ", deliveryDate=" + deliveryDate + ", assignedTo="
					+ assignedTo + ", deliveredBy=" + deliveredBy
					+ ", paymentStatus=" + paymentStatus
					+ ", paymentReference=" + paymentReference
					+ ", orderValue=" + orderValue + ", createdBy=" + createdBy
					+ ", createdOn=" + createdOn + "]";
		}


 
		 
		
		
}
