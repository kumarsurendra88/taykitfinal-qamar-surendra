package com.dataisys.taykit.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;






import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
/*import com.dataisys.pudo.dao.DeliveryRegisterDao;
import com.dataisys.pudo.dao.OrderDao;
import com.dataisys.pudo.dao.PaymentCollectionDao;*/
import com.dataisys.taykit.model.RunsheetDetails;
//import com.dataisys.pudo.model.Denominations;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.PaymentCollection;
import com.dataisys.taykit.model.ShortfallAmount;
import com.dataisys.taykit.model.Store;

@RestController
public class PaymentCollectionController {
	@Autowired
	com.dataisys.taykit.dao.PaymentCollectionDao paymentCollectionDao;
	
	@Autowired
	OrderDao orderDao;
	@Autowired
	PudoStoreDao storeDao;
	@RequestMapping(value="/PaymentCollection/pay",method=RequestMethod.POST,headers="Content-Type=application/json")
	public void payAmount(@RequestBody PaymentCollection entry ,HttpSession session)
	{
		System.out.println(entry);
		String accountNo=(String) session.getAttribute("loggedStore");
		Date date=new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String today=formatter.format(date);
		Store obj=storeDao.getStoreByAccountNo(accountNo);
		entry.setStoreId(obj.getName());
		entry.setDate(today);
		paymentCollectionDao.pay(entry);
    }
	@RequestMapping(value="/GetPaymentsPaidForToday",method=RequestMethod.GET)
	public List<PaymentCollection> getPaymentsPaidForToday(HttpSession session)
	{
		
		String accountNo=(String) session.getAttribute("loggedStore");
		Store obj=storeDao.getStoreByAccountNo(accountNo);
		String storename=obj.getName();
		Date date=new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today=formatter.format(date);
		
		return paymentCollectionDao.getTodaysPayments(storename,today);
    }
	@RequestMapping(value="/ShortFallAmountDetails",method=RequestMethod.POST,headers="Content-Type=application/json")
	public void shortFallAmount(@RequestBody ShortfallAmount details ,HttpSession session)
	{
		System.out.println(details);
		paymentCollectionDao.shortFallAmountDetails(details);
    }
	@RequestMapping(value="/GetShortFallAmount/{runsheetID}",method=RequestMethod.GET)
	public ShortfallAmount getShortFallAmount(@PathVariable("runsheetID") String runsheetId ,HttpSession session)
	{
		
		return paymentCollectionDao.getShoetfallAmountForRunsheet(runsheetId);
	}
	@RequestMapping(value="/getAllDeliveriesForDate/{id},{date}",method=RequestMethod.GET)
	public List<RunsheetDetails> getAllDeliveriesForDate(@PathVariable("id") String id,@PathVariable("date") String date )
	{
		System.out.println(date);
		System.out.println(id);
		List<RunsheetDetails> list= paymentCollectionDao.gettodaysDeliveries(date, id);
		//List<RunsheetDetails> list=new ArrayList<RunsheetDetails>();
		//list.add(obj);
		return list;
		
	}
	@RequestMapping(value="/getOrdersByRunsheet/{id}",method=RequestMethod.GET)
	public List<Order> getOrdersByRunsheet(@PathVariable("id") String id )
	{
		return orderDao.getOrdersByRunsheetId(id);
		
	}
}
