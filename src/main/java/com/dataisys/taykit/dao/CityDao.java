package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.City;

public interface CityDao {

	public void createCity(City city);
	public List<City> getCity();
	

}
