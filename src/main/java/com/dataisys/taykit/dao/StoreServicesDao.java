package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.StoreService;
import com.dataisys.taykit.model.StoreServices;

public interface StoreServicesDao {
public List<StoreServices> getServices();
public void addServices(List<StoreService> services);
public List<StoreService> getStoreServicesById(double id);
public void updateServices(List<StoreService> providedServices, int id);
}
