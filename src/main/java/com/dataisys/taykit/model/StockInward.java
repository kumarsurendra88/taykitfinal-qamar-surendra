package com.dataisys.taykit.model;

public class StockInward {
	private double storeId;
	private String trackingId;
	private String storeName;
	private String orderStatus;
	private String flag;
	public String getFlag() {
		return flag;
	}




	public void setFlag(String flag) {
		this.flag = flag;
	}




	public StockInward(double storeId, String trackingId, String storeName) {
		super();
		this.storeId = storeId;
		this.trackingId = trackingId;
		this.storeName = storeName;
	}
	
	
	
	
	public String getOrderStatus() {
		return orderStatus;
	}




	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}




	public StockInward() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	@Override
	public String toString() {
		return "StockInward [storeId=" + storeId + ", trackingId=" + trackingId
				+ "]";
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	
}
