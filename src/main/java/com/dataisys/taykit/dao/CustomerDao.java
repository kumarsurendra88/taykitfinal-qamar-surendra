package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.Order;


/**
 * @author DataIsys
 *
 */
public interface CustomerDao {

public void createCustomer(Customer customer);
public void deleteCustomer(double id);
public List<Customer> getCustomer();

//Added By Kishore
public Customer getPerticularCustomer(Customer customer);
public void updateCustomerByGpsLocation(Customer customer);
public String updateCustomerByPhoneAddress(Customer object);



}
