package com.dataisys.taykit.mobile.model;

import java.sql.Timestamp;

public class Store {

	private String accountNo;
	private double storeId;
	private double storeServiceId;
	private double storeCategoryId;
	private double storeContactId;
	private String storeType;
	private String storeName;
	private String storeAddress;
	private String storeCity;
	private double pincode;
	private String storeGpsLocation;
	private String storeEmail;
	private String storePhone;
	private String storeOperations;
	private String storeStatus;
	private String updatedBy;
	private Timestamp updatedOn;
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public double getStoreServiceId() {
		return storeServiceId;
	}
	public void setStoreServiceId(double storeServiceId) {
		this.storeServiceId = storeServiceId;
	}
	public double getStoreCategoryId() {
		return storeCategoryId;
	}
	public void setStoreCategoryId(double storeCategoryId) {
		this.storeCategoryId = storeCategoryId;
	}
	public double getStoreContactId() {
		return storeContactId;
	}
	public void setStoreContactId(double storeContactId) {
		this.storeContactId = storeContactId;
	}
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}
	public String getStoreCity() {
		return storeCity;
	}
	public void setStoreCity(String storeCity) {
		this.storeCity = storeCity;
	}
	public double getPincode() {
		return pincode;
	}
	public void setPincode(double pincode) {
		this.pincode = pincode;
	}
	public String getStoreGpsLocation() {
		return storeGpsLocation;
	}
	public void setStoreGpsLocation(String storeGpsLocation) {
		this.storeGpsLocation = storeGpsLocation;
	}
	public String getStoreEmail() {
		return storeEmail;
	}
	public void setStoreEmail(String storeEmail) {
		this.storeEmail = storeEmail;
	}
	public String getStorePhone() {
		return storePhone;
	}
	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}
	public String getStoreOperations() {
		return storeOperations;
	}
	public void setStoreOperations(String storeOperations) {
		this.storeOperations = storeOperations;
	}
	public String getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}	
}
