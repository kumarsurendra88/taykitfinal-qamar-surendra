package com.dataisys.taykit.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.RetailerDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.ProductCategoriesDao;
import com.dataisys.taykit.dao.RetailerWarehouseDao;
import com.dataisys.taykit.factories.ProductCategoryFactory;
import com.dataisys.taykit.factories.RetailerContactFactory;
import com.dataisys.taykit.factories.RetailerServicesFactory;
import com.dataisys.taykit.factories.RetailerWarehouseFactory;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.RetailerWarehouse;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StoreReport;
import com.dataisys.taykit.model.StoreServices;
import com.mysql.jdbc.StringUtils;
@RestController
public class RetailerController {
   @Autowired 
   RetailerDao retailerDao;
	@Autowired
	RetailerServicesDao retailerServiceDao;
	@Autowired
	RetailerContactDao retailerContactsDao;
	@Autowired
	RetailerWarehouseDao retailerWarehouseDao;
	@Autowired
	ProductCategoriesDao categoryServiceDao;
	@Autowired
	OrderDao orderDao;
	
	
	@RequestMapping(value="/getRetailer" , method=RequestMethod.GET)	
	public List<Retailer> getRetailers(){
		return retailerDao.getRetailers();
		
	}
	
	@RequestMapping(value="/addRetailerContact" , method=RequestMethod.POST)	
	public void addRetailerContact(@RequestBody RetailerContact contact){
		 RetailerContactFactory object=RetailerContactFactory.getFactory();
		 object.setContacts(contact);	 
	}
	@RequestMapping(value="/getRetailerById/{id}" , method=RequestMethod.GET)	
	public Retailer addRetailerbyId(@PathVariable("id") double id){
		return retailerDao.getRetailerbyId(id);
	}
	@RequestMapping(value="/addRetailer" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addRetailer(@RequestBody Retailer retailer){
		 System.out.println(retailer);
		 RetailerServicesFactory serviceFactory=RetailerServicesFactory.getFactory();
		 List<RetailerServices>retailerservices=serviceFactory.getServices();
		 ProductCategoryFactory categoryFactory=ProductCategoryFactory.getFactory();
		 List<ProductCategories> categoryServices=categoryFactory.getServices();
		 RetailerContactFactory factory= RetailerContactFactory.getFactory();
		 List<RetailerContact> retailerContacts=factory.getContacts();
		 RetailerWarehouseFactory warehouse= RetailerWarehouseFactory.getFactory();
		 List<RetailerWarehouse> warehouses=warehouse.getWarehouses();
		 retailerDao.AddReatiler(retailer, retailerservices, categoryServices,retailerContacts,warehouses);
		 RetailerServicesFactory.setFactory(null);
		 ProductCategoryFactory.setFactory(null);
		 RetailerContactFactory.setFactory(null);
		 RetailerWarehouseFactory.setFactory(null);
	}
	@RequestMapping(value="/updateRetailerbyid/{id}" ,headers="Content-Type=application/json")	
	public void updateRetailerById(@PathVariable("id") double id,@RequestBody Retailer retailer){
		 System.out.println(retailer);
		 RetailerServicesFactory serviceFactory=RetailerServicesFactory.getFactory();
		 List<RetailerServices>retailerservices=serviceFactory.getServices();
		 ProductCategoryFactory categoryFactory=ProductCategoryFactory.getFactory();
		 List<ProductCategories> categoryServices=categoryFactory.getServices();
		 RetailerContactFactory factory= RetailerContactFactory.getFactory();
		 List<RetailerContact> retailerContacts=factory.getContacts();
		 RetailerWarehouseFactory warehouse= RetailerWarehouseFactory.getFactory();
		 List<RetailerWarehouse> warehouses=warehouse.getWarehouses();
		 retailerDao.updateReatiler(retailer, retailerservices, categoryServices,retailerContacts,warehouses,id);
		 RetailerServicesFactory.setFactory(null);
		 ProductCategoryFactory.setFactory(null);
		 RetailerContactFactory.setFactory(null);
		 RetailerWarehouseFactory.setFactory(null);
	}
	@RequestMapping(value="/addRetailerWarehouse",method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addRetailerWarehouse(@RequestBody RetailerWarehouse warehouse){
        System.out.println(warehouse);
		RetailerWarehouseFactory object=RetailerWarehouseFactory.getFactory();
		object.setContacts(warehouse);
		 
	}
	@RequestMapping(value="/getRetailerContactsById/{id}", method=RequestMethod.GET)	
	public List<RetailerContact> getRetailerContact(@PathVariable("id") double id){
		return retailerContactsDao.getretailerContactsBId(id);
			
	}
	@RequestMapping(value="/getRetailerWarehouseById/{id}", method=RequestMethod.GET)	
	public List<RetailerWarehouse> getRetailerWarehouse(@PathVariable("id") double id){
		return retailerWarehouseDao.getRetailerWarehouseById(id);
			
	}

	@RequestMapping(value="/getRetailerServices" , method=RequestMethod.GET)	
	public List<RetailerServices> getStoreServices(){
		List<RetailerServices> services=retailerServiceDao.getServices();
		
		return services;
		
	}
	@RequestMapping(value="/RetailerServicesAdd" , method=RequestMethod.POST,headers="Content-Type=application/json")	
	public void addRetailerServices(@RequestBody List<RetailerServices> services){
		RetailerServicesFactory factory=RetailerServicesFactory.getFactory();
		factory.setServices(services);
	}
	@RequestMapping(value="/getRetailerAvailableServices/{id}" , method=RequestMethod.GET)	
	public List<RetailerServices> getStoreavailableServices(@PathVariable("id") double id){
		List<RetailerServices> totalservices=retailerServiceDao.getServices();
		List<RetailerService> availableservices=retailerServiceDao.getRetailerServicesById(id);
		Iterator<RetailerService> it=availableservices.iterator();
		while(it.hasNext())
		{  
			RetailerService obj=it.next();
			Double serviceId=obj.getServiceId();
			Iterator<RetailerServices> it2=totalservices.iterator();
			while(it2.hasNext())
			{
				
				
				RetailerServices obj2=it2.next();
				if(obj2.getServiceTypeId()==serviceId)
				{
					
					obj2.setServiceStatus(true);
				}
			}
			
		}
		return totalservices;
		
	}
	@RequestMapping(value="/getRetailerAvailableCategories/{id}" , method=RequestMethod.GET)	
	public List<ProductCategories> getStoreavailableCategoriesServiced(@PathVariable("id") double id){
		List<ProductCategories> totalCatogoriesServiced=categoryServiceDao.getproductCategories();
		List<RetailerCategory> availablecategoriesserviced=categoryServiceDao.getAvailablecategoriesForRetailer(id);
		Iterator<RetailerCategory> it=availablecategoriesserviced.iterator();
		while(it.hasNext())
		{  
			RetailerCategory obj=it.next();
			Double serviceId=obj.getProductCategoryId();
			Iterator<ProductCategories> it2=totalCatogoriesServiced.iterator();
			while(it2.hasNext())
			{
				
				
				ProductCategories obj2=it2.next();
				if(obj2.getCategoryId()==serviceId)
				{
					
					obj2.setCategoryStatus(true);
				}
			}
			
		}
		
		return totalCatogoriesServiced;
		
	}
	//added on 11/2
	
	@RequestMapping(value="/getRetailerNameFromOrderReturnDb", method=RequestMethod.GET)
    public List<Retailer> getRetailerNameFromOrderReturnDb(){
        
        List<Retailer> getRetailerNameFromOrderReturnDb=retailerDao.getRetailerNameFromOrderReturnDb();
        System.out.println("daoImplpl is executed");
        return getRetailerNameFromOrderReturnDb;
        
    }
	
	//added on 2/18/2016
	public String  token(String id){
		//String line = "foo,bar,c;qual=\"baz,blurb\",d;junk=\"quux,syzygy\"";
        String[] tokens = id.split(",");
        System.out.println("tokens is "+tokens+" and size is "+tokens.length);
        StringBuffer sb= new StringBuffer("");
        for(int i=0;i < tokens.length;i++){
        	sb.append("'"+tokens[i]+"',");
        	
        }
        
        
        System.out.println("data after for loop"+sb.substring(0, sb.length()-1));
        return sb.substring(0, sb.length()-1);
		
	}
	@RequestMapping(value="/getTrackingIdMultiple/{id}", method=RequestMethod.GET)
	public List<Order> getMultipleTrackingIdSearch(@PathVariable("id")String id){
		System.out.println("inside getMultipleTrackingIdSearch value is "+id);
		String mainData=token(id);
		System.out.println("data collected before passing to query is "+mainData);
		List<Order> trackingId=orderDao.getMultipleTrackingIdSearch(mainData);
		System.out.println("returbned data is "+trackingId);
		return trackingId;
		
	}
	@RequestMapping(value="/getOrderIdMultiple/{id}", method=RequestMethod.GET)
	public Order getMultipleOrderIdSearch(@PathVariable("id")String id){
		System.out.println("inside getMultipleOrderIdSearch value is "+id);
		Order orderId=orderDao.getMultipleOrderIdSearch(id);
		return orderId;
		
	}
	
}
