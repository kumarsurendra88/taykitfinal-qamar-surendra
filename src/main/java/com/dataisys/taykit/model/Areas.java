package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Areas {

	int area_id;
	String area_name,city_name,area_status;
	
	
	
	public String getArea_status() {
		return area_status;
	}

	public void setArea_status(String area_status) {
		this.area_status = area_status;
	}

	public Areas(){}

	public Areas(int area_id,String area_name,String city_name,String area_status) {
		super();
		this.area_id=area_id;
		this.area_name=area_name;
		this.city_name=city_name;
		this.area_status=area_status;
	}
	
	
	
	
	
	public String getArea_name() {
		return area_name;
	}
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	
	
	public int getArea_id() {
		return area_id;
	}
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}
	
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	
	
	@Override
	public String toString() {
		return "Areas [area_id=" + area_id
				+ ", area_name=" + area_name + ", city_name="
				+ city_name + ",area_status="+area_status+"]";
	
}
}