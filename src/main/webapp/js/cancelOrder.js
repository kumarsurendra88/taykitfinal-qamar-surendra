function cancelOrderController($scope,$http){	
	
	console.log("inside cancel order");
	$scope.dis="";
	$scope.changeOrderStatus;
	
	$http.get('./getRetailer')
	.success(function (data) {
	$scope.myData1 = data;		
	});
	
	$http.get('./getAllCancelledOrders')
	.success(function (data) {
	$scope.myData = data;
	console.log("count of data original is "+data.length)
	//$scope.gridOptions.data = data;
	});
	
	

		console.log("inside grid");
	$scope.gridOptions = {
		    pagingPageSizes: [25, 50, 75],
		    pagingPageSize: 25,
		    enableColumnResizing: true,
		    columnDefs: [
						{ name: 'trackingId',displayName:'Tracking#' },
						{ name: 'orderDetails',displayName:'Detail' },
						{ name: 'customerName',displayName:'Customer Name' },
						{ name: 'customerContact',displayName:'Customer Contact' },
						{name:'customerAddress',displayName:'Customer Address'},
						{name:'orderStatus',displayName:'Status'}
		    ],
		    enableGridMenu: true,
		    enableSelectAll: true,
		    exporterCsvFilename: 'myFile.csv',
		    exporterPdfDefaultStyle: {fontSize: 9},
		    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
		    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
		    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
		    exporterPdfFooter: function ( currentPage, pageCount ) {
		      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
		    },
		    exporterPdfCustomFormatter: function ( docDefinition ) {
		      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
		      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
		      return docDefinition;
		    },
		    exporterPdfOrientation: 'portrait',
		    exporterPdfPageSize: 'LETTER',
		    exporterPdfMaxGridWidth: 500,
		    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		    onRegisterApi: function(gridApi){ 
		      $scope.gridApi = gridApi;
		    }
		    
		  };
	
	$scope.changeStatus = function(dis1){
		
		
		
		
		console.log("inside change status selected is "+dis1.changeStatus);
	    var selectedItems=$scope.gridApi.selection.getSelectedRows();
	    
	    if((selectedItems.length)==0){
			
			alert("select tracking ids")
		}
	    var mySelect = document.getElementById("status");
        var mySelection = mySelect.selectedIndex;
        
	    
	    console.log("selected item is of length"+selectedItems.length);
	    console.log("trackingId is "+selectedItems[0].trackingId)
	    var selectedTrackingId=[];
	    for(i=0;i<selectedItems.length;i++){
	    	selectedTrackingId.push(selectedItems[i].trackingId);
	    	
	    }
	  var dataToPass={trackingIds:selectedTrackingId,status:dis1.changeStatus};
	  console.log("data to pass is "+dataToPass);
	    
	    console.log("after for loop the tracking is array is "+selectedTrackingId+"and length is "+selectedTrackingId.length)
	    
	    if((selectedTrackingId.length)==0||(dis1.changeStatus=="")){
	    	
	    	
	    	alert("Select Both Tracking ID and Status to be changed before proceeding");
	    	
	    }
	    else{
	    	
	    	var r = confirm("Do you want to proceed?Press OK to proceed or Cancel to abort");
	    	if (r == true) {
	    	    x = "You pressed OK!";
	    	    console.log("going to post");
	    	    $http({
					method: 'POST',
					url: './updateCancelledOrderStatus',
					headers: {'Content-Type': 'application/json'},
					//data: $scope.dis
					data: dataToPass
				}).success(function (data,status,headers,config) {
				console.log("status of posting is "+status);
				alert("You changed the order status of selected tracking id(s) successfully !");
				dis1.changeStatus="";
				
				
				});
	    	} else {
	    	    x = "You pressed Cancel!";
	    	    console.log("cancel");
	    	}
	    	
	    	
	    }
	    
	    
	    
	};
	
		  $scope.resetTable=function(dis){
			  console.log("dis.lenght is "+dis.fromDate);
			  $http.get('./getAllCancelledOrders')
				.success(function (data) {
				$scope.myData = data;
				console.log("count of data on reset is  "+data.length)
				$scope.gridOptions.data = data;
				dis.toDate="";
				dis.fromDate="";
				dis.retailerName="";
				dis.status="";				
				});			  
		  }
	$scope.seacrhCancelledOrder=function(dis){
		
		$http({
			method: 'POST',
			url: './getAllCancelledOrdersByData',
			headers: {'Content-Type': 'application/json'},
			//data: $scope.dis
			data: $scope.dis
		}).success(function (data,status,headers,config) {
		console.log(data);
		$scope.status=data;
		console.log(data);
		$scope.myData = data;
		console.log("success function completed");
		$scope.gridOptions.data = data;
		$scope.gridApi.core.refresh();
	
		
		//refresh();
		console.log("length after search is "+$scope.myData.length)
		});
		console.log("inside search cancel "+dis.fromDate+" "+dis.toDate);
	}
		 
		 
	     
	    /* $scope.reset = function() {
				console.log("Inside FuelRegister2Controller's reset()");			
			   location.href = "http://localhost:8080/mytracker/login.do#/fuelRegister";
		    };	
	             */    
};




function pendingRTOController($scope,$http){	
	
	console.log("iniside pendingRTO");
	$scope.dis="";
	$http.get('./getRetailer')
	.success(function (data) {
	$scope.myData1 = data;		
	});
	
	$scope.gridOptions = {
		    pagingPageSizes: [25, 50, 75],
		    pagingPageSize: 25,
		    enableColumnResizing: true,
		    columnDefs: [
						{ name: 'trackingId',displayName:'Tracking#' },
						{ name: 'orderDetails',displayName:'Detail' },
						{ name: 'customerName',displayName:'Customer Name' },
						{ name: 'customerContact',displayName:'Customer Contact' },
						{name:'customerAddress',displayName:'Customer Address'},
						{name:'orderStatus',displayName:'Status'}
		    ],
		    enableGridMenu: true,
		    enableSelectAll: true,
		    exporterCsvFilename: 'myFile.csv',
		    exporterPdfDefaultStyle: {fontSize: 9},
		    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
		    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
		    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
		    exporterPdfFooter: function ( currentPage, pageCount ) {
		      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
		    },
		    exporterPdfCustomFormatter: function ( docDefinition ) {
		      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
		      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
		      return docDefinition;
		    },
		    exporterPdfOrientation: 'portrait',
		    exporterPdfPageSize: 'LETTER',
		    exporterPdfMaxGridWidth: 500,
		    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		    onRegisterApi: function(gridApi){ 
		      $scope.gridApi = gridApi;
		    }
		    
		  };
	

$scope.resetTable=function(dis){
	
	
		var data1;
		$scope.gridOptions.data =data1;
		dis.toDate="";
		dis.fromDate="";
		dis.retailerName="";			
		 
}



$scope.seacrhRTOOrder=function(dis){
	
	$http({
		method: 'POST',
		url: './getAllRTOConfirmedOrders',
		headers: {'Content-Type': 'application/json'},
		//data: $scope.dis
		data: $scope.dis
	}).success(function (data,status,headers,config) {
	console.log(data);
	$scope.status=data;
	console.log(data);
	$scope.myData = data;
	console.log("success function completed");
	$scope.gridOptions.data = data;
	$scope.gridApi.core.refresh();	
	});
	console.log("inside search cancel "+dis.fromDate+" "+dis.toDate);
}


$scope.sendRetailer = function(){
	
    var selectedItems=$scope.gridApi.selection.getSelectedRows();
    
    if((selectedItems.length)==0){
		
		alert("select tracking ids in sendRetailer")
	}     
    console.log("selected item is of length"+selectedItems.length);
    console.log("trackingId is "+selectedItems[0].trackingId)
    var selectedTrackingId=[];
    for(i=0;i<selectedItems.length;i++){
    	selectedTrackingId.push(selectedItems[i].trackingId);
    	
    }
  var dataToPass={trackingIds:selectedTrackingId};
  console.log("data to pass is "+dataToPass);
    
    console.log("after for loop the tracking is array is "+selectedTrackingId+"and length is "+selectedTrackingId.length)
    
    if((selectedTrackingId.length)==0){
    	
    	
    	alert("Select Both Tracking ID and Status to be changed before proceeding");
    	
    }
    else{
    	
    	var r = confirm("Do you want to proceed?Press OK to proceed or Cancel to abort");
    	if (r == true) {
    	    x = "You pressed OK!";
    	    console.log("going to post");
    	    $http({
				method: 'POST',
				url: './updateReferenceNumberForRTO',
				headers: {'Content-Type': 'application/json'},
				//data: $scope.dis
				data: dataToPass
			}).success(function (data,status,headers,config) {
			console.log("status of posting is "+status);
			alert("You changed the order status of selected tracking id(s) successfully !");
			dis1.changeStatus="";
			
			
			});
    	} else {
    	    x = "You pressed Cancel!";
    	    console.log("cancel");
    	}   	   	
    }            
};

	
};



function pendingReturnController($scope,$http){	
	
	console.log("iniside pendingReturn");
	$scope.dis="";
	$http.get('./getRetailer')
	.success(function (data) {
	$scope.myData1 = data;		
	});
	
	$scope.gridOptions = {
		    pagingPageSizes: [25, 50, 75],
		    pagingPageSize: 25,
		    enableColumnResizing: true,
		    columnDefs: [
						{ name: 'trackingId',displayName:'Tracking#' },
						{ name: 'orderDetails',displayName:'Detail' },
						{ name: 'customerName',displayName:'Customer Name' },
						{ name: 'customerContact',displayName:'Customer Contact' },
						{name:'customerAddress',displayName:'Customer Address'},
						{name:'status',displayName:'Status'}
		    ],
		    enableGridMenu: true,
		    enableSelectAll: true,
		    exporterCsvFilename: 'myFile.csv',
		    exporterPdfDefaultStyle: {fontSize: 9},
		    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
		    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
		    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
		    exporterPdfFooter: function ( currentPage, pageCount ) {
		      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
		    },
		    exporterPdfCustomFormatter: function ( docDefinition ) {
		      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
		      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
		      return docDefinition;
		    },
		    exporterPdfOrientation: 'portrait',
		    exporterPdfPageSize: 'LETTER',
		    exporterPdfMaxGridWidth: 500,
		    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		    onRegisterApi: function(gridApi){ 
		      $scope.gridApi = gridApi;
		    }
		    
		  };
	

$scope.resetTable=function(dis){
	
	
		var data1;
		$scope.gridOptions.data =data1;
		dis.toDate="";
		dis.fromDate="";
		dis.retailerName="";			
		 
}



$scope.seacrhRTOOrder=function(dis){
	
	$http({
		method: 'POST',
		url: './getAllPendingReturns',
		headers: {'Content-Type': 'application/json'},
		//data: $scope.dis
		data: $scope.dis
	}).success(function (data,status,headers,config) {
	console.log(data);
	$scope.status=data;
	console.log(data);
	$scope.myData = data;
	console.log("success function completed");
	$scope.gridOptions.data = data;
	$scope.gridApi.core.refresh();	
	});
	console.log("inside search cancel "+dis.fromDate+" "+dis.toDate);
}


$scope.sendRetailer = function(){
	
    var selectedItems=$scope.gridApi.selection.getSelectedRows();
    
    if((selectedItems.length)==0){
		
		alert("select tracking ids in sendRetailer")
	}     
    console.log("selected item is of length"+selectedItems.length);
    console.log("trackingId is "+selectedItems[0].trackingId)
    var selectedTrackingId=[];
    for(i=0;i<selectedItems.length;i++){
    	selectedTrackingId.push(selectedItems[i].trackingId);
    	
    }
  var dataToPass={trackingIds:selectedTrackingId};
  console.log("data to pass is "+dataToPass);
    
    console.log("after for loop the tracking is array is "+selectedTrackingId+"and length is "+selectedTrackingId.length)
    
    if((selectedTrackingId.length)==0){
    	
    	
    	alert("Select Both Tracking ID and Status to be changed before proceeding");
    	
    }
    else{
    	
    	var r = confirm("Do you want to proceed?Press OK to proceed or Cancel to abort");
    	if (r == true) {
    	    x = "You pressed OK!";
    	    console.log("going to post");
    	    $http({
				method: 'POST',
				url: './updateReferenceNumberForReturn',
				headers: {'Content-Type': 'application/json'},
				//data: $scope.dis
				data: dataToPass
			}).success(function (data,status,headers,config) {
			console.log("status of posting is "+status);
			alert("You changed the order status of selected tracking id(s) successfully !");
			dis1.changeStatus="";
			
			
			});
    	} else {
    	    x = "You pressed Cancel!";
    	    console.log("cancel");
    	}   	   	
    }            
};

	
};

routerApp.service('returnUpload', ['$http' ,'$location' , function ($http , $location ) {
    this.uploadFileToUrl = function(file, uploadUrl){
    	console.log("upload url is "+uploadUrl+" and file name to be uploaded  is "+file);
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	
        	console.log("hi... file uploaded successfully");
        	alert("file uploaded successfully");
        })
        .error(function(){
        });
        
        
        
    }
}]);
routerApp.controller('rtoDispatchController', ['$scope','$http','$location', 'returnUpload', function($scope,$http, $location, returnUpload){
	
	
	console.log("inside rtoDispatchController ")
		
	
	
	$http.get('./getRetailer')
	.success(function (data) {
	$scope.myData1 = data;		
	});
	
	$scope.gridOptions = {
		    pagingPageSizes: [25, 50, 75],
		    pagingPageSize: 25,
		    enableColumnResizing: true,
		    columnDefs: [
						{ name: 'trackingId',displayName:'Tracking#' },
						{ name: 'orderDetails',displayName:'Detail' },
						{ name: 'customerName',displayName:'Customer Name' },
						{ name: 'customerContact',displayName:'Customer Contact' },
						{name:'customerAddress',displayName:'Customer Address'},
						{name:'orderStatus',displayName:'Status'},
						
						{name:'cancelledOrderRef',displayName:'Order Refrernce Number'}
		    ],
		    enableGridMenu: true,
		    enableSelectAll: true,
		    exporterCsvFilename: 'myFile.csv',
		    exporterPdfDefaultStyle: {fontSize: 9},
		    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
		    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
		    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
		    exporterPdfFooter: function ( currentPage, pageCount ) {
		      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
		    },
		    exporterPdfCustomFormatter: function ( docDefinition ) {
		      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
		      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
		      return docDefinition;
		    },
		    exporterPdfOrientation: 'portrait',
		    exporterPdfPageSize: 'LETTER',
		    exporterPdfMaxGridWidth: 500,
		    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		    onRegisterApi: function(gridApi){ 
		      $scope.gridApi = gridApi;
		    }
		    
		  };
	
	
	
	$scope.searchRtoDispatch=function(dis){
		
		$http({
			method: 'POST',
			url: './getAllRTODispatchedOrdersWithRefNo',
			headers: {'Content-Type': 'application/json'},
			//data: $scope.dis
			data: $scope.dis
		}).success(function (data,status,headers,config) {
		console.log(data);
		$scope.status=data;
		console.log(data);
		$scope.myData = data;
		console.log("success function completed");
		$scope.gridOptions.data = data;
		$scope.gridApi.core.refresh();	
		});
		console.log("inside search cancel "+dis.fromDate+" "+dis.toDate);
	}
	
	$scope.uploadPOR = function(){
		  /*if($scope.retailerName==null){
			  alert("Please select a Retailer..!!")
			  document.getElementById("retailer").focus();
		  }else if($scope.myFile==null){			 
			  alert("Please select a file to upload..!!")
			  document.getElementById("uploadFile").focus();		  
		  } else{
			  	console.log("File name is"+$scope.myFile)
		        var file = $scope.myFile;
			  	var retailer=$scope.retailerName;
			  	retailerFactory.setRetailer(retailer)
		        console.log('file is ' + retailer);
		        var uploadUrl = "./importData";
		        fileUpload.uploadFileToUrl(file, uploadUrl);
		  }	*/	  
		
		
		console.log("File name is"+$scope.myFile)
      var file = $scope.myFile;
      var uploadUrl = "./uploadPor";
      returnUpload.uploadFileToUrl(file, uploadUrl);
	    };
}]);

routerApp.controller('pendingReturn2Controller', ['$scope','$http','$location', 'returnUpload', function($scope,$http, $location, returnUpload){
	
	
	$http.get('./getRetailer')
	.success(function (data) {
	$scope.myData1 = data;		
	});
	
	console.log("inside pendingReturn2Controller ")
	$scope.gridOptions = {
		    pagingPageSizes: [25, 50, 75],
		    pagingPageSize: 25,
		    enableColumnResizing: true,
		    columnDefs: [
						{ name: 'trackingId',displayName:'Tracking#' },
						{ name: 'orderDetails',displayName:'Detail' },
						{ name: 'customerName',displayName:'Customer Name' },
						{ name: 'customerContact',displayName:'Customer Contact' },
						{name:'customerAddress',displayName:'Customer Address'},
						{name:'status',displayName:'Status'},
						
						{name:'returnOrderRefNo',displayName:'Return Order RefNo'}
		    ],
		    enableGridMenu: true,
		    enableSelectAll: true,
		    exporterCsvFilename: 'myFile.csv',
		    exporterPdfDefaultStyle: {fontSize: 9},
		    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
		    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
		    exporterPdfHeader: { text: "My Tracker", style: 'headerStyle' },
		    exporterPdfFooter: function ( currentPage, pageCount ) {
		      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
		    },
		    exporterPdfCustomFormatter: function ( docDefinition ) {
		      docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
		      docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
		      return docDefinition;
		    },
		    exporterPdfOrientation: 'portrait',
		    exporterPdfPageSize: 'LETTER',
		    exporterPdfMaxGridWidth: 500,
		    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		    onRegisterApi: function(gridApi){ 
		      $scope.gridApi = gridApi;
		    }
		    
		  };
	
	
	$scope.seacrhRTOOrder=function(dis){
		console.log("inside post");
		$http({
			method: 'POST',
			url: './getAllReturnDispatchedOrdersWithRefNo',
			headers: {'Content-Type': 'application/json'},
			//data: $scope.dis
			data: $scope.dis
		}).success(function (data,status,headers,config) {
		console.log(data);
		$scope.status=data;
		console.log(data);
		$scope.myData = data;
		console.log("success function completed");
		$scope.gridOptions.data = data;
		$scope.gridApi.core.refresh();	
		});
		console.log("inside search cancel "+dis.fromDate+" "+dis.toDate);
	}
	
	$scope.uploadPOR = function(){
		  /*if($scope.retailerName==null){
			  alert("Please select a Retailer..!!")
			  document.getElementById("retailer").focus();
		  }else if($scope.myFile==null){			 
			  alert("Please select a file to upload..!!")
			  document.getElementById("uploadFile").focus();		  
		  } else{
			  	console.log("File name is"+$scope.myFile)
		        var file = $scope.myFile;
			  	var retailer=$scope.retailerName;
			  	retailerFactory.setRetailer(retailer)
		        console.log('file is ' + retailer);
		        var uploadUrl = "./importData";
		        fileUpload.uploadFileToUrl(file, uploadUrl);
		  }	*/	  
		
		
		console.log("File name is"+$scope.myFile)
        var file = $scope.myFile;
        var uploadUrl = "./uploadPor";
        returnUpload.uploadFileToUrl(file, uploadUrl);
	    };
}]);

routerApp.factory('TrackindIdFactory', function() {
	var data = [ {
		trackingId : 0
	} ];
	return {
		getTrackingId : function() {
			return data.trackingId;
		},
		setTrackingId : function(track) {
			data.trackingId = track;

		}
	};
});
routerApp.factory('updateFactory',function() {
	var oderId;
	var orderTitle;
	return {
			setId:function (id) {
				console.log("Inside setId() of updateFactory and the Id is: "+id);
				oderId = id;
			},
			getId:function() {
				console.log("Inside getId() of updateFactory and the Id is: "+oderId);
				return oderId;
			}
	};
});
routerApp.service('StatusReasons', [function () {

    // private
    var reasons= [];

    this.setStatuses = function(newObj) {
    	reasons.push(newObj);
    	console.log(reasons);
       return [].concat(reasons);
    };
    this.getStatuses = function(){
        // will return a copy of the personArray at the time of request
    	console.log("in service get"+reasons);
        return [].concat(reasons);
    };    
    this.clear = function(){
    	reasons = [];
        return [].concat(reasons);
    };

}]);
routerApp.factory('Reason', function() {
	var data = [ {
		reason : ''
	} ];
	return {
		getReason : function() {
			return data.reason;
		},
		setReason : function(track) {
			data.reason = track;

		}
	};
});
routerApp.controller('agGridController', ['$scope','$http','$location','TrackindIdFactory','updateFactory','StatusReasons','Reason',function($scope,$http,$location,TrackindIdFactory,updateFactory,StatusReasons,Reason){
	console.log("inside aggrid controller");
	
	$http.get("./getAllOrders")	
	.then(function(res){
		allOfTheData = res.data;
        createNewDatasource();

        });
	$scope.pageSize = '500';
	 $scope.onPageSizeChanged = function() {
	        createNewDatasource();
	    };

	 function createNewDatasource() {
	        if (!allOfTheData) {
	            // in case user selected 'onPageSizeChanged()' before the json was loaded
	            return;
	        }
	        
	        var dataSource = {
                    //rowCount: 2, //- not setting the row count, infinite paging will be used
                    pageSize: parseInt($scope.pageSize), // changing to number, as scope keeps it as a string
                    getRows: function (params) {
                        // this code should contact the server for rows. however for the purposes of the demo,
                        // the data is generated locally, a timer is used to give the experience of
                        // an asynchronous call
                        console.log('asking for ' + params.startRow + ' to ' + params.endRow);
                        setTimeout( function() {
                            // take a chunk of the array, matching the start and finish times
                            var rowsThisPage = allOfTheData.slice(params.startRow, params.endRow);
                            // see if we have come to the last page. if we have, set lastRow to
                            // the very last row of the last page. if you are getting data from
                            // a server, lastRow could be returned separately if the lastRow
                            // is not in the current page.
                            var lastRow = -1;
                            if (allOfTheData.length <= params.endRow) {
                                lastRow = allOfTheData.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };

                $scope.gridOptions.api.setDatasource(dataSource);
}

	var columnDefs = [
	                  {headerName: "trackingId", field: "trackingId",template:'<a ng-href="" ng-click="edit(data.trackingId)">{{data.trackingId}}</a>'},
	                  {headerName: "orderId", field: "orderId"},
	                  {headerName: "customerName", field: "customerName"}
	              ];


	

	    var allOfTheData;

	              $scope.gridOptions = {
	            		  angularCompileRows: true,
	                  columnDefs: columnDefs,
	                  enableFilter: true,
	                  rowData: null

	                
	              };
	          	$scope.edit=function(row)
	          	{
	          		console.log("inside edit function and data received is "+row);
	          		TrackindIdFactory.setTrackingId(row);
	          		$location.path('updateorder');
	          	}
	          	
	    
	            

}]);

routerApp.directive('pageSelect', function() {
  return {
    restrict: 'E',
    template: '<input type="text" class="select-page" ng-model="inputPage" ng-change="selectPage(inputPage)">',
    link: function(scope, element, attrs) {
      scope.$watch('currentPage', function(c) {
        scope.inputPage = c;
      });
    }
  }
});

            
routerApp.controller('stController',['$scope','$http','$filter',function($scope,$http,$filter){
console.log("inside st table controller");
/*$http.get('./getAllOrders')
.success(function(data) {
	console.log("inside call"+data);
	  $scope.rowCollection=data;
});*/

$scope.rowCollection = [
                       {trackingId: 'Laurent', orderId: 'Renard', customerName: new Date('1987-05-21'), customerEmail: 102, storeName: 'whatever@gmail.com'},
                       {trackingId: 'Blandine', orderId: 'Faivre', customerName: new Date('1987-04-25'), customerEmail: -2323.22, email: 'oufblandou@gmail.com'},
                       {trackingId: 'Francoise', orderId: 'Frere', customerName: new Date('1955-08-27'), customerEmail: 42343, email: 'raymondef@gmail.com'}
                   ];


	
}])
  .directive('stRatio',function(){
        return {
          link:function(scope, element, attr){
            var ratio=+(attr.stRatio);
            
            element.css('width',ratio+'%');
            
          }
        };
    });

routerApp.controller('updateCustomerController',['$scope','$http','$location',function($scope,$http,$location){

	console.log("inside updateCustomerController");
	
	$scope.dis;
	
	$scope.updateOrder=function(dis){
		console.log("inside the update function and id received is "+dis.value+":"+dis.id+" phoneNumber is "+dis.phoneNumber+" customerAddress is "+dis.customerAddress)
		
		if(document.getElementById("sel1").value.length == 0){
			alert("Warning:Tracking/Order Id can not be empty!")
		}
		else{
		if((document.getElementById("customerAddress").value.length == 0) && (document.getElementById("customerMobile").value.length == 0)){
			alert("Warning:Both Customer Address and Phone Number Can not be empty")
		}
		else{
		$http({
			method: 'POST',
			url: './updateCustomerByPhoneAddress',
			headers: {'Content-Type': 'application/json'},
			
			data: $scope.dis
		}).success(function (data,status,headers,config) {
		
		console.log(data)
		if(data=="2"){
			console.log("inside if 0")
			alert("Warning:Select whether Order/Tracking Id")
		}
		if(data=="0"){
			console.log("inside if 0")
			alert("Warning:Enter a valid tracking/order Id")
		}
	     	if(data=="1"){
	     		console.log("inside 1")
	     		alert("Updated Successfully")
	     		
	     		
	     		
	     		
	     	}
		});
		}
	}
	}

	
}])


routerApp.controller('storeReportSummaryController',['$scope','$http',function($scope,$http){
	alert("inside storeReportSummaryController in cancel.js");
	
}]);

