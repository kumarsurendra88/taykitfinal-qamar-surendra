package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.CustomerHistory;

public interface CustomerHistoryDao {

	public void createCustomerHistory(CustomerHistory customerHistory);
	public void updateCustomerHistory(CustomerHistory customerHistory);
	public List<CustomerHistory> getAllCustomerHistory();
	public List<CustomerHistory> getCustomerHistoryByPhoneNumber(String contactNumber);
	
}
