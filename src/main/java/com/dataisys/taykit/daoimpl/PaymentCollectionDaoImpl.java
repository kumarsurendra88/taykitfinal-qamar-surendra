package com.dataisys.taykit.daoimpl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.PaymentCollectionRowMapper;
import com.dataisys.taykit.rowmapper.RunsheetRowMapper;
import com.dataisys.taykit.rowmapper.ShortfallRowmapper;
import com.dataisys.taykit.dao.PaymentCollectionDao;
import com.dataisys.taykit.model.RunsheetDetails;
import com.dataisys.taykit.model.ShortfallAmount;
import com.dataisys.taykit.model.PaymentCollection;

public class PaymentCollectionDaoImpl extends JdbcDaoSupport implements PaymentCollectionDao {

	public List getCashEntries(int userid) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<RunsheetDetails> gettodaysDeliveries(String date, String id) {
		
		String sql2="SELECT * from `runsheet` where delivery_user='"+id+"' AND date='"+date+"'";
		List<RunsheetDetails> deliveryDetails =getJdbcTemplate().query(sql2, new RunsheetRowMapper());
		
		Iterator<RunsheetDetails> it=deliveryDetails.iterator();
		while(it.hasNext())
		{
			RunsheetDetails obj= it.next();
		String sql5="SELECT COUNT(*) FROM `order` where assigned_to='"+id+"' AND delivery_boy_assign_date='"+date+"' AND runsheet_number='"+obj.getRunSheetNO()+"' AND payment_type='prepaid' And order_status='Delivered'";
		int totalPrepaidDelivered=getJdbcTemplate().queryForInt(sql5);
		String sql6="SELECT COUNT(*) FROM `order` where assigned_to='"+id+"' AND delivery_boy_assign_date='"+date+"' AND runsheet_number='"+obj.getRunSheetNO()+"' AND payment_type='COD' And order_status='Delivered'";
		int totalCodDelivered=getJdbcTemplate().queryForInt(sql6);
		
		
		String totalCOD="SELECT COUNT(payment_type)FROM `order` WHERE payment_type='COD' AND assigned_to='"+id+"' AND delivery_boy_assign_date='"+date+"' AND runsheet_number='"+obj.getRunSheetNO()+"'";
		int totalCod=getJdbcTemplate().queryForInt(totalCOD);
		
		String codValue="SELECT SUM(order_value)FROM `order` WHERE payment_type='COD' AND order_status='Delivered' AND assigned_to='"+id+"' AND delivery_boy_assign_date='"+date+"' AND runsheet_number='"+obj.getRunSheetNO()+"'";
		double codCollected= ((double) getJdbcTemplate().queryForInt(codValue));
		
		
		
		
		
		
		ShortfallAmount shortfallDetails = new ShortfallAmount();
		try{
		String sql1="SELECT * from `shortfall_amount` where runsheet_no='"+obj.getRunSheetNO()+"'";
		shortfallDetails=getJdbcTemplate().queryForObject(sql1, new ShortfallRowmapper());
		
		}catch(IncorrectResultSizeDataAccessException e)
		{
			shortfallDetails.setReceipt("0");;
			shortfallDetails.setShortFallAmount(0.0);
		}
		obj.setShortFallValue(shortfallDetails.getShortFallAmount());
		obj.setTotalCodDelivered(totalCodDelivered);
		obj.setTotalPrepaidDelivered(totalPrepaidDelivered);
		obj.setTotalCod(totalCod);
		obj.setCodCollected(codCollected);
		System.out.println("value of codCollected"+codCollected);
		}
		//deliveryDetails.setShortFallReciept(shortfallDetails.getReceipt());
		
		
		
		return deliveryDetails;
	}

	public int pay(PaymentCollection entry) {
		String sql="insert into `paymentdetails`(runsheet_no,cod_tocollect,Cod_paid,shortfall_amount,delivery_user,card_payment,store_id,date) values(?,?,?,?,?,?,?,?)  ";
		getJdbcTemplate().update(sql, entry.getRunsheetNo(),entry.getCodToCollect(),entry.getCodPaid(),entry.getShortFallAmount(),entry.getDeliveryUser(),entry.getCardPayment(),entry.getStoreId(),entry.getDate());
		String sql1="update runsheet set cod_status=? where runsheet_no=?";
		getJdbcTemplate().update(sql1, "Close",entry.getRunsheetNo());
		return 0;
	}

	public void shortFallAmountDetails(ShortfallAmount details) {
		String sql="insert into `shortfall_amount`(runsheet_no,delivery_user,tracking_id,shortfall_amount,description,receipt) values(?,?,?,?,?,?)  ";
		getJdbcTemplate().update(sql, details.getRunsheetNo(),details.getDeliveryUser(),details.getTrackingId(),details.getShortFallAmount(),details.getDescription(),details.getReceipt());
	}

	public ShortfallAmount getShoetfallAmountForRunsheet(String runsheetId) {
		double shortAmount = 0;
		try{
		String sql2="SELECT shortfall_amount from `shortfall_amount` where runsheet_no='"+runsheetId+"'";
		shortAmount=getJdbcTemplate().queryForObject(sql2, Double.class);
		}
		catch(IncorrectResultSizeDataAccessException e){
			
		}
		ShortfallAmount shortAmountObj=new ShortfallAmount();
		shortAmountObj.setShortFallAmount(shortAmount);
		return shortAmountObj;
	}

	public List<PaymentCollection> getTodaysPayments(String storename, String date) {
		String sql="select * from `paymentdetails` where store_id='"+storename+"' And date='"+date+"'";
		return getJdbcTemplate().query(sql, new PaymentCollectionRowMapper());
	}

	
	
}
