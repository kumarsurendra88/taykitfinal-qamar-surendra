package com.dataisys.taykit.model;

import java.util.List;

public class StoreContact {

	private double contactId;
	private double storeId;
	private String name;
	private String designation;
	private String contactNo;
	private String email;
	private String notes;
	private List<StoreContact> contacts;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getContactNo() {
		return contactNo;
	}
	public StoreContact() {
		super();
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public StoreContact(String name, String designation, String contactNo,
			String email, String notes, List<StoreContact> contacts) {
		super();
		this.name = name;
		this.designation = designation;
		this.contactNo = contactNo;
		this.email = email;
		this.notes = notes;
		this.contacts = contacts;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Override
	public String toString() {
		return "StoreContact [name=" + name + ", designation=" + designation
				+ ", contactNo=" + contactNo + ", email=" + email + ", Notes="
				+ notes + "]";
	}
	public List<StoreContact> getCurrentContacts(){
		
		
		return contacts;
		
	}
	public void setCurrentContacts(StoreContact contact){
		contacts.add(contact);
	}
	public double getContactId() {
		return contactId;
	}
	public void setContactId(double contactId) {
		this.contactId = contactId;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId2) {
		this.storeId = storeId2;
	}
}
