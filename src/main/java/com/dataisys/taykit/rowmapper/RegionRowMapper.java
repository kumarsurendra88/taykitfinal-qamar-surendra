package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Region;

public class RegionRowMapper implements RowMapper<Region>
{

	public Region mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
	
		Region region=new Region();
		region.setRegion_id(rs.getInt("region_id"));
		region.setZone_id(rs.getInt("zone_id"));
		region.setRegion_name(rs.getString("region_name"));
		region.setRegion_desc(rs.getString("region_desc"));
	
	
	return region;
	
}
}