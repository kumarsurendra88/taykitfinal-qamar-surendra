package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.RetailerAvailableServicesRowMapper;
import com.dataisys.taykit.rowmapper.RetailerTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.RetailerWarehouseRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.RetailerWarehouse;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class RetailerServiceDaoImpl extends JdbcDaoSupport implements RetailerServicesDao {
	public List<RetailerServices> getServices() {

				String sql= "SELECT * FROM `retailer_service_type`";
				
				List<RetailerServices> services=getJdbcTemplate().query(sql,new RetailerTotalServiceRowMapper());
				return services;
		
	}

	public void setRetailerService(final List<RetailerService> services) {
		
		
		  String SQL = "insert into retailer_service(retailer_id, service_id) values (?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   RetailerService detail = services.get(i);
		   ps.setDouble(1, detail.getRetailerId());
		   ps.setDouble(2, detail.getServiceId());
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return services.size();
		      
		                  }


		                });
        return;
	}

	public List<RetailerService> getRetailerServicesById(double id) {
		String sql="select * from `retailer_service` where retailer_id=?";
		List<RetailerService>contacts=getJdbcTemplate().query(sql, new Object[]{id}, new RetailerAvailableServicesRowMapper());
		return contacts;
	}

	public void updateRetailerService(List<RetailerService> providedServices,
			double id) {
		String sql="delete from `retailer_service` where retailer_id="+id+"";
		getJdbcTemplate().execute(sql);
		setRetailerService(providedServices);
		
	}

}
