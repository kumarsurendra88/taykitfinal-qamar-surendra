package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.OrderAssignmentDAO;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderAssign;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.OrderAssignMapper;
import com.dataisys.taykit.rowmapper.UserMapper;

public class OrderAssignmentDAOImpl extends JdbcDaoSupport implements
		OrderAssignmentDAO {

	public void assignOrder(String assignTo, List<Order> orders) {
		
		String sql = "INSERT INTO order_assign(assign_to, order_id)"+
					 "VALUES (?,?)";
		for (Order order : orders) {			
			getJdbcTemplate().update(sql,assignTo,order.getOrderId());
		}
	}

	public List<OrderAssign> getOrdersFromAssignByDbId(String assignTo) {
		String sql = "SELECT * FROM `order_assign` WHERE assign_to = ?";
		List<OrderAssign> listOfAssignedOrders = getJdbcTemplate().query(sql,new Object[]{assignTo},new OrderAssignMapper());
		return listOfAssignedOrders;
	}

	public List<OrderAssign> getAllOrdersFromOrderAssignment() {
		// TODO Auto-generated method stub
		return null;
	}

}
