package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.Module;
import com.dataisys.taykit.model.User;

public interface UserDao {


		public boolean createUser(User user,HttpSession session);
		
		public List<User> getUSers();
		
		public User getUserByCredentials(String username, String password,String accNo);
		
		public void updateUser(User user);
		public User getUserById(double userId);
		public List<User> getAllUsers();
		public List<User> getAllUsersByType(String userType);

		//recently added by kishore
		public User getDeliveryBoyByName(String name);
		public User getUserByAccNumber(String accountNumber);
		public Module getModuleDetails(String moduleName);

}
