package com.dataisys.taykit.model;

import java.util.ArrayList;

public class OrderReturnForMultipleProducts {
	
	private String trackingId;
	private String invoiceNumber;
	private String orderTitle;
	private String returnNote;
	private String customerName;
	private String contactNumber;
	private String emailId;
	private ArrayList<ProductForm> products;
	
	
	public ArrayList<ProductForm> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<ProductForm> products) {
		this.products = products;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getOrderTitle() {
		return orderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}
	public String getReturnNote() {
		return returnNote;
	}
	public void setReturnNote(String returnNote) {
		this.returnNote = returnNote;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}	

}
