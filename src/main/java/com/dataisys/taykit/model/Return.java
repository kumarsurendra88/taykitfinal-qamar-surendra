package com.dataisys.taykit.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Return {

	private String returnId;
	private String invoiceNo;
	private String trackingId;
	private String orderId;
	private String storeName;
	private String location;
	public String getStoreName() {
		return storeName;
	}


	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}
	private String courierService;
	private String consigneeName;
	private String city;
	private String state;
	private String country;
	private String excelAddress;
	private String phone;
	private String paymentMode;
	private String productToBeShipped;
	private String reverseId;
	private double retailerId;
	private Date returnDate;
	private String category;
	private String details;
	private String customerName;
	private String customerAddress;
	private String status;
	private int pincode;
	private String contactNo;
	private String emailId;
	private String approved;
	private String productCode;
	private int quantity;
	private double orderValue;
	private String updateBy;
	private Timestamp updatedOn;
	private double weight;
	private String assignedTo;
	private String runSheetNo;
	private String runSheetStatus;
	private String storeRunsheet;
	private String statusReason;
	private Date deliveryDate;
	private Date deliveredDate;
	
	private double storeId;	
	
	private double reportingStoreId;
	
	private String orderType;
	
	private String returnOrderRefNo;
	
	
	
	
	public String getReturnOrderRefNo() {
		return returnOrderRefNo;
	}


	public void setReturnOrderRefNo(String returnOrderRefNo) {
		this.returnOrderRefNo = returnOrderRefNo;
	}


	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


	public double getReportingStoreId() {
		return reportingStoreId;
	}


	public void setReportingStoreId(double reportingStoreId) {
		this.reportingStoreId = reportingStoreId;
	}


	public Date getDeliveredDate() {
		return deliveredDate;
	}


	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}


	public double getStoreId() {
		return storeId;
	}


	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}


	public Date getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public String getStatusReason() {
		return statusReason;
	}


	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}


	public String getRunSheetNo() {
		return runSheetNo;
	}


	public void setRunSheetNo(String runSheetNo) {
		this.runSheetNo = runSheetNo;
	}


	public String getRunSheetStatus() {
		return runSheetStatus;
	}


	public void setRunSheetStatus(String runSheetStatus) {
		this.runSheetStatus = runSheetStatus;
	}


	public String getStoreRunsheet() {
		return storeRunsheet;
	}


	public void setStoreRunsheet(String storeRunsheet) {
		this.storeRunsheet = storeRunsheet;
	}


	public String getAssignedTo() {
		return assignedTo;
	}


	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}


	public Return(){}
	
	
	public Return(String returnId, String trackingId, String reverseId,
			double retailerId, Date returnDate, String category,
			String details, String customerName, String customerAddress,
			int pincode, String contactNo, String emailId, String approved,
			String updateBy, Timestamp updatedOn) {
		super();
		this.returnId = returnId;
		this.trackingId = trackingId;
		this.reverseId = reverseId;
		this.retailerId = retailerId;
		this.returnDate = returnDate;
		this.category = category;
		this.details = details;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.pincode = pincode;
		this.contactNo = contactNo;
		this.emailId = emailId;
		this.approved = approved;
		this.updateBy = updateBy;
		this.updatedOn = updatedOn;
	}
	
	
	
	
	
	
	
	
	public double getWeight() {
		return weight;
	}


	public void setWeight(double weight) {
		this.weight = weight;
	}


	public String getCourierService() {
		return courierService;
	}


	public void setCourierService(String courierService) {
		this.courierService = courierService;
	}


	public String getConsigneeName() {
		return consigneeName;
	}


	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getExcelAddress() {
		return excelAddress;
	}


	public void setExcelAddress(String excelAddress) {
		this.excelAddress = excelAddress;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPaymentMode() {
		return paymentMode;
	}


	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}


	public String getProductToBeShipped() {
		return productToBeShipped;
	}


	public void setProductToBeShipped(String productToBeShipped) {
		this.productToBeShipped = productToBeShipped;
	}


	public String getProductCode() {
		return productCode;
	}






	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}






	public int getQuantity() {
		return quantity;
	}






	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}






	public double getOrderValue() {
		return orderValue;
	}






	public void setOrderValue(double orderValue) {
		this.orderValue = orderValue;
	}






	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getInvoiceNo() {
		return invoiceNo;
	}



	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}



	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getReturnId() {
		return returnId;
	}
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getReverseId() {
		return reverseId;
	}
	public void setReverseId(String reverseId) {
		this.reverseId = reverseId;
	}
	public double getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(double retailerId) {
		this.retailerId = retailerId;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getApproved() {
		return approved;
	}
	public void setApproved(String approved) {
		this.approved = approved;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	


}
