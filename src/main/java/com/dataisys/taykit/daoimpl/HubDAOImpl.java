package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.HubDAO;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.MasterBag;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.RequiredBeans;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.mobile.model.Store;
import com.dataisys.taykit.rowmapper.HubRowMapper;
import com.dataisys.taykit.rowmapper.RegionRowMapper;
import com.dataisys.taykit.rowmapper.StoreMapper;
import com.dataisys.taykit.rowmapper.StoreRowMapper;

public class HubDAOImpl extends JdbcDaoSupport implements HubDAO{

	public boolean CreatHub(int region_id, String hub_name, String hub_desc) {
		boolean flag=false;

		System.out.println("region_id : "+region_id);
		System.out.println("hub_desc : "+hub_desc);
		System.out.println("hub_name : "+hub_name);
		Date today = new Date(); 
		
		System.out.println("today date is" +today);
		
		System.out.println("running scheduler" );
		
		Calendar cal = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
				
		System.out.println("after converting"+dateFormat.format(today));
		
		String sql="insert into hubs(region_id,hub_name,hub_desc) values(?,?,?)";
	//	getJdbcTemplate().update(sql,dateFormat.format(today));
		int i=getJdbcTemplate().update(sql,region_id,hub_name,hub_desc);
		System.out.println("value of i : "+i);
		if(i>0)
		{
			flag=true;
			System.out.println("hub Inserted");
		}
		
		return flag;
	}

	public List<Hub> getHubByRegionName(String region_name) {
		System.out.println("***********zone_id : "+region_name);
		String sql="select * from hubs where region_id=(select region_id from regions where region_name=?) ";
		List<Hub> ListOfHubNames=getJdbcTemplate().query(sql,new Object[] {region_name}, new HubRowMapper());
				
			
		System.out.println("******************: "+ListOfHubNames);
		
		return ListOfHubNames;
	}

	public List<Hub> getHubs() {


		String sql="SELECT hb.hub_id,hb.hub_name,hb.hub_desc,reg.region_name FROM `hubs` hb INNER JOIN regions reg ON reg.region_id=hb.region_id";

			System.out.println(sql);
return getJdbcTemplate().query(sql,new ResultSetExtractor<List<Hub>>(){

public List<Hub> extractData(ResultSet rs) throws SQLException,
		DataAccessException {
System.out.println("inside");
	List<Hub> list=new ArrayList<Hub>(); 
	System.out.println("inside");
    while(rs.next()){  
    	Hub e=new Hub();  
   //  e.setActivityDate(rs.getDate("activityDate"));
    e.setHub_id(rs.getDouble("hub_id"));
    e.setHub_name(rs.getString("hub_name"));
    e.setHub_desc(rs.getString("hub_desc"));
    e.setRegion_name(rs.getString("region_name"));
    list.add(e);  
    System.out.println("inside while");
    }  
    return list;
}

});
	}

	public void updateHub(Hub hub, double id) {
		String SQL = "update hubs set hub_name=?,region_id=(select region_id from regions where region_name=?),hub_desc =? where hub_id=?";
		System.out.println("data in impl : "+hub.getHub_name()+" : "+hub.getRegion_name()+" : "+hub.getHub_desc()+" : "+id); 
		getJdbcTemplate().update( SQL,hub.getHub_name(),hub.getRegion_name(),hub.getHub_desc(),id);
		System.out.println("Hub Updated");
	}

	public Hub getHubById(double id) {
		 String sql="SELECT a.region_name,b.hub_id,b.hub_name,b.hub_desc FROM regions a, hubs b WHERE a.region_id = b.region_id and b.hub_id="+id+"";
		 System.out.println(sql);
		 return getJdbcTemplate().query(sql,new ResultSetExtractor<Hub>(){

		 public Hub extractData(ResultSet rs) throws SQLException,
		 		DataAccessException {
		 System.out.println("inside");
		 	//List<Hub> list=new ArrayList<Hub>(); 
		 	System.out.println("inside");
		 	Hub e=new Hub();  
		     while(rs.next()){  
		     	
		    //  e.setActivityDate(rs.getDate("activityDate"));
		     e.setHub_id(rs.getDouble("hub_id"));
		     e.setHub_name(rs.getString("hub_name"));
		     e.setHub_desc(rs.getString("hub_desc"));
		     e.setRegion_name(rs.getString("region_name"));
		    // list.add(e);  
		     System.out.println("inside while");
		     }  
		     return e;
		 }

		 });
	}

	public RequiredBeans getHubByAccountNo(String accountNo,String userType) {
		System.out.println("Account Number"+accountNo);
		
		
		System.out.println("user type : "+userType);

		String Q1="";
		Object o;
		if(userType.equalsIgnoreCase("Store Admin") || userType.equalsIgnoreCase("Store Executive"))
		{
			o=new Store();
			Q1="select store_name from store where store_id=(select store_id from user_store where account_no='"+accountNo+"')";
			
			
		}
		else if(userType.equalsIgnoreCase("Hub Admin") || userType.equalsIgnoreCase("Hub Executive"))
		{
			o=new Hub();
			Q1="select hub_name from hubs where hub_id=(select hub_id from user_hub where account_no='"+accountNo+"')";
			
		}
	final String ut=userType;
		
		System.out.println("Q : "+Q1);
		
		
		return getJdbcTemplate().query(Q1,new ResultSetExtractor<RequiredBeans>(){

			public RequiredBeans extractData(ResultSet rs) throws SQLException,
					DataAccessException {System.out.println("inside");
				//List<MasterBag> list=new ArrayList<MasterBag>(); 
					RequiredBeans obj=new RequiredBeans();
					Store st=new Store();
					Hub hub=new Hub();
				System.out.println("inside");
			    while(rs.next()){  
			    
			    	if(ut.equalsIgnoreCase("Store Admin") || ut.equalsIgnoreCase("Store Executive"))
					{
			    		System.out.println("inside store case");
			    		obj.setName(rs.getString("store_name"));
			    		
					}
			    	else if(ut.equalsIgnoreCase("Hub Admin") || ut.equalsIgnoreCase("Hub Executive"))
					{
			    		System.out.println("inside hub case"+rs.getString("hub_name"));
			    		
			    		obj.setName(rs.getString("hub_name"));
					}
			   
			    }
			    System.out.println("before return the value is "+obj);
				return obj;
		   
			}

			});
		
		
		/*String sql="select * from hubs where hub_id=(select hub_id from user_hub where  account_no=?)";
		Hub obj=getJdbcTemplate().queryForObject(sql, new Object[]{accountNo},new HubRowMapper());
		return obj;*/
	}

	public double getHubIdByHubName(String HubName) {


		String sql="SELECT hub_id from hubs where hub_name='"+HubName+"'";

			System.out.println(sql);
return getJdbcTemplate().query(sql,new ResultSetExtractor<Double>(){

public Double extractData(ResultSet rs) throws SQLException,
		DataAccessException {
	double d=0;
System.out.println("inside");
	List<Hub> list=new ArrayList<Hub>(); 
	System.out.println("inside");
    while(rs.next()){  
    d=rs.getDouble("hub_id");
    System.out.println("inside while");
    }  
    return d;
}

});
	}
		
}