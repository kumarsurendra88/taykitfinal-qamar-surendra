package com.dataisys.taykit.mobile.model;

public class ResponseForm {

	private boolean status;
	private String message;
	private String data;
	private String userType;
	private String regKeyMessage;	
	
	public String getRegKeyMessage() {
		return regKeyMessage;
	}
	public void setRegKeyMessage(String regKeyMessage) {
		this.regKeyMessage = regKeyMessage;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}	
}
