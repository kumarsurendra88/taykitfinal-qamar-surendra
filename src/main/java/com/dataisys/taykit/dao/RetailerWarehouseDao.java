package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.RetailerWarehouse;

/**
 * @author DataIsys
 *
 */
public interface RetailerWarehouseDao {

public void addWarehouse(Double retailerId);
public void addWarehouses(List<RetailerWarehouse> warehouses);
public List<RetailerWarehouse> getRetailerWarehouseById(double id);
}
