package test;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



//import com.csvreader.*;
import com.opencsv.CSVReader;

public class RunsheetGenerator {

	public RunsheetGenerator() {
		// TODO Auto-generated constructor stub
	}

	public  void main1() {
		try{
			 BufferedReader br = null;
			 String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
			 
			 System.out.println("befor");
			 //String workingDirectory = System.getProperty("user.dir");
			 CSVReader reader = new CSVReader(new FileReader(path1));
			
			/* CSVReader reader = new CSVReader(new FileReader("C:/Users/DataIsys/Documents/runsheet_raw.csv"));*/
			 System.out.println("after");
		     String [] nextLine;
		     nextLine = reader.readNext();
		     String Store =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeName =  nextLine[1];
		     nextLine = reader.readNext();
		     String AssigneeContact =  nextLine[1];
		     nextLine = reader.readNext();
		     String RunSheetID =  nextLine[1];
		     nextLine = reader.readNext();
		     reader.readNext();
		     String Table_top="";
		     String Runsheet ="";
		     String RunsheetScript="";
		     Double COD_TOTAL=0.0;
		     int i=0;
		     	 System.out.println("after int i");
		    	 RunsheetScript="";
		     Runsheet=Runsheet+"<style>tr{  page-break-inside: avoid;    -webkit-region-break-inside: avoid;} table,tr,td,th{ border-collapse: collapse; border: 1px solid black; }</style>\n";
		     Runsheet=Runsheet+"<table border=\"1\"><THEAD><tr><th>#</th><th>Tracking ID</th><th>Customer Name</th><th>COD Amount</th><th>Retailer</th><th>Shipment type</th><th>Customer Sign</th><th>Deli time</th><th>Remarks</th></tr></THEAD>\n<TBODY>";
		     while ((nextLine = reader.readNext()) != null) {
		    	
		    	 System.out.println("inside while");
		    	 i++;
		    	 String COD=nextLine[0];
		    	 String Customer_Name=nextLine[1];
		    	// String Customer_Address=nextLine[2];
		    	 String Customer_Contact=nextLine[3];
		    	 String Tracking_ID=nextLine[4];
		    	 String Shipment_Type=nextLine[5];
		    	 String Retailer=nextLine[6];
		    	 String RunSheetNo=nextLine[7];
		    	 COD_TOTAL=COD_TOTAL+Double.parseDouble(COD);
		    	 Runsheet=Runsheet+"<tr><td>"+i+"</td><td>"+/*Tracking_ID+"<br>"+*/"<div id=\"bar_"+i+"\"></div></td><td>"+Customer_Name+"<br>"+Customer_Contact+"</td><td>"+COD+"</td><td>"+Retailer+"</td><td>"+Shipment_Type+"</td><td>"+RunSheetNo+"</td><td></td><td></tr>\n";
		    	 RunsheetScript=RunsheetScript+" $(\"#bar_"+i+"\").barcode(\n";
		    	 RunsheetScript=RunsheetScript+"		 	\""+Tracking_ID+"\",\n";
		    	 RunsheetScript=RunsheetScript+"		 	\"code128\" \n";
		    	 RunsheetScript=RunsheetScript+"		 	);\n\n\n";
		    	 
		    	 System.out.println("end of while");
		    	 
		     }
		     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		     Date date = new Date();
		     System.out.println(); //2014/08/06 15:59:48
		     
		    String path= System.getProperty("user.home") + "/Desktop"+"/runsheet_final.html";
		     
		     Runsheet=Runsheet+"</TBODY></table>";
		     PrintWriter writer=null;
		     File testf = null;
		     File testf1=null;
		     try{
		    	 
		    	/* writer = new PrintWriter(workingDirectory/runtime.ht, filename);*/
		    writer = new PrintWriter(path, "UTF-8");
		     testf = new File( this.getClass().getResource( "/jquery-barcode.js" ).toURI() );
		     testf1 = new File( this.getClass().getResource( "/jquery-latest.min.js" ).toURI() );
		     System.out.println(testf.toString());
		     if(testf.exists()){
		    	 System.out.println("file exist");
		     }
		     else{
		    	 System.out.println("sorry");
		     }
		     }
		     catch(Exception e){
		    	 System.out.println();
		     }
		     writer.println("<script src = \"http://106.51.231.60:8080/taykitnew/jquery-latest.min.js\" type=\"text/javascript\" ></script>");
		     writer.println("<script src = \"http://106.51.231.60:8080/taykitnew/jquery-barcode.js\" type=\"text/javascript\" ></script>");
		   
		     writer.println("<table  border=\"1\" style=\"border-collapse: collapse; border: 1px solid black;\">");
		     writer.println("<tr><td>Store ID:<b>"+Store+"</b></td><td>Exec Name:<b>"+AssigneeName+"</b></td><td>Date:<b>"+dateFormat.format(date)+"</b></td></tr>");
		     writer.println("<tr><td>Runsheet ID:<b>"+RunSheetID+"</b></td><td>Exec Contact:<b>"+AssigneeContact+"</b></td></tr>");
		     writer.println("<tr><td># Shipments:<b>"+i+"</b></td><td>Total COD Amount:<b>"+COD_TOTAL+"</b></td></tr>");
		     writer.println("<tr><td>Opening Km:</td><td>Closing Km:</td></tr>");
		     writer.println("</table><br><br>");
		     
	    
		     writer.println(Runsheet);
		     writer.println("<script>");
		     writer.println(RunsheetScript);
		     writer.println("</script>");
		     System.out.println("before cloasing");
		     writer.close();
		}catch(Exception e){}

	}
	
	
	public static void main(String args[]){
		
		RunsheetGenerator o=new RunsheetGenerator();
		o.main1();
	}

}