/*package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Store {

	private double storeId;
	private String pincode;
	
	private String accountNo;
	public Store() {
		super();
	}
	public Store(double storeId, String pincode, String name, String address,
			String city, String type, String landMark, String geoLocation,
			String category, boolean edmAvailbility, String cctvId) {
		super();
		this.storeId = storeId;
		this.pincode = pincode;
		this.name = name;
		this.address = address;
		this.city = city;
		this.type = type;
		this.landMark = landMark;
		this.geoLocation = geoLocation;
		this.category = category;
		this.edmAvailbility = edmAvailbility;
		this.cctvId = cctvId;
	}
	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", pincode=" + pincode + ", name="
				+ name + ", address=" + address + ", city=" + city + ", type="
				+ type + ", landMark=" + landMark + ", geoLocation="
				+ geoLocation + ", category=" + category + ", edmAvailbility="
				+ edmAvailbility + ", cctvId=" + cctvId + "]";
	}
	private String name;
	private String address;
	private String city;
	private String type;
	private String landMark;
	private String geoLocation;
	private String category;
	private boolean edmAvailbility;
	private String cctvId;
	private Boolean storeStatus;
	private String updatedBy;
	private Timestamp updatedOn;
	private String reportingStore;
	private String surveyToken;
	private String cashPayment;
	private String creditCardPayment;
	private String mobilePayment;
	
	
	
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCashPayment() {
		return cashPayment;
	}
	public void setCashPayment(String cashPayment) {
		this.cashPayment = cashPayment;
	}
	public String getCreditCardPayment() {
		return creditCardPayment;
	}
	public void setCreditCardPayment(String creditCardPayment) {
		this.creditCardPayment = creditCardPayment;
	}
	public String getMobilePayment() {
		return mobilePayment;
	}
	public void setMobilePayment(String mobilePayment) {
		this.mobilePayment = mobilePayment;
	}
	public String getReportingStore() {
		return reportingStore;
	}
	public void setReportingStore(String reportingStore) {
		this.reportingStore = reportingStore;
	}
	public String getSurveyToken() {
		return surveyToken;
	}
	public void setSurveyToken(String surveyToken) {
		this.surveyToken = surveyToken;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
	}
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public String getGeoLocation() {
		return geoLocation;
	}
	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean getEdmAvailbility() {
		return edmAvailbility;
	}
	public void setEdmAvailbility(boolean b) {
		this.edmAvailbility = b;
	}
	public String getCctvId() {
		return cctvId;
	}
	public void setCctvId(String cctvId) {
		this.cctvId = cctvId;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Boolean getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(Boolean storeStatus) {
		this.storeStatus = storeStatus;
	}
	
}
*/
package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Store {

	private double storeId;
	
	
	private String accountNo;
	public Store() {
		super();
	}
	public Store(double storeId, String pincode, String name, String address,
			String city, String type, String landMark, String geoLocation,
			String category, boolean edmAvailbility, String cctvId, String zone_name, String region_name, String hub_name) {
		super();
		this.storeId = storeId;
		
		this.name = name;
		this.address = address;
		this.city = city;
		this.type = type;
		this.landMark = landMark;
		this.geoLocation = geoLocation;
		this.category = category;
		this.edmAvailbility = edmAvailbility;
		this.cctvId = cctvId;
		/*this.zone_name=zone_name;
		this.region_name=region_name;
		this.hub_name=hub_name;*/
		/*System.out.println("$$$$$$$hub name : "+this.hub_name);
		System.out.println("$$$$$$$region name : "+this.region_name);
		System.out.println("$$$$$$$zone name : "+this.zone_name);*/
		//System.out.println("$$$$$$$ pincode : "+this.pincode);
	}
	/*@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", pincode=" + pincode + ", name="
				+ name + ", address=" + address + ", city=" + city + ", type="
				+ type + ", landMark=" + landMark + ", geoLocation="
				+ geoLocation + ", category=" + category + ", edmAvailbility="
				+ edmAvailbility + ", cctvId=" + cctvId + ", zone_id=" + zone_id + ", region_id=" + region_id + ", hub_id=" + hub_id + "]";
	}*/
	
	
	
	private String name;



	private String pinCode;
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}



	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", accountNo=" + accountNo
				+ ", name=" + name + ", pinCode=" + pinCode + ", address="
				+ address + ", city=" + city + ", type=" + type + ", landMark="
				+ landMark + ", geoLocation=" + geoLocation + ", category="
				+ category + ", edmAvailbility=" + edmAvailbility + ", cctvId="
				+ cctvId + ", storeStatus=" + storeStatus + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn + ", reportingStore="
				+ reportingStore + ", surveyToken=" + surveyToken
				+ ", cashPayment=" + cashPayment + ", creditCardPayment="
				+ creditCardPayment + ", mobilePayment=" + mobilePayment
				+ ", zone_name=" + zone_name + ", hub_name=" + hub_name
				+ ", region_name=" + region_name + ", zone_id=" + zone_id
				+ ", region_id=" + region_id + ", hub_id=" + hub_id
				+ ", getPinCode()=" + getPinCode() + ", getZone_name()="
				+ getZone_name() + ", getHub_name()=" + getHub_name()
				+ ", getRegion_name()=" + getRegion_name() + ", getZone_id()="
				+ getZone_id() + ", getRegion_id()=" + getRegion_id()
				+ ", getHub_id()=" + getHub_id() + ", getAccountNo()="
				+ getAccountNo() + ", getCashPayment()=" + getCashPayment()
				+ ", getCreditCardPayment()=" + getCreditCardPayment()
				+ ", getMobilePayment()=" + getMobilePayment()
				+ ", getReportingStore()=" + getReportingStore()
				+ ", getSurveyToken()=" + getSurveyToken() + ", getStoreId()="
				+ getStoreId() + ", getName()=" + getName() + ", getAddress()="
				+ getAddress() + ", getCity()=" + getCity() + ", getType()="
				+ getType() + ", getLandMark()=" + getLandMark()
				+ ", getGeoLocation()=" + getGeoLocation() + ", getCategory()="
				+ getCategory() + ", getEdmAvailbility()="
				+ getEdmAvailbility() + ", getCctvId()=" + getCctvId()
				+ ", getUpdatedOn()=" + getUpdatedOn() + ", getUpdatedBy()="
				+ getUpdatedBy() + ", getStoreStatus()=" + getStoreStatus()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}



	private String address;
	private String city;
	private String type;
	private String landMark;
	private String geoLocation;
	private String category;
	private boolean edmAvailbility;
	private String cctvId;
	private Boolean storeStatus;
	private String updatedBy;
	private Timestamp updatedOn;
	private String reportingStore;
	private String surveyToken;
	private String cashPayment;
	private String creditCardPayment;
	private String mobilePayment;
	private String zone_name;
	public String getZone_name() {
		return zone_name;
	}
	public void setZone_name(String zone_name) {
		this.zone_name = zone_name;
	}
	public String getHub_name() {
		return hub_name;
	}
	public void setHub_name(String hub_name) {
		this.hub_name = hub_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	private String hub_name;
	private String region_name;
	
	private int zone_id,region_id;
	public int getZone_id() {
		return zone_id;
	}
	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public double getHub_id() {
		return hub_id;
	}
	public void setHub_id(double hub_id) {
		this.hub_id = hub_id;
	}
	private double hub_id;
	
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCashPayment() {
		return cashPayment;
	}
	public void setCashPayment(String cashPayment) {
		this.cashPayment = cashPayment;
	}
	public String getCreditCardPayment() {
		return creditCardPayment;
	}
	public void setCreditCardPayment(String creditCardPayment) {
		this.creditCardPayment = creditCardPayment;
	}
	public String getMobilePayment() {
		return mobilePayment;
	}
	public void setMobilePayment(String mobilePayment) {
		this.mobilePayment = mobilePayment;
	}
	public String getReportingStore() {
		return reportingStore;
	}
	public void setReportingStore(String reportingStore) {
		this.reportingStore = reportingStore;
	}
	public String getSurveyToken() {
		return surveyToken;
	}
	public void setSurveyToken(String surveyToken) {
		this.surveyToken = surveyToken;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public String getGeoLocation() {
		return geoLocation;
	}
	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean getEdmAvailbility() {
		return edmAvailbility;
	}
	public void setEdmAvailbility(boolean b) {
		this.edmAvailbility = b;
	}
	public String getCctvId() {
		return cctvId;
	}
	public void setCctvId(String cctvId) {
		this.cctvId = cctvId;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Boolean getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(Boolean storeStatus) {
		this.storeStatus = storeStatus;
	}
	
}
