package com.dataisys.taykit.dao;

import com.dataisys.taykit.model.OrderComment;


public interface OrderCommentDao {
		public void updateComment(OrderComment object);
		public void orderCommentAfterDelivery(OrderComment orderComment);
		public void updateOrder(OrderComment object);
		public void updateReason(OrderComment object);
		public void updateOrderReturn(OrderComment object);
}
