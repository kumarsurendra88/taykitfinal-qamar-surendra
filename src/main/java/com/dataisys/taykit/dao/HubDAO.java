package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.RequiredBeans;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.Category;
import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.rowmapper.StoreMapper;

public interface HubDAO {

	public boolean CreatHub(int region_id,String hub_name,String hub_desc);
	public List<Hub> getHubByRegionName(String region_name);
	//
	public List<Hub> getHubs();
	public void updateHub(Hub hub, double id);
	public Hub getHubById(double id);
	public RequiredBeans getHubByAccountNo(String accountNo, String userType);
	
	public double getHubIdByHubName(String HubName);
}
