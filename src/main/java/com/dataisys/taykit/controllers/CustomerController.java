package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;





import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.model.Customer;




@RestController
public class CustomerController {

	@Autowired
	CustomerDao customerDao;
	@RequestMapping(value="/createCustomer",method=RequestMethod.POST)
	public ModelAndView createOrder(Model model,@ModelAttribute com.dataisys.taykit.model.Customer customer){
		System.out.println("hiiiii");
		
		
		customerDao.createCustomer(customer);
		return new ModelAndView("landingpage.html");
		
	}
	
@RequestMapping(value="/getCustomer" , method=RequestMethod.GET)
	
	public List<com.dataisys.taykit.model.Customer> getCustomerList() throws IOException{
		
		List<com.dataisys.taykit.model.Customer> customers= customerDao.getCustomer();
		return customers;
	}

//added 11/2

@RequestMapping(value="/updateCustomerByPhoneAddress", method=RequestMethod.POST,headers="Content-Type=application/json")
public @ResponseBody String updateOrder(@RequestBody Customer object) throws IOException{
System.out.println("Got Object and  customer address is "+object.getCustomerAddress()+" and customer number is "+object.getCustomerMobile());
System.out.println("id is "+object.getValue()+" its value is "+object.getId());
String result;
if(object.getValue()!=null && !object.getValue().isEmpty()){
    
    
    result=customerDao.updateCustomerByPhoneAddress(object);
    
}

else{
    System.out.println("inside controller check of id as null");
    result="2";
}
return result;

}


}
