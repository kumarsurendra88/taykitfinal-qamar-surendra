package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class City {

private double cityId;
private String cityName; 
private String status;
private String notes;
private String updateBy;
private Timestamp updated_on;

public City(){}

public City(double cityId, String cityName, String status, String updateBy,
		Timestamp updated_on) {
	super();
	this.cityId = cityId;
	this.cityName = cityName;
	this.status = status;
	this.updateBy = updateBy;
	this.updated_on = updated_on;
}







public String getNotes() {
	return notes;
}

public void setNotes(String notes) {
	this.notes = notes;
}

public double getCityId() {
	return cityId;
}



public void setCityId(double cityId) {
	this.cityId = cityId;
}



public String getCityName() {
	return cityName;
}



public void setCityName(String cityName) {
	this.cityName = cityName;
}



public String getStatus() {
	return status;
}



public void setStatus(String status) {
	this.status = status;
}



public String getUpdateBy() {
	return updateBy;
}



public void setUpdateBy(String updateBy) {
	this.updateBy = updateBy;
}



public Timestamp getUpdated_on() {
	return updated_on;
}



public void setUpdated_on(Timestamp updated_on) {
	this.updated_on = updated_on;
}




}
