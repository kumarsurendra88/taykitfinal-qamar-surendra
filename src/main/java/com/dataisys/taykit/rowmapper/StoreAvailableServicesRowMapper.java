package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.StoreService;
import com.dataisys.taykit.model.StoreServices;

public class StoreAvailableServicesRowMapper implements RowMapper<StoreService>{
	
	
	public StoreService mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		StoreService service= new StoreService();
		service.setStoreId(rs.getDouble("store_id"));
		service.setServiceName(rs.getString("service_name"));
		service.setStoreServiceId(rs.getDouble("store_service_id"));
		service.setStoreServiceTypeId(rs.getDouble("service_type_id"));
		service.setUpdatedBy(rs.getString("updated_by"));
		return service;
		
	}



}
