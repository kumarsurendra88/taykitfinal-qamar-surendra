package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class CustomerData {
	private String contact;
	private String orderType;
	private String storeName;
	private String comments;
	private Timestamp updatedOn;
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public CustomerData() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CustomerData(String contact, String orderType, String storeName,
			String comments) {
		super();
		this.contact = contact;
		this.orderType = orderType;
		this.storeName = storeName;
		this.comments = comments;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "CustomerData [contact=" + contact + ", orderType=" + orderType
				+ ", storeName=" + storeName + ", comments=" + comments + "]";
	}
	 
	
	
}
