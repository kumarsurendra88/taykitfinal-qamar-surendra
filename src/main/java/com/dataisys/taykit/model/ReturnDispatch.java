package  com.dataisys.taykit.model;

import java.util.ArrayList;

public class ReturnDispatch {

		private ArrayList<String> trackingIds;
		private String noteToTaykitRO;
		private String courierName;
		private String referenceNo;
		private String noteToRetailer;		
		
		public ArrayList<String> getTrackingIds() {
			return trackingIds;
		}
		public void setTrackingIds(ArrayList<String> trackingIds) {
			this.trackingIds = trackingIds;
		}
		public String getNoteToTaykitRO() {
			return noteToTaykitRO;
		}
		public void setNoteToTaykitRO(String noteToTaykitRO) {
			this.noteToTaykitRO = noteToTaykitRO;
		}
		public String getCourierName() {
			return courierName;
		}
		public void setCourierName(String courierName) {
			this.courierName = courierName;
		}
		public String getReferenceNo() {
			return referenceNo;
		}
		public void setReferenceNo(String referenceNo) {
			this.referenceNo = referenceNo;
		}
		public String getNoteToRetailer() {
			return noteToRetailer;
		}
		public void setNoteToRetailer(String noteToRetailer) {
			this.noteToRetailer = noteToRetailer;
		}		
		
}
