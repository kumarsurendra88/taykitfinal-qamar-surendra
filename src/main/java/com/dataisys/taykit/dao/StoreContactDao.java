package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreContact;

/**
 * @author DataIsys
 *
 */
public interface StoreContactDao {

public StoreContact getContact(int storeID);
public void addContact(List<StoreContact> contacts);
public List<StoreContact> getStoreContactsBId(double id);
public List<com.dataisys.taykit.model.Store> getStoreByHubName(String hub_name);
public List<com.dataisys.taykit.model.Store> getStoreByAccNo(double account_no);
public List<Hub> getHubNamesById(double user_id);
public List<Hub> getHubNamesByIdForSA(double user_id);
}
