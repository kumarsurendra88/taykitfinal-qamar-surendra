package com.dataisys.taykit.dao;

import java.util.Date;
import java.util.List;

import com.dataisys.taykit.model.RunsheetDetails;
import com.dataisys.taykit.model.PaymentCollection;
import com.dataisys.taykit.model.ShortfallAmount;



/**
 * @author DataIsys
 *
 */
public interface PaymentCollectionDao {
public List<RunsheetDetails> gettodaysDeliveries(String date,String id);
public int pay(PaymentCollection entry);
//public void gettodaysDeliveries(String date, String id);
public void shortFallAmountDetails(ShortfallAmount details);
public ShortfallAmount getShoetfallAmountForRunsheet(String runsheetId);
public List<PaymentCollection> getTodaysPayments(String storename, String today);
}
