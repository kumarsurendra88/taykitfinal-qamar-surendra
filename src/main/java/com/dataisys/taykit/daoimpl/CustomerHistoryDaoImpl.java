package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.CustomerHistoryDao;
import com.dataisys.taykit.model.CustomerHistory;

public class CustomerHistoryDaoImpl extends JdbcDaoSupport implements CustomerHistoryDao {

	public void createCustomerHistory(CustomerHistory customerHistory) {
		
		System.out.println("Inside createCustomerHistory() of CustomerHistoryDaoImpl");
		String sql = "INSERT INTO customer_history(customer_name, customer_contact, tracking_id) VALUES (?,?,?)";
		try {
			getJdbcTemplate().update(sql, customerHistory.getCustomerName(), customerHistory.getCustomerMobile(), customerHistory.getTrackingId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void updateCustomerHistory(CustomerHistory customerHistory) {
		// TODO Auto-generated method stub

	}

	public List<CustomerHistory> getAllCustomerHistory() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<CustomerHistory> getCustomerHistoryByPhoneNumber(
			String contactNumber) {
		// TODO Auto-generated method stub
		return null;
	}

}
