package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderAssign;

public interface OrderAssignmentDAO {	
	public void assignOrder(String assignTo, List<Order> orders);
	public List<OrderAssign> getOrdersFromAssignByDbId(String assignTo);
	public List<OrderAssign> getAllOrdersFromOrderAssignment();
}
