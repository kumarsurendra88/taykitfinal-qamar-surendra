package com.dataisys.taykit.model;

import java.util.Date;

//@author irfan okkk
public class PaymentCollection {
private int paymentId;
private String StoreId;
private double codToCollect;
private double codPaid;
private double shortFallAmount;
private String runsheetNo;
private String deliveryUser;
private String date;
private double cardPayment;
public int getPaymentId() {
	return paymentId;
}
public void setPaymentId(int paymentId) {
	this.paymentId = paymentId;
}
public String getStoreId() {
	return StoreId;
}
public void setStoreId(String string) {
	StoreId = string;
}
public double getCodToCollect() {
	return codToCollect;
}
public void setCodToCollect(double codToCollect) {
	this.codToCollect = codToCollect;
}
public double getCodPaid() {
	return codPaid;
}
public void setCodPaid(double codPaid) {
	this.codPaid = codPaid;
}
public double getShortFallAmount() {
	return shortFallAmount;
}
public void setShortFallAmount(double shortFallAmount) {
	this.shortFallAmount = shortFallAmount;
}
public String getRunsheetNo() {
	return runsheetNo;
}
public void setRunsheetNo(String runsheetNo) {
	this.runsheetNo = runsheetNo;
}
public String getDeliveryUser() {
	return deliveryUser;
}
public void setDeliveryUser(String deliveryUser) {
	this.deliveryUser = deliveryUser;
}
public String getDate() {
	return date;
}
public void setDate(String today) {
	this.date = today;
}
@Override
public String toString() {
	return "PaymentCollection [paymentId=" + paymentId + ", StoreId=" + StoreId
			+ ", codToCollect=" + codToCollect + ", codPaid=" + codPaid
			+ ", shortFallAmount=" + shortFallAmount + ", runsheetNo="
			+ runsheetNo + ", deliveryUser=" + deliveryUser + ", date=" + date
			+ "]";
}
public PaymentCollection(int paymentId, String storeId, double codToCollect,
		double codPaid, double shortFallAmount, String runsheetNo,
		String deliveryUser, String date) {
	super();
	this.paymentId = paymentId;
	StoreId = storeId;
	this.codToCollect = codToCollect;
	this.codPaid = codPaid;
	this.shortFallAmount = shortFallAmount;
	this.runsheetNo = runsheetNo;
	this.deliveryUser = deliveryUser;
	this.date = date;
}
public PaymentCollection() {
	super();
}
public double getCardPayment() {
	return cardPayment;
}
public void setCardPayment(double cardPayment) {
	this.cardPayment = cardPayment;
}


}
