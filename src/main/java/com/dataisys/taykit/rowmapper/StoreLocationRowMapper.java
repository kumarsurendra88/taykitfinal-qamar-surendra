package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.StoreLocation;

public class StoreLocationRowMapper implements RowMapper<StoreLocation>{

	
	
	public StoreLocation mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 StoreLocation detail= new StoreLocation();
	 	detail.setStoreAddress(rs.getString("store_address"));
	 	detail.setStoreId(rs.getDouble("store_id"));
	 	detail.setLatitude(rs.getString("latitude"));
	 	detail.setLongitude(rs.getString("longitude"));
	 	detail.setStoreName(rs.getString("store_name"));
	 	return detail;
	 	
 }
	 	
 
 
 }
}

