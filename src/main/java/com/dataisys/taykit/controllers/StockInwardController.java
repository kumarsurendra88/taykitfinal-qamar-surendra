package com.dataisys.taykit.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.StockInward;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.services.OtpService;

@RestController
public class StockInwardController {
	@Autowired
	OrderDao orderDao;
	@Autowired
	public PudoStoreDao storeDao;

	@RequestMapping(value="/InwardEntry",method = RequestMethod.POST)
	public void inwardStock(@RequestBody StockInward obj,HttpSession session){
		System.out.println("Inside InwardEntry");
		System.out.println("got object"+obj);
		orderDao.inwardOrder(obj,session);
		
		//Code for sending OTP
		try {
			Order order = orderDao.getOrdersByTrackingId(obj.getTrackingId());
			Store store = storeDao.getStoreById((long)order.getStoreId());
			if(order != null) {
				if(order.getOrderType().trim().equalsIgnoreCase("collect")) {
					OtpService service = new OtpService();
					String otp = service.otpService(order, store);
					System.out.println("TrackingId: "+order.getTrackingId());
					System.out.println("Otp is: "+otp);
					orderDao.updateOtp(order.getTrackingId(), otp);
				}
			}
		} catch (Exception e) {
			System.err.println("Inside StockInwardController's catch block and the error is: "+e.getMessage());
		}
	}
}