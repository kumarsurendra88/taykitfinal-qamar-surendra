routerApp.factory('storeRunSheet',function(){
	var storeRunsheet=null;
	
	return{
		setNumber:function(number){
			storeRunsheet=number;
		},
		getNumber:function(){
			return storeRunsheet;
		}
		
	}
})


routerApp.factory('storeSummaryFactory',function(){
	var storeName=null;
	var storeStatus=null;
	
	return{
		setName:function(name){
			storeName=name;
		},
		getName:function(){
			return storeName;
		},
		setStatus:function(status){
			storeStatus=status;
		},
		getStatus:function(){
			return storeStatus;
		},
		
	}
})




function storeRunsheet($scope,$http,$location,storeRunSheet){
	$http.get("./getLoggedStore")
	.success(function(response){
		$scope.name=response.name;
		$scope.obj={storeId:response.storeId};
	})
	
	$http.get("./getStores")
		.success(function(response){
			console.log(response)
			$scope.stores=response;
		})
	
	//$scope.orderTypes=["Delivery","Collect"]
	$scope.getRunSheet=function(){
		console.log($scope.obj)
		if($scope.obj.fromDate>$scope.obj.toDate){
			alert("From date is greater than To date..!!!")
		}else{
			$http({
				method:"POST",
				url:"./getStoreRunSheets",
				data:$scope.obj
			})
			.success(function(response){
				console.log(response)
				if(response.length>0){
					 
					$scope.details=response;
				}else{
					alert("No Runsheets Found..!!")
				}
				
			})
		}
	}
	
	$scope.loadByRunSheet=function(storeRunsheet,status){
		storeRunSheet.setNumber(storeRunsheet);
		if(status=="OPEN"){
			$location.path("storeRunSheet2")
		}else{
			$location.path("storeRunSheetView")
		}
		
	}
}


function storeRunsheet2($scope,$http,$location,storeRunSheet){
	console.log(storeRunSheet.getNumber());
	
	$scope.statuses=["Delivered","At Taykit warehouse","In transit","Out For Delivery","Future Delivery","Could not attempt","Undeliverable","Order cancelled","Scheduled delivery"]
	var futureDeliveryReasons=["Door Locked","No Response","Customer Not Reachable"];
	var orderCancelledReasons=["Late","Not Required","Parcel Tampered"];
	var cudntAttemptReasons=["Vehicle breakdown","Bad Weather"];
	var undlrvbleReasons=["Wrong number","Customer not at address"];
	var totalCod=0;
	$http.get("./getOrdersByStoreRunsheet/"+storeRunSheet.getNumber())
	.success(function(response){
		$scope.orders=response;
		angular.forEach($scope.orders,function(value,key){
			console.log("Value "+value.trackingId+"  "+value.orderStatus)
			console.log(value.orderStatus.toUpperCase())
			if(value.orderStatus.toUpperCase()=="DELIVERED"||value.orderStatus.toUpperCase()=="DELIVERED1"){
				$scope.orders[key].deliveredFlag=true;
				if(value.paymentType=="COD"){
					totalCod+=value.orderValue;
				}
				
				console.log("deliverd")
				console.log(totalCod)
			}else{
				$scope.orders[key].deliveredFlag=false;
				console.log("not deleivered")
			}
			console.log("Key "+key)
		})
	})
	console.log(totalCod)
	$scope.loadReason=function(index,status){
		
		if(status=="Future Delivery"){
			$scope.reasons=futureDeliveryReasons;
		}else if(status=="Undeliverable"){
			$scope.reasons=undlrvbleReasons;
		}else if(status=="Order cancelled"){
			$scope.reasons=orderCancelledReasons;
		}else if(status=="Could not attempt"){
			$scope.reasons=cudntAttemptReasons;
		}else{
			$scope.reasons="";
		}
	}
	
	$scope.save=function(){
		console.log($scope.orders);

		$http({
		 		 		method : "POST",
				 		url : "./updateOrdersByRunsheet", 
				 		data :$scope.orders
				 		})
		.success(function(response){
			alert("Saved successfully..!!")
		})
	}
	
	$scope.closeRunSheet=function(){
		console.log($scope.orders);
		$http({
			method : "POST",
			url : "./closeStoreRunsheet",
			data : storeRunSheet.getNumber()
		})
		.success(function(response){
			alert("Runsheet was successfully closed..!!")
			 $location.path('storeRunSheet');
		})
	}
	
	$scope.cancel=function(){
		 $location.path('storeRunSheet');
		
	}
}

routerApp.factory('storeRunSheet',function(){
	var storeRunsheet=null;
	return{
		setNumber:function(number){
			storeRunsheet=number;
		},
		getNumber:function(){
			return storeRunsheet;
		}
	}
})
routerApp.factory('reportingStore',function(){
	var store=null;
	return{
		setStore:function(storename){
			store=storename;
		},
		getStore:function(){
			return store;
		}
	}
})

routerApp.factory('shortFall',function(){
	var amt=null;
	return{
		setAmount:function(amount){
			amt=amount;
		},
		getAmount:function(){
			return amt;
		}
	}
})

function storeRunsheetCash($scope,$http,$location,storeRunSheet,reportingStore){
	$http.get("./getLoggedStore")
	.success(function(response){
		$scope.name=response.name;
		$scope.obj={storeId:response.storeId};
	})
	
	$http.get("./getStores")
		.success(function(response){
			console.log(response)
			$scope.stores=response;
		})
	
	//$scope.orderTypes=["Delivery","Collect"]
	$scope.getRunSheet=function(){
		console.log($scope.obj.storeName)
		reportingStore.setStore($scope.obj.storeName)
		console.log(reportingStore.getStore())
		if($scope.obj.fromDate>$scope.obj.toDate){
			alert("From date is greater than To date..!!!")
		}else{
			$http({
				method:"POST",
				url:"./getStoreRunSheetsCash",
				data:$scope.obj
			})
			.success(function(response){
				console.log(response)
				if(response.length>0){
					 
					$scope.details=response;
				}else{
					alert("No Runsheets Found..!!")
				}
				
			})
		}
	}
	
	$scope.loadByRunSheet=function(storeRunsheet,status){
		storeRunSheet.setNumber(storeRunsheet);
		if(status=="OPEN"){
			$location.path("storeRunSheetCash2")
		}else{
			$location.path("storeRunSheetCash2")
		}
		
	}
}


function storeRunsheetCash2($scope,$http,$location,$modal,storeRunSheet,reportingStore,shortFall){
	console.log(storeRunSheet.getNumber());
	console.log(reportingStore.getStore());
	$scope.runshhetNumber=storeRunSheet.getNumber();
	$scope.reportingStore=reportingStore.getStore();
	
	$scope.receipt={}
	$scope.receipt.storeName=reportingStore.getStore();
	$scope.receipt.runsheetNumber=storeRunSheet.getNumber();
	$scope.receipt.shortFall=0;
	$scope.receipt.codCollected=0;
	$scope.receipt.card=0
	
	 
	
	
	/*$scope.statuses=["Delivered","At Taykit warehouse","In transit","Out For Delivery","Future Delivery","Could not attempt","Undeliverable","Order cancelled","Scheduled delivery"]
	var futureDeliveryReasons=["Door Locked","No Response","Customer Not Reachable"];
	var orderCancelledReasons=["Late","Not Required","Parcel Tampered"];
	var cudntAttemptReasons=["Vehicle breakdown","Bad Weather"];
	var undlrvbleReasons=["Wrong number","Customer not at address"];
	*/
	var totalCod=0;
	$http.get("./getOrdersByStoreRunsheet/"+storeRunSheet.getNumber())
	.success(function(response){
		$scope.orders=response;
		angular.forEach($scope.orders,function(value,key){
			 
			if(value.orderStatus.toUpperCase()=="DELIVERED"||value.orderStatus.toUpperCase()=="DELIVERED1"){
				$scope.orders[key].deliveredFlag=true;
				if(value.paymentType=="COD"){
					totalCod+=value.orderValue;
				}
				
				 
			}else{
				$scope.orders[key].deliveredFlag=false;
				 
			}
			 
		})
		console.log(totalCod)
		$scope.receipt.codAmount=totalCod;
	})
	
	$scope.loadReason=function(index,status){
		
		if(status=="Future Delivery"){
			$scope.reasons=futureDeliveryReasons;
		}else if(status=="Undeliverable"){
			$scope.reasons=undlrvbleReasons;
		}else if(status=="Order cancelled"){
			$scope.reasons=orderCancelledReasons;
		}else if(status=="Could not attempt"){
			$scope.reasons=cudntAttemptReasons;
		}else{
			$scope.reasons="";
		}
	}
	$scope.ShortFall=function(){
		 var modalInstance = $modal.open({
				templateUrl :'partialviews/storeCash.html',
				controller  : 'storeShortController',
				backdrop:'static'
			});
	}
	$scope.updateShortFall=function(){
		$scope.receipt.shortFall=$scope.receipt.codAmount-$scope.receipt.codCollected-$scope.receipt.card;
	}
	
	$scope.save=function(){
		console.log($scope.receipt);
		shortFall.setAmount($scope.receipt.shortFall);
		console.log(shortFall.getAmount())
		
		$http({
		 		 		method : "POST",
				 		url : "./makeStorePayment", 
				 		data :$scope.receipt
				 		})
		.success(function(response){
			alert("Saved successfully..!!")
		})
	}
	
	$scope.closeRunSheet=function(){
		/*console.log($scope.orders);
		$http({
			method : "POST",
			url : "./closeStoreRunsheet",
			data : storeRunSheet.getNumber()
		})
		.success(function(response){
			alert("Runsheet was successfully closed..!!")
			 $location.path('storeRunSheet');
		})*/
	}
	
	$scope.cancel=function(){
		 $location.path('storeRunSheet');
		
	}
}

function storeShortController($scope,$http,$modalInstance,$modal,shortFall,reportingStore,storeRunSheet){
	console.log("Hello")
	console.log(shortFall.getAmount())
	$scope.short={};
	$scope.short.deliveryUser=reportingStore.getStore();
	$scope.short.runsheetNo=storeRunSheet.getNumber();
	$scope.short.shortFallAmount=shortFall.getAmount();
	
	$scope.save=function(){
		console.log($scope.short)
		$http({
		 		 		method : "POST",
				 		url : "./saveStoreShortFall", 
				 		data :$scope.short
				 		})
		.success(function(response){
			alert("Saved successfully..!!")
		})
	}
	
	$scope.close=function()
	{
		
		    $modalInstance.dismiss('cancel');
		  
	}
}

function storeSummery($scope,$http,$location,storeRunSheet,storeSummaryFactory){
	$http.get("./getLoggedStore")
	.success(function(response){
		$scope.name=response.name;
		$scope.obj={storeId:response.storeId};
	})
	
	$http.get("./getStoreSummery")
		.success(function(response){
			console.log(response)
			console.log(response)
			if(response.length>0){
				 
				$scope.details=response;
			}else{
				alert("No Runsheets Found..!!")
			}			
		})
	
	//$scope.orderTypes=["Delivery","Collect"]
	$scope.getRunSheet=function(){
		console.log($scope.obj)
		if($scope.obj.fromDate>$scope.obj.toDate){
			alert("From date is greater than To date..!!!")
		}else{
			$http({
				method:"GET",
				url:"./getStoreSummery",
				
			})
			.success(function(response){
				console.log(response)
				if(response.length>0){
					 
					$scope.details=response;
				}else{
					alert("No Runsheets Found..!!")
				}
				
			})
		}
	}
	
	$scope.loadByRunSheet=function(storeName,status){
		
		console.log("storeName"+storeName)
		console.log("storeName"+status)
		storeSummaryFactory.setName(storeName);
		storeSummaryFactory.setStatus(status);
		$location.path("storeDetailSummery")
	}
}

 







function storeSummery2($scope,$http,$location,storeRunSheet,storeSummaryFactory){
	
	storeSummaryFactory.getName();
	storeSummaryFactory.getStatus();
	
	console.log("inside store summery")
	
	$http({
		method:"GET",
		url:"./getDetailStoreSummery/"+storeSummaryFactory.getName()+","+storeSummaryFactory.getStatus()
		 
	}).success(function(response){
		console.log(response)
		if(response.length>0){
			 
			$scope.orders=response;
		}
	})
	
	
}









