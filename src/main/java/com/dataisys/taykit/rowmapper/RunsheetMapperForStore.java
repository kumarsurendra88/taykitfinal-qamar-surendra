package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Order;

public class RunsheetMapperForStore implements RowMapper<Order> {

	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		
		Order order = new Order();
		order.setStoreRunsheet(rs.getString("store_runsheet"));
		order.setTrackingId(rs.getString("tracking_id"));
		
		return order;
	}

}
