package com.dataisys.taykit.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.model.ShortfallAmount;
import com.dataisys.taykit.model.StorePaymentCollection;

@RestController
public class StorePaymentController {
	
	@Autowired
	PudoStoreDao storeDao;
	
	@RequestMapping(value="/makeStorePayment")
	public void makeStorePayment(@RequestBody StorePaymentCollection obj){
		System.out.println("got"+obj);
		storeDao.collectStorePayment(obj);
	}
	
	@RequestMapping(value="/saveStoreShortFall")
	public void shortFallAmount(@RequestBody ShortfallAmount details )	{
		System.out.println(details);
		storeDao.saveStoreShortFall(details);
    }
}
