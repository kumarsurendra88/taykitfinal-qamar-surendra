package com.dataisys.taykit.model;

import java.sql.Date;
import java.sql.Timestamp;

import org.springframework.jdbc.core.JdbcTemplate;

public class OrderActivity {
	
	private double activityId;
	private String trackingId;
	private double toLocation;
	private double fromLocation;
	private String status;
	private Date activityDate;
	private String activityBy;
	private Timestamp updatedOn;
	private String activityDesc;
	private double deliveryboyId;
	String commentDate1;
	String order_type,customer_address,order_status,store_name;
	public String getStore_name() {
		return store_name;
	}



	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}



	public String getOrder_type() {
		return order_type;
	}



	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}



	public String getCustomer_address() {
		return customer_address;
	}



	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}



	public String getOrder_status() {
		return order_status;
	}



	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	private String reason;
	public OrderActivity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public String getReason() {
		return reason;
	}



	public void setReason(String reason) {
		this.reason = reason;
	}



	public String getCommentDate1() {
		return commentDate1;
	}



	public void setCommentDate1(String commentDate1) {
		this.commentDate1 = commentDate1;
	}



	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public double getDeliveryboyId() {
		return deliveryboyId;
	}

	public void setDeliveryboyId(double deliveryboyId) {
		this.deliveryboyId = deliveryboyId;
	}

	public OrderActivity(double activityId, String trackingId,
			double toLocation, double fromLocation, String status,
			Date activityDate, String activityBy, Timestamp updatedOn,
			String activityDesc, double deliveryboyId) {
		super();
		this.activityId = activityId;
		this.trackingId = trackingId;
		this.toLocation = toLocation;
		this.fromLocation = fromLocation;
		this.status = status;
		this.activityDate = activityDate;
		this.activityBy = activityBy;
		this.updatedOn = updatedOn;
		this.activityDesc = activityDesc;
		this.deliveryboyId = deliveryboyId;
	}

	public double getActivityId() {
		return activityId;
	}
	public void setActivityId(double activityId) {
		this.activityId = activityId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String string) {
		this.trackingId = string;
	}
	public double getToLocation() {
		return toLocation;
	}
	public void setToLocation(double toLocation) {
		this.toLocation = toLocation;
	}
	public double getFromLocation() {
		return fromLocation;
	}
	public void setFromLocation(double fromLocation) {
		this.fromLocation = fromLocation;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}
	public String getActivityBy() {
		return activityBy;
	}
	public void setActivityBy(String activityBy) {
		this.activityBy = activityBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "OrderActivity [activityId=" + activityId + ", trackingId="
				+ trackingId + ", toLocation=" + toLocation + ", fromLocation="
				+ fromLocation + ", status=" + status + ", activityDate="
				+ activityDate + ", activityBy=" + activityBy + ", updatedOn="
				+ updatedOn + ", activityDesc=" + activityDesc
				+ ", deliveryboyId=" + deliveryboyId + "]";
	}

	
	 
	
	
	
}
