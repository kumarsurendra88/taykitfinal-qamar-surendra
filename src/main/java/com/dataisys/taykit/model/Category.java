package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class Category {

private String categoryName;

private String categoryDesc;

private String categroyStatus;

private String updatedBy;

private Timestamp updatedOn;


public Category(){}


public Category(String categoryName, String categoryDesc,
		String categroyStatus, String updatedBy, Timestamp updatedOn) {
	super();
	this.categoryName = categoryName;
	this.categoryDesc = categoryDesc;
	this.categroyStatus = categroyStatus;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
}


public String getCategoryName() {
	return categoryName;
}


public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}


public String getCategoryDesc() {
	return categoryDesc;
}


public void setCategoryDesc(String categoryDesc) {
	this.categoryDesc = categoryDesc;
}


public String getCategroyStatus() {
	return categroyStatus;
}


public void setCategroyStatus(String categroyStatus) {
	this.categroyStatus = categroyStatus;
}


public String getUpdatedBy() {
	return updatedBy;
}


public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}


public Timestamp getUpdatedOn() {
	return updatedOn;
}


public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}






}
