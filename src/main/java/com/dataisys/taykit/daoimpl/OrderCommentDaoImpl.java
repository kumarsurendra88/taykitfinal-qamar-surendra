package com.dataisys.taykit.daoimpl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.OrderCommentDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.model.OrderComment;
import com.dataisys.taykit.model.User;

public class OrderCommentDaoImpl extends JdbcDaoSupport implements OrderCommentDao{

	@Autowired
	UserDao userDao;
	
	public void updateComment(OrderComment object) {	
		
			System.out.println("Inside updateComment");
			String sql="insert into `order_comment`(tracking_id, comment_text, comment_by, comment_date) VALUES(?,?,?,?)";
			getJdbcTemplate().update(sql, object.getTrackingId(),object.getCommentText(),object.getCommentBy(),object.getCommentDate()); 		 
			
	}
	
	public void orderCommentAfterDelivery(OrderComment orderComment) {
		
		System.out.println("Inside OrderCommentAfterDelivery");
		String sql="insert into `order_comment`(tracking_id, comment_text, comment_by, comment_date, received_by, user_name) VALUES(?,?,?,?,?,?)";
		java.util.Date utilDate = new java.util.Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(utilDate);
		cal.set(Calendar.MILLISECOND, 0);
		
		//String sql1 = "SELECT * FROM `user` WHERE account_no = ?";
		System.out.println("CommentBy: "+orderComment.getCommentBy());
		User user = userDao.getUserByAccNumber(orderComment.getCommentBy());
		if(user != null) {
			getJdbcTemplate().update(sql, orderComment.getTrackingId(),orderComment.getCommentText(),orderComment.getCommentBy(),new java.sql.Timestamp(cal.getTimeInMillis()),orderComment.getReceived_by(),user.getUserName());			
		} else {
			System.err.println("User is null in orderCommentAfterDelivery in OrderComment");
		}		
	}
	
	public void updateOrder(OrderComment object) {
		// TODO Auto-generated method stub
		String sql="update `order` SET order_status=?, status_reason = ?, recent_status_date = ? WHERE tracking_id=? ";
		getJdbcTemplate().update(sql, object.getCommentText(),object.getReason(), new Date(), object.getTrackingId());

		String sql2 = "insert into order_activity (activity_desc,status,tracking_id,activity_date) values(?,?,?,?)";
		getJdbcTemplate().update(sql2,
		"status updated as " + object.getCommentText(),
		object.getCommentText(), object.getTrackingId(), new Date());

		}
	
	//Added By Kishore
	public void updateReason(OrderComment object) {		
		// TODO Auto-generated method stub
		System.out.println("Inside updateReason");
		String sql="insert into `order_comment`(tracking_id, comment_text,reasons, comment_by, comment_date, user_name) VALUES(?,?,?,?,?,?)";
		
		User user = userDao.getUserByAccNumber(object.getCommentBy());
		if(user != null) {
			getJdbcTemplate().update(sql, object.getTrackingId(),object.getCommentText(),object.getReason(),object.getCommentBy(),object.getCommentDate(), user.getUserName());			
		} else {
			System.err.println("User is null");
		}		
	}
	
	public void updateOrderReturn(OrderComment object) {
		System.out.println("Inside updateOrderReturn in OrderComment's DaoImpl");
		String sql="update `order_return` SET order_status=?, status_reason = ?, recent_status_date = ? WHERE tracking_id=? ";
		getJdbcTemplate().update(sql, object.getCommentText(),object.getReason(), new Date(), object.getTrackingId());

		String sql2 = "insert into order_activity (activity_desc,status,tracking_id,activity_date) values(?,?,?,?)";
		getJdbcTemplate().update(sql2,
		"status updated as " + object.getCommentText(),
		object.getCommentText(), object.getTrackingId(), new Date());

		}
}


