package com.dataisys.taykit.model;

public class StoreLocation {
private double storeId;
private String storeName;
private String storeAddress;
private String latitude;
private String longitude;
public StoreLocation() {
	super();
}
public double getStoreId() {
	return storeId;
}
public void setStoreId(double storeId) {
	this.storeId = storeId;
}
public String getStoreName() {
	return storeName;
}
public void setStoreName(String storeName) {
	this.storeName = storeName;
}
public String getStoreAddress() {
	return storeAddress;
}
public void setStoreAddress(String storeAddress) {
	this.storeAddress = storeAddress;
}
public String getLatitude() {
	return latitude;
}
public void setLatitude(String latitude) {
	this.latitude = latitude;
}
public String getLongitude() {
	return longitude;
}
public void setLongitude(String longitude) {
	this.longitude = longitude;
}
@Override
public String toString() {
	return "StoreLocation [storeId=" + storeId + ", storeName=" + storeName
			+ ", storeAddress=" + storeAddress + ", latitude=" + latitude
			+ ", longitude=" + longitude + "]";
}
public StoreLocation(double storeId, String storeName, String storeAddress,
		String latitude, String longitude) {
	super();
	this.storeId = storeId;
	this.storeName = storeName;
	this.storeAddress = storeAddress;
	this.latitude = latitude;
	this.longitude = longitude;
}

}
