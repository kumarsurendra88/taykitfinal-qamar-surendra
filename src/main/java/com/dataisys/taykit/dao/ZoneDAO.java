package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Region;
import com.dataisys.taykit.model.Zone;

//import com.dataisys.taykit.model.Zone;

public interface ZoneDAO {

	public boolean CreateZone(String zone_name);
	public List<Zone> getZoneNames();
	public String getZoneNamesByID(String accNumber);
}
