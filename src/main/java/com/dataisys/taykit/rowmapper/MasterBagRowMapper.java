package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.MasterBag;

public class MasterBagRowMapper implements RowMapper<MasterBag>
{

	public MasterBag mapRow(ResultSet rs, int rowNum) throws SQLException {

MasterBag mb=new MasterBag();
mb.setMasterbag_no(rs.getDouble("masterbag_no"));
mb.setDestination(rs.getDouble("destination"));
mb.setMasterbag_type(rs.getString("masterbag_type"));
/*mb.setOrder_id(rs.getString("order_id"));
mb.setReturn_id(rs.getString("return_id"));
*/
return mb;
	}

}
