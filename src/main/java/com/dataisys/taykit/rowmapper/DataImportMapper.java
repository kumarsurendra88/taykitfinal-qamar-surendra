package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.DataImport;

public class DataImportMapper implements RowMapper<DataImport> {

	public DataImport mapRow(ResultSet rs, int rowNum) throws SQLException {
		DataImport obj=new DataImport();
			obj.setDataId(rs.getDouble("data_id"));
			obj.setSessionId(rs.getString("session_id"));
			obj.setImportType(rs.getString("import_type"));
			obj.setRetailerId(rs.getDouble("retailer_id"));
			obj.setDataStatus(rs.getString("data_status"));
			obj.setOrderCategory(rs.getString("order_category"));
			obj.setStoreId(rs.getDouble("store_id"));
			obj.setOrderType(rs.getString("order_type"));
			obj.setDataNote(rs.getString("data_note"));
			obj.setUpdatedBy(rs.getString("updated_by"));
			obj.setUpdatedOn(rs.getTimestamp("updated_on"));
			obj.setTrackingId(rs.getString("tracking_id"));
			obj.setOrderId(rs.getString("order_id"));
			obj.setOrderTitle(rs.getString("order_title"));
			obj.setOrderDetails(rs.getString("order_details"));
			obj.setOrderPriority(rs.getString("order_priority"));
			obj.setOrderProductId(rs.getDouble("order_product_id"));
			obj.setActivityId(rs.getDouble("activity_id"));
			obj.setRevisionId(rs.getDouble("revision_id"));
			obj.setAttachmentId(rs.getDouble("attachment_id"));
			obj.setOrderRefno(rs.getString("order_refno"));
			obj.setCustomerName(rs.getString("customer_name"));
			obj.setCustomerEmail(rs.getString("customer_email"));
			obj.setCustomerContact(rs.getString("customer_mobile"));
			obj.setCustomerAddress(rs.getString("customer_address"));
			obj.setCityName(rs.getString("city_name"));
			obj.setCustomerZipcode(rs.getInt("customer_zipcode"));
			obj.setPaymentType(rs.getString("payment_type"));
			obj.setBayNumber(rs.getInt("bay_number"));
			obj.setOrderStatus(rs.getString("order_status"));
			obj.setReceiptDate(rs.getDate("receipt_date"));
			obj.setDeliveryDate(rs.getDate("delivery_date"));
			obj.setAssignedTo(rs.getString("assigned_to"));
			obj.setDeliveredBy(rs.getString("delivered_by"));
			obj.setPaymentStatus(rs.getString("payment_status"));
			obj.setPaymentReference(rs.getString("payment_reference"));
			obj.setOrderValue(rs.getDouble("order_value"));
			obj.setCreatedBy(rs.getString("created_by"));
			obj.setCreatedOn(rs.getDate("created_on"));
		return obj;
	}

}
