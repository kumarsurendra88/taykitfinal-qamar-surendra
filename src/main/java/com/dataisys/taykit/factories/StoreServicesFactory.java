package com.dataisys.taykit.factories;

import java.util.ArrayList;
import java.util.List;

import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.StoreServices;

public class StoreServicesFactory {
private static StoreServicesFactory factory;
private List<StoreServices> services;

	
	public static StoreServicesFactory getFactory() {
		if (factory==null){
			factory=new StoreServicesFactory();
		   return factory;
		}
             return factory;
   } 


public static void setFactory(StoreServicesFactory factory) {
	StoreServicesFactory.factory = factory;
}


public List<StoreServices> getServices() {
	return services;
}


public void setServices(List <StoreServices> service) {
	this.services=service;
}


public StoreServicesFactory() {
	super();
}

}
