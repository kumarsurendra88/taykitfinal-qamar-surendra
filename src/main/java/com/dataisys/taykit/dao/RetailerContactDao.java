package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.RetailerContact;

/**
 * @author DataIsys
 *
 */
public interface RetailerContactDao {

public void addContact(List<RetailerContact> contacts);
public RetailerContact getContact(int retailerID);
public List<RetailerContact> getretailerContactsBId(double retailerID);


}
