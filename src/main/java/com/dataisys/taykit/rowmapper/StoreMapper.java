package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.mobile.model.Store;

public class StoreMapper implements RowMapper<Store>{

	public Store mapRow(ResultSet rs, int rowNum) throws SQLException {
		Store store = new Store();
		store.setAccountNo(rs.getString("account_no"));
		store.setStoreId(rs.getDouble("store_id"));
		store.setStoreServiceId(rs.getDouble("store_service_id"));
		store.setStoreCategoryId(rs.getDouble("store_category_id"));
		store.setStoreContactId(rs.getDouble("store_contact_id"));
		store.setStoreType(rs.getString("store_type"));
		store.setStoreName(rs.getString("store_name"));
		store.setStoreAddress(rs.getString("store_address"));
		store.setStoreCity(rs.getString("store_city"));
		store.setPincode(rs.getDouble("pincode"));
		store.setStoreGpsLocation(rs.getString("store_gpslocation"));
		store.setStoreEmail(rs.getString("store_email"));
		store.setStorePhone(rs.getString("store_phone"));
		store.setStoreOperations(rs.getString("store_operations"));
		store.setStoreStatus(rs.getString("store_status"));
		store.setUpdatedBy(rs.getString("updated_by"));
		store.setUpdatedOn(rs.getTimestamp("updated_on"));
		//store.setAccountNo(rs.getString("account_no"));
		return store;
	}

}
