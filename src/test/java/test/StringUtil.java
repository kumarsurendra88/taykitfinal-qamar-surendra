
package test;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class StringUtil {
	
	
	public static void main(String args[]){
		StringUtil.generateRandomString(8);
	}

  private static Random random = new Random((new Date()).getTime());
  
    /**
     * generates an alphanumeric string based on specified length.
     * @param length # of characters to generate
     * @return random string
     */
    public static String generateRandomString(int length) {
      char[] values = {'M','L','D' ,'L','1','2','5','4','5','6','7','8','9'};
      String out = "";
      for (int i=0;i<length;i++) {
          int idx=random.nextInt(values.length);
        out += values[idx];
      }
      System.out.println(out);
      return out;
    }
    
}
