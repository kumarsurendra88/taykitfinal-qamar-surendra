package com.dataisys.taykit.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.CustomerDataDao;
import com.dataisys.taykit.model.CustomerData;
import com.dataisys.taykit.model.Order;

@RestController
public class CustomerDataController {
	
	
	@Autowired
	CustomerDataDao customerDataDao;
	@RequestMapping(value="/saveData")
	public void saveData(@RequestBody CustomerData obj){
		System.out.println("got "+obj);
		customerDataDao.saveData(obj);
	}
	
	@RequestMapping(value="/getPreviousData/{contact}")
	public List<CustomerData> getPrevoius(@PathVariable ("contact") String contact){
		return customerDataDao.getPrevoius(contact);
	}
	
	@RequestMapping(value="/customerOrders/{customerContactNo}" )
	public @ResponseBody List<Order> getPrevoidus(@PathVariable ("customerContactNo") String customerContactNo){
		return customerDataDao.getOrderDetails(customerContactNo);
	}
	
	@RequestMapping(value="/customerPastOrders/{customerContactNo}" )
	public @ResponseBody List<Order> getPastData(@PathVariable ("customerContactNo") String customerContactNo){
		return customerDataDao.getcustomerPastOrders(customerContactNo);
	}
	
	
}
