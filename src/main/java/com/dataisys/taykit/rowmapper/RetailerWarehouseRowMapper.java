package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Device;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerWarehouse;

public class RetailerWarehouseRowMapper implements RowMapper<RetailerWarehouse>{

	
	
	public RetailerWarehouse mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 RetailerWarehouse warehouse= new RetailerWarehouse();
	 warehouse.setAddress(rs.getString("godown_address"));
	 warehouse.setCity(rs.getString("godown_city"));
	 
	 warehouse.setContactNo(rs.getString("godown_phone"));
	 warehouse.setGeoLocation(rs.getString("godown_gpslocation"));
	 warehouse.setName(rs.getString("godown_name"));
	 warehouse.setNotes(rs.getString("godown_notes"));
	 warehouse.setRetailerId(rs.getDouble("retailer_id"));
	 //warehouse.setUpdatedBy(rs.getString("updated_by"));
	 return warehouse;
	 	
 }
	 	
 
 
 }
}

