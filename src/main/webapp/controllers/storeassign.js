routerApp.factory('updateFactory',function() {
	var oderId;
	var orderTitle;
	return {
			setId:function (id) {
				console.log("Inside setId() of updateFactory and the Id is: "+id);
				oderId = id;
			},
			getId:function() {
				console.log("Inside getId() of updateFactory and the Id is: "+oderId);
				return oderId;
			}
	};
});

//added on 11/2
routerApp.controller('assignStoreReturn',['$scope','$http','$location', 'updateFactory',function($scope,$http,$location,updateFactory) {
    $scope.OrderTypes=["Pickup","DropOff"]
   
    $http.get("./getStores")
       .success(function(response){
           $scope.stores=response;
       })
        $scope.save = function() {
       console.log($scope.order)
       var storeName=$scope.storeName
       console.log("order Id is "+$scope.order.orderId+" store name is "+$scope.storeName+" order type is "+$scope.order.orderType)
       var res = './assignStoreReturn/'+storeName; 
       
        $http({
            
            method: 'POST',
          
             url: res,
             headers: {'Content-Type': 'application/json'},
             data: $scope.order
            
           })
           .success(function (data) 
             {
               
               
               alert("Order Assigned Successfully...!!!");
               
               $scope.order.orderId=""
                   //$scope.storeName=""
             });
    }
       
}])

routerApp.controller('storeAssign',['$scope','$http','$location', 'updateFactory',function($scope,$http,$location,updateFactory) {
    
	 $scope.OrderTypes=["Collect","Delivery"]
	/*$http.get("./getStoreUnassignedOrder")
	
	.success(function(response) {$scope.users = response;});*/
	
	$http.get("./getStores")
	.success(function(response){
		$scope.stores=response;
	})
	
 $scope.save = function() {
		console.log($scope.order)
    	var storeName=$scope.storeName
    	/*console.log("assign to value is" +$scope.assignTo)
    	console.log("hii inside save function");
    	$scope.assignFun(assignId);
    	console.log($scope.mySelections);
    	console.log("after myselection")
    	var d=$scope.mySelections; 
    	console.log("hhhhh" +$scope.d);
    	var B={};
    	B=d[0];
    	console.log(" the data i am having" +B);
    	*/
    	var res = './assignStore/'+storeName; 
    	
    	 $http({
    		 
    		 method: 'POST',
    	   
    	      url: res,
    	      headers: {'Content-Type': 'application/json'},
    	      data: $scope.order
    	     
    	    })
    	    .success(function (data) 
    	      {
    	    	
    	    	
    	    	alert("Order Assigned Successfully...!!!");
    	    	
    	    	$scope.order.trackingId=""
    	    		//$scope.storeName=""
    	      });
    	  };
    	
	/*console.log("data fetched is" $scope.users);*/
    /*
     * $scope.filterOptions = {
	        filterText: ''
	      };
     * $scope.gridUser = { 
     
    		
    		data: 'users',
        enableCellSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: true,
        
     	multiSelect: true ,
        columnDefs: [{field: 'orderId',
        				displayName: 'OrderId',
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
        			  {field: 'trackingId', displayName: 'trackingId', enableCellEdit: false, cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="loadById(row)">{{row.getProperty(col.field)}}</a></div>',
        				displayName: 'TrackingId',
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        			},
      
                     {
        				field: 'orderTitle', 
        				displayName: 'OrderTitle', 
        				enableCellEdit: false,
        				cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
        				}, 
        				 
                     {
        					field:'customerName', 
        					displayName:'CustomerName', 
        					enableCellEdit: false,
        					cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {	
                    	 field:'customerEmail', 
                    	 displayName:'CustomerEmailId', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                    },
                    {	
                   	 field:'orderStatus', 
                   	 displayName:'orderStatus', 
                   	 enableCellEdit: false,
                   	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                   },
                   {	
                     	 field:'paymentType', 
                     	 displayName:'paymentType', 
                     	 enableCellEdit: false,
                     	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                   
                     
                     
                     
                     {
                    	 field:'receiptDate', 
                    	 displayName:'ReceiptDate', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {field:'deliveredDate', 
                    	 displayName:'deliveredDate', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {
                    	 field:'retailerId', 
                    	 displayName:'retailerId', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {
                    	 field:'storeId', 
                    	 displayName:'StoreId', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     },
                     {
                    	 field:'orderValue', 
                    	 displayName:'value', 
                    	 enableCellEdit: false,
                    	 cellTemplate: '<div class="ngCellText"><input type="text" required ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\"></div>'
                     }
                    
                     ],
                    
                     selectedItems: $scope.mySelections,
                     filterOptions: $scope.filterOptions
    };
    
    $scope.loadById = function(row) { 
		 console.log("Inside edit()");
		 var ordId=row.entity.orderId;
		 var ordId=row.entity.trackingId;
		 console.log(ordId);
		 updateFactory.setId(ordId);
		 
	 $location.path('updateorder');
	 };
    
    
    $scope.assignFun=function(assignId){
		
		console.log("checking for selected row" +$scope.mySelections)
	}*/
    
   
    	}]);




