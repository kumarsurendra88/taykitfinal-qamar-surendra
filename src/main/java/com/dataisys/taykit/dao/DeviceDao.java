package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.Device;
//import com.dataisys.taykit.model.MobileData;

/**
 * @author DataIsys
 *
 */
public interface DeviceDao {

public List<Device> getDevices();
public void addDevice(Device device);
public void updateDevice(int id);


}
