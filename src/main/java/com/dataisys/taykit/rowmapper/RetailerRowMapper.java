package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Retailer;

public class RetailerRowMapper implements RowMapper<Retailer>{

	
	
	public Retailer mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 	Retailer retailer= new Retailer();
	 	retailer.setRetailerId(rs.getDouble("retailer_id"));
	 	retailer.setRetailerAddress(rs.getString("retailer_address"));
	 	retailer.setRetailerZip(rs.getString("retailer_zip"));
	 	retailer.setRetailerCity(rs.getString("retailer_city"));
	 	retailer.setRetailerEmail(rs.getString("retailer_email"));
	 	retailer.setRetailerGeoLocation(rs.getString("retailer_gpslocation"));
	 	retailer.setRetailerName(rs.getString("retailer_name"));
	 	retailer.setRetailerPhone(rs.getString("retailer_phone"));
	 	retailer.setRetailerType(rs.getString("retailer_type"));
	 	retailer.setCreatedOn(rs.getTimestamp("created_on"));
	 	retailer.setUpdatedBY(rs.getString("updated_by"));
	 	retailer.setUpdatedOn(rs.getTimestamp("updated_on"));
	 	return retailer;
	 	
 }
	 	
 
 
 }
}

