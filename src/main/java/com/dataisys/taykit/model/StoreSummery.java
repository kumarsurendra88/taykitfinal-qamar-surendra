package com.dataisys.taykit.model;

public class StoreSummery {

private String storeNames;
private double ordersLessThanOneDay;
private double ordersLessThanThreeDay;
private double orderLessThansixDays;
private double ordergreaterThanSevenDays;


public StoreSummery(){}


public StoreSummery(String storeNames, double ordersLessThanOneDay,
		double ordersLessThanThreeDay, double orderLessThansixDays,
		double ordergreaterThanSevenDays) {
	super();
	this.storeNames = storeNames;
	this.ordersLessThanOneDay = ordersLessThanOneDay;
	this.ordersLessThanThreeDay = ordersLessThanThreeDay;
	this.orderLessThansixDays = orderLessThansixDays;
	this.ordergreaterThanSevenDays = ordergreaterThanSevenDays;
}


public String getStoreNames() {
	return storeNames;
}


public void setStoreNames(String storeNames) {
	this.storeNames = storeNames;
}


public double getOrdersLessThanOneDay() {
	return ordersLessThanOneDay;
}


public void setOrdersLessThanOneDay(double ordersLessThanOneDay) {
	this.ordersLessThanOneDay = ordersLessThanOneDay;
}


public double getOrdersLessThanThreeDay() {
	return ordersLessThanThreeDay;
}


public void setOrdersLessThanThreeDay(double ordersLessThanThreeDay) {
	this.ordersLessThanThreeDay = ordersLessThanThreeDay;
}


public double getOrderLessThansixDays() {
	return orderLessThansixDays;
}


public void setOrderLessThansixDays(double orderLessThansixDays) {
	this.orderLessThansixDays = orderLessThansixDays;
}


public double getOrdergreaterThanSevenDays() {
	return ordergreaterThanSevenDays;
}


public void setOrdergreaterThanSevenDays(double ordergreaterThanSevenDays) {
	this.ordergreaterThanSevenDays = ordergreaterThanSevenDays;
}




}
