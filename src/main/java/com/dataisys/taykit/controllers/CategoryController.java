package com.dataisys.taykit.controllers;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.CategoryDao;
import com.dataisys.taykit.model.Category;
import com.dataisys.taykit.model.City;
import com.mysql.jdbc.Connection;



@RestController
public class CategoryController {

	@Autowired
	CategoryDao categoryDao;
	@Autowired
	DriverManagerDataSource dataSource;
	
	@RequestMapping(value="/createCategory",method=RequestMethod.POST)
	public ModelAndView createCategory(Model model,@ModelAttribute Category category){
		System.out.println("hiiiii");
		 
		System.out.println("category status" +category.getCategroyStatus());
		
		System.out.println("category Description" +category.getCategoryDesc());
		
		if(category.getCategroyStatus().trim().equalsIgnoreCase("on")){
			
			category.setCategroyStatus("active");
		}
		else{
			category.setCategroyStatus("inactive");
		}
		
		categoryDao.createCategory(category);
		return new ModelAndView("landingpage.html");
		
	}
	
	@RequestMapping(value="/getCategory",method=RequestMethod.GET)
	public List<Category> getCategory(){
java.sql.Connection con=DataSourceUtils.getConnection(dataSource);
		
		System.out.println("connection con"+con);
		List<Category> categoryList=categoryDao.getCategory();
		
		return categoryList;
		
	}

}
