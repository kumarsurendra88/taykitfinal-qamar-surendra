routerApp.factory('updateregion', function($resource) {
"use strict";
return $resource('./UpdateRegionById/:id', {}, {
update : {
method : 'PUT'
},
save : {
method : 'POST'
},
query : {
method : 'GET',
isArray : true
}
});
});




routerApp.factory('RegionIdFactory', function() {
	var data = [ {
		region_id : 0
	} ];
	return {
		getRegion_id : function() {
			return data.region_id;
		},
		setRegion_id : function(region_id) {
			
			data.region_id = region_id;

		}
	};
});


function updateRegionController($scope,$http,updateregion,RegionIdFactory,$location) {
	
	
	var id=RegionIdFactory.getRegion_id();
	if(id==0){
		alert("no data with the selected Region id");
	}else{
		
    $http.get("./getRegionsById/"+id)
	
	.success(function(response) {
		
		$scope.Region = response;
		
	});

	}
	
	$scope.save=function(){
		updateregion.save({id:id},$scope.Region,function(){
			
				alert("Hub Updated");
			})
	}
	
	$scope.cancel=function(){
		
		 $location.path('regionmanagement');
	}
	
	      
	}


