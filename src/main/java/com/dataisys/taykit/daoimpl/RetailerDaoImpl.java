package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.RetailerRowMapper;
import com.dataisys.taykit.rowmapper.StoreLocationRowMapper;
import com.dataisys.taykit.dao.ProductCategoriesDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.RetailerDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.RetailerWarehouseDao;
import com.dataisys.taykit.factories.ProductCategoryFactory;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.RetailerWarehouse;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StoreLocation;

public class RetailerDaoImpl extends JdbcDaoSupport implements RetailerDao {

	@Autowired
	RetailerServicesDao retailerServiceDao;
	@Autowired
	RetailerContactDao retailerContactsDao;
	@Autowired
	RetailerWarehouseDao retailerWarehouseDao;
	@Autowired
	ProductCategoriesDao categoryServiceDao;
	public List<Retailer> getRetailers() {
		
		String sql= "SELECT * FROM `retailer`";
		
		List<Retailer>retailerDeatils=getJdbcTemplate().query(sql,new RetailerRowMapper());
		return retailerDeatils;
	}
	
	
	public double getRetailerIdByName(String retailerName) {
		String sql="select retailer_id from retailer where retailer_name=?";
		double retailerId=getJdbcTemplate().queryForInt(sql, new Object[]{retailerName});
		return retailerId;
		}

	public int getAmount(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void AddReatiler(Retailer retail, List<RetailerServices> service,
			List<ProductCategories> category,List<RetailerContact> contacts,List<RetailerWarehouse> warehouses) {
		    System.out.println("in retailer implementstion");
		    System.out.println("retailer details are"+retail);
		    System.out.println("retailer services"+service);
		    System.out.println("retailer category"+category);
		    System.out.println("contacts"+contacts);
		    System.out.println("warehouses"+warehouses);
		    
		    
		    String SQL = "insert into retailer (retailer_address,retailer_type,retailer_name,retailer_city,retailer_zip,retailer_gpslocation,retailer_email,retailer_phone) values (?,?,?,?,?,?,?,?)";
		    getJdbcTemplate().update( SQL,retail.getRetailerAddress(),retail.getRetailerType(),retail.getRetailerName(),retail.getRetailerCity(),retail.getRetailerZip(),retail.getRetailerGeoLocation(),retail.getRetailerEmail(),retail.getRetailerPhone());
		      
		    
		    
		    
		    double retailerId;
		    retailerId=getRetailerId();
		    List<RetailerService> providedServices=new ArrayList<RetailerService>();
		    List<RetailerCategory> providedCategories=new ArrayList<RetailerCategory>();
		    Iterator<RetailerServices> it=service.iterator();
		    Iterator<ProductCategories> categoryIt=category.iterator();
		    Iterator<RetailerContact> contactsIt=contacts.iterator();
		    Iterator<RetailerWarehouse> waresouseIt=warehouses.iterator();
		    while(contactsIt.hasNext())
		    {
		    	contactsIt.next().setRetailerId(retailerId);
		    }
		    while(waresouseIt.hasNext())
		    {
		    	waresouseIt.next().setRetailerId(retailerId);
		    }
		    while(it.hasNext())
		    {
		    	RetailerServices obj=it.next();
		    	if(obj.getServiceStatus()==true)
		    	{
		    	RetailerService data=new RetailerService();
		    	double serviceTypeId=obj.getServiceTypeId();
		    	data.setRetailerId(retailerId);
		    	data.setServiceId(serviceTypeId);
		    	providedServices.add(data);
		    	}
		    }
		    while(categoryIt.hasNext())
		    {
		    	ProductCategories obj=categoryIt.next();
		    	if(obj.getCategoryStatus()==true)
		    	{
		    	RetailerCategory data=new RetailerCategory();
		    	double categoryTypeId=obj.getCategoryId();
		    	String name=obj.getCategoryName();
		    	data.setCategoryName(name);
		    	data.setProductCategoryId(categoryTypeId);
		    	data.setRetailerId(retailerId);
		    	providedCategories.add(data);
		    	
		    	}
		    }
		    retailerServiceDao.setRetailerService(providedServices);
		    retailerContactsDao.addContact(contacts);
		    retailerWarehouseDao.addWarehouses(warehouses);
		    categoryServiceDao.addRetailerCategories(providedCategories);
	}
	
	public Double getRetailerId()
	   {
		 
		   String SQl="SELECT * FROM retailer WHERE retailer_id=(SELECT MAX(retailer_id) FROM retailer)";
		   Retailer record=getJdbcTemplate().queryForObject(SQl, new RetailerRowMapper()) ;
		   Double id=record.getRetailerId();
		   System.out.println("returned retailer_id is"+id);
		return id; 	
	   }
public void insertRetailerContact(){
	
}

public Retailer getRetailerbyId(double id) {
	String sql="select * from`retailer` where retailer_id=?";
	return getJdbcTemplate().queryForObject(sql, new Object[]{id}, new RetailerRowMapper());
}

public void updateReatiler(Retailer retail,
		List<RetailerServices> service,
		List<ProductCategories> category,
		List<RetailerContact> contacts,
		List<RetailerWarehouse> warehouses, double id) {
	 System.out.println("in retailer implementstion");
	    System.out.println("retailer details are"+retail);
	    System.out.println("retailer services"+service);
	    System.out.println("retailer category"+category);
	    System.out.println("contacts"+contacts);
	    System.out.println("warehouses"+warehouses);
	    
	    
	    String SQL = "update retailer set retailer_address=?,retailer_type=?,retailer_name=?,retailer_city=?,retailer_zip=?,retailer_gpslocation=?,retailer_email=?,retailer_phone=? where retailer_id=?";
	    getJdbcTemplate().update( SQL,retail.getRetailerAddress(),retail.getRetailerType(),retail.getRetailerName(),retail.getRetailerCity(),retail.getRetailerZip(),retail.getRetailerGeoLocation(),retail.getRetailerEmail(),retail.getRetailerPhone(),id);
	      
	    
	    
	    
	    
	    List<RetailerService> providedServices=new ArrayList<RetailerService>();
	    List<RetailerCategory> providedCategories=new ArrayList<RetailerCategory>();
	    Iterator<RetailerServices> it=service.iterator();
	    Iterator<ProductCategories> categoryIt=category.iterator();
	    Iterator<RetailerContact> contactsIt=contacts.iterator();
	    Iterator<RetailerWarehouse> waresouseIt=warehouses.iterator();
	    while(contactsIt.hasNext())
	    {
	    	contactsIt.next().setRetailerId(id);
	    }
	    while(waresouseIt.hasNext())
	    {
	    	waresouseIt.next().setRetailerId(id);
	    }
	    while(it.hasNext())
	    {
	    	RetailerServices obj=it.next();
	    	if(obj.getServiceStatus()==true)
	    	{
	    	RetailerService data=new RetailerService();
	    	double serviceTypeId=obj.getServiceTypeId();
	    	data.setRetailerId(id);
	    	data.setServiceId(serviceTypeId);
	    	providedServices.add(data);
	    	}
	    }
	    while(categoryIt.hasNext())
	    {
	    	ProductCategories obj=categoryIt.next();
	    	if(obj.getCategoryStatus()==true)
	    	{
	    	RetailerCategory data=new RetailerCategory();
	    	double categoryTypeId=obj.getCategoryId();
	    	String name=obj.getCategoryName();
	    	data.setCategoryName(name);
	    	data.setProductCategoryId(categoryTypeId);
	    	data.setRetailerId(id);
	    	providedCategories.add(data);
	    	
	    	}
	    }
	    retailerServiceDao.updateRetailerService(providedServices,id);
	    retailerContactsDao.addContact(contacts);
	    retailerWarehouseDao.addWarehouses(warehouses);
	    categoryServiceDao.updateRetailerCategories(providedCategories,id);
	
}


public Retailer getRetailerByName(String retailerName) {
	try {
		String sql = "SELECT * FROM retailer WHERE retailer_name = '"+retailerName+"'";
		System.out.println("Query is : "+sql);
		Retailer retailer = getJdbcTemplate().queryForObject(sql, new RetailerRowMapper());
		return retailer;
	} catch(Exception e) {	
		e.printStackTrace();
		return null;
	}
}


public List<Retailer> getRetailerNameFromOrderReturnDb() {

    final List<Retailer> rl=new ArrayList<Retailer>(); 
    String sql="SELECT retailer.`retailer_name` FROM retailer INNER JOIN order_return ON retailer.`retailer_id`=order_return.`retailer_id`";
    
    
    
    getJdbcTemplate().query(sql, new ResultSetExtractor<Retailer>(){
        
        public Retailer extractData(ResultSet rs)throws SQLException,DataAccessException 
        {
            System.out.println("inside ");
            
            Retailer rt=new Retailer();  
            while(rs.next()){
                rt.setRetailerName(rs.getString("retailer_name"));
                rl.add(rt);
            }
            return null;
            
        }
        
    });

    return rl;
}



}
