package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class OrderProduct {

	private double orderProductId;
	private double orderId;
	private String productCode;
	private String productDesc;
	private String updatedBy;
	private Timestamp updatedOn;
	public OrderProduct() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderProduct(double orderProductId, double orderId,
			String productCode, String productDesc, String updatedBy,
			Timestamp updatedOn) {
		super();
		this.orderProductId = orderProductId;
		this.orderId = orderId;
		this.productCode = productCode;
		this.productDesc = productDesc;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
	}
	public double getOrderProductId() {
		return orderProductId;
	}
	public void setOrderProductId(double orderProductId) {
		this.orderProductId = orderProductId;
	}
	public double getOrderId() {
		return orderId;
	}
	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Override
	public String toString() {
		return "OrderProduct [orderProductId=" + orderProductId + ", orderId="
				+ orderId + ", productCode=" + productCode + ", productDesc="
				+ productDesc + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + "]";
	}
	
	
}
