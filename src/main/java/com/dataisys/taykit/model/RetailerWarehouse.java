package com.dataisys.taykit.model;


public class RetailerWarehouse {
private String name;
private String address;
private String city;
private String area;
private String contactNo;
private String notes;
private String geoLocation;
private double retailerId;
private double updatedBy;
private double updatedOn;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getArea() {
	return area;
}
public void setArea(String area) {
	this.area = area;
}
public String getContactNo() {
	return contactNo;
}
public void setContactNo(String contactNo) {
	this.contactNo = contactNo;
}
public String getNotes() {
	return notes;
}
public void setNotes(String notes) {
	this.notes = notes;
}
public String getGeoLocation() {
	return geoLocation;
}
public void setGeoLocation(String geoLocation) {
	this.geoLocation = geoLocation;
}
@Override
public String toString() {
	return "RetailerWarehouse [name=" + name + ", address=" + address
			+ ", city=" + city + ", area=" + area + ", contactNo=" + contactNo
			+ ", notes=" + notes + ", geoLocation=" + geoLocation + "]";
}
public RetailerWarehouse(String name, String address, String city, String area,
		String contactNo, String notes, String geoLocation) {
	super();
	this.name = name;
	this.address = address;
	this.city = city;
	this.area = area;
	this.contactNo = contactNo;
	this.notes = notes;
	this.geoLocation = geoLocation;
}
public RetailerWarehouse() {
	super();
}
public double getRetailerId() {
	return retailerId;
}
public void setRetailerId(double retailerId) {
	this.retailerId = retailerId;
}
public double getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(double updatedOn) {
	this.updatedOn = updatedOn;
}
public double getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(double updatedBy) {
	this.updatedBy = updatedBy;
}

}