package com.dataisys.taykit.mobile.model;

import java.sql.Timestamp;

public class UserStore {

	private double userStoreId;
	private double userId;
	private double storeId;
	private String updatedBy;
	private Timestamp updatedOn;
	public double getUserStoreId() {
		return userStoreId;
	}
	public void setUserStoreId(double userStoreId) {
		this.userStoreId = userStoreId;
	}
	public double getUserId() {
		return userId;
	}
	public void setUserId(double userId) {
		this.userId = userId;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
}
