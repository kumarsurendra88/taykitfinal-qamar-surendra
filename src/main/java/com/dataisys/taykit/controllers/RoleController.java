package com.dataisys.taykit.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.RoleDao;
import com.dataisys.taykit.model.City;
import com.dataisys.taykit.model.Role;



@RestController
public class RoleController {
	
	
	@Autowired
	RoleDao roleDao;
	@RequestMapping(value="/createRole",method=RequestMethod.POST)
	public ModelAndView createRole(Model model,@ModelAttribute Role role){
		System.out.println("hiiiii");
		
		
		roleDao.creaetRole(role);;
		return new ModelAndView("landingpage.html");
		
	}
	
	@RequestMapping(value="/getRole",method=RequestMethod.GET)
	public List<Role> getRole(){
		
		List<Role> roles=roleDao.getRole();
		
		return roles;
		
		
	}

}
