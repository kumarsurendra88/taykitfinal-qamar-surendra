package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.City;
import com.dataisys.taykit.model.Customer;

public class CityRowMapper implements RowMapper<City> {

	public City mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		City city=new City();
		city.setCityId(rs.getDouble("city_id"));
		
		city.setCityName(rs.getString("city_name"));
		
		city.setStatus(rs.getString("city_status"));
		
		city.setUpdateBy(rs.getString("updated_by"));
		
		city.setNotes(rs.getString("notes"));
		
		
		return city;
	}

}
