package com.dataisys.taykit.mobile.model;

public class RegisterKeyResponse {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	

}
