package com.dataisys.taykit.factories;

import java.util.ArrayList;
import java.util.List;

import com.dataisys.taykit.model.RetailerContact;

public class RetailerContactFactory {
private static RetailerContactFactory factory;
private List<RetailerContact> contactsList =new ArrayList<RetailerContact>();

	
	public static RetailerContactFactory getFactory() {
		if(factory==null){
			System.out.println("in if");
			factory=new RetailerContactFactory();
			System.out.println("facory object is"+factory);
		   return factory;
		}
             return factory;
   } 
public static void setFactory(RetailerContactFactory factory) {
	RetailerContactFactory.factory = factory;
}

public List<RetailerContact> getContacts() {
	return contactsList;
}


public  void setContacts(RetailerContact contacts) {
	contactsList.add(contacts);
	System.out.println(contactsList);
}


public RetailerContactFactory() {
	super();
}

}
