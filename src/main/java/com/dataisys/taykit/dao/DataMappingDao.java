package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.DataMapping;

public interface DataMappingDao {

	public void saveMapping(List<DataMapping> list);
	public List<DataMapping> getMappingByRetailer(int retailerId);
	public int checkMapping(int retailerId);
	public void updateMapping(List<DataMapping> list,double retailerId);
}
