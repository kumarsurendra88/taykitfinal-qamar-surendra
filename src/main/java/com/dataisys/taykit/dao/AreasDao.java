package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Areas;

public interface AreasDao {
	public List<Areas> getAreas();
}
