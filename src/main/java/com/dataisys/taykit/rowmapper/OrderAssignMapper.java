package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.OrderAssign;

public class OrderAssignMapper implements RowMapper<OrderAssign>{

	public OrderAssign mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderAssign ob= new OrderAssign();
		ob.setAssignId(rs.getDouble("assign_id"));
		ob.setOrderId(rs.getDouble("order_id"));
		ob.setAssignFrom(rs.getString("assign_from"));
		ob.setAssignFromType(rs.getString("assign_from_type"));
		ob.setAssignTo(rs.getString("assign_to"));
		ob.setAssignToType(rs.getString("assign_to_type"));
		ob.setUpdatedBy(rs.getString("updated_by"));
		ob.setUpdated_on(rs.getTimestamp("updated_on"));
		return ob;
	}

}
