package com.dataisys.taykit.model;

public class UpdateDeliveryStatus {
	private String runsheetNumber;
	
	private int totalOrders;
	private int totalPrepaid;
	private int totalPrepaidDelivered;
	private int totalPrepaidPending;
	private int totalCOD;
	private int totalCODDelivered;
	private int totalCODPending;
	private Double codCollected;
	private String runsheetStatus;
	private double codRemitted;
	private double codShortFall;
	
	
	
	public double getCodRemitted() {
		return codRemitted;
	}
	public void setCodRemitted(double codRemitted) {
		this.codRemitted = codRemitted;
	}
	public double getCodShortFall() {
		return codShortFall;
	}
	public void setCodShortFall(double codShortFall) {
		this.codShortFall = codShortFall;
	}
	public UpdateDeliveryStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UpdateDeliveryStatus(String runsheetNumber, int totalOrders,
			int totalPrepaid, int totalPrepaidDelivered,
			int totalPrepaidPending, int totalCOD, int totalCODDelivered,
			int totalCODPending, Double codCollected) {
		super();
		this.runsheetNumber = runsheetNumber;
		this.totalOrders = totalOrders;
		this.totalPrepaid = totalPrepaid;
		this.totalPrepaidDelivered = totalPrepaidDelivered;
		this.totalPrepaidPending = totalPrepaidPending;
		this.totalCOD = totalCOD;
		this.totalCODDelivered = totalCODDelivered;
		this.totalCODPending = totalCODPending;
		this.codCollected = codCollected;
	}
	
	public int getTotalOrders() {
		return totalOrders;
	}
	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}
	public int getTotalPrepaid() {
		return totalPrepaid;
	}
	public void setTotalPrepaid(int totalPrepaid) {
		this.totalPrepaid = totalPrepaid;
	}
	public int getTotalPrepaidDelivered() {
		return totalPrepaidDelivered;
	}
	public void setTotalPrepaidDelivered(int totalPrepaidDelivered) {
		this.totalPrepaidDelivered = totalPrepaidDelivered;
	}
	public int getTotalPrepaidPending() {
		return totalPrepaidPending;
	}
	public void setTotalPrepaidPending(int totalPrepaidPending) {
		this.totalPrepaidPending = totalPrepaidPending;
	}
	public int getTotalCOD() {
		return totalCOD;
	}
	public void setTotalCOD(int totalCOD) {
		this.totalCOD = totalCOD;
	}
	public int getTotalCODDelivered() {
		return totalCODDelivered;
	}
	public void setTotalCODDelivered(int totalCODDelivered) {
		this.totalCODDelivered = totalCODDelivered;
	}
	public int getTotalCODPending() {
		return totalCODPending;
	}
	public void setTotalCODPending(int totalCODPending) {
		this.totalCODPending = totalCODPending;
	}
	public Double getCodCollected() {
		return codCollected;
	}
	public void setCodCollected(Double codCollected) {
		this.codCollected = codCollected;
	}
	@Override
	public String toString() {
		return "UpdateDeliveryStatus [totalOrders=" + totalOrders
				+ ", totalPrepaid=" + totalPrepaid + ", totalPrepaidDelivered="
				+ totalPrepaidDelivered + ", totalPrepaidPending="
				+ totalPrepaidPending + ", totalCOD=" + totalCOD
				+ ", totalCODDelivered=" + totalCODDelivered
				+ ", totalCODPending=" + totalCODPending + ", codCollected="
				+ codCollected + "]";
	}
	public String getRunsheetNumber() {
		return runsheetNumber;
	}
	public void setRunsheetNumber(String runsheetNumber) {
		this.runsheetNumber = runsheetNumber;
	}
	public String getRunsheetStatus() {
		return runsheetStatus;
	}
	public void setRunsheetStatus(String runsheetStatus) {
		this.runsheetStatus = runsheetStatus;
	}
	
	
	
}
