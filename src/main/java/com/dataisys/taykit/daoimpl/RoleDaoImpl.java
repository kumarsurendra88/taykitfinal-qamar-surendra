package com.dataisys.taykit.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.RoleDao;
import com.dataisys.taykit.model.Role;
import com.dataisys.taykit.rowmapper.CityRowMapper;
import com.dataisys.taykit.rowmapper.RolesRowMapper;

public class RoleDaoImpl extends JdbcDaoSupport implements RoleDao{

	public void creaetRole(Role role) {
		// TODO Auto-generated method stub
	String sql="insert into role(role_name,role_desc,role_status,updated_by)"+"values(?,?,?,?)";
	getJdbcTemplate().update(sql,role.getRoleName(),role.getRoleDesc(),role.getRoleStatus(),role.getUpdatedBy());
	
	
	}

	public List<Role> getRole() {
		// TODO Auto-generated method stub
		
	String sql="select *from role";

	
	List<Role> roles=getJdbcTemplate().query(sql,new RolesRowMapper());
	
	return roles;
	
	}

}
