package com.dataisys.taykit.model;

import java.sql.Date;

public class StorePaymentCollection {
	private String runsheetNumber;
	private String storeName;
	private double codAmount;
	private double codCollected;
	private double card;
	private double shortFall;
	private Date handedDate;
	
	
	public StorePaymentCollection() {
		super();
		// TODO Auto-generated constructor stub
	}


	public StorePaymentCollection(String runsheetNumber, String storeName,
			double codAmount, double codCollected, double card,
			double shortFall, Date handedDate) {
		super();
		this.runsheetNumber = runsheetNumber;
		this.storeName = storeName;
		this.codAmount = codAmount;
		this.codCollected = codCollected;
		this.card = card;
		this.shortFall = shortFall;
		this.handedDate = handedDate;
	}


	public String getRunsheetNumber() {
		return runsheetNumber;
	}


	public void setRunsheetNumber(String runsheetNumber) {
		this.runsheetNumber = runsheetNumber;
	}


	public String getStoreName() {
		return storeName;
	}


	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}


	public double getCodAmount() {
		return codAmount;
	}


	public void setCodAmount(double codAmount) {
		this.codAmount = codAmount;
	}


	public double getCodCollected() {
		return codCollected;
	}


	public void setCodCollected(double codCollected) {
		this.codCollected = codCollected;
	}


	public double getCard() {
		return card;
	}


	public void setCard(double card) {
		this.card = card;
	}


	public double getShortFall() {
		return shortFall;
	}


	public void setShortFall(double shortFall) {
		this.shortFall = shortFall;
	}


	public Date getHandedDate() {
		return handedDate;
	}


	public void setHandedDate(Date handedDate) {
		this.handedDate = handedDate;
	}


	@Override
	public String toString() {
		return "StorePaymentCollection [runsheetNumber=" + runsheetNumber
				+ ", storeName=" + storeName + ", codAmount=" + codAmount
				+ ", codCollected=" + codCollected + ", card=" + card
				+ ", shortFall=" + shortFall + ", handedDate=" + handedDate
				+ "]";
	}
	
	
}
