routerApp.factory('updatehub', function($resource) {
"use strict";
return $resource('./UpdateHubById/:id', {}, {
update : {
method : 'PUT'
},
save : {
method : 'POST'
},
query : {
method : 'GET',
isArray : true
}
});
});


routerApp.factory('HubIdFactory', function() {
	var data = [ {
		hub_id : 0
	} ];
	return {
		getHub_id : function() {
			return data.hub_id;
		},
		setHub_id : function(hub_id) {
			
			data.hub_id = hub_id;

		}
	};
});


function updateHubController($scope,$http,updatehub,HubIdFactory,$location) {
	
	
	$http.get("./getRegionList").success(function(response) {
		$scope.listOfRegionNames=response;
		console.log($scope.listOfRegionNames);
		});
	
	
	var id=HubIdFactory.getHub_id();
	if(id==0){
		alert("no data with the selected Hub id");
	}else{
		
    $http.get("./getHubById/"+id)
	
	.success(function(response) {
		
		$scope.Hub = response;
		
	});

	}
	
	$scope.save=function(){
		updatehub.save({id:id},$scope.Hub,function(){
			
				alert("Hub Updated");
			})
	}
	
	$scope.cancel=function(){
	
		 $location.path('hubmanagement');
	}
	
	      
	}


