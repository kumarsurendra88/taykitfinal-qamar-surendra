package com.dataisys.taykit.model;

import java.sql.Date;

public class MasterBag {

	private double masterbag_no;
	private double destination;
	private String masterbag_type;
	double created_by;
	private String order_id,return_id;
	private String status;
	private String flag;
	private double source;
	private String hub_name,store_name;
	private int counter;
	private Date activated_on;
	public Date getActivated_on() {
		return activated_on;
	}
	public void setActivated_on(Date activated_on) {
		this.activated_on = activated_on;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public String getHub_name() {
		return hub_name;
	}
	public void setHub_name(String hub_name) {
		this.hub_name = hub_name;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public double getSource() {
		return source;
	}
	public void setSource(double source) {
		this.source = source;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getReturn_id() {
		return return_id;
	}
	public void setReturn_id(String return_id) {
		this.return_id = return_id;
	}
	public double getCreated_by() {
		return created_by;
	}
	public void setCreated_by(double created_by) {
		this.created_by = created_by;
	}
	public double getMasterbag_no() {
		return masterbag_no;
	}
	public void setMasterbag_no(double masterbag_no) {
		this.masterbag_no = masterbag_no;
	}
	public double getDestination() {
		return destination;
	}
	public void setDestination(double destination) {
		this.destination = destination;
	}
	public String getMasterbag_type() {
		return masterbag_type;
	}
	public void setMasterbag_type(String masterbag_type) {
		this.masterbag_type = masterbag_type;
	}
	@Override
	public String toString() {
		return "MasterBag [masterbag_no=" + masterbag_no + ", destination="
				+ destination + ", masterbag_type=" + masterbag_type
				+ ", created_by=" + created_by + ", order_id=" + order_id
				+ ", return_id=" + return_id + ", status=" + status + ", flag="
				+ flag + ", source=" + source + ", hub_name=" + hub_name
				+ ", store_name=" + store_name + ", counter=" + counter
				+ ", activated_on=" + activated_on + "]";
	}
	

	
}
