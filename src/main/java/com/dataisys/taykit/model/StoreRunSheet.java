package com.dataisys.taykit.model;

import java.sql.Date;

public class StoreRunSheet {
	private int storeId;
	private String storeName;
	private Date toDate;
	private Date fromDate;
	private String orderType;
	
	
	private int totalOrders;
	private int totalPrepaid;
	private int totalCod;
	private Double sumPrepaid;
	private Double sumCod;
	private String storeRunsheet; 
	public String getStoreRunsheet() {
		return storeRunsheet;
	}
	public void setStoreRunsheet(String storeRunsheet) {
		this.storeRunsheet = storeRunsheet;
	}
	public StoreRunSheet() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StoreRunSheet(int storeId, String storeName, Date toDate,
			Date fromDate, String orderType, int totalOrders, int totalPrepaid,
			int totalCod, Double sumPrepaid, Double sumCod) {
		super();
		this.storeId = storeId;
		this.storeName = storeName;
		this.toDate = toDate;
		this.fromDate = fromDate;
		this.orderType = orderType;
		this.totalOrders = totalOrders;
		this.totalPrepaid = totalPrepaid;
		this.totalCod = totalCod;
		this.sumPrepaid = sumPrepaid;
		this.sumCod = sumCod;
	}
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public int getTotalOrders() {
		return totalOrders;
	}
	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}
	public int getTotalPrepaid() {
		return totalPrepaid;
	}
	public void setTotalPrepaid(int totalPrepaid) {
		this.totalPrepaid = totalPrepaid;
	}
	public int getTotalCod() {
		return totalCod;
	}
	public void setTotalCod(int totalCod) {
		this.totalCod = totalCod;
	}
	public Double getSumPrepaid() {
		return sumPrepaid;
	}
	public void setSumPrepaid(Double sumPrepaid) {
		this.sumPrepaid = sumPrepaid;
	}
	public Double getSumCod() {
		return sumCod;
	}
	public void setSumCod(Double sumCod) {
		this.sumCod = sumCod;
	}
	@Override
	public String toString() {
		return "StoreRunSheet [storeId=" + storeId + ", storeName=" + storeName
				+ ", toDate=" + toDate + ", fromDate=" + fromDate
				+ ", orderType=" + orderType + ", totalOrders=" + totalOrders
				+ ", totalPrepaid=" + totalPrepaid + ", totalCod=" + totalCod
				+ ", sumPrepaid=" + sumPrepaid + ", sumCod=" + sumCod + "]";
	}
	 
	
	 
	
	
	
	
}
