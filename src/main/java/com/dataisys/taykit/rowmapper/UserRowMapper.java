package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.City;
import com.dataisys.taykit.model.User;

public class UserRowMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		User user=new User();
		user.setUserId(rs.getDouble("user_id"));
		user.setUserType(rs.getString("user_type"));
		user.setUserName(rs.getString("user_fullname"));
		user.setUserMobil(rs.getString("user_mobile"));
		user.setUserLogin(rs.getString("user_loginname"));
		user.setAccNumber(rs.getString("account_no"));
		user.setUserPassword(rs.getString("user_loginpassword"));
		user.setModuleName(rs.getString("module_name"));
		user.setUserStatus(rs.getString("user_status"));
		
		return user;
	}



}
