package com.dataisys.taykit.model;

public class Device {
private double deviceId;
private String serialNO;
private String name;
@Override
public String toString() {
	return "Device [deviceId=" + deviceId + ", serialNO=" + serialNO
			+ ", name=" + name + ", type=" + type + ", emeiNO=" + emeiNO
			+ ", simNo=" + simNo + ", assignedTo=" + assignedTo
			+ ", description=" + description + "]";
}
public double getDeviceId() {
	return deviceId;
}
public Device() {
	super();
}
public Device(double deviceId, String serialNO, String name, String type,
		String emeiNO, String simNo, double assignedTo, String description) {
	super();
	this.deviceId = deviceId;
	this.serialNO = serialNO;
	this.name = name;
	this.type = type;
	this.emeiNO = emeiNO;
	this.simNo = simNo;
	this.assignedTo = assignedTo;
	this.description = description;
}
public void setDeviceId(double deviceId) {
	this.deviceId = deviceId;
}
public String getSerialNO() {
	return serialNO;
}
public void setSerialNO(String serialNO) {
	this.serialNO = serialNO;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getEmeiNO() {
	return emeiNO;
}
public void setEmeiNO(String emeiNO) {
	this.emeiNO = emeiNO;
}
public String getSimNo() {
	return simNo;
}
public void setSimNo(String simNo) {
	this.simNo = simNo;
}
public double getAssignedTo() {
	return assignedTo;
}
public void setAssignedTo(double assignedTo) {
	this.assignedTo = assignedTo;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
private String type;
private String emeiNO;
private String simNo;
private double assignedTo;
private String description;
}
