package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.RetailerContactRowMapper;
import com.dataisys.taykit.rowmapper.RetailerTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.RetailerWarehouseRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.RetailerContactDao;
import com.dataisys.taykit.dao.RetailerServicesDao;
import com.dataisys.taykit.dao.RetailerWarehouseDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.RetailerWarehouse;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class RetailerWarehouseDaoImpl extends JdbcDaoSupport implements RetailerWarehouseDao {

	public void addWarehouse(Double retailerId) {
		// TODO Auto-generated method stub
		
	}

	public void addWarehouses(final List<RetailerWarehouse> warehouses) {
		 String SQL = "insert into retailer_godown(retailer_id,godown_notes,godown_name,godown_city,godown_address,godown_phone,godown_gpslocation) values (?,?,?,?,?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   RetailerWarehouse detail = warehouses.get(i);
		   ps.setDouble(1, detail.getRetailerId());
		   ps.setString(2, detail.getNotes());
		   ps.setString(3, detail.getName());
		   ps.setString(4, detail.getCity());
		   ps.setString(5, detail.getAddress());
		   ps.setString(6, detail.getContactNo());
		   ps.setString(7, detail.getGeoLocation());
		  
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return warehouses.size();
		      
		                  }


		                });
      return;
		
	}

	public List<RetailerWarehouse> getRetailerWarehouseById(double id) {
		String sql="select * from `retailer_godown` where retailer_id=?";
		List<RetailerWarehouse>contacts=getJdbcTemplate().query(sql, new Object[]{id}, new RetailerWarehouseRowMapper());
		return contacts;
	}

	
	
}
