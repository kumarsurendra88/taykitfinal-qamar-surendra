package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;




import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.OrderActivity;



public class OrderActivityMapper implements RowMapper<OrderActivity> {

public OrderActivity mapRow(ResultSet rs, int rowNum) throws SQLException {
OrderActivity obj= new OrderActivity();
obj.setActivityBy(rs.getString("activity_by"));
obj.setActivityDate(rs.getDate("activity_date"));
obj.setActivityId(rs.getDouble("activity_id"));
obj.setActivityDesc(rs.getString("activity_desc"));
obj.setDeliveryboyId(rs.getDouble("deliveryboy_id"));
obj.setFromLocation(rs.getDouble("from_location"));
obj.setStatus(rs.getString("status"));
obj.setToLocation(rs.getDouble("to_location"));
obj.setTrackingId(rs.getString("tracking_id"));
obj.setUpdatedOn(rs.getTimestamp("updated_on"));
obj.setReason(rs.getString("reason"));
return obj;
}

}

