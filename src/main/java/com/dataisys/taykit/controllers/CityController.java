package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import com.dataisys.taykit.dao.CityDao;
import com.dataisys.taykit.model.City;



@RestController
public class CityController {

	@Autowired
	CityDao cityDao;
	@RequestMapping(value="/createCity",method=RequestMethod.POST)
	public ModelAndView createCity(Model model,@ModelAttribute City city){
		System.out.println("hiiiii");
		
		if(city.getStatus().equals("on")){
			
			city.setStatus("active");
			
		}
		else
		{
			
			city.setStatus("disabled");
		}
		
		
		cityDao.createCity(city);
		return new ModelAndView("landingpage.html");
		
	}
	

	
	
@RequestMapping(value="/getCity" , method=RequestMethod.GET)
	
	public List<City> getCityList() throws IOException{
		
		List<City> city= cityDao.getCity();
		return city;
	}
}
