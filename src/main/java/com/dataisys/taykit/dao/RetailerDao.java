
package com.dataisys.taykit.dao;

import java.util.List;

import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerContact;
import com.dataisys.taykit.model.RetailerServices;
import com.dataisys.taykit.model.RetailerWarehouse;
import com.dataisys.taykit.model.ProductCategories;

public interface RetailerDao {
public List<Retailer> getRetailers();

public Double getRetailerId();
public Retailer getRetailerbyId(double id);

public Retailer getRetailerByName(String retailerName);
public void AddReatiler(Retailer retail,List<RetailerServices> service,List<ProductCategories> category,
		List<RetailerContact> contacts,List<RetailerWarehouse> warehouses);

public void updateReatiler(Retailer retailer,
		List<RetailerServices> retailerservices,
		List<ProductCategories> categoryServices,
		List<RetailerContact> retailerContacts,
		List<RetailerWarehouse> warehouses, double id);
		public double getRetailerIdByName(String retailerName);

		public List<Retailer> getRetailerNameFromOrderReturnDb();
}