package com.dataisys.taykit.daoimpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.rowmapper.CustomerRowMapper;
import com.dataisys.taykit.rowmapper.RetailerAvailableCategoriesRowMapper;
import com.dataisys.taykit.rowmapper.RetailerAvailableServicesRowMapper;
import com.dataisys.taykit.rowmapper.RetailerRowMapper;
import com.dataisys.taykit.rowmapper.StoreAvailableCategoriesRowMapper;
import com.dataisys.taykit.rowmapper.StoreProductServiceRowMapper;
import com.dataisys.taykit.rowmapper.StoreTotalServiceRowMapper;
import com.dataisys.taykit.rowmapper.UserRowMapper;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.ProductCategoriesDao;
import com.dataisys.taykit.dao.StoreServicesDao;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.Retailer;
import com.dataisys.taykit.model.RetailerCategory;
import com.dataisys.taykit.model.RetailerService;
import com.dataisys.taykit.model.StoreCategory;
import com.dataisys.taykit.model.StoreServices;
import com.dataisys.taykit.model.User;

public class CategoryServiceDaoImpl extends JdbcDaoSupport implements ProductCategoriesDao {

	public List<ProductCategories> getproductCategories() {
		

			String sql= "SELECT * FROM `product_category`";
			
			List<ProductCategories> services=getJdbcTemplate().query(sql,new StoreProductServiceRowMapper());
			return services;
	
}

	public void addRetailerCategories(final List<RetailerCategory> categories) {
		System.out.println("called category"+categories);
		 String SQL = "insert into retailer_category(retailer_id, product_category_id,category_name) values (?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		   RetailerCategory detail = categories.get(i);
		   ps.setDouble(1, detail.getRetailerId());
		   ps.setDouble(2, detail.getProductCategoryId());
		   ps.setString(3, detail.getCategoryName());
		   
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return categories.size();
		      
		                  }


		                });
		  System.out.println("category added");
       return;
		
	}

	public void addStoreCategories(final List<StoreCategory> categories) {
		System.out.println("called category"+categories);
		 String SQL = "insert into store_category(store_id, product_category_id,category_name) values (?,?,?)";
		  getJdbcTemplate().batchUpdate(SQL, new BatchPreparedStatementSetter() {
		   public void setValues(PreparedStatement ps, int i) throws SQLException {
		      
		  StoreCategory detail = categories.get(i);
		   ps.setDouble(1, detail.getStoreId());
		   ps.setDouble(2, detail.getProductCategoryId());
		   ps.setString(3, detail.getCategoryName());
		   
		 
		                     
		     
		                  }
		            
		      
		                  public int getBatchSize() {
		     
		                      return categories.size();
		      
		                  }


		                });
		  System.out.println("category added");
      return;
		
	}

	public List<RetailerCategory> getAvailablecategoriesForRetailer(double id) {
		String sql="select * from `retailer_category` where retailer_id=?";
		List<RetailerCategory>categories=getJdbcTemplate().query(sql, new Object[]{id}, new RetailerAvailableCategoriesRowMapper());
		return categories;
	}

	public void updateRetailerCategories(
			List<RetailerCategory> providedCategories, double id) {
		String sql="delete from `retailer_category` where retailer_id="+id+"";
		getJdbcTemplate().execute(sql);
		addRetailerCategories(providedCategories);
		
	}

	public List<StoreCategory> getAvailablecategoriesForStore(double id) {
		String sql="select * from `store_category` where store_id=?";
		List<StoreCategory>categories=getJdbcTemplate().query(sql, new Object[]{id}, new StoreAvailableCategoriesRowMapper());
		return categories;
	}

	public void updateStoreCategories(List<StoreCategory> providedCategories,
			int id) {
		String sql="delete from `store_category` where store_id="+id+"";
		getJdbcTemplate().execute(sql);
		addStoreCategories(providedCategories);
	}
	

	}

	
	


