

package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Customer;

public class CustomerRowMapper implements RowMapper<Customer>{
	
	
	public Customer mapRow(ResultSet rs , int rowNum) throws SQLException{
		
		Customer customer= new Customer();
		customer.setCustomerName(rs.getString("customer_fullname"));
		customer.setCustomerAddress(rs.getString("customer_address"));
		customer.setCustomerEmail(rs.getString("customer_email"));
		customer.setCustomerMobile(rs.getString("customer_mobile"));
		customer.setPinCode(rs.getString("customer_pincode"));
		//customer.setCustomerTime(rs.getString("customer_time"));
		//customer.setStatus(rs.getString("customer_status"));
		customer.setCustomerId(rs.getDouble("customer_id"));
		customer.setCity(rs.getString("customer_city"));
		//customer.setLatitude(rs.getDouble("latitude"));
		//customer.setLongitude(rs.getDouble("longitude"));
		//customer.setAddressMapped(rs.getString("mapped"));
		
	/*	System.out.println("customer lati" +customer.getLatitude());
		System.out.println("customer long" +customer.getLatitude());*/
		
		return customer;
		
	}



}
