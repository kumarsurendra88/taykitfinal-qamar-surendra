package com.dataisys.taykit.daoimpl;

import java.util.Iterator;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.DataMappingDao;
import com.dataisys.taykit.model.DataMapping;
import com.dataisys.taykit.rowmapper.DataMappingMapper;

public class DataMappingImpl extends JdbcDaoSupport implements DataMappingDao{

	public void saveMapping(List<DataMapping> list) {
		String sql="insert into data_mapping (retailer_id,source_column,target_column,type,allow_null,Description,updated_by,updated_on)values(?,?,?,?,?,?,?,?)";
		Iterator<DataMapping> itr=list.iterator();
		while(itr.hasNext()){
			DataMapping obj=itr.next();
			//obj.setRetailerId(1);
			getJdbcTemplate().update(sql, obj.getRetailerId(),obj.getSourceColumn(),obj.getTargetColumn(),obj.getType(),
					obj.getNullValue(),obj.getDescription(),obj.getUpdatedBy(),obj.getUpdatedOn());
		}
	}

	public List<DataMapping> getMappingByRetailer(int retailerId) {
		String sql="select * from data_mapping where retailer_id=?";
		
		return getJdbcTemplate().query(sql, new Object[]{retailerId}, new DataMappingMapper());
	}

	public int checkMapping(int retailerId) {
		 
		DataMapping object=null;
		int id=0;
		String sql="select * from data_mapping where retailer_id=? limit 1";
		try{
			 
			object=getJdbcTemplate().queryForObject(sql,  new Object[]{retailerId}, new DataMappingMapper());
			return id=(int) object.getRetailerId();
			 
		}catch(EmptyResultDataAccessException e){
			 
			e.printStackTrace();
			 
			return id=0;
		}
		 
		//return id;
	}

	public void updateMapping(List<DataMapping> list,double retailerId) {
		String sql="update data_mapping set source_column=?,target_column=?,type=?,allow_null=?,Description=?,updated_by=?,updated_on=? where retailer_id=? and mapping_id=?";
		Iterator<DataMapping> itr=list.iterator();
		while(itr.hasNext()){
			DataMapping obj=itr.next();
			System.out.println("Updating with "+obj);
			 
			getJdbcTemplate().update(sql,obj.getSourceColumn(),obj.getTargetColumn(),obj.getType(),
					obj.getNullValue(),obj.getDescription(),obj.getUpdatedBy(),obj.getUpdatedOn(),retailerId,obj.getMappingId());
		}
	}

	 
}
