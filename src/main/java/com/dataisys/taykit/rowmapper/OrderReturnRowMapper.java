package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.mobile.model.*;
import com.dataisys.taykit.model.Return;

public class OrderReturnRowMapper implements RowMapper<Return> {

	public Return mapRow(ResultSet rs, int rowNum) throws SQLException {
		Return orderReturn = new Return();
		
		//orderReturn.setReturnId(returnId);
		orderReturn.setReturnId(rs.getString("return_id"));
		
		orderReturn.setProductToBeShipped(rs.getString("order_details"));
		orderReturn.setTrackingId(rs.getString("tracking_id"));
		orderReturn.setOrderId(rs.getString("order_id"));
		orderReturn.setCourierService(rs.getString("courier_service"));
		orderReturn.setProductToBeShipped(rs.getString("order_details"));
		orderReturn.setPaymentMode(rs.getString("order_type"));
		orderReturn.setRetailerId(rs.getDouble("retailer_id"));
		orderReturn.setConsigneeName(rs.getString("customer_name"));
		orderReturn.setCustomerAddress(rs.getString("address"));
		System.out.println("customer address is "+orderReturn.getCustomerAddress());
		orderReturn.setPhone(rs.getString("customer_phone"));
		orderReturn.setContactNo(rs.getString("customer_mobile"));
		orderReturn.setCountry(rs.getString("country"));
		orderReturn.setCity(rs.getString("city_name"));
		orderReturn.setState(rs.getString("state"));
		orderReturn.setStatus(rs.getString("order_status"));
		orderReturn.setAssignedTo(rs.getString("assigned_to"));;
		orderReturn.setPincode(rs.getInt("pincode"));
		orderReturn.setDeliveredDate(rs.getDate("delivered_date"));
		orderReturn.setReportingStoreId(rs.getDouble("reporting_store_id"));
		orderReturn.setOrderType(rs.getString("order_type"));
		orderReturn.setQuantity(rs.getInt("quantity"));
		orderReturn.setReturnOrderRefNo(rs.getString("return_order_ref_no"));

		
		return orderReturn;
	}

}
