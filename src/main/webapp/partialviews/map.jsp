<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <title>Live Tracking of Vehicle no: <%=request.getParameter("vehicleNumber") %></title>
  <script src="http://maps.google.com/maps/api/js?sensor=false"  type="text/javascript"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
  <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="../../js/jquery-1.11.2.js"></script>
  <!--  <script src="../js/marker.js"></script> -->
</head>
<body>
  
  <div class = "row" style="margin-left: 20px">
  	&nbsp;&nbsp;&nbsp;&nbsp;<input type = "date"/>
  	&nbsp;&nbsp;&nbsp;&nbsp;<button>Apply</button>
  	&nbsp;&nbsp;&nbsp;&nbsp;<button>Stoppage Details</button>
  	&nbsp;&nbsp;&nbsp;&nbsp;Last Updated On:
  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type = "checkbox">Show Distributor Location(s)
  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button>Close</button>  	
  </div>
  <div class = "row">
	  <div class = "col-xs-10" id="map" style = "height: 900px"></div>	  
	  <div class = "col-xs-2">
	  		<h4>Status</h4>
	  </div>
  </div>
  

 <script>

 var vehicleNumber = "<%= request.getParameter("vehicleNumber") %>";
 var vehicleId = "<%= request.getParameter("vehicleId") %>";
 var latitude = "<%= request.getParameter("lat") %>";
 var longitude = "<%= request.getParameter("lon") %>";
 
 console.log("VehicleNumber: "+vehicleNumber+" vehicleId: "+vehicleId+" latitude: "+latitude+" longitude: "+longitude);
 
 	/* $.getJSON('http://localhost:8080/mytracker/getCurrentDetails.do', 
	     {
	        x: "1",
	        y: "2"
	     }, 
	     function(data) {
	         getResult(data.lat, data.lon);
	     }); */
 
 
 $.getJSON('http://localhost:8080/mytracker/getCurrentDetails.do', function(json) {
	  var locations=[];
	    	console.log("Inside getJSON's getCurrentDetails.do");
	  for(var i = 0, len = json.length; i < len; i++) {
		    console.log("vehicleId: "+json[i]['vehicleId']);
		    console.log("positionId: "+json[i]['positionId']);
	        console.log("latitude "+json[i]['latitude']);
	        console.log("longitude "+json[i]['longitude']);
	        console.log("currentTime: "+json[i]['currentTime']);
	        console.log("currentLocation: "+json[i]['currentLocation']);
	        
	        var address = {latitude: json[i]['latitude'], longitude: json[i]['longitude'] };
	        locations.push(address);       
	    }
	    console.log("data size is"+locations.length);
	    console.log("data are"+locations); 
	    console.log("first object is"+locations[0]['latitude']);
 });

/* To Post the data using jsquery */
var values = {
				positionId: 0,
			    startTime: null,
			    vehicleId: 32
             };
             
$.ajax({
    type: "POST",
    url: "http://localhost:8080/mytracker/getVehicleTodaysTrack.do",
    data: JSON.stringify(values),
    error: function(msg)
    {
       console.log(msg);
    },
    success: function(msg)
    {
       console.log("message is: "+msg);
    }
});
/* Ending posting th data using jquery*/


/* To Post the data using jquery  one way but it's not working*/
	/* jQuery["postJSON"] = function( url, data, callback ) {
    // shift arguments if data argument was omitted
	    if ( jQuery.isFunction( data ) ) {
	        callback = data;
	        data = undefined;
	    }
	
	    return jQuery.ajax({
	        url: url,
	        type: "POST",
	        contentType:"application/json; charset=utf-8",
	        dataType: "json",
	        data: data,
	        success: callback
	    });
	};
	console.log("Before invoking the postJSON");
	$.postJSON('http://localhost:8080/mytracker/getVehicleTodaysTrack.do', {
		positionId: 0,
	    startTime: null,
	    vehicleId: 32		
	}, function (data, status, xhr) {
		console.log("I'm inside postJSON's getVehicleTodaysTrack and the data's length is: "+data.length);
	}); */

/* End for posting data */

/* $.getJSON('http://localhost:8080/mytracker/getVehicleTodaysTrack.do', {
    positionId: 0,
    startTime: null,
    vehicleId: 32     
 }, function(json) {
	console.log("I'm inside getJSON's getVehicleTodaysTrack and the data's length is: "+json.length);
}); */
 
 var locations = [
                  ['Location1', 12.99269962310791, 77.82859802246094],
			      ['Location2', 12.992899894714355,77.82879638671875],
			      ['Location3', 12.988499641418457,77.82969665527344],
			      ['Location4', 12.987899780273438,77.8301010131836],
			      ['Location5', 12.920100212097168,77.6176986694336]
                ];

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: new google.maps.LatLng(12.99269962310791,77.82859802246094),
                  mapTypeId: google.maps.MapTypeId.HYBRID
                });
				
                
                /* Below code is for marker */
                /* var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < locations.length; i++) {  
                  marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                  });

                  google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent(locations[i][0]);
                      infowindow.open(map, marker);
                    }
                  })(marker, i));
                }    */         

 </script> 
 
</body>
</html>