package com.dataisys.taykit.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.rowmapper.DeliveryBoysMapper;
public class DeliveryBoyDaoimpl extends JdbcDaoSupport implements DeliveryBoyDao {

	public List getAll() {
		String sql="select * from deliveryboy";
		List<DeliveryBoys> listofUsers=getJdbcTemplate().query(sql,new DeliveryBoysMapper());
		return listofUsers;
		
	}
	
	public List<DeliveryBoys> getAllByStore(HttpSession session) {
		
		User user=(User)session.getAttribute("user");
		 if(user.getUserType().equalsIgnoreCase("Store Admin")||user.getUserType().equalsIgnoreCase("Hub Admin")){
             String sql="select * from deliveryboy";
             List<DeliveryBoys> listofUsers=getJdbcTemplate().query(sql,new DeliveryBoysMapper());
             return listofUsers;
             }
		if(user.getUserType().equalsIgnoreCase("SUPER_ADMIN")){
		String sql="select * from deliveryboy";
		List<DeliveryBoys> listofUsers=getJdbcTemplate().query(sql,new DeliveryBoysMapper());
		return listofUsers;
		}
		else{
			
			String sql="select *from `deliveryboy` where store_id="+(Double)session.getAttribute("storeId");
			List<DeliveryBoys> listofUsers=getJdbcTemplate().query(sql,new DeliveryBoysMapper());
			return listofUsers;
		}
	}

	public DeliveryBoys DeliveryBoyId(String id) {
		System.out.println("before query");
		   String SQl="SELECT * FROM deliveryboy WHERE deliveryboy_name=? LIMIT 1";
			System.out.println("after string sql");
		  DeliveryBoys record=getJdbcTemplate().queryForObject(SQl, new Object[]{id},new DeliveryBoysMapper()) ;
		   System.out.println("returned delivery boy is"+record);
		  return record;
	}
	
	//latest code for getting deliveryboyByStoreId
			public List<DeliveryBoys> getDeliveryBoysByStoreId(double storeId) {		
				String sql= "SELECT * FROM deliveryboy WHERE store_id = ?";
				List<DeliveryBoys> listOfDeliveryBoys = getJdbcTemplate().query(sql,new Object[]{storeId},new DeliveryBoysMapper());
				return listOfDeliveryBoys;
			}
			
			//Added on 2016-02-05
			public List<Integer> getStoreIdsForEachDeliveryBoy(String deliveryBoyName) {
				
				String sql = "SELECT store_id FROM deliveryboy WHERE deliveryboy_name = '"+deliveryBoyName.trim()+"'";
				final List<Integer> storeList = new ArrayList<Integer>();
				getJdbcTemplate().query(sql, new ResultSetExtractor<Integer>() {
					public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

						double storeId = 0;
						while(rs.next()) {
							storeId = rs.getDouble("store_id");
							int extractedId = (int)storeId;
							storeList.add(extractedId);
						}
						return null;
					}
				});
				return storeList;
			}
			
			
			public void updateOrderStatusToPickup(String trackingId, int storeId) {
				System.out.println("Inside updateOrderStatusToPickup In DeliveryBoyDaoImpl");
				String sql = "UPDATE `order` SET order_type = 'Collect', store_id = "+storeId+" WHERE tracking_id = '"+trackingId+"' AND order_type = 'Delivery' AND (order_status != 'Delivered' And order_status NOT LIKE 'Order cancelled%')";
				getJdbcTemplate().update(sql);
			}
			
			public void updateOrderStatusToDropOff(String trackingId, int storeId) {
				System.out.println("Inside updateOrderStatusToDropOff In DeliveryBoyDaoImpl");
				String sql = "UPDATE `order_return` SET order_type = 'Dropoff', store_id = "+storeId+" WHERE tracking_id = '"+trackingId+"' AND order_type = 'Pickup' AND (order_status != 'Delivered' AND order_status != 'Returned' And order_status NOT LIKE 'Order cancelled%')";
				getJdbcTemplate().update(sql);
			}
			
	 

}
