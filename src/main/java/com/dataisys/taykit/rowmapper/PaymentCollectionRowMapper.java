package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;



import com.dataisys.taykit.model.PaymentCollection;
import com.dataisys.taykit.model.RunsheetDetails;

public class PaymentCollectionRowMapper implements RowMapper<PaymentCollection>{
	
	
	public PaymentCollection mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 PaymentCollection entry= new PaymentCollection();
	 	entry.setCardPayment(rs.getDouble("card_payment"));
	 	entry.setCodPaid(rs.getDouble("Cod_paid"));
	 	entry.setCodToCollect(rs.getDouble("cod_tocollect"));
	 	entry.setDate(rs.getString("date"));
	 	entry.setDeliveryUser(rs.getString("delivery_user"));
	 	entry.setRunsheetNo(rs.getString("runsheet_no"));
	 	entry.setShortFallAmount(rs.getDouble("shortfall_amount"));
	 	entry.setStoreId(rs.getString("store_id"));
	 	return entry;
	 	
 }
	 	
 
 
 }
}
