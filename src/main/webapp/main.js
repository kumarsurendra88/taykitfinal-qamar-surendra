// main.js
var app = angular.module('myApp', ['ngGrid']);
app.controller('MyCtrl', function($scope) {
    $scope.myData = [{name: "Moroni", age: 50,complex:{name:"abc"}},
                     {name: "Tiancum", age: 43, complex:{name:"def"}},
                     {name: "Jacob", age: 27},
                     {name: "Nephi", age: 29},
                     {name: "Enos", age: 34}];
                     
                     
                     
                     
    
    var filterBarPlugin = {
        init: function(scope, grid) {
            filterBarPlugin.scope = scope;
            filterBarPlugin.grid = grid;
            $scope.$watch(function() {
                var searchQuery = "";
                angular.forEach(filterBarPlugin.scope.columns, function(col) {
                    if (col.visible && col.filterText) {
                        var filterText = (col.filterText.indexOf('*') == 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                        searchQuery += col.displayName + ": " + filterText;
                    }
                });
                return searchQuery;
            }, function(searchQuery) {
                filterBarPlugin.scope.$parent.filterText = searchQuery;
                filterBarPlugin.grid.searchProvider.evalFilter();
            });
        },
        scope: undefined,
        grid: undefined,
    };
    
    $scope.myHeaderCellTemplate = '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{cursor: col.cursor}" ng-class="{ ngSorted: !noSortVisible }">'+
                           '<div ng-click="col.sort($event)" ng-class="\'colt\' + col.index" class="ngHeaderText">{{col.displayName}}</div>'+
                           '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>'+
                           '<div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>'+
                           '<div class="ngSortPriority">{{col.sortPriority}}</div>'+
                         '</div>'+
                         '<input type="text" ng-click="stopClickProp($event)" placeholder="Filter..." ng-model="col.filterText" ng-style="{ \'width\' : col.width - 14 + \'px\' }" style="position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;"/>' +
                         '<div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>';
    
                     
    var colDefs = [
      {field:'name',headerCellTemplate: $scope.myHeaderCellTemplate},
      {field:'age',headerCellTemplate: $scope.myHeaderCellTemplate},
      {field:'complex.name',headerCellTemplate: $scope.myHeaderCellTemplate}
      ];
      
      
    $scope.gridOptions = { 
        data: 'myData',
        columnDefs : colDefs,
        showGroupPanel: true,
        jqueryUIDraggable: true,
        plugins: [filterBarPlugin],
        headerRowHeight: 60 // give room for filter bar
    };
});