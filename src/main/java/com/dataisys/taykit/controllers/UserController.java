package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.dao.UserDao;

@RestController
public class UserController {

	@Autowired
	UserDao userDao;
	@RequestMapping(value="/createUser",method=RequestMethod.POST)
	public boolean createOrder(@RequestBody com.dataisys.taykit.model.User user,HttpSession session){
		System.out.println("hiiiii");
		
		System.out.println("**********accNo :"+user.getAccNumber());
		boolean flag=userDao.createUser(user,session);
		return flag;//new ModelAndView("landingpage.html");
		
	}
@RequestMapping(value="/getUser" , method=RequestMethod.GET)
	
	public List<com.dataisys.taykit.model.User> getUserList() throws IOException{
		
		List<com.dataisys.taykit.model.User> users= userDao.getUSers();
		/*System.out.println("list : "+users.get(0));
		System.out.println("list : "+users.get(1));
		System.out.println("list : "+users.get(2));
		System.out.println("list : "+users.get(3));*/
		return users;
	}

}
