package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class OrderAssign {

	private double assignId;
	private double orderId;
	private String assignFrom;
	private String assignFromType;
	private String assignTo;
	private String assignToType;
	private String updatedBy;
	private Timestamp updated_on;
	
	
	
	public OrderAssign() {
		super();
	}



	public OrderAssign(double assignId, double orderId, String assignFrom,
			String assignFromType, String assignTo, String assignToType,
			String updatedBy, Timestamp updated_on) {
		super();
		this.assignId = assignId;
		this.orderId = orderId;
		this.assignFrom = assignFrom;
		this.assignFromType = assignFromType;
		this.assignTo = assignTo;
		this.assignToType = assignToType;
		this.updatedBy = updatedBy;
		this.updated_on = updated_on;
	}



	public double getAssignId() {
		return assignId;
	}



	public void setAssignId(double assignId) {
		this.assignId = assignId;
	}



	public double getOrderId() {
		return orderId;
	}



	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}



	public String getAssignFrom() {
		return assignFrom;
	}



	public void setAssignFrom(String assignFrom) {
		this.assignFrom = assignFrom;
	}



	public String getAssignFromType() {
		return assignFromType;
	}



	public void setAssignFromType(String assignFromType) {
		this.assignFromType = assignFromType;
	}



	public String getAssignTo() {
		return assignTo;
	}



	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}



	public String getAssignToType() {
		return assignToType;
	}



	public void setAssignToType(String assignToType) {
		this.assignToType = assignToType;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Timestamp getUpdated_on() {
		return updated_on;
	}



	public void setUpdated_on(Timestamp updated_on) {
		this.updated_on = updated_on;
	}



	@Override
	public String toString() {
		return "OrderAssign [assignId=" + assignId + ", orderId=" + orderId
				+ ", assignFrom=" + assignFrom + ", assignFromType="
				+ assignFromType + ", assignTo=" + assignTo + ", assignToType="
				+ assignToType + ", updatedBy=" + updatedBy + ", updated_on="
				+ updated_on + "]";
	}
	
	
	
}
