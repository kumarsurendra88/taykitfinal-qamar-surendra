package com.dataisys.taykit.factories;

import java.util.ArrayList;
import java.util.List;

import com.dataisys.taykit.model.StoreContact;

public class StoreContactFactory {
private static StoreContactFactory factory;
private List<StoreContact> contacts =new ArrayList<StoreContact>();

	
	public static StoreContactFactory getFactory() {
		if (factory==null){
			factory=new StoreContactFactory();
		   return factory;
		}
             return factory;
   } 


public static void setFactory(StoreContactFactory factory) {
	StoreContactFactory.factory = factory;
}


public List<StoreContact> getContacts() {
	return contacts;
}


public void setContacts(StoreContact contacts) {
	this.contacts.add(contacts);
}


public StoreContactFactory() {
	super();
}

}
