package com.dataisys.taykit.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dataisys.taykit.dao.DataImportDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.ExcelUtil;
import com.dataisys.taykit.model.DataImport;
import com.dataisys.taykit.model.Order;

@RestController
public class DataImportController {
	@Autowired
	DataImportDao importDao;
	@Autowired
	PudoStoreDao storeDao;
	int length1;
	int length11;
	@RequestMapping(value="/importData",method=RequestMethod.POST)
	public List<String> dataImport(UploadForm uploadItem, BindingResult result,HttpSession session) throws IOException{
		//System.out.println("Executing Dataimport controller");
		InputStream is = uploadItem.getFile().getInputStream();
		
		HSSFWorkbook workbook = new HSSFWorkbook(is);
		HSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		
		//Sending Back Headers
		List<String> headerList = new ArrayList<String>();
		for (int i = 0; i == 0; i++) {
			Row row = sheet.getRow(i);

			 
			for (int j = 0; j < row.getLastCellNum(); j++) {
				 
				headerList.add(row.getCell(j).toString());
			}
		}
		
		
		boolean isHeader = true;
		
		try{
			java.sql.Date null1=null;
			while(rowIterator.hasNext()){	
				//System.out.println("Inside row");
				Row row = rowIterator.next();
				if(!isHeader){
					
					
					if(row.getLastCellNum()<=21)
						
					{
						System.out.println("checking for myntra data");
						Cell orderId=row.getCell(0);
						System.out.println("orderId" +orderId);
						Cell trackingId=row.getCell(1);
						
						Cell orderTitle = row.getCell(2);
						
						Cell customerName = row.getCell(3);
						
						Cell customerAddress = row.getCell(5);
						Cell city = row.getCell(6);
						//Cell status = row.getCell(7);
						Cell country = row.getCell(8);
						Cell pincode = row.getCell(9);
						Cell mobile = row.getCell(10);
						Cell paymentType = row.getCell(11);
						Cell value = row.getCell(12);
						Cell vat = row.getCell(13);
						Cell ammountToBePaid = row.getCell(14);
						Cell alternativeContactNo = row.getCell(15);
						Cell content=row.getCell(16);
						Cell weight = row.getCell(17);
						Cell status = row.getCell(18);
						Cell orderReferenceNo=row.getCell(19);
						
						try{
									DataImport order = new DataImport();
									
									order.setSessionId(session.getId());
									order.setImportType("Direct");
									order.setDataStatus("Under_Process");
									order.setOrderCategory("");
									
									
									if(trackingId!=null){
										
											//order.setOrderId(ExcelUtil.getStringCellData(orderId));
											order.setTrackingId(ExcelUtil.getStringCellData(trackingId));
									}
									
									
									
									if(orderId !=null){
										
										//data.setOrderId(ExcelUtil.getStringCellData(orderId));
									//	System.out.println("Order Id");
										
										order.setOrderId(ExcelUtil.getStringCellData(orderId));
										BigDecimal bd = new BigDecimal(order.getOrderId());
										
										
										System.out.println("orderId after Converting converting"+bd);
										
										String s="0"+bd;
										String newContactNo=s.substring(1);
										if(newContactNo.contains("+")){
													
											Double d = (Double.parseDouble(newContactNo));
											BigDecimal bd1 = new BigDecimal(d);
											System.out.println("trying to convert failed orderId"+d);
											System.out.println("Big decimal type of failed orderId"+bd1);
											//System.out.println("failed number of contacts conversion"+failCountNumber);
											String contactForFail="0"+bd1;
											String newContactForFail=contactForFail.substring(1);
											order.setOrderId(newContactForFail);
										}
										else{
										System.out.println("Contact number after removing 0 is"+newContactNo);
										order.setOrderId(newContactNo);
										}
									
									}
									
									
									if(orderTitle!=null){
										
										order.setOrderTitle(ExcelUtil.getStringCellData(orderTitle));
									}
									if(customerName!=null){
										
										order.setCustomerName(ExcelUtil.getStringCellData(customerName));
									}
									if(customerAddress!=null){
										order.setCustomerAddress(ExcelUtil.getStringCellData(customerAddress));
									}
									if(city!=null){
										order.setCityName(ExcelUtil.getStringCellData(city));
									}
									/*if(status!=null){
										order.setOrderStatus(ExcelUtil.getStringCellData(status));
									}*/
									if(pincode!=null){
											order.setCustomerZipcode(ExcelUtil.getNumbericCellData(pincode));
											/*System.out.println("string value of pincode" +pincode2);
											int pin=Integer.parseInt(pincode2);
											System.out.println("integer value of pincode" +pin);
											order.setCustomerZipcode(pin);*/
									}
									if(mobile!=null){
										order.setCustomerContact(ExcelUtil.getStringCellData(mobile));
										
										//order.setCustomerContactno(ExcelUtil.getStringCellData(customerContactno));
										/*long d=ExcelUtil.getNumbericCellData(customerContactno);
										System.out.println("value in number format is"+d);*/
										System.out.println("befor converting" +order.getCustomerContact());
										
										
									BigDecimal bd = new BigDecimal(order.getCustomerContact());
									
										
									System.out.println("customer contact number after converting"+bd);
									
									String s="0"+bd;
									String newContactNo=s.substring(1);
									if(newContactNo.contains("+"))
									{
										//failCountNumber++;
									Double d = (Double.parseDouble(newContactNo));
										BigDecimal bd1 = new BigDecimal(d);
										System.out.println("trying to convert failed contact number"+d);
										System.out.println("Big decimal type of failed contact number"+bd1);
										//System.out.println("failed number of contacts conversion"+failCountNumber);
										String contactForFail="0"+bd1;
										String newContactForFail=contactForFail.substring(1);
										order.setCustomerContact(newContactForFail);
										
									}
									else{
										System.out.println("Contact number after removing 0 is"+newContactNo);
										order.setCustomerContact(newContactNo);
									}
										
									}
								/*	if(email!=null){
										
										order.setCustomerEmail(ExcelUtil.getStringCellData(email));
									}*/
									if(paymentType!=null){
										
										int pay=ExcelUtil.getNumbericCellData(paymentType);
										if(pay==0)
										{
											order.setPaymentType("PREPAID");
										}
										else
										{
											order.setPaymentType("COD");
										}
									}
									if(ammountToBePaid!=null){
										
										String value1=ExcelUtil.getStringCellData(ammountToBePaid);
										System.out.println("amount to be paid is"+value1);
										Double pin=Double.parseDouble(value1);
										System.out.println("value of amount after conversion" +pin);
										order.setOrderValue(pin);
									}
									
									if(country !=null){
										
										order.setCountry(ExcelUtil.getStringCellData(country));
									}
									
									/*if(value!=null){
										String value1=ExcelUtil.getStringCellData(value);
										Double actualValue=Double.parseDouble(value1);
										order.setOrderValue(actualValue);
										
									}*/
									if(vat!=null){
										String value1=ExcelUtil.getStringCellData(vat);
										Double vatValue=Double.parseDouble(value1);
										
										order.setVat(vatValue);
									}
									
									if(alternativeContactNo!=null){
										order.setAlternaitveContactNo(ExcelUtil.getStringCellData(alternativeContactNo));
										
									}
									if(content!=null){
										
										order.setOrderDetails(ExcelUtil.getStringCellData(content));
									}
									if(weight!=null){
										String value1=ExcelUtil.getStringCellData(weight);
										Double weightValue=Double.parseDouble(value1);
										order.setWeight(weightValue);
										
									}
									
									if(status!=null){
										
										order.setOrderStatus(ExcelUtil.getStringCellData(status));
									}
									
									if(orderReferenceNo!=null){
										
										order.setOrderStatus("orderReferenceNo");
									}
									java.util.Date utilDate = new java.util.Date();
									null1= new java.sql.Date(utilDate.getTime());
									
									System.out.println("sql date" +null1);
									order.setReceiptDate(null1);
									System.out.println("date after setting to delivery"+ order.getReceiptDate());
									
									order.setRetailerId(3);
									//orderList.add(order);
									System.out.println("before creating order");
									order.setOrderStatus("at central warehouse");
									
									importDao.dataImport(order);
									System.out.println("after creting order");
									
									//System.out.println("aftermaking call");
						}
						catch(Exception e){
							
							e.printStackTrace();
							System.out.println("errror for tracking Id" );
						}
					}
					
					else
					{
						
						java.util.Date newReciptDate=null;
					
					System.out.println("inside else");
					
					Cell orderId=row.getCell(0);
					System.out.println("order Id"+orderId);
					Cell trackingId=row.getCell(1);
					System.out.println("trackingId"+trackingId);
					Cell orderTitle = row.getCell(2);
					System.out.println("orderTitle"+orderTitle);
					Cell orderDetail = row.getCell(3);
					System.out.println("orderDetail"+orderDetail);
					Cell orderType=row.getCell(4);
					System.out.println("Order Type"+orderDetail);
					Cell orderPriority = row.getCell(5);
					System.out.println("orderPriority"+orderDetail);
					Cell orderRefno = row.getCell(6);
					System.out.println("orderRefno"+orderDetail);
					Cell customerName = row.getCell(7);
					System.out.println("name"+orderDetail);
					Cell customerMobile = row.getCell(8);
					System.out.println("Mobiel"+orderDetail);
					Cell customerEmailId = row.getCell(9);
					System.out.println("email"+orderDetail);
					Cell customerAddress=row.getCell(11);
					System.out.println("Address"+orderDetail);
					Cell cityName=row.getCell(10);
					System.out.println("cityName"+orderDetail);
					Cell customerZipCode=row.getCell(12);
					System.out.println("zipcode"+orderDetail);
					Cell paymentType = row.getCell(13);
					System.out.println("paymentType"+orderDetail);
					Cell bayNumber=row.getCell(16);
					System.out.println("baynumber"+orderDetail);
					Cell orderStatus = row.getCell(14);
					System.out.println("status"+orderDetail);
					
					Cell reciptDate=row.getCell(15);
					System.out.println("receiptdate"+reciptDate);
					if(reciptDate!=null){
						System.out.println("receiptdate inside if"+orderDetail);
					String dDate1=reciptDate.toString();
					System.out.println(" receiptdate is"+dDate1);
					 length1=dDate1.length();
					System.out.println("lenght of recipt Date is" +length1);
					if(length1>=6){
					 newReciptDate=reciptDate.getDateCellValue();
					}
					}
					//System.out.println("lenght of recipt Date is" +length1);
					//java.util.Date newReciptDate=reciptDate.getDateCellValue();
					
					Cell deliveredDate= row.getCell(17);
					System.out.println("checking for deliverd date"+deliveredDate);
					if(deliveredDate!=null){
						System.out.println("checking for deliverd date inside else"+deliveredDate);
						String dDate11=deliveredDate.toString();
						length11=dDate11.length();
						System.out.println("lenght of delivery Date is" +length11);
					
					System.out.println("deliverydate");
					}
					Cell retailerId=row.getCell(19);
					System.out.println("retailerId"+orderDetail);
					Cell orderValue=row.getCell(20);
					
					System.out.println("orderValue"+orderDetail);
					Cell paymentReference=row.getCell(21);
					System.out.println("paymentREferene"+orderDetail);
					Cell storeId= row.getCell(22);
					
					Cell weight=row.getCell(23);
					
					
					System.out.println("stoereID"+orderDetail);
					try{
						
						System.out.println("inside try block");
						DataImport data= new DataImport();
						
						data.setSessionId(session.getId());
						data.setImportType("Direct");
						
						/*if(retailerId!=null){
						data.setRetailerId(ExcelUtil.getNumbericCellData(retailerId));
						}*/
						
						data.setRetailerId(1);
						
						data.setDataStatus("Under_Process");
						data.setOrderCategory("");
						//data.setStoreId(ExcelUtil.getNumbericCellData(storeId));
						data.setStoreId(0);
						if(orderType!=null)
						{
						data.setOrderType(ExcelUtil.getStringCellData(orderType));
						}
						data.setDataNote("");
						data.setUpdatedBy("");
						//data.setUpdatedOn();
						if(trackingId!=null){
							System.out.println("tracking Id");
							data.setTrackingId(ExcelUtil.getStringCellData(trackingId));
						}else{
							data.setDataStatus("Invalid");
						}
						
						if(orderId !=null){
							
							//data.setOrderId(ExcelUtil.getStringCellData(orderId));
							System.out.println("Order Id");
							
							data.setOrderId(ExcelUtil.getStringCellData(orderId));
							BigDecimal bd = new BigDecimal(data.getOrderId());
							
							
							System.out.println("orderId after Converting converting"+bd);
							
							String s="0"+bd;
							String newContactNo=s.substring(1);
							
							if(newContactNo.contains("+")){
										
								Double d = (Double.parseDouble(newContactNo));
								BigDecimal bd1 = new BigDecimal(d);
								System.out.println("trying to convert failed orderId"+d);
								System.out.println("Big decimal type of failed orderId"+bd1);
								//System.out.println("failed number of contacts conversion"+failCountNumber);
								String contactForFail="0"+bd1;
								String newContactForFail=contactForFail.substring(1);
								data.setOrderId(newContactForFail);
							}
							else{
							System.out.println("Contact number after removing 0 is"+newContactNo);
							data.setOrderId(newContactNo);
							}
							
						
						
						
						}
						if(orderTitle!=null){
							
							
							data.setOrderTitle(ExcelUtil.getStringCellData(orderTitle));
							
						}if(orderDetail !=null){
						data.setOrderDetails(ExcelUtil.getStringCellData(orderDetail));
						}
						if(orderPriority!=null){
						data.setOrderPriority(ExcelUtil.getStringCellData(orderPriority));
						}
						/*data.setOrderProductId();
						data.setActivityId();
						data.setRevisionId( );
						data.setAttachmentId();*/
						if(orderRefno!=null){
						
						data.setOrderRefno(ExcelUtil.getStringCellData(orderRefno));
						}
						if(customerName!=null){
							data.setCustomerName(ExcelUtil.getStringCellData(customerName));
						}else{
							data.setDataStatus("Invalid");
						}
						if(customerEmailId!=null){
							data.setCustomerEmail(ExcelUtil.getStringCellData(customerEmailId));
						}/*else{
							data.setDataStatus("Invalid");
						}*/
						if(customerMobile!=null){
							data.setCustomerContact(ExcelUtil.getStringCellData(customerMobile));
							
							//order.setCustomerContactno(ExcelUtil.getStringCellData(customerContactno));
							/*long d=ExcelUtil.getNumbericCellData(customerContactno);
							System.out.println("value in number format is"+d);*/
							
							
						BigDecimal bd = new BigDecimal(data.getCustomerContact());
						
							
						System.out.println("customer contact number after converting"+bd);
						
						String s="0"+bd;
						String newContactNo=s.substring(1);
						if(newContactNo.contains("+"))
						{
							//failCountNumber++;
							Double d = (Double.parseDouble(newContactNo));
							BigDecimal bd1 = new BigDecimal(d);
							System.out.println("trying to convert failed contact number"+d);
							System.out.println("Big decimal type of failed contact number"+bd1);
							//System.out.println("failed number of contacts conversion"+failCountNumber);
							String contactForFail="0"+bd1;
							String newContactForFail=contactForFail.substring(1);
							data.setCustomerContact(newContactForFail);
							
						}
						else{
						System.out.println("Contact number after removing 0 is"+newContactNo);
						data.setCustomerContact(newContactNo);
						}
							
						}else{
							data.setDataStatus("Invalid");
						}
						if(customerAddress!=null){
							data.setCustomerAddress(ExcelUtil.getStringCellData(customerAddress));
						}else{
							data.setDataStatus("Invalid");
						}
						if(cityName!=null){
							data.setCityName(ExcelUtil.getStringCellData(cityName));
						}else{
							data.setDataStatus("Invalid");
						}
						if(customerZipCode!=null){
						
							data.setCustomerZipcode(ExcelUtil.getNumbericCellData(customerZipCode));
							/*System.out.println("string value of pincode" +pincode2);
							int pin=Integer.parseInt(pincode2);
							System.out.println("integer value of pincode" +pin);
							data.setCustomerZipcode(pin);*/
							System.out.println("customer pin code is "+data.getCustomerZipcode());
						}else{
							data.setDataStatus("Invalid");
						}
						
						
						if(paymentType!=null){
						data.setPaymentType(ExcelUtil.getStringCellData(paymentType));
						}
						if(bayNumber!=null){
						data.setBayNumber(ExcelUtil.getNumbericCellData(bayNumber));
						}
						if(orderStatus!=null){
						data.setOrderStatus(ExcelUtil.getStringCellData(orderStatus));
						}
					 
						
						 if(reciptDate !=null){
							 
							 if(length1>6){
									try
									{
										
										SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
								        
										java.sql.Date sql = new java.sql.Date(newReciptDate.getTime());
										
									System.out.println("recipt date after converting is" +sql);
									data.setReceiptDate(sql);
									}
									catch(Exception e){
										
										data.setReceiptDate(null);
										e.getStackTrace();
									
									}
									}
									else{
										data.setReceiptDate(null);
									}
						 }
						 
						 if(deliveredDate !=null){
							 if(length11>6){
							 try
								{
									
									SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
							        
									java.sql.Date sql = new java.sql.Date(newReciptDate.getTime());
									
								System.out.println("recipt date after converting is" +sql);
								data.setDeliveryDate(sql);
								}
								catch(Exception e){
									
									data.setReceiptDate(null);
									e.getStackTrace();
								
								}
								}
								else{
									data.setDeliveryDate(null);
								}
							 
							 
						 }
						 
						 if(weight!=null){
							 
							
							 data.setWeight(ExcelUtil.getNumbericCellData(weight));
						 }
						
						//SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				        
					/*	java.sql.Date sql = new java.sql.Date(newReciptDate.getTime());
						data.setReceiptDate(sql);*/
						
						//data.setDeliveryDate(null);
						//data.setAssignedTo(null);
						data.setDeliveredBy(null);
						data.setPaymentStatus(null);
						data.setPaymentReference(null);
						if(orderValue!=null){
						data.setOrderValue(ExcelUtil.getNumbericCellData(orderValue));
						}
						else
						{
							data.setOrderValue(0);
						}
						data.setCreatedBy(null);
						data.setCreatedOn(null);
						importDao.dataImport(data);
						 
					}catch(Exception e){
						e.printStackTrace();
					}
					}
				}isHeader = false;
			}
			
		}catch(Exception e){
			System.out.println(e);
		
		
		}
		
		
		is.close();
		System.out.println("Returning Headers");
		return headerList;
	}
	
	@RequestMapping(value="/getAllImports")
	public List<DataImport> getAllImports(HttpSession session){
		return importDao.getAll(session);
	}
	
	@RequestMapping(value="/getUnderProcess")
	public List<DataImport> getUnderProcess(HttpSession session){
		return importDao.getUnderProcess(session);
	}
	
	@RequestMapping(value="/getInvalid")
	public List<DataImport> getInvalid(HttpSession session){
		return importDao.getInvalid(session);
	}
	
	@RequestMapping(value="/assignStore/{store}")
	public void assignStore(@RequestBody List<DataImport> list,@PathVariable("store") String storeName, HttpSession session){
		double id=(int)storeDao.getStoreIdByName(storeName);
		importDao.assignStore(list, session,(int) id);
	}
	
	@RequestMapping(value="/getUnassignedStore")
	public List<DataImport> getUnassignedStores(HttpSession session){
		return importDao.getUnassignedStore(session);
	}
	
	@RequestMapping(value="/getFinal")
	public List<DataImport> getFinal(HttpSession session){
		List<DataImport> list=importDao.getUnderProcess(session);
		Iterator<DataImport> itr=list.iterator();
		/*while(itr.hasNext()){
			DataImport obj=itr.next();
			String name=storeDao.getStoreNameById(obj.getStoreId());
			obj.setStoreName(name);
		}*/
		return list;
	}
	
	@RequestMapping(value="/moveOrders")
	public void moveOrders(HttpSession session){
		importDao.moveOrders(session);
	}
	
	@RequestMapping(value="/assignStoreByTrackingId/{store}")
	public void assignStoreByTrackingId(@RequestBody List<String> list,@PathVariable("store") String storeName, HttpSession session){
	System.out.println("Got list"+list);
	double id=(int)storeDao.getStoreIdByName(storeName);
	importDao.assignStoreByTrackingId(list, session,(int) id);
	}
	
}
