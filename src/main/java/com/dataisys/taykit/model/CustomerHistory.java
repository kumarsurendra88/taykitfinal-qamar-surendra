package com.dataisys.taykit.model;

public class CustomerHistory {

	private double customerHistoryId;
	private String customerName;
	private String customerMobile;
	private String trackingId;	
	
	
	public double getCustomerHistoryId() {
		return customerHistoryId;
	}
	public void setCustomerHistoryId(double customerHistoryId) {
		this.customerHistoryId = customerHistoryId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}	
}
