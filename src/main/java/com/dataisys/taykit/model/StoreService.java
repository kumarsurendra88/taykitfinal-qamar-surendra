package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class StoreService {
private double storeServiceId;
private double storeServiceTypeId;
private double storeId;
private String serviceName;
private String updatedBy;
private Timestamp updatedOn;
public StoreService(double storeServiceId, double storeTypeId, double storeId,
		String serviceName, String updatedBy, Timestamp updatedOn) {
	super();
	this.storeServiceId = storeServiceId;
	this.storeServiceTypeId = storeTypeId;
	this.storeId = storeId;
	this.serviceName = serviceName;
	this.updatedBy = updatedBy;
	this.updatedOn = updatedOn;
}
public StoreService() {
	super();
}
public double getStoreServiceId() {
	return storeServiceId;
}
public void setStoreServiceId(double storeServiceId) {
	this.storeServiceId = storeServiceId;
}
public double getStoreServiceTypeId() {
	return storeServiceTypeId;
}
public void setStoreServiceTypeId(double storeTypeId) {
	this.storeServiceTypeId = storeTypeId;
}
public double getStoreId() {
	return storeId;
}
public void setStoreId(double storeId) {
	this.storeId = storeId;
}
public String getServiceName() {
	return serviceName;
}
public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}
public String getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}
public Timestamp getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Timestamp updatedOn) {
	this.updatedOn = updatedOn;
}
@Override
public String toString() {
	return "StoreService [storeServiceId=" + storeServiceId + ", storeTypeId="
			+ storeServiceTypeId + ", storeId=" + storeId + ", serviceName="
			+ serviceName + ", updatedBy=" + updatedBy + ", updatedOn="
			+ updatedOn + "]";
}
}
