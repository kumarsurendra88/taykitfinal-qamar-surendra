package com.dataisys.taykit.model;
import org.apache.poi.ss.usermodel.Cell;

public class ExcelUtil {

public static String getStringCellData(Cell cell) {
if(cell.getCellType() == Cell.CELL_TYPE_STRING)
return cell.getStringCellValue();
else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
return new Double(cell.getNumericCellValue()).toString();
else
return "";
}

public static int getNumbericCellData(Cell cell) {
if(cell.getCellType() == Cell.CELL_TYPE_STRING)
return new Double(cell.getStringCellValue()).intValue();
else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
return new Double(cell.getNumericCellValue()).intValue();
else
return 0;
}
}

