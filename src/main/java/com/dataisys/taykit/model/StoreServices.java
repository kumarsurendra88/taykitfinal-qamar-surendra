package com.dataisys.taykit.model;

public class StoreServices {
private double serviceId;
private String name;
private String description;
private String updatedOn;
private Double updatedBy;
private boolean serviceStatus;
public double getServiceId() {
	return serviceId;
}
public void setServiceId(double serviceId) {
	this.serviceId = serviceId;
}
public StoreServices(double serviceId, String name, String description,
		String updatedOn, double updatedBy) {
	super();
	this.serviceId = serviceId;
	this.name = name;
	this.description = description;
	this.updatedOn = updatedOn;
	this.updatedBy = updatedBy;
}
public StoreServices() {
	super();
}

@Override
public String toString() {
	return "StoreServices [serviceId=" + serviceId + ", name=" + name
			+ ", description=" + description + ", updatedOn=" + updatedOn
			+ ", updatedBy=" + updatedBy + ", serviceStatus=" + serviceStatus
			+ "]";
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(String updatedOn) {
	this.updatedOn = updatedOn;
}
public double getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(double updatedBy) {
	this.updatedBy = updatedBy;
}
public boolean getServiceStatus() {
	return serviceStatus;
}
public void setServiceStatus(boolean serviceStatus) {
	this.serviceStatus = serviceStatus;
}
}
