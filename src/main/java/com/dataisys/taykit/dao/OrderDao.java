package com.dataisys.taykit.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.CancelledOrderByData;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderActivity;
//import com.dataisys.taykit.model.OrderAttachments;
import com.dataisys.taykit.model.OrderComment;
import com.dataisys.taykit.model.RunsheetData;
import com.dataisys.taykit.model.SearchOrder;
import com.dataisys.taykit.model.StockInward;
import com.dataisys.taykit.model.StockOutward;
import com.dataisys.taykit.model.StoreLocation;
import com.dataisys.taykit.model.StoreRunSheet;
import com.dataisys.taykit.model.StoreSummery;
import com.dataisys.taykit.model.UpdateDeliveryStatus;

public interface OrderDao {

public void createOrder(Order order);


public List<Order> getUnassignedOrder();

public Order getOrders(String orderId);

public void updateOrder(Order object);

public void updateAssign(Order object, double id);

public void addComment(String comment, String trackingId, HttpSession session);

public List<OrderComment> getComments(String trackingId);

public void addAttachments(String trackingId, UploadForm uploadItem) throws IOException;

public void inwardOrder(StockInward obj,HttpSession session);

public void transferStock(StockOutward obj);

public List<Order> getOrdersByDeliveryBoyId(String deliveryBoyAccNo);
public List<Order> getReturnOrder();

public Order getOrdersByTrackingId(String trackingId);

public void assignDeliveryBoy(AssignDeliveryBoy obj);

public List<Order> getOrders();


public List <StoreLocation> getStoreDetails();

public void updateStatus(List<Order> orders ,String status, String reason);

public List<com.dataisys.taykit.model.Order> getAllOrders(HttpSession session);

public List<Order> getStoreUnaasigned();

public void updateOrderStatus(Order object);

public List<Order> getRunSheetDeliveries(Order order);

//Recently Added Code by kishore
public List<Order> getOrdersByStoreId(String storeId);
public List<Order> getOrdersForInwardByStoreId(String storeId); //new
public List<Order> getOrdersByStoreId(String storeId, String orderType);
public void updateOrderDelivery(String trackingId, String orderStatus, String paymentStatus);
public void updateOrderDelivery(String trackingId, String orderStatus, String paymentStatus,String deliveredGeoLocation, String rRNo);
public void updateOtp(String trackingId, String otp);
public void updateAssign(List<Order> list, String assignId, String userType);
public List<com.dataisys.taykit.model.Order> getAllOrdersByContactNumber(String contactNumber);
public void updateDeliveredLocationLatLongsInOrder(Order order);
public void podLevelInward(StockInward obj); //Added on 08-12-2015

public List<com.dataisys.taykit.model.OrderActivity> getAllOrderActivities(String trackingId);

public void getRunsheetNumber(String userName,String runsheetNumber);

public List<OrderActivity> getCommentsId(String trackId,String idType);

public void runSheetDetails(RunsheetData runSheet);


public List<UpdateDeliveryStatus> getRunSheets(Order order);


public List<Order> getSelectedRunsheetDetails(String runsheetNumber);


public void updateOrdersByRunsheet(Order obj);


public void closeRunSheet(String runSheetNumber);


public List<Order> getOrdersByRunsheetId(String id);


public List<Order> ordersForStoreRunSheet(Order order);

public void getRunSheetNumberForStore(double storeId ,String runsheetNumber, String string);


public List<UpdateDeliveryStatus> getStoreRunsheets(StoreRunSheet obj);


public List<Order> getOrdersByStoreRunSheet(String storeRunsheet);


public void closeStoreRunsheet(String runSheetNumber);

public List<Order> getOrdersByRunsheet(String runSheetNo);

public double valueForOrder(String runSheetNo);

public void storeTempNumber(double number);

public double getTempNumber();


public List<UpdateDeliveryStatus> getStoreRunSheetsCash(StoreRunSheet obj);


public List<Order> getAllDeliveredOrders();


public List<Order> searchOrderByTrackingId(String trackingId);


public List<Order> searchOrderByOrderId(String orderId);


public List<Order> searchOrderByCustomerName(String customerName);


public List<Order> searchOrderByCustomerContact(String customerContact);


public List<Order> getRecentOrders();

public List<String> listOfInactiveRunsheets(String assignTo);
public List<UpdateDeliveryStatus> getRunsheetsForDeliveryBoys(Order order);
public double getTempRunNumber();
public void storeTempRunNumber(double number);

public void updateNumbers();

public List<StoreSummery> getStoreSummery(HttpSession session);
public List<Order> DetailSummery(String storeName,String status);




public List<String> listOfInactiveRunsheets();


public List<Order> getRecentOrdersNew(Order order);

public void transferToRetailer(StockOutward obj);

public List<Order> getAllCancelledOrders(HttpSession sesstion);  //Added new by Kishore

public List<Order> getAllCancelledOrders(HttpSession session, CancelledOrderByData cancelledOrderByData);
public void updateCancelledOrderStatus(CancelledOrderByData cancelledOrderByData);
public List<Order> getAllRTOConfirmedOrders(HttpSession session, CancelledOrderByData cancelledOrderByData) ;
public void updateReferenceNoForRTO(CancelledOrderByData cancelledOrderByData);

//*****functions added for runsheet*******//
public void runSheetPartialDetails(String runsheetNumber,String username);
public List<Order> getRunSheetDeliveries2(String DeliveryBoyName,String randomRunsheetNumber);


public List<Order> getOrdersBasedOnSearch(SearchOrder searchOrder);


public List<Order> getAllRTODispatchedOrdersWithRefNo(HttpSession session,
		CancelledOrderByData cancelledOrderByData);


public List<Order> getOrdersBasedOnSearch1(SearchOrder searchOrder);

public  List<Order> getOrderDetailsById(String trackId,String idType);


public boolean CreatedOrderAPI(Order object);


public void updateAssignReturn(Order object, double id);


public List<Order> getMultipleTrackingIdSearch(String id);


public Order getMultipleOrderIdSearch(String id);

}
