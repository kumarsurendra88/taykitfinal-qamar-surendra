package com.dataisys.taykit.model;

import java.sql.Timestamp;

public class UserRight {

private double userId;

private int moduleId;

private String moduleAccess;

private String updatedBy;

private Timestamp updatdeOn;

public UserRight(){}

public UserRight(double userId, int moduleId, String moduleAccess,
		String updatedBy, Timestamp updatdeOn) {
	super();
	this.userId = userId;
	this.moduleId = moduleId;
	this.moduleAccess = moduleAccess;
	this.updatedBy = updatedBy;
	this.updatdeOn = updatdeOn;
}

public double getUserId() {
	return userId;
}

public void setUserId(double userId) {
	this.userId = userId;
}

public int getModuleId() {
	return moduleId;
}

public void setModuleId(int moduleId) {
	this.moduleId = moduleId;
}

public String getModuleAccess() {
	return moduleAccess;
}

public void setModuleAccess(String moduleAccess) {
	this.moduleAccess = moduleAccess;
}

public String getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}

public Timestamp getUpdatdeOn() {
	return updatdeOn;
}

public void setUpdatdeOn(Timestamp updatdeOn) {
	this.updatdeOn = updatdeOn;
}







}
