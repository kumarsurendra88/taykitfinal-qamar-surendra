package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.Hub;
import com.dataisys.taykit.model.Region;

public class HubRowMapper implements RowMapper<Hub>
{

	public Hub mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
	
		Hub hub=new Hub();
		/*region.setRegion_id(rs.getInt("region_id"));
		region.setZone_id(rs.getInt("zone_id"));
		region.setRegion_name(rs.getString("region_name"));
		region.setRegion_desc(rs.getString("region_desc"));
	*/
		
		hub.setHub_id(rs.getDouble("hub_id"));
		hub.setHub_name(rs.getString("hub_name"));
		hub.setRegion_id(rs.getInt("region_id"));
		hub.setHub_desc(rs.getString("hub_desc"));
	
	return hub;
	}
}