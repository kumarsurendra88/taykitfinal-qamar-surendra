package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.ShortfallAmount;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreContact;
import com.dataisys.taykit.model.ProductCategories;
import com.dataisys.taykit.model.StorePaymentCollection;
import com.dataisys.taykit.model.StoreServices;

public interface PudoStoreDao {

public void createStore(Store pudoStore);

public void updateStore(Store pudoStore);

public void deleteStore(double storeId);
public void AddStore(Store store,List<StoreServices> service,List<ProductCategories> category,
		List<StoreContact> contacts,HttpSession session);

public List<Store> getStores(HttpSession session);
public List<Store> getStores();
public Store getStoreById(double id);

public void updateStore(Store store, List<StoreServices> storeservices,
		List<ProductCategories> categoryServices,
		List<StoreContact> storeContacts, int id);

public double getStoreIdByName(String storeName);
public Store getStoreByAccountNo(String accountNo);

public void collectStorePayment(StorePaymentCollection obj);

public void saveStoreShortFall(ShortfallAmount details);
public String getStoreAccNo(String storeName);

public double getStoreIdByAccountNo(String accountNo );



}

