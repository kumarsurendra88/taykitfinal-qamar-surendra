package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.dataisys.taykit.model.RunsheetDetails;

public class RunsheetRowMapper implements RowMapper<RunsheetDetails>{
	
	
	public RunsheetDetails mapRow(ResultSet rs , int rowNum) throws SQLException {
 {

	 RunsheetDetails runsheet= new RunsheetDetails();
	 	runsheet.setCodStatus(rs.getString("cod_status"));
	 	runsheet.setTotalCod(rs.getInt("total_cod"));
	 	runsheet.setTotalPrepaidOrders(rs.getInt("total_prepaid"));
	 	runsheet.setTotalShipment(rs.getInt("total_shipment"));
	 	runsheet.setRunSheetNO(rs.getString("runsheet_no"));
	 	runsheet.setCodCollected(rs.getDouble("total_cod_cash"));
	 	return runsheet;
	 	
 }
	 	
 
 
 }
}
