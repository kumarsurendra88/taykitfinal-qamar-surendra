package com.dataisys.taykit.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreReport;
import com.dataisys.taykit.model.User;

public interface StoreDao {
	
	public List<Store> getStoreNameInOrderReturn();

	public List<Store> getStoreNameOfHub(User user);


	public String getStoreId(String storeName);

	public StoreReport getTotalInward(String storeId, String fromDate, String toDate);

}
