package com.dataisys.taykit.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.DeliveryBoyDao;
import com.dataisys.taykit.dao.OrderCommentDao;
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.OrderDeliveryDAO;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.ReturnDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.mobile.model.OrderDelivery;
import com.dataisys.taykit.mobile.model.ReturnsForAndroid;
import com.dataisys.taykit.model.Customer;
import com.dataisys.taykit.model.DeliveryBoys;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderComment;
import com.dataisys.taykit.model.Return;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.User;

@Controller
@RequestMapping("/getData/v1")
public class DeliveryBoyController {
	
	@Autowired 
	public OrderDao orderDao;
	
	@Autowired
	public CustomerDao customerDao;
	
	@Autowired
	public OrderCommentDao orderCommentDao;
	
	@Autowired
	public OrderDeliveryDAO orderDeliveryDAO;
	
	@Autowired
	public RegistrationKeyDao registrationKeyDao;
	
	@Autowired 
	public ReturnDao returnDao;
	
	@Autowired
	public DeliveryBoyDao deliveryBoyDao;
	
	@Autowired
	public UserDao userDao;
	
	@Autowired
	PudoStoreDao storeDao;
	
	@RequestMapping("getAssignedOrderById/{deliveryBoyId}")
	public @ResponseBody List<com.dataisys.taykit.model.Order> getAssignedOrders(@PathVariable String deliveryBoyId) {
		
		//HttpSession session = request.getSession(false);
		try {
			System.out.println("Inside getAssignedOrderById from DeliveryBoy side and deliveryBoyId is: "+deliveryBoyId);
			List<Order> list = orderDao.getOrdersByDeliveryBoyId(deliveryBoyId);
			System.out.println("The list of Order which are assigned to a perticular delivery boy is: "+list.size());
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/* Added For Returns by Surendra*/
	
	@RequestMapping("getPickupReturnOrderById/{deliveryBoyId}")
	public @ResponseBody List<ReturnsForAndroid> getPickupReturnsByDeliveryBoyId(@PathVariable String deliveryBoyId) {	
		System.out.println("inside getPickupReturnByDeliveryBoyId");
		List<Return> list = returnDao.getPickupReturnsByDeliveryBoyId(deliveryBoyId);
		
		System.out.println("List is null: "+list);
		
		List<ReturnsForAndroid> listOfPickupOrders = new ArrayList<ReturnsForAndroid>();
		for(Return ret : list) {
			ReturnsForAndroid returnForAndroid = new ReturnsForAndroid();
			returnForAndroid.setTrackingId(ret.getTrackingId());
			returnForAndroid.setCustomerEmail(ret.getEmailId());
			returnForAndroid.setCustomerMobile(ret.getContactNo());
			returnForAndroid.setCityName(ret.getCity());
			returnForAndroid.setCountry(ret.getCountry());
			returnForAndroid.setOrderId(ret.getOrderId());
			returnForAndroid.setReturnId(String.valueOf((long)Double.parseDouble(ret.getReturnId())));
			returnForAndroid.setAssignedTo(ret.getAssignedTo());
			returnForAndroid.setOrderType(ret.getPaymentMode().trim());
			returnForAndroid.setCustomerName(ret.getConsigneeName().trim());
			returnForAndroid.setCustomerAddress(ret.getCustomerAddress());
			returnForAndroid.setOrderDetails(ret.getProductToBeShipped());
			returnForAndroid.setPaymentMode("cod");
			returnForAndroid.setQuantity(ret.getQuantity());
			listOfPickupOrders.add(returnForAndroid);			
		}
		System.out.println("executed implementation");
		System.out.println("return data is list is "+list);
		return listOfPickupOrders;
		
	}
	
	/* End */
	
	@RequestMapping(value = "makeDeliveryByCOD", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void makeDeliveryByCod(@RequestBody OrderDelivery orderDelivery) {
		System.out.println("Inside makeDeliveryBoyCod");
		System.out.println("order.getTrackingId: "+orderDelivery.getTrackingId());
		System.out.println("order.getUserId: "+orderDelivery.getUserId());
		//System.out.println("order.getSignature: "+orderDelivery.getSignature());
		System.out.println("delivered GeoLocation: "+orderDelivery.getDeliveredLocation());
		System.out.println("the time from android side is "+orderDelivery.getUpdatedOn());
		System.out.println("Get Reference Number: "+orderDelivery.getrRNo());
		
		String paymentStatus = "paid";
		String orderStatus = "Delivered";
		Order fetchedOrder = null;
		
		try {
			fetchedOrder = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fetchedOrder != null) {
			System.out.println("Inside makeDeliveryByCOD if(fetchedOrder != null): and order status is: "+fetchedOrder.getOrderStatus());
			if(!((fetchedOrder.getOrderStatus().trim().equalsIgnoreCase("Delivered")) || (fetchedOrder.getOrderStatus().trim().contains("Order cancelled")))) {
				
				System.out.println("Inside if->if");
				orderDeliveryDAO.createOrderDelivery(orderDelivery);
				orderDao.updateOrderDelivery(orderDelivery.getTrackingId(), orderStatus, paymentStatus, orderDelivery.getDeliveredLocation(),orderDelivery.getrRNo());	
				
				OrderComment orderComment = new OrderComment();
				
				long commentBy = (long) orderDelivery.getUserId();
				orderComment.setTrackingId(orderDelivery.getTrackingId());
				orderComment.setCommentText("Delivered");
				orderComment.setCommentBy(commentBy+"");
				orderComment.setReceived_by(orderDelivery.getReceivedBy());
				
				orderCommentDao.orderCommentAfterDelivery(orderComment);
				
				//Code for updating customer Gps Location
				Order order = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
				String customerName = order.getCustomerName();
				String customerContact = order.getCustomerContact();
				Customer customer = new Customer();
				customer.setCustomerName(customerName);
				customer.setCustomerMobile(customerContact);
				customer.setCustomerGpsLocation(orderDelivery.getDeliveredLocation());
				customerDao.updateCustomerByGpsLocation(customer);
			} else {
				System.out.println("Order Status is already Delivered so no updation again");
			}
		} else {
			System.out.println("Fetched order is null");
		}		
	}
	
	@RequestMapping(value = "makeDeliveryByCODCard", method=RequestMethod.POST,headers="Content-Type=application/json")
    public @ResponseBody void makeDeliveryByCodCard(@RequestBody OrderDelivery orderDelivery) {             
           System.out.println("Inside makeDeliveryBoyCodCard");
           System.out.println("order.getTrackingId: "+orderDelivery.getTrackingId());
           System.out.println("order.getUserId: "+orderDelivery.getUserId());
           System.out.println("order.getSignature: "+orderDelivery.getSignature());
           System.out.println("delivered GeoLocation: "+orderDelivery.getDeliveredLocation());
          
           String paymentStatus = "paid";
           String orderStatus = "Delivered";
          
           orderDeliveryDAO.createOrderDelivery(orderDelivery);
           orderDao.updateOrderDelivery(orderDelivery.getTrackingId(), orderStatus, paymentStatus,orderDelivery.getDeliveredLocation(), orderDelivery.getrRNo());
                       
           if(orderDelivery.getTrackingId().startsWith("D001"))
           {
                  updateRetailerOrderStatus(orderDelivery.getTrackingId());
           }
                       
           //Code for updating customer Gps Location
           Order order = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
           String customerName = order.getCustomerName();
           String customerContact = order.getCustomerContact();
           Customer customer = new Customer();
           customer.setCustomerName(customerName);
           customer.setCustomerMobile(customerContact);
           customer.setCustomerGpsLocation(orderDelivery.getDeliveredLocation());           
           customerDao.updateCustomerByGpsLocation(customer);
    }
	
	
	private void updateRetailerOrderStatus(String trackingId)
    {
           CloseableHttpClient httpclient = HttpClients.createDefault();
     try {
          
           Order order = orderDao.getOrdersByTrackingId(trackingId);
          
         HttpPost httpPost = new HttpPost("http://mapi.zivame.com/v1/order/delivered");
        
           //HttpPost httpPost = new HttpPost("http://localhost:8080/taykitapi/api/authenticate");
         httpPost.addHeader("X-Zivame-API-Key", "5645524695d7ac077a0b11a1956ce6e7==");
         httpPost.addHeader("Content-Type", "application/json");
        
         JSONObject json = new JSONObject();
         json.put("awb_no", order.getTrackingId());
         json.put("order_no", order.getOrderId());
         //json.put("pickup_date","2015-12-05T15:39:19.217000");
         json.put("delivered_date", order.getDeliveredDate().toString());
         json.put("status", "delivered");
         json.put("carrier", "Delivery");
        
         /*json.put("username", USERNAME);
         json.put("password", PASSWORD);*/
        
         
         StringEntity entity = new StringEntity(json.toString());
         httpPost.setEntity(entity);
          
         System.out.println(json.toString());
        
         HttpResponse response = httpclient.execute(httpPost);
         BufferedReader buffReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));           
         StringBuffer respBuffer = new StringBuffer();
         String line = "";
         while ((line = buffReader.readLine()) != null) {           
            System.out.println(line);
            respBuffer.append(line);
         }
        
         System.out.println("API Response: "+ respBuffer.toString());
        
         /*JSONObject responseJson = new JSONObject(respBuffer.toString());
         token = responseJson.getString("message");           
         System.out.println("Token: "+ token);*/
        
     } catch(Exception ex) {
           System.out.println("Error in fetchToken :"+  ex.getMessage());
           System.out.println(ex.getStackTrace());
     } finally {
           try {
                  httpclient.close();
           } catch(Exception ex) {
                  System.out.println(ex.getMessage());
           }
     }
    }
	
	
	@RequestMapping(value = "makeDeliveryByPrepaid", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void makeDeliveryByPrepaid(@RequestBody OrderDelivery orderDelivery) {		
		System.out.println("Inside makeDeliveryByPrepaid");
		System.out.println("order.getTrackingId: "+orderDelivery.getTrackingId());
		System.out.println("order.getUserId: "+orderDelivery.getUserId());
		//System.out.println("order.getSignature: "+orderDelivery.getSignature());
		System.out.println("delivered GeoLocation: "+orderDelivery.getDeliveredLocation());
		
		String paymentStatus = "Prepaid";
		String orderStatus = "Delivered";
		
		Order fetchedOrder = null;
		
		try {
			fetchedOrder = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fetchedOrder != null) {			
			System.out.println("Inside makeDeliveryByPrepaid if(fetchedOrder != null): and order status is: "+fetchedOrder.getOrderStatus());
			if(!((fetchedOrder.getOrderStatus().trim().equalsIgnoreCase("Delivered")) || (fetchedOrder.getOrderStatus().trim().contains("Order cancelled")))) {

				orderDeliveryDAO.createOrderDelivery(orderDelivery);
				orderDao.updateOrderDelivery(orderDelivery.getTrackingId(), orderStatus, paymentStatus, orderDelivery.getDeliveredLocation(),orderDelivery.getrRNo());
				
                try {
					OrderComment orderComment = new OrderComment();
					
					long commentBy = (long) orderDelivery.getUserId();
					orderComment.setTrackingId(orderDelivery.getTrackingId());
					orderComment.setCommentText("Delivered");
					orderComment.setCommentBy(commentBy+"");
					orderComment.setReceived_by(orderDelivery.getReceivedBy());
					
					orderCommentDao.orderCommentAfterDelivery(orderComment);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Code for updating customer Gps Location
				Order order = orderDao.getOrdersByTrackingId(orderDelivery.getTrackingId());
				String customerName = order.getCustomerName();
				String customerContact = order.getCustomerContact();
				Customer customer = new Customer();
				customer.setCustomerName(customerName);
				customer.setCustomerMobile(customerContact);
				customer.setCustomerGpsLocation(orderDelivery.getDeliveredLocation());
				customerDao.updateCustomerByGpsLocation(customer);
			} else {
				System.out.println("Order Status is already Delivered so no updation again");
			}
		} else {
			System.out.println("Fetched Order is null in Make Delivery By Prepaid");
		}
	}
	
	@RequestMapping(value="/updateOrderComment", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void updateOrder(@RequestBody OrderComment object) throws IOException{
		System.out.println("Tracking Id: "+object.getTrackingId());
		System.out.println("Comment Text: "+object.getCommentText());
		System.out.println("User Id: "+object.getCommentBy());
		System.out.println("Comment Date: "+object.getCommentDate());
		orderCommentDao.updateComment(object);
		orderCommentDao.updateOrder(object);
	}
	
	//Added By Suri
	@RequestMapping(value="/updateOrderReason", method=RequestMethod.POST,headers="Content-Type=application/json")
	public @ResponseBody void updateOrder1(@RequestBody OrderComment object) throws IOException{
		System.out.println("Tracking Id: "+object.getTrackingId());
		System.out.println("Comment Text: "+object.getCommentText());
		System.out.println("User Id: "+object.getCommentBy());
		System.out.println("Comment Date: "+object.getCommentDate());
		System.out.println("reason is "+object.getReason());
		
		Order fetchedOrder = null;
		
		try {
			fetchedOrder = orderDao.getOrdersByTrackingId(object.getTrackingId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fetchedOrder != null) {	
			System.out.println("Inside updateOrderReason if(fetchedOrder != null): and order status is: "+fetchedOrder.getOrderStatus());
			if(!((fetchedOrder.getOrderStatus().trim().equalsIgnoreCase("Delivered")) || (fetchedOrder.getOrderStatus().trim().contains("Order cancelled")))) {
				System.out.println("Inside if");
				orderCommentDao.updateOrder(object);
				orderCommentDao.updateReason(object);
			} else {
				System.out.println("Order Status is already Delivered so no updation again");
			}		
		} else {
			System.out.println("Fetched Order is null in updateOrderReason");
		}		
	}	
	
	@RequestMapping(value="/updateOrderReturnReason", method=RequestMethod.POST,headers="Content-Type=application/json") 
	public @ResponseBody void updateOrderReturnReason(@RequestBody OrderComment object) throws IOException {
		System.out.println("Inside updateOrderReturnReason in DeliveryBoyController");
		System.out.println("Tracking Id: "+object.getTrackingId());
		System.out.println("Comment Text: "+object.getCommentText());
		System.out.println("User Id: "+object.getCommentBy());
		System.out.println("Comment Date: "+object.getCommentDate());
		System.out.println("reason is "+object.getReason());
		orderCommentDao.updateOrderReturn(object);
	}
	
	//DeliveryBoyId
	@RequestMapping("getDeliveryBoyById/{deliveryBoyId}")
	public @ResponseBody DeliveryBoys getDeliveryBoyById(@PathVariable String deliveryBoyId) {		
		System.out.println("Inside getDeliveryBoyById in DeliveryBoyController");
		DeliveryBoys deliveryBoy = null;
		User user = userDao.getUserByAccNumber(deliveryBoyId);
		if(user != null) {
			deliveryBoy = deliveryBoyDao.DeliveryBoyId(user.getUserName());
			if(deliveryBoy != null) {
				return deliveryBoy;
			} else {
				System.out.println("Delivery boy is null in getDeliveryBoyById in DeliveryBoyController");
			}
		}
		return deliveryBoy;		
	}	
	
	//Added on 04-02-2016
	@RequestMapping("getStoresById/{deliveryBoyId}")
	public @ResponseBody List<Store> getStoresByDeliveryBoy(@PathVariable String deliveryBoyId) {
		System.out.println("Inside getStoresByDeliveryBoy");
		List<Integer> storeIdList = null;
		List<Store> stores = new ArrayList<Store>();
		try {
			User user = userDao.getUserByAccNumber(deliveryBoyId);
			if(user != null) {
				storeIdList = deliveryBoyDao.getStoreIdsForEachDeliveryBoy(user.getUserName());
				for (Integer storeId : storeIdList) {
					Store store = storeDao.getStoreById(storeId);	
					stores.add(store);
				}
			}
			return stores;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping("changeOrderStatusFromDeliveryToPickup/{trackingId}/{storeId}")
	public @ResponseBody void updateChangeOrderStatusFromDeliveryToPickup(@PathVariable("trackingId") String trackingId, @PathVariable("storeId") int storeId) {
		System.out.println("Inside updateChangeOrderStatusFromDeliveryToPicup and the trackingId is: "+trackingId);				
			deliveryBoyDao.updateOrderStatusToPickup(trackingId,storeId);		
	}
	
	@RequestMapping("changeOrderStatusFromPickupToDropOff/{trackingId}/{storeId}")
	public @ResponseBody void updateChangeOrdersStatusFromPickupToDropOff(@PathVariable("trackingId") String trackingId, @PathVariable("storeId") int storeId) {
		System.out.println("Inside updateChangeOrderStatusFromPickupToDropOff and it's trackingId: "+trackingId);
		deliveryBoyDao.updateOrderStatusToDropOff(trackingId, storeId);
	}
}
