package com.dataisys.taykit.mobile.model;

import java.sql.Date;
import java.sql.Timestamp;

public class ReturnsForAndroid {

	private String runsheet_number;
	private String trackingId;
	private String returnId;
	private String orderId;
	private String courierService;
	
	private String orderTitle;
	private String orderDetails;
	private String orderType;
	private String orderCategory;
	private String orderPriority;
	
	private double orderProductId;
	private double activityId;
	private double revisionId;
	private double attachmentId;
	private double storeId;
	private double retailerId;
	
	private String orderRefNo;
	private String customerName;
	private String customerPhone;
	private String customerMobile;
	private String customerEmail;
	private String customerAddress;
	private String country;
	private String state;	
	private String cityName;
	private String pincode;
	
	
	private String orderStatus;
	private String statusReason;
	private Date receiptDate;
	private Date deliveryDate;
	private String assignedTo;
	private Date deliveredDate;
	private String deliveredBy;	
	
	private double orderValue;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Timestamp updatedOn;
	private String stockFlag;
	private String isCollected;	
	private String runsheetStatus;
	private String storeRunsheet;
	private String weight;
	private int quantity;
	private String paymentMode;	
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getRunsheet_number() {
		return runsheet_number;
	}
	public void setRunsheet_number(String runsheet_number) {
		this.runsheet_number = runsheet_number;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getReturnId() {
		return returnId;
	}
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCourierService() {
		return courierService;
	}
	public void setCourierService(String courierService) {
		this.courierService = courierService;
	}
	public String getOrderTitle() {
		return orderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}
	public String getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(String orderDetails) {
		this.orderDetails = orderDetails;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderCategory() {
		return orderCategory;
	}
	public void setOrderCategory(String orderCategory) {
		this.orderCategory = orderCategory;
	}
	public String getOrderPriority() {
		return orderPriority;
	}
	public void setOrderPriority(String orderPriority) {
		this.orderPriority = orderPriority;
	}
	public double getOrderProductId() {
		return orderProductId;
	}
	public void setOrderProductId(double orderProductId) {
		this.orderProductId = orderProductId;
	}
	public double getActivityId() {
		return activityId;
	}
	public void setActivityId(double activityId) {
		this.activityId = activityId;
	}
	public double getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(double revisionId) {
		this.revisionId = revisionId;
	}
	public double getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(double attachmentId) {
		this.attachmentId = attachmentId;
	}
	public double getStoreId() {
		return storeId;
	}
	public void setStoreId(double storeId) {
		this.storeId = storeId;
	}
	public double getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(double retailerId) {
		this.retailerId = retailerId;
	}
	public String getOrderRefNo() {
		return orderRefNo;
	}
	public void setOrderRefNo(String orderRefNo) {
		this.orderRefNo = orderRefNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public Date getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	public String getDeliveredBy() {
		return deliveredBy;
	}
	public void setDeliveredBy(String deliveredBy) {
		this.deliveredBy = deliveredBy;
	}
	public double getOrderValue() {
		return orderValue;
	}
	public void setOrderValue(double orderValue) {
		this.orderValue = orderValue;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStockFlag() {
		return stockFlag;
	}
	public void setStockFlag(String stockFlag) {
		this.stockFlag = stockFlag;
	}
	public String getIsCollected() {
		return isCollected;
	}
	public void setIsCollected(String isCollected) {
		this.isCollected = isCollected;
	}
	public String getRunsheetStatus() {
		return runsheetStatus;
	}
	public void setRunsheetStatus(String runsheetStatus) {
		this.runsheetStatus = runsheetStatus;
	}
	public String getStoreRunsheet() {
		return storeRunsheet;
	}
	public void setStoreRunsheet(String storeRunsheet) {
		this.storeRunsheet = storeRunsheet;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}	
}
