package com.dataisys.taykit.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dataisys.taykit.GCM.Content;
import com.dataisys.taykit.GCM.POST2GCM;
import com.dataisys.taykit.dao.CustomerDao;
import com.dataisys.taykit.dao.OrderDao;
import com.dataisys.taykit.dao.PudoStoreDao;
import com.dataisys.taykit.dao.RegistrationKeyDao;
import com.dataisys.taykit.dao.UserDao;
import com.dataisys.taykit.forms.UploadForm;
import com.dataisys.taykit.model.AssignDeliveryBoy;
import com.dataisys.taykit.model.Order;
import com.dataisys.taykit.model.OrderActivity;
import com.dataisys.taykit.model.OrderComment;
import com.dataisys.taykit.model.PudoStore;
import com.dataisys.taykit.model.RegistrationKey;
import com.dataisys.taykit.model.SearchOrder;
import com.dataisys.taykit.model.Store;
import com.dataisys.taykit.model.StoreRunSheet;
import com.dataisys.taykit.model.StoreSummery;
import com.dataisys.taykit.model.UpdateDeliveryStatus;
import com.dataisys.taykit.model.User;
import com.dataisys.taykit.services.OtpService;
import com.opencsv.CSVWriter;
 




@RestController

public class OrderController   {
	
	
	
	
	@Autowired
	OrderDao orderDao;
	@Autowired
	CustomerDao custDao;
	
	@Autowired
	PudoStoreDao storeDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	RegistrationKeyDao regKeyDao;
	
	@RequestMapping(value="/searchOrderByTrackingId/{trackingId}")
	public List<Order> searchOrderByTrackingId(@PathVariable("trackingId") String trackingId){
		System.out.println("Got "+trackingId);
		return orderDao.searchOrderByTrackingId(trackingId);
	}
	@RequestMapping(value="/searchOrderByOrderId/{orderId}")
	public List<Order> searchOrderByOrderId(@PathVariable("orderId") String orderId){
		System.out.println("Got "+orderId);
		return orderDao.searchOrderByOrderId(orderId);
	}
	@RequestMapping(value="/searchOrderByCustomerName/{customerName}")
	public List<Order> searchOrderByCustomerName(@PathVariable("customerName") String customerName){
		System.out.println("Got "+customerName);
		return orderDao.searchOrderByCustomerName(customerName);
	}
	@RequestMapping(value="/searchOrderByCustomerContact/{customerContact}")
	public List<Order> searchOrderByCustomerContact(@PathVariable("customerContact") String customerContact){
		System.out.println("Got "+customerContact);
		return orderDao.searchOrderByCustomerContact(customerContact);
	}


@RequestMapping(value="/getRecentOrders")
	public List<Order> getRecentOrders(HttpSession session){

		List<Order> orders= orderDao.getRecentOrders();
		return orders;
	}
	@RequestMapping(value="/getUnassignedOrder" , method=RequestMethod.GET)
	public List<Order> getUnassignedOrder() throws IOException{
		
		List<Order> orders= orderDao.getUnassignedOrder();
		return orders;
	}
	
	@RequestMapping(value="/getOrderById/{orderId}", method=RequestMethod.GET)
	public @ResponseBody Order getOrderListById(@PathVariable("orderId") String trackingtId) throws IOException{
		
		
		
		Order order= orderDao.getOrders(trackingtId);
		
		return order;
	}
	
	
	
	@RequestMapping(value="/updateOrder", method=RequestMethod.POST)
	public void updateOrder(@RequestBody Order object) throws IOException{
	//System.out.println("Got Object"+object);
	orderDao.updateOrder(object);
	}
	
	
	@RequestMapping(value="/assignStore/{storeName}", method=RequestMethod.POST)
	public void updateOrder(@RequestBody Order object,@PathVariable("storeName") String storeName,HttpSession session) throws IOException{
		System.out.println("Got Object"+object +storeName );
		double id=(int)storeDao.getStoreIdByName(storeName);
		orderDao.updateAssign(object,id);
	}
	
	
	@RequestMapping(value="/assignDeliverBoy", method=RequestMethod.POST)
	public void assignDeliveryBoy(@RequestBody AssignDeliveryBoy obj,HttpSession session) {
		System.out.println("Got Object"+obj);
		System.out.println("runsheet Number" +obj.getRunSheetNumber());
		orderDao.assignDeliveryBoy(obj);
		
		//Code to make a delivery boy get an alert by Kishore
		
		try {
			System.out.println("Delivery Boy Name: "+obj.getAssignTo());
			User user = userDao.getDeliveryBoyByName(obj.getAssignTo());
			RegistrationKey regKey = regKeyDao.getRegkeyByAccountid(user.getAccNumber());
			System.out.println("Obtained RegKey: "+regKey.getRegKey()+" For DeliveryBoy is: "+user.getUserName());
			
			String apiKey = "AIzaSyAHuIzTDBpD7Cr8oST3TtL2hQn8xJoggjo";
			Content content = new Content();
			content.addRegId(regKey.getRegKey());
			content.createData("You've got an Order To Deliver", obj.getTrackingId());
			POST2GCM.post(apiKey, content);
			
		} catch (Exception e) {
			System.out.println("Inside catch of assignDeliveryBoy from webapp in OrderController");
			e.printStackTrace();
		}
		
		
	}
	
	@RequestMapping(value="/addComment/{trackingId}", method=RequestMethod.POST)
	public void addComment(@RequestBody String comment,@PathVariable("trackingId") String trackingId,HttpSession session) throws IOException{
	//System.out.println("Got Object"+object);
	orderDao.addComment(comment,trackingId,session);
	}

	@RequestMapping(value="/getComments/{trackingId}", method=RequestMethod.POST)
	public List<OrderComment> getComments(@PathVariable("trackingId") String trackingId) throws IOException{
	//System.out.println("Got Object"+object);

	return orderDao.getComments(trackingId);
	}
	
	
	@RequestMapping(value="/addAttachment/{trackingId}", method=RequestMethod.POST, headers = "content-type=multipart/*")
	public void addAttachment(@PathVariable("trackingId") String trackingId,UploadForm uploadItem) throws IOException{
		//System.out.println("Uploading File");
		
		 orderDao.addAttachments(trackingId,uploadItem);
	}
	
	@RequestMapping(value="/updateStatus/{status},{reason}", method=RequestMethod.POST)
	public void updateOrderStatus(@PathVariable("status") String status,@PathVariable("reason") String reason,@RequestBody List<Order> orders) throws IOException{
		orderDao.updateStatus(orders, status,reason);
		//orderDao.updateOrder(object);
	}
	
	
	@RequestMapping(value="/getAllOrders" , method=RequestMethod.GET)
	public List<Order> getAllOrders(HttpSession session) throws IOException {		

		List<Order> orders= orderDao.getAllOrders( session);
	
	
	return orders;
	}
	

	@RequestMapping(value="/getAllDeliveredOrders")
	public List<Order> getAllDeliveredOrders(HttpSession session){
	
	List<Order> orders= orderDao.getAllDeliveredOrders();
	return orders;
	}
	
	@RequestMapping(value="/getStoreUnassignedOrder" , method=RequestMethod.GET)
	public List<Order> getStoreUnassignedOrder() throws IOException{

	List<Order> orders= orderDao.getStoreUnaasigned();
	return orders;
	}
	
	@RequestMapping(value="/getAllOrderActivities/{trackingId}" , method=RequestMethod.GET)
	public List<OrderActivity> getAllOrderActivities(@PathVariable("trackingId") String trackingId) throws IOException{
		
		List<OrderActivity> orders= orderDao.getAllOrderActivities(trackingId);
		return orders;
	}
	
	
	@RequestMapping(value="/updateOrderStatus" , method=RequestMethod.POST)
	public void updateOrderStatus(@RequestBody Order object){

	orderDao.updateOrderStatus(object);
	}
	
	//*****************Generating run sheet with username starts************************//
	@RequestMapping(value="/generateRunSheetNumberWithUserName" , method=RequestMethod.POST)
		
		public void generateRunSheetWithUserName(@RequestBody Order order,HttpSession session){
			
			
			
		//	String RandomRunsheetNumber=generateRunSheetNumber(9);
			String RandomRunsheetNumber=generateUniqueRunsheetNumber();
			String sheet=order.getAssignedTo();
			//adding only runsheet and delivery boy name and date  to the runsheet table
			 orderDao.runSheetPartialDetails(RandomRunsheetNumber,order.getAssignedTo());
	}
		
	//*****************Generating run sheet with username ends************************//
		
	//Qamar's code for runsheet activation
@RequestMapping(value="/generateRunSheetNumber/{runSheetNumber},{DeliveryBoyName}" , method=RequestMethod.POST)
	
	public void generateRunSheet(@PathVariable ("runSheetNumber") String runSheetNumber,@PathVariable ("DeliveryBoyName") String DeliveryBoyName, HttpSession session){
		
		
		
//System.out.println("db name "+order.getAssignedTo());
		/*String RandomRunsheetNumber=generateUniqueRunsheetNumber();
		String sheet=order.getAssignedTo();
		System.out.println("assign ti :"+sheet);
		java.util.Date date= new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String newDate=dateFormat.format(date);
		String fff=newDate.replace(":", "-");
					String ggg=fff.replace("/", "-");
		sheet=sheet+""+ggg;
		
		System.out.println("file name is"+sheet);
		
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheets"+"/"+sheet+".csv";
		
		String path1="runsheets";
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(path1).getFile());
		System.out.println("befor adding " +file);
		 path1=file+"/"+sheet+".csv";
		 System.out.println("csv file path" +path1);*/
		System.out.println("runSheetNumber : DeliveryBoyName: "+runSheetNumber+" : "+DeliveryBoyName);
		String RandomRunsheetNumber=runSheetNumber;//"RUN012345";
		System.out.println("*********RandomRunsheetNumber************* : "+RandomRunsheetNumber);
		String sheet=DeliveryBoyName;//order.getAssignedTo();
		System.out.println("assign ti :"+sheet);
		java.util.Date date= new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String newDate=dateFormat.format(date);
		String fff=newDate.replace(":", "-");
					String ggg=fff.replace("/", "-");
		sheet=sheet+""+ggg;
		
		System.out.println("file name is"+sheet);
		System.out.println("RandomRunsheetNumber : "+RandomRunsheetNumber);
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheet_raw.csv";
		//String path1= System.getProperty("user.home") + "/Desktop"+"/runsheets"+"/"+sheet+".csv";
		
		String path1="runsheets";
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(path1).getFile());
		System.out.println("befor adding " +file);
		 path1=file+"/"+sheet+".csv";
		 System.out.println("csv file path" +path1);
		System.out.println("username : "+ sheet);//order.getDeliveredBy());
		
		 
		List<Order> ord=orderDao.getRunSheetDeliveries2(DeliveryBoyName,runSheetNumber);//.getRunSheetDeliveries2(RandomRunsheetNumber);
		//Order runsheeetVaulu=ord.get(0);
		
		System.out.println("runsheer number : "+runSheetNumber);
		
		//String runSheetNumber=runsheeetVaulu.getRunSheetNumber();
		System.out.println("runsheeet value in order controller is" +runSheetNumber);
		String accountNo=(String) session.getAttribute("loggedStore");
		Store obj=storeDao.getStoreByAccountNo(accountNo);
		
		
		System.out.println("store object : "+obj);
		
		 if(RandomRunsheetNumber!=null){
			 
				// System.out.println("Ofder dao"+dao);
				 
				/* System.out.println("Ofder dao"+orderDao);
				 orderDao.getAllOrders();*/
				 System.out.println("befor calling updateRunsheet");
				 
				 orderDao.getRunsheetNumber(DeliveryBoyName, runSheetNumber);
				 System.out.println("After calling updateRunsheet");
				 
				 
			 }
		
		
		
		
		
		
		
		try{
		FileWriter writer = new FileWriter(path1);
		//CSVWriter writer = new CSVWriter(new FileWriter(path1));
		
		
		//writer.writeNext("STORE ID");
		 
	    writer.append("STORE ID");
	    writer.append(',');
	    writer.write(obj.getName());
	    writer.append('\n');

	    writer.append("ASSIGNEE NAME");
	    writer.append(',');
	   // writer.write(order.getAssignedTo());
	    System.out.println("Delivery boy name : "+DeliveryBoyName);
	    writer.write(DeliveryBoyName);
            writer.append('\n');
			
	    writer.append("ASSIGNEE CONTACT NO");
	    writer.append(',');
	    writer.append("");
	    writer.append('\n');
	    
	    writer.append("RUNSHEET NUMBER");
	    writer.append(',');
	    writer.write(RandomRunsheetNumber);
	    writer.append('\n');
	    
	    writer.append('\n');
	    
	    writer.append("COD");
	    writer.append(',');
	    writer.append("CUSTOMER NAME");
	    writer.append(',');
	    writer.append("CUSTOMER ADDRESS");
	    writer.append(',');
	    writer.append("CONTACT NO");
	    writer.append(',');
	    writer.append("TRACKING ID");
	    writer.append(',');
	    writer.append("SHIPMENT TYPE");
	    writer.append(',');
	    writer.append("RETAILER ID");
	    writer.append(',');
	   writer.append('\n');
	   String retailerName=null;
	   String newAdd=null;
	   for(Order o:ord){
		   
		   if(o.getRetailerId()==3){
			   retailerName="Myntra";
		   }
		   else{
			   retailerName="retailer1";
		   }
		   
		   writer.append(""+o.getOrderValue());
		   writer.append(',');
		   writer.write(o.getCustomerName());
		   writer.append(',');
		   String customerAddress=o.getCustomerAddress().replace(",", ":");
		   String newcustomerAddress= customerAddress.replace("#", "No");
		   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
		  
		   /*if(newcustomerAddress!=null){
			  // System.out.println("Address is"+newAdd);
			   newAdd=   newcustomerAddress.trim();
			   System.out.println("Address After trimming"+newAdd);
		   }*/
		  System.out.println("Before"+newcustomerAddress);
		  newAdd=   newCustomerAddress1.trim();
		   System.out.println("Address After trimming"+newAdd);
		  
		   writer.write(newAdd);
		   writer.append(',');
		   writer.write(o.getCustomerContact());
		   writer.append(',');
		   writer.write(o.getTrackingId());
		   writer.append(',');
		   writer.write(o.getOrderType());
		   writer.append(',');
		   writer.write(retailerName);
		   writer.append(',');
		   writer.append('\n');
	   }
	    
	    
	    
	    
	    
			
	    //generate whatever data you want
			
	    writer.flush();
	    writer.close();
	}
		
	
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
		 RunsheetGenerator rsg= new RunsheetGenerator();
		 System.out.println("ordedao : "+orderDao);
		 System.out.println("ok");
         rsg.main4(orderDao,sheet);
		// return new ModelAndView("fileupload.html");
    }

	
	
	@RequestMapping(value="/storeRunSheetNumber" , method=RequestMethod.POST)
	public void generateRunsheetForStores(@RequestBody PudoStore store,HttpSession session){
		
		if(store.getOrderType().equalsIgnoreCase("Collect")||store.getOrderType().equalsIgnoreCase("Drop Off") ){
			
			 generateRunsheetForCollect(store,session);
		}
		
		else if(store.getOrderType().equalsIgnoreCase("Drop off")){
			
			
		}
		else{
		
		
		String RandomRunsheetNumber=generateRunSheetNumber(10);
		
	
		String storeName=store.getStoreName();
		String fileName=storeName;
		java.util.Date date= new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String newDate=dateFormat.format(date);
		String fff=newDate.replace(":", "-");
					String ggg=fff.replace("/", "-");
					fileName=fileName+""+ggg;
		
		
		//String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+fileName+".csv";
					String path1="storerunsheets";
					
					ClassLoader classLoader = getClass().getClassLoader();
					File file = new File(classLoader.getResource(path1).getFile());
					path1=file+"/"+fileName+".csv";
		System.out.println("complete file path is" +path1);
		System.out.println("store name is"+storeName);
		double storeId=storeDao.getStoreIdByName(storeName);
		Order order=new Order();
		order.setStoreId(storeId);
		order.setOrderType(store.getOrderType());
		
		List<Order> ord=orderDao.ordersForStoreRunSheet(order);
		/*Order runsheeetVaulu=ord.get(0);
		
		System.out.println("runsheer");
		
		String runSheetNumber=runsheeetVaulu.getRunSheetNumber();
		System.out.println("runsheeet value in order controller is" +runSheetNumber);*/
		String accountNo=(String) session.getAttribute("loggedStore");
		Store obj=storeDao.getStoreByAccountNo(accountNo);
		
		
		
		
		 if(RandomRunsheetNumber!=null){
			 
				
				 System.out.println("befor calling updateRunsheet");
				 
				 orderDao.getRunSheetNumberForStore(storeId, RandomRunsheetNumber,order.getOrderType());
				 System.out.println("After calling updateRunsheet");
				 
				 
			 }
		
		
		
		
		
		
		
		try{
		FileWriter writer = new FileWriter(path1);
	
		 
	    writer.append("STORE ID");
	    writer.append(',');
	    writer.write(obj.getName());
	    //writer.write(obj.getName());
	    writer.append('\n');

	    writer.append("ASSIGNEE NAME");
	    writer.append(',');
	    writer.write(storeName);
            writer.append('\n');
			
	    writer.append("ASSIGNEE CONTACT NO");
	    writer.append(',');
	    writer.append("");
	    writer.append('\n');
	    
	    writer.append("RUNSHEET NUMBER");
	    writer.append(',');
	    writer.write(RandomRunsheetNumber);
	    writer.append('\n');
	    
	    writer.append('\n');
	    
	    writer.append("COD");
	    writer.append(',');
	    writer.append("CUSTOMER NAME");
	    writer.append(',');
	    writer.append("CUSTOMER ADDRESS");
	    writer.append(',');
	    writer.append("CONTACT NO");
	    writer.append(',');
	    writer.append("TRACKING ID");
	    writer.append(',');
	    writer.append("SHIPMENT TYPE");
	    writer.append(',');
	    writer.append("RETAILER ID");
	    writer.append(',');
	   writer.append('\n');
	   String retailerName=null;
	   String newAdd=null;
	   for(Order o:ord){
		   
		   
		   
		   
		   if(o.getRetailerId()==3){
			   retailerName="Myntra";
		   }
		   else{
			   retailerName="retailer1";
		   }
		   
		   writer.append(""+o.getOrderValue());
		   writer.append(',');
		   writer.write(o.getCustomerName());
		   writer.append(',');
		   String customerAddress=o.getCustomerAddress().replace(",", ":");
		   String newcustomerAddress= customerAddress.replace("#", "No");
		   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
		 
		  System.out.println("Before"+newcustomerAddress);
		  newAdd=   newCustomerAddress1.trim();
		   System.out.println("Address After trimming"+newAdd);
		  
		   writer.write(newAdd);
		   writer.append(',');
		   writer.write(o.getCustomerContact());
		   writer.append(',');
		   writer.write(o.getTrackingId());
		   writer.append(',');
		   writer.write(o.getOrderType());
		   writer.append(',');
		   writer.write(retailerName);
		   writer.append(',');
		   writer.append('\n');
	   }
	    
	    
	    
	    
	    
			
	    //generate whatever data you want
			
	    writer.flush();
	    writer.close();
	}
		
	
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
		 RunsheetGenerator rsg= new RunsheetGenerator();
         rsg.main3(orderDao,fileName);
		// return new ModelAndView("fileupload.html");
    }
		
	
	}	
		
		
		
	public String generateRunSheetNumber(int length){
		Random number=new Random();
		
	/*	Random number=new Random();
		return number.nextInt(500);*/
		char[] values = {'M','L','D' ,'A','B','R','E','F','G','K','I','N','X','0','O','1','2','5','4','5','6','7','8','9'};
	      String out = "";
	      for (int i=0;i<length;i++) {
	          int idx=number.nextInt(values.length);
	        out += values[idx];
	      }
	      System.out.println(out);
	      return out;
	    }
		
	
	@RequestMapping(value="/getRunsheets", method=RequestMethod.POST)
	public List<UpdateDeliveryStatus> getRunsheets(@RequestBody Order order){
		System.out.println("Got"+order.getStoreId()+order.getAssignedTo()+order.getDeliveryBoyAssignDate());
		
		return orderDao.getRunSheets(order);
	}
	
	@RequestMapping(value="/getSelectedRunsheetDetails/{runsheetNumber}")
	public List<Order> getSelectedRunsheetDetails(@PathVariable("runsheetNumber") String runsheetNumber){
		
		return orderDao.getSelectedRunsheetDetails(runsheetNumber);
	}
	
	
	@RequestMapping(value="/updateOrdersByRunsheet")
	public void updateOrdersByRunsheet(@RequestBody List<Order> list){
		System.out.println("Got list"+list);
		Iterator<Order> itr=list.iterator();
		while(itr.hasNext()){
			Order obj=itr.next();
			System.out.println("tracking Id is " +obj.getTrackingId() + " order status is " +obj.getOrderStatus());
			orderDao.updateOrdersByRunsheet(obj);
		}
		
	}
	
	@RequestMapping(value="/closeRunsheet")
	public void closeRunSheet(@RequestBody String runSheetNumber){
		orderDao.closeRunSheet(runSheetNumber);
	}
	
	@RequestMapping(value="/getStoreRunSheets")
	public List<UpdateDeliveryStatus> getStoreRunsheets(@RequestBody StoreRunSheet obj){
		System.out.println(obj);
		return orderDao.getStoreRunsheets(obj);
	}
	
	@RequestMapping(value="/getStoreRunSheetsCash")
	public List<UpdateDeliveryStatus> getStoreRunSheetsCash(@RequestBody StoreRunSheet obj){
	System.out.println(obj);
	return orderDao.getStoreRunSheetsCash(obj);
	}
	
	@RequestMapping(value="/getOrdersByStoreRunsheet/{storeRunsheet}")
	public List<Order> getOrdersByStoreRunsheet(@PathVariable("storeRunsheet") String storeRunsheet){
		return orderDao.getOrdersByStoreRunSheet(storeRunsheet);
	}
	
	@RequestMapping(value="/closeStoreRunsheet")
	public void closeStoreRunsheet(@RequestBody String runSheetNumber){
		orderDao.closeStoreRunsheet(runSheetNumber);
	}
	
	
	public  void generateRunsheetForCollect(PudoStore details, HttpSession sessi){
		
			
		System.out.println("inside run sheet generator for collect");
		
		String RandomRunsheetNumber=generateRunSheetNumber(10);
		
		String storeName=details.getStoreName();
		String fileName=storeName;
		java.util.Date date= new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String newDate=dateFormat.format(date);
		String fff=newDate.replace(":", "-");
					String ggg=fff.replace("/", "-");
					fileName=fileName+""+ggg;
		
		
					//String path1= System.getProperty("user.home") + "/Desktop"+"/storerunsheets"+"/"+fileName+".csv";
					String path1="storerunsheets";
					ClassLoader classLoader = getClass().getClassLoader();
					File file = new File(classLoader.getResource(path1).getFile());
					path1=file+"/"+fileName+".csv";
		double storeId=storeDao.getStoreIdByName(storeName);
		Order order=new Order();
		order.setStoreId(storeId);
		order.setOrderType(details.getOrderType());
		
		List<Order> ord=orderDao.ordersForStoreRunSheet(order);
		Order runsheeetVaulu=ord.get(0);
		
		System.out.println("runsheer");
		
		String runSheetNumber=runsheeetVaulu.getRunSheetNumber();
		System.out.println("runsheeet value in order controller is" +runSheetNumber);
		String accountNo=(String) sessi.getAttribute("loggedStore");
		Store obj=storeDao.getStoreByAccountNo(accountNo);
		
		

		
		
		 if(RandomRunsheetNumber!=null){
			 
				
				 System.out.println("befor calling updateRunsheet");
				 
				 orderDao.getRunSheetNumberForStore(storeId, RandomRunsheetNumber,order.getOrderType());
				 System.out.println("After calling updateRunsheet");
				 
				 
			 }
		
		
		
		
		
		
		
		try{
		FileWriter writer = new FileWriter(path1);
	
		 
	    writer.append("STORE ID");
	    writer.append(',');
	    writer.write(obj.getName());
	    //writer.write(obj.getName());
	    writer.append('\n');

	    writer.append("ASSIGNEE NAME");
	    writer.append(',');
	    writer.write(storeName);
            writer.append('\n');
			
	    writer.append("ASSIGNEE CONTACT NO");
	    writer.append(',');
	    writer.append("");
	    writer.append('\n');
	    
	    writer.append("RUNSHEET NUMBER");
	    writer.append(',');
	    writer.write(RandomRunsheetNumber);
	    writer.append('\n');
	    
	    writer.append('\n');
	    
	    writer.append("COD");
	    writer.append(',');
	    writer.append("CUSTOMER NAME");
	    writer.append(',');
	    writer.append("CUSTOMER ADDRESS");
	    writer.append(',');
	    writer.append("CONTACT NO");
	    writer.append(',');
	    writer.append("TRACKING ID");
	    writer.append(',');
	    writer.append("SHIPMENT TYPE");
	    writer.append(',');
	    writer.append("RETAILER ID");
	    writer.append(',');
	   writer.append('\n');
	   String retailerName=null;
	   String newAdd=null;
	   for(Order o:ord){
		   
		   
		   
		   
		   if(o.getRetailerId()==3){
			   retailerName="Myntra";
		   }
		   else{
			   retailerName="retailer1";
		   }
		   
		   writer.append(""+o.getOrderValue());
		   writer.append(',');
		   writer.write(o.getCustomerName());
		   writer.append(',');
		   String customerAddress=o.getCustomerAddress().replace(",", ":");
		   String newcustomerAddress= customerAddress.replace("#", "No");
		   String newCustomerAddress1=newcustomerAddress.replace("\n" , ":").replace("\r", ":");
		 
		  System.out.println("Before"+newcustomerAddress);
		  newAdd=   newCustomerAddress1.trim();
		   System.out.println("Address After trimming"+newAdd);
		  
		   writer.write(newAdd);
		   writer.append(',');
		   writer.write(o.getCustomerContact());
		   writer.append(',');
		   writer.write(o.getTrackingId());
		   writer.append(',');
		   writer.write(o.getOrderType());
		   writer.append(',');
		   writer.write(retailerName);
		   writer.append(',');
		   writer.append('\n');
	   }
	    
	    
	    
	    
	    
			
	    //generate whatever data you want
			
	    writer.flush();
	    writer.close();
	}
		
	
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
		 RunsheetGenerator rsg= new RunsheetGenerator();
         rsg.main2(orderDao,fileName);
		// return new ModelAndView("fileupload.html");
    }
	
	
	
	@RequestMapping(value="/getOrderValueForRunSheet/{runsheetNumber}" , method=RequestMethod.GET)
	public @ResponseBody double getOrderValueForRunSheet(@PathVariable("runsheetNumber") String runsheetNumber) throws IOException{
		
		double orderValue= orderDao.valueForOrder(runsheetNumber);
		return orderValue;
	}
	
	
	@RequestMapping(value="/getRunsheetsForDeliveryBoys", method=RequestMethod.POST)
	public List<UpdateDeliveryStatus> getRunsheetsForDeliveryBoys(@RequestBody Order order,HttpSession session){
	System.out.println("Got"+order.getStoreId()+order.getAssignedTo()+order.getDeliveryBoyAssignDate());

	return orderDao.getRunsheetsForDeliveryBoys(order);
	}
	
	
	//generate unique random number
	public String generateUniqueRunsheetNumber(){
		
		int runsheetNumber=(int) orderDao.getTempRunNumber();
		String sequence="RUN";
		System.out.println("tracking id from table" +runsheetNumber);
	
		   String name = String.format("%06d", runsheetNumber);
		   System.out.println("asdfasdf"+name);
		   
		   String NewString=sequence+name;
		   
		    System.out.println("Runsheet number generated is" +NewString);
		    	runsheetNumber++;
		    	orderDao.storeTempRunNumber(runsheetNumber);
		    	return NewString;
	    }
	
	

	@RequestMapping(value="/correctNumbers", method=RequestMethod.GET)
	public void getRunsheetsF(){
	System.out.println("inside correctNumbers");
	 orderDao.updateNumbers();
	}
		
	@RequestMapping(value="/getStoreSummery", method=RequestMethod.GET)
	public List<StoreSummery> getDetailStoreSummery(HttpSession session){
	System.out.println("inside correctNumbers");
	List<StoreSummery> storeSummery= orderDao.getStoreSummery(session);
			return storeSummery;
	}
	
	@RequestMapping(value="/getDetailStoreSummery/{storeName},{status}", method=RequestMethod.GET)
	
	
	public List<Order> returnOrders(@PathVariable("storeName") String storeName,@PathVariable("status") String status) throws IOException{
		
		System.out.println("inside store store name is "+storeName+""+"status is "+status );
		List<Order> ord=null;
		ord=orderDao.DetailSummery(storeName,status);
		return ord;
	}
	
@RequestMapping(value="/generateRunSheet" , method=RequestMethod.GET)
	
	public void runSheet(HttpSession session){
		
		
		
	
		/*double RandomRunsheetNumber=generateUniqueRunsheetNumber();*/
		
		//	orderDao.storeTempNumber(RandomRunsheetNumber);
		
		
	
}

//************list of inactive run sheets start**********************//

@RequestMapping(value="/inactiveRunsheets/{assignTo}", method=RequestMethod.GET)

public List<String> getInactiveRunSheets(@PathVariable("assignTo") String assignTo,HttpSession session)
{
	System.out.println("inside inactive runsheets assignto value : "+assignTo);
	//	session.getAttribute("user)
		List<String> values=orderDao.listOfInactiveRunsheets(assignTo);
		System.out.println("&^&^&^&^ Before return");
		return values;
}

//************list of inactive run sheets ends**********************//
//************API for Urbun  tailor Qamar's Code starts************

@RequestMapping(value="/CreateOrderAPI", method=RequestMethod.POST)
public boolean CreatedOrderAPI(@RequestBody Order object) throws IOException{

System.out.println("Got Object inside controller : "+object);
boolean flag=orderDao.CreatedOrderAPI(object);
return flag;
}
//************API for Urbun  tailor Qamar's Code end************

//kishore surandar data
/*@RequestMapping(value="/searchOrder" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> searchOrder(@RequestBody SearchOrder searchOrder) {
	System.out.println("Inside searchOrder in OrderController");
	System.out.println("OrderStatus: "+searchOrder.getOrderStatus());
	System.out.println("OrderId: "+searchOrder.getOrderId());
	System.out.println("FromDate: "+searchOrder.getFromDate());
	System.out.println("ToDate: "+searchOrder.getToDate());
	System.out.println("RetailerName: "+searchOrder.getRetailerName());
	List<Order> listOfPendingReturns = orderDao.getOrdersBasedOnSearch1(searchOrder); 
	System.out.println("response is "+listOfPendingReturns.size());
	return listOfPendingReturns;
}*/

@RequestMapping(value="/searchOrder" , method=RequestMethod.POST)
public @ResponseBody List<com.dataisys.taykit.model.Order> searchOrder(@RequestBody SearchOrder searchOrder) {
    System.out.println("Inside searchOrder in OrderController");
    System.out.println("OrderStatus: "+searchOrder.getOrderStatus());
    System.out.println("OrderId: "+searchOrder.getOrderId());
    System.out.println("FromDate: "+searchOrder.getFromDate());
    System.out.println("ToDate: "+searchOrder.getToDate());
    System.out.println("RetailerName: "+searchOrder.getRetailerName());
    List<Order> listOfPendingReturns = orderDao.getOrdersBasedOnSearch1(searchOrder); 
    System.out.println("response is "+listOfPendingReturns.size());
    
    return listOfPendingReturns;
}
@RequestMapping(value="/assignStoreReturn/{storeName}", method=RequestMethod.POST)
public void updateOrderReturn(@RequestBody Order object,@PathVariable("storeName") String storeName) throws IOException{
    System.out.println("Got Object"+object +storeName );
    double id=(int)storeDao.getStoreIdByName(storeName);
    System.out.println("the store id obtained "+id);
    orderDao.updateAssignReturn(object,id);
    System.out.println("db has been updated ");
}
	}
				
					