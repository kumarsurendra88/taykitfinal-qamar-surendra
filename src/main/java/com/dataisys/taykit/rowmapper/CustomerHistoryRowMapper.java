package com.dataisys.taykit.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dataisys.taykit.model.CustomerHistory;

public class CustomerHistoryRowMapper implements RowMapper<CustomerHistory> {
	
	public CustomerHistory mapRow(ResultSet rs, int rowNum) throws SQLException {		
		CustomerHistory customerHistory = new CustomerHistory();
		customerHistory.setCustomerHistoryId(rs.getDouble("customer_history_id"));
		customerHistory.setCustomerName(rs.getString("customer_name"));
		customerHistory.setCustomerMobile(rs.getString("customer_contact"));
		customerHistory.setTrackingId(rs.getString("tracking_id"));
		
		return customerHistory;
	}
}
